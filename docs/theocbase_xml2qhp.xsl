<?xml version='1.0'?>
<xsl:stylesheet version="1.1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="utf-8" indent="yes"/>
<xsl:param name="lang" select="'en'"/>

<xsl:strip-space elements="keyword"/>

<xsl:template match="text()|@*"/>
<xsl:template match="text()|@*" mode="keywords"/>
<xsl:template match="text()|@*" mode="files"/>

<xsl:template match="/">
    <xsl:element name="QtHelpProject">
        <xsl:attribute name="version">1.0</xsl:attribute>
        <xsl:element name="namespace">net.theocbase</xsl:element>
        <xsl:element name="virtualFolder">doc</xsl:element>
        <xsl:element name="filterSection">
            <xsl:element name="toc">
                <xsl:apply-templates/>
            </xsl:element>
            <xsl:element name="keywords">
                <xsl:apply-templates mode="keywords"/>
            </xsl:element>
            <xsl:element name="files">
                <xsl:apply-templates mode="files"/>
            </xsl:element>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="keyword" mode="keywords">
    <xsl:element name="keyword">
        <xsl:attribute name="name"><xsl:value-of select="name"/></xsl:attribute>
        <xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute>
        <xsl:attribute name="ref"><xsl:value-of select="concat($lang, substring-after(@ref, 'en'))"/></xsl:attribute>
    </xsl:element>
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="section">
    <xsl:element name="section">
        <xsl:attribute name="title"><xsl:value-of select="title"/></xsl:attribute>
        <xsl:attribute name="ref"><xsl:value-of select="concat($lang, substring-after(@ref, 'en'))"/></xsl:attribute>
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

<xsl:template match="file" mode="files">
  <xsl:choose>
    <xsl:when test="starts-with(@file, 'en')">
      <xsl:element name="file"><xsl:value-of select="concat($lang, substring-after(@file, 'en'))"/></xsl:element>
    </xsl:when>
    <xsl:otherwise>
      <xsl:element name="file"><xsl:value-of select="@file"/></xsl:element>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
