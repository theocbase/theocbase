#include <QtTest>
#include <QTest>
#include <QDebug>
#include "../common/common.h"
#include "sql_class.h"
#include "weekinfo.h"
#include "ccongregation.h"
#include "specialevent.h"
#include "lmm_schedule.h"
#include "cpublictalks.h"

class test_weekinfo : public QObject
{
    Q_OBJECT

public:
    test_weekinfo();
    ~test_weekinfo();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test1_noexception();
    void test2_covisit();
    void test3_clearException();
    void test4_assemblyexception();
    void test5_conventionexception();
    void test6_virtualConventionException();
    void test7_memorialexception();
    void test8_otherexception();
};

test_weekinfo::test_weekinfo()
{
}

test_weekinfo::~test_weekinfo()
{
}

void test_weekinfo::initTestCase()
{
    common::initDataBase();
}

void test_weekinfo::cleanupTestCase()
{
    common::clearDatabase();
}

void test_weekinfo::test1_noexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek());
    qDebug() << d;
    wi.setDate(d);
    wi.load();
    QVERIFY(wi.specialEvent() == SpecialEvent::None);
    QVERIFY(wi.exceptionDisplayText().isEmpty());
}

void test_weekinfo::test2_covisit()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek());
    qDebug() << d;

    // counselor 2 and 3 should be removed
    // CBS conductor and reader should be removed
    // wt reader should be removed

    auto *counselor2 = new person();
    counselor2->setGender(person::Male);
    counselor2->setFirstname("Class2");
    counselor2->setLastname("Counselor");
    counselor2->setUsefor(person::LMM_Chairman);
    cpersons::addPerson(counselor2);

    auto *counselor3 = new person();
    counselor3->setGender(person::Male);
    counselor3->setFirstname("Class3");
    counselor3->setLastname("Counselor");
    counselor3->setUsefor(person::LMM_Chairman);
    cpersons::addPerson(counselor3);

    auto *cbsconductor = new person();
    cbsconductor->setGender(person::Male);
    cbsconductor->setFirstname("Conductor");
    cbsconductor->setLastname("CBS");
    cbsconductor->setUsefor(person::CBSConductor);
    cpersons::addPerson(cbsconductor);

    auto *cbsreader = new person();
    cbsreader->setGender(person::Male);
    cbsreader->setFirstname("Reader");
    cbsreader->setLastname("CBS");
    cbsreader->setUsefor(person::CBSReader);
    cpersons::addPerson(cbsreader);

    auto *wtreader = new person();
    wtreader->setGender(person::Male);
    wtreader->setFirstname("Reader");
    wtreader->setLastname("WT");
    wtreader->setUsefor(person::WtReader);
    cpersons::addPerson(wtreader);

    // Add CBS schedule row
    LMM_Schedule cbs(MeetingPart::LMM_CBS, 0, d, "CBS THEME", "...", -1, 30);
    cbs.save();

    LMM_Meeting meeting;
    meeting.loadMeeting(d);
    meeting.setBibleReading("BIBLE READING");
    meeting.setCounselor2(counselor2);
    meeting.setCounselor3(counselor3);

    meeting.loadAssignments();
    auto cbsassignment = qobject_cast<LMM_Assignment_ex *>(meeting.getAssignment(MeetingPart::LMM_CBS));
    QVERIFY(cbsassignment != nullptr);
    cbsassignment->setSpeaker(cbsconductor);
    cbsassignment->setAssistant(cbsreader);
    cbsassignment->save();

    meeting.save();

    // WT Reader
    cpublictalks cp;
    auto weekendmeeting = cp.getMeeting(d);
    weekendmeeting->setWtReader(wtreader);
    weekendmeeting->save();

    wi.setDate(d);
    wi.load();
    wi.setSpecialEvent(SpecialEvent::CircuitOverseersVisit);
    wi.setExceptionStart(d.addDays(1));
    wi.setExceptionEnd(d.addDays(6));

    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::CircuitOverseersVisit);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    meeting.loadMeeting(d);
    // check counselor 2 and 3
    QCOMPARE(meeting.counselor2(), nullptr);
    QCOMPARE(meeting.counselor3(), nullptr);

    meeting.loadAssignments();
    cbsassignment = qobject_cast<LMM_Assignment_ex *>(meeting.getAssignment(MeetingPart::LMM_CBS));
    // check CBS
    QVERIFY(cbsassignment == nullptr);

    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item i;
    i.insert("date", d);
    i.insert("talk_id", MeetingPartClass::toDbTalkId(MeetingPart::LMM_CBS, 0));
    int scheduleid = sql->selectScalar("SELECT id FROM lmm_schedule WHERE date = :date", &i, -1).toInt();
    i.clear();
    i.insert("id", scheduleid);
    sql_items items = sql->selectSql("SELECT * FROM lmm_assignment WHERE lmm_schedule_id = :id", &i);
    sql_item item = items[0];
    QCOMPARE(item.value("assignee_id").toInt(), -1);
    QCOMPARE(item.value("assistant_id").toInt(), -1);

    // check WT reader
    weekendmeeting = cp.getMeeting(d);
    QCOMPARE(weekendmeeting->wtReader(), nullptr);
}

void test_weekinfo::test3_clearException()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek());
    wi.setDate(d);
    wi.load();

    QVERIFY(wi.specialEvent() != SpecialEvent::None);
    wi.setSpecialEvent(SpecialEvent::None);
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::None);
    QVERIFY(wi.exceptionText().isEmpty());
}

void test_weekinfo::test4_assemblyexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setSpecialEvent(SpecialEvent::CircuitAssembly);
    wi.setExceptionStart(d.addDays(6));
    wi.setExceptionEnd(d.addDays(6));
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::CircuitAssembly);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no meetings
    QVERIFY(wi.midweekDay() == 0);
    QVERIFY(wi.weekendDay() == 0);
    // start and end day should be same
    QVERIFY(wi.exceptionStart() == wi.exceptionEnd());
}

void test_weekinfo::test5_conventionexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setSpecialEvent(SpecialEvent::Convention);
    wi.setExceptionStart(d.addDays(4));
    wi.setExceptionEnd(d.addDays(6));
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::Convention);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no meetings
    QVERIFY(wi.midweekDay() == 0);
    QVERIFY(wi.weekendDay() == 0);
    // start and end day should not be same
    QVERIFY(wi.exceptionStart() != wi.exceptionEnd());
}

void test_weekinfo::test6_virtualConventionException()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setSpecialEvent(SpecialEvent::VirtualConvention);
    QString exceptionText = "Virtual Convention (Friday Morning)";
    wi.setExceptionText(exceptionText);
    wi.setExceptionStart(d.addDays(0));
    wi.setExceptionEnd(d.addDays(6));
    // midweek meeting on Thursday
    wi.setMidweekDay(3);
    // weekend meeting on Sunday
    wi.setWeekendDay(7);
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::VirtualConvention);
    QVERIFY(wi.exceptionDisplayText() == exceptionText);

    // meeting days
    QVERIFY(wi.midweekDay() == 3);
    QVERIFY(wi.weekendDay() == 7);
}

void test_weekinfo::test7_memorialexception()
{
    WeekInfo wi;
    // Memorial on Tuesday
    QDate memorialDate(2020, 4, 7);
    QDate d = memorialDate.addDays(1 - memorialDate.dayOfWeek());
    wi.setDate(d);
    wi.load();

    wi.setSpecialEvent(SpecialEvent::Memorial);
    wi.setExceptionStart(memorialDate);
    wi.setExceptionEnd(memorialDate);
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::Memorial);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no midweek meeting
    QVERIFY(wi.midweekDay() == 0);
    QVERIFY(wi.weekendDay() != 0);

    // Memorial on Saturday
    memorialDate.setDate(2021, 3, 27);
    d = memorialDate.addDays(1 - memorialDate.dayOfWeek());
    wi.setDate(d);
    wi.load();

    wi.setSpecialEvent(SpecialEvent::Memorial);
    wi.setExceptionStart(memorialDate);
    wi.setExceptionEnd(memorialDate);
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::Memorial);
    QVERIFY(!wi.exceptionDisplayText().isEmpty());

    // no weekend meeting
    QVERIFY(wi.midweekDay() != 0);
    QVERIFY(wi.weekendDay() == 0);
}

void test_weekinfo::test8_otherexception()
{
    WeekInfo wi;
    QDate d = QDate::currentDate();
    d = d.addDays(1 - d.dayOfWeek() + 7);
    wi.setDate(d);
    wi.load();

    wi.setSpecialEvent(SpecialEvent::Other);
    QString exceptionText = "My custom exception";
    wi.setExceptionText(exceptionText);
    wi.setExceptionStart(d.addDays(0));
    wi.setExceptionEnd(d.addDays(6));
    // midweek meeting on Wednesday
    wi.setMidweekDay(3);
    // weekend meeting on Saturday
    wi.setWeekendDay(6);
    wi.saveChanges();

    QVERIFY(wi.specialEvent() == SpecialEvent::Other);
    QVERIFY(wi.exceptionDisplayText() == exceptionText);

    // meeting days
    QVERIFY(wi.midweekDay() == 3);
    QVERIFY(wi.weekendDay() == 6);
}

QTEST_MAIN(test_weekinfo)

#include "test_weekinfo.moc"
