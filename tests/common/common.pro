QT += widgets sql network quickwidgets networkauth qml

TEMPLATE = lib

HEADERS += \
    $$PWD/common.h \
    ../../src/singleton.h \
    ../../src/sql_class.h \
    ../../src/wtimport.h \
    ../../src/importlmmworkbook.h \
    ../../src/epub.h \
    ../../src/internet.h \
    ../../src/jwpub.h \
    ../../src/xml_reader.h \
    ../../src/zipper.h \
    ../../src/zip/qzipreader_p.h \
    ../../src/zip/qzipwriter_p.h \
    ../../src/jwpub.h \
    ../../src/internet.h \
    ../../src/lmm_meeting.h \
    ../../src/lmm_schedule.h \
    ../../src/assignmentInfo.h \
    ../../src/lmm_assignment.h \
    ../../src/lmm_assignment_ex.h \
    ../../src/family.h \
    ../../src/cpublictalks.h \
    ../../src/cpersons.h \
    ../../src/person.h \
    ../../src/ccongregation.h \
    ../../src/specialevent.h \
    ../../src/sortfilterproxymodel.h \
    ../../src/sqlcombo.h \
    ../../src/accesscontrol.h \
    ../../src/sync_cloud.h \
    ../../src/cloud/cloud_controller.h \
    ../../src/cloud/dropbox.h \
    ../../src/tbstyle.h

SOURCES += \
    ../../src/sql_class.cpp \
    ../../src/wtimport.cpp \
    ../../src/importlmmworkbook.cpp \
    ../../src/xml_reader.cpp \
    ../../src/epub.cpp \
    ../../src/zipper.cpp \
    ../../src/zip/qzip.cpp \
    ../../src/jwpub.cpp \
    ../../src/internet.cpp \
    ../../src/lmm_meeting.cpp \
    ../../src/lmm_schedule.cpp \
    ../../src/assignmentInfo.cpp \
    ../../src/lmm_assignment.cpp \
    ../../src/lmm_assignment_ex.cpp \
    ../../src/family.cpp \
    ../../src/cpublictalks.cpp \
    ../../src/cpersons.cpp \
    ../../src/person.cpp \
    ../../src/ccongregation.cpp \
    ../../src/specialevent.cpp \
    ../../src/sqlcombo.cpp \
    ../../src/accesscontrol.cpp \
    ../../src/sync_cloud.cpp \
    ../../src/cloud/cloud_controller.cpp \
    ../../src/cloud/dropbox.cpp \
    ../../src/tbstyle.cpp

macx: LIBS += -lz
unix: LIBS += -lz

RESOURCES += ../../src/database.qrc

DEFINES += TB_LIBRARY
