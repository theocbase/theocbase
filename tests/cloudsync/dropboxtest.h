#ifndef DROPBOXTEST_H
#define DROPBOXTEST_H

#include "cloud/dropbox.h"
#include <QDir>

class dropboxtest : public dropbox
{
public:
    dropboxtest(bool deleteTestFiles);

    void authenticate() override;
    QDateTime getModifiedDate(QString cloudFileName) override;
    bool logged() override;
    void download(QString relativeCloudFileName, QByteArray &content, QDateTime &modifiedDate) override;
    Q_INVOKABLE DBAccount *getAccountInfo(bool forceLoad = false) override;
    QDateTime upload(QByteArray data, QString relativeCloudFileName) override;
    void revoke() override;

    QString getLocalFileName(QString fileName);

private:    
    bool m_logged = false;
    QString m_folder;
};

#endif // DROPBOXTEST_H
