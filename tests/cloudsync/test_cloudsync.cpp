#include <QtTest>
#include "../common/common.h"
#include "sql_class.h"
#include "accesscontrol.h"
#include "cloud/cloud_controller.h"
#include "dropboxtest.h"
#include "cpersons.h"
#include "lmm_meeting.h"

class test_cloudsync : public QObject
{
    Q_OBJECT

public:
    test_cloudsync();
    ~test_cloudsync();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_AccessControl_NotLogged();
    void test_Sync1();
    void test_SyncConflict();
    void test_CloudReset();
    void test_CloudReset2();

private:
    void setRoles(User *user, QList<Permission::RoleId> roleIds);
};

test_cloudsync::test_cloudsync()
{
}

test_cloudsync::~test_cloudsync()
{
}

void test_cloudsync::initTestCase()
{
    common::initDataBase();
}

void test_cloudsync::cleanupTestCase()
{
    common::clearDatabase();
}

void test_cloudsync::test_AccessControl_NotLogged()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    QSharedPointer<cloud_controller> cloud(new cloud_controller(nullptr, new dropboxtest(true)));
    if (cloud->logged())
        cloud->logout();
    else
        cloud->initAccessControl();

    auto user = ac->user();
    QVERIFY(user);
    // All roles except administrator
    for (auto role : ac->roles()) {
        qDebug() << role.id();
        if (role.id() != Permission::RoleId::Administrator)
            QVERIFY(user->hasRole(role));
    }
}

void test_cloudsync::test_Sync1()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    QSharedPointer<cloud_controller> cloud(new cloud_controller(nullptr, new dropboxtest(true)));

    QSignalSpy spySyncConflict(cloud.data(), &cloud_controller::syncConflict);
    QSignalSpy spyCloudResetFound(cloud.data(), &cloud_controller::cloudResetFound);
    QSignalSpy spyCloudResetStarted(cloud.data(), &cloud_controller::cloudResetStarted);
    QSignalSpy spyCloudResetDone(cloud.data(), &cloud_controller::cloudResetFinished);

    cloud->clearDatabase();

    cloud->login();
    cloud->initAccessControl();

    auto user = ac->user();
    setRoles(user, { Permission::RoleId::Administrator, Permission::RoleId::LMMOverseer });

    // Save midweek meeting
    QDate d = QDate::currentDate();
    d = d.addDays((d.dayOfWeek() - 1) * -1);
    QString bibleReading = "GENESIS 1-3";
    LMM_Meeting meeting;
    meeting.loadMeeting(d);
    meeting.setBibleReading(bibleReading);

    // Add new person
    auto *chairman = new person();
    chairman->setGender(person::Male);
    chairman->setFirstname("T");
    chairman->setLastname("B");
    chairman->setUsefor(person::LMM_Chairman);
    chairman->setCongregationid(1);
    cpersons::addPerson(chairman);

    meeting.setChairman(chairman);
    meeting.save();

    // Synchronize data
    cloud->synchronize();

    // Logout and clear DB
    cloud->logout();

    meeting.loadMeeting(d);
    QVERIFY(meeting.bibleReading().isEmpty());
    QCOMPARE(meeting.chairman(), nullptr);
    cpersons cp;
    QCOMPARE(cp.getAllPersons(0).size(), 0);

    // Login and sync data from cloud
    cloud->login();
    cloud->initAccessControl();
    cloud->synchronize();

    // Load meeting and check the data
    meeting.loadMeeting(d);
    QCOMPARE(meeting.bibleReading(), bibleReading);
    QVERIFY(meeting.chairman() != nullptr);
    QCOMPARE(meeting.chairman()->fullname(), "T B");
    QCOMPARE(cp.getAllPersons(0).size(), 1);

    cloud->logout();

    QVERIFY(spyCloudResetFound.empty());
    QVERIFY(spySyncConflict.empty());
}

void test_cloudsync::test_SyncConflict()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    QSharedPointer<cloud_controller> cloud(new cloud_controller(nullptr, new dropboxtest(true)));
    QSignalSpy spySyncConflict(cloud.data(), &cloud_controller::syncConflict);
    QSignalSpy spyCloudResetFound(cloud.data(), &cloud_controller::cloudResetFound);
    QSignalSpy spyCloudResetStarted(cloud.data(), &cloud_controller::cloudResetStarted);
    QSignalSpy spyCloudResetDone(cloud.data(), &cloud_controller::cloudResetFinished);

    cloud->clearDatabase();

    cloud->login();
    cloud->initAccessControl();

    auto user = ac->user();
    setRoles(user, { Permission::RoleId::Administrator, Permission::RoleId::Elder });

    QDate d = QDate::currentDate();
    d = d.addDays((d.dayOfWeek() - 1) * -1);

    LMM_Meeting meeting1;
    meeting1.loadMeeting(d);
    meeting1.setNotes("TB TEST");
    meeting1.saveNotes();

    cloud->synchronize();

    cloud->logout();

    // login as admin
    cloud->login();
    cloud->initAccessControl();
    user = ac->user();
    setRoles(user, { Permission::RoleId::Administrator, Permission::RoleId::Elder });

    meeting1.loadMeeting(d);
    meeting1.setNotes("admin user");
    meeting1.saveNotes();

    cloud->synchronize();

    cloud->logout();

    // Admin user should see the message about the sync conflict
    QCOMPARE(spySyncConflict.count(), 1);

    spySyncConflict.clear();

    // login as normal user
    cloud->login();
    cloud->initAccessControl();
    user = ac->user();
    setRoles(user, { Permission::RoleId::Elder });

    meeting1.loadMeeting(d);
    meeting1.setNotes("normal user");
    meeting1.saveNotes();

    cloud->synchronize();

    cloud->logout();

    // Normal user should not see message about the sync conflict
    QVERIFY(spySyncConflict.isEmpty());

    qDebug() << "Sync Conflict Singal" << spySyncConflict.count();
    qDebug() << "Cloud Reset Found Signal" << spyCloudResetFound.count();
    qDebug() << "Cloud Reset Started Singal" << spyCloudResetStarted.count();
    qDebug() << "Cloud Reset Done Signal" << spyCloudResetDone.count();
}

void test_cloudsync::test_CloudReset()
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();
    auto db = new dropboxtest(true);
    QSharedPointer<cloud_controller> cloud(new cloud_controller(nullptr, db));
    QSignalSpy spySyncConflict(cloud.data(), &cloud_controller::syncConflict);
    QSignalSpy spyCloudResetFound(cloud.data(), &cloud_controller::cloudResetFound);
    QSignalSpy spyCloudResetStarted(cloud.data(), &cloud_controller::cloudResetStarted);
    QSignalSpy spyCloudResetDone(cloud.data(), &cloud_controller::cloudResetFinished);

    cloud->clearDatabase();

    cloud->login();
    cloud->initAccessControl();

    auto user = ac->user();
    setRoles(user, { Permission::RoleId::LMMOverseer });

    // add person
    auto *p = new person();
    p->setGender(person::Male);
    p->setFirstname("T");
    p->setLastname("B");
    p->setUsefor(person::LMM_Chairman);
    p->setCongregationid(1);
    cpersons::addPerson(p);

    cloud->synchronize();

    // read syncfile
    QFile file(db->getLocalFileName(cloud->authentication()->getAccountInfo()->getSyncFile()));
    file.open(QFile::ReadOnly);
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    QJsonObject json = doc.object();
    file.close();

    cloud->logout();

    // login as normal user
    cloud->login();
    cloud->initAccessControl();

    user = ac->user();
    setRoles(user, { Permission::RoleId::LMMOverseer });
    cpersons cp;

    auto p2 = new person();
    p2->setGender(person::Male);
    p2->setFirstname("Second");
    p2->setLastname("Test");
    p2->setUsefor(person::LMM_BibleReading);
    p2->setCongregationid(1);
    cpersons::addPerson(p2);

    QCOMPARE(cp.getAllPersons(0).size(), 1);

    cloud->synchronize();

    QCOMPARE(cp.getAllPersons(0).size(), 2);

    // do cloud reset => restore old json content and change reset_time and sync_id
    json["reset_time"] = QVariant(QDateTime::currentDateTime().toSecsSinceEpoch()).toString();
    json["sync_id"] = 20;

    file.remove();

    file.open(QFile::WriteOnly);
    file.write(QJsonDocument(json).toJson());
    file.close();

    cloud->synchronize();

    // normal user should not get a message about the cloud reset
    QVERIFY(spyCloudResetFound.isEmpty());
    QVERIFY(spySyncConflict.isEmpty());
    QCOMPARE(cp.getAllPersons(0).size(), 1);
    auto personCheck = cp.getAllPersons(0).at(0);
    QCOMPARE(personCheck->fullname(), "T B");

    cloud->logout(1);

    // Login as admin
    cloud->login();
    cloud->initAccessControl();

    user = ac->user();
    setRoles(user, { Permission::RoleId::Administrator, Permission::RoleId::LMMOverseer });

    QDate d = QDate::currentDate();
    d = d.addDays((d.dayOfWeek() - 1) * -1);
    LMM_Meeting meeting;
    meeting.loadMeeting(d);
    meeting.setNotes("Admin");
    meeting.saveNotes();

    cloud->synchronize();

    json["reset_time"] = QVariant(QDateTime::currentDateTime().toSecsSinceEpoch()).toString();
    json["sync_id"] = 30;
    file.remove();
    file.open(QFile::WriteOnly);
    file.write(QJsonDocument(json).toJson());
    file.close();

    cloud->synchronize();
    // admin user should see the message about the cloud reset
    QCOMPARE(spyCloudResetFound.size(), 1);
}

void test_cloudsync::test_CloudReset2()
{
    QString dbfile = common::databasePath();
    QString dbFileAdmin = QDir::tempPath() + QDir::separator() + "theocbase_admin_1.sqlite";
    QString dbFileUser = QDir::tempPath() + QDir::separator() + "theocbase_user_1.sqlite";

    if (QFile::exists(dbFileAdmin))
        QFile::remove(dbFileAdmin);
    if (QFile::exists(dbFileUser))
        QFile::remove(dbFileUser);

    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->closeConnection();
    common::initDataBase();

    AccessControl *ac = &Singleton<AccessControl>::Instance();
    auto db = new dropboxtest(true);
    QSharedPointer<cloud_controller> cloud(new cloud_controller(nullptr, db));
    QSignalSpy spySyncConflict(cloud.data(), &cloud_controller::syncConflict);
    QSignalSpy spyCloudResetFound(cloud.data(), &cloud_controller::cloudResetFound);
    QSignalSpy spyCloudResetStarted(cloud.data(), &cloud_controller::cloudResetStarted);
    QSignalSpy spyCloudResetDone(cloud.data(), &cloud_controller::cloudResetFinished);
    QSignalSpy spyCloudSyncDone(cloud.data(), &cloud_controller::syncFinished);

    cpersons cp;

    //    1. log out from dropbox
    //    2. remove theocbase.sqlite
    //    3. start theocbase
    cloud->clearDatabase();

    //    4. login as admin
    cloud->login();
    cloud->initAccessControl();
    auto user = ac->user();
    setRoles(user, { Permission::RoleId::Administrator, Permission::RoleId::LMMOverseer });

    auto p = new person();
    p->setFirstname("T");
    p->setLastname("B");
    p->setCongregationid(1);
    cp.addPerson(p);

    //    5. synchronize with admin user
    cloud->synchronize();

    QCOMPARE(spySyncConflict.size(), 0);
    QCOMPARE(spyCloudSyncDone.size(), 1);

    //    6. exit theocbase
    sql->closeConnection();

    //    7. rename theocbase.sqlite as theocbase_admin_1.sqlite
    QFile::rename(dbfile, dbFileAdmin);

    //    8. start theocbase and log out from dropbox
    common::initDataBase();

    cloud->logout(0);

    //    9. login as non-admin
    cloud->login();
    cloud->initAccessControl();
    user = ac->user();
    setRoles(user, { Permission::RoleId::LMMOverseer });

    //    10. synchronize with admin user's shared data
    cloud->synchronize();

    //    11. make changes (e.g. midweek meeting notes)
    QDate d = QDate::currentDate();
    d = d.addDays((d.dayOfWeek() - 1) * -1);
    LMM_Meeting meeting;
    meeting.loadMeeting(d);
    meeting.setNotes("Sync Test");
    meeting.saveNotes();

    //    12. synchronize
    cloud->synchronize();

    //    13. exit theocbase
    sql->closeConnection();

    //    14. rename theocbase.sqlite as theocbase_user_1.sqlite
    QFile::rename(dbfile, dbFileUser);

    //    15. start theocbase and log out from dropbox
    common::initDataBase();
    cloud->logout(0);

    //    16. exit theocbase
    sql->closeConnection();

    //    17. copy theocbase_admin_1.sqlite to theocbase.sqlite
    QFile::remove(dbfile);
    QFile::copy(dbFileAdmin, dbfile);

    //    18. start theocbase and login as admin
    common::initDataBase();
    cloud->login();
    cloud->initAccessControl();
    user = ac->user();
    setRoles(user, { Permission::RoleId::LMMOverseer, Permission::RoleId::Administrator });

    //    19. make cloud reset as admin
    cloud->resetCloudData();
    QCOMPARE(spyCloudResetStarted.size(), 1);
    QCOMPARE(spyCloudResetDone.size(), 1);

    //    20. check for changes from #11 (shouldn't appear)
    meeting.loadMeeting(d);
    QVERIFY(meeting.notes().isEmpty());

    //    21. log out and exit theocbase
    cloud->logout(0);
    sql->closeConnection();

    //    22. copy theocbase_user_1.sqlite to theocbase.sqlite
    QFile::remove(dbfile);
    QFile::copy(dbFileUser, dbfile);

    //    23. start theocbase and login as user
    common::initDataBase();
    cloud->login();
    cloud->initAccessControl();
    user = ac->user();
    setRoles(user, { Permission::RoleId::LMMOverseer });

    //    24. synchronize - message about cloud reset should not appear
    spyCloudSyncDone.clear();
    spyCloudResetFound.clear();
    cloud->synchronize();
    QCOMPARE(spyCloudSyncDone.size(), 1);
    QCOMPARE(spyCloudResetFound.size(), 0);

    //    25. check for changes from #11 (should be removed)
    meeting.loadMeeting(d);
    QVERIFY(meeting.notes().isEmpty());

    //    26. exit theocbase
    sql->closeConnection();

    //    27. copy theocbase_admin_1.sqlite as theocbase.sqlite
    QFile::remove(dbfile);
    QFile::copy(dbFileAdmin, dbfile);

    //    28. start theocbase and login as admin
    common::initDataBase();
    cloud->login();
    cloud->initAccessControl();
    user = ac->user();
    setRoles(user, { Permission::RoleId::LMMOverseer, Permission::RoleId::Administrator });

    //    29. synchronize - message about cloud reset should appear
    spyCloudSyncDone.clear();
    spyCloudResetFound.clear();
    cloud->synchronize();
    QCOMPARE(spyCloudResetFound.size(), 1);
    QCOMPARE(spyCloudSyncDone.size(), 0);

    //    30. check for changes from #11 (shouldn't appear)
    meeting.loadMeeting(d);
    QVERIFY(meeting.notes().isEmpty());
    QCOMPARE(sql->selectSql("select * from persons").size(), 1);
}

void test_cloudsync::setRoles(User *user, QList<Permission::RoleId> roleIds)
{
    AccessControl *ac = &Singleton<AccessControl>::Instance();

    QList<const Role *> roleList;

    for (const Role &role : ac->roles()) {
        if (roleIds.contains(role.id())) {
            Role *newRole = new Role();
            newRole->setId(role.id());
            newRole->setPermissions(role.permissions());
            roleList.append(newRole);
        }
    }
    user->setRoles(roleList);
}

QTEST_MAIN(test_cloudsync)

#include "test_cloudsync.moc"
