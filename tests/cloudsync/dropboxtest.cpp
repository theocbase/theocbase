#include "dropboxtest.h"

dropboxtest::dropboxtest(bool deleteTestFiles)
{
    m_folder = QDir(SRCDIR).filePath("files");

    QDir dir(m_folder);
    if (deleteTestFiles && dir.exists())
        dir.removeRecursively();
}

void dropboxtest::authenticate()
{
    m_logged = true;
    emit statusChanged(QAbstractOAuth::Status::Granted);
}

QDateTime dropboxtest::getModifiedDate(QString cloudFileName)
{
    QString filePath = getLocalFileName(cloudFileName);
    if (QFile::exists(filePath)) {
        QFileInfo info(filePath);
        return info.lastModified();
    } else {
        return QDateTime();
    }
}

bool dropboxtest::logged()
{
    return m_logged;
}

void dropboxtest::download(QString relativeCloudFileName, QByteArray &content, QDateTime &modifiedDate)
{
    QString filePath = getLocalFileName(relativeCloudFileName);
    QFile file(filePath);
    if (file.exists(filePath)) {
        if (file.open(QIODevice::ReadOnly)) {
            content = file.readAll();
            modifiedDate = QFileInfo(file).lastModified();
        }
    }
}

DBAccount *dropboxtest::getAccountInfo(bool forceLoad)
{
    m_account = new DBAccount(this);
    if (logged() && forceLoad) {
        m_account->setName("Test User");
        m_account->setEmail(("test.user@theocbase.net"));
    } else {
        m_account->setName("");
        m_account->setEmail("");
    }
    return m_account;
}

QDateTime dropboxtest::upload(QByteArray data, QString relativeCloudFileName)
{
    QString filePath = getLocalFileName(relativeCloudFileName);

    QString dirPath = QFileInfo(filePath).absoluteDir().path();
    qDebug() << "upload" << dirPath;
    if (!QDir(dirPath).exists()) {
        QDir().mkpath(dirPath);
    }

    QFile newDoc(filePath);
    if (newDoc.exists())
        newDoc.remove();

    if (newDoc.open(QIODevice::WriteOnly)) {
        newDoc.write(data);
    }
    newDoc.close();
    QFileInfo info(newDoc);
    return info.lastModified();
}

void dropboxtest::revoke()
{

}

QString dropboxtest::getLocalFileName(QString fileName)
{
     QString path = m_folder + QDir::separator() + fileName;
     return QDir().cleanPath(path);
}
