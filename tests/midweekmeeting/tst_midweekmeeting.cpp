#include <QtTest>
#include "../common/common.h"
#include "lmm_meeting.h"
#include "cpersons.h"

// add necessary includes here

class midweekmeeting : public QObject
{
    Q_OBJECT

public:
    midweekmeeting();
    ~midweekmeeting();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test1_chairman();
    void test2_meetingNotes();
};

midweekmeeting::midweekmeeting()
{
}

midweekmeeting::~midweekmeeting()
{
}

void midweekmeeting::initTestCase()
{
    common::initDataBase();
}

void midweekmeeting::cleanupTestCase()
{
    common::clearDatabase();
}

void midweekmeeting::test1_chairman()
{
    // chairman
    auto *chairman = new person();
    chairman->setGender(person::Male);
    chairman->setFirstname("C");
    chairman->setLastname("Chairman");
    chairman->setUsefor(person::LMM_Chairman);
    cpersons::addPerson(chairman);
    // counselor
    auto *counselor1 = new person();
    counselor1->setGender(person::Male);
    counselor1->setFirstname("C");
    counselor1->setLastname("Counselor1");
    counselor1->setUsefor(person::LMM_Chairman);
    cpersons::addPerson(counselor1);

    QDate d1(2021, 2, 1);
    QDate d2 = d1.addDays(7);
    QDate d3 = d2.addDays(7);

    LMM_Meeting meeting1;
    meeting1.loadMeeting(d1);
    meeting1.setChairman(chairman);
    meeting1.setCounselor3(nullptr);
    meeting1.save();

    LMM_Meeting meeting2;
    meeting2.loadMeeting(d2);
    meeting2.setCounselor2(counselor1);
    meeting2.setCounselor3(chairman);
    meeting2.save();

    LMM_Meeting tstMeeting;

    QSignalSpy spyChairman(&tstMeeting, &LMM_Meeting::chairmanChanged);
    QSignalSpy spyCounselor2(&tstMeeting, &LMM_Meeting::counselor2Changed);
    QSignalSpy spyCounselor3(&tstMeeting, &LMM_Meeting::counselor3Changed);

    // test meeting1
    tstMeeting.loadMeeting(d1);
    QCOMPARE(spyChairman.count(), 1);
    QVERIFY(tstMeeting.chairman() != nullptr);
    QVERIFY(tstMeeting.chairman()->fullname() == chairman->fullname());
    QCOMPARE(spyCounselor2.count(), 1);
    QVERIFY(tstMeeting.counselor2() == nullptr);
    QCOMPARE(spyCounselor3.count(), 1);
    QVERIFY(tstMeeting.counselor3() == nullptr);

    // test meeting2
    tstMeeting.loadMeeting(d2);
    QCOMPARE(spyChairman.count(), 2);
    QVERIFY(tstMeeting.chairman() == nullptr);
    QCOMPARE(spyCounselor2.count(), 2);
    QVERIFY(tstMeeting.counselor2() != nullptr);
    QVERIFY(tstMeeting.counselor2()->fullname() == counselor1->fullname());
    QCOMPARE(spyCounselor3.count(), 2);
    QVERIFY(tstMeeting.counselor3() != nullptr);
    QVERIFY(tstMeeting.counselor3()->fullname() == chairman->fullname());

    // test empty week
    tstMeeting.loadMeeting(d3);
    QCOMPARE(spyChairman.count(), 3);
    QVERIFY(tstMeeting.chairman() == nullptr);
    QCOMPARE(spyCounselor2.count(), 3);
    QVERIFY(tstMeeting.counselor2() == nullptr);
    QCOMPARE(spyCounselor3.count(), 3);
    QVERIFY(tstMeeting.counselor3() == nullptr);
}

void midweekmeeting::test2_meetingNotes()
{
    QDate d1(2021, 2, 1);
    QDate d2 = d1.addDays(7);

    QString meetingNotes = "Zoom=Brother1\nAV=Brother2";

    LMM_Meeting meeting1;
    // save some meeting info
    meeting1.loadMeeting(d1);
    meeting1.setBibleReading("LEVITICUS 26-27");
    meeting1.setChairman(nullptr);
    meeting1.setNotes(meetingNotes);
    meeting1.save();
    meeting1.saveNotes();
    // reload meeting and save notes
    meeting1.loadMeeting(d1);
    meeting1.setNotes(meetingNotes);
    meeting1.saveNotes();

    LMM_Meeting meeting2;
    // load empty meeting and save notes
    meeting2.loadMeeting(d2);
    meeting2.setNotes(meetingNotes);
    meeting2.saveNotes();

    LMM_Meeting tstMeeting;
    tstMeeting.loadMeeting(d1);
    QCOMPARE(tstMeeting.notes(), meetingNotes);
    tstMeeting.loadMeeting(d2);
    QCOMPARE(tstMeeting.notes(), meetingNotes);

    // clear notes
    meeting1.setNotes("");
    meeting1.saveNotes();
    // check that notes are empty
    tstMeeting.loadMeeting(d1);
    QVERIFY(tstMeeting.notes().isEmpty());
}

QTEST_MAIN(midweekmeeting)

#include "tst_midweekmeeting.moc"
