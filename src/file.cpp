#include "file.h"

file::file()
{
}

QString file::Debug(QString contents)
{
    return writeFile(localdatabasedir + "/debug.txt", QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss") + "\t" + contents + "\r\n", true);
}

QString file::Debug(QString fileName, QString contents)
{
    return writeFile(fileName, QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss") + "\t" + contents + "\r\n", true);
}

QString file::writeFile(QString fileName, QString contents, bool okAppend)
{
    QFile outputFile(fileName);
    outputFile.open(okAppend ? QIODevice::WriteOnly | QIODevice::Append : QIODevice::WriteOnly);
    if(!outputFile.isOpen()){
        return outputFile.errorString();
    };
    QTextStream outStream(&outputFile);
    outStream << contents;
    outputFile.close();
    return "";
}

QString file::readFile(QString fileName)
{
    QFile inputFile(fileName);
    inputFile.open(QIODevice::ReadOnly);
    if(!inputFile.isOpen()){
        //return inputFile.errorString();
        return "";
    }
    QTextStream inStream(&inputFile);
    QString contents(inStream.readAll());
    inputFile.close();
    return contents;

}
