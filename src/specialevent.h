/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2022, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPECIALEVENT_H
#define SPECIALEVENT_H

#include <QObject>
#include <QList>
#include <QQmlEngine>
#include <QVector>
#include <QQmlListProperty>
#include <QtQml/qqml.h>

// Expose enum from Qt to QML, and protect enum for proper use in C++
class SpecialEventClass
{
    Q_GADGET
public:
    enum Value {
        None = -1,
        CircuitOverseersVisit = 0,
        Convention = 1,
        Memorial = 2,
        VisitOfHeadquartersRepresentative = 3,
        Other = 4,
        CircuitAssembly = 5,
        VirtualConvention = 6,
        VisitOfBethelSpeaker = 7
    };
    Q_ENUM(Value)
private:
    explicit SpecialEventClass();
};
typedef SpecialEventClass::Value SpecialEvent;

// Expose enum from Qt to QML, and protect enum for proper use in C++
class SpecialEventExclusivityClass
{
    Q_GADGET
public:
    enum Value {
        RegularMeeting = 0,
        NoOtherMeetingInSameWeek = 1,
        NoOtherMeetingInSamePartOfTheWeek = 2,
        AdditionalMeeting = 3
    };
    Q_ENUM(Value)
private:
    explicit SpecialEventExclusivityClass();
};
typedef SpecialEventExclusivityClass::Value SpecialEventExclusivity;

class SpecialEventRule : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SpecialEventClass::Value id READ id CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(QString description READ description CONSTANT)
    Q_PROPERTY(bool canChangeDescription READ canChangeDescription CONSTANT)
    Q_PROPERTY(SpecialEventExclusivityClass::Value exclusivity READ exclusivity CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(Qt::DayOfWeek startDay READ startDay CONSTANT)
    Q_PROPERTY(Qt::DayOfWeek endDay READ endDay CONSTANT)
    Q_PROPERTY(bool isCircuitOverseersVisit READ isCircuitOverseersVisit CONSTANT)
    Q_PROPERTY(bool isConvention READ isConvention CONSTANT)
    Q_PROPERTY(bool isSingleDay READ isSingleDay CONSTANT)
    Q_PROPERTY(bool isWithoutPublicTalk READ isWithoutPublicTalk CONSTANT)
    Q_PROPERTY(bool isWithFinalTalk READ isWithoutPublicTalk CONSTANT)
    Q_PROPERTY(bool isWatchtowerStudyAbbreviated READ isWatchtowerStudyAbbreviated CONSTANT)
    Q_PROPERTY(bool isProgramTransmitted READ isProgramTransmitted CONSTANT)
    //QML_ELEMENT
public:
    SpecialEventRule(QObject *parent = nullptr);

    SpecialEvent id() const;
    void setId(SpecialEvent newId);

    QString description() const;
    void setDescription(const QString &newDescription);

    bool canChangeDescription() const;
    void setCanChangeDescription(bool newCanChangeDescription);

    SpecialEventExclusivity exclusivity() const;
    void setExclusivity(SpecialEventExclusivity newExclusivity);

    Qt::DayOfWeek startDay() const;
    void setStartDay(Qt::DayOfWeek newStartDay);

    Qt::DayOfWeek endDay() const;
    void setEndDay(Qt::DayOfWeek newEndDay);

    bool isCircuitOverseersVisit() const;
    void setIsCircuitOverseersVisit(bool newIsCircuitOverseersVisit);

    bool isConvention() const;
    void setIsConvention(bool newIsConvention);

    bool isSingleDay() const;
    void setIsSingleDay(bool newIsSingleDay);

    bool isWithoutPublicTalk() const;
    void setIsWithoutPublicTalk(bool newIsWithoutPublicTalk);

    bool isWithFinalTalk() const;
    void setIsWithFinalTalk(bool newIsWithFinalTalk);

    bool isWatchtowerStudyAbbreviated() const;
    void setIsWatchtowerStudyAbbreviated(bool newIsWatchtowerStudyAbbreviated);

    bool isProgramTransmitted() const;
    void setIsProgramTransmitted(bool newProgramTransmitted);

private:
    SpecialEvent m_id;
    QString m_description;
    bool m_canChangeDescription;
    SpecialEventExclusivity m_exclusivity;
    Qt::DayOfWeek m_startDay;
    Qt::DayOfWeek m_endDay;
    bool m_isCircuitOverseersVisit;
    bool m_isConvention;
    bool m_isSingleDay;
    bool m_isWithoutPublicTalk;
    bool m_isWithFinalTalk;
    bool m_isWatchtowerStudyAbbreviated;
    bool m_isProgramTransmitted;
};

class SpecialEvents : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<SpecialEventRule> specialEventRules READ specialEventRules CONSTANT)
    Q_DISABLE_COPY(SpecialEvents)
    //QML_ELEMENT
public:
    static SpecialEvents &Instance()
    {
        static SpecialEvents s_instance;
        QQmlEngine::setObjectOwnership(&s_instance, QQmlEngine::CppOwnership);
        return s_instance;
    }
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
    {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        SpecialEvents *s_instance = new SpecialEvents();
        return s_instance;
    }

    QQmlListProperty<SpecialEventRule> specialEventRules();

    int count() const;
    SpecialEventRule *specialEventRule(int) const;
    Q_INVOKABLE SpecialEventRule *findSpecialEventRule(int) const;
    void resetSpecialEventRules();

private:
    SpecialEvents(QObject *parent = nullptr);
    QList<SpecialEventRule *> m_specialEventRules;
};

#endif // SPECIALEVENT_H
