#ifndef WEEKINFO_H
#define WEEKINFO_H

#include <QObject>
#include <QDate>
#include "ccongregation.h"
#include "lmm_schedule.h"
#include "lmm_meeting.h"
#include "cpublictalks.h"

class WeekInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(SpecialEvent specialEvent READ specialEvent WRITE setSpecialEvent NOTIFY specialEventChanged)
    Q_PROPERTY(QString exceptionDisplayText READ exceptionDisplayText NOTIFY exceptionDisplayTextChanged)
    Q_PROPERTY(QString exceptionText READ exceptionText WRITE setExceptionText NOTIFY exceptionTextChanged)
    Q_PROPERTY(QDate exceptionStart READ exceptionStart WRITE setExceptionStart NOTIFY exceptionStartChanged)
    Q_PROPERTY(QDate exceptionEnd READ exceptionEnd WRITE setExceptionEnd NOTIFY exceptionEndChanged)
    Q_PROPERTY(int midweekDay READ midweekDay WRITE setMidweekDay NOTIFY midweekDayChanged)
    Q_PROPERTY(int weekendDay READ weekendDay WRITE setWeekendDay NOTIFY weekendDayChanged)

public:
    WeekInfo(QObject *parent = nullptr);
    QDate date() const;

    QString exceptionText() const;

    SpecialEvent specialEvent() const;

    QDate exceptionStart() const;

    QDate exceptionEnd() const;

    int midweekDay() const;

    int weekendDay() const;

    QString exceptionDisplayText() const;

    Q_INVOKABLE void saveChanges();

    Q_INVOKABLE void load();

public slots:
    void setDate(QDate date);

    void setExceptionText(QString exceptionText);

    void setSpecialEvent(SpecialEvent specialEvent);

    void setExceptionStart(QDate exceptionStart);

    void setExceptionEnd(QDate exceptionEnd);

    void setMidweekDay(int midweekDay);

    void setWeekendDay(int weekendDay);

signals:
    void dateChanged(QDate date);

    void exceptionTextChanged(QString exceptionText);

    void specialEventChanged(SpecialEvent specialEvent);

    void exceptionStartChanged(QDate exceptionStart);

    void exceptionEndChanged(QDate exceptionEnd);

    void midweekDayChanged(int midweekDay);

    void weekendDayChanged(int weekendDay);

    void exceptionDisplayTextChanged(QString exceptionDisplayText);

private:
    ccongregation c;
    void loadCurrentWeek();
    QDate m_date;
    QString m_exceptionText;
    SpecialEvent m_specialEvent;
    QDate m_exceptionStart;
    QDate m_exceptionEnd;
    int m_midweekDay;
    int m_weekendDay;

    QString m_exceptionDisplayText;
};

#endif // WEEKINFO_H
