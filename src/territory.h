/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TERRITORY_H
#define TERRITORY_H

#include <QObject>
#include <QAbstractListModel>
#include <QXmlQuery>
#include <QXmlResultItems>
#include <QtCore>
#include <QGeoCoordinate>
#include "sql_class.h"
#if defined(Q_OS_LINUX)
#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_alg.h"
#elif defined(Q_OS_MAC) || defined(Q_OS_WIN)
#include "ogrsf_frmts.h"
#include "gdal_alg.h"
#endif

class territory : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int territoryId READ territoryId CONSTANT)
    Q_PROPERTY(int territoryNumber READ territoryNumber WRITE setTerritoryNumber NOTIFY notification)
    Q_PROPERTY(QString locality READ locality WRITE setLocality NOTIFY notification)
    Q_PROPERTY(int cityId READ cityId WRITE setCityId NOTIFY notification)
    Q_PROPERTY(int typeId READ typeId WRITE setTypeId NOTIFY notification)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY notification)
    Q_PROPERTY(QString remark READ remark WRITE setRemark NOTIFY notification)
    Q_PROPERTY(QString wktGeometry READ wktGeometry WRITE setWktGeometry NOTIFY notification)
    Q_PROPERTY(bool isDirty READ isDirty WRITE setIsDirty NOTIFY notification)

public:
    explicit territory(QObject *parent = nullptr);
    territory(int territoryId, QString uuid, QObject *parent = nullptr);
    ~territory();

    int territoryId() { return m_territoryId; }

    int territoryNumber() { return m_territoryNumber; }
    void setTerritoryNumber(int value);

    QString locality() { return m_locality; }
    void setLocality(QString value);

    int cityId() { return m_cityId; }
    void setCityId(int value);

    int typeId() { return m_typeId; }
    void setTypeId(int value);

    int priority() { return m_priority; }
    void setPriority(int value);

    QString remark() { return m_remark; }
    void setRemark(QString value);

    QString wktGeometry() { return m_wktGeometry; }
    void setWktGeometry(QString value);
    OGRGeometry *getOGRGeometry();
    bool setOGRGeometry(OGRGeometry *geometry);
    Q_INVOKABLE bool crosses(QVariantList path);

    bool isDirty() { return m_isDirty; }
    void setIsDirty(bool value);

    QString uuid() { return m_uuid; }

    Q_INVOKABLE int getNewNumber();
    Q_INVOKABLE bool loadTerritory(int territoryId);
    Q_INVOKABLE bool remove();
    Q_INVOKABLE bool save();

signals:
    void notification();
    void territoryNumberChanged(int territoryId, int territoryNumber);
    void localityChanged(int territoryId, QString locality);
    void cityChanged(int territoryId, int cityId);
    void typeChanged(int territoryId, int typeId);

public slots:

private:
    int m_territoryId;
    int m_territoryNumber;
    QString m_locality;
    int m_cityId;
    int m_typeId;
    int m_priority;
    QString m_remark;
    QString m_wktGeometry;
    bool m_isDirty;
    QString m_uuid;
};

Q_DECLARE_METATYPE(territory *)

class territory_type : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ territoryTypeId CONSTANT)
    Q_PROPERTY(QString name READ territoryTypeName WRITE setTerritoryTypeName NOTIFY notification)

public:
    territory_type(int id, QString typeName, QObject *parent = nullptr);
    ~territory_type();

    int territoryTypeId() { return m_territoryTypeId; }

    QString territoryTypeName() { return m_territoryTypeName; }
    void setTerritoryTypeName(QString value) { m_territoryTypeName = value; }

signals:
    void notification();

public slots:

private:
    int m_territoryTypeId;
    QString m_territoryTypeName;
};

class DataObject
{
public:
    DataObject(const int &id, const QString &name);
    DataObject(const int &id, const QString &name, const QString &color);
    DataObject(const int &id, const QString &name, const QString &color, const bool isValid);

    int id() const;
    QString name() const;
    QString color() const;
    bool isValid() const;

private:
    int m_id;
    QString m_name;
    QString m_color;
    bool m_isValid;
};

//Q_DECLARE_METATYPE(DataObject*)

class DataObjectListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DataObjectRoles {
        IdRole = Qt::UserRole + 1,
        NameRole,
        ColorRole,
        IsValid
    };

    DataObjectListModel(QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const;

    void addDataObject(const DataObject &dataObject);
    Q_INVOKABLE void addDataObject(const int &id, const QString &name, const QString &color, const bool isValid);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Q_INVOKABLE QVariantMap get(int row);

    Q_INVOKABLE int getId(int index) const;
    Q_INVOKABLE int getIndex(int id) const;
    Q_INVOKABLE int getIndex(QString name) const;

private:
    QList<DataObject> m_dataObjects;
};

//Q_DECLARE_METATYPE(DataObjectListModel*)

#endif // TERRITORY_H
