﻿update lmm_workbookregex set key = 'date1' where key = 'date'
delete from lmm_workbookregex where lang = 'sv'
insert into lmm_workbookregex (lang, key, value) select 'sv', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*–(?<thruday>\d+)\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'sv', 'song', 'Sång\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'sv', 'timing', '[(](?<timingextra>[^[)\d]*)(?<timing>\d+)\smin[.]*[)]'
insert into lmm_workbookregex (lang, key, value) select 'sv', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'en'
insert into lmm_workbookregex (lang, key, value) select 'en', 'date1', '^(?<month>\D+)\s(?<fromday>\d+),*\s*\d*\d*\d*\d*\D*\s*(?<thruday>\d+),*\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'en', 'song', 'Song\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'en', 'timing', '[(](?<timing>\d+)\smin.(?<timingextra>[^)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'en', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'ru'
insert into lmm_workbookregex (lang, key, value) select 'ru', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*—\s*(?<thruday>\d+)\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'ru', 'song', 'Песня\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'ru', 'timing', '[(](?<timing>\d+)\sмин(?<timingextra>[^)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ru', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'hu'
insert into lmm_workbookregex (lang, key, value) select 'hu', 'date1', '^\d*\d*\d*\d*[.]*\s*(?<month>[^\s]+)\s+(?<fromday>\d+)\D+(?<thruday>\d+)'
insert into lmm_workbookregex (lang, key, value) select 'hu', 'song', '(\d+).\sének(.*)'
insert into lmm_workbookregex (lang, key, value) select 'hu', 'timing', '[(](?<timingextra>\D*)(?<timing>\d+)\s*perc\s*[)]'
insert into lmm_workbookregex (lang, key, value) select 'hu', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'pt'
insert into lmm_workbookregex (lang, key, value) select 'pt', 'date1', '^(?<fromday>\d+)\s*d*e*\s*(?<month1>[^\s]*)\s*d*e*\s*\d*\d*\d*\d*[–-]\s*(?<thruday>\d+)[.]*°*\sde\s(?<month2>\D+)\s*d*e*\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'pt', 'song', 'Cântico\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'pt', 'timing', '[(](?<timing>\d+)\smin[.]*(?<timingextra>[^)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'pt', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'el'
insert into lmm_workbookregex (lang, key, value) select 'el', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*[-–](?<thruday>\d+)\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'el', 'song', 'Ύμνος\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'el', 'timing', '[(](?<timing>\d+)\sλεπτά(?<timingextra>[^[)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'el', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'nl'
insert into lmm_workbookregex (lang, key, value) select 'nl', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*[-–](?<thruday>\d+)\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'nl', 'song', 'Lied\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'nl', 'timing', '[(](?<timing>\d+)\smin.(?<timingextra>[^[)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'nl', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'it'
insert into lmm_workbookregex (lang, key, value) select 'it', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*[–-]\s*(?<thruday>\d+)\s*(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'it', 'song', 'Cantico\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'it', 'timing', '[(](?<timing>\d+)\smin(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'it', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'ro'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*[–-]\s*(?<thruday>\d+)\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'song', 'Cântarea\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'timing', '[(](?<timingextra>\D*)(?<timing>\d+)\smin\.*[)]'
insert into lmm_workbookregex (lang, key, value) select 'ro', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'cs'
insert into lmm_workbookregex (lang, key, value) select 'cs', 'date1', '^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*[––]\s*(?<thruday>\d+)[.]\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'cs', 'song', 'Píseň č.\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'cs', 'timing', '[(](?<timing>\d+)\smin[.]*(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'cs', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'ht'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'date1', '^(?<fromday>\d+)\s*(?<month1>[^\s]*)\s*\d*\d*\d*\d*[\s-–]*(?<thruday>\d+)\s*(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'song', 'Kantik\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'timing', '[(](?<timing>\d+)\smin\D+(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ht', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'bg'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'date1', '^(?<fromday>\d+)\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*[–—]\s*(?<thruday>\d+)\s*(?<month2>[\D]+)\s*\d*\d*\d*\d*'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'song', 'Песен\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'timing', '[(](?<timingextra>\D*)\s*(?<timing>\d+)\sмин[)]'
insert into lmm_workbookregex (lang, key, value) select 'bg', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'zh-Hant'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant', 'date1', '^(?<month>\d+\D)(?<fromday>\d+)日*-(?<thruday>\d+)'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant', 'song', '唱詩(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant', 'timing', '[（]\D*(?<timing>\d+)\D*[）]'
insert into lmm_workbookregex (lang, key, value) select 'zh-Hant', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'ja'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'date1', '^\d*\d*\d*\d*年*(?<month>\d+\D)(?<fromday>\d+)日*[‐–]\d*\d*\d*\d*年*(?<thruday>\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'song', '(\d+)番の歌'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'timing', '（(?<timing>\d+)分(?<timingextra>\D*)）'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
update publictalks set active = 1, time_stamp = strftime('%s','now') where theme_name is null or lang_id < 1
delete from lmm_workbookregex where lang = 'af'
insert into lmm_workbookregex (lang, key, value) select 'af', 'date1', '^(?<fromday>\d+)\s*(?<month1>[^–]*)\s*\d*\d*\d*\d*\D*(?<thruday>\d+)\s*(?<month2>\D*)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'af', 'song', 'Lied\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'af', 'timing', '[(](?<timing>\d+)\smin.(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'af', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'sk'
insert into lmm_workbookregex (lang, key, value) select 'sk', 'date1', '^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*–\s*(?<thruday>\d+)[.]\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'sk', 'song', '^Pieseň č.\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'sk', 'timing', '[(](?<timing>\d+)\smin\.*(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'sk', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'sl'
insert into lmm_workbookregex (lang, key, value) select 'sl', 'date1', '^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*\d*\d*\d*\d*–(?<thruday>\d+)[.]\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'sl', 'song', 'Pesem\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'sl', 'timing', '[(](?<timing>\d+)\smin.(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'sl', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
