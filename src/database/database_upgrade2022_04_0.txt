#okNextError Any
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Swahili','Kiswahili (Swahili)','sw',0,1,'47854c7b-c383-4250-b5bb-2f6a0a8497c2')
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Tamil','தமிழ் (Tamil)','ta',0,1,'986615cd-5d8e-4b6e-907d-940c12126a25')
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Tagalog','Tagalog (Tagalog)','tl',0,1,'e10d6f27-ac34-4f4a-a438-049a1d7269a0')
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Xhosa','IsiXhosa (Xhosa)','xh',0,1,'7c1255d2-f3a6-451e-a30c-d4b16dbf861c')
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Punjabi','ਪੰਜਾਬੀ (Punjabi)','pa',0,1,'7264ac66-206f-4eab-82bf-ca0e76b84c10')
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Punjabi, Shahmukhi','پنجابی (شاہ مُکھی) (Punjabi, Shahmukhi)','pa_PK',0,1,'a70a1a62-8347-4ac9-a9c2-6430dafc8405')
INSERT INTO "languages" ("id","language","desc","code","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM languages),'Urdu','اُردو (Urdu)','ur',0,1,'03681b82-9e85-40b5-ad87-066b091e1f1b')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'sw_KE',(SELECT id FROM languages WHERE code = 'sw'),'{0:MMMM dd} - {1:dd, yyyy}','{0:MMMM dd} - {1:MMMM dd, yyyy}','{0:MMMM dd, yyyy} - {1:MMMM dd, yyyy}',0,1,'9bd897ac-7539-4dd6-84d5-d8d0e8602624')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'ta_IN',(SELECT id FROM languages WHERE code = 'ta'),'{0:dd} - {1:dd MMMM yyyy}','{0:dd MMMM} - {1:dd MMMM yyyy}','{0:dd MMMM yyyy} - {1:dd MMMM yyyy}',0,1,'ae5a2026-cdf9-4ca0-b7e3-521e351d026f')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'ta_LK',(SELECT id FROM languages WHERE code = 'ta'),'{0:dd} - {1:dd MMMM yyyy}','{0:dd MMMM} - {1:dd MMMM yyyy}','{0:dd MMMM yyyy} - {1:dd MMMM yyyy}',0,1,'ae5a2026-cdf9-4ca0-b7e3-521e351d026f')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'tl_PH',(SELECT id FROM languages WHERE code = 'tl'),'{0:MMMM dd} - {1:dd, yyyy}','{0:MMMM dd} - {1:MMMM dd, yyyy}','{0:MMMM dd, yyyy} - {1:MMMM dd, yyyy}',0,1,'6978befd-f3a2-4ab6-a747-861e6ce5f661')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'xh_ZA',(SELECT id FROM languages WHERE code = 'xh'),'{0:dd} - {1:dd MMMM yyyy}','{0:dd MMMM} - {1:dd MMMM yyyy}','{0:dd MMMM yyyy} - {1:dd MMMM yyyy}',0,1,'3b9eae34-cccb-4a85-b736-23cc69baedc1')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'pa_IN',(SELECT id FROM languages WHERE code = 'pa'),'{0:dd} - {1:dd MMMM yyyy}','{0:dd MMMM} - {1:dd MMMM yyyy}','{0:dd MMMM yyyy} - {1:dd MMMM yyyy}',0,1,'a9be974c-dfeb-4f31-a211-177ad55bf65e')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'pa_PK',(SELECT id FROM languages WHERE code = 'pa_PK'),'{0:dd} - {1:dd MMMM yyyy}','{0:dd MMMM} - {1:dd MMMM yyyy}','{0:dd MMMM yyyy} - {1:dd MMMM yyyy}',0,1,'19ecbfe6-08e8-4c88-a561-516e1c575263')
INSERT INTO "locales" ("id","code","language_id","samemonth_rangeformat","sameyear_rangeformat","general_rangeformat","time_stamp","active","uuid") VALUES ((SELECT MAX(id)+1 FROM locales),'ur_PK',(SELECT id FROM languages WHERE code = 'ur'),'{0:dd} - {1:dd MMMM, yyyy}','{0:dd MMMM} - {1:dd MMMM, yyyy}','{0:dd MMMM yyyy} - {1:dd MMMM, yyyy}',0,1,'2fe9cd5f-4478-4535-ba0f-038d3299f0a0')

DELETE FROM lmm_workbookregex WHERE lang = 'TL'
insert into lmm_workbookregex (lang, key, value) select 'TL|TG', 'date1', '^(?<month1>[^\s]*)\s*(?<fromday>\d+)\s*[\s-–]*(?<month2>\D+)\s*(?<thruday>\d+)$'
insert into lmm_workbookregex (lang, key, value) select 'TL|TG', 'song', 'Awit\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'TL|TG', 'timing', '[(](?<timing>\d+)\smin[.](?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'TL|TG', 'study', '[(]th\Daralin\D(?<source>\d+)[)]'
insert into lmm_workbookregex (lang, key, value) select 'TL|TG', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'

insert into lmm_workbookregex (lang, key, value) select 'ta|TL', 'date1', '^(?<month1>[^\s]*)\s*(?<fromday>\d+)\s*[\s-–]*(?<month2>\D+)\s*(?<thruday>\d+)$'
insert into lmm_workbookregex (lang, key, value) select 'ta|TL', 'song', 'பாட்டு\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ta|TL', 'timing', '[(](?<timing>\d+)\sநிமி[.](?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ta|TL', 'study', '[(]th.படிப்பு.(?<source>\d+)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ta|TL', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'

DROP VIEW publicmeetinghistory;
CREATE VIEW publicmeetinghistory AS SELECT DISTINCT m.date AS weekof, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') AS mtg_date, m.theme_id AS theme_id, pt.theme_number, pt.theme_name AS theme, m.speaker_id AS speaker_id, m.chairman_id AS chairman_id, m.wtreader_id AS wtreader_id, m.wt_conductor_id, m.wt_source, m.wt_theme, m.final_talk, m.song_pt, m.song_wt_start, m.song_wt_end, m.hospitality_id, m.opening_prayer_id, m.final_prayer_id, m.id FROM publicmeeting m LEFT JOIN publictalks pt ON m.theme_id = pt.id LEFT JOIN congregationmeetingtimes cmt ON cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') AND strftime('%Y', m.date) = cmt.mtg_year;

ALTER TABLE territory_address RENAME TO territory_address_old;
CREATE TABLE territory_address (id INTEGER PRIMARY KEY, territory_id INTEGER, country TEXT, state TEXT, county TEXT, city TEXT, district TEXT, street TEXT, housenumber TEXT, postalcode TEXT, wkt_geometry TEXT, name TEXT, addresstype_number INTEGER, request_date DATE, lang_id INTEGER, time_stamp INTEGER DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
INSERT INTO territory_address (id, territory_id, country, state, county, city, district, street, housenumber, postalcode, wkt_geometry, name, addresstype_number, lang_id, time_stamp, active, uuid) SELECT id, territory_id, country, state, county, city, district, street, housenumber, postalcode, wkt_geometry, name, addresstype_number, lang_id, time_stamp, active, uuid FROM territory_address_old;
DROP TABLE territory_address_old;
DROP VIEW IF EXISTS territoryaddresses;
CREATE VIEW territoryaddresses AS SELECT a.id, a.territory_id, t.territory_number, t.locality, a.country, a.state, a.county, a.city, a.district, a.street, a.housenumber, a.postalcode, a.wkt_geometry, a.name, a.addresstype_number, a_t.addresstype_name, a_t.color, a.request_date, a.lang_id FROM territory_address a LEFT JOIN territory_addresstype a_t ON a.addresstype_number = a_t.addresstype_number AND a_t.lang_id = (SELECT id FROM languages WHERE languages.code = (SELECT value FROM settings WHERE name = "theocbase_language") AND languages.active) LEFT JOIN territory t ON a.territory_id = t.id WHERE a.active;

update persons set usefor = usefor | 2048, time_stamp = strftime('%s','now') where usefor & 512;
