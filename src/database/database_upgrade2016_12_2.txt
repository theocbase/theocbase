﻿DROP TABLE IF EXISTS outgoing_old
DROP TABLE IF EXISTS publicmeeting_old
DROP TABLE IF EXISTS publictalks_old
DROP TABLE IF EXISTS speaker_publictalks_old
INSERT INTO languages (id,language,desc,code, uuid) SELECT (SELECT MAX(id)+1 FROM languages),'Guarani','Guarani (guarani)','gn','5123dd8b-087b-644f-db8f-8603acd1bad4' WHERE NOT EXISTS (SELECT 1 FROM languages WHERE code = 'gn')
delete from lmm_workbookregex where lang = 'de'
insert into lmm_workbookregex (lang, key, value) select 'de', 'date1', '^(?<fromday>\d+)[.]\s*(?<month1>\D*)\s*\d*\d*\d*\d*\s*–\s*(?<thruday>\d+)[.]\s(?<month2>\D+)\s*\d*\d*\d*\d*$'
insert into lmm_workbookregex (lang, key, value) select 'de', 'song', 'Lied\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'de', 'timing', '[(](?<timingextra>[^[)\d]*)(?<timing>\d+)\s[M|m]in.?[)]'
insert into lmm_workbookregex (lang, key, value) select 'de', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'hy'
insert into lmm_workbookregex (lang, key, value) select 'hy', 'date1', '^(?<month1>[^\s]+)\s(?<fromday>\d+)–(?<month2>\D*)(?<thruday>\d+)$'
insert into lmm_workbookregex (lang, key, value) select 'hy', 'song', 'Երգ\s*(\d+)(.*)'
insert into lmm_workbookregex (lang, key, value) select 'hy', 'timing', '[(](?<timing>\d+)\sր(?<timingextra>[^)]*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'hy', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
