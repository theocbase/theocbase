#okNextError No query
ALTER TABLE lmm_schedule ADD COLUMN active BOOLEAN DEFAULT 1
update lmm_schedule set talk_id = case when talk_id = 8 then 10 when talk_id = 9 then 12 when talk_id = 10 then 13 when talk_id = 11 then 14 when talk_id = 12 then 15 when talk_id = 13 then 16 else talk_id end, time_stamp = strftime('%s','now') where (select max(talk_id) from lmm_schedule) < 14
update lmm_schedule set active=0,time_stamp = strftime('%s','now') where id in (select id from (select id,min(talk_id), theme from lmm_schedule group by date,theme having count(theme) > 1) )

DROP VIEW IF EXISTS territoryaddresses;
CREATE VIEW territoryaddresses AS SELECT a.id, a.territory_id, t.territory_number, t.locality, a.country, a.state, a.county, a.city, a.district, a.street, a.housenumber, a.postalcode, a.wkt_geometry, a.name, a.addresstype_number, a_t.addresstype_name, a_t.color, a.lang_id FROM territory_address a LEFT JOIN territory_addresstype a_t ON a.addresstype_number = a_t.addresstype_number AND a_t.lang_id = (SELECT id FROM languages WHERE languages.code = (SELECT value FROM settings WHERE name = "theocbase_language") AND languages.active) AND a_t.active LEFT JOIN territory t ON a.territory_id = t.id WHERE a.active;

INSERT INTO languages (id,language,desc,code, uuid) SELECT (SELECT MAX(id)+1 FROM languages),'Serbian','Srpski (Serbian)','sr','e22428cc-f96c-da96-74a9-39c209ad1000' WHERE NOT EXISTS (SELECT 1 FROM languages WHERE code = 'sr')

INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Name Postfix for en", ": ", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Name Postfix for en");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Prepare This Month’s Presentations", "5", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Prepare This Month’s Presentations");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Initial Call", "6", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Initial Call");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Return Visit", "7", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Return Visit");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Bible Study", "10", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Bible Study");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Talk", "11", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Talk");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Initial Call Video", "5", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Initial Call Video");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: First Return Visit", "7", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: First Return Visit");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: First Return Visit Video", "5", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: First Return Visit Video");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Second Return Visit Video", "5", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Second Return Visit Video");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Second Return Visit", "8", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Second Return Visit");
INSERT INTO settings (name, value, time_stamp, active) SELECT "Talk Type: Third Return Visit", "9", strftime('%s','now'),1 WHERE NOT EXISTS (SELECT 1 FROM settings WHERE name = "Talk Type: Third Return Visit");
