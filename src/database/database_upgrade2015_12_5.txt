﻿delete from lmm_workbookregex where lang = 'ja'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'date', '^(?<month>\d\D)(?<fromday>\d+)日*[‐–](?<thruday>\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'song', '(\d+)番の歌'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'timing', '（(?<timing>\d+)分(?<timingextra>\D*)）'
insert into lmm_workbookregex (lang, key, value) select 'ja', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'pa'
insert into lmm_workbookregex (lang, key, value) select 'pa', 'date', '^(?<fromday>\d+)-(?<thruday>\d+)\s+(?<month>\D+)'
insert into lmm_workbookregex (lang, key, value) select 'pa', 'song', 'ਗੀਤ\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'pa', 'timing', '[(](?<timing>\d+)\sਮਿੰਟ(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'pa', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
delete from lmm_workbookregex where lang = 'ms'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'date', '^(?<fromday>\d+)\s*(?<month1>[^\s-–]*)[\s-–]*(?<thruday>\d+)\s*(?<month2>\D+)$'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'song', 'Lagu\s(\d+)'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'timing', '[(](?<timing>\d+)\smin\D+(?<timingextra>\D*)[)]'
insert into lmm_workbookregex (lang, key, value) select 'ms', 'assignment1', '•(?<theme>[^•]+)•*(?<source>.*)'
UPDATE outgoing SET speaker_id = (SELECT IFNULL((SELECT id FROM persons WHERE uuid = speaker_id),-1) AS id) WHERE LENGTH(speaker_id) = 36
UPDATE outgoing SET congregation_id = (SELECT IFNULL((SELECT id FROM congregations WHERE uuid = congregation_id),-1) AS id) WHERE LENGTH(congregation_id) = 36
#okNextError No query
ALTER TABLE lmm_meeting ADD COLUMN counselor2 INT DEFAULT -1
#okNextError No query
ALTER TABLE lmm_meeting ADD COLUMN counselor3 INT DEFAULT -1
