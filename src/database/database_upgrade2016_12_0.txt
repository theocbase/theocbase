﻿ALTER TABLE speaker_publictalks RENAME TO speaker_publictalks_old;
CREATE TABLE speaker_publictalks (id INTEGER PRIMARY KEY, speaker_id INT, theme_id INT, lang_id INT, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DELETE FROM speaker_publictalks;
INSERT INTO speaker_publictalks (id, speaker_id, theme_id, lang_id, time_stamp, active, uuid) SELECT speaker_publictalks_old.id, speaker_id, publictalks.id, speaker_publictalks_old.lang_id, speaker_publictalks_old.time_stamp, speaker_publictalks_old.active, speaker_publictalks_old.uuid FROM speaker_publictalks_old LEFT JOIN publictalks ON speaker_publictalks_old.theme_number = publictalks.theme_number AND publictalks.lang_id  = (select id from languages where code = (select value from settings where name = 'theocbase_language')) and publictalks.active;

ALTER TABLE outgoing RENAME TO outgoing_old;
CREATE TABLE outgoing(id INTEGER PRIMARY KEY, date NUMERIC, speaker_id NUMERIC, congregation_id NUMERIC, theme_id NUMERIC, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DELETE FROM outgoing;
INSERT INTO outgoing (id, date, speaker_id, congregation_id, theme_id, time_stamp, active, uuid) SELECT outgoing_old.id, date, speaker_id, congregation_id, publictalks.id, outgoing_old.time_stamp, outgoing_old.active, outgoing_old.uuid FROM outgoing_old LEFT JOIN publictalks ON outgoing_old.theme_id = publictalks.theme_number AND publictalks.lang_id  = (select id from languages where code = (select value from settings where name = 'theocbase_language')) and publictalks.active;

ALTER TABLE publictalks RENAME TO publictalks_old;
CREATE TABLE publictalks (id INTEGER PRIMARY KEY, theme_number INT, theme_name TEXT, revision TEXT, release_date DATE, discontinue_date DATE, lang_id INT, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT);
DELETE FROM publictalks;
INSERT INTO publictalks (id , theme_number, theme_name, lang_id, time_stamp , active, uuid) SELECT id , theme_number, theme_name, lang_id, time_stamp , active, uuid FROM publictalks_old;

ALTER TABLE publicmeeting RENAME TO publicmeeting_old;
CREATE TABLE publicmeeting(id INTEGER PRIMARY KEY, date DATE, theme_id INT, speaker_id INT, wtreader_id INT, chairman_id INT, cancelled_id INT, wt_conductor_id INT, wt_source TEXT, song_pt INT, song_wt_start INT, song_wt_end INT, time_stamp INT DEFAULT 0, active BOOLEAN DEFAULT 1, uuid TEXT, wt_theme text, final_talk TEXT);
DELETE FROM publicmeeting;
INSERT INTO publicmeeting (id, date, theme_id, speaker_id, wtreader_id, chairman_id, cancelled_id, wt_conductor_id, wt_source, song_pt, song_wt_start, song_wt_end, time_stamp, active, uuid, wt_theme, final_talk) SELECT publicmeeting_old.id, date, CASE WHEN publictalks.id IS NULL THEN -1 ELSE publictalks.id END, speaker_id, wtreader_id, chairman_id, cancelled_id, wt_conductor_id, wt_source, song_pt, song_wt_start, song_wt_end, publicmeeting_old.time_stamp, publicmeeting_old.active, publicmeeting_old.uuid, wt_theme, final_talk FROM publicmeeting_old  LEFT JOIN publictalks ON publicmeeting_old.theme_number = publictalks.theme_number AND publictalks.lang_id  = (select id from languages where code = (select value from settings where name = 'theocbase_language')) and publictalks.active;

DROP VIEW publicmeetinghistory;
CREATE VIEW publicmeetinghistory AS SELECT DISTINCT m.date AS weekof, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') AS mtg_date, m.theme_id AS theme_id, pt.theme_number, pt.theme_name AS theme, m.speaker_id AS speaker_id, m.chairman_id AS chairman_id, m.wtreader_id AS wtreader_id, m.wt_conductor_id, m.wt_source, m.wt_theme, m.final_talk, m.song_pt, m.song_wt_start, m.song_wt_end, m.id FROM publicmeeting m LEFT JOIN publictalks pt ON m.theme_id = pt.id LEFT JOIN congregationmeetingtimes cmt ON cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') AND strftime('%Y', m.date) = cmt.mtg_year

DELETE FROM speaker_publictalks WHERE speaker_id = -1 OR theme_id = -1 OR lang_id = -1
UPDATE speaker_publictalks SET time_stamp = strftime('%s','now')
UPDATE publicmeeting SET time_stamp = strftime('%s','now')
UPDATE outgoing SET time_stamp = strftime('%s','now')
