#okNextError No query
INSERT INTO languages (id,language, code) SELECT (SELECT MAX(id)+1 FROM languages), 'Czech', 'cs' WHERE NOT EXISTS (SELECT 1 FROM languages WHERE code = 'cs')
#okNextError No query (Fix invalid dates in student_studies table)
UPDATE student_studies SET start_date = date(start_date,'+100 year') WHERE start_date <= date('now','-100 year')
UPDATE student_studies SET end_date = date(end_date,'+100 year') WHERE end_date <= date('now','-100 year') AND end_date NOT NULL
#okNextError
DROP VIEW IF EXISTS schoolhistory
CREATE VIEW IF NOT EXISTS schoolhistory AS SELECT school.student_id, school.assistant_id, school.volunteer_id, school.date, school_schedule.number, school.classnumber FROM school INNER JOIN school_schedule ON school.school_schedule_id = school_schedule.id ORDER BY school.date ASC
