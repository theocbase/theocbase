UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-03-21' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-04-18' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-08-22' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-09-26' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-10-10' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-11-14' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2011-11-28' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-01-30' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-02-13' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-02-20' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-03-05' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-04-02' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-04-09' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-11-12' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2012-12-24' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2014-01-20' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2014-01-27' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2014-10-06' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2014-10-13' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2014-12-01' AND number = 3
UPDATE school_schedule SET onlybrothers = 1 WHERE date = '2014-12-08' AND number = 3
UPDATE school_schedule SET theme = replace(theme,'**','*') WHERE theme LIKE '**%' AND onlybrothers = 1
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "1"),"1",(SELECT study_name FROM studies where study_number = "1"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "2"),"2",(SELECT study_name FROM studies where study_number = "2"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "3"),"3",(SELECT study_name FROM studies where study_number = "3"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "4"),"4",(SELECT study_name FROM studies where study_number = "4"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "5"),"5",(SELECT study_name FROM studies where study_number = "5"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "6"),"6",(SELECT study_name FROM studies where study_number = "6"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "7"),"7",(SELECT study_name FROM studies where study_number = "7"),"1","0","0");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "8"),"8",(SELECT study_name FROM studies where study_number = "8"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "9"),"9",(SELECT study_name FROM studies where study_number = "9"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "10"),"10",(SELECT study_name FROM studies where study_number ="10"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "11"),"11",(SELECT study_name FROM studies where study_number ="11"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "12"),"12",(SELECT study_name FROM studies where study_number ="12"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "13"),"13",(SELECT study_name FROM studies where study_number ="13"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "14"),"14",(SELECT study_name FROM studies where study_number ="14"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "15"),"15",(SELECT study_name FROM studies where study_number ="15"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "16"),"16",(SELECT study_name FROM studies where study_number ="16"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "17"),"17",(SELECT study_name FROM studies where study_number ="17"),"1","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "18"),"18",(SELECT study_name FROM studies where study_number ="18"),"0","1","0");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "19"),"19",(SELECT study_name FROM studies where study_number ="19"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "20"),"20",(SELECT study_name FROM studies where study_number ="20"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "21"),"21",(SELECT study_name FROM studies where study_number ="21"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "22"),"22",(SELECT study_name FROM studies where study_number ="22"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "23"),"23",(SELECT study_name FROM studies where study_number ="23"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "24"),"24",(SELECT study_name FROM studies where study_number ="24"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "25"),"25",(SELECT study_name FROM studies where study_number ="25"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "26"),"26",(SELECT study_name FROM studies where study_number ="26"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "27"),"27",(SELECT study_name FROM studies where study_number ="27"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "28"),"28",(SELECT study_name FROM studies where study_number ="28"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "29"),"29",(SELECT study_name FROM studies where study_number ="29"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "30"),"30",(SELECT study_name FROM studies where study_number ="30"),"0","1","0");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "31"),"31",(SELECT study_name FROM studies where study_number ="31"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "32"),"32",(SELECT study_name FROM studies where study_number ="32"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "33"),"33",(SELECT study_name FROM studies where study_number ="33"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "34"),"34",(SELECT study_name FROM studies where study_number ="34"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "35"),"35",(SELECT study_name FROM studies where study_number ="35"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "36"),"36",(SELECT study_name FROM studies where study_number ="36"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "37"),"37",(SELECT study_name FROM studies where study_number ="37"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "38"),"38",(SELECT study_name FROM studies where study_number ="38"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "39"),"39",(SELECT study_name FROM studies where study_number ="39"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "40"),"40",(SELECT study_name FROM studies where study_number ="40"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "41"),"41",(SELECT study_name FROM studies where study_number ="41"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "42"),"42",(SELECT study_name FROM studies where study_number ="42"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "43"),"43",(SELECT study_name FROM studies where study_number ="43"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "44"),"44",(SELECT study_name FROM studies where study_number ="44"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "45"),"45",(SELECT study_name FROM studies where study_number ="45"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "46"),"46",(SELECT study_name FROM studies where study_number ="46"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "47"),"47",(SELECT study_name FROM studies where study_number ="47"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "48"),"48",(SELECT study_name FROM studies where study_number ="48"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "49"),"49",(SELECT study_name FROM studies where study_number ="49"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "50"),"50",(SELECT study_name FROM studies where study_number ="50"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "51"),"51",(SELECT study_name FROM studies where study_number ="51"),"0","1","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "52"),"52",(SELECT study_name FROM studies where study_number ="52"),"0","0","1");
INSERT OR REPLACE INTO studies (id,study_number, study_name, reading, demonstration, discource) VALUES((SELECT id FROM studies where study_number = "53"),"53",(SELECT study_name FROM studies where study_number ="53"),"0","0","1");
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_default","135|86,18|15,97|15,117|15,23|23,98|23,20|32,80|53,20|40,64|42,86|42,108|42",(SELECT id FROM settings WHERE name = "school_print_slip_default"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_af","148|105,20|20,20|30,100|20,20|40,20|60,20|50,100|30,30|70,20|80,20|90,20|100",(SELECT id FROM settings WHERE name = "school_print_slip_af"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_en","135|86,18|15,97|15,117|15,23|23,98|23,20|32,80|53,20|40,64|42,86|42,108|42",(SELECT id FROM settings WHERE name = "school_print_slip_en"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_fi","148|105,22|23,125|23,115|30,30|30,37|38,25|44,95|68,30|52,36|60,59|60,82|60",(SELECT id FROM settings WHERE name = "school_print_slip_fi"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_hr","148|105,32|21,87|21,115|21,32|29,87|29,20|37,99|65,32|45,87|45,87|50,87|55",(SELECT id FROM settings WHERE name = "school_print_slip_hr"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_sv","148|105,20|19,102|19,120|19,30|28,98|28,19|36,40|70,37|53,81|55,107|55,125|55",(SELECT id FROM settings WHERE name = "school_print_slip_sv"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_fr","147|95,22|25,101|25,119|25,33|33,99|33,22|44,93|63,22|51,64|51,89|51,114|51",(SELECT id FROM settings WHERE name = "school_print_slip_fr"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_et","148|105,22|23,95|23,119|23,25|32,92|32,23|40,42|64,30|48,65|49,82|49,104|49",(SELECT id FROM settings WHERE name = "school_print_slip_et"))
INSERT OR REPLACE INTO settings (name, value,id) VALUES ("school_print_slip_de","148|105,22|23,105|23,123|23,30|32,95|32,25|42,135|64,32|52,77|52,90|52,118|52",(SELECT id FROM settings WHERE name = "school_print_slip_de"))
UPDATE congregations SET meeting1_time=(SELECT value FROM settings WHERE name = 'school_time') WHERE id = (SELECT value FROM settings WHERE name = 'congregation_id') AND (SELECT value FROM settings WHERE name = 'school_time') IS NOT NULL
UPDATE congregations SET meeting2_time=(SELECT value FROM settings WHERE name = 'publicmeeting_time') WHERE id = (SELECT value FROM settings WHERE name = 'congregation_id') AND (SELECT value FROM settings WHERE name = 'publicmeeting_time') IS NOT NULL
DELETE FROM settings WHERE name = 'publicmeeting_time'
DELETE FROM settings WHERE name = 'school_time'
