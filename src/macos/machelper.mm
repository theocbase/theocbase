#include "machelper.h"
#include <Appkit/AppKit.h>
#include <AppKit/NSToolbar.h>
#include <AppKit/NSView.h>
#include <AppKit/NSWindow.h>
#include <QDebug>

void MacHelper::initToolbar(QMacToolBar *toolbar)
{
    qDebug() << "init ok";
    [toolbar->nativeToolbar() setDisplayMode:NSToolbarDisplayModeIconOnly];
}

void MacHelper::colorizeTitleBar(QWindow *qtwindow)
{
    NSView *view = (NSView *)qtwindow->winId();
    NSWindow *window = [view window];
    //window.backgroundColor = [NSColor colorWithCalibratedRed:(35/255.0f) green:(54/255.0f) blue:(94/255.0f) alpha:0.1];
    window.titlebarAppearsTransparent = true;
    window.styleMask |= NSWindowStyleMaskFullSizeContentView;
    window.titleVisibility = NSWindowTitleHidden;
}

void MacHelper::setSystemPalette()
{
    auto NSColorToQColor = [](NSColor *nsColor) {
        QColor qtColor;
        NSString *colorSpaceName = [nsColor colorSpaceName];
        if (colorSpaceName == NSDeviceCMYKColorSpace) {
             CGFloat cyan = 0, magenta = 0, yellow = 0, black = 0, alpha = 0;
             [nsColor getCyan:&cyan magenta:&magenta yellow:&yellow black:&black alpha:&alpha];
             qtColor.setCmykF(cyan, magenta, yellow, black, alpha);
         } else if (colorSpaceName == NSCalibratedRGBColorSpace || colorSpaceName == NSDeviceRGBColorSpace)  {
             CGFloat red = 0, green = 0, blue = 0, alpha = 0;
             [nsColor getRed:&red green:&green blue:&blue alpha:&alpha];
             qtColor.setRgbF(red, green, blue, alpha);
         } else if (colorSpaceName == NSNamedColorSpace) {
             NSColor *tmpColor = [nsColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
             CGFloat red = 0, green = 0, blue = 0, alpha = 0;
             [tmpColor getRed:&red green:&green blue:&blue alpha:&alpha];
             qtColor.setRgbF(red, green, blue, alpha);
         } else {
             NSColorSpace *colorSpace = [nsColor colorSpace];
             if ([colorSpace colorSpaceModel] == NSCMYKColorSpaceModel && [nsColor numberOfComponents] == 5){
                 CGFloat components[5];
                 [nsColor getComponents:components];
                 qtColor.setCmykF(components[0], components[1], components[2], components[3], components[4]);
             } else {
                 NSColor *tmpColor = [nsColor colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
                 CGFloat red = 0, green = 0, blue = 0, alpha = 0;
                 [tmpColor getRed:&red green:&green blue:&blue alpha:&alpha];
                 qtColor.setRgbF(red, green, blue, alpha);
             }
         }
        return qtColor;
    };

    QColor controlColor = NSColorToQColor([NSColor controlColor]);
    QColor controlBackgroundColor = NSColorToQColor([NSColor controlBackgroundColor]);
    QColor buttonTextColor = NSColorToQColor([NSColor controlTextColor]);
    QColor disabledButtonTextColor = NSColorToQColor([NSColor disabledControlTextColor]);
    QColor labelColor = NSColorToQColor([NSColor labelColor]);        
    NSArray<NSColor *> *baseColors = nil;
    if (@available(macOS 10.14, *)) {
        baseColors = [NSColor alternatingContentBackgroundColors];
    } else {
        baseColors = [NSColor controlAlternatingRowBackgroundColors];
    }
    QColor baseColor = NSColorToQColor(baseColors[0]);
    QColor alternateBaseColor = TBStyle::getCompositeColor(baseColor, NSColorToQColor(baseColors[1]));

    QPalette palette = qApp->palette();
    palette.setColor(QPalette::ButtonText, buttonTextColor);
    palette.setColor(QPalette::Disabled, QPalette::ButtonText, disabledButtonTextColor);
    palette.setColor(QPalette::ToolTipBase, controlColor);
    palette.setColor(QPalette::ToolTipText, labelColor);
    palette.setColor(QPalette::Base, controlBackgroundColor);
    palette.setBrush(QPalette::AlternateBase, alternateBaseColor);
    qApp->setPalette(palette);
}
