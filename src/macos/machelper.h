#ifndef MACHELPER_H
#define MACHELPER_H

#include <QObject>
#include <QMacToolBar>
#include <QWindow>
#include <QPalette>
#include <QApplication>
#include "tbstyle.h"

class MacHelper : public QObject
{
    Q_OBJECT

public:
    static void initToolbar(QMacToolBar *toolbar);
    static void colorizeTitleBar(QWindow *qtwindow);
    static void setSystemPalette();
private:
};

#endif // MACHELPER_H
