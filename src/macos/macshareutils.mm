/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "macshareutils.h"

#include <Appkit/AppKit.h>
#include <AppKit/NSView.h>
#include <AppKit/NSWindow.h>

@interface sharingServiceDelegate : NSObject <NSSharingServicePickerDelegate, NSSharingServiceDelegate>
@end

@interface sharingServiceDelegate ()
@property NSSharingService *emailSharingService;
@property NSString *recipientString;
@property NSString *subjectString;
@property NSString *bodyString;
@property NSSharingServicePicker *picker;

@end

@implementation sharingServiceDelegate
- (id)init
{
    self = [super init];
    if (self) {
        //NSSharingService *emailSharingService = [NSSharingService sharingServiceNamed:NSSharingServiceNameComposeEmail];
        //emailSharingService.delegate = self;
        //self.emailSharingService = emailSharingService;
        qDebug() << "init";
    }
    return self;
}

- (void)sharingServicePicker:(NSSharingServicePicker *)sharingServicePicker didChooseSharingService:(NSSharingService *)service
{
    qDebug() << "didChooseSharingService";
    Q_UNUSED(sharingServicePicker);
    Q_UNUSED(service);
}

- (id<NSSharingServiceDelegate>)sharingServicePicker:(NSSharingServicePicker *)sharingServicePicker delegateForSharingService:(NSSharingService *)sharingService
{
    Q_UNUSED(sharingServicePicker);
    qDebug() << "delegateForSharingService";
    [sharingService setRecipients:@[ self.recipientString ]];
    [sharingService setSubject:self.subjectString];

    return self;
}

- (NSArray<NSSharingService *> *)sharingServicePicker:(NSSharingServicePicker *)sharingServicePicker sharingServicesForItems:(NSArray *)items proposedSharingServices:(NSArray<NSSharingService *> *)proposedServices
{
    Q_UNUSED(sharingServicePicker);
    Q_UNUSED(items);
    NSMutableArray *newServices = [NSMutableArray arrayWithObjects:
                                                          [NSSharingService sharingServiceNamed:NSSharingServiceNameComposeEmail],
                                                          [NSSharingService sharingServiceNamed:NSSharingServiceNameComposeMessage], nil];

    for (NSSharingService *sharingService in proposedServices) {
        if ([[sharingService title] isEqualToString:@"Email"] || [[sharingService title] isEqualToString:@"Message"])
            continue;
        [newServices addObject:sharingService];
    }

    return newServices;
}
@end

MacShareUtils::MacShareUtils(QObject *parent)
    : PlatformShareUtils(parent)
{
}

void MacShareUtils::share(const QString shareText, const QString email, QPoint pos)
{
    qDebug() << "MACSHARE" << shareText << email << pos;

    QWindowList windows = qApp->topLevelWindows();
    QWindow *mainWindow = nullptr;
    for (auto w : windows) {
        if (!w->title().isEmpty())
            mainWindow = w;
    }
    if (!mainWindow)
        return;

    NSView *view = (NSView *)mainWindow->winId();

    NSMutableArray *shareItems = [NSMutableArray arrayWithObjects:shareText.toNSString(), nil];

    sharingServiceDelegate *shareDelegate = [[sharingServiceDelegate alloc] init];

    NSSharingServicePicker *sharingServicePicker = [[NSSharingServicePicker alloc] initWithItems:shareItems];
    sharingServicePicker.delegate = shareDelegate;

    shareDelegate.recipientString = email.toNSString();
    shareDelegate.subjectString = shareText.split("\n").first().toNSString();
    shareDelegate.bodyString = shareText.toNSString();

    QRect rect(QPoint(pos.x() - mainWindow->x(), pos.y() - mainWindow->y()), QSize(0, 0));

    [sharingServicePicker showRelativeToRect:rect.toCGRect() ofView:view preferredEdge:NSMinYEdge];
}
