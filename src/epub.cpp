#include "epub.h"

epub::epub(QObject *parent)
    : QObject(parent), curlocal(nullptr)
{
    sql = &Singleton<sql_class>::Instance();
    zipr = &Singleton<zipper>::Instance();
}

epub::~epub()
{
    if (curlocal != nullptr)
        delete curlocal;
}

bool epub::Prepare(QString filename)
{
    // check if filename is in url format
    const QUrl fileUrl(filename);
    if (fileUrl.isLocalFile())
        filename = fileUrl.toLocalFile();
    ePubfilename = filename;

    QRegularExpression re("(w|mwb)_(?<language>.+)?_[0-9]+");
    QRegularExpressionMatch match = re.match(filename);
    if (match.hasMatch()) {
        epubLangCode = match.captured("language");
    }

    if (curlocal != nullptr) {
        delete curlocal;
        curlocal = nullptr;
    }

    extractedPath = zipr->UnZip(filename);
    lastErr = zipr->errMessage;
    return extractedPath.isEmpty();
}

void epub::ImportTOC()
{
    oebpsPath = extractedPath + "/OEBPS";
    xml_reader r(oebpsPath + "/toc.xhtml");
    r.register_elementsearch("a", 0);
    XLM_READER_CONNECT_TO(r, baseXmlPartFound);
    r.read();
}

void epub::ImportCover()
{
    oebpsPath = extractedPath + "/OEBPS";
    xml_reader r(oebpsPath + "/cover.xhtml");
    r.register_elementsearch("img", xmlPartsContexts::coverImage);
    XLM_READER_CONNECT_TO(r, baseXmlPartFound);
    r.read();
}

void epub::SetLocalData(QString href)
{
    xml_reader r(oebpsPath + "/" + href);
    r.register_elementsearch("html", xmlPartsContexts::languageCode);
    XLM_READER_CONNECT_TO(r, baseXmlPartFound);
    r.read();
}

void epub::baseXmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth)
{
    Q_UNUSED(tokenType);
    Q_UNUSED(relativeDepth);

    switch (context) {
    case xmlPartsContexts::tocEntry: {
        QString href(xml->attributes().value("href").toString());
        QString chapter(xml->readElementText(QXmlStreamReader::IncludeChildElements));
        if (curlocal == nullptr)
            SetLocalData(href);

        emit ProcessTOCEntry(href, chapter);
    } break;

    case xmlPartsContexts::languageCode:
        if (curlocal == nullptr) {
            QString xmllang = xml->attributes().value("xml:lang").toString();
            // some new epubs have "meps-lang" to define the language
            QString mepslang = xml->attributes().value("class").toString();
            QRegularExpression re("(ml|meps-lang)-(?<language>.+)?[ ](ms|meps-script)");
            QRegularExpressionMatch match = re.match(mepslang);
            if (match.hasMatch()) {
                mepslang = match.captured("language");
            }
            if (xmllang == "en" && mepslang != "E")
                language = mepslang;
            else
                language = xmllang;

            qDebug() << language;
            //language = xml->attributes().value("xml:lang").toString();
            if (useEpubLangCode)
                language_ex = language + "|" + epubLangCode;
            else
                language_ex = language;
            curlocal = new QLocale(language);

            QString overwriteNumbers(sql->getSetting(language_ex + "_numbers", sql->getSetting(language + "_numbers")));
            if (!overwriteNumbers.isEmpty()) {
                for (int i = 0; i < overwriteNumbers.length(); i++)
                    numbers.insert(overwriteNumbers.mid(i, 1), QString::number(i % 10));
            }
        }
        break;
    case xmlPartsContexts::coverImage: {
        QString src(xml->attributes().value("src").toString());
        QString coverImagePath(oebpsPath + "/" + src);
        const QUrl coverImageUrl(coverImagePath);
        if (coverImageUrl.isLocalFile())
            coverImagePath = coverImageUrl.toLocalFile();
        emit ProcessCoverImage(coverImagePath);
    } break;
    }
}

int epub::stringToInt(QString number)
{
    if (numbers.isEmpty()) {
        return number.toInt();
    } else {
        QString temp;
        for (int i = 0; i < number.length(); i++) {
            temp += numbers[number.mid(i, 1)];
        }
        return temp.toInt();
    }
}
