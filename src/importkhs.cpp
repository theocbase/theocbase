#include "importkhs.h"

importKHS::importKHS(QString directory)
{
    m_directory.setPath(directory);
    sql = &Singleton<sql_class>::Instance();
}

void importKHS::Go()
{
    sql->startTransaction();

    QString lastImport_start = QVariant(QDateTime::currentDateTime().toSecsSinceEpoch()).toString();
    bool imported = false;

    QString filename = m_directory.absolutePath() + m_directory.separator() + "options.DBF";
    DBFFile optionsFile(filename);
    if (optionsFile.header.numRecords > 0) {
        QMap<QString, QVariant> optionsRecord = optionsFile.records[0];
        // language
        QString language = optionsRecord.value("LANGUAGE").toString();
        sql_items l;
        sourceLanguageId = 2;
        l = sql->selectSql("languages", "language", language);
        if (!l.empty()) {
            sourceLanguageId = l[0].value("id").toInt();
        }
        targetLanguageId = sql->getLanguageDefaultId();
        // meeting times
        QLocale khsLocale(l[0].value("code").toString());
        int midweekMeetingDay = optionsRecord.value("TMS_DOW").toInt();
        ccongregation c;
        c.setMidweekMeetingDay(midweekMeetingDay - 1);
        QString lmTimeString = optionsRecord.value("LM_TIME").toString();
        QDateTime lmDateTime = time2QDateTime(&lmTimeString, khsLocale);
        // databases
        imported = importCongregations(lmDateTime.time().toString("hh:mm"));
        imported |= importDayTimes(khsLocale);
        imported |= importNames();
        imported |= importSpeakers();
        imported |= importOutlines();
        imported |= importTalklist();
        imported |= importWESchedule();
        imported |= importOutgoing();
    }

    if (imported) {
        // save new timestamps for the current import
        sql->saveSetting("last_khsimport_start", lastImport_start);
        sql->saveSetting("last_khsimport_end", QVariant(QDateTime::currentDateTime().toSecsSinceEpoch()).toString());
    }

    QMessageBox::information(nullptr, QObject::tr("Import") + " KHS", QObject::tr("Import Complete"));
    //sql->commitTransaction();
}

importKHS::DBFFile::DBFFile(QString fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << fileName;
        return;
    }

    file.seek(0);
    // read header
    file.read(reinterpret_cast<char *>(&header), sizeof(header));
    // read field descriptor
    DBFFieldDescriptor fieldDescriptor;
    while (file.read(reinterpret_cast<char *>(&fieldDescriptor), sizeof(fieldDescriptor))) {
        fields.insert(QVariant(fieldDescriptor.fieldName).toString(), fieldDescriptor);
        char nextChar;
        file.getChar(&nextChar);
        if (nextChar == 0x0D) {
            // jump over 263-byte range, that contains the backlink information,
            // which is the relative path of an associated database (.dbc) file.
            file.seek(file.pos() + 263);
            break;
        } else
            file.seek(file.pos() - 1);
    }
    // read records
    QVarLengthArray<char, 1024> recordData(header.recordSize);
    while (file.read(recordData.data(), header.recordSize)) {
        QMap<QString, QVariant> record = getFieldData(recordData.constData());
        records.append(record);

        char nextChar;
        if (!file.getChar(&nextChar))
            break;
        if (nextChar == 0x1A)
            break;
        else
            file.seek(file.pos() - 1);
    }
    file.close();
}

QMap<QString, QVariant> importKHS::DBFFile::getFieldData(const char *recordData) const
{
    QMap<QString, QVariant> recordMap;
    QMapIterator<QString, DBFFieldDescriptor> i(fields);
    while (i.hasNext()) {
        i.next();
        DBFFieldDescriptor fieldDescriptor = i.value();

        switch (fieldDescriptor.fieldType) {
        case 'C': {
            QVarLengthArray<char, 255> buffer(fieldDescriptor.fieldLength + 1);
            memcpy(buffer.data(), &recordData[fieldDescriptor.fieldOffset], size_t(fieldDescriptor.fieldLength));
            buffer.data()[fieldDescriptor.fieldLength] = '\0';
            recordMap.insert(i.key(), QString(getTextCodec()->toUnicode(buffer.constData())).trimmed());
            break;
        }
        case 'D': {
            QVarLengthArray<char, 9> buffer(fieldDescriptor.fieldLength + 1);
            memcpy(buffer.data(), &recordData[fieldDescriptor.fieldOffset], size_t(fieldDescriptor.fieldLength));
            buffer.data()[fieldDescriptor.fieldLength] = '\0';
            recordMap.insert(i.key(), QDate::fromString(QVariant(buffer.constData()).toString(), "yyyyMMdd"));
            break;
        }
        case 'I': {
            unsigned char buffer[4];
            memcpy(buffer, &recordData[fieldDescriptor.fieldOffset], 4);
            int intValue = buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
            recordMap.insert(i.key(), intValue);
            break;
        }
        case 'L': {
            char logicalValue = recordData[fieldDescriptor.fieldOffset];
            recordMap.insert(i.key(), QString("1TtYy").indexOf(logicalValue) != -1);
            break;
        }
        case 'M':
            qDebug() << "WARNING: DBF Memo field type cannot be imported; field:" << fieldDescriptor.fieldName;
            break;
        case 'N':
            qDebug() << "WARNING: DBF Numeric field type cannot be imported; field:" << fieldDescriptor.fieldName;
            break;
        case 'T':
            qDebug() << "WARNING: DBF Timestamp field type cannot be imported; field:" << fieldDescriptor.fieldName;
            break;
        default:
            qDebug() << "WARNING: DBF" << fieldDescriptor.fieldType << "field type not recognized; field:" << fieldDescriptor.fieldName;
            break;
        }
    }
    return recordMap;
}

QTextCodec *importKHS::DBFFile::getTextCodec() const
{
    switch (header.codePageMark) {
        //    case 0x01:
        //        return QTextCodec::codecForName("U.S. MS-DOS");
        //        break;
        //    case 0x02:
        //        return QTextCodec::codecForName("International MS-DOS");
        //        break;
    case 0x03:
        return QTextCodec::codecForName("Windows-1252"); // Windows ANSI
        break;
        //    case 0x04:
        //        return QTextCodec::codecForName("Standard Macintosh");
        //        break;
        //    case 0x64:
        //        return QTextCodec::codecForName("Eastern European MS-DOS");
        //        break;
        //    case 0x65:
        //        return QTextCodec::codecForName("Russian MS-DOS");
        //        break;
        //    case 0x66:
        //        return QTextCodec::codecForName("Nordic MS-DOS");
        //        break;
        //    case 0x67:
        //        return QTextCodec::codecForName("Icelandic MS-DOS");
        //        break;
        //    case 0x68:
        //        return QTextCodec::codecForName("Kamenicky (Czech) MS-DOS");
        //        break;
        //    case 0x69:
        //        return QTextCodec::codecForName("Mazovia (Polish) MS-DOS");
        //        break;
        //    case 0x6A:
        //        return QTextCodec::codecForName("Greek MS-DOS (437G)");
        //        break;
        //    case 0x6B:
        //        return QTextCodec::codecForName("Turkish MS-DOS");
        //        break;
        //    case 0x78:
        //        return QTextCodec::codecForName("Traditional Chinese (Hong Kong SAR, Taiwan) Windows");
        //        break;
        //    case 0x79:
        //        return QTextCodec::codecForName("Korean Windows");
        //        break;
        //    case 0x7A:
        //        return QTextCodec::codecForName("Chinese Simplified (PRC, Singapore) Windows");
        //        break;
        //    case 0x7B:
        //        return QTextCodec::codecForName("Japanese Windows");
        //        break;
        //    case 0x7C:
        //        return QTextCodec::codecForName("Thai Windows");
        //        break;
        //    case 0x7D:
        //        return QTextCodec::codecForName("Hebrew Windows");
        //        break;
        //    case 0x7E:
        //        return QTextCodec::codecForName("Arabic Windows");
        //        break;
        //    case 0x96:
        //        return QTextCodec::codecForName("Russian Macintosh");
        //        break;
        //    case 0x97:
        //        return QTextCodec::codecForName("Macintosh EE");
        //        break;
        //    case 0x98:
        //        return QTextCodec::codecForName("Greek Macintosh");
        //        break;
    case 0xC8:
        return QTextCodec::codecForName("Windows-1250"); // Eastern European Windows
        break;
    case 0xC9:
        return QTextCodec::codecForName("Windows-1251"); // Russian Windows
        break;
    case 0xCA:
        return QTextCodec::codecForName("Windows-1254"); // Turkish Windows
        break;
    case 0xCB:
        return QTextCodec::codecForName("Windows-1253"); // Greek Windows
        break;
    default:
        return QTextCodec::codecForLocale();
        break;
    }
}

bool importKHS::importCongregations(QString midweekMeetingTime)
{
    bool imported = false;
    // case insensitive search for the congregations.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "congregations.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        ccongregation cc;
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            // import only congregations with valid names; field name must not exceed 10 characters
            QString congregationName = record.value("CONGREGATI").toString();
            if (!congregationName.isEmpty()) {
                ccongregation::congregation c;
                bool isLocalConagregation = record.value("LOCALCONGR").toBool();
                if (isLocalConagregation) {
                    c = cc.getMyCongregation();
                    c.name = congregationName;
                    c.time_meeting1 = midweekMeetingTime;
                } else
                    c = cc.getOrAddCongregation(congregationName);
                int oldId = record.value("ID").toInt();
                congOldIdToNewId.insert(oldId, c.id);
                c.address = record.value("ADDRESS").toString();
                c.circuit = record.value("CIRCUIT").toString();
                c.info = record.value("NOTES").toString();
                //            qDebug() << "coordinator's name and phone number:" << record.value("PO").toString();
                //            qDebug() << "public talk coordinator's name:" << record.value("COORDINATO").toString();
                //            qDebug() << "public talk coordinator's phone (home):" << record.value("EPHONE").toString();
                //            qDebug() << "public talk coordinator's phone (mobile):" << record.value("ECELLPHONE").toString();
                //            qDebug() << "public talk coordinator's e-mail:" << record.value("EMAIL").toString();
                //            qDebug() << "public talk coordinator's address:" << record.value("COORDADD").toString();
                c.save();
                imported = true;
            }
        }
    }
    return imported;
}

bool importKHS::importDayTimes(QLocale locale)
{
    bool imported = false;
    // case insensitive search for the daytimes.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "daytimes.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        ccongregation cc;
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            int congregationId = record.value("ID").toInt();
            ccongregation::congregation c = cc.getCongregationById(congOldIdToNewId.value(congregationId));
            int year = record.value("YEAR").toInt();
            if (year < 1900 || year > 2100)
                year = QDate::currentDate().year();
            auto &mtgtime = c.getPublicmeeting(year);
            QString desc = record.value("DESC").toString();
            QDateTime dateTime = time2QDateTime(&desc, locale);
            mtgtime.setMeetingday(dateTime.date().dayOfWeek());
            mtgtime.setMeetingtime(dateTime.time().toString("hh:mm"));
            c.save();
            imported = true;
        }
    }
    return imported;
}

bool importKHS::importNames()
{
    bool imported = false;
    // case insensitive search for the names.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "names.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];

            cpersons persons;
            person *p = nullptr;

            QString firstName = record.value("FIRSTNAME").toString();
            QString lastName = record.value("LASTNAME").toString();
            QString fullname = firstName + " " + lastName;
            QString formatted = "firstname || ' ' || lastname";
            QString sqlcommand = QString("SELECT id FROM persons WHERE %1 = '%2' ORDER BY active DESC, id DESC").arg(formatted, fullname);
            sql_items ps = sql->selectSql(sqlcommand);
            if (!ps.empty()) {
                p = persons.getPerson(ps[0].value("id").toInt());
            }

            int gender = record.value("GENDER").toInt();
            if (!p) {
                // create new
                p = new person();
                p->setLastname(lastName);
                p->setFirstname(firstName);
                p->setGender(gender == 1 ? person::Male : person::Female);
                persons.addPerson(p);
            }
            int oldId = record.value("ID").toInt();
            namesOldIdToNewId.insert(oldId, p->id());

            p->setCongregationid(sql->getSetting("congregation_id", "1").toInt());
            p->setPhone(record.value("EPHONE").toString());
            p->setMobile(record.value("ECELLPHONE").toString());
            p->setEmail(record.value("EMAIL").toString());
            p->setInfo(record.value("ADDRESS").toString());

            bool isElder = record.value("ELDER").toBool();
            bool isServant = record.value("SERVANT").toBool();
            p->setServant(isElder || isServant);

            bool isSpeaker = record.value("SPEAKER").toBool();
            bool isLMMOverseer = record.value("LM_OVERSEE").toBool();
            // Treasures From God's Word
            bool isLMMChairman = record.value("LM_CHAIRMA").toBool();
            bool isDoingLMMPrayer = record.value("LM_OVERSEE").toBool();
            bool isDoingLMMTreasuresTalk = record.value("LM_TALK").toBool();
            bool isDoingLMMSpiritualGems = record.value("LM_GEMS").toBool();
            bool isLMMAuxAssistant = record.value("LM_AUX_ASS").toBool();
            bool isDoingLMMBibleReading = record.value("LM_BIBLERE").toBool();
            // Apply Yourself to the Field Ministry
            bool isDoingLMMPreparePresentations = record.value("LM_PREPARE").toBool();
            bool isDoingLMMInitialCall = record.value("LM_IC").toBool();
            bool isDoingLMMReturnVisit = record.value("LM_RV").toBool();
            bool isDoingLMMBibleStudy = record.value("LM_BS").toBool();
            bool isDoingLMMApplyTalk = record.value("LM_TALKBYB").toBool();
            bool isDoingLMMAssistant = record.value("LM_ASSISTA").toBool();
            bool isDoingLMMMainHallOnly = record.value("LM_MAIN_ON").toBool();
            bool isDoingLMMAuxHallOnly = record.value("LM_AUX_ONL").toBool();
            bool isDoingLMMOtherVideoPart = record.value("LM_VIDEO").toBool();
            // Living As Christians
            bool isDoingLMMLivingTalks = record.value("LM_PARTS").toBool();
            bool isLMMCBSConductor = record.value("CONDUCTOR").toBool();
            bool isLMMCBSReader = record.value("BSREADER").toBool();
            // Weekend Meeting
            bool isWEChairman = record.value("CHAIRMAN").toBool();
            bool isWTConductor = record.value("WTCONDUCTOR").toBool();
            bool isWTReader = record.value("READER").toBool();
            // Sound
            //        qDebug() << "main hall attendant:" << record.value("ATTENDANT").toString();
            //        qDebug() << "door assistant:" << record.value("DOOR").toString();
            //        qDebug() << "sound:" << record.value("SOUND").toString();
            //        qDebug() << "video assistant:" << record.value("VIDEOASST").toString();
            //        qDebug() << "microphones:" << record.value("MICS").toString();
            //        qDebug() << "platform:" << record.value("STAGE").toString();
            // Public Witnessing
            //        qDebug() << "approved for public witnessing:" << record.value("CART").toString();

            int usefor = p->usefor();
            usefor = isSpeaker ? (usefor | person::PublicTalk | person::Prayer) : (usefor & ~person::PublicTalk);
            usefor = isLMMOverseer ? (usefor | person::LMM_Chairman) : (usefor & ~person::LMM_Chairman);
            usefor = isLMMChairman ? (usefor | person::LMM_Chairman) : (usefor & ~person::LMM_Chairman);
            usefor = isLMMAuxAssistant ? (usefor | person::LMM_Chairman) : (usefor & ~person::LMM_Chairman);
            usefor = isDoingLMMPrayer ? (usefor | person::Prayer) : (usefor & ~person::Prayer);
            usefor = isDoingLMMTreasuresTalk ? (usefor | person::LMM_Treasures) : (usefor & ~person::LMM_Treasures);
            usefor = isDoingLMMSpiritualGems ? (usefor | person::LMM_Digging) : (usefor & ~person::LMM_Digging);
            usefor = isDoingLMMBibleReading ? (usefor | person::LMM_BibleReading) : (usefor & ~person::LMM_BibleReading);
            usefor = isDoingLMMPreparePresentations ? (usefor | person::LMM_PreparePresentations) : (usefor & ~person::LMM_PreparePresentations);
            usefor = isDoingLMMInitialCall ? (usefor | person::LMM_InitialCall) : (usefor & ~person::LMM_InitialCall);
            usefor = isDoingLMMReturnVisit ? (usefor | person::LMM_ReturnVisit) : (usefor & ~person::LMM_ReturnVisit);
            usefor = isDoingLMMBibleStudy ? (usefor | person::LMM_BibleStudy) : (usefor & ~person::LMM_BibleStudy);
            usefor = isDoingLMMApplyTalk ? (usefor | person::LMM_ApplyTalks) : (usefor & ~person::LMM_ApplyTalks);
            usefor = isDoingLMMAssistant ? (usefor | person::Assistant) : (usefor & ~person::Assistant);
            usefor = isDoingLMMMainHallOnly ? (usefor | person::SchoolMain) : (usefor & ~person::SchoolMain);
            usefor = isDoingLMMAuxHallOnly ? (usefor | person::SchoolAux) : (usefor & ~person::SchoolAux);
            usefor = isDoingLMMOtherVideoPart ? (usefor | person::LMM_OtherVideoPart) : (usefor & ~person::LMM_OtherVideoPart);
            usefor = isDoingLMMLivingTalks ? (usefor | person::LMM_LivingTalks) : (usefor & ~person::LMM_LivingTalks);
            usefor = isLMMCBSConductor ? (usefor | person::CBSConductor) : (usefor & ~person::CBSConductor);
            usefor = isLMMCBSReader ? (usefor | person::CBSReader) : (usefor & ~person::CBSReader);
            usefor = isWEChairman ? (usefor | person::Chairman) : (usefor & ~person::Chairman);
            usefor = isWTConductor ? (usefor | person::WtCondoctor) : (usefor & ~person::WtCondoctor);
            usefor = isWTReader ? (usefor | person::WtReader) : (usefor & ~person::WtReader);
            usefor = isWEChairman ? (usefor | person::Prayer) : (usefor & ~person::Prayer);
            p->setUsefor(usefor);

            //        qDebug() << "diceased:" << record.value("DECEASED").toBool();
            //        qDebug() << "moveDate:" << record.value("MOVE_DATE").toDate();

            p->update();
            imported = true;
        }
    }
    return imported;
}

bool importKHS::importSpeakers()
{
    bool imported = false;
    // case insensitive search for the speakers.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "speakers.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            int namesId = record.value("NAMES_ID").toInt();
            cpersons persons;
            person *p = nullptr;
            p = persons.getPerson(namesOldIdToNewId.value(namesId));
            if (!p) {
                QString firstName = record.value("FIRSTNAME").toString();
                QString lastName = record.value("LASTNAME").toString();
                QString fullname = firstName + " " + lastName;
                QString formatted = "firstname || ' ' || lastname";
                QString sqlcommand = QString("SELECT id FROM persons WHERE %1 = '%2' ORDER BY active DESC, id DESC").arg(formatted, fullname);
                sql_items ps = sql->selectSql(sqlcommand);
                if (!ps.empty()) {
                    p = persons.getPerson(ps[0].value("id").toInt());
                }

                if (!p) {
                    // create new
                    p = new person();
                    p->setLastname(lastName);
                    p->setFirstname(firstName);
                    p->setGender(person::Male);
                    persons.addPerson(p);

                    int congregationId = record.value("CONGREGATI").toInt();
                    p->setCongregationid(congOldIdToNewId.value(congregationId));
                }
            }
            int oldId = record.value("SPEAKER_ID").toInt();
            speakersOldIdToNewId.insert(oldId, p->id());
            p->setPhone(record.value("EPHONE").toString());
            p->setMobile(record.value("ECELLPHONE").toString());
            p->setEmail(record.value("EMAIL").toString());
            p->setInfo(record.value("ADDRESS").toString());

            bool isElder = record.value("ELDER").toBool();
            p->setServant(isElder);
            qDebug() << "approved for outgoing talks:" << record.value("APPROVED").toBool();
            int usefor = p->usefor();
            usefor = usefor | person::PublicTalk;
            p->setUsefor(usefor);

            QStringList info = (QStringList()
                                << p->info()
                                << record.value("NOTES").toString());
            info.removeAll("");
            p->setInfo(info.join('\n'));

            p->update();

            imported = true;
        }
    }
    return imported;
}

bool importKHS::importOutlines()
{
    bool imported = false;
    // case insensitive search for the outlines.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "outlines.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            int themeNumber = record.value("OUTLINE").toInt();
            QString themeName;
            QString revision;
            switch (sourceLanguageId) {
            case 5:
                themeName = record.value("TITLE_P").toString();
                revision = record.value("REV_P").toString();
                break;
            case 7:
                themeName = record.value("TITLE_G").toString();
                revision = record.value("REV_G").toString();
                break;
            case 8:
                themeName = record.value("TITLE_F").toString();
                revision = record.value("REV_F").toString();
                break;
            case 9:
                themeName = record.value("TITLE_D").toString();
                revision = record.value("REV_D").toString();
                break;
            case 11:
                themeName = record.value("TITLE_E").toString();
                revision = record.value("REV_E").toString();
                break;
            default:
                themeName = record.value("TITLE").toString();
                revision = record.value("REVISION").toString();
                break;
            }

            sql_item pt;
            pt.insert("theme_number", themeNumber);
            pt.insert("theme_name", themeName);
            pt.insert("revision", revision);
            pt.insert("lang_id", targetLanguageId);

            QDate date = QDate::currentDate();
            sql_items e = sql->selectSql(
                    "SELECT * FROM publictalks WHERE theme_number = '" + QVariant(themeNumber).toString()
                    + "' AND lang_id = " + QVariant(targetLanguageId).toString()
                    + " AND active AND (discontinue_date IS NULL OR discontinue_date='' OR discontinue_date > '"
                    + date.toString(Qt::ISODate)
                    + "') AND (release_date IS NULL OR release_date='' OR release_date <= '"
                    + date.toString(Qt::ISODate) + "') ");
            int themeId = 0;
            if (!e.empty()) {
                // update existing talk
                themeId = e[0].value("id").toInt();
                sql->updateSql("publictalks", "id", QVariant(themeId).toString(), &pt);
            } else {
                // check for discontinued talks
                e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" + QVariant(themeNumber).toString() + "' AND lang_id = '" + QVariant(targetLanguageId).toString() + "' AND active");
                if (!e.empty()) {
                    // discontined talk exists; add current date as release date,
                    // to make sure no concurrent talks exist at the same time
                    pt.insert("release_date", date.toString(Qt::ISODate));
                }
                // insert new talk
                themeId = sql->insertSql("publictalks", &pt, "id");
            }
            outlinesOldIdToNewId.insert(themeNumber, themeId);
            imported = true;
        }
    }
    return imported;
}

bool importKHS::importTalklist()
{
    bool imported = false;
    // case insensitive search for the talklist.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "talklist.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            int speakerOldId = record.value("SPEAKER_ID").toInt();
            int speakerNewId = speakersOldIdToNewId.value(speakerOldId);
            int outlineOldId = record.value("OUTLINE").toInt();
            int outlineNewId = outlinesOldIdToNewId.value(outlineOldId);

            if (outlineNewId > 0) {
                sql_item speakertalk;
                speakertalk.insert(":speaker_id", speakerNewId);
                speakertalk.insert(":lang_id", targetLanguageId);
                speakertalk.insert(":theme_id", outlineNewId);
                QVariant sp_talkID = sql->selectScalar("SELECT id FROM speaker_publictalks WHERE speaker_id = :speaker_id AND lang_id = :lang_id AND theme_id = :theme_id", &speakertalk);
                if (sp_talkID.isNull()) {
                    sql_item s;
                    s.insert("speaker_id", speakerNewId);
                    s.insert("theme_id", outlineNewId);
                    s.insert("lang_id", targetLanguageId);
                    sql->insertSql("speaker_publictalks", &s, "id");
                    imported = true;
                }
            } else {
                qDebug() << "Could not find talk";
            }
        }
    }
    return imported;
}

bool importKHS::importWESchedule()
{
    bool imported = false;
    // case insensitive search for the schedule.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "schedule.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            QDate date = record.value("DATE").toDate();
            QDate firstDayOfWeek = date.addDays((date.dayOfWeek() - 1) * -1);
            cpublictalks cp;
            cptmeeting *ptMeeting = cp.getMeeting(firstDayOfWeek);

            // in - speaker
            int speakerOldId = record.value("SPEAKER_ID").toInt();
            int speakerNewId = speakersOldIdToNewId.value(speakerOldId);
            if (speakerNewId > 0) {
                int outlineOldId = record.value("OUTLINE").toInt();
                int outlineNewId = outlinesOldIdToNewId.value(outlineOldId);
                QVariant themeNumber = sql->selectScalar("select theme_number from publictalks where id = :talkNewID", outlineNewId, 0);
                if (!themeNumber.isNull()) {
                    // get correct talk by taking into consideration the date it was given (due to discontinued talks)
                    ptMeeting->theme = cp.getThemeByNumber(themeNumber.toInt(), firstDayOfWeek);
                    ptMeeting->setSpeaker(cpersons::getPerson(speakerNewId));
                    imported = true;
                }
            }

            // chairman
            int chairmanOldId = record.value("CHAIRMAN").toInt();
            int chairmanNewId = namesOldIdToNewId.value(chairmanOldId);
            if (chairmanNewId > 0) {
                ptMeeting->setChairman(cpersons::getPerson(chairmanNewId));
                imported = true;
            }

            // wt-reader
            int wtReaderOldId = record.value("READER").toInt();
            int wtReaderNewId = namesOldIdToNewId.value(wtReaderOldId);
            if (wtReaderNewId > 0) {
                ptMeeting->setWtReader(cpersons::getPerson(wtReaderNewId));
                imported = true;
            }

            // host
            int hostOldId = record.value("HOST").toInt();
            int hostNewId = namesOldIdToNewId.value(hostOldId);
            if (hostNewId > 0) {
                ptMeeting->setHospitalityhost(cpersons::getPerson(hostNewId));
                imported = true;
            }

            if (imported) {
                ptMeeting->save();
            }
        }

        // remove old data from last import
        if (imported) {
            QString lastImport_start = sql->getSetting("last_khsimport_start", "-1");
            if (lastImport_start != "-1") {
                QString lastImport_end = sql->getSetting("last_khsimport_end", "-1");
                sql_item s;
                s.insert("lastImport_start", lastImport_start);
                s.insert("lastImport_end", lastImport_end);
                sql_items meetings = sql->selectSql("SELECT * FROM publicmeeting WHERE time_stamp >= " + lastImport_start + " AND time_stamp <= " + lastImport_end + " AND active");
                for (unsigned int i = 0; i < meetings.size(); i++) {
                    cpublictalks cp;
                    cptmeeting *ptMeeting = cp.getMeeting(meetings[i].value("date").toDate());
                    ptMeeting->initMeeting();
                    ptMeeting->save();
                }
            }
        }
    }
    return imported;
}

bool importKHS::importOutgoing()
{
    bool imported = false;
    // case insensitive search for the outgoing.DBF-file
    QDirIterator it(m_directory.absolutePath(), { "outgoing.DBF" }, QDir::Files);
    if (it.hasNext()) {
        QFile f(it.next());
        DBFFile dbfFile(it.filePath());
        for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
            QMap<QString, QVariant> record = dbfFile.records[i];
            QDate date = record.value("DATE").toDate();
            QDate firstDayOfWeek = date.addDays((date.dayOfWeek() - 1) * -1);
            cpublictalks cp;
            cptmeeting *temp = cp.getMeeting(firstDayOfWeek);

            // out - speaker
            int speakerOldId = record.value("SPEAKER").toInt();
            int speakerNewId = speakersOldIdToNewId.value(speakerOldId, 0);
            if (speakerNewId > 0) {
                int outlineOldId = record.value("OUTLINE").toInt();
                int outlineNewId = outlinesOldIdToNewId.value(outlineOldId);
                QVariant themeNumber = sql->selectScalar("select theme_number from publictalks where id = :talkNewID", outlineNewId, 0);
                if (!themeNumber.isNull()) {
                    // get correct talk by taking into consideration the date it was given (due to discontinued talks)
                    int themeId = cp.getThemeByNumber(themeNumber.toInt(), firstDayOfWeek).id;
                    int congOldId = record.value("CONGREGATI").toInt();
                    int congNewID = congOldIdToNewId.value(congOldId);

                    // update of existing rows
                    //  - search for date & congregation
                    //    update speaker & theme
                    //  - rows that remain with old ta1ks-import time-stamp
                    //    will be removed later
                    sql_item outgoing;
                    outgoing.insert(":date", firstDayOfWeek.toString(Qt::ISODate));
                    outgoing.insert(":congregation_id", congNewID);
                    QVariant outgoingID = sql->selectScalar("SELECT id FROM outgoing WHERE date = :date AND congregation_id = :congregation_id", &outgoing);

                    if (!outgoingID.isNull()) {
                        sql_item s;
                        s.insert("id", outgoingID);
                        s.insert("date", firstDayOfWeek);
                        s.insert("speaker_id", speakerNewId);
                        s.insert("congregation_id", congNewID);
                        s.insert("theme_id", themeId);
                        s.insert("active", 1);
                        sql->updateSql("outgoing", "id", outgoingID.toString(), &s);

                        imported = true;
                    } else {
                        cpublictalks cp;
                        cptmeeting *currentMeeting = cp.getMeeting(firstDayOfWeek);
                        cpoutgoing *out = cp.addOutgoingSpeaker(firstDayOfWeek, speakerNewId, outlineNewId, congNewID);

                        if (out) {
                            currentMeeting->save();
                            imported = true;
                        }
                    }
                }
            }

            if (imported) {
                temp->save();
            }
        }

        // remove old data from last import
        if (imported) {
            QString lastImport_start = sql->getSetting("last_khsimport_start", "-1");
            if (lastImport_start != "-1") {
                QString lastImport_end = sql->getSetting("last_khsimport_end", "-1");
                sql_item s;
                s.insert("time_stamp", 0);
                s.insert("lastImport_start", lastImport_start);
                s.insert("lastImport_end", lastImport_end);
                sql->execSql("UPDATE outgoing SET active = 0, time_stamp = :time_stamp WHERE time_stamp >= :lastImport_start AND time_stamp <= :lastImport_end", &s, true);
            }
        }
    }
    return imported;
}

QDateTime importKHS::time2QDateTime(QString *time, QLocale locale)
{
    QString timeStr(*time);
    QString timeFmt("hh:mm");
    QRegularExpression re("^((?<day>.+) ){0,1}(?<hour>\\d{1,2}):\\d\\d[ ]{0,1}(?<ap>(([aA]|[pP])[mM]){0,1})");
    QRegularExpressionMatch match = re.match(time);
    if (match.hasMatch()) {
        QString day = match.capturedLength("day") ? "dddd " : "";
        QString hm = match.capturedLength("hour") > 1 ? "hh:mm" : "h:mm";
        QString ap = match.capturedLength("ap") ? " ap" : "";
        timeStr = time->left(match.capturedLength()).trimmed();
        timeFmt = QString("%1%2%3").arg(day).arg(hm).arg(ap);
    }
    return locale.toDateTime(timeStr, timeFmt);
}
