/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "csync.h"
#include <QMap>
#include <QFile>
#include <QMessageBox>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QMapIterator>

csync::csync(QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
}

void csync::CreateXMLFile(QString filename, QDate tempdate, int spinbox,
                          bool publishers, bool speakers,
                          bool mwmeeting, bool publicmeeting, bool outgoing)
{

    QFile file(filename);
    if (QFile::exists(filename))
        QFile::remove(filename);
    generateXML qxml;
    /*open a file */
    if (!file.open(QIODevice::WriteOnly)) {
        /* show wrror message if not able to open file */
        QMessageBox::warning(nullptr, "Read only", "The file is in read only mode");
    } else {
        /*if file is successfully opened then create XML*/
        QXmlStreamWriter *xmlWriter = new QXmlStreamWriter();
        /* set device (here file)to streamwriter */
        xmlWriter->setDevice(&file);
        /* Writes a document start with the XML version number version. */
        xmlWriter->writeStartDocument();
        xmlWriter->writeStartElement("theocbase");

        qxml.writeInfo(xmlWriter, versionnumber);

        xmlWriter->writeStartElement("persons");
        qxml.writePersons(xmlWriter, publishers, speakers);
        xmlWriter->writeEndElement();

        xmlWriter->writeStartElement("families");
        qxml.writeFamilies(xmlWriter);
        xmlWriter->writeEndElement();

        if (publicmeeting) {
            xmlWriter->writeStartElement("public_meetings");
            qxml.writePublicMeetings(xmlWriter, tempdate, spinbox);
            xmlWriter->writeEndElement();
        }

        if (outgoing) {
            xmlWriter->writeStartElement("public_meetings");
            qxml.writeOutgoing(xmlWriter, tempdate, spinbox);
            xmlWriter->writeEndElement();
        }

        if (mwmeeting) {
            xmlWriter->writeStartElement("midweek_meeting");
            qxml.writeMidweekMeeting(xmlWriter, tempdate, spinbox);
            xmlWriter->writeEndElement();
        }

        xmlWriter->writeEndElement(); // </theocbase>
        /*end document */
        xmlWriter->writeEndDocument();
        delete xmlWriter;
    }
}
void csync::readXmlFile(QString filename)
{
    emit progressBarChanged(0);
    //http://www.developer.nokia.com/Community/Wiki/Using_QXmlStreamReader_to_parse_XML_in_Qt
    sql->copyDatabase();

    QList<QHash<QString, QString>> personslist;
    QList<QHash<QString, QString>> familieslist;
    QVector<QPair<QHash<QString, QString>, QVector<QHash<QString, QString>>>> mwlist;
    QVector<QHash<QString, QString>> plist;
    QVector<QList<QString>> elist;
    QVector<QList<QString>> pelist;
    int tempversion = 0;

    QFile *file = new QFile(filename);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox::critical(nullptr, "TheocBase",
                              tr("File reading failed"));
    }

    ccongregation cc;
    defaultcongregation = ccongregation::congregation(); // set to 'none'
    QXmlStreamReader xml(file);

    while (!xml.atEnd() && !xml.hasError()) {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement && xml.name() == "info") {
            while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "info")) {
                xml.readNext();
                if (xml.tokenType() != QXmlStreamReader::EndElement) {
                    QString name = xml.name().toString();
                    int year = 0;
                    if (name == "version") {
                        xml.readNext();
                        qDebug() << "version " + xml.text().toString();
                        tempversion = QVariant(xml.text().toString()).toInt();
                    } else if (name == "default_congregation") {
                        xml.readNext();
                        defaultcongregation = cc.getOrAddCongregation(xml.text().toString());
                    } else if (name.startsWith("meeting")) {
                        year = QVariant(name.right(4)).toInt();
                        name = name.left(name.length() - 5);
                        xml.readNext();
                        if (name == "meeting_day") {
                            // "default_congregation must come before this in the XML file, so that we have a valid object
                            defaultcongregation.getPublicmeeting(year).setMeetingday(QVariant(xml.text().toString()).toInt());
                        } else if (name == "meeting_time") {
                            // "default_congregation must come before this in the XML file, so that we have a valid object
                            defaultcongregation.getPublicmeeting(year).setMeetingtime(xml.text().toString());
                        }
                    }
                }
            }
        }
        if (token == QXmlStreamReader::StartElement && xml.name() == "persons") {
            while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "persons")) {
                xml.readNext();
                if (xml.name() == "person") {
                    QHash<QString, QString> personrow;
                    personrow.reserve(9);
                    //QVector<QString> personrow(8);
                    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "person")) {
                        xml.readNext();
                        if (xml.tokenType() == QXmlStreamReader::StartElement) {
                            /* We've found first name. */
                            if (xml.name() == "firstname") {
                                xml.readNext();
                                personrow.insert("FIRSTNAME", xml.text().toString());
                            } else if (xml.name() == "lastname") {
                                xml.readNext();
                                personrow.insert("LASTNAME", xml.text().toString());
                            } else if (xml.name() == "usefor") {
                                xml.readNext();
                                personrow.insert("USEFOR", xml.text().toString());
                            } else if (xml.name() == "congregation") {
                                xml.readNext();
                                personrow.insert("CONGREGATION", xml.text().toString());
                            } else if (xml.name() == "gender") {
                                xml.readNext();
                                personrow.insert("GENDER", xml.text().toString());
                            } else if (xml.name() == "email") {
                                xml.readNext();
                                personrow.insert("EMAIL", xml.text().toString());
                            } else if (xml.name() == "phone") {
                                xml.readNext();
                                personrow.insert("PHONE", xml.text().toString());
                            } else if (xml.name() == "servant") {
                                xml.readNext();
                                personrow.insert("SERVANT", xml.text().toString());
                            } else if (xml.name() == "uuid") {
                                xml.readNext();
                                personrow.insert("UUID", xml.text().toString());
                            }
                        }
                    }
                    // write to personslist
                    personslist.append(personrow);
                }
            }
        }

        if (token == QXmlStreamReader::StartElement && xml.name() == "families") {
            while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "families")) {
                xml.readNext();
                if (xml.name() == "family") {
                    QHash<QString, QString> familyrow;
                    familyrow.reserve(4);
                    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "family")) {
                        xml.readNext();
                        if (xml.tokenType() == QXmlStreamReader::StartElement) {
                            QString tagname = "";
                            if (xml.name() == "head_firstname") {
                                tagname = "HEAD_FIRSTNAME";
                            } else if (xml.name() == "head_lastname") {
                                tagname = "HEAD_LASTNAME";
                            } else if (xml.name() == "member_firstname") {
                                tagname = "MEMBER_FIRSTNAME";
                            } else if (xml.name() == "member_lastname") {
                                tagname = "MEMBER_LASTNAME";
                            }
                            if (tagname == "")
                                continue;
                            xml.readNext();
                            familyrow.insert(tagname, xml.text().toString());
                        }
                    }
                    familieslist.append(familyrow);
                }
            }
        }

        if (token == QXmlStreamReader::StartElement && xml.name() == "midweek_meeting") {
            while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "midweek_meeting")) {
                xml.readNext();
                if (xml.name() == "qty" && xml.tokenType() != QXmlStreamReader::EndElement) {
                    xml.readNext();
                    qDebug() << "QTY" << xml.text().toString();
                    // TODO: write qty
                } else if (xml.name() == "midweekmeeting") {
                    QPair<QHash<QString, QString>, QVector<QHash<QString, QString>>> mwmeeting;
                    QHash<QString, QString> meeting;
                    QVector<QHash<QString, QString>> talks;
                    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "midweekmeeting")) {
                        xml.readNext();
                        if (xml.name() == "talk") {
                            QHash<QString, QString> talk;
                            while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "talk")) {
                                xml.readNext();
                                if (xml.tokenType() == QXmlStreamReader::StartElement) {
                                    QStringRef key = xml.name();
                                    xml.readNext();
                                    qDebug() << "talk" << key << xml.text().toString();
                                    talk.insert(key.toString(), xml.text().toString());
                                }
                            }
                            talks.append(talk);
                        } else {
                            if (xml.tokenType() == QXmlStreamReader::StartElement) {
                                QStringRef key = xml.name();
                                xml.readNext();
                                qDebug() << key << xml.text().toString();
                                meeting.insert(key.toString(), xml.text().toString());
                            }
                        }
                    }
                    mwmeeting.first = meeting;
                    mwmeeting.second = talks;
                    mwlist.append(mwmeeting);
                }
            }
        }

        if (token == QXmlStreamReader::StartElement && xml.name() == "public_meetings") {
            while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "public_meetings")) {
                xml.readNext();
                if (xml.name() == "public_meeting") {
                    QHash<QString, QString> prow;
                    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "public_meeting")) {
                        xml.readNext();
                        if (xml.tokenType() == QXmlStreamReader::StartElement) {
                            QStringRef key = xml.name();
                            xml.readNext();
                            prow.insert(key.toString(), xml.text().toString());
                        }
                    }
                    plist.append(prow);
                }

                if (xml.name() == "themes") {
                    QList<QString> erow;
                    erow.reserve(3);
                    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "themes")) {
                        xml.readNext();
                        if (xml.tokenType() == QXmlStreamReader::StartElement) {
                            if (xml.name() == "theme_id") {
                                xml.readNext();
                                erow.insert(0, xml.text().toString());
                            } else if (xml.name() == "theme") {
                                xml.readNext();
                                erow.insert(1, xml.text().toString());
                            } else if (xml.name() == "lang_id") {
                                xml.readNext();
                                erow.insert(2, xml.text().toString());
                            }
                        }
                    }
                    elist.append(erow);
                }
                if (xml.name() == "speaker_themes") {
                    QList<QString> perow;
                    perow.reserve(4);
                    while (!(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "speaker_themes")) {
                        xml.readNext();
                        if (xml.tokenType() == QXmlStreamReader::StartElement) {
                            if (xml.name() == "speaker_firstname") {
                                xml.readNext();
                                perow.insert(0, xml.text().toString());
                            } else if (xml.name() == "speaker_lastname") {
                                xml.readNext();
                                perow.insert(1, xml.text().toString());
                            } else if (xml.name() == "theme_id") {
                                xml.readNext();
                                perow.insert(2, xml.text().toString());
                            } else if (xml.name() == "lang_id") {
                                xml.readNext();
                                perow.insert(3, xml.text().toString());
                            }
                        }
                    }
                    pelist.append(perow);
                }
            }

        } //public_meetings lopetus tagi :)
    }
    emit progressBarChanged(20);
    if (xml.hasError()) {
        QMessageBox::critical(nullptr, "TheocBase",
                              "XML Parse Error");
        return;
    }
    xml.clear();

    if (tempversion != versionnumber) {
        QMessageBox::critical(nullptr, "TheocBase",
                              tr("XML file generated in the wrong version.") + QVariant(tempversion).toString() + "-" + QVariant(versionnumber).toString());
        return;
    }

    syncInfo();
    if (personslist.count() > 0)
        syncPersons(personslist);
    if (familieslist.count() > 0)
        syncFamilies(familieslist);
    emit progressBarChanged(40);
    if (mwlist.count() > 0)
        syncMidweekMeeting(mwlist);
    emit progressBarChanged(80);
    syncPublicTalks(elist, pelist, plist);
    emit progressBarChanged(100);

    defaultcongregation = ccongregation::congregation(); // make defaultcongregation = none
}

person *csync::findPerson(QHash<QString, QString> container, QString namePrefix)
{
    QString fname(namePrefix + "_firstname");
    QString lname(namePrefix + "_lastname");
    qDebug() << "findPerson:" << fname;
    if (container.contains(fname) && container.contains(lname)) {
        qDebug() << " " << container[fname] << container[lname];
        return cpersons::getPerson(container[fname] + " " + container[lname]);
    } else
        return nullptr;
}

void csync::syncInfo()
{
    if (defaultcongregation.isValid()) // not 'none'
        defaultcongregation.save();
}

void csync::syncPersons(QList<QHash<QString, QString>> plist)
{
    ccongregation cc;
    cpersons cp;
    int congregationid = cc.getMyCongregation().id;

    QString message = "---Persons---\n";
    QHash<QString, QString> p;
    foreach (p, plist) {
        person *po = cp.getPerson(p["FIRSTNAME"] + " " + p["LASTNAME"]);
        if (!po) {
            // create new
            po = new person(-1, p["UUID"]);
            po->setLastname(p["LASTNAME"]);
            po->setFirstname(p["FIRSTNAME"]);
            po->setGender(p["GENDER"] == "S" ? person::Female : person::Male);
            po->setServant(QVariant(p["SERVANT"]).toBool());
            po->setUsefor(QVariant(p["USEFOR"]).toInt());
            int cid = cc.getCongregationId(p["CONGREGATION"]);
            if (defaultcongregation.name == p["CONGREGATION"])
                cid = congregationid;
            if (cid < 0) {
                cid = cc.addCongregation(p["CONGREGATION"]).id;
            }
            po->setCongregationid(cid);
            po->setEmail(p["EMAIL"]);
            po->setPhone(p["PHONE"]);

            cp.addPerson(po);

            message.append("Lisatty " + p["FIRSTNAME"] + " " + p["LASTNAME"] + "\n");
            emit newReportRow(tr("Persons - added ") + p["FIRSTNAME"] + " " + p["LASTNAME"], Publisher);
        } else {
            // update existing
            // m_ui->checkBoxPublicTalk->setChecked(person::esitelma & p->ohjelmatyypit);
            int usefor = po->usefor();
            int types = QVariant(p["USEFOR"]).toInt();
            if ((types & person::PublicTalk) && !(usefor & person::PublicTalk))
                usefor += person::PublicTalk;
            if ((types & person::Assistant) && !(usefor & person::Assistant))
                usefor += person::Assistant;
            if ((types & person::FieldMinistry) && !(usefor & person::FieldMinistry))
                usefor += person::FieldMinistry;
            if ((types & person::Chairman) && !(usefor & person::Chairman))
                usefor += person::Chairman;
            if ((types & person::WtReader) && !(usefor & person::WtReader))
                usefor += person::WtReader;
            if ((types & person::IsBreak) && !(usefor & person::IsBreak))
                usefor += person::IsBreak;
            if ((types & person::CBSConductor) && !(usefor & person::CBSConductor))
                usefor += person::CBSConductor;
            if ((types & person::CBSReader) && !(usefor & person::CBSReader))
                usefor += person::CBSReader;
            if ((types & person::Prayer) && !(usefor & person::Prayer))
                usefor += person::Prayer;

            if ((types & person::LMM_Chairman) && !(usefor & person::LMM_Chairman))
                usefor += person::LMM_Chairman;
            if ((types & person::LMM_Treasures) && !(usefor & person::LMM_Treasures))
                usefor += person::LMM_Treasures;
            if ((types & person::LMM_Digging) && !(usefor & person::LMM_Digging))
                usefor += person::LMM_Digging;
            if ((types & person::LMM_BibleReading) && !(usefor & person::LMM_BibleReading))
                usefor += person::LMM_BibleReading;
            if ((types & person::LMM_PreparePresentations) && !(usefor & person::LMM_PreparePresentations))
                usefor += person::LMM_PreparePresentations;
            if ((types & person::LMM_InitialCall) && !(usefor & person::LMM_InitialCall))
                usefor += person::LMM_InitialCall;
            if ((types & person::LMM_ReturnVisit) && !(usefor & person::LMM_ReturnVisit))
                usefor += person::LMM_ReturnVisit;
            if ((types & person::LMM_BibleStudy) && !(usefor & person::LMM_BibleStudy))
                usefor += person::LMM_BibleStudy;
            if ((types & person::LMM_LivingTalks) && !(usefor & person::LMM_LivingTalks))
                usefor += person::LMM_LivingTalks;

            if ((types & person::SchoolMain) && !(usefor & person::SchoolMain))
                usefor += person::SchoolMain;
            if ((types & person::SchoolAux) && !(usefor & person::SchoolAux))
                usefor += person::SchoolAux;

            if (po->usefor() != usefor)
                po->setUsefor(usefor);
            if (po->email() != p["EMAIL"])
                po->setEmail(p["EMAIL"]);
            if (po->phone() != p["PHONE"])
                po->setPhone(p["PHONE"]);
            po->update();
            message.append("Updated " + p["FIRSTNAME"] + " " + p["LASTNAME"] + "\n");
            emit newReportRow(tr("Persons - updated ") + p["FIRSTNAME"] + " " + p["LASTNAME"], Publisher);
        }
        delete po;
    }
    //QMessageBox::information(0, "", message);
}

void csync::syncMidweekMeeting(QVector<QPair<QHash<QString, QString>, QVector<QHash<QString, QString>>>> mwlist)
{
    qDebug() << "sync MidweekMeeting";
    QPair<QHash<QString, QString>, QVector<QHash<QString, QString>>> mwmeeting;
    foreach (mwmeeting, mwlist) {
        QHash<QString, QString> meeting(mwmeeting.first);
        QVector<QHash<QString, QString>> talks(mwmeeting.second);
        if (
                !meeting.contains("date") || !meeting.contains("chairman_firstname") || !meeting.contains("chairman_lastname"))
            continue;

        // verify talk_id has been upgraded for 2019
        QHash<QString, QString> talk;
        int maxId(0);
        foreach (talk, talks) {
            if (talk.contains("talk_id")) {
                int dbTalkID(talk["talk_id"].toInt());
                if (dbTalkID > maxId)
                    maxId = dbTalkID;
            }
        }
        if (maxId < 100) {
            foreach (talk, talks) {
                if (talk.contains("talk_id")) {
                    talk["talk_id"] = QVariant(talk["talk_id"].toInt() * 10).toString();
                }
            }
        }

        QDate currentdate = QDate::fromString(meeting["date"], Qt::ISODate);
        if (currentdate.isValid()) {
            LMM_Meeting mtg(nullptr);
            mtg.loadMeeting(currentdate);
            if (meeting.contains("bible_reading"))
                mtg.setBibleReading(meeting["bible_reading"]);
            if (meeting.contains("song_beginning"))
                mtg.setSongBeginning(meeting["song_beginning"].toInt());
            if (meeting.contains("song_middle"))
                mtg.setSongMiddle(meeting["song_middle"].toInt());
            if (meeting.contains("song_end"))
                mtg.setSongEnd(meeting["song_end"].toInt());
            person *p = findPerson(meeting, "chairman");
            mtg.setChairman(p);
            mtg.setPrayerBeginning(findPerson(meeting, "prayer_beginning"));
            mtg.setPrayerEnd(findPerson(meeting, "prayer_end"));
            mtg.setCounselor2(findPerson(meeting, "counselor2"));
            mtg.setCounselor3(findPerson(meeting, "counselor3"));
            mtg.save();

            QHash<QString, QString> talk;
            foreach (talk, talks) {
                if (talk.contains("talk_id")) {
                    int dbTalkID(talk["talk_id"].toInt());
                    MeetingPart meetingPart;
                    int sequence(0);
                    LMM_Schedule::splitDbTalkId(dbTalkID, meetingPart, sequence);
                    LMM_Schedule sch(meetingPart, sequence, currentdate,
                                     talk.contains("theme") ? talk["theme"] : "",
                                     talk.contains("source") ? talk["source"].replace("\r\n", "{br}") : "",
                                     talk.contains("time") ? talk["time"].toInt() : 0);
                    sch.save();

                    AssignmentInfos *assignmentInfos = &AssignmentInfos::Instance();
                    int roomNr = talk.contains("class") && talk["class"].toInt() > 1 ? talk["class"].toInt() : 1;
                    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, false, roomNr);
                    if (!assignmentInfo)
                        continue;

                    LMM_Assignment *a = nullptr;
                    switch (meetingPart) {
                    case MeetingPart::LMM_BibleReading:
                    case MeetingPart::LMM_InitialCall:
                    case MeetingPart::LMM_ReturnVisit:
                    case MeetingPart::LMM_BibleStudy:
                    case MeetingPart::LMM_StudentTalk: {
                        AssignmentInfo *subAssignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, true, roomNr);
                        if (!subAssignmentInfo)
                            continue;
                        LMM_Assignment_ex *a_ex = new LMM_Assignment_ex(assignmentInfo, subAssignmentInfo, sequence, sch.scheduleId(), -1, nullptr);
                        a = a_ex;
                        a_ex->setAssistant(findPerson(talk, "assistant"));
                        a_ex->setVolunteer(findPerson(talk, "volunteer"));
                        if (talk.contains("time"))
                            a_ex->setTiming(talk["time"]);
                        if (talk.contains("setting"))
                            a_ex->setSetting(talk["setting"]);
                        if (talk.contains("completed"))
                            a_ex->setCompleted(QVariant(talk["completed"]).toBool());
                        a_ex->setClassnumber(roomNr);
                    } break;
                    case MeetingPart::LMM_CBS: {
                        AssignmentInfo *subAssignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, true, roomNr);
                        if (!subAssignmentInfo)
                            continue;
                        LMM_Assignment_ex *a_cbs = new LMM_Assignment_ex(assignmentInfo, subAssignmentInfo, sequence, sch.scheduleId(), -1);
                        a = a_cbs;
                        a_cbs->setAssistant(findPerson(talk, "reader"));
                    } break;
                    default:
                        a = new LMM_Assignment(assignmentInfo, sequence, sch.scheduleId(), -1);
                        break;
                    }
                    a->setDate(currentdate);
                    a->setSpeaker(findPerson(talk, "speaker"));
                    if (talk.contains("note"))
                        a->setNote(talk["note"]);
                    a->save();
                    delete (a);
                }
            }
            emit newReportRow("Midweek meeting imported: " + currentdate.toString(), MidweekMeeting);
        }
    }
}

void csync::syncFamilies(QList<QHash<QString, QString>> flist)
{
    qDebug() << "sync families 1";
    QHash<QString, QString> fs;
    foreach (fs, flist) {
        if (fs["MEMBER_LASTNAME"] == "")
            continue;
        person *member = cpersons::getPerson(fs["MEMBER_FIRSTNAME"] + " " + fs["MEMBER_LASTNAME"]);
        if (member) {
            qDebug() << "sync families 2" << member->fullname();
            person *head = cpersons::getPerson(fs["HEAD_FIRSTNAME"] + " " + fs["HEAD_LASTNAME"]);
            if (head) {
                family *hf = family::getOrAddFamily(head->id());
                bool found = false;
                foreach (person *mf, hf->getMembers()) {
                    if (mf->id() == member->id()) {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    hf->addMember(member->id());
            } else {
                family *f = family::getPersonFamily(member->id());
                if (f) {
                    // remove from family
                    f->removeMember(member->id());
                }
            }
        }
    }
}

void csync::syncPublicTalks(QVector<QList<QString>> themelist,
                            QVector<QList<QString>> speakerthemes,
                            QVector<QHash<QString, QString>> meetinglist)
{
    QString message = "---Public Talks---\n";
    int defaultlang = sql->getLanguageDefaultId();
    if (defaultlang == 0) {
        QMessageBox::information(nullptr, "", QObject::tr("Default language not selected!"));
        return;
    }
    cpersons cp;
    int created = 0;
    for (QList<QString> t : themelist) {
        sql_items e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" + t[0] + "' AND lang_id = " + t[2] + " AND active");
        if (e.empty()) {
            // create new
            sql_item sp;
            sp.insert("theme_number", t[0]);
            // now that sql_class::insertSql uses parameters, the apostrophes are not an issue
            sp.insert("theme_name", t[1]);
            sp.insert("lang_id", t[2]);
            sql->insertSql("publictalks", &sp, "id");
            created += 1;
            emit newReportRow(tr("Public talks - theme added ") + QVariant(t[0]).toString(), PublicTalk);
        }
    }
    message.append("Added " + QVariant(created).toString() + " themes" + " \n");

    cpublictalks cpt;
    for (QList<QString> st : speakerthemes) {
        person *speaker = cp.getPerson(st[0] + " " + st[1]);
        if (!speaker)
            continue;

        int speakerId = -1;
        if (speaker)
            speakerId = speaker->id();
        sql_items pe = sql->selectSql("SELECT * FROM speaker_publictalks WHERE speaker_id = " + QVariant(speakerId).toString() + " AND lang_id = " + st[3] + " AND theme_id = (SELECT id FROM publictalks WHERE theme_number = " + st[2] + " AND lang_id = " + st[3] + ") AND active");
        if (pe.empty()) {
            // add new
            sql_item s;
            s.insert("speaker_id", speakerId);
            qDebug() << st[0] << st[1] << st[2] << st[3];
            sql_items t = sql->selectSql("SELECT id FROM publictalks WHERE theme_number = " + st[2] + " AND lang_id = " + st[3]);
            if (t.empty())
                continue;
            s.insert("theme_id", t[0].value("theme_id").toInt());
            s.insert("lang_id", st[3]);
            sql->insertSql("speaker_publictalks", &s, "id");
        }
    }

    for (QHash<QString, QString> m : meetinglist) {
        int sId = -1;
        int cId = -1;
        int rId = -1;
        person *speaker = cp.getPerson(m.value("speaker_firstname") + " " + m.value("speaker_lastname"));
        if (speaker)
            sId = speaker->id();
        person *chairman = cp.getPerson(m.value("chairman_firstname") + " " + m.value("chairman_lastname"));
        if (chairman)
            cId = chairman->id();
        person *reader = cp.getPerson(m.value("reader_firstname") + " " + m.value("reader_lastname"));
        if (reader)
            rId = reader->id();

        cpttheme theme = cpt.getThemeByNumber(m.value("theme_id").toInt());
        sql_item s;
        s.insert("date", m.value("date"));
        s.insert("theme_id", theme.id);
        s.insert("speaker_id", sId);
        s.insert("chairman_id", cId);
        s.insert("wtreader_id", rId);
        s.insert("song_pt", m.value("song_pt", "0").toInt());
        s.insert("song_wt_start", m.value("song_wt_start", "0").toInt());
        s.insert("song_wt_end", m.value("song_wt_end", "0").toInt());
        sql_items y = sql->selectSql("publicmeeting", "date", m.value("date"), "");
        if (!y.empty()) {
            // exists
            sql->updateSql("publicmeeting", "id", y[0].value("id").toString(), &s);
            message.append("Public meeting updated " + m.value("date") + "\n");
            emit newReportRow(tr("Public Talk and WT - schedule updated ") + m.value("date"), WeekendMeeting);
        } else {
            // create a new
            s.insert("date", m.value("date"));
            sql->insertSql("publicmeeting", &s, "id");
            message.append("New publicmeeting created " + m.value("date") + "\n");
            emit newReportRow(tr("Public Talk and WT - schedule added ") + m.value("date"), WeekendMeeting);
        }
    }
    //QMessageBox::information(0,"",message);
}

void csync::readFromTmsWare(QString filename)
{
    QFile file(filename);

    int iRow = 0;
    ccongregation *cc = new ccongregation();
    defaultcongregation = cc->getMyCongregation();
    QList<QHash<QString, QString>> plist;
    QList<QHash<QString, QString>> alist;
    QStringList header;

    if (file.open(QIODevice::ReadOnly)) {
        // Resolve import type by file name
        bool importstudent = filename.contains(QRegularExpression("Students.csv$", QRegularExpression::CaseInsensitiveOption));
        bool importtalkhistory = filename.contains(QRegularExpression("Talk.*History.csv$", QRegularExpression::CaseInsensitiveOption));
        bool importsettinghistory = filename.contains(QRegularExpression("Setting.*History.csv$", QRegularExpression::CaseInsensitiveOption));

        bool error = false;

        QString alldata = file.readAll();
        QStringList rows = alldata.split(QRegularExpression("[\r\n]"));
        foreach (QString line, rows) {
            iRow += 1;
            if (iRow == 1) {
                // read header row
                header = line.toUpper().remove(";").split(",");
                // validate header row of file
                if (importstudent && !validateTmsWareStudentsHeader(header))
                    error = true;
                if (importtalkhistory && !validateTmsWareTalkHistoryHeader(header))
                    error = true;

                if (error) {
                    QMessageBox::information(nullptr, "", QObject::tr("The header row of CSV file is not valid."));
                    break;
                }
                continue;
            }
            if (line.length() < 2)
                continue;

            // some cases there is semi colon at the end of the row
            line = line.remove(QRegularExpression("\";$"));
            // some cases there are quotations at the beginning and end of the row
            if (line.indexOf(QRegularExpression("^\".*(\"$)")) > -1) {
                qDebug() << "remove";
                line = line.remove(QRegularExpression("(^\")|(\"$)"));
            }

            QStringList list = line.split(QRegularExpression("(,)(?=(?:[^\"]|\"[^\"]*\")*$)"));
            QHash<QString, QString> row;
            if (importtalkhistory) {
                row = parseTmsWareTalkHistory(list, header);
                if (!row.isEmpty()) {
                    if (row.contains("ASSISTANT")) {
                        alist.append(row);
                        continue;
                    }
                } else {
                    continue;
                }
            } else if (importstudent) {
                row = parseTmsWareStudents(list, header);
            } else if (importsettinghistory) {
                //
            } else {
                continue;
            }
            plist.append(row);
        }

        file.close();

        if (!error) {
            if (importtalkhistory) {
                qDebug() << "importtalkhistory";
                // check assistant
                QHash<QString, QString> arow;
                foreach (arow, alist) {
                    for (int i = 0; i < plist.size(); i++) {
                        QHash<QString, QString> trow = plist.at(i);
                        if ((arow["DATE"] == trow["DATE"]) && (arow["NUMBER"] == trow["NUMBER"]) && (arow["SCHOOLNUMBER"] == trow["SCHOOLNUMBER"])) {
                            trow.insert("ASSISTANT_FIRSTNAME", arow["STUDENT_FIRSTNAME"]);
                            trow.insert("ASSISTANT_LASTNAME", arow["STUDENT_LASTNAME"]);
                            plist.replace(i, trow);
                            break;
                        }
                    }
                }
                QHash<QString, QString> row;
                foreach (row, plist)
                    qDebug() << row;

                // TODO: provide support for newer versions, e.g. via syncMidweekMeeting
                //syncSchool(plist, sc.getClassesQty());
            } else if (importstudent) {
                qDebug() << "importstudent";
                syncPersons(plist);
            } else if (importsettinghistory) {
            }
        }
    }

    defaultcongregation = ccongregation::congregation(); // set it none 'none'
}

QHash<QString, QString> csync::parseTmsWareStudents(QStringList rowstring, QStringList headerrow)
{
    QHash<QString, QString> row;
    if (rowstring.size() < 6)
        return row;

    QStringList list = rowstring;
    int usefor = 0;
    QStringList useforList = QString(list.at(headerrow.indexOf("USE FOR"))).remove("\"").split(",");
    if (useforList.contains("H"))
        usefor += person::LMM_Treasures | person::LMM_Digging; // formerly Highlights
    if (useforList.contains("1"))
        usefor += person::LMM_BibleReading; // formerly No. 1
    if (useforList.contains("2"))
        usefor += person::LMM_InitialCall | person::LMM_ReturnVisit | person::LMM_BibleStudy; // formerly No. 2
    bool isUsedForNr3;
    if (useforList.contains("3")) {
        isUsedForNr3 = true;
        usefor += person::LMM_InitialCall | person::LMM_ReturnVisit | person::LMM_BibleStudy; // formerly No. 3
    }
    if (useforList.contains("ASST"))
        usefor += person::Assistant;
    if (useforList.contains("C.B.S. Conductor"))
        usefor += person::CBSConductor;
    if (useforList.contains("C.B.S. Prayer"))
        usefor += person::Prayer;
    if (useforList.contains("Read"))
        usefor += person::CBSReader;

    row.insert("FIRSTNAME", list.at(headerrow.indexOf("FIRST NAME")));
    row.insert("LASTNAME", list.at(headerrow.indexOf("LAST NAME")));
    QString gender = "";
    if (QString(list.at(headerrow.indexOf("GENDER"))).toUpper() == "BROTHER") {
        gender = "B";
        if (isUsedForNr3)
            usefor |= person::LMM_ApplyTalks; // formerly No. 3
    } else {
        gender = "S";
    }
    row.insert("GENDER", gender);
    row.insert("USEFOR", QVariant(usefor).toString());
    row.insert("EMAIL", list.at(headerrow.indexOf("EMAIL ADDRESS")));
    row.insert("PHONE", list.at(headerrow.indexOf("PHONE #")));
    QString servant = "";
    if (QString(list.at(headerrow.indexOf("SERVANT"))).toUpper() == "Y") {
        servant = "true";
    } else {
        servant = "false";
    }
    row.insert("SERVANT", servant);
    row.insert("CONGREGATION", defaultcongregation.name);
    return row;
}

QHash<QString, QString> csync::parseTmsWareTalkHistory(QStringList rowstring, QStringList headerrow)
{
    QHash<QString, QString> row;
    if (rowstring.size() < 4)
        return row;

    QStringList name = QString(rowstring.at(headerrow.indexOf("FULL NAME"))).split(" ");
    if (name.size() < 2)
        return row;

    QString talk = rowstring.at(headerrow.indexOf("TALK"));
    QString talknumber;
    if (talk == "H") {
        talknumber = "0";
    } else if (talk.startsWith("1")) {
        talknumber = "1";
    } else if (talk.startsWith("2")) {
        talknumber = "2";
    } else if (talk.startsWith("3")) {
        talknumber = "3";
    } else if (talk.startsWith("ASST")) {
        if (talk.contains(" ")) {
            talknumber = talk.split(" ").at(1).left(1);
            row.insert("ASSISTANT", "TRUE");
        }
    } else {
        // ???
        qDebug() << "wrong talk number";
        return row;
    }

    QString schoolnumber = "1";
    if (talk.endsWith("b", Qt::CaseInsensitive)) {
        schoolnumber = "2";
    } else if (talk.endsWith("c", Qt::CaseInsensitive)) {
        schoolnumber = "3";
    }

    row.insert("NUMBER", talknumber);
    row.insert("SCHOOLNUMBER", schoolnumber);
    row.insert("STUDENT_FIRSTNAME", name.at(0));
    row.insert("STUDENT_LASTNAME", name.at(1));
    QDate date = parseDate(rowstring.at(headerrow.indexOf("TALK DATE")));
    row.insert("DATE", date.toString(Qt::ISODate));
    row.insert("DONE", QVariant(QString(rowstring.at(headerrow.indexOf("RESULT"))).toUpper() == "COMPLETED").toString());
    row.insert("THEME", "");
    row.insert("SOURCE", "");

    return row;
}

QHash<QString, QString> csync::parseTmsWareStudyHistory(QStringList rowstring, QStringList headerrow)
{
    QHash<QString, QString> row;
    if (rowstring.size() < 5)
        return row;

    QStringList name = QString(rowstring.at(headerrow.indexOf("FULL NAME"))).split(" ");
    if (name.size() < 2)
        return row;

    row.insert("STUDENT_FIRSTNAME", name.at(0));
    row.insert("STUDENT_LASTNAME", name.at(1));
    row.insert("STUDY", rowstring.at(headerrow.indexOf("STUDY#")));
    QDate date1 = parseDate(rowstring.at(headerrow.indexOf("DATE ASSIGNED")));
    QDate date2 = parseDate(rowstring.at(headerrow.indexOf("DATE COMPLETED")));

    row.insert("DATE_ASSIGNED", date1.toString(Qt::ISODate));
    if (date2.isValid()) {
        row.insert("DATE_COMPLETED", date2.toString(Qt::ISODate));
    } else {
        row.insert("DATE_COMPLETED", "");
    }
    bool e = QVariant(headerrow.indexOf("EXERCISES COMPLETED")).toBool();
    row.insert("EXERCISES", QVariant(e).toString());

    return row;
}

bool csync::validateTmsWareStudentsHeader(QStringList header)
{
    QStringList validNames;
    validNames.append("USE FOR");
    validNames.append("FIRST NAME");
    validNames.append("LAST NAME");
    validNames.append("GENDER");
    validNames.append("EMAIL ADDRESS");
    validNames.append("PHONE #");
    validNames.append("SERVANT");
    foreach (QString name, validNames) {
        if (!header.contains(name))
            return false;
    }
    return true;
}

bool csync::validateTmsWareTalkHistoryHeader(QStringList header)
{
    QStringList validNames;
    validNames.append("TALK");
    validNames.append("FULL NAME");
    validNames.append("TALK DATE");
    validNames.append("RESULT");
    foreach (QString name, validNames) {
        if (!header.contains(name, Qt::CaseInsensitive))
            return false;
    }
    return true;
}

bool csync::validateTmsWareStudyHistoryHeader(QStringList header)
{
    QStringList validNames;
    validNames.append("FULL NAME");
    validNames.append("STUDY#");
    validNames.append("DATE ASSIGNED");
    validNames.append("DATE COMPLETED");
    validNames.append("EXERCISES COMPLETED");
    foreach (QString name, validNames) {
        if (!header.contains(name, Qt::CaseInsensitive))
            return false;
    }
    return true;
}

QDate csync::parseDate(QString datestring)
{
    QDate date;
    date = QLocale().toDate(datestring, QLocale::ShortFormat);
    if (!date.isValid()) {
        if (datestring.contains("-")) {
            date = QDate::fromString(datestring, Qt::ISODate);
            if (!date.isValid()) {
                date = QDate::fromString(datestring, "d-M-yyyy");
                if (!date.isValid()) {
                    date = QDate::fromString(datestring, "dd-MM-yyyy");
                }
            }
        } else if (datestring.contains(".")) {
            date = QDate::fromString(datestring, "d.M.yyyy");
            if (!date.isValid()) {
                date = QDate::fromString(datestring, "dd.MM.yyyy");
            }
        } else if (datestring.contains("/")) {
            date = QDate::fromString(datestring, "d/M/yyyy");
            if (!date.isValid()) {
                date = QDate::fromString(datestring, "dd/MM/yyyy");
            }
        }
    } else if (date.year() < (QDate::currentDate().year() - 99)) {
        // fix to use correct century
        date = date.addYears(100);
    }

    return date;
}
