#ifndef JWPUB_H
#define JWPUB_H

#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlDriver>
#include <QString>
#include "sql_class.h"
#include "zipper.h"

class jwpub
{
public:
    jwpub();
    ~jwpub();
    bool Prepare(QString filename);

    QString jwPubfilename;
    QString extractedPath;
    QString sqlDb;
    QString lastErr;

protected:
    QTemporaryDir tempdir;
    zipper *zipr;
    sql_class *tbSql;
    QSqlDatabase *jwpubDb;
};

#endif // JWPUB_H
