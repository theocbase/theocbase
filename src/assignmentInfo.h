/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2022, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASSIGNMENTINFO_H
#define ASSIGNMENTINFO_H

#include <QObject>
#include <QQmlEngine>
#include <stdexcept>
#include "person.h"
#include "tbstyle.h"

// Expose enum from Qt to QML, and protect enum for proper use in C++
class MeetingTypeClass
{
    Q_GADGET
public:
    enum class Type {
        None = 0,
        WeekendMeeting = 1,
        MidweekMeeting,
        MeetingForFieldService
    };
    Q_ENUM(Type)

    Q_INVOKABLE static QString toString(Type type)
    {
        switch (type) {
        case Type::None:
            return "";
        case Type::WeekendMeeting:
            return QObject::tr("Weekend Meeting");
        case Type::MidweekMeeting:
            return QObject::tr("Our Christian Life and Ministry Meeting");
        case Type::MeetingForFieldService:
            return QObject::tr("Meeting for Field Service");
        }
        return "";
    }

    Q_INVOKABLE static int toInt(Type type)
    {
        return static_cast<int>(type);
    }

    Q_INVOKABLE static Type fromInt(int typeId)
    {
        return static_cast<Type>(typeId);
    }

private:
    explicit MeetingTypeClass();
};
typedef MeetingTypeClass::Type MeetingType;

class MeetingSectionClass
{
    Q_GADGET
public:
    enum class Section : quint16 {
        None = 0,
        PublicTalk = 1,
        WatchtowerStudy = 2,
        TreasuresFromGodsWord = 3,
        ApplyYourselfToTheFieldMinistry = 4,
        LivingAsChristians = 5
    };
    Q_ENUM(Section)

    Q_INVOKABLE static QString toString(Section section)
    {
        switch (section) {
        case Section::None:
            return "";
        case Section::TreasuresFromGodsWord:
            return QObject::tr("Treasures From God's Word");
        case Section::ApplyYourselfToTheFieldMinistry:
            return QObject::tr("Apply Yourself To The Field Ministry");
        case Section::LivingAsChristians:
            return QObject::tr("Living As Christians");
        case Section::PublicTalk:
            return QObject::tr("Public Talk");
        case Section::WatchtowerStudy:
            return QObject::tr("Watchtower Study");
        }
        return "";
    }

private:
    explicit MeetingSectionClass();
};
typedef MeetingSectionClass::Section MeetingSection;

class MeetingPartClass
{
    Q_GADGET
public:
    // In order to preserve the DB, this value could be 'in memory' only
    // (computed from DB values)... For the moment, LMM_* value are the
    // very same values as in the DB...
    // Note: these enum raw values are used for sorting in history table
    //       None is used for 'invisible' to sort
    enum class Part : qint16 {
        None = 0,
        LMM_Chairman = 1,
        LMM_Treasures = 2,
        LMM_Digging = 3,
        LMM_BibleReading = 4,
        LMM_SampleConversationVideo = 5,
        LMM_InitialCall = 6,
        LMM_ReturnVisit = 7,
        LMM_BibleStudy = 10,
        LMM_StudentTalk = 11,
        LMM_LivingTalk1 = 12,
        LMM_LivingTalk2 = 13,
        LMM_LivingTalk3 = 14,
        LMM_CBS = 15,
        Service_Talk = 16,

        // Public Meeting
        PM_Chairman = 21,
        PublicTalk = 22, // PM_CircuitOverseer = 22 or other number,
        //
        WatchtowerStudy = 23,
        // PublicMeeting_ClosingTalk = 25, // for CircuitOverseer visit?

        LMM_MemorialInvitation = 27,
        LMM_OtherFMVideoPart = 28,
        LMM_Counselor = 29,
        SongAndPrayer = 30
    };
    Q_ENUM(Part)

    Q_INVOKABLE static QString toString(Part part)
    {
        switch (part) {
        case Part::None:
            return "";
        case Part::LMM_Chairman:
            return QObject::tr("Chairman", "Meeting part");
        case Part::LMM_Counselor:
            return QObject::tr("Counselor", "Meeting part");
        case Part::SongAndPrayer:
            return QObject::tr("Song and Prayer", "Meeting part");
        case Part::LMM_Treasures:
            return QObject::tr("Treasures From God’s Word", "Meeting part");
        case Part::LMM_Digging:
            return QObject::tr("Spiritual Gems", "Meeting part");
        case Part::LMM_BibleReading:
            return QObject::tr("Bible Reading", "Meeting part");
        case Part::LMM_SampleConversationVideo:
            return QObject::tr("Sample Conversation Video", "Meeting part");
        case Part::LMM_InitialCall:
            return QObject::tr("Initial Call", "Meeting part");
        case Part::LMM_ReturnVisit:
            return QObject::tr("Return Visit", "Meeting part");
        case Part::LMM_BibleStudy:
            return QObject::tr("Bible Study", "Meeting part");
        case Part::LMM_StudentTalk:
            return QObject::tr("Talk", "Meeting part");
        case Part::LMM_LivingTalk1:
            return QObject::tr("Living as Christians Talk", "Meeting part");
        case Part::LMM_LivingTalk2:
            return QObject::tr("Living as Christians Talk", "Meeting part");
        case Part::LMM_LivingTalk3:
            return QObject::tr("Living as Christians Talk", "Meeting part");
        case Part::LMM_CBS:
            return QObject::tr("Congregation Bible Study", "Meeting part");
        case Part::Service_Talk:
            return QObject::tr("Service Talk", "Meeting part");
        case Part::LMM_MemorialInvitation:
            return QObject::tr("Memorial Invitation", "Meeting part");
        case Part::LMM_OtherFMVideoPart:
            return QObject::tr("Other Video Part", "Meeting part");
        case Part::PM_Chairman:
            return QObject::tr("Chairman", "Meeting part");
        case Part::PublicTalk:
            return QObject::tr("Public Talk", "Meeting part");
        case Part::WatchtowerStudy:
            return QObject::tr("Watchtower Study", "Meeting part");
        }
        return "";
    }

    Q_INVOKABLE static int toTalkId(Part part)
    {
        return static_cast<int>(part);
    }

    Q_INVOKABLE static int toDbTalkId(Part part, int sequence = 0)
    {
        return static_cast<int>(part) * 10 + sequence;
    }

    Q_INVOKABLE static Part fromInt(int talkId)
    {
        return static_cast<Part>(talkId);
    }

private:
    explicit MeetingPartClass();
};
typedef MeetingPartClass::Part MeetingPart;

// Expose enum from Qt to QML, and protect enum for proper use in C++
class AssignmentCategoryClass
{
    Q_GADGET
public:
    enum class Category : quint8 {
        None = 0,
        NonstudentMeetingPart = 1,
        OtherAssignment,
        StudentPart,
        StudentPartAssistant,
        SupportTask // e.g.: hospitality, attendant, A/V support
    };
    Q_ENUM(Category)

    static QString toString(Category category)
    {
        switch (category) {
        case Category::None:
            return "";
        case Category::NonstudentMeetingPart:
            return QObject::tr("Meeting parts", "Assignment category");
        case Category::OtherAssignment:
            return QObject::tr("Other assignments", "Assignment category");
        case Category::StudentPart:
            return QObject::tr("Student parts", "Assignment category");
        case Category::StudentPartAssistant:
            return QObject::tr("Assistant in student parts", "Assignment category");
        case Category::SupportTask:
            return QObject::tr("Support tasks", "Assignment category");
        }
        return "";
    }

private:
    explicit AssignmentCategoryClass();
};
typedef AssignmentCategoryClass::Category AssignmentCategory;

// Expose enum from Qt to QML, and protect enum for proper use in C++
class AssignmentTypeClass
{
    Q_GADGET
public:
    enum Type : qint16 {
        None = 0, // for filtering
        Chairman = 1,
        Counselor,
        StudyConductor,
        Talk,
        Discussion, // TODO: Distinguish between talk, discussion, or viewing a video; maybe based on texts like: 'Local Needs', 'Talk', 'Discussion', 'Organizational Accomplishments'...
        Reader,
        Demonstration,
        Assistant,
        Prayer,
        Hospitality
    };
    Q_ENUM(Type)

    static QString toString(Type type)
    {
        switch (type) {
        case Type::None:
            return "";
        case Type::Chairman:
            return QObject::tr("Chairman", "Assignment type");
        case Type::Counselor:
            return QObject::tr("Counselor", "Assignment type");
        case Type::StudyConductor:
            return QObject::tr("Study conductor", "Assignment type");
        case Type::Talk:
            return QObject::tr("Talk", "Assignment type");
        case Type::Discussion:
            return QObject::tr("Discussion", "Assignment type");
        case Type::Demonstration:
            return QObject::tr("Demonstration", "Assignment type");
        case Type::Assistant:
            return QObject::tr("Assistant", "Assignment type");
        case Type::Reader:
            return QObject::tr("Reader", "Assignment type");
        case Type::Prayer:
            return QObject::tr("Prayer", "Assignment type");
        case Type::Hospitality:
            return QObject::tr("Hospitality", "Assignment type");
        }
        return "";
    }

private:
    explicit AssignmentTypeClass();
};
typedef AssignmentTypeClass::Type AssignmentType;

// Expose enum from Qt to QML, and protect enum for proper use in C++
class AssignmentSubtypeClass
{
    Q_GADGET
public:
    enum Subtype : quint8 {
        None,
        OpeningPrayer,
        ConcludingPrayer,
        LMMChairman,
        PMChairman
    };
    Q_ENUM(Subtype)

    static QString toString(Subtype subtype)
    {
        switch (subtype) {
        case Subtype::None:
            return "";
        case Subtype::OpeningPrayer:
            return QObject::tr("Opening prayer");
        case Subtype::ConcludingPrayer:
            return QObject::tr("Concluding prayer");
        case Subtype::LMMChairman:
            return QObject::tr("Life and Ministry Meeting Chairman");
        case Subtype::PMChairman:
            return QObject::tr("Public Meeting Chairman");
        }
        return "";
    }

private:
    explicit AssignmentSubtypeClass();
};
typedef AssignmentSubtypeClass::Subtype AssignmentSubtype;

class AssignmentInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MeetingTypeClass::Type meetingType READ meetingType CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(MeetingSectionClass::Section meetingSection READ meetingSection CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(QString meetingSectionName READ meetingSectionName CONSTANT)
    Q_PROPERTY(QColor meetingSectionColor READ meetingSectionColor CONSTANT)
    Q_PROPERTY(QColor meetingSectionTextColor READ meetingSectionTextColor CONSTANT)
    Q_PROPERTY(MeetingPartClass::Part meetingPart READ meetingPart CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(QString meetingPartName READ meetingPartName CONSTANT)
    Q_PROPERTY(AssignmentCategoryClass::Category assignmentCategory READ assignmentCategory CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(AssignmentTypeClass::Type assignmentType READ assignmentType CONSTANT) // use fully qualified type name for QML
    Q_PROPERTY(QString assignmentTypeName READ assignmentTypeName CONSTANT)
    Q_PROPERTY(AssignmentSubtypeClass::Subtype assignmentSubtype READ assignmentSubtype CONSTANT)
    Q_PROPERTY(person::UseFor role READ role CONSTANT)
    Q_PROPERTY(int roomNumber READ roomNumber CONSTANT)
    Q_PROPERTY(bool canCounsel READ canCounsel CONSTANT)
    Q_PROPERTY(bool canHaveAssistant READ canHaveAssistant CONSTANT)
    Q_PROPERTY(bool isSupplementary READ isSupplementary CONSTANT)

public:
    AssignmentInfo(QObject *parent = nullptr);
    AssignmentInfo(MeetingType meetingType, MeetingSection meetingSection, MeetingPart meetingPart, AssignmentCategory assignmentCategory, AssignmentType assignmentType, AssignmentSubtype assignmentSubtype, person::UseFor role, int roomNumber, QObject *parent = nullptr);

    MeetingType meetingType() const;
    MeetingSection meetingSection() const;
    QString meetingSectionName() const;
    QColor meetingSectionColor() const;
    QColor meetingSectionTextColor() const;
    MeetingPart meetingPart() const;
    QString meetingPartName() const;
    AssignmentCategory assignmentCategory() const;
    AssignmentType assignmentType() const;
    QString assignmentTypeName() const;
    AssignmentSubtype assignmentSubtype() const;
    person::UseFor role() const;
    int roomNumber() const;
    bool canCounsel() const;
    bool canHaveAssistant() const;
    bool isSupplementary() const;

private:
    MeetingType m_meetingType;
    MeetingSection m_meetingSection;
    MeetingPart m_meetingPart;
    AssignmentCategory m_assignmentCategory;
    AssignmentType m_assignmentType;
    AssignmentSubtype m_assignmentSubtype;
    person::UseFor m_role;
    int m_roomNumber;
};

class AssignmentInfos : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(AssignmentInfos)
public:
    static AssignmentInfos &Instance()
    {
        static AssignmentInfos s_instance;
        QQmlEngine::setObjectOwnership(&s_instance, QQmlEngine::CppOwnership);
        return s_instance;
    }
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
    {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        AssignmentInfos *s_instance = new AssignmentInfos();
        return s_instance;
    }

    Q_INVOKABLE AssignmentInfo *findAssignmentInfo(MeetingType meetingType, MeetingPart meetingPart, bool isSupplementary = false, int roomNumber = 1, AssignmentSubtype assignmentSubtype = AssignmentSubtype::None) const;
    Q_INVOKABLE AssignmentInfo *findAssignmentInfo(int talkId, bool isSupplementary = false, int roomNumber = 1, AssignmentSubtype assignmentSubtype = AssignmentSubtype::None) const;
    QList<MeetingPart> getMeetingParts(QList<MeetingType> meetingTypes, QList<AssignmentCategory> assignmntCategories, AssignmentType assignmentType = AssignmentType::None) const;

private:
    AssignmentInfos(QObject *parent = Q_NULLPTR);
    void initAssignmentInfos();
    QList<AssignmentInfo *> m_assignmentInfos;
};

class UnknownAssignmentInfoIException : public std::invalid_argument
{
public:
    UnknownAssignmentInfoIException(MeetingType meetingType, int roomNumber, MeetingPart meetingPart, bool isSupplementary);
};

class UnknownTalkIException : public std::invalid_argument
{
public:
    UnknownTalkIException(int talkId);
    UnknownTalkIException(int talkId, QDate date);
};

#endif // ASSIGNMENTINFO_H
