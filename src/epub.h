#ifndef EPUB_H
#define EPUB_H

#include <QObject>
#include <QString>
#include <QXmlStreamReader>
#include "xml_reader.h"
#include "sql_class.h"
#include "zipper.h"

class epub : public QObject
{
    Q_OBJECT

public:
    explicit epub(QObject *parent = nullptr);
    ~epub();
    bool Prepare(QString filename);
    void ImportTOC();
    void ImportCover();
    int stringToInt(QString number);

    QString lastErr;
    QLocale *curlocal;
    QString language;
    QString epubLangCode;
    QString language_ex;
    bool useEpubLangCode {true};

    QString ePubfilename;
    QString extractedPath;
    QString oebpsPath;

protected:
    QTemporaryDir tempdir;
    zipper *zipr;
    QHash<QString, QString> numbers;

private slots:
    void baseXmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth);

signals:
    void ProcessTOCEntry(QString href, QString chapter);
    void ProcessCoverImage(QString fileName);

private:
    enum xmlPartsContexts {
        tocEntry,
        languageCode,
        coverImage
    };
    void SetLocalData(QString href);
    sql_class *sql;
};

#endif // EPUB_H
