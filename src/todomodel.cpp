/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "todomodel.h"

TodoModel::TodoModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

TodoModel::~TodoModel()
{
    qDeleteAll(_list);
    _list.clear();
}

int TodoModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _list.empty() ? 0 : _list.count();
}

int TodoModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 6;
}

QHash<int, QByteArray> TodoModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[IdRole] = "id";
    items[SpeakerFullNameRole] = "speakerFullName";
    items[SpeakerIdRole] = "speakerId";
    items[CongregationNameRole] = "congregationName";
    items[CongregationIdRole] = "congregationId";
    items[DateRole] = "date";
    items[TimeRangeRole] = "timeRange";
    items[ThemeAndNumberRole] = "themeAndNumber";
    items[ThemeIdRole] = "themeId";
    items[ThemeNumberRangeRole] = "themeNumberRange";
    items[NotesRole] = "notes";
    items[IsIncomingRole] = "isIncoming";
    return items;
}

QVariant TodoModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case IdRole:
        return _list[index.row()]->id();
    case DateRole:
        return _list[index.row()]->date().startOfDay();
    case TimeRangeRole:
        return _list[index.row()]->timeRange();
    case SpeakerFullNameRole:
        return _list[index.row()]->speakerFullName();
    case SpeakerIdRole:
        return _list[index.row()]->speaker() ? _list[index.row()]->speaker()->id() : 0;
    case CongregationNameRole:
        return _list[index.row()]->congregationName();
    case CongregationIdRole:
        return _list[index.row()]->congregationId();
    case ThemeAndNumberRole:
        return _list[index.row()]->themeAndNumber();
    case ThemeIdRole:
        return _list[index.row()]->theme().id;
    case ThemeNumberRangeRole:
        return _list[index.row()]->themeAndNumber();
    case NotesRole:
        return _list[index.row()]->notes();
    case IsIncomingRole:
        return _list[index.row()]->isIncoming();
    default:
        return QVariant();
    }
}

bool TodoModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid()) {
        QDateTime d;
        switch (role) {
        case DateRole:
            d = value.toDateTime();
            _list[index.row()]->setDate(d.offsetFromUtc() < 0 ? d.toUTC().date() : d.date());
            break;
        case SpeakerFullNameRole:
            _list[index.row()]->setSpeakerFullName(value.toString());
            break;
        case CongregationNameRole:
            _list[index.row()]->setCongregationName(value.toString());
            break;
        case ThemeAndNumberRole:
            _list[index.row()]->setThemeAndNumber(value.toString());
            break;
        case NotesRole:
            _list[index.row()]->setNotes(value.toString());
            break;
        default:
            return false;
        }
        emit dataChanged(index, index, { role });
        return true;
    }
    return false;
}

void TodoModel::loadList()
{
    beginResetModel();
    qDeleteAll(_list);
    _list.clear();
    _list = todo::getList();
    endResetModel();
    emit modelChanged();
}

int TodoModel::addRow(const bool inComing)
{
    todo *newTodo = new todo(inComing);
    newTodo->setDate(QDate::currentDate());
    newTodo->save();
    int newIndex = 0;
    if (inComing) {
        int i = 0;
        for (i = 0; i < _list.size(); i++) {
            if (!_list[i]->isIncoming()) {
                newIndex = i;
                break;
            }
        }
    } else {
        newIndex = rowCount();
    }
    beginInsertRows(QModelIndex(), newIndex, newIndex);
    _list.insert(newIndex, newTodo);
    endInsertRows();
    emit modelChanged();
    return newIndex;
}

void TodoModel::removeRow(const int rowIndex)
{
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    todo *selTodo = _list[rowIndex];
    selTodo->deleteme();
    delete selTodo;
    _list.removeAt(rowIndex);
    endRemoveRows();
    emit modelChanged();
}

void TodoModel::saveRow(const int rowIndex)
{
    _list[rowIndex]->save();
    emit modelChanged();
}

void TodoModel::editRow(const int row, const QDate date, const QString speakerFullName, const QString congregationName, const QString themeAndNumber, const QString notes)
{
    beginResetModel();
    _list[row]->setDate(date);
    _list[row]->setSpeakerFullName(speakerFullName);
    _list[row]->setCongregationName(congregationName);
    _list[row]->setThemeAndNumber(themeAndNumber);
    _list[row]->setNotes(notes);
    _list[row]->save();
    endResetModel();
    emit modelChanged();
}

QVariantMap TodoModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QModelIndex TodoModel::getTodoIndex(int todoId) const
{
    for (int row = 0; row < this->rowCount(); ++row) {
        QModelIndex rowIndex = this->index(row, 0);
        if (rowIndex.data(IdRole) == todoId)
            return rowIndex;
    }
    return QModelIndex();
}

QString TodoModel::moveToSchedule(const int row)
{
    QString returnValue = _list[row]->moveToSchedule();
    if (returnValue.isEmpty()) {
        removeRow(row);
        emit modelChanged();
    }
    return returnValue;
}

TodoSFProxyModel::TodoSFProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *TodoSFProxyModel::source() const
{
    return sourceModel();
}

void TodoSFProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
    emit sourceChanged();
}

bool TodoSFProxyModel::isIncoming() const
{
    return m_isIncoming;
}

void TodoSFProxyModel::setIsIncoming(bool newIsIncoming)
{
    if (m_isIncoming == newIsIncoming)
        return;
    m_isIncoming = newIsIncoming;
    emit isIncomingChanged();
}

QString TodoSFProxyModel::filterText() const
{
    return m_filterText;
}

void TodoSFProxyModel::setFilterText(QString value)
{
    m_filterText = value;
    invalidateFilter();
    emit filterChanged();
}

QByteArray TodoSFProxyModel::sortRole() const
{
    return roleNames().value(QSortFilterProxyModel::sortRole());
}

void TodoSFProxyModel::setSortRole(const QByteArray &role)
{
    QSortFilterProxyModel::setSortRole(roleKey(role));
    emit sortChanged();
}

QByteArray TodoSFProxyModel::groupByRole() const
{
    return m_groupByRole;
}

void TodoSFProxyModel::setGroupByRole(const QByteArray &role)
{
    beginResetModel();
    m_groupByRole = role;
    endResetModel();
    emit groupByChanged();
}

bool TodoSFProxyModel::filterAcceptsRow(int sourceRow,
                                        const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return sourceModel()->data(index, TodoModel::Roles::IsIncomingRole).toBool() == isIncoming()
            && (sourceModel()->data(index, TodoModel::Roles::CongregationNameRole).toString().contains(filterText(), Qt::CaseInsensitive)
                || sourceModel()->data(index, TodoModel::Roles::SpeakerFullNameRole).toString().contains(filterText(), Qt::CaseInsensitive)
                || sourceModel()->data(index, TodoModel::Roles::ThemeAndNumberRole).toString().contains(filterText(), Qt::CaseInsensitive));
}

bool TodoSFProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    // compare sections first
    switch (roleKey(groupByRole())) {
    case TodoModel::Roles::CongregationNameRole: {
        QVariant val1 = sourceModel()->data(left, TodoModel::Roles::CongregationNameRole);
        QVariant val2 = sourceModel()->data(right, TodoModel::Roles::CongregationNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        break;
    }
    case TodoModel::Roles::SpeakerFullNameRole: {
        QVariant val1 = sourceModel()->data(left, TodoModel::Roles::SpeakerFullNameRole);
        QVariant val2 = sourceModel()->data(right, TodoModel::Roles::SpeakerFullNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        break;
    }
    default:
        break;
    }
    // sort by date within sections
    QVariant leftData = sourceModel()->data(left, TodoModel::Roles::DateRole);
    QVariant rightData = sourceModel()->data(right, TodoModel::Roles::DateRole);
    if (leftData.userType() == QMetaType::QDate) {
        return leftData.toDate() < rightData.toDate();
    } else
        return leftData.toString() < rightData.toString();
}

int TodoSFProxyModel::roleKey(const QByteArray &role) const
{
    QHash<int, QByteArray> roles = roleNames();
    QHashIterator<int, QByteArray> it(roles);
    while (it.hasNext()) {
        it.next();
        if (it.value() == role)
            return it.key();
    }
    return -1;
}

TodoValidator::TodoValidator(QObject *parent)
    : QValidator(parent), m_model(nullptr), m_todoId(-1), m_role(TodoModel::Roles::None)
{
}

TodoValidator::~TodoValidator()
{
}

QValidator::State TodoValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos)

    if (input.isEmpty()) {
        emit errorChanged("");
        return Acceptable;
    }
    if (!model()) {
        emit errorChanged("");
        return Acceptable;
    }
    if (role() == TodoModel::Roles::None) {
        emit errorChanged("");
        return Acceptable;
    }
    QModelIndex modelIndex = model()->getTodoIndex(todoId());
    if (!modelIndex.isValid()) {
        emit errorChanged("");
        return Acceptable;
    }

    cpublictalks cpt;
    bool isIncoming = model()->data(modelIndex, TodoModel::Roles::IsIncomingRole).toBool();
    int congregationId = model()->data(modelIndex, TodoModel::Roles::CongregationIdRole).toInt();

    // check date
    QDate meetingDate = model()->data(modelIndex, TodoModel::Roles::DateRole).toDate();
    QDate currentWeekCommencingDate = QDate::currentDate().addDays(1 - QDate::currentDate().dayOfWeek());
    if (role() == TodoModel::Roles::DateRole) {
        if (!meetingDate.isValid()) {
            emit errorChanged("");
            return Intermediate;
        }
        if (meetingDate < currentWeekCommencingDate) {
            emit errorChanged(tr("This item is overdue. Please remove it or choose another date.", "Public talk to do item"));
            return Invalid;
        }
        if (isIncoming) {
            // check for existing scheduled talk
            cptmeeting *meeting = cpt.getMeeting(meetingDate);
            if (meeting->speaker()) {
                emit errorChanged(tr("This date is already scheduled. Please choose another date.", "Public talk to do item"));
                return Invalid;
            }
        } else {
            // check for existing scheduled talk in same congregation
            QList<cpoutgoing *> outgoing = cpt.getOutgoingSpeakers(meetingDate);
            foreach (cpoutgoing *ot, outgoing) {
                if (ot->congregation().id == congregationId) {
                    emit errorChanged(tr("This date is already scheduled. Please choose another date.", "Public talk to do item"));
                    return Invalid;
                }
            }
        }
        emit errorChanged("");
        return Acceptable;
    }

    // check theme
    int themeId = model()->data(modelIndex, TodoModel::Roles::ThemeIdRole).toInt();
    if (role() == TodoModel::Roles::ThemeAndNumberRole) {
        cpttheme pt = cpt.getThemeById(themeId);
        if (pt.discontinueDate.isValid() && pt.discontinueDate < meetingDate) {
            emit errorChanged(tr("This talk has been discontinued. Please choose another talk."));
            return Invalid;
        }
    }

    // check speaker
    int speakerId = model()->data(modelIndex, TodoModel::Roles::SpeakerIdRole).toInt();
    cpersons cp;
    person *assignee = cp.getPerson(speakerId);
    if (assignee) {
        bool isIncoming = model()->data(modelIndex, TodoModel::Roles::IsIncomingRole).toBool();
        if (role() == TodoModel::Roles::CongregationNameRole) {
            // validate congregation
            if (isIncoming && assignee->congregationName().compare(input, Qt::CaseInsensitive)) {
                emit errorChanged(tr("The speaker is no member of this congregation. Please choose another speaker or congregation."));
                return Invalid;
            }
            emit errorChanged("");
            return Acceptable;
        }

        if (role() == TodoModel::Roles::ThemeAndNumberRole) {
            // validate theme
            sql_items speakersThemes = cpt.getThemeList(assignee);
            auto iterator = std::find_if(speakersThemes.begin(), speakersThemes.end(),
                                         [&themeId](const sql_item &themeItem) { return themeItem.value("theme_id").toInt() == themeId; });
            if (iterator == speakersThemes.end()) {
                emit errorChanged(tr("This talk cannot be delivered by the speaker. Please choose another talk or speaker."));
                return Invalid;
            }
            emit errorChanged("");
            return Acceptable;
        }

        // check if QML delegate is up to date
        if (role() == TodoModel::Roles::SpeakerFullNameRole && !input.startsWith(assignee->fullname())) {
            emit errorChanged("");
            return Intermediate;
        }

        if (!isIncoming) {
            // is outgoing speaker member of own congregation?
            ccongregation cc;
            int ownCongregationId = cc.getMyCongregation().id;
            bool isNoMemberOfOwnCongregation = assignee
                    ? assignee->congregationid() != ownCongregationId
                    : false;
            if (isNoMemberOfOwnCongregation) {
                emit errorChanged(tr("The assigned person is no member of the congregation."));
                return Invalid;
            }
        }

        // check admission
        if (!(assignee->usefor())) {
            emit errorChanged(tr("Unassignable. Please check the speaker's settings."));
            return Invalid;
        }
    }
    emit errorChanged("");
    return Acceptable;
}

TodoModel *TodoValidator::model() const
{
    return m_model;
}

void TodoValidator::setModel(TodoModel *newModel)
{
    if (m_model == newModel)
        return;
    m_model = newModel;
    emit modelChanged();
}

int TodoValidator::todoId() const
{
    return m_todoId;
}

void TodoValidator::setTodoId(int newTodoId)
{
    if (m_todoId == newTodoId)
        return;
    m_todoId = newTodoId;
    emit todoIdChanged();
}

TodoModel::Roles TodoValidator::role() const
{
    return m_role;
}

void TodoValidator::setRole(TodoModel::Roles newRole)
{
    if (m_role == newRole)
        return;
    m_role = newRole;
    emit roleChanged();
}
