#ifndef DROPBOXSYNCBUTTON_H
#define DROPBOXSYNCBUTTON_H

#include <QToolButton>
#include <QPropertyAnimation>
#include <QQuickWidget>
#include <QQuickItem>
#include <QQmlFileSelector>
#include <QMovie>
#include <QDebug>
#include <QObject>
#include <QGuiApplication>
#include <QScreen>
#include <QSvgRenderer>
#include <QPainter>

class DropboxSyncButton : public QToolButton
{
    Q_OBJECT
public:
    DropboxSyncButton(QWidget *parent = nullptr);
    QQuickWidget *panel() const;
    void startIconAnimation();
    void stopIconAnimation();
    void closePanel();

protected:
    virtual void enterEvent(QEvent *e);
    virtual void leaveEvent(QEvent *e);
    virtual void hideEvent(QHideEvent *event);
    virtual bool eventFilter(QObject *object, QEvent *event);

signals:
    void panelEnter();
    void panelLeave();

private slots:
    void setPosition(const QRectF &rect);

private:
    QQuickWidget *m_panel;
    QPropertyAnimation *m_animation;
    QIcon m_restore_icon;
    QMovie *m_movie = nullptr;
    QSvgRenderer *m_renderer;
    QPixmap pixmap;
    bool m_panel_entered;
    bool m_animation_running = false;
};

#endif // DROPBOXSYNCBUTTON_H
