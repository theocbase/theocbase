#include "lmm_assignmentcontoller.h"

LMM_AssignmentContoller::LMM_AssignmentContoller(QObject *parent)
    : QObject(parent)
{
    m_assignment = nullptr;
}

LMM_Assignment_ex *LMM_AssignmentContoller::assignment() const
{
    return m_assignment;
}

void LMM_AssignmentContoller::setAssignment(LMM_Assignment_ex *a)
{
    m_assignment = a;
}

QPoint LMM_AssignmentContoller::getMousePosition() const
{
    qDebug() << "get mouse pos";
    return QCursor::pos();
}

QRect LMM_AssignmentContoller::getScreenAvailableGeometry() const
{
    QScreen *currentScreen = QGuiApplication::screenAt(QCursor::pos());
    return currentScreen->availableGeometry();
}

QPoint LMM_AssignmentContoller::getDialogPosition(int dialogHeight, int dialogWidth, int controlPosX, int controlPosY) const
{
    QPoint mousePos = QCursor::pos();
    QRect screen = getScreenAvailableGeometry();
    int dialogPosX, dialogPosY;

    qDebug() << "Screen x: " << screen.x() << " y: " << screen.y() << " w: " << screen.width() << " h: " << screen.height();
    qDebug() << "Mouse x:" << mousePos.x() << " y: " << mousePos.y();
    qDebug() << "Control x:" << controlPosX << " y: " << controlPosY << " w: " << dialogWidth << " h: " << dialogHeight;
    dialogPosX = mousePos.x() - controlPosX;
    dialogPosY = mousePos.y() - controlPosY;
    qDebug() << "Calculated Position x: " << dialogPosX << " y: " << dialogPosY;

    if (dialogPosY + dialogHeight > screen.y() + screen.height() - 10) {
        dialogPosY = screen.y() + screen.height() - dialogHeight - 10;
    }

    if (dialogPosX + dialogWidth > screen.x() + screen.width() - 10) {
        dialogPosX = screen.x() + screen.width() - dialogWidth - 10;
    }

    qDebug() << "Calculated final Position x: " << dialogPosX << " y: " << dialogPosY;
    return QPoint(dialogPosX, dialogPosY);
}

person *LMM_AssignmentContoller::getPublisherById(int id) const
{
    return cpersons::getPerson(id);
}

QString LMM_AssignmentContoller::getHistoryTooltip(int userid, int fontsize)
{
    auto tableWidgetSchoolTask = getSchoolHistory(userid, Qt::black, Qt::white);
    int rows = std::min(10, tableWidgetSchoolTask.rowCount() + 1);
    QTextDocument doc;
    QFont docFont;
    docFont.setPointSize(fontsize);
    doc.setDefaultFont(docFont);
    QTextCursor cursor(&doc);
    cursor.movePosition(QTextCursor::Start);

    QTextTableFormat format;
    format.setBorder(1);
    format.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
    format.setCellSpacing(0);
    format.setCellPadding(2);
    format.setMargin(0);
    QTextTable *table = cursor.insertTable(rows, 7, format);

    table->cellAt(0, 0).firstCursorPosition().insertText(QObject::tr("Date"));
    table->cellAt(0, 1).firstCursorPosition().insertText("#");
    table->cellAt(0, 2).firstCursorPosition().insertText(QObject::tr("Assignment"));
    table->cellAt(0, 3).firstCursorPosition().insertText(QObject::tr("Note"));
    table->cellAt(0, 4).firstCursorPosition().insertText(QObject::tr("Time"));
    table->cellAt(0, 5).firstCursorPosition().insertText(" ");
    table->cellAt(0, 6).firstCursorPosition().insertText(QObject::tr("Together", "The column header text to show partner in student assignment"));

    if (tableWidgetSchoolTask.rowCount()) {
        for (int r = 1; r < rows; ++r) {
            for (int c = 0; c < 6; ++c) {
                auto item = tableWidgetSchoolTask.item(r - 1, c);
                QTextCharFormat format;
                QFont font;
                font.setItalic(item.italic());
                font.setBold(item.bold());
                font.setStrikeOut(item.strikeOut());
                font.setPointSize(fontsize);
                format.setFont(font);

                if (item.customBackground())
                    format.setBackground(item.background());
                if (item.customForeground())
                    format.setForeground(item.foreground());

                if (!item.iconName().isEmpty()) {
                    table->cellAt(r, c).firstCursorPosition().insertImage(item.iconName());
                    c++;
                }

                table->cellAt(r, c).firstCursorPosition().insertText(item.text(), format);
                table->cellAt(r, c).setFormat(format);
            }
        }
    }
    return doc.toHtml();
}
