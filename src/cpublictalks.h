/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPUBLICTALKS_H
#define CPUBLICTALKS_H

#include <QObject>
#include <QDate>
#include <QStandardItemModel>
#include <QValidator>
#include "cpersons.h"
#include "person.h"
#include "ccongregation.h"
#include "sql_class.h"
#include "specialevent.h"

struct ThemeListItem {
    int id;
    int number;
    QString theme;
    QDate last;
    QString revision;
};

struct cpttheme {
    QString theme = "";
    int number = -1;
    int id = -1;
    QDate discontinueDate = QDate();
    QString themeInLanguage(int lang_id, QDate date = QDate::currentDate());
};

class cpoutgoing
{
public:
    cpoutgoing(int id, int congregation_id, QDate weekOf);
    ~cpoutgoing() = default;
    cpttheme theme() const;
    void setTheme(const cpttheme &t);
    person *speaker() const;
    void setSpeaker(person *p);
    ccongregation::congregation const &congregation() const;
    void setCongregation(const int id);
    int id() const;
    QDate weekOf() const;
    void setWeekOf(const QDate &newWeekOf);
    QDate date() const;
    QString time() const;

    bool save();

private:
    cpttheme _ctheme;
    QSharedPointer<person> _speaker;
    ccongregation::congregation _congregation;
    int _id;
    QDate _weekOf;
    sql_class *sql;
};

class cptmeeting : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date NOTIFY meetingChanged)
    Q_PROPERTY(person *chairman READ chairman WRITE setChairman NOTIFY meetingChanged)
    Q_PROPERTY(int themeId READ themeId NOTIFY meetingChanged)
    Q_PROPERTY(int themeNumber READ themeNumber NOTIFY meetingChanged)
    Q_PROPERTY(QString theme READ themeName NOTIFY meetingChanged)
    Q_PROPERTY(person *speaker READ speaker WRITE setSpeaker NOTIFY meetingChanged)
    Q_PROPERTY(QString wtTheme MEMBER wttheme NOTIFY meetingChanged)
    Q_PROPERTY(person *wtConductor READ wtConductor WRITE setWtConductor NOTIFY meetingChanged)
    Q_PROPERTY(person *wtReader READ wtReader WRITE setWtReader NOTIFY meetingChanged)
    Q_PROPERTY(int songTalk READ songTalk WRITE setSongTalk NOTIFY songTalkChanged)
    Q_PROPERTY(int songWtStart READ songWtStart WRITE setSongWtStart NOTIFY songWtStartChanged)
    Q_PROPERTY(int songWtEnd READ songWtEnd WRITE setSongWtEnd NOTIFY songWtEndChanged)
    Q_PROPERTY(QString songTalkTitle READ getSong1Title NOTIFY meetingChanged)
    Q_PROPERTY(QString songWtStartTitle READ getSong2Title NOTIFY meetingChanged)
    Q_PROPERTY(QString songWtEndTitle READ getSong3Title NOTIFY meetingChanged)
    Q_PROPERTY(int wtTime MEMBER wt_time NOTIFY meetingChanged)
    Q_PROPERTY(QString wtSource MEMBER wtsource NOTIFY meetingChanged)
    Q_PROPERTY(QString wtIssue MEMBER wtissue NOTIFY meetingChanged)
    Q_PROPERTY(QString finalTalk READ getFinalTalk WRITE setFinalTalk NOTIFY meetingChanged)
    Q_PROPERTY(QString finalTalkSpeakerName MEMBER final_talk_speaker NOTIFY meetingChanged)
    Q_PROPERTY(QString notes MEMBER notes NOTIFY meetingChanged)
    Q_PROPERTY(QDateTime startTime MEMBER start_time NOTIFY meetingChanged)
    Q_PROPERTY(person *openingPrayer READ openingPrayer WRITE setOpeningPrayer NOTIFY meetingChanged)
    Q_PROPERTY(person *finalPrayer READ finalPrayer WRITE setFinalPrayer NOTIFY meetingChanged)
    Q_PROPERTY(person *hospitalityHost READ getHospitalityhost WRITE setHospitalityhost NOTIFY meetingChanged)

public:
    cptmeeting(QDate date = QDate(), QObject *parent = nullptr);
    ~cptmeeting() = default;

    cpttheme theme;
    QString wtsource;
    QString wtissue;
    QString wttheme;
    int wt_time;
    QString notes;
    QDateTime start_time;
    QString final_talk_speaker;
    int id;

    void initMeeting();

    QDate date() const;

    person *speaker();
    void setSpeaker(person *s);

    person *chairman();
    void setChairman(person *c);

    person *wtConductor();
    void setWtConductor(person *c);

    person *wtReader();
    void setWtReader(person *r);

    int themeId() const;
    int themeNumber() const;

    QString themeName() const;
    Q_INVOKABLE void setTheme(int id);

    person *getHospitalityhost() const;
    void setHospitalityhost(person *hospitalityhost);

    int songTalk() const;
    void setSongTalk(int newSongTalk);

    int songWtStart() const;
    void setSongWtStart(int newSongWtStart);

    int songWtEnd() const;
    void setSongWtEnd(int newSongWtEnd);

    QString getSong1Title() const;
    QString getSong2Title() const;
    QString getSong3Title() const;
    QString getFinalTalk() const;
    void setFinalTalk(const QString &talktheme);

    QColor getWtIssueColor();
    QColor getWtIssueLightColor();

    person *openingPrayer() const;
    void setOpeningPrayer(person *newOpeningPrayer);
    person *finalPrayer() const;
    void setFinalPrayer(person *newFinalPrayer);

    Q_INVOKABLE void save();
    Q_INVOKABLE void saveNotes();

signals:
    void meetingChanged();
    void songTalkChanged();
    void songWtStartChanged();
    void songWtEndChanged();

private:
    QDate _date;
    sql_class *sql;
    QString getSongTitle(int song) const;
    QString m_final_talk;
    person *m_speaker;
    person *m_chairman;
    person *m_wtconductor;
    person *m_wtreader;
    person *m_hospitalityhost;
    person *m_openingPrayer;
    person *m_finalPrayer;
    int m_songTalk;
    int m_songWtStart;
    int m_songWtEnd;
};

class cpublictalks
{
public:
    cpublictalks();
    cptmeeting *getMeeting(QDate date);
    QList<cpoutgoing *> getOutgoingSpeakers(const QDate d);
    QList<cpoutgoing *> getOutgoingBySpeaker(QDate d, int speakerid, bool onlythismonth = false);
    QList<cpoutgoing *> getOutgoingBySpeaker(int speakerid, QDate fromdate, QDate todate);
    cpoutgoing *getOutgoingByCongregation(const QDate d, const QString &congregationName);
    cpoutgoing *addOutgoingSpeaker(const QDate d, int speakerid, int themeid, int congregationid);
    bool removeOutgoingSpeaker(int id);
    cpttheme getThemeById(int id);
    cpttheme getThemeByNumber(int number, QDate date = QDate::currentDate());
    sql_items getThemeList(person *speaker = nullptr);
    std::vector<ThemeListItem> getThemeList(const QString &customFilter, person *speaker = nullptr, QDate date = QDate::currentDate(), bool addBlankRow = false);
    QStandardItemModel *getThemesTable(const QString &customFilter, person *speaker = nullptr, QDate date = QDate::currentDate(), bool addBlankRow = false);
    QStandardItemModel *getSpeakers(int themeid = 0, int congregationid = 0, const QString &customFilter = "", bool onlyOwnCongregation = false, bool addBlankRow = true, QDate date = QDate());
    person *defaultWtConductor();

private:
    QString langId;
    sql_class *sql;
};

class WEMeetingValidator : public QValidator
{
    Q_OBJECT
    Q_PROPERTY(cptmeeting *meeting READ meeting WRITE setMeeting NOTIFY meetingChanged)
    Q_PROPERTY(WEMeetingValidator::Field field READ field WRITE setField NOTIFY fieldChanged)
public:
    enum Field {
        None = 0,
        Chairman = 1,
        Theme,
        Congregation, // TODO: add a field in the db to keep track of with which conregation an exchange was arranged
        Speaker,
        WtConductor,
        WtReader,
        OpeningPrayer,
        FinalPrayer,
        HospitalityHost
    };
    Q_ENUM(Field)

    WEMeetingValidator(QObject *parent = nullptr);
    ~WEMeetingValidator();

    QValidator::State validate(QString &input, int &pos) const override;

    cptmeeting *meeting() const;
    void setMeeting(cptmeeting *newMeeting);

    WEMeetingValidator::Field field() const;
    void setField(Field newField);

Q_SIGNALS:
    void meetingChanged();
    void fieldChanged();
    void errorChanged(const QString &error) const;

private:
    cptmeeting *m_meeting;
    Field m_field;
};

#endif // CPUBLICTALKS_H
