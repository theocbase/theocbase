#include "dropboxsyncbutton.h"

DropboxSyncButton::DropboxSyncButton(QWidget *parent)
    : QToolButton(parent)
{
    m_panel = new QQuickWidget(this->parentWidget());
    m_panel->installEventFilter(this);
    m_panel->setGeometry(QRect(0, 0, 0, 0));
    m_panel->setWindowFlags(m_panel->windowFlags() | Qt::ToolTip);
    m_panel->setMouseTracking(true);
    m_panel->setResizeMode(QQuickWidget::SizeViewToRootObject);
    new QQmlFileSelector(m_panel->engine(), this);

    connect(this, &DropboxSyncButton::panelLeave, [=]() {
        if (m_panel->isVisible()) {
            m_panel->close();
        }
    });

    qreal dpr = qApp->devicePixelRatio();
    m_renderer = new QSvgRenderer(QString(":/icons/animated_sync.svg"), this);
    pixmap = QPixmap(24 * dpr, 24 * dpr);
    pixmap.fill(Qt::transparent);
    pixmap.setDevicePixelRatio(dpr);

    connect(m_renderer, &QSvgRenderer::repaintNeeded, [this] {
        if (m_animation_running) {
            QPainter p(&pixmap);
            pixmap.fill(Qt::transparent);
            m_renderer->render(&p, QRectF(0, 0, 24, 24));
            setIcon(pixmap);
        }
    });
}

void DropboxSyncButton::enterEvent(QEvent *e)
{
    if (!m_panel->isVisible()) {
        if (m_panel->source().isEmpty()) {
            m_panel->setSource(QUrl("qrc:/qml/DropboxSettings.qml"));
            connect(m_panel->rootObject(), SIGNAL(childrenRectChanged(QRectF)),
                    this, SLOT(setPosition(QRectF)));
        }

        QRectF rect;
        if (m_panel->rootObject()) {
            rect = m_panel->rootObject()->property("childrenRect").toRectF();
            setPosition(rect);
        }
        m_panel->show();
        m_panel->setMouseTracking(true);
    }
    QToolButton::enterEvent(e);
}

void DropboxSyncButton::leaveEvent(QEvent *e)
{    
    QRect testrect = QRect(this->mapToGlobal(this->rect().bottomLeft()),
                           m_panel->size());
    QPoint mousePos = QCursor::pos();

    if (m_panel->isVisible()) {
        if (testrect.contains(mousePos))
            return;
        emit panelLeave();
    }

    QToolButton::leaveEvent(e);
}

void DropboxSyncButton::hideEvent(QHideEvent *event)
{
    if (m_panel->isVisible())
        m_panel->close();
    QToolButton::hideEvent(event);
}

bool DropboxSyncButton::eventFilter(QObject *object, QEvent *event)
{
    if (object == m_panel) {
        if (event->type() == QEvent::MouseMove) {
            if (!m_panel_entered)
                emit panelEnter();
            m_panel_entered = true;
        } else if (event->type() == QEvent::Leave) {
            if (m_panel_entered) {
                emit panelLeave();
                m_panel_entered = false;
            }
        }
    }

    return QToolButton::eventFilter(object, event);
}

void DropboxSyncButton::setPosition(const QRectF &rect)
{
    QScreen *actualScreen = this->window()->windowHandle()->screen();
    QRect screenRect = actualScreen->availableGeometry();
    QPoint p = this->mapToGlobal(this->rect().bottomLeft());
    p.setY(p.y() + 1);
    if (p.x() + rect.width() > screenRect.x() + screenRect.width()) {
        p.setX(screenRect.x() + screenRect.width() - rect.width());
    }
    m_panel->move(p);
    if (!m_panel->isVisible()) {
        m_panel->setVisible(true);
    }
}

QQuickWidget *DropboxSyncButton::panel() const
{
    return m_panel;
}

void DropboxSyncButton::startIconAnimation()
{
    m_animation_running = true;
    m_restore_icon = this->icon();
}

void DropboxSyncButton::stopIconAnimation()
{
    m_animation_running = false;
    this->setIcon(m_restore_icon);
}

void DropboxSyncButton::closePanel()
{
    m_panel->close();
}
