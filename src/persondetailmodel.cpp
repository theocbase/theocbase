#include "persondetailmodel.h"
#include "availability/weekendmeetingavailabilitychecker.h"
#include "family.h"

using namespace tbAvailability;

PersonDetail::PersonDetail()
{
}

PersonDetail::PersonDetail(const QString personFullName,
                           QObject *parent)
    : QObject(parent), m_personId(0), m_personFirstName(""), m_personLastName(""), m_personFullName(personFullName), m_date(QDate()), m_assignmentInfo(nullptr), m_theme(""), m_congregationName(""), m_assigneeId(0), m_assigneeFirstName(""), m_assigneeLastName(""), m_volunteerId(0), m_volunteerFirstName(""), m_volunteerLastName(""), m_assistantId(0), m_assistantFirstName(""), m_assistantLastName(""), m_studyPoint(""), m_timing(""), m_note(""), m_previous1Date(QDate()), m_previous1AssignmentInfo(nullptr), m_previous1Theme(""), m_previous1CongregationName(""), m_previous1AssigneeId(0), m_previous1AssigneeFirstName(""), m_previous1AssigneeLastName(""), m_previous1VolunteerId(0), m_previous1VolunteerFirstName(""), m_previous1VolunteerLastName(""), m_previous1AssistantId(0), m_previous1AssistantFirstName(""), m_previous1AssistantLastName(""), m_previous2Date(QDate()), m_previous2AssignmentInfo(nullptr), m_previous2Theme(""), m_previous2CongregationName(""), m_previous2AssigneeId(0), m_previous2AssigneeFirstName(""), m_previous2AssigneeLastName(""), m_previous2VolunteerId(0), m_previous2VolunteerFirstName(""), m_previous2VolunteerLastName(""), m_previous2AssistantId(0), m_previous2AssistantFirstName(""), m_previous2AssistantLastName(""), m_isUnavailable(false), m_isMultiAssigned(false), m_anyPreviousAssignmentInfo(nullptr), m_anyFollowingAssignmentInfo(nullptr), m_isFamilyMemberAssigned(false), m_assignmentCount(1), m_assignmentFrequencyRange(1), m_timeRange(1)
{
}

PersonDetail::PersonDetail(const int personId, const QString personFirstName, const QString personLastName,
                           const QDate date, AssignmentInfo *assignmentInfo, const QString theme, const QString congregationName,
                           const int assigneeId, const QString assigneeFirstName, const QString assigneeLastName,
                           const int volunteerId, const QString volunteerFirstName, const QString volunteerLastName,
                           const int assistantId, const QString assistantFirstName, const QString assistantLastName,
                           const QString studyPoint, const QString timing, const QString note,
                           const QDate previous1Date, AssignmentInfo *previous1AssignmentInfo, const QString previous1Theme, const QString previous1CongregationName,
                           const int previous1AssigneeId, const QString previous1AssigneeFirstName, const QString previous1AssigneeLastName,
                           const int previous1VolunteerId, const QString previous1VolunteerFirstName, const QString previous1VolunteerLastName,
                           const int previous1AssistantId, const QString previous1AssistantFirstName, const QString previous1AssistantLastName,
                           const QDate previous2Date, AssignmentInfo *previous2AssignmentInfo, const QString previous2Theme, const QString previous2CongregationName,
                           const int previous2AssigneeId, const QString previous2AssigneeFirstName, const QString previous2AssigneeLastName,
                           const int previous2VolunteerId, const QString previous2VolunteerFirstName, const QString previous2VolunteerLastName,
                           const int previous2AssistantId, const QString previous2AssistantFirstName, const QString previous2AssistantLastName,
                           const bool isUnavailable, const bool isMultiAssigned, const int weekOfAnyPreviousAssignment, AssignmentInfo *anyPreviousAssignmentInfo,
                           const int weekOfAnyFollowingAssignment, AssignmentInfo *anyFollowingAssignmentInfo, const bool isFamilyMemberAsssigned,
                           const bool isSpeakerAwayInSameMonth, const int assignmentCount, const int assignmentFrequencyRange, const int timeRange,
                           QObject *parent)
    : QObject(parent), m_personId(personId), m_personFirstName(personFirstName), m_personLastName(personLastName), m_personFullName(""), m_date(date), m_assignmentInfo(assignmentInfo), m_theme(theme), m_congregationName(congregationName), m_assigneeId(assigneeId), m_assigneeFirstName(assigneeFirstName), m_assigneeLastName(assigneeLastName), m_volunteerId(volunteerId), m_volunteerFirstName(volunteerFirstName), m_volunteerLastName(volunteerLastName), m_assistantId(assistantId), m_assistantFirstName(assistantFirstName), m_assistantLastName(assistantLastName), m_studyPoint(studyPoint), m_timing(timing), m_note(note), m_previous1Date(previous1Date), m_previous1AssignmentInfo(previous1AssignmentInfo), m_previous1Theme(previous1Theme), m_previous1CongregationName(previous1CongregationName), m_previous1AssigneeId(previous1AssigneeId), m_previous1AssigneeFirstName(previous1AssigneeFirstName), m_previous1AssigneeLastName(previous1AssigneeLastName), m_previous1VolunteerId(previous1VolunteerId), m_previous1VolunteerFirstName(previous1VolunteerFirstName), m_previous1VolunteerLastName(previous1VolunteerLastName), m_previous1AssistantId(previous1AssistantId), m_previous1AssistantFirstName(previous1AssistantFirstName), m_previous1AssistantLastName(previous1AssistantLastName), m_previous2Date(previous2Date), m_previous2AssignmentInfo(previous2AssignmentInfo), m_previous2Theme(previous2Theme), m_previous2CongregationName(previous2CongregationName), m_previous2AssigneeId(previous2AssigneeId), m_previous2AssigneeFirstName(previous2AssigneeFirstName), m_previous2AssigneeLastName(previous2AssigneeLastName), m_previous2VolunteerId(previous2VolunteerId), m_previous2VolunteerFirstName(previous2VolunteerFirstName), m_previous2VolunteerLastName(previous2VolunteerLastName), m_previous2AssistantId(previous2AssistantId), m_previous2AssistantFirstName(previous2AssistantFirstName), m_previous2AssistantLastName(previous2AssistantLastName), m_isUnavailable(isUnavailable), m_isMultiAssigned(isMultiAssigned), m_weekOfAnyPreviousAssignment(weekOfAnyPreviousAssignment), m_anyPreviousAssignmentInfo(anyPreviousAssignmentInfo), m_weekOfAnyFollowingAssignment(weekOfAnyFollowingAssignment), m_anyFollowingAssignmentInfo(anyFollowingAssignmentInfo), m_isFamilyMemberAssigned(isFamilyMemberAsssigned), m_isSpeakerAwayInSameMonth(isSpeakerAwayInSameMonth), m_assignmentCount(assignmentCount), m_assignmentFrequencyRange(assignmentFrequencyRange), m_timeRange(timeRange)
{
}

int PersonDetail::personId() const
{
    return m_personId;
}

QString PersonDetail::personFirstName() const
{
    return m_personFirstName;
}

QString PersonDetail::personLastName() const
{
    return m_personLastName;
}

QString PersonDetail::personFullName(QString format)
{
    return !m_personFullName.isEmpty()
            ? m_personFullName // e.g. name of CO
            : getFullName(this->personFirstName(), this->personLastName(), format);
}

QDate PersonDetail::date() const
{
    return m_date;
}

AssignmentInfo *PersonDetail::assignmentInfo() const
{
    return m_assignmentInfo;
}

QString PersonDetail::theme() const
{
    return m_theme;
}

const QString &PersonDetail::congregationName() const
{
    return m_congregationName;
}

int PersonDetail::assigneeId() const
{
    return m_assigneeId;
}

QString PersonDetail::assigneeFirstName() const
{
    return m_assigneeFirstName;
}

QString PersonDetail::assigneeLastName() const
{
    return m_assigneeLastName;
}

QString PersonDetail::assigneeFullName(QString format)
{
    return getFullName(this->assigneeFirstName(), this->assigneeLastName(), format);
}

int PersonDetail::volunteerId() const
{
    return m_volunteerId;
}

QString PersonDetail::volunteerFirstName() const
{
    return m_volunteerFirstName;
}

QString PersonDetail::volunteerLastName() const
{
    return m_volunteerLastName;
}

QString PersonDetail::volunteerFullName(QString format)
{
    return getFullName(this->volunteerFirstName(), this->volunteerLastName(), format);
}

int PersonDetail::assistantId() const
{
    return m_assistantId;
}
QString PersonDetail::assistantFirstName() const
{
    return m_assistantFirstName;
}

QString PersonDetail::assistantLastName() const
{
    return m_assistantLastName;
}

QString PersonDetail::assistantFullName(QString format)
{
    return getFullName(this->assistantFirstName(), this->assistantLastName(), format);
}

QString PersonDetail::studyPoint() const
{
    return m_studyPoint;
}

QString PersonDetail::timing() const
{
    return m_timing;
}

QString PersonDetail::note() const
{
    return m_note;
}

QDate PersonDetail::previous1Date() const
{
    return m_previous1Date;
}

AssignmentInfo *PersonDetail::previous1AssignmentInfo() const
{
    return m_previous1AssignmentInfo;
}

QString PersonDetail::previous1Theme() const
{
    return m_previous1Theme;
}

const QString &PersonDetail::previous1CongregationName() const
{
    return m_previous1CongregationName;
}

QString PersonDetail::previous1AssigneeFirstName() const
{
    return m_previous1AssigneeFirstName;
}

QString PersonDetail::previous1AssigneeLastName() const
{
    return m_previous1AssigneeLastName;
}

QString PersonDetail::previous1AssigneeFullName(QString format)
{
    return getFullName(this->previous1AssigneeFirstName(), this->previous1AssigneeLastName(), format);
}

QString PersonDetail::previous1VolunteerFirstName() const
{
    return m_previous1VolunteerFirstName;
}

QString PersonDetail::previous1VolunteerLastName() const
{
    return m_previous1VolunteerLastName;
}

QString PersonDetail::previous1VolunteerFullName(QString format)
{
    return getFullName(this->previous1VolunteerFirstName(), this->previous1VolunteerLastName(), format);
}

QString PersonDetail::previous1AssistantFirstName() const
{
    return m_previous1AssistantFirstName;
}

QString PersonDetail::previous1AssistantLastName() const
{
    return m_previous1AssistantLastName;
}

QString PersonDetail::previous1AssistantFullName(QString format)
{
    return getFullName(this->previous1AssistantFirstName(), this->previous1AssistantLastName(), format);
}

QDate PersonDetail::previous2Date() const
{
    return m_previous2Date;
}

AssignmentInfo *PersonDetail::previous2AssignmentInfo() const
{
    return m_previous2AssignmentInfo;
}

QString PersonDetail::previous2Theme() const
{
    return m_previous2Theme;
}

const QString &PersonDetail::previous2CongregationName() const
{
    return m_previous2CongregationName;
}

QString PersonDetail::previous2AssigneeFirstName() const
{
    return m_previous2AssigneeFirstName;
}

QString PersonDetail::previous2AssigneeLastName() const
{
    return m_previous2AssigneeLastName;
}

QString PersonDetail::previous2AssigneeFullName(QString format)
{
    return getFullName(this->previous2AssigneeFirstName(), this->previous2AssigneeLastName(), format);
}

QString PersonDetail::previous2VolunteerFirstName() const
{
    return m_previous2VolunteerFirstName;
}

QString PersonDetail::previous2VolunteerLastName() const
{
    return m_previous2VolunteerLastName;
}

QString PersonDetail::previous2VolunteerFullName(QString format)
{
    return getFullName(this->previous2VolunteerFirstName(), this->previous2VolunteerLastName(), format);
}

QString PersonDetail::previous2AssistantFirstName() const
{
    return m_previous2AssistantFirstName;
}

QString PersonDetail::previous2AssistantLastName() const
{
    return m_previous2AssistantLastName;
}

QString PersonDetail::previous2AssistantFullName(QString format)
{
    return getFullName(this->previous2AssistantFirstName(), this->previous2AssistantLastName(), format);
}

bool PersonDetail::isUnavailable() const
{
    return m_isUnavailable;
}

bool PersonDetail::isMultiAssigned() const
{
    return m_isMultiAssigned;
}

int PersonDetail::weekOfAnyPreviousAssignment() const
{
    return m_weekOfAnyPreviousAssignment;
}

AssignmentInfo *PersonDetail::anyPreviousAssignmentInfo() const
{
    return m_anyPreviousAssignmentInfo;
}

int PersonDetail::weekOfAnyFollowingAssignment() const
{
    return m_weekOfAnyFollowingAssignment;
}

AssignmentInfo *PersonDetail::anyFollowingAssignmentInfo() const
{
    return m_anyFollowingAssignmentInfo;
}

bool PersonDetail::isFamilyMemberAssigned() const
{
    return m_isFamilyMemberAssigned;
}

bool PersonDetail::isSpeakerAwayInSameMonth() const
{
    return m_isSpeakerAwayInSameMonth;
}

int PersonDetail::assignmentCount() const
{
    return m_assignmentCount;
}

int PersonDetail::assignmentFrequencyRange() const
{
    return m_assignmentFrequencyRange;
}

int PersonDetail::timeRange() const
{
    return m_timeRange;
}

int PersonDetail::weeksIdle() const
{
    return qMax(qMin(qMin(weekOfAnyPreviousAssignment(), weekOfAnyFollowingAssignment()) - 1, 4), 0);
}

QString PersonDetail::getFullName(QString firstName, QString lastName, QString format)
{
    // FORMAT:
    // "FirstName LastName" = DEFAULT
    // "LastName FirstName"
    // "LastName, FirstName"
    if (firstName.isEmpty() && lastName.isEmpty())
        return "";
    format = format.replace("FirstName", "%1");
    format = format.replace("LastName", "%2");
    QString fullname = QString(format).arg(firstName).arg(lastName);
    return (fullname);
}

PersonDetailModel::PersonDetailModel(QObject *parent)
    : QAbstractTableModel(parent), assignmentInfos(&AssignmentInfos::Instance())
{
}

PersonDetailModel::~PersonDetailModel()
{
    qDeleteAll(personDetails);
    personDetails.clear();
}

int PersonDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return personDetails.empty() ? 0 : personDetails.count();
}

int PersonDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QHash<int, QByteArray> PersonDetailModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[PersonIdRole] = "personId";
    items[PersonFirstNameRole] = "personFirstName";
    items[PersonLastNameRole] = "personLastName";
    items[PersonFullNameRole] = "personFullName";
    items[PersonDisplayNameRole] = "personDisplayName";
    items[DateRole] = "date";
    items[YearRole] = "year";
    items[AssignmentInfoRole] = "assignmentInfo";
    items[AssignmentTypeRole] = "assignmentType";
    items[MeetingPartRole] = "meetingPart";
    items[ThemeRole] = "theme";
    items[CongregationNameRole] = "congregationName";
    items[AlphabetRole] = "alphabet";
    items[AssigneeFullNameRole] = "assigneeFullName";
    items[VolunteerFullNameRole] = "volunteerFullName";
    items[AssistantFullNameRole] = "assistantFullName";
    items[StudyPointRole] = "studyPoint";
    items[TimingRole] = "timing";
    items[NoteRole] = "note";
    items[Previous1DateRole] = "previous1Date";
    items[Previous1AssignmentInfoRole] = "previous1AssignmentInfo";
    items[Previous1AssignmentTypeRole] = "previous1AssignmentType";
    items[Previous1MeetingPartRole] = "previous1MeetingPart";
    items[Previous1ThemeRole] = "previous1Theme";
    items[Previous1CongregationNameRole] = "previous1CongregationName";
    items[Previous1AssigneeFullNameRole] = "previous1AssigneeFullName";
    items[Previous1VolunteerFullNameRole] = "previous1VolunteerFullName";
    items[Previous1AssistantFullNameRole] = "previous1AssistantFullName";
    items[Previous2DateRole] = "previous2Date";
    items[Previous2AssignmentInfoRole] = "previous2AssignmentInfo";
    items[Previous2AssignmentTypeRole] = "previous2AssignmentType";
    items[Previous2MeetingPartRole] = "previous2MeetingPart";
    items[Previous2ThemeRole] = "previous2Theme";
    items[Previous2CongregationNameRole] = "previous2CongregationName";
    items[Previous2AssigneeFullNameRole] = "previous2AssigneeFullName";
    items[Previous2VolunteerFullNameRole] = "previous2VolunteerFullName";
    items[Previous2AssistantFullNameRole] = "previous2AssistantFullName";
    items[IsUnavailableRole] = "isUnavailable";
    items[IsMultiAssignedRole] = "isMultiAssigned";
    items[WeekOfAnyPreviousAssignmentRole] = "weekOfAnyPreviousAssignment";
    items[AnyPreviousAssignmentInfoRole] = "anyPreviousAssignmentInfo";
    items[WeekOfAnyFollowingAssignmentRole] = "weekOfAnyFollowingAssignment";
    items[AnyFollowingAssignmentInfoRole] = "anyFollowingAssignmentInfo";
    items[IsFamilyMemberAssignedRole] = "isFamilyMemberAssigned";
    items[IsSpeakerAwayInSameMonth] = "isSpeakerAwayInSameMonth";
    items[AssignmentCountRole] = "assignmentCount";
    items[AssignmentFrequencyRangeRole] = "assignmentFrequencyRange";
    items[TimeRangeRole] = "timeRange";
    items[WeeksIdleRole] = "weeksIdle";
    return items;
}

QVariant PersonDetailModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row > personDetails.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return personDetails[index.row()]->personId();
        case 1:
            return personDetails[index.row()]->personFirstName();
        case 2:
            return personDetails[index.row()]->personLastName();
        case 3:
            return personDetails[index.row()]->personFullName();
        case 4:
            return personDetails[index.row()]->personFullName(displayNameFormat);
        case 5:
            return personDetails[index.row()]->date();
        case 6:
            return personDetails[index.row()]->date().year();
        case 7:
            return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->assignmentInfo());
        case 8:
            return personDetails[index.row()]->assignmentInfo() ? personDetails[index.row()]->assignmentInfo()->assignmentTypeName() : "";
        case 9:
            return personDetails[index.row()]->assignmentInfo() ? personDetails[index.row()]->assignmentInfo()->meetingPartName() : "";
        case 10:
            return personDetails[index.row()]->theme();
        case 11:
            return personDetails[index.row()]->congregationName();
        case 12:
            return personDetails[index.row()]->personFullName(displayNameFormat).left(1);
        case 13:
            return personDetails[index.row()]->assigneeFullName();
        case 14:
            return personDetails[index.row()]->volunteerFullName();
        case 15:
            return personDetails[index.row()]->assistantFullName();
        case 16:
            return personDetails[index.row()]->studyPoint();
        case 17:
            return personDetails[index.row()]->timing();
        case 18:
            return personDetails[index.row()]->note();
        case 19:
            return personDetails[index.row()]->previous1Date();
        case 20:
            return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->previous1AssignmentInfo());
        case 21:
            return personDetails[index.row()]->previous1AssignmentInfo() ? personDetails[index.row()]->previous1AssignmentInfo()->assignmentTypeName() : "";
        case 22:
            return personDetails[index.row()]->previous1AssignmentInfo() ? personDetails[index.row()]->previous1AssignmentInfo()->meetingPartName() : "";
        case 23:
            return personDetails[index.row()]->previous1Theme();
        case 24:
            return personDetails[index.row()]->previous1CongregationName();
        case 25:
            return personDetails[index.row()]->previous1AssigneeFullName();
        case 26:
            return personDetails[index.row()]->previous1VolunteerFullName();
        case 27:
            return personDetails[index.row()]->previous1AssistantFullName();
        case 28:
            return personDetails[index.row()]->previous2Date();
        case 29:
            return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->previous2AssignmentInfo());
        case 30:
            return personDetails[index.row()]->previous2AssignmentInfo() ? personDetails[index.row()]->previous2AssignmentInfo()->assignmentTypeName() : "";
        case 31:
            return personDetails[index.row()]->previous2AssignmentInfo() ? personDetails[index.row()]->previous2AssignmentInfo()->meetingPartName() : "";
        case 32:
            return personDetails[index.row()]->previous2Theme();
        case 33:
            return personDetails[index.row()]->previous2CongregationName();
        case 34:
            return personDetails[index.row()]->previous2AssigneeFullName();
        case 35:
            return personDetails[index.row()]->previous2VolunteerFullName();
        case 36:
            return personDetails[index.row()]->previous2AssistantFullName();
        case 37:
            return personDetails[index.row()]->isUnavailable();
        case 38:
            return personDetails[index.row()]->isMultiAssigned();
        case 39:
            return personDetails[index.row()]->weekOfAnyPreviousAssignment();
        case 40:
            return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->anyPreviousAssignmentInfo());
        case 41:
            return personDetails[index.row()]->weekOfAnyFollowingAssignment();
        case 42:
            return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->anyFollowingAssignmentInfo());
        case 43:
            return personDetails[index.row()]->isFamilyMemberAssigned();
        case 44:
            return personDetails[index.row()]->isSpeakerAwayInSameMonth();
        case 45:
            return personDetails[index.row()]->assignmentCount();
        case 46:
            return personDetails[index.row()]->assignmentFrequencyRange();
        case 47:
            return personDetails[index.row()]->timeRange();
        case 48:
            return personDetails[index.row()]->weeksIdle();
        }
    }

    switch (role) {
    case PersonIdRole:
        return personDetails[index.row()]->personId();
    case PersonFirstNameRole:
        return personDetails[index.row()]->personFirstName();
    case PersonLastNameRole:
        return personDetails[index.row()]->personLastName();
    case PersonFullNameRole:
        return personDetails[index.row()]->personFullName();
    case PersonDisplayNameRole:
        return personDetails[index.row()]->personFullName(displayNameFormat);
    case DateRole:
        return personDetails[index.row()]->date();
    case YearRole:
        return personDetails[index.row()]->date().year();
    case AssignmentInfoRole:
        return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->assignmentInfo());
    case AssignmentTypeRole:
        return personDetails[index.row()]->assignmentInfo() ? personDetails[index.row()]->assignmentInfo()->assignmentTypeName() : "";
    case MeetingPartRole:
        return personDetails[index.row()]->assignmentInfo() ? personDetails[index.row()]->assignmentInfo()->meetingPartName() : "";
    case ThemeRole:
        return personDetails[index.row()]->theme();
    case CongregationNameRole:
        return personDetails[index.row()]->congregationName();
    case AlphabetRole:
        return personDetails[index.row()]->personFullName(displayNameFormat).left(1);
    case AssigneeFullNameRole:
        return personDetails[index.row()]->assigneeFullName();
    case VolunteerFullNameRole:
        return personDetails[index.row()]->volunteerFullName();
    case AssistantFullNameRole:
        return personDetails[index.row()]->assistantFullName();
    case StudyPointRole:
        return personDetails[index.row()]->studyPoint();
    case TimingRole:
        return personDetails[index.row()]->timing();
    case NoteRole:
        return personDetails[index.row()]->note();
    case Previous1DateRole:
        return personDetails[index.row()]->previous1Date();
    case Previous1AssignmentInfoRole:
        return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->previous1AssignmentInfo());
    case Previous1AssignmentTypeRole:
        return personDetails[index.row()]->previous1AssignmentInfo() ? personDetails[index.row()]->previous1AssignmentInfo()->assignmentTypeName() : "";
    case Previous1MeetingPartRole:
        return personDetails[index.row()]->previous1AssignmentInfo() ? personDetails[index.row()]->previous1AssignmentInfo()->meetingPartName() : "";
    case Previous1ThemeRole:
        return personDetails[index.row()]->previous1Theme();
    case Previous1CongregationNameRole:
        return personDetails[index.row()]->previous1CongregationName();
    case Previous1AssigneeFullNameRole:
        return personDetails[index.row()]->previous1AssigneeFullName();
    case Previous1VolunteerFullNameRole:
        return personDetails[index.row()]->previous1VolunteerFullName();
    case Previous1AssistantFullNameRole:
        return personDetails[index.row()]->previous1AssistantFullName();
    case Previous2DateRole:
        return personDetails[index.row()]->previous2Date();
    case Previous2AssignmentInfoRole:
        return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->previous2AssignmentInfo());
    case Previous2AssignmentTypeRole:
        return personDetails[index.row()]->previous2AssignmentInfo() ? personDetails[index.row()]->previous2AssignmentInfo()->assignmentTypeName() : "";
    case Previous2MeetingPartRole:
        return personDetails[index.row()]->previous2AssignmentInfo() ? personDetails[index.row()]->previous2AssignmentInfo()->meetingPartName() : "";
    case Previous2ThemeRole:
        return personDetails[index.row()]->previous2Theme();
    case Previous2CongregationNameRole:
        return personDetails[index.row()]->previous2CongregationName();
    case Previous2AssigneeFullNameRole:
        return personDetails[index.row()]->previous2AssigneeFullName();
    case Previous2VolunteerFullNameRole:
        return personDetails[index.row()]->previous2VolunteerFullName();
    case Previous2AssistantFullNameRole:
        return personDetails[index.row()]->previous2AssistantFullName();
    case IsUnavailableRole:
        return personDetails[index.row()]->isUnavailable();
    case IsMultiAssignedRole:
        return personDetails[index.row()]->isMultiAssigned();
    case WeekOfAnyPreviousAssignmentRole:
        return personDetails[index.row()]->weekOfAnyPreviousAssignment();
    case AnyPreviousAssignmentInfoRole:
        return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->anyPreviousAssignmentInfo());
    case WeekOfAnyFollowingAssignmentRole:
        return personDetails[index.row()]->weekOfAnyFollowingAssignment();
    case AnyFollowingAssignmentInfoRole:
        return QVariant::fromValue<AssignmentInfo *>(personDetails[index.row()]->anyFollowingAssignmentInfo());
    case IsFamilyMemberAssignedRole:
        return personDetails[index.row()]->isFamilyMemberAssigned();
    case IsSpeakerAwayInSameMonth:
        return personDetails[index.row()]->isSpeakerAwayInSameMonth();
    case AssignmentCountRole:
        return personDetails[index.row()]->assignmentCount();
    case AssignmentFrequencyRangeRole:
        return personDetails[index.row()]->assignmentFrequencyRange();
    case TimeRangeRole:
        return personDetails[index.row()]->timeRange();
    case WeeksIdleRole:
        return personDetails[index.row()]->weeksIdle();
    default:
        return QVariant();
    }
}

QVariantMap PersonDetailModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

void PersonDetailModel::addPersonDetail(PersonDetail *personDetail)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    personDetails << personDetail;
    endInsertRows();
}

void PersonDetailModel::clear()
{
    beginResetModel();
    qDeleteAll(personDetails);
    personDetails.clear();
    m_personCount = 0;
    m_minAssignmentCount = 0;
    m_maxAssignmentCount = 0;
    endResetModel();
}

// load lmm assignments history
// param listType: 0 = one row per person with fields containing last few assignments
//                 1 = one row per assignment
// param personId: load assignments of the given person only
//                 0 = load all qualified persons' assignments (default)
// param includePersonId: include person, e.g. to display old selection
//                 0 = do not include anyone else
// param meetingType: type of meeting (e.g. weekend)
// param lmmClass: auxiliary class room
// param meetingPart: meeting part to choose assignees for
// param isAssistant: choose assistant for current meeting part
// param assistantToPersonId: person whom the meeting part is assigned to
// param loadNonStudentMeetingParts: load nonstudent assignments; default = true
// param loadOtherAssignments: load supplemental meeting assignments, e.g. reader or prayer; default = true
// param loadStudentParts: load student assignments; default = true
// param loadStudentPartAssistants: load assistants for student parts; default = true
// param loadSimilarAssignmentOnly: e.g. assistant with same student; default = false
// param date: for list type 0, the meeting date to check for alerts, statistics etc.
// param assignmentSubtype: assignment subtype (e.g. concluding prayer)
void PersonDetailModel::loadPersonDetails(int listType, int personId, int includePersonId, MeetingType meetingType, int lmmClass, MeetingPart meetingPart,
                                          bool isAssistant, int assistantToPersonId,
                                          bool loadWeekendMeetingParts,
                                          bool loadMidweekMeetingParts,
                                          bool loadNonStudentMeetingParts,
                                          bool loadOtherAssignments,
                                          bool loadStudentParts,
                                          bool loadStudentPartAssistants,
                                          bool loadSimilarAssignentsOnly,
                                          QDate date,
                                          AssignmentSubtypeClass::Subtype assignmentSubtype)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    if (meetingPart == MeetingPart::Service_Talk) {
        beginResetModel();
        qDeleteAll(personDetails);
        personDetails.clear();
        QString co = sql->getSetting("circuitoverseer");
        addPersonDetail(new PersonDetail(co));
        m_personCount = 0;
        m_minAssignmentCount = 0;
        m_maxAssignmentCount = 0;
        endResetModel();
        return;
    }

    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(meetingType, meetingPart, isAssistant, lmmClass, assignmentSubtype);
    if (assignmentInfo == nullptr) {
        qDebug() << "Coulnd't find assignment info for meeting type =" << MeetingTypeClass::toString(meetingType) << ", meeting part =" << MeetingPartClass::toString(meetingPart) << ", assistant =" << isAssistant << ", class =" << lmmClass;
        return;
    }

    // create join criteria to filter details according to requested meeting parts
    QList<MeetingType> meetingTypes;
    if (loadWeekendMeetingParts)
        meetingTypes.append(MeetingType::WeekendMeeting);
    if (loadMidweekMeetingParts)
        meetingTypes.append(MeetingType::MidweekMeeting);
    QList<AssignmentCategory> assigneeAssignmentCategories;
    QList<AssignmentCategory> assistantAssignmentCategories;
    if (loadNonStudentMeetingParts)
        assigneeAssignmentCategories.append(AssignmentCategory::NonstudentMeetingPart);
    if (loadOtherAssignments)
        assistantAssignmentCategories.append(AssignmentCategory::OtherAssignment);
    if (loadStudentParts)
        assigneeAssignmentCategories.append(AssignmentCategory::StudentPart);
    if (loadStudentPartAssistants)
        assistantAssignmentCategories.append(AssignmentCategory::StudentPartAssistant);
    QString assigneeTalkIds = getDbTalkIds(meetingTypes,
                                           assigneeAssignmentCategories,
                                           assignmentInfo->assignmentType() != AssignmentType::Assistant && loadSimilarAssignentsOnly
                                                   ? assignmentInfo->assignmentType()
                                                   : AssignmentType::None); // not filtered in case of assistants, since similar means 'with same student'
    QString assistantTalkIds = getDbTalkIds(meetingTypes,
                                            assistantAssignmentCategories,
                                            assignmentInfo->assignmentType() != AssignmentType::Assistant && loadSimilarAssignentsOnly
                                                    ? assignmentInfo->assignmentType()
                                                    : AssignmentType::None); // not filtered in case of assistants, since similar means 'with same student'
    QString assignmentQuery;
    if (!assigneeTalkIds.isEmpty() || !assistantTalkIds.isEmpty())
        assignmentQuery = QString("%1%2%3") // assignee or assistant details
                                  .arg(assigneeTalkIds.isEmpty() ? "" : QString("((p.id = h.assignee_id OR p.id = h.volunteer_id) AND talk_id/10 in (%1))").arg(assigneeTalkIds))
                                  .arg(!assigneeTalkIds.isEmpty() && !assistantTalkIds.isEmpty() ? " OR " : "")
                                  .arg(assistantTalkIds.isEmpty() ? "" : QString("(p.id = h.assistant_id AND talk_id/10 in (%1))").arg(assistantTalkIds));
    else
        assignmentQuery = " talk_id/10 IN ()"; // no details

    QString similarAssignmentQuery("");
    if (assignmentInfo->assignmentType() == AssignmentType::Assistant && loadSimilarAssignentsOnly) {
        // load assignments with same student
        similarAssignmentQuery = QString("AND ((a.assignee_id = %1 AND a.volunteer_id <= 0) OR a.volunteer_id = %1) AND a.assistant_id > 0").arg(assistantToPersonId);
    }

    // filter by qualified persons
    QStringList personIds;
    ccongregation c;
    QString genderCode("");
    int familyHeadId(0);
    if (assistantToPersonId > 0) {
        person *assistantToPerson = cpersons::getPerson(assistantToPersonId);
        genderCode = assistantToPerson ? (assistantToPerson->gender() == person::Male ? "B" : "S") : "";
        QSharedPointer<family> assistantToFamily(family::getPersonFamily(assistantToPersonId));
        familyHeadId = assistantToFamily ? assistantToFamily->getHeadId() : 0;
    }
    QDate meetingDate = date.addDays(c.getMeetingDay(assignmentInfo->meetingType(), date) - 1);

    Availability a;
    try {
        if (assignmentInfo->meetingType() == MeetingType::MidweekMeeting) {
            MidweekMeetingAvailabilityChecker checker(meetingDate, date);
            a = checker.GetAssignablePersons(lmmClass, assignmentInfo->meetingPart(), isAssistant, genderCode, familyHeadId);
        } else if (assignmentInfo->meetingType() == MeetingType::WeekendMeeting) {
            WeekendMeetingAvailabilityChecker checker(meetingDate, date);
            if (assignmentInfo->assignmentType() == AssignmentType::Talk)
                a = checker.GetPublicSpeaker();
            else
                a = checker.GetAssignablePersons(assignmentInfo->role(), assignmentSubtype);
        } else
            return;
    } catch (const UnknownTalkIException &e) {
        QMessageBox::critical(nullptr, "TheocBase", e.what());
        return;
    } catch (const UnknownAssignmentInfoIException &e) {
        QMessageBox::critical(nullptr, "TheocBase", e.what());
        return;
    }

    if (personId > 0) {
        // filter list of assignments explicitly by the given person
        std::vector<int> pIds;
        pIds.push_back(personId);
        a.FilterOnPersonIds(pIds);
    }
    for (int n = 0; n < a.Count(); ++n) {
        AvailabilityItem *ai = a.GetItem(n);
        personIds.append(QVariant(ai->Id).toString());
    }
    // check if the included person is assignable at all and add the id to the list, if not
    bool isIncludePersonUnassignable(!personIds.contains(QVariant(includePersonId).toString()));
    if (isIncludePersonUnassignable) {
        personIds.append(QVariant(includePersonId).toString());
    }

    QString sqlQuery;
    switch (listType) {
    case 0:
        sqlQuery = QString("SELECT"
                           " p.id person_id, p.firstname personFirstName, p.lastname personLastName, c.name personCongregationName,"
                           " (person_id = %2 AND %3) isUnassignable,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN date END) date,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN meetingType END) meetingType,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN talk_id END) talk_id,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN theme END) theme,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN congregationName END) congregationName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN classnumber END) classnumber,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN assignee_id END) assignee_id,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN assigneeFirstName END) assigneeFirstName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN assigneeLastName END) assigneeLastName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN volunteer_id END) volunteer_id,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN volunteerFirstName END) volunteerFirstName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN volunteerLastName END) volunteerLastName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN assistant_id END) assistant_id,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN assistantFirstName END) assistantFirstName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN assistantLastName END) assistantLastName,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN setting END) studyPoint,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN timing END) timing,"
                           " MIN(CASE WHEN assignmentRank = 1 THEN note END) note,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN date END) previous1_date,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN meetingType END) previous1_meetingType,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN talk_id END) previous1_talk_id,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN theme END) previous1_theme,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN congregationName END) previous1_congregationName,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN classnumber END) previous1_classnumber,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN assignee_id END) previous1_assignee_id,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN assigneeFirstName END) previous1_assigneeFirstName,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN assigneeLastName END) previous1_assigneeLastName,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN volunteer_id END) previous1_volunteer_id,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN volunteerFirstName END) previous1_volunteerFirstName,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN volunteerLastName END) previous1_volunteerLastName,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN assistant_id END) previous1_assistant_id,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN assistantFirstName END) previous1_assistantFirstName,"
                           " MIN(CASE WHEN assignmentRank = 2 THEN assistantLastName END) previous1_assistantLastName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN date END) previous2_date,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN meetingType END) previous2_meetingType,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN talk_id END) previous2_talk_id,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN theme END) previous2_theme,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN congregationName END) previous2_congregationName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN classnumber END) previous2_classnumber,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN assignee_id END) previous2_assignee_id,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN assigneeFirstName END) previous2_assigneeFirstName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN assigneeLastName END) previous2_assigneeLastName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN volunteer_id END) previous2_volunteer_id,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN volunteerFirstName END) previous2_volunteerFirstName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN volunteerLastName END) previous2_volunteerLastName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN assistant_id END) previous2_assistant_id,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN assistantFirstName END) previous2_assistantFirstName,"
                           " MIN(CASE WHEN assignmentRank = 3 THEN assistantLastName END) previous2_assistantLastName,"
                           " MAX(assignmentRank) assignmentCount "
                           "FROM persons p "
                           "LEFT JOIN congregations c ON p.congregation_id = c.id "
                           "LEFT JOIN ("
                           " SELECT p.id person_id,"
                           "  RANK () OVER (PARTITION BY p.id ORDER BY h.date DESC, h.talk_id) assignmentRank, h.*"
                           " FROM persons p"
                           " JOIN ("
                           " SELECT a.date, %6 meetingType, s.talk_id, s.theme, '%5' congregationName, a.classnumber,"
                           "  a.assignee_id, p1.firstname assigneeFirstName, p1.lastname assigneeLastName,"
                           "  a.volunteer_id, p2.firstname volunteerFirstName, p2.lastname volunteerLastName,"
                           "  a.assistant_id, p3.firstname assistantFirstName, p3.lastname assistantLastName,"
                           "  a.setting, a.timing, a.note"
                           "  FROM lmm_assignment a"
                           " LEFT JOIN persons p1 ON a.assignee_id = p1.id"
                           " LEFT JOIN persons p2 ON a.volunteer_id = p2.id"
                           " LEFT JOIN persons p3 ON a.assistant_id = p3.id"
                           " LEFT JOIN lmm_schedule s ON a.lmm_schedule_id = s.id"
                           " WHERE s.active = 1 AND a.date >= '%1' %4" // lmm assignments
                           " UNION ALL"
                           " SELECT m1.date, %6 meetingType, %8 talk_id, '%9' theme, '%5' congregationName, 1 classnumber,"
                           "  m1.chairman assignee_id, p1.firstname assigneeFirstName, p1.lastname assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  -1 assistant_id, '' assistantFirstName, '' assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM lmm_meeting m1"
                           " LEFT JOIN persons p1 ON m1.chairman = p1.id"
                           " WHERE m1.date >= '%1'" // lmm chairman
                           " UNION ALL"
                           " SELECT m2.date, %6 meetingType, %10 talk_id, '%11' theme, '%5' congregationName, 2 classnumber,"
                           "  m2.counselor2 assignee_id, p2.firstname assigneeFirstName, p2.lastname assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  -1 assistant_id, '' assistantFirstName, '' assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM lmm_meeting m2"
                           " LEFT JOIN persons p2 ON m2.counselor2 = p2.id"
                           " WHERE m2.date >= '%1'" // lmm counselor 2
                           " UNION ALL"
                           " SELECT m3.date, %6 meetingType, %10 talk_id, '%11' theme, '%5' congregationName, 3 classnumber,"
                           "  m3.counselor3 assignee_id, p3.firstname assigneeFirstName, p3.lastname assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  -1 assistant_id, '' assistantFirstName, '' assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM lmm_meeting m3"
                           " LEFT JOIN persons p3 ON m3.counselor3 = p3.id"
                           " WHERE m3.date >= '%1'" // lmm counselor 3
                           " UNION ALL"
                           " SELECT m4.date, %6 meetingType, %12 talk_id, '%13' theme, '%5' congregationName, 1 classnumber,"
                           "  -1 assignee_id, '' assigneeFirstName, '' assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  m4.prayer_beginning assistant_id, p4.firstname assistantFirstName, p4.lastname assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM lmm_meeting m4"
                           " LEFT JOIN persons p4 ON m4.prayer_beginning = p4.id"
                           " WHERE m4.date >= '%1'" // lmm opening prayer
                           " UNION ALL"
                           " SELECT m5.date, %6 meetingType, %14 talk_id, '%15' theme, '%5' congregationName, 1 classnumber,"
                           "  -1 assignee_id, '' assigneeFirstName, '' assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  m5.prayer_end assistant_id, p5.firstname assistantFirstName, p5.lastname assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM lmm_meeting m5"
                           " LEFT JOIN persons p5 ON m5.prayer_end = p5.id"
                           " WHERE m5.date >= '%1'" // concluding prayer
                           " UNION ALL"
                           " SELECT m6.date, %7 meetingType, %16 talk_id, '%17' theme, '%5' congregationName, 1 classnumber,"
                           "  m6.chairman_id assignee_id, p6.firstname assigneeFirstName, p6.lastname assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  -1 assistant_id, '' assistantFirstName, '' assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM publicmeeting m6"
                           " LEFT JOIN persons p6 ON m6.chairman_id = p6.id"
                           " WHERE m6.date >= '%1'" // pm chairman
                           " UNION ALL"
                           " SELECT m7.date, %7 meetingType, %18 talk_id, pt.theme_name theme, c7.name congregationName, 1 classnumber,"
                           "  m7.speaker_id assignee_id, p7.firstname assigneeFirstName, p7.lastname assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  -1 assistant_id, '' assistantFirstName, '' assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM publicmeeting m7"
                           " LEFT JOIN persons p7 ON m7.speaker_id = p7.id"
                           " LEFT JOIN congregations c7 ON p7.congregation_id = c7.id"
                           " LEFT JOIN publictalks pt ON m7.theme_id = pt.id"
                           " WHERE m7.date >= '%1'" // public talk
                           " UNION ALL"
                           " SELECT m8.date, %7 meetingType, %19 talk_id, '%20' theme, '%5' congregationName, 1 classnumber,"
                           "  m8.wt_conductor_id assignee_id, p8.firstname assigneeFirstName, p8.lastname assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  m8.wtreader_id assistant_id, p8a.firstname assistantFirstName, p8a.lastname assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM publicmeeting m8"
                           " LEFT JOIN persons p8 ON m8.wt_conductor_id = p8.id"
                           " LEFT JOIN persons p8a ON m8.wtreader_id = p8a.id"
                           " WHERE m8.date >= '%1'" // watchtower study
                           " UNION ALL"
                           " SELECT m9.date, %7 meetingType, %12 talk_id, '%13' theme, c9.name congregationName, 1 classnumber,"
                           "  -1 assignee_id, '' assigneeFirstName, '' assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  m9.opening_prayer_id assistant_id, p9.firstname assistantFirstName, p9.lastname assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM publicmeeting m9"
                           " LEFT JOIN persons p9 ON m9.opening_prayer_id = p9.id"
                           " LEFT JOIN congregations c9 ON p9.congregation_id = c9.id"
                           " WHERE m9.date >= '%1'" // pm opening prayer
                           " UNION ALL"
                           " SELECT m10.date, %7 meetingType, %14 talk_id, '%15' theme, c10.name congregationName, 1 classnumber,"
                           "  -1 assignee_id, '' assigneeFirstName, '' assigneeLastName,"
                           "  -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                           "  m10.final_prayer_id assistant_id, p10.firstname assistantFirstName, p10.lastname assistantLastName,"
                           "  '' setting, '' timing, '' note"
                           " FROM publicmeeting m10"
                           " LEFT JOIN persons p10 ON m10.final_prayer_id = p10.id"
                           " LEFT JOIN congregations c10 ON p10.congregation_id = c10.id"
                           " WHERE m10.date >= '%1'" // pm concluding prayer
                           ") h ON ")
                           .arg(date.addYears(-1).toString(Qt::ISODate))
                           .arg(includePersonId)
                           .arg(isIncludePersonUnassignable)
                           .arg(similarAssignmentQuery)
                           .arg(sql->EscapeQuotes(c.getMyCongregation().name))
                           .arg(MeetingTypeClass::toInt(MeetingType::MidweekMeeting))
                           .arg(MeetingTypeClass::toInt(MeetingType::WeekendMeeting))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::LMM_Chairman))
                           .arg(sql->EscapeQuotes(AssignmentSubtypeClass::toString(AssignmentSubtype::LMMChairman)))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::LMM_Counselor))
                           .arg(sql->EscapeQuotes(AssignmentTypeClass::toString(AssignmentType::Counselor)))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::SongAndPrayer))
                           .arg(sql->EscapeQuotes(AssignmentSubtypeClass::toString(AssignmentSubtype::OpeningPrayer)))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::SongAndPrayer, 1))
                           .arg(sql->EscapeQuotes(AssignmentSubtypeClass::toString(AssignmentSubtype::ConcludingPrayer)))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::PM_Chairman))
                           .arg(sql->EscapeQuotes(AssignmentSubtypeClass::toString(AssignmentSubtype::PMChairman)))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::PublicTalk))
                           .arg(MeetingPartClass::toDbTalkId(MeetingPart::WatchtowerStudy))
                           .arg(sql->EscapeQuotes(MeetingPartClass::toString(MeetingPart::WatchtowerStudy)));

        sqlQuery.append(assignmentQuery);

        sqlQuery.append(" ORDER BY p.id, assignmentRank) ON person_id = p.id "
                        "GROUP BY p.id ");
        if (personIds.count() > 0) {
            sqlQuery.append(QString("HAVING p.id in (%1) ").arg(personIds.join(",")));
        }

        break;
    default:
        sqlQuery = QString(" SELECT p.id person_id, p.firstname as personFirstName, p.lastname as personLastName, h.*"
                           " FROM persons p"
                           " JOIN (SELECT a.id assignment_id, a.lmm_schedule_id, a.date, s.talk_id, s.theme, a.classnumber,"
                           " a.assignee_id, p1.firstname as assigneeFirstName, p1.lastname as assigneeLastName,"
                           " a.volunteer_id, p2.firstname as volunteerFirstName, p2.lastname as volunteerLastName,"
                           " a.assistant_id, p3.firstname as assistantFirstName, p3.lastname as assistantLastName,"
                           " a.setting as studyPoint, a.timing, a.note,"
                           " '' as previous1_date, '' as previous1_theme, 0 as previous1_classnumber,"
                           " '' as previous1_assignee_id, '' as previous1_assigneeFirstName, '' as previous1_assigneeLastName,"
                           " '' as previous1_volunteer_id, '' as previous1_volunteerFirstName, '' as previous1_volunteerLastName,"
                           " '' as previous1_assistant_id, '' as previous1_assistantFirstName, '' as previous1_assistantLastName,"
                           " '' as previous2_date, '' as previous2_theme, 0 as previous2_classnumber,"
                           " '' as previous2_assignee_id, '' as previous2_assigneeFirstName, '' as previous2_assigneeLastName,"
                           " '' as previous2_volunteer_id, '' as previous2_volunteerFirstName, '' as previous2_volunteerLastName,"
                           " '' as previous2_assistant_id, '' as previous2_assistantFirstName, '' as previous2_assistantLastName,"
                           " 1 as assignmentCount"
                           " FROM lmm_assignment a "
                           "LEFT JOIN persons p1 ON a.assignee_id = p1.id "
                           "LEFT JOIN persons p2 ON a.volunteer_id = p2.id "
                           "LEFT JOIN persons p3 ON a.assistant_id = p3.id "
                           "LEFT JOIN lmm_schedule s ON a.lmm_schedule_id = s.id) h ON ");

        sqlQuery.append(assignmentQuery);
        sqlQuery.append(QString(" WHERE"));

        if (personIds.count() > 0)
            sqlQuery.append(QString(" person_id in (%1) AND ").arg(personIds.join(",")));

        sqlQuery.append("h.date >= '2016-01-01' ORDER BY h.date ASC");
        break;
    }

    sql_items assignmentRows = sql->selectSql(sqlQuery);
    updateModel(date, assignmentInfo->role(), assignmentRows, a);
}

QString PersonDetailModel::getDbTalkIds(QList<MeetingType> meetingTypes, QList<AssignmentCategory> assignmentCategories, AssignmentType assignmentType)
{
    QList<MeetingPart> meetingParts(assignmentInfos->getMeetingParts(meetingTypes, assignmentCategories, assignmentType));
    QString dbTalkIds;
    for (int i = 0; i < meetingParts.size(); i++)
        dbTalkIds += (i ? "," : "") + QVariant(static_cast<int>(meetingParts[i])).toString();
    return dbTalkIds;
}

// load local speakers with public talk assignments history
// param includePersonId: include person, e.g. to display old selection
//                 0 = do not include anyone else
// param themeId: load speakers, who deliver the given talk
// param date: the meeting date to check for alerts, statistics etc.
void PersonDetailModel::loadLocalPublicTalkPersonDetails(int includePersonId, int themeId,
                                                         QDate date)
{
    ccongregation c;
    int congregationId = c.getMyCongregation().id;
    loadPublicTalkPersonDetails(includePersonId, congregationId, themeId, date);
}

// load local and visiting speakers with public talk assignments history
// param includePersonId: include person, e.g. to display old selection
//                 0 = do not include anyone else
// param congregationId: load speakers from the given congregation only
// param themeId: load speakers, who deliver the given talk
// param date: the meeting date to check for alerts, statistics etc.
void PersonDetailModel::loadPublicTalkPersonDetails(int includePersonId, int congregationId, int themeId,
                                                    QDate date)
{
    sql_class *sql = &Singleton<sql_class>::Instance();

    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::WeekendMeeting, MeetingPart::PublicTalk);
    if (assignmentInfo == nullptr || !date.isValid())
        return;

    // filter by qualified persons
    ccongregation c;
    QDate weekCommencingDate = date.addDays(-date.dayOfWeek() + 1);
    QDate meetingDate = date.addDays(c.getMeetingDay(assignmentInfo->meetingType(), weekCommencingDate) - 1);
    WeekendMeetingAvailabilityChecker checker(meetingDate, weekCommencingDate);
    bool onlyLocal = c.getMyCongregation().id == congregationId;
    Availability a = onlyLocal ? checker.GetLocalPublicSpeaker(themeId) : checker.GetPublicSpeaker(congregationId, themeId);

    QStringList personIds;
    for (int n = 0; n < a.Count(); ++n) {
        AvailabilityItem *ai = a.GetItem(n);
        personIds.append(QVariant(ai->Id).toString());
    }
    // check if the included person is assignable at all and add the id to the list, if not
    bool isIncludePersonUnassignable(!personIds.contains(QVariant(includePersonId).toString()));
    if (isIncludePersonUnassignable) {
        personIds.append(QVariant(includePersonId).toString());
    }

    QString sqlQuery = QString("SELECT"
                               " p.id person_id, p.firstname personFirstName, p.lastname personLastName, c.name personCongregationName,"
                               " (person_id = %2 AND %3) isUnassignable,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN date END) date,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN meetingType END) meetingType,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN talk_id END) talk_id,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN theme END) theme,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN congregationName END) congregationName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN classnumber END) classnumber,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assignee_id END) assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assigneeFirstName END) assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assigneeLastName END) assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN volunteer_id END) volunteer_id,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN volunteerFirstName END) volunteerFirstName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN volunteerLastName END) volunteerLastName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assistant_id END) assistant_id,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assistantFirstName END) assistantFirstName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assistantLastName END) assistantLastName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN setting END) studyPoint,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN timing END) timing,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN note END) note,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN date END) previous1_date,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN meetingType END) previous1_meetingType,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN talk_id END) previous1_talk_id,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN theme END) previous1_theme,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN congregationName END) previous1_congregationName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN classnumber END) previous1_classnumber,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assignee_id END) previous1_assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assigneeFirstName END) previous1_assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assigneeLastName END) previous1_assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN volunteer_id END) previous1_volunteer_id,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN volunteerFirstName END) previous1_volunteerFirstName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN volunteerLastName END) previous1_volunteerLastName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assistant_id END) previous1_assistant_id,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assistantFirstName END) previous1_assistantFirstName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assistantLastName END) previous1_assistantLastName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN date END) previous2_date,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN meetingType END) previous2_meetingType,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN talk_id END) previous2_talk_id,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN theme END) previous2_theme,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN congregationName END) previous2_congregationName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN classnumber END) previous2_classnumber,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assignee_id END) previous2_assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assigneeFirstName END) previous2_assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assigneeLastName END) previous2_assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN volunteer_id END) previous2_volunteer_id,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN volunteerFirstName END) previous2_volunteerFirstName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN volunteerLastName END) previous2_volunteerLastName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assistant_id END) previous2_assistant_id,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assistantFirstName END) previous2_assistantFirstName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assistantLastName END) previous2_assistantLastName,"
                               " MAX(assignmentRank) assignmentCount "
                               "FROM persons p "
                               "LEFT JOIN congregations c ON p.congregation_id = c.id "
                               "LEFT JOIN ("
                               " SELECT p.id person_id,"
                               "  RANK () OVER (PARTITION BY p.id ORDER BY h.date DESC, h.talk_id) assignmentRank, h.*"
                               " FROM persons p"
                               " JOIN ("
                               " SELECT m1.date, %4 meetingType, %5 talk_id, pt.theme_name theme, c1.name congregationName, 1 classnumber,"
                               " m1.speaker_id assignee_id, p1.firstname assigneeFirstName, p1.lastname assigneeLastName,"
                               " -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                               " -1 assistant_id, '' assistantFirstName, '' assistantLastName,"
                               " '' setting, '' timing, '' note"
                               " FROM publicmeeting m1"
                               " LEFT JOIN persons p1 ON m1.speaker_id = p1.id"
                               " LEFT JOIN congregations c1 ON p1.congregation_id = c1.id"
                               " LEFT JOIN publictalks pt ON m1.theme_id = pt.id"
                               " WHERE m1.date >= '%1'" // public talk
                               ") h ON p.id = h.assignee_id ")
                               .arg(date.addYears(-10).toString(Qt::ISODate))
                               .arg(includePersonId)
                               .arg(isIncludePersonUnassignable)
                               .arg(MeetingTypeClass::toInt(MeetingType::WeekendMeeting))
                               .arg(MeetingPartClass::toDbTalkId(MeetingPart::PublicTalk));

    sqlQuery.append(" ORDER BY p.id, assignmentRank) ON person_id = p.id "
                    "GROUP BY p.id ");
    if (personIds.count() > 0) {
        sqlQuery.append(QString("HAVING p.id in (%1) ").arg(personIds.join(",")));
    }

    sql_items assignmentRows = sql->selectSql(sqlQuery);
    updateModel(date, assignmentInfo->role(), assignmentRows, a);
}

// load hospitality assignments history
// param includePersonId: include person, e.g. to display old selection
//                 0 = do not include anyone else
// param date: the meeting date to check for alerts, statistics etc.
void PersonDetailModel::loadHospitalityPersonDetails(int includePersonId, QDate date)
{
    sql_class *sql = &Singleton<sql_class>::Instance();

    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::WeekendMeeting, MeetingPart::PublicTalk, true);
    if (assignmentInfo == nullptr)
        return;

    // filter by qualified persons
    ccongregation c;

    QDate meetingDate = date.addDays(c.getMeetingDay(assignmentInfo->meetingType(), date) - 1);

    WeekendMeetingAvailabilityChecker checker(meetingDate, date);
    Availability a = checker.GetAssignablePersons(assignmentInfo->role());

    QStringList personIds;
    for (int n = 0; n < a.Count(); ++n) {
        AvailabilityItem *ai = a.GetItem(n);
        personIds.append(QVariant(ai->Id).toString());
    }
    // check if the included person is assignable at all and add the id to the list, if not
    bool isIncludePersonUnassignable(!personIds.contains(QVariant(includePersonId).toString()));
    if (isIncludePersonUnassignable) {
        personIds.append(QVariant(includePersonId).toString());
    }

    QString sqlQuery;
    sqlQuery = QString("SELECT"
                       " p.id person_id, p.firstname personFirstName, p.lastname personLastName,"
                       " (person_id = %2 AND %3) isUnassignable,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN date END) date,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN meetingType END) meetingType,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN talk_id END) talk_id,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN theme END) theme,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN congregationName END) congregationName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN classnumber END) classnumber,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN assignee_id END) assignee_id,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN assigneeFirstName END) assigneeFirstName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN assigneeLastName END) assigneeLastName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN volunteer_id END) volunteer_id,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN volunteerFirstName END) volunteerFirstName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN volunteerLastName END) volunteerLastName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN assistant_id END) assistant_id,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN assistantFirstName END) assistantFirstName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN assistantLastName END) assistantLastName,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN setting END) studyPoint,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN timing END) timing,"
                       " MIN(CASE WHEN assignmentRank = 1 THEN note END) note,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN date END) previous1_date,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN meetingType END) previous1_meetingType,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN talk_id END) previous1_talk_id,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN theme END) previous1_theme,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN congregationName END) previous1_congregationName,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN classnumber END) previous1_classnumber,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN assignee_id END) previous1_assignee_id,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN assigneeFirstName END) previous1_assigneeFirstName,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN assigneeLastName END) previous1_assigneeLastName,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN volunteer_id END) previous1_volunteer_id,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN volunteerFirstName END) previous1_volunteerFirstName,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN volunteerLastName END) previous1_volunteerLastName,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN assistant_id END) previous1_assistant_id,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN assistantFirstName END) previous1_assistantFirstName,"
                       " MIN(CASE WHEN assignmentRank = 2 THEN assistantLastName END) previous1_assistantLastName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN date END) previous2_date,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN meetingType END) previous2_meetingType,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN talk_id END) previous2_talk_id,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN theme END) previous2_theme,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN congregationName END) previous2_congregationName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN classnumber END) previous2_classnumber,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN assignee_id END) previous2_assignee_id,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN assigneeFirstName END) previous2_assigneeFirstName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN assigneeLastName END) previous2_assigneeLastName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN volunteer_id END) previous2_volunteer_id,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN volunteerFirstName END) previous2_volunteerFirstName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN volunteerLastName END) previous2_volunteerLastName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN assistant_id END) previous2_assistant_id,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN assistantFirstName END) previous2_assistantFirstName,"
                       " MIN(CASE WHEN assignmentRank = 3 THEN assistantLastName END) previous2_assistantLastName,"
                       " MAX(assignmentRank) assignmentCount "
                       "FROM persons p "
                       "LEFT JOIN ("
                       " SELECT p.id person_id,"
                       "  RANK () OVER (PARTITION BY p.id ORDER BY h.date DESC, h.talk_id) assignmentRank, h.*"
                       " FROM persons p"
                       " JOIN ("
                       " SELECT m1.date, %4 meetingType, %5 talk_id, '%6' theme, c1.name congregationName, 1 classnumber,"
                       " m1.speaker_id assignee_id, p1.firstname assigneeFirstName, p1.lastname assigneeLastName,"
                       " -1 volunteer_id, '' volunteerFirstName, '' volunteerLastName,"
                       " m1.hospitality_id assistant_id, p2.firstname assistantFirstName, p2.lastname assistantLastName,"
                       " '' setting, '' timing, '' note"
                       " FROM publicmeeting m1"
                       " LEFT JOIN persons p1 ON m1.speaker_id = p1.id"
                       " LEFT JOIN congregations c1 ON p1.congregation_id = c1.id"
                       " LEFT JOIN persons p2 ON m1.hospitality_id = p2.id"
                       " WHERE m1.date >= '%1'" // pm hospitality
                       ") h ON p.id = h.assistant_id")
                       .arg(date.addYears(-1).toString(Qt::ISODate))
                       .arg(includePersonId)
                       .arg(isIncludePersonUnassignable)
                       .arg(MeetingTypeClass::toInt(MeetingType::WeekendMeeting))
                       .arg(MeetingPartClass::toDbTalkId(MeetingPart::PublicTalk))
                       .arg(sql->EscapeQuotes(AssignmentTypeClass::toString(AssignmentType::Hospitality)));
    sqlQuery.append(" ORDER BY p.id, assignmentRank) ON person_id = p.id "
                    "GROUP BY p.id ");
    if (personIds.count() > 0) {
        sqlQuery.append(QString("HAVING p.id in (%1) ").arg(personIds.join(",")));
    }

    sql_items assignmentRows = sql->selectSql(sqlQuery);
    updateModel(date, assignmentInfo->role(), assignmentRows, a);
}

void PersonDetailModel::updateModel(QDate date, person::UseFor role, const sql_items &assignmentRows, Availability &a)
{
    beginResetModel();
    qDeleteAll(personDetails);
    personDetails.clear();

    // update name display order
    sql_class *sql = &Singleton<sql_class>::Instance();
    displayNameFormat = sql->getSetting("nameFormat", "%2, %1");

    if (!assignmentRows.empty()) {
        QVector<int> personIds;

        // collect statistical data
        int minAssignmentCount = 999;
        int maxAssignmentCount = 0;
        for (unsigned int i = 0; i < assignmentRows.size(); i++) {
            sql_item s = assignmentRows[i];
            if (!personIds.contains(s.value("person_id").toInt()))
                personIds.append(s.value("person_id").toInt());
            minAssignmentCount = qMin(minAssignmentCount, s.value("assignmentCount").toInt());
            maxAssignmentCount = qMax(maxAssignmentCount, s.value("assignmentCount").toInt());
        }

        m_personCount = personIds.count();
        m_minAssignmentCount = minAssignmentCount;
        m_maxAssignmentCount = maxAssignmentCount;
        int minTimeRange = 1;
        int maxTimeRange = 5;
        double timeRangeUnit = 7; // days

        ccongregation cc;
        for (unsigned int i = 0; i < assignmentRows.size(); i++) {
            sql_item s = assignmentRows[i];
            int person_id = s.value("person_id").toInt();

            MeetingType meetingType = MeetingTypeClass::fromInt(s.value("meetingType").toInt());
            MeetingPart talkId = MeetingPartClass::fromInt(s.value("talk_id").toInt() / 10);
            bool isAssistant = person_id == s.value("assistant_id").toInt();
            int roomNr = s.value("classnumber").toInt();
            AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(meetingType, talkId, isAssistant, roomNr);
            MeetingType previous1MeetingType = MeetingTypeClass::fromInt(s.value("previous1_meetingType").toInt());
            MeetingPart previous1TalkId = MeetingPartClass::fromInt(s.value("previous1_talk_id").toInt() / 10);
            bool isPrevious1Assistant = person_id == s.value("previous1_assistant_id").toInt();
            int previous1RoomNr = s.value("previous1_classnumber").toInt();
            AssignmentInfo *previous1AssignmentInfo = assignmentInfos->findAssignmentInfo(previous1MeetingType, previous1TalkId, isPrevious1Assistant, previous1RoomNr);
            MeetingType previous2MeetingType = MeetingTypeClass::fromInt(s.value("previous2_meetingType").toInt());
            MeetingPart previous2TalkId = MeetingPartClass::fromInt(s.value("previous2_talk_id").toInt() / 10);
            bool isPrevious2Assistant = person_id == s.value("previous2_assistant_id").toInt();
            int previous2RoomNr = s.value("previous2_classnumber").toInt();
            AssignmentInfo *previous2AssignmentInfo = assignmentInfos->findAssignmentInfo(previous2MeetingType, previous2TalkId, isPrevious2Assistant, previous2RoomNr);

            // alerts
            bool isUnavailable = s.value("isUnassignable").toBool();
            bool isMultiAssigned(false);
            int weekOfAnyPreviousAssignment(0);
            AssignmentInfo *anyPreviousAssignmentInfo = nullptr;
            int weekOfAnyFollowingAssignment(0);
            AssignmentInfo *anyFollowingAssignmentInfo = nullptr;
            bool isSpeakerAwayInSameMonth(false);
            bool isFamilyMemberAssigned(false);
            AvailabilityItem *ai = a.GetItemByPersonId(person_id);
            if (ai) {
                isUnavailable = isUnavailable || ai->OnHoliday || ai->IsBreak;
                isMultiAssigned = ai->HasAssignmentsOtherThan(role, date);
                ai->GetClosestAssignmentsInOtherWeeks(date, weekOfAnyPreviousAssignment, anyPreviousAssignmentInfo, weekOfAnyFollowingAssignment, anyFollowingAssignmentInfo, 5);
                isSpeakerAwayInSameMonth = ai->IsSpeakerAwayInSameMonth;
                isFamilyMemberAssigned = ai->IsFamilyMemberAssigned;
            }

            int assignmentFrequencyRange = s.value("assignmentCount").isNull()
                    ? 0
                    : qCeil((s.value("assignmentCount").toDouble() - qMax(1, minAssignmentCount) + 1) / (maxAssignmentCount - qMax(1, minAssignmentCount) + 1) * 5);
            QDate lastDate = s.value("date").toDate();
            int timeRange = lastDate.isValid()
                    ? qFloor((double)lastDate.daysTo(date) / timeRangeUnit)
                    : 5;
            timeRange = qMax(timeRange, minTimeRange);
            timeRange = qMin(timeRange, maxTimeRange);

            QDate meetingDate = s.value("date").toDate();
            if (meetingDate.isValid() && assignmentInfo)
                meetingDate = meetingDate.addDays(cc.getMeetingDay(assignmentInfo->meetingType(), meetingDate) - 1);
            QDate previous1_meetingDate = s.value("previous1_date").toDate();
            if (previous1_meetingDate.isValid() && previous1AssignmentInfo)
                previous1_meetingDate = previous1_meetingDate.addDays(cc.getMeetingDay(previous1AssignmentInfo->meetingType(), previous1_meetingDate) - 1);
            QDate previous2_meetingDate = s.value("previous2_date").toDate();
            if (previous2_meetingDate.isValid() && previous2AssignmentInfo)
                previous2_meetingDate = previous2_meetingDate.addDays(cc.getMeetingDay(previous2AssignmentInfo->meetingType(), previous2_meetingDate) - 1);

            QString theme = s.value("theme").toString();
            if (theme.isEmpty())
                theme = " ";
            QString previous1Theme = s.value("previous1_theme").toString();
            if (previous1Theme.isEmpty())
                previous1Theme = " ";
            QString previous2Theme = s.value("previous2_theme").toString();
            if (previous2Theme.isEmpty())
                previous2Theme = " ";
            QString congregationName = s.value("congregationName").toString();
            if (congregationName.isEmpty()) // speakers who didn't give a talk yet
                congregationName = s.value("personCongregationName").toString();
            addPersonDetail(new PersonDetail(s.value("person_id").toInt(),
                                             s.value("personFirstName").toString(),
                                             s.value("personLastName").toString(),
                                             meetingDate,
                                             assignmentInfo,
                                             theme,
                                             congregationName,
                                             s.value("assignee_id").toInt(),
                                             s.value("assigneeFirstName").toString(),
                                             s.value("assigneeLastName").toString(),
                                             s.value("volunteer_id").toInt(),
                                             s.value("volunteerFirstName").toString(),
                                             s.value("volunteerLastName").toString(),
                                             s.value("assistant_id").toInt(),
                                             s.value("assistantFirstName").toString(),
                                             s.value("assistantLastName").toString(),
                                             s.value("studyPoint").toString(),
                                             s.value("timing").toString(),
                                             s.value("note").toString(),
                                             previous1_meetingDate,
                                             previous1AssignmentInfo,
                                             previous1Theme,
                                             s.value("previous1_congregationName").toString(),
                                             s.value("previous1_assignee_id").toInt(),
                                             s.value("previous1_assigneeFirstName").toString(),
                                             s.value("previous1_assigneeLastName").toString(),
                                             s.value("previous1_volunteer_id").toInt(),
                                             s.value("previous1_volunteerFirstName").toString(),
                                             s.value("previous1_volunteerLastName").toString(),
                                             s.value("previous1_assistant_id").toInt(),
                                             s.value("previous1_assistantFirstName").toString(),
                                             s.value("previous1_assistantLastName").toString(),
                                             previous2_meetingDate,
                                             previous2AssignmentInfo,
                                             previous2Theme,
                                             s.value("previous2_congregationName").toString(),
                                             s.value("previous2_assignee_id").toInt(),
                                             s.value("previous2_assigneeFirstName").toString(),
                                             s.value("previous2_assigneeLastName").toString(),
                                             s.value("previous2_volunteer_id").toInt(),
                                             s.value("previous2_volunteerFirstName").toString(),
                                             s.value("previous2_volunteerLastName").toString(),
                                             s.value("previous2_assistant_id").toInt(),
                                             s.value("previous2_assistantFirstName").toString(),
                                             s.value("previous2_assistantLastName").toString(),
                                             isUnavailable,
                                             isMultiAssigned,
                                             weekOfAnyPreviousAssignment,
                                             anyPreviousAssignmentInfo,
                                             weekOfAnyFollowingAssignment,
                                             anyFollowingAssignmentInfo,
                                             isFamilyMemberAssigned,
                                             isSpeakerAwayInSameMonth,
                                             s.value("assignmentCount").toInt(),
                                             assignmentFrequencyRange,
                                             timeRange));
        }
    } else {
        m_personCount = 0;
        m_minAssignmentCount = 0;
        m_maxAssignmentCount = 0;
    }
    endResetModel();
}

PersonDetailSFProxyModel::PersonDetailSFProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *PersonDetailSFProxyModel::source() const
{
    return sourceModel();
}

void PersonDetailSFProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
}

QString PersonDetailSFProxyModel::filterText() const
{
    return m_filterText;
}

void PersonDetailSFProxyModel::setFilterText(QString value)
{
    m_filterText = value;
    invalidateFilter();
    emit filterChanged();
}

QByteArray PersonDetailSFProxyModel::sortRole() const
{
    return roleNames().value(QSortFilterProxyModel::sortRole());
}

void PersonDetailSFProxyModel::setSortRole(const QByteArray &role)
{
    QSortFilterProxyModel::setSortRole(roleKey(role));
    emit sortChanged();
}

QByteArray PersonDetailSFProxyModel::groupByRole() const
{
    return m_groupByRole;
}

void PersonDetailSFProxyModel::setGroupByRole(const QByteArray &role)
{
    beginResetModel();
    m_groupByRole = role;
    endResetModel();
    emit groupByChanged();
}

bool PersonDetailSFProxyModel::areUnavailablesHidden() const
{
    return m_areUnavailablesHidden;
}

void PersonDetailSFProxyModel::setAreUnavailablesHidden(bool newAreUnavailablesHidden)
{
    if (m_areUnavailablesHidden == newAreUnavailablesHidden)
        return;
    m_areUnavailablesHidden = newAreUnavailablesHidden;
    invalidateFilter();
    emit areUnavailablesHiddenChanged();
}

bool PersonDetailSFProxyModel::filterAcceptsRow(int sourceRow,
                                                const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return (!areUnavailablesHidden() || !sourceModel()->data(index, PersonDetailModel::Roles::IsUnavailableRole).toBool())
            && (sourceModel()->data(index, PersonDetailModel::Roles::PersonDisplayNameRole).toString().contains(filterText(), Qt::CaseInsensitive));
    //            || sourceModel()->data(index, PersonDetailModel::Roles::AssigneeFullNameRole).toString().contains(filterText(), Qt::CaseInsensitive)
    //            || sourceModel()->data(index, PersonDetailModel::Roles::AssistantFullNameRole).toString().contains(filterText(), Qt::CaseInsensitive)
    //            || sourceModel()->data(index, PersonDetailModel::Roles::VolunteerFullNameRole).toString().contains(filterText(), Qt::CaseInsensitive)
    //            || sourceModel()->data(index, PersonDetailModel::Roles::ThemeRole).toString().contains(filterText(), Qt::CaseInsensitive));
}

bool PersonDetailSFProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    int inSectionSortMode = 0; // sort by date
    // compare sections first
    switch (roleKey(groupByRole())) {
    case PersonDetailModel::Roles::AlphabetRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::PersonDisplayNameRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::PersonDisplayNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        break;
    }
    case PersonDetailModel::Roles::MeetingPartRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::MeetingPartRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::MeetingPartRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        inSectionSortMode = 1; // sort by name within sections
        break;
    }
    case PersonDetailModel::Roles::AssignmentTypeRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::AssignmentTypeRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::AssignmentTypeRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        inSectionSortMode = 1; // sort by name within sections
        break;
    }
    case PersonDetailModel::Roles::ThemeRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::ThemeRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::ThemeRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        inSectionSortMode = 1; // sort by name within sections
        break;
    }
    case PersonDetailModel::Roles::CongregationNameRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::CongregationNameRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::CongregationNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        inSectionSortMode = 1; // sort by name within sections
        break;
    }
    case PersonDetailModel::Roles::StudyPointRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::StudyPointRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::StudyPointRole);
        if (!val1.isNull() && !val2.isNull()) {
            int studyPoint1 = val1.toString().split(" ")[0].toInt();
            int studyPoint2 = val2.toString().split(" ")[0].toInt();
            return studyPoint1 < studyPoint2;
        }
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        break;
    }
    case PersonDetailModel::Roles::AssignmentFrequencyRangeRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::AssignmentCountRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::AssignmentCountRole);
        if (!val1.isNull() && !val2.isNull()) {
            int assignmentCount1 = val1.toInt();
            int assignmentCount2 = val2.toInt();
            return assignmentCount1 < assignmentCount2;
        }
        break;
    }
    case PersonDetailModel::Roles::WeeksIdleRole: {
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::WeeksIdleRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::WeeksIdleRole);
        if (!val1.isNull() && !val2.isNull()) {
            int weeksIdle1 = val1.toInt();
            int weeksIdle2 = val2.toInt();
            return weeksIdle1 < weeksIdle2;
        }
        break;
    }
    default:
        break;
    }

    if (inSectionSortMode == 1) {
        // sort by name within sections
        QVariant val1 = sourceModel()->data(left, PersonDetailModel::Roles::PersonDisplayNameRole);
        QVariant val2 = sourceModel()->data(right, PersonDetailModel::Roles::PersonDisplayNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
    }
    // sort by date within sections
    QVariant leftData = sourceModel()->data(left, PersonDetailModel::Roles::DateRole);
    QVariant rightData = sourceModel()->data(right, PersonDetailModel::Roles::DateRole);
    if (leftData.userType() == QMetaType::QDate) {
        return leftData.toDate() < rightData.toDate();
    } else
        return leftData.toString() < rightData.toString();
}

int PersonDetailSFProxyModel::roleKey(const QByteArray &role) const
{
    QHash<int, QByteArray> roles = roleNames();
    QHashIterator<int, QByteArray> it(roles);
    while (it.hasNext()) {
        it.next();
        if (it.value() == role)
            return it.key();
    }
    return -1;
}
