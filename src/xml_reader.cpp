#include "xml_reader.h"

xml_reader::xml_reader(QString fileName) :
    quitRequest(false),
    fileName(fileName)
{
}

void xml_reader::register_elementsearch(QString name, int context, int searchContext, bool emitAllChildren)
{
    register_attributesearch(QXmlStreamReader::StartElement, name, nullptr, nullptr, context, searchContext, emitAllChildren);
}

void xml_reader::register_elementsearch(QXmlStreamReader::TokenType tokenType, QString name, int context, int searchContext, bool emitAllChildren)
{
    register_attributesearch(tokenType, name, nullptr, nullptr, context, searchContext, emitAllChildren);
}

void xml_reader::register_attributesearch(QString element_name, QString attribute_name, QString attribute_value, int context, int searchContext, bool emitAllChildren)
{
    register_attributesearch(QXmlStreamReader::StartElement, element_name, attribute_name, attribute_value, context, searchContext, emitAllChildren);
}

void xml_reader::register_attributesearch(QXmlStreamReader::TokenType tokenType, QString element_name, QString attribute_name, QString attribute_value, int context, int searchContext, bool emitAllChildren)
{
    if (!searches.contains(tokenType))
        searches.insert(tokenType, new QList<search*>());
    searches[tokenType]->append(new search(element_name, attribute_name, attribute_value, context, searchContext, emitAllChildren));
}

bool xml_reader::read()
{
    QFile* file = new QFile(fileName);
    if(!file->open(QIODevice::ReadOnly | QIODevice::Text)){
        // exception(QObject::tr("File reading failed") + " " + tocfilename);
        return true;
    }

    QXmlStreamReader xml(file);
    QString data;
    QList<search*> contextStack;
    QList<int> depthStack;
    int depth(0);
    contextStack.append(new search(nullptr, nullptr, nullptr, -1, -1, false));
    depthStack.append(0);
    search *currentContext = contextStack[0];
    int currentContextNumber(currentContext->context);
    while(!xml.atEnd() && !xml.hasError() && !quitRequest) {
        QXmlStreamReader::TokenType tokenType = xml.readNext();

        // for debugging
        QStringRef name = xml.name();
        QStringRef text = xml.text();

        //qDebug() << "";
        //qDebug() << "xml: start";

        if (tokenType == QXmlStreamReader::StartDocument || tokenType == QXmlStreamReader::StartElement)
            depth++;

        bool doEmit(currentContextNumber > -1 && currentContext->emitAllChildren);
        QList<search*> *searchType = searches[tokenType];
        //qDebug() << "xml: searchType ";
        if (searchType != nullptr)
        {
            //QList<search*> s2 = *(QList<search*>*)(s);
            QStringRef name = xml.name();
            foreach(search *s, *searchType)
            {
                if (
                        (s->searchContext < 0 || s->searchContext == currentContextNumber) &&
                        (s->element_name == nullptr || s->element_name == name) &&
                        (s->attribute_name == nullptr || xml.attributes().value(s->attribute_name) == s->attribute_value)
                    )
                {
                    if (tokenType != QXmlStreamReader::EndDocument && tokenType != QXmlStreamReader::EndElement)
                    {
                        currentContext = s;
                        currentContextNumber = currentContext->context;
                        contextStack.append(s);
                        depthStack.append(depth);
                    }
                    doEmit = true;
                    break;
                }
            }
        }
        if (doEmit)
            emit found(&xml, tokenType, currentContext->context, depth - depthStack.last());

        if (tokenType == QXmlStreamReader::EndDocument || tokenType == QXmlStreamReader::EndElement)
        {
            depth--;
            while (contextStack.length() > 1 && depthStack.last() > depth)
            {
                contextStack.removeLast();
                depthStack.removeLast();
            }
            currentContext = contextStack.last();
            currentContextNumber = currentContext->context;
        }
        //qDebug() << "xml: end";
    }

    file->close();
    delete file;

    return false;
}
