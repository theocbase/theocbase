#ifndef SQLCOMBOHELPER_H
#define SQLCOMBOHELPER_H
#include "lmm_schedule.h"
#include <QDate>
#include <QString>
#include <cassert>

namespace SQLCOMBO {
namespace sf_school {
enum sort_filter_1 {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
enum sort_filter_2 {
    sf_stdnts,
    sf_stdnts_dt_desc,
    sf_stdnts_assign,
    sf_stdnts_assign_dt_desc,
    sf_stdnts_assist,
    sf_stdnts_assist_dt_desc,
    sf_stdnts_together,
    sf_stdnts_together_dt_desc
};
} // namespace sf_school

namespace sf_cbs {
enum sort_filter_1 {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
enum sort_filter_2 {
    sf_any_part,
    sf_any_part_dt_desc,
    sf_current_part,
    sf_current_part_dt_desc,
    sf_conductor,
    sf_conductor_dt_desc,
    sf_reader,
    sf_reader_dt_desc,
};
} // namespace sf_cbs
namespace sf_localneed {
enum sort_filter_1 {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
enum sort_filter_2 {
    sf_any_part,
    sf_any_part_dt_desc,
    sf_current_part,
    sf_current_part_dt_desc,
};
} // namespace sf_localneed
namespace sf_talk {
enum sort_filter_1 {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
enum sort_filter_2 {
    sf_any_part,
    sf_any_part_dt_desc,
    sf_current_part,
    sf_current_part_dt_desc,
};
} // namespace sf_talk
namespace sf_prayeropen {
enum sort_filter_1 {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
enum sort_filter_2 {
    sf_any_part,
    sf_any_part_dt_desc,
    sf_current_part,
    sf_current_part_dt_desc,
};
} // namespace sf_prayeropen
namespace sf_prayerclose {
enum sort_filter_1 {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
enum sort_filter_2 {
    sf_any_part,
    sf_any_part_dt_desc,
    sf_current_part,
    sf_current_part_dt_desc,
};
} // namespace sf_prayerclose

class sf_variant
{
    int val;
    enum vtid_enum {
        sf_cbs_id,
        sf_localneed_id,
        sf_prayerclose_id,
        sf_prayeropen_id,
        sf_school_id,
        sf_talk_id,

    };
    vtid_enum tid;

public:
    void set(sf_cbs::sort_filter_2 x)
    {
        val = x;
        tid = sf_cbs_id;
    }
    void set(sf_localneed::sort_filter_2 x)
    {
        val = x;
        tid = sf_localneed_id;
    }
    void set(sf_prayerclose::sort_filter_2 x)
    {
        val = x;
        tid = sf_prayerclose_id;
    }
    void set(sf_prayeropen::sort_filter_2 x)
    {
        val = x;
        tid = sf_prayeropen_id;
    }
    void set(sf_school::sort_filter_2 x)
    {
        val = x;
        tid = sf_school_id;
    }
    void set(sf_talk::sort_filter_2 x)
    {
        val = x;
        tid = sf_talk_id;
    }

    sf_cbs::sort_filter_2 get(sf_cbs::sort_filter_2 &x) const
    {
        assert(tid == sf_cbs_id);
        x = static_cast<sf_cbs::sort_filter_2>(val);
        return x;
    }
    sf_localneed::sort_filter_2 get(sf_localneed::sort_filter_2 &x) const
    {
        assert(tid == sf_localneed_id);
        x = static_cast<sf_localneed::sort_filter_2>(val);
        return x;
    }
    sf_prayerclose::sort_filter_2 get(sf_prayerclose::sort_filter_2 &x) const
    {
        assert(tid == sf_prayerclose_id);
        x = static_cast<sf_prayerclose::sort_filter_2>(val);
        return x;
    }
    sf_prayeropen::sort_filter_2 get(sf_prayeropen::sort_filter_2 &x) const
    {
        assert(tid == sf_prayeropen_id);
        x = static_cast<sf_prayeropen::sort_filter_2>(val);
        return x;
    }
    sf_school::sort_filter_2 get(sf_school::sort_filter_2 &x) const
    {
        assert(tid == sf_school_id);
        x = static_cast<sf_school::sort_filter_2>(val);
        return x;
    }
    sf_talk::sort_filter_2 get(sf_talk::sort_filter_2 &x) const
    {
        assert(tid == sf_talk_id);
        x = static_cast<sf_talk::sort_filter_2>(val);
        return x;
    }
};

enum aggregate {
    aggr_closest_prefer_first,
    aggr_closest_prefer_last,
    aggr_closest
};

enum dt_mode { dt_mtg,
               dt_weekstart,
               dt_offset };
enum std_sort_filter {
    sf_last_name_asc,
    sf_first_name_asc,
    sf_last_selected_desc,
    sf_last_with_dt_desc
};
} // namespace SQLCOMBO

class SqlComboHelper
{
    SQLCOMBO::std_sort_filter filter1;
    SQLCOMBO::dt_mode date_mode;
    SQLCOMBO::sf_variant filter2;
    int cap;
    bool bro_only;
    bool school_assign;
    QString dt_str;
    QDate dt;
    int talk_num;
    int assisting_id;
    bool specific_assigned;
    bool specific_talk;
    bool specific_together;
    short classnum;

protected:
    QString get_midweek_sql(SQLCOMBO::aggregate) const;

public:
    SqlComboHelper();
    void clear() { }
    void set_sort_filter(SQLCOMBO::sf_school::sort_filter_1,
                         SQLCOMBO::sf_school::sort_filter_2);
    void set_sort_filter(SQLCOMBO::sf_cbs::sort_filter_1,
                         SQLCOMBO::sf_cbs::sort_filter_2);
    void set_sort_filter(SQLCOMBO::sf_localneed::sort_filter_1,
                         SQLCOMBO::sf_localneed::sort_filter_2);
    void set_sort_filter(SQLCOMBO::sf_talk::sort_filter_1,
                         SQLCOMBO::sf_talk::sort_filter_2);
    void set_sort_filter(SQLCOMBO::sf_prayeropen::sort_filter_1,
                         SQLCOMBO::sf_prayeropen::sort_filter_2);
    void set_sort_filter(SQLCOMBO::sf_prayerclose::sort_filter_1,
                         SQLCOMBO::sf_prayerclose::sort_filter_2);
    void set_dt_mode(SQLCOMBO::dt_mode x) { date_mode = x; }
    void set_assisting_id(int id)
    {
        assisting_id = id;
        if (assisting_id && talk_num != MeetingPartClass::toTalkId(MeetingPart::LMM_CBS)) {
            specific_together = true;
            specific_talk = false;
        }
    }
    void set_date(const QDate &x)
    {
        dt = x;
        dt_str = x.toString(Qt::ISODate);
    }
    void set_classnumber(short x) { classnum = x; }
    void set_talk_conductor(MeetingPart x);
    void set_only_brothers(bool x) { bro_only = x; }
    QString
    get_sql(SQLCOMBO::aggregate aggr = SQLCOMBO::aggr_closest_prefer_first) const;
};

#endif // SQLCOMBO_H
