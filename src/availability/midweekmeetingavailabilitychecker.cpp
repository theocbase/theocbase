#include "midweekmeetingavailabilitychecker.h"

using namespace tbAvailability;

namespace {
const QString DATA_SRC_LMM_ASSIGNMENTS = "LMMAssignments";
const QString DATA_SRC_LMM_MTG = "LMMMeeting";
const int LOOKBACK_WEEKS = 25; // number of historical weeks to analyse for "last used" date
const int LOOKAHEAD_WEEKS = 5; // number of future weeks to analyse for "last used" date
}

// MidweekMeetingAvailabilityChecker
//==================================

MidweekMeetingAvailabilityChecker::MidweekMeetingAvailabilityChecker(const QDate &meetingDate,
                                                                     const QDate &weekCommencingDate)
    : AvailabilityChecker(meetingDate,
                          weekCommencingDate,
                          std::vector<person::UseFor>({ person::LMM_Chairman, person::Prayer,
                                                        person::LMM_Treasures, person::LMM_Digging, person::LMM_BibleReading,
                                                        person::LMM_PreparePresentations, person::LMM_OtherVideoPart,
                                                        person::LMM_InitialCall, person::LMM_ReturnVisit, person::LMM_BibleStudy,
                                                        person::LMM_ApplyTalks,
                                                        person::LMM_LivingTalks,
                                                        person::CBSReader, person::CBSConductor,
                                                        person::Assistant }),
                          std::vector<QString>({ DATA_SRC_LMM_ASSIGNMENTS, DATA_SRC_LMM_MTG }))
{
}

Availability MidweekMeetingAvailabilityChecker::GetAssignablePersons(int lmmClass, MeetingPart meetingPart, bool isAssistant, QString genderCode, int familyHeadId)
{
    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, isAssistant, lmmClass);
    if (!assignmentInfo)
        throw new UnknownAssignmentInfoIException(MeetingType::MidweekMeeting, lmmClass, meetingPart, isAssistant);

    Availability result = GetLocal(assignmentInfo->role());
    if (assignmentInfo->assignmentCategory() == AssignmentCategory::StudentPart
        || assignmentInfo->assignmentCategory() == AssignmentCategory::StudentPartAssistant) {
        result.FilterOnPersonIds(GetPersonsForStudentTalks(lmmClass, genderCode, familyHeadId));
    }
    return result;
}

Availability MidweekMeetingAvailabilityChecker::GetAssignablePersons(person::UseFor assignment)
{
    Availability result = GetLocal(assignment);
    return result;
}

QString MidweekMeetingAvailabilityChecker::GenerateAssignedPersonsSql(QString dataSourceId)
{
    if (dataSourceId == DATA_SRC_LMM_ASSIGNMENTS) {
        // NOTE: db may benefit from an index on the "lmm_assignment.date" column if table is large

        QString s =
                "SELECT a.date, a.assignee_id, f1.family_head assignee_familyHead, a.assistant_id, f2.family_head assistant_familyHead, s.talk_id, a.classnumber "
                "FROM lmm_assignment a "
                "INNER JOIN lmm_schedule s ON a.lmm_schedule_id = s.id "
                "LEFT JOIN families f1 ON a.assignee_id = f1.person_id "
                "LEFT JOIN families f2 ON a.assistant_id = f2.person_id "
                "WHERE s.active = 1 AND a.date >= '%1' AND a.date <= '%2' "
                "ORDER BY a.date";

        return s.arg(weekCommencingDate_.addDays(LOOKBACK_WEEKS * -7).toString(Qt::ISODate),
                     weekCommencingDate_.addDays(LOOKAHEAD_WEEKS * 7).toString(Qt::ISODate));
    }

    else if (dataSourceId == DATA_SRC_LMM_MTG) {
        // NOTE: db may benefit from an index on the "lmm_assignment.date" column if table is large

        QString s =
                "SELECT m.date, m.chairman, f_ch.family_head chairman_familyHead, m.counselor2, f_ch.family_head counselor2_familyHead, m.counselor3, f_ch.family_head counselor3_familyHead, m.prayer_beginning, f_pb.family_head prayer_beginning_familyHead, m.prayer_end, f_pe.family_head prayer_end_familyHead "
                "FROM lmm_meeting m "
                "LEFT JOIN families f_ch ON m.chairman = f_ch.person_id "
                "LEFT JOIN families f_c2 ON m.counselor2 = f_c2.person_id "
                "LEFT JOIN families f_c3 ON m.counselor3 = f_c3.person_id "
                "LEFT JOIN families f_pb ON m.prayer_beginning = f_pb.person_id "
                "LEFT JOIN families f_pe ON m.prayer_end = f_pe.person_id "
                "WHERE m.active AND m.date <= '%1' "
                "ORDER BY m.date DESC LIMIT %2";

        return s.arg(weekCommencingDate_.addDays(LOOKAHEAD_WEEKS * 7).toString(Qt::ISODate),
                     QString::number(LOOKBACK_WEEKS + LOOKAHEAD_WEEKS));
    }

    return QString();
}

bool MidweekMeetingAvailabilityChecker::IsStudentAssignment(person::UseFor assignment)
{
    switch (assignment) {
    case person::LMM_BibleReading:
    case person::LMM_InitialCall:
    case person::LMM_ReturnVisit:
    case person::LMM_BibleStudy:
    case person::Assistant:
    case person::LMM_ApplyTalks:
        return true;
    default:
        return false;
    }
}

void MidweekMeetingAvailabilityChecker::PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons)
{
    int personId = result.Id;

    if (dataSourceId == DATA_SRC_LMM_ASSIGNMENTS) {
        foreach (sql_item mtgAssigments, assignedPersons) {
            QDate dt = mtgAssigments.value("date").toDate();
            int assigneeId = mtgAssigments.value("assignee_id").toInt();
            int assistantId = mtgAssigments.value("assistant_id").toInt();
            int dbTalkId = mtgAssigments.value("talk_id").toInt();
            int roomNr = mtgAssigments.value("classnumber").toInt();
            bool isSupplementary = assistantId == personId;
            MeetingPart meetingPart;
            int sequence(0);
            LMM_Schedule::splitDbTalkId(dbTalkId, meetingPart, sequence);

            if (assigneeId == personId || isSupplementary) {
                AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, isSupplementary, roomNr);
                if (!assignmentInfo)
                    throw UnknownTalkIException(dbTalkId, dt);
                result.AddHomeAssignment(dt, assignmentInfo);
            }
        }
    } else if (dataSourceId == DATA_SRC_LMM_MTG) {
        foreach (sql_item mtgAssigments, assignedPersons) {
            QDate dt = mtgAssigments.value("date").toDate();
            int chairmanId = mtgAssigments.value("chairman").toInt();
            int counselor2Id = mtgAssigments.value("counselor2").toInt();
            int counselor3Id = mtgAssigments.value("counselor3").toInt();
            int prayerStartId = mtgAssigments.value("prayer_beginning").toInt();
            int prayerEndId = mtgAssigments.value("prayer_end").toInt();
            bool isSupplementary(false);

            MeetingPart meetingPart(MeetingPart::None);
            if (chairmanId == personId || counselor2Id == personId || counselor3Id == personId) {
                meetingPart = MeetingPart::LMM_Chairman;
            }

            if (prayerStartId == personId || prayerEndId == personId) {
                meetingPart = MeetingPart::SongAndPrayer;
                isSupplementary = true;
            }

            if (meetingPart != MeetingPart::None) {
                AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, isSupplementary);
                if (!assignmentInfo)
                    throw UnknownAssignmentInfoIException(MeetingType::MidweekMeeting, 1, meetingPart, isSupplementary);
                result.AddHomeAssignment(dt, assignmentInfo);
            }
        }
    }

    result.IsFamilyMemberAssigned = HasFamilyMemberOtherAssignments(result, assignedPersons);
}

bool MidweekMeetingAvailabilityChecker::HasFamilyMemberOtherAssignments(AvailabilityItem &result, const sql_items &assignedPersons)
{
    if (result.IsFamilyMemberAssigned)
        return true;

    int personId = result.Id;
    int familyHeadId = result.FamilyHeadId;

    if (familyHeadId == 0) {
        return false;
    }

    auto iterator = std::find_if(assignedPersons.begin(), assignedPersons.end(),
                                 [this, &personId, &familyHeadId](const sql_item &assignedPerson) {
                                     return assignedPerson.value("date").toDate() == weekCommencingDate_
                                             && ((assignedPerson.value("assignee_id").toInt() != personId
                                                  && assignedPerson.value("assignee_familyHead").toInt() == familyHeadId)
                                                 || (assignedPerson.value("assistant_id").toInt() != personId
                                                     && assignedPerson.value("assistant_familyHead").toInt() == familyHeadId)
                                                 || (assignedPerson.value("chairman").toInt() != personId
                                                     && assignedPerson.value("chairman_familyHead").toInt() == familyHeadId)
                                                 || (assignedPerson.value("counselor2").toInt() != personId
                                                     && assignedPerson.value("counselor2_familyHead").toInt() == familyHeadId)
                                                 || (assignedPerson.value("counselor3").toInt() != personId
                                                     && assignedPerson.value("counselor3_familyHead").toInt() == familyHeadId)
                                                 || (assignedPerson.value("prayer_beginning").toInt() != personId
                                                     && assignedPerson.value("prayer_beginning_familyHead").toInt() == familyHeadId)
                                                 || (assignedPerson.value("prayer_end").toInt() != personId
                                                     && assignedPerson.value("prayer_end_familyHead").toInt() == familyHeadId));
                                 });
    return iterator != assignedPersons.end();
}
