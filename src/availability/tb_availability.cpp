#include "tb_availability.h"

using namespace tbAvailability;

// AvailabilityItem
//=================

AvailabilityItem::AvailabilityItem()
    : Id(0)
    , FamilyHeadId(0)
    , OnHoliday(false)
    , OutsideSpeaker(false)
    , IsFamilyMemberAssigned(false)
{
}

bool AvailabilityItem::operator<(const AvailabilityItem &rhs) const
{
    return QString::localeAwareCompare(DisplayName, rhs.DisplayName) < 0;
}

bool AvailabilityItem::HasRole(person::UseFor role)
{
    return (Roles & role) > 0;
}

void AvailabilityItem::AddHomeAssignment(QDate weekStartDate, AssignmentInfo *assignment)
{
    AssignmentsByDateI iter = AssignmentsAtHome.find(weekStartDate);
    if (iter == AssignmentsAtHome.end()) {
        SetOfAssignments assignments;
        assignments.insert(std::pair<person::UseFor, AssignmentInfo *>(assignment->role(), assignment));
        AssignmentsAtHome.insert(std::pair<QDate, SetOfAssignments>(weekStartDate, assignments));
    } else {
        iter->second.insert(std::pair<person::UseFor, AssignmentInfo *>(assignment->role(), assignment));
    }
}

bool AvailabilityItem::HasAssignmentsOtherThan(person::UseFor role, QDate weekCommencingDate)
{
    bool result = false;

    AssignmentsByDateI iter = AssignmentsAtHome.find(weekCommencingDate);
    if (iter != AssignmentsAtHome.end()) {
        SetOfAssignments &assignments = iter->second;
        int numAssignments = static_cast<int>(assignments.size());
        result = numAssignments > 1 || (numAssignments == 1 && assignments.find(role) == assignments.end());
    }

    return result;
}

bool AvailabilityItem::GetClosestAssignmentsInOtherWeeks(QDate weekCommencingDate, int &weekOfPreviousAssignment, AssignmentInfo *&previousAssignmentInfo, int &weekOfFollowingAssignment, AssignmentInfo *&followingAssignmentInfo, int weeks)
{
    weekOfPreviousAssignment = INT_MAX;
    previousAssignmentInfo = nullptr;
    weekOfFollowingAssignment = INT_MAX;
    followingAssignmentInfo = nullptr;
    for (int i = 1; i <= weeks; ++i) {
        for (int j : { -1, 1 }) { // look before and after current week
            if ((j < 0 && weekOfPreviousAssignment < i) || (j > 0 && weekOfFollowingAssignment < i))
                continue;
            QDate date = weekCommencingDate.addDays(i * j * 7);
            AssignmentsByDateI iter = AssignmentsAtHome.find(date);
            if (iter != AssignmentsAtHome.end()) {
                SetOfAssignments &assignments = iter->second;
                int numAssignments = static_cast<int>(assignments.size());
                if (numAssignments > 0) {
                    if (j < 0)
                        weekOfPreviousAssignment = i;
                    else
                        weekOfFollowingAssignment = i;
                    for (auto it = assignments.begin(); it != assignments.end(); ++it) {
                        if (j < 0)
                            previousAssignmentInfo = it->second;
                        else
                            followingAssignmentInfo = it->second;
                        if (it->second->assignmentCategory() == AssignmentCategory::NonstudentMeetingPart
                            || it->second->assignmentCategory() == AssignmentCategory::StudentPart) {
                            break; // stop searching, if nonstudent or student parts were found and continue in case of supplementary parts on the same day
                        }
                    }
                }
            }
        }
        if (previousAssignmentInfo && followingAssignmentInfo)
            break;
    }
    return previousAssignmentInfo || followingAssignmentInfo;
}

// NB - returns week commencing date
QDate AvailabilityItem::GetDateLastAssigned(person::UseFor role)
{
    QDate result;
    for (AssignmentsByDateI iter = AssignmentsAtHome.begin(); iter != AssignmentsAtHome.end(); ++iter) {
        if (iter->second.find(role) != iter->second.end()) {
            if (result.isNull() || iter->first > result) {
                result = iter->first;
            }
        }
    }
    return result;
}

// Availability
//=============

void Availability::Add(AvailabilityItem &item)
{
    items_.push_back(item);
}

void Availability::Sort()
{
    std::sort(items_.begin(), items_.end());
}

void Availability::Filter(person::UseFor role)
{
    items_.erase(
            std::remove_if(
                    items_.begin(), items_.end(),
                    [&role](AvailabilityItem &item) { return !item.HasRole(role); }),
            items_.end());
}

void Availability::FilterOnPersonIds(std::vector<int> personIds)
{
    items_.erase(std::remove_if(
                         items_.begin(), items_.end(),
                         [&personIds](AvailabilityItem &item) {
                             return std::find(personIds.begin(), personIds.end(), item.Id) == personIds.end();
                         }),
                 items_.end());
}

int Availability::Count()
{
    return static_cast<int>(items_.size());
}

AvailabilityItem *Availability::GetItem(int index)
{
    return &items_[static_cast<size_t>(index)];
}

AvailabilityItem *Availability::GetItemByPersonId(int personId)
{
    auto it = std::find_if(items_.begin(), items_.end(), [&personId](const AvailabilityItem &item) { return item.Id == personId; });
    if (it != items_.end()) {
        auto index = std::distance(items_.begin(), it);
        return &items_[static_cast<size_t>(index)];
    }
    return nullptr;
}
