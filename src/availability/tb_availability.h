#ifndef AVAILABILITY_H
#define AVAILABILITY_H

#ifndef QSTRING_H
#include <QString>
#endif

#ifndef _VECTOR_
#include <vector>
#endif

#ifndef _SET_
#include <set>
#endif

#ifndef CPERSONS_H
#include "../cpersons.h"
#endif

namespace tbAvailability {

typedef std::map<person::UseFor, AssignmentInfo *> SetOfAssignments;
typedef std::map<QDate, SetOfAssignments> AssignmentsByDate;
typedef std::map<QDate, SetOfAssignments>::iterator AssignmentsByDateI;

// AvailabilityItem
//=================

struct AvailabilityItem {
    int Id;
    QString FirstName;
    QString LastName;
    QString DisplayName;
    QString CongregationName;
    QString Circuit;
    int FamilyHeadId;
    bool IsBreak;
    bool OnHoliday;
    bool OutsideSpeaker;
    bool IsSpeakerAwayInSameMonth;
    bool IsFamilyMemberAssigned;
    int Roles;
    SetOfAssignments Assignments;
    AssignmentsByDate AssignmentsAtHome;

    AvailabilityItem();

    void AddHomeAssignment(QDate weekCommencingDate, AssignmentInfo *assignment);
    bool operator<(const AvailabilityItem &rhs) const;
    bool HasRole(person::UseFor role);
    bool HasAssignmentsOtherThan(person::UseFor role, QDate weekCommencingDate);
    bool GetClosestAssignmentsInOtherWeeks(QDate weekCommencingDate, int &weekOfPreviousAssignment, AssignmentInfo *&previousAssignmentInfo, int &weekOfFollowingAssignment, AssignmentInfo *&followingAssignmentInfo, int weeks = 4);
    QDate GetDateLastAssigned(person::UseFor role);
};

// Availability
//=============

class Availability
{
private:
    std::vector<AvailabilityItem> items_;

public:
    void Add(AvailabilityItem &item);
    void Sort();
    void Filter(person::UseFor role);
    void FilterOnPersonIds(std::vector<int> personIds);
    int Count();
    AvailabilityItem *GetItem(int index);
    AvailabilityItem *GetItemByPersonId(int personId);
};

} // namespace tbAvailability

#endif // AVAILABILITY_H
