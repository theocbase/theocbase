/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVAILABILITYCHECKER_H
#define AVAILABILITYCHECKER_H

#include "../assignmentInfo.h"
#ifndef QSTRING_H
#include <QString>
#endif

#ifndef _VECTOR_
#include <vector>
#endif

#ifndef CPERSONS_H
#include "../cpersons.h"
#endif

#ifndef CCONGREGATION_H
#include "../ccongregation.h"
#endif

#ifndef AVAILABILITY_H
#include "tb_availability.h"
#endif

namespace tbAvailability {

// AvailabilityChecker
//====================

class AvailabilityChecker
{
private:
    sql_class &sql_;
    std::vector<person::UseFor> relevantQualifications_;
    std::vector<QString> dataSourceIds_;

    QDate GetMeetingDate(const QDate &weekCommencingDate, ccongregation::meetings meetingType);

    QString GenerateHolidayPersonsSql();
    QString GenerateQualifiedPersonsSql(int congregationId = 0, bool includeVisitingSpeaker = false);
    QString GenerateQualificationsWhereClause();
    AvailabilityItem CreateAvailabilityItem(const sql_item &qualifiedPerson,
                                            const sql_items &holidayPersons,
                                            const std::map<QString, sql_items> &assignedPersons);

    void AddRoles(AvailabilityItem &result, const sql_item &qualifiedPerson);
    void AddHolidays(AvailabilityItem &item, const sql_items &holidayPersons);
    std::map<QString, sql_items> GetAssignedPersonsFromAllDataSources();
    bool IsLocalCongRoleOnly(person::UseFor role);
    QString nameFormat;

protected:
    QDate weekCommencingDate_;
    QDate meetingDate_;
    int localCongregationId_;
    AssignmentInfos *assignmentInfos;

    AvailabilityChecker(const QDate &meetingDate,
                        const QDate &weekCommencingDate,
                        const std::vector<person::UseFor> &relevantQualifications,
                        const std::vector<QString> &dataSourceIds);

    AvailabilityItem CreateStandardAvailabilityItem(const sql_item &qualifiedPerson,
                                                    const sql_items &holidayPersons);

    std::vector<int> GetSpeakersForTheme(int themeId);
    std::vector<int> GetPersonsForStudentTalks(int lmmClass, QString genderCode = "", int familyHeadId = 0);

    virtual QString GenerateAssignedPersonsSql(QString dataSourceId) = 0;
    virtual void PopulateAssignments(QString dataSourceId, AvailabilityItem &result, const sql_items &assignedPersons) = 0;
    virtual QString GenerateDisplayName(const QString &firstName, const QString &lastName);

public:
    virtual ~AvailabilityChecker();

    Availability Get(int congregationId = 0, bool includeVisitingSpeaker = false);
    Availability GetLocal(bool includeVisitingSpeaker = false);

    Availability Get(person::UseFor role, int congregationId = 0, bool includeVisitingSpeaker = false);
    Availability GetLocal(person::UseFor role, bool includeVisitingSpeaker = false);
};

} // namespace tbAvailability

#endif // AVAILABILITYCHECKER_H
