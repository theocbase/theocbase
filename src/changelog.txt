TheocBase 2014.03.0 (2014-03-30)
===================================


TheocBase 2014.02.1 (2014-02-10)
===================================
Fixed bugs
------------
- Public meeting and wt study editing didn't save data in the database

TheocBase 2014.02.0 (2014-02-09)
===================================
New Features
------------
- Import from WINTM scheduling program
- Import from TMSE scheduling program
- "Congregations and Speakers" has new button to add multiple talks for a speaker
- The fourth row removed from service meeting edit page

Improvements
------------
- Can set day and time of Congregations' current and future weekend meetings
- The option to choose day of CBS when CO visit

Fixed bugs
------------
- Fixed timeline bug that show all publishers even CBS or SM was selected
- CBS data wasn't included in the data exchange file
- OS X native toolbar is now back

TheocBase 2014.01.0 (2014-01-05)
===================================
New Features
------------
- New "Sleek"-style template for combined schedule
- TheocBase now "remembers" defined window size
- "min" text added in to the schedule printing of service meeting parts

Improvements
------------

Fixed bugs
------------
- Cloud schedule uploaded an emtpy HTML file to the server
- volunteer list was listing sisters instead of brothers on school results window
- database error when importing public talk themes
- import wizard was not checking talk numbers
- cursor moving problem in TMS-maintenance
- Allowed assign the assignments that should be possible only for servants

TheocBase 2013.12.0 (2013-12-14)
===================================
New Features
------------
- Danish translations
- Slovak translations
- Template based printing for outgoing speakers schedule and assignment slips
- Licensed under GLPv3
- Database fields tranlated into English

Improvements
------------
- Print templates now supports full set of HTML/CSS
- QR Code placement and size is now read from template
- Congregations and circuit is shown when choosing a public talk speaker

Fixed bugs
------------
- The wrong URL in help command
- School reminders was not possible to send the current week's assignments
- Review reader was not printed in the schedule
- All data of school result is not trasfferred in data exchange operation

TheocBase 2013.10.0 (2013-10-25)
===================================
New Features
------------
- OS X 10.9 Support

TheocBase 2013.07.0 (2013-07-31)
===================================
New Features
------------
- Qt5 Framework
- Support for Mobile Version
- Printing the schedule with QR code and upload file to Cloud

Improvements
------------
- The installer improvements on Windows and Linux

Fixed bugs
------------
- Cannot choose 'No school' option when editing TMS
- Fixed bug when changing positions of assignment slip text
- Some cases wrong date was used in TMS priting
- Some cases school worksheet printing crashes
- When copying TMS worksheet the schedule was copied
- Wrong dates was shown in CBS reader list

TheocBase 0.9.9 (2013-04-28)
===================================
New Features
------------
- New application icon
- New splash screen
- New dialog for congregations and speakers
- New option to use students only for main class or only for aux. classes

Improvements
------------
- Front page layout improved
- The ability to remove multiple theme rows in Settings.

Fixed bugs
------------
- Active date changed after closing Settings-dialog
- Some cases the wrong date was displayed when choosing a student
- Wrong default values when printing assignment slips
- Duplicate school program was added when importing data from TMSWare or thb-file

TheocBase 0.9.8 (2013-02-10)
===================================
New Features
------------
- New edit page when defining public meeting and wt study.
- Auto-updater
- Importing from TMS Explorer
- New song field available for print template

Improvements
------------
- Student's talk history is now displayed in descending order

Fixed bugs
------------
- Review information was not displayed in school work sheet
- It was possible add many congregation in the same name
- It was possible use sister for volunteer of highlights

TheocBase 0.9.7 (2013-01-06)
===================================
New Features
------------
- 12 weeks is displayed in public meeting edit
- New fields for worksheet template (week after next and service meeting starter)

Improvements
------------
- Added separated option for publisers and speakers in data exchange
- Added ability to remove multiple rows at once from school program table
- Start Page translated in many language
- Columns size adjusted in many screen
- Minutes text added for service meeting

Fixed bugs
------------
- It was possible to define the wrong first day of week in school program import
- Invalid data for review week when importing school program in English language
- Standard dialog were not translated all time
- Publishers list was not in alpabetical order
- Some cases themes of last speaker remembered when adding a new speaker.

TheocBase 0.9.6 (2012-12-07)
===================================
New Features
------------
- Template based printing for school worksheet
- Polish translations

Improvements
------------
- Template selection changed on setting page.
- Added information email reminders.

Fixed bugs
------------
- Fixed bug when printing assignment slip on Mac OS X
- Wrong data saved when adding single public talk theme
- Many other bugs fixed

TheocBase 0.9.5 (2012-10-31)
===================================
New Features
------------
- Croatian translations
- Now it's possible to configure STMP settings manually

Improvements
------------
- Default text position settings for assignment
  slip printing (now Afrikaans, Croatian, English, Finnish and Swedish)
- Translations for application menu on Mac OS X
- The duplication in the name is now checked when adding a new publisher

Fixed bugs
------------
- Selected themes not saved to speaker
- Some cases invalid data when importing school schedule
- Wrong name format in assignment slip
- Wrong date in school work sheet when the convention week
- Speaker list sorted by last name
- Exception list sorted by date

TheocBase 0.9.4 (2012-10-15)
===================================
New Features
------------
- Afrikaans translation
- Swedish translation
- Start Page

Improvements
------------
- Translation updates
- Number of publishers is displayed 'Publishers'-dialog
- Used studies are displayed in a different way in school result dialog

Fixed bugs
------------
- Review should not be in the convention week
- Theme order in speaker's public talk list
- Convention week is not shown in the public meeting schedule
- Congregation of speakers is not printed in schedule
- Title text can not be changed for combination print on Mac OS X
- Name of no3 assistant is not shown in front page
- Wrong tab order in publishers dialog
- Some cases it wasn't possible edit student's studies list

TheocBase 0.9.3 (2012-09-18)
===================================
New Features
-------------
- E-Mail reminders
- Template based printing for public meeting
- Estonian translations
- Romanian translations

Improvements
-------------
- Better username/password dialog on startup
- Some data added to school's work sheet

Fixed bugs
-------------
- Some cases wrong study selected for sisters
- Wrong date printed when circuit overseer visit
- Some cases public talk speker cannot selected

TheocBase 0.9.2.2 (2012-08-11)
===================================
Fixed Bugs
----------
- Incorrect dates saved in the shcool program import

Improvements
-------------
- Even better Studies -tab


TheocBase 0.9.2.1 (2012-07-05)
===================================
Fixed Bugs


TheocBase 0.9.2 (2012-06-25)
===================================
New features
------------
- Template based printing
- Ability to print four assignment slips per page
- Checkbox for CBS reader
- Editable length of grey period in timeline view
- Spanish translation

Improvements
------------
- Editing of study history
- Transfer file extension changed .xml to .thb. TheocBase is the
default program for thb-file.

Fixed Bugs
----------
