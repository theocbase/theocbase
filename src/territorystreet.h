/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TERRITORYSTREET_H
#define TERRITORYSTREET_H

#include <QObject>
#include <QAbstractTableModel>
#include "sql_class.h"
#include <QSortFilterProxyModel>
#include <QGeoRectangle>
#if defined(Q_OS_LINUX)
#include "gdal/ogrsf_frmts.h"
#include "gdal/gdal_alg.h"
#elif defined(Q_OS_MAC) || defined(Q_OS_WIN)
#include "ogrsf_frmts.h"
#include "gdal_alg.h"
#endif

class TerritoryStreet : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id NOTIFY notification)
    Q_PROPERTY(int territoryId READ territoryId NOTIFY notification)
    Q_PROPERTY(QString fromNumber READ fromNumber NOTIFY notification)
    Q_PROPERTY(QString toNumber READ toNumber NOTIFY notification)
    Q_PROPERTY(int quantity READ quantity NOTIFY notification)
    Q_PROPERTY(int streetTypeId READ streetTypeId NOTIFY notification)
    Q_PROPERTY(QString wktGeometry READ wktGeometry NOTIFY notification)

public:
    explicit TerritoryStreet(QObject *parent = nullptr);
    TerritoryStreet(const int territoryId, QObject *parent = nullptr);
    TerritoryStreet(const int territoryId, const QString streetName, const QString fromNumber, const QString toNumber, const int quantity, const int streetTypeId,
                    const QString wktGeometry, QObject *parent = nullptr);
    TerritoryStreet(const int id, const int territoryId, const QString streetName, const QString fromNumber, const QString toNumber, const int quantity, const int streetTypeId,
                    const QString wktGeometry, QObject *parent = nullptr);
    ~TerritoryStreet();

    int id() const;
    int territoryId() const;
    void setTerritoryId(const int value);
    QString streetName() const;
    void setStreetName(const QString value);
    QString fromNumber() const;
    void setFromNumber(const QString value);
    QString toNumber() const;
    void setToNumber(const QString value);
    int quantity() const;
    void setQuantity(const int value);
    int streetTypeId() const;
    void setStreetTypeId(const int value);
    QString wktGeometry() const;
    void setWktGeometry(const QString value);
    bool isDirty() const { return m_isDirty; }
    void setIsDirty(const bool value);
    bool save();
    Q_INVOKABLE QGeoRectangle boundingGeoRectangle();

private:
    int m_id;
    int m_territoryId;
    QString m_streetName;
    QString m_fromNumber;
    QString m_toNumber;
    int m_quantity;
    int m_streetTypeId;
    QString m_wktGeometry;
    bool m_isDirty;

signals:
    void notification();
};

class TerritoryStreetModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(TerritoryStreet *selectedStreet READ selectedStreet NOTIFY notification)

public:
    enum Roles {
        StreetIdRole = Qt::UserRole,
        TerritoryIdRole,
        StreetNameRole,
        FromNumberRole,
        ToNumberRole,
        QuantityRole,
        StreetTypeIdRole,
        WktGeometryRole
    };

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    TerritoryStreetModel();
    TerritoryStreetModel(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QVariantMap get(int row);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QModelIndex getStreetIndex(int streetId) const;
    int addStreet(TerritoryStreet *territoryStreet);
    Q_INVOKABLE int addStreet(int territoryId, const QString streetName, QString wktGeometry, int streetTypeId = 1);
    Q_INVOKABLE void removeStreet(int row);
    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    Q_INVOKABLE void loadStreets(int territoryId = 0);
    Q_INVOKABLE void saveStreets();
    Q_INVOKABLE bool updateStreet(int streetId, QString wktGeometry);
    Q_INVOKABLE bool updateStreet(int streetId, int territoryId);
    Q_INVOKABLE bool updateStreet(int streetId, int territoryId, const QString streetName, QString wktGeometry);

    TerritoryStreet *selectedStreet() const { return m_selectedStreet; }
    Q_INVOKABLE void setSelectedStreet(const QModelIndex &selectedIndex = QModelIndex());

    Q_INVOKABLE int getDefaultStreetTypeId();

private:
    TerritoryStreet *getItem(const QModelIndex &index) const;

    QList<TerritoryStreet *> territoryStreets;
    TerritoryStreet *m_selectedStreet;

signals:
    void notification();
    void editCompleted();
    void selectedChanged(const QModelIndex &value = QModelIndex());
};

class TerritoryStreetSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    TerritoryStreetSortFilterProxyModel(QObject *parent = nullptr);

    int filterTerritoryId() { return m_territoryId; }
    Q_INVOKABLE void setFilterTerritoryId(int value);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    int m_territoryId;
};

#endif // TERRITORYSTREET_H
