/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CTERRITORIES_H
#define CTERRITORIES_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QGeoPath>
#include <QGeoRectangle>
#include <vector>
#include "territory.h"
#include "territoryassignment.h"
#include "territorystreet.h"
#include "territoryaddress.h"
#include "sql_class.h"
#include "ccongregation.h"

class GeocodeResult : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString country READ country WRITE setCountry NOTIFY notification)
    Q_PROPERTY(QString state READ state WRITE setState NOTIFY notification)
    Q_PROPERTY(QString county READ county WRITE setCounty NOTIFY notification)
    Q_PROPERTY(QString city READ city WRITE setCity NOTIFY notification)
    Q_PROPERTY(QString district READ district WRITE setDistrict NOTIFY notification)
    Q_PROPERTY(QString street READ street WRITE setStreet NOTIFY notification)
    Q_PROPERTY(QString houseNumber READ houseNumber WRITE setHouseNumber NOTIFY notification)
    Q_PROPERTY(QString postalCode READ postalCode WRITE setPostalCode NOTIFY notification)
    Q_PROPERTY(double latitude READ latitude WRITE setLatitude NOTIFY notification)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude NOTIFY notification)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY notification)
public:
    GeocodeResult(QObject *parent = nullptr);
    ~GeocodeResult();

    QString country() { return m_country; }
    void setCountry(QString value);
    QString state() { return m_state; }
    void setState(QString value);
    QString county() { return m_county; }
    void setCounty(QString value);
    QString city() { return m_city; }
    void setCity(QString value);
    QString district() { return m_district; }
    void setDistrict(QString value);
    QString street() { return m_street; }
    void setStreet(QString value);
    QString houseNumber();
    void setHouseNumber(QString value);
    QString postalCode() { return m_postalCode; }
    void setPostalCode(QString value);
    double latitude() { return m_latitude; }
    void setLatitude(double value);
    double longitude() { return m_longitude; }
    void setLongitude(double value);
    QString wktGeometry();
    QString text() { return m_text; }
    void setText(QString value);

private:
    QString m_country;
    QString m_state;
    QString m_county;
    QString m_city;
    QString m_district;
    QString m_street;
    QString m_houseNumber;
    QString m_postalCode;
    double m_latitude;
    double m_longitude;
    QString m_text;
signals:
    void notification();
};

//Q_DECLARE_METATYPE(GeocodeResult)

class StreetResult : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isChecked READ isChecked WRITE setIsChecked NOTIFY notification)
    Q_PROPERTY(QString streetName READ streetName WRITE setStreetName NOTIFY notification)
    Q_PROPERTY(QString wktGeometry READ wktGeometry WRITE setWktGeometry NOTIFY notification)
public:
    StreetResult(QObject *parent = nullptr);
    ~StreetResult();

    bool isChecked() { return m_isChecked; }
    void setIsChecked(bool value);
    QString streetName() const { return m_streetName; }
    void setStreetName(QString value);
    QString wktGeometry() { return m_wktGeometry; }
    void setWktGeometry(QString value);
    bool isAlreadyAdded() { return m_isAlreadyAdded; }
    void setIsAlreadyAdded(bool isAlreadyAdded);

private:
    bool m_isChecked;
    QString m_streetName;
    QString m_wktGeometry;
    bool m_isAlreadyAdded;
signals:
    void notification();
};

class StreetResultModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    enum Roles {
        IsCheckedRole = Qt::UserRole,
        StreetNameRole,
        IsAlreadyAddedRole,
        WktGeometryRole
    };

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    StreetResultModel();
    StreetResultModel(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QVariantMap get(int row);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE void addStreetResult(StreetResult *streetResult);
    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;

private:
    StreetResult *getItem(const QModelIndex &index) const;
    QList<StreetResult *> streetResults;

signals:
    void notification();
};

class StreetResultSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *source READ source WRITE setSource)
    Q_PROPERTY(QString searchText READ filterSearchText WRITE setFilterSearchText)
    Q_PROPERTY(bool hideAddedStreets READ filterHideAddedStreets WRITE setFilterHideAddedStreets)

public:
    StreetResultSortFilterProxyModel(QObject *parent = nullptr);

    QObject *source() const;
    void setSource(QObject *source);

    int filterCongregationId() { return m_congregationId; }
    Q_INVOKABLE void setFilterCongregationId(int value);

    QString filterSearchText() const;
    void setFilterSearchText(QString value);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
    }

    bool filterHideAddedStreets() const;
    void setFilterHideAddedStreets(bool filterHideAddedStreets);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    int m_congregationId;
    QString m_filterSearchText;
    bool m_filterHideAddedStreets;
};

class Boundary
{
    Q_GADGET
    Q_PROPERTY(int territoryId READ territoryId WRITE setTerritoryId)
    Q_PROPERTY(QGeoPath path READ path WRITE setPath)
    Q_PROPERTY(QVariantList coordinates READ coordinates)
    Q_PROPERTY(bool isHole READ isHole WRITE setIsHole)

public:
    int territoryId() const;
    void setTerritoryId(int value);
    QGeoPath path() const;
    void setPath(const QGeoPath &value);
    QVariantList coordinates() const;

    bool isHole() const;
    void setIsHole(bool value);

private:
    int m_territoryId;
    QGeoPath m_path;
    bool m_isHole;
};
Q_DECLARE_METATYPE(Boundary)

class Street
{
    Q_GADGET
    Q_PROPERTY(int streetId READ streetId WRITE setStreetId)
    Q_PROPERTY(int territoryId READ territoryId WRITE setTerritoryId)
    Q_PROPERTY(QGeoPath path READ path WRITE setPath)
    Q_PROPERTY(QVariantList coordinates READ coordinates)
    Q_PROPERTY(int streetTypeId READ streetTypeId WRITE setStreetTypeId)

public:
    int streetId() const;
    void setStreetId(int value);
    int territoryId() const;
    void setTerritoryId(int value);
    QGeoPath path() const;
    void setPath(const QGeoPath &value);
    QVariantList coordinates() const;
    int streetTypeId() const;
    void setStreetTypeId(int value);

private:
    int m_streetId;
    int m_territoryId;
    QGeoPath m_path;
    int m_streetTypeId;
};
Q_DECLARE_METATYPE(Street)

class CSVSchema : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString delimiter READ delimiter WRITE setDelimiter NOTIFY notification)
    Q_PROPERTY(QList<QString> fields READ fields WRITE setFields NOTIFY notification)
public:
    CSVSchema(QObject *parent = nullptr);
    ~CSVSchema();

    QString delimiter() const;
    void setDelimiter(const QString &delimiter);
    QList<QString> fields() const;
    void setFields(const QList<QString> &fields);

private:
    QString m_delimiter;
    QList<QString> m_fields;

signals:
    void notification();
};

class cterritories : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief cterritories - Constructor
     */
    explicit cterritories(QObject *parent = nullptr);
    ~cterritories();

    Q_INVOKABLE DataObjectListModel *getAddressTypes();
    Q_INVOKABLE DataObjectListModel *getStreetTypes();

    /**
     * @brief getAllTerritories - Get all territories from database
     * @return - List of territories
     */
    Q_INVOKABLE QVariantList getAllTerritories(QVariantList territoryIds = QVariantList());

    /**
     * @brief getTerritory - Get a territory object by name
     * @param locality - territory locality
     * @return - territory object or 0 if not exist
     */
    Q_INVOKABLE static territory *getTerritory(QString locality);

    /**
     * @brief getTerritoryByNumber - Get a territory object by number
     * @param number - number of the territory
     * @return - territory object or 0 if not exist
     */
    Q_INVOKABLE static territory *getTerritoryByNumber(int number);

    /**
     * @brief getTerritoryById - Get a territory object by id
     * @param id - Id in database
     * @return - territory object or 0 if not exist
     */
    Q_INVOKABLE static territory *getTerritoryById(int id);

    /**
     * @brief removeTerritory - Remove territory from the database
     * @param id - Territory's id in the database
     * @return - success or failure
     */
    Q_INVOKABLE bool removeTerritory(int id);

    Q_INVOKABLE int importKmlGeometry(QUrl fileUrl, int nameMatchField, int descriptionMatchField, bool searchByDescription);
    Q_INVOKABLE void getTerritoryBoundaries(int territoryId = 0);
    Q_INVOKABLE bool setTerritoryBoundary(int territoryId, QVariantList path, bool dissolve, QVariantList overlappedTerritoryIds, bool annexOverlappingAreas);
    Q_INVOKABLE bool setTerritoryBoundary(int territoryId, QString wktGeometry, bool dissolve, QVariantList overlappedTerritoryIds, bool annexOverlappingAreas);
    Q_INVOKABLE int splitTerritory(int territoryId, QVariantList cutAreaPath);
    Q_INVOKABLE QString getCongregationAddress() const;
    Q_INVOKABLE void geocodeAddress(QString address);
    Q_INVOKABLE CSVSchema *getCSVschema(QUrl fileUrl, QString delimiter);
    Q_INVOKABLE int importAddresses(QUrl fileUrl, int addressField, int nameField, QString delimiter, int territoryId, int addressTypeNumber, QUrl failedFileUrl);
    Q_INVOKABLE void requestStreetList(int territoryId);
    Q_INVOKABLE void getTerritoryStreets(int territoryId = 0);
    Q_INVOKABLE QGeoCoordinate getClosestPoint(QGeoCoordinate point, double tolerance);
signals:
    void geocodeFinished(const QVariantList geocodeResults);
    void boundariesLoaded(const QVariantMap boundaries, int territoryId);
    void addressImportProgressChanged(int rowCount, int importCount);
    void geocodeError(QString message);
    void streetsLoaded(const QVariantMap boundaries, int territoryId);
    void streetListReceived(const QVariantList streets);
    void streetRequestFailed(QString message);
private slots:
    void addressRequestFinished(QNetworkReply *reply);
    void streetRequestFinished(QNetworkReply *reply);

private:
    QNetworkAccessManager *addressRequest;
    QMap<int, QGeoCoordinate> m_allPoints;
    QUrl getGeocodeUrl(QString address);
    QList<GeocodeResult *> getGeocodeResults(QJsonDocument &doc);
    QList<StreetResult *> getStreetResults(QJsonDocument &doc, int territoryId);
    bool setTerritoryBoundary(int territoryId, OGRGeometry *path, bool dissolve, QVariantList overlappedTerritoryIds, bool annexOverlappingAreas);
    bool annexArea(int &intoTerritoryId, int fromTerritoryId, OGRGeometry *areaGeometry);
    void moveStreetsToTerritory(int fromTerritoryId, int intoTerritoryId);
    void moveAddressesToTerritory(int fromTerritoryId, int intoTerritoryId);
};

//Q_DECLARE_METATYPE(cterritories*)

#endif // CTERRITORIES_H
