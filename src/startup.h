/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STARTUP_H
#define STARTUP_H

#include <QtWidgets>
#include "sql_class.h"
#include <QDesktopServices>
#include <QFile>
#include <settings.h>

namespace Ui {
class startup;
}

class startup : public QDialog
{
    Q_OBJECT
    
public:
    startup(QWidget *parent = 0);
    ~startup();
    
private slots:
    void on_webView_loadFinished(bool arg1);

signals:

private:
    Ui::startup *ui;
    sql_class *sql;

private slots:

};

#endif // STARTUP_H
