#ifndef LMMTALKTYPEDIT_H
#define LMMTALKTYPEDIT_H

#include <QDialog>
#include <QTableWidgetItem>
#include "sql_class.h"
#include "talkTypeComboBox.h"
#include "assignmentInfo.h"

namespace Ui {
class lmmtalktypeedit;
}

class lmmtalktypeedit : public QDialog
{
    Q_OBJECT

public:
    explicit lmmtalktypeedit(QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());
    ~lmmtalktypeedit();

    void Init(bool showButtons = true);

signals:

private slots:
    void on_gridTalkTypes_itemChanged(QTableWidgetItem *item);

private:
    Ui::lmmtalktypeedit *ui;
    sql_class *sql;
};

#endif // LMMTALKTYPEDIT_H
