#ifndef PERSONDETAILMODEL_H
#define PERSONDETAILMODEL_H

#include <QAbstractListModel>
#include <QtMath>
#include "sql_class.h"
#include "ccongregation.h"
#include "cpersons.h"
#include "person.h"
#include "lmm_assignment.h"
#include "availability/midweekmeetingavailabilitychecker.h"

class PersonDetail : public QObject
{
    Q_OBJECT
public:
    PersonDetail();
    PersonDetail(const QString personFullName, QObject *parent = nullptr);
    PersonDetail(const int personId, const QString personFirstName, const QString personLastName,
                 const QDate date, AssignmentInfo *assignmentInfo, const QString theme, const QString congregationName,
                 const int assigneeId, const QString assigneeFirstName, const QString assigneeLastName,
                 const int volunteerId, const QString volunteerFirstName, const QString volunteerLastName,
                 const int assistantId, const QString assistantFirstName, const QString assistantLastName,
                 const QString studyPoint, const QString timing, const QString note,
                 const QDate previous1Date, AssignmentInfo *previous1AssignmentInfo, const QString previous1Theme, const QString previous1CongregationName,
                 const int previous1AssigneeId, const QString previous1assigneeFirstName, const QString previous1assigneeLastName,
                 const int previous1VolunteerId, const QString previous1VolunteerFirstName, const QString previous1VolunteerLastName,
                 const int previous1AssistantId, const QString previous1AssistantFirstName, const QString previous1AssistantLastName,
                 const QDate previous2Date, AssignmentInfo *previous2AssignmentInfo, const QString previous2Theme, const QString previous2CongregationName,
                 const int previous2AssigneeId, const QString previous2assigneeFirstName, const QString previous2assigneeLastName,
                 const int previous2VolunteerId, const QString previous2VolunteerFirstName, const QString previous2VolunteerLastName,
                 const int previous2AssistantId, const QString previous2AssistantFirstName, const QString previous2AssistantLastName,
                 const bool isUnavailable, const bool isMultiAssigned,
                 const int weekOfAnyPreviousAssignment, AssignmentInfo *anyPreviousAssignmentInfo,
                 const int weekOfAnyFollowingAssignment, AssignmentInfo *anyFollowingAssignmentInfo,
                 const bool isFamilyMemberAsssigned, const bool isSpeakerAwayInSameMonth, const int assignmentCount, const int assignmentFrequencyRange,
                 const int timeRange,
                 QObject *parent = nullptr);

    int personId() const;
    QString personFirstName() const;
    QString personLastName() const;

    int assignmentId() const;
    int lmmScheduleId() const;
    QDate date() const;
    AssignmentInfo *assignmentInfo() const;
    QString theme() const;
    const QString &congregationName() const;
    int classNumber() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString personFullName(QString format = "FirstName LastName");

    int assigneeId() const;
    QString assigneeFirstName() const;
    QString assigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString assigneeFullName(QString format = "FirstName LastName");

    int volunteerId() const;
    QString volunteerFirstName() const;
    QString volunteerLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString volunteerFullName(QString format = "FirstName LastName");

    int assistantId() const;
    QString assistantFirstName() const;
    QString assistantLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString assistantFullName(QString format = "FirstName LastName");

    QString studyPoint() const;
    QString timing() const;
    QString note() const;

    QDate previous1Date() const;
    AssignmentInfo *previous1AssignmentInfo() const;
    QString previous1Theme() const;
    const QString &previous1CongregationName() const;
    int previous1ClassNumber() const;

    QString previous1AssigneeFirstName() const;
    QString previous1AssigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous1AssigneeFullName(QString format = "FirstName LastName");

    QString previous1VolunteerFirstName() const;
    QString previous1VolunteerLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous1VolunteerFullName(QString format = "FirstName LastName");

    QString previous1AssistantFirstName() const;
    QString previous1AssistantLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous1AssistantFullName(QString format = "FirstName LastName");

    QDate previous2Date() const;
    AssignmentInfo *previous2AssignmentInfo() const;
    QString previous2Theme() const;
    const QString &previous2CongregationName() const;
    int previous2ClassNumber() const;

    QString previous2AssigneeFirstName() const;
    QString previous2AssigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous2AssigneeFullName(QString format = "FirstName LastName");

    QString previous2VolunteerFirstName() const;
    QString previous2VolunteerLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous2VolunteerFullName(QString format = "FirstName LastName");

    QString previous2AssistantFirstName() const;
    QString previous2AssistantLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous2AssistantFullName(QString format = "FirstName LastName");

    bool isUnavailable() const;
    bool isMultiAssigned() const;
    int weekOfAnyPreviousAssignment() const;
    AssignmentInfo *anyPreviousAssignmentInfo() const;
    int weekOfAnyFollowingAssignment() const;
    AssignmentInfo *anyFollowingAssignmentInfo() const;
    bool isFamilyMemberAssigned() const;
    bool isSpeakerAwayInSameMonth() const;

    int assignmentCount() const;
    int assignmentFrequencyRange() const;
    int timeRange() const;
    int weeksIdle() const;

signals:
    void notification();

private:
    int m_personId;
    QString m_personFirstName;
    QString m_personLastName;
    QString m_personFullName;
    QDate m_date;
    AssignmentInfo *m_assignmentInfo;
    QString m_theme;
    QString m_congregationName;
    int m_assigneeId;
    QString m_assigneeFirstName;
    QString m_assigneeLastName;
    int m_volunteerId;
    QString m_volunteerFirstName;
    QString m_volunteerLastName;
    int m_assistantId;
    QString m_assistantFirstName;
    QString m_assistantLastName;
    QString m_studyPoint;
    QString m_timing;
    QString m_note;
    QDate m_previous1Date;
    AssignmentInfo *m_previous1AssignmentInfo;
    QString m_previous1Theme;
    QString m_previous1CongregationName;
    int m_previous1AssigneeId;
    QString m_previous1AssigneeFirstName;
    QString m_previous1AssigneeLastName;
    int m_previous1VolunteerId;
    QString m_previous1VolunteerFirstName;
    QString m_previous1VolunteerLastName;
    int m_previous1AssistantId;
    QString m_previous1AssistantFirstName;
    QString m_previous1AssistantLastName;
    QDate m_previous2Date;
    AssignmentInfo *m_previous2AssignmentInfo;
    QString m_previous2Theme;
    QString m_previous2CongregationName;
    int m_previous2AssigneeId;
    QString m_previous2AssigneeFirstName;
    QString m_previous2AssigneeLastName;
    int m_previous2VolunteerId;
    QString m_previous2VolunteerFirstName;
    QString m_previous2VolunteerLastName;
    int m_previous2AssistantId;
    QString m_previous2AssistantFirstName;
    QString m_previous2AssistantLastName;
    bool m_isUnavailable;
    bool m_isMultiAssigned;
    int m_weekOfAnyPreviousAssignment;
    AssignmentInfo *m_anyPreviousAssignmentInfo;
    int m_weekOfAnyFollowingAssignment;
    AssignmentInfo *m_anyFollowingAssignmentInfo;
    bool m_isFamilyMemberAssigned;
    bool m_isSpeakerAwayInSameMonth;
    int m_assignmentCount;
    int m_assignmentFrequencyRange;
    int m_timeRange;

    QString getFullName(QString firstName, QString lastName, QString format = "FirstName LastName");
};

class PersonDetailModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
    Q_PROPERTY(int personCount READ personCount)
    Q_PROPERTY(int minAssignmentCount READ minAssignmentCount)
    Q_PROPERTY(int maxAssignmentCount READ maxAssignmentCount)
public:
    explicit PersonDetailModel(QObject *parent = nullptr);
    ~PersonDetailModel();

    enum Roles {
        PersonIdRole = Qt::UserRole,
        PersonFirstNameRole,
        PersonLastNameRole,
        PersonFullNameRole,
        PersonDisplayNameRole,
        DateRole,
        YearRole,
        AssignmentInfoRole,
        AssignmentTypeRole,
        MeetingPartRole,
        ThemeRole,
        CongregationNameRole,
        AlphabetRole,
        AssigneeFullNameRole,
        VolunteerFullNameRole,
        AssistantFullNameRole,
        StudyPointRole,
        TimingRole,
        NoteRole,
        Previous1DateRole,
        Previous1AssignmentInfoRole,
        Previous1AssignmentTypeRole,
        Previous1MeetingPartRole,
        Previous1ThemeRole,
        Previous1CongregationNameRole,
        Previous1AssigneeFullNameRole,
        Previous1VolunteerFullNameRole,
        Previous1AssistantFullNameRole,
        Previous2DateRole,
        Previous2AssignmentInfoRole,
        Previous2AssignmentTypeRole,
        Previous2MeetingPartRole,
        Previous2ThemeRole,
        Previous2CongregationNameRole,
        Previous2AssigneeFullNameRole,
        Previous2VolunteerFullNameRole,
        Previous2AssistantFullNameRole,
        IsUnavailableRole,
        IsMultiAssignedRole,
        WeekOfAnyPreviousAssignmentRole,
        AnyPreviousAssignmentInfoRole,
        WeekOfAnyFollowingAssignmentRole,
        AnyFollowingAssignmentInfoRole,
        IsFamilyMemberAssignedRole,
        IsSpeakerAwayInSameMonth,
        AssignmentCountRole,
        AssignmentFrequencyRangeRole,
        TimeRangeRole,
        WeeksIdleRole
    };

    enum class GroupByRoles : quint16 {
        YearRole = quint16(Roles::YearRole),
        AlphabetRole = quint16(Roles::AlphabetRole),
        AssignmentTypeRole = quint16(Roles::AssignmentTypeRole),
        MeetingPartRole = quint16(Roles::MeetingPartRole),
        ThemeRole = quint16(Roles::ThemeRole),
        CongregationNameRole = quint16(Roles::CongregationNameRole),
        AssignmentFrequencyRangeRole = quint16(Roles::AssignmentFrequencyRangeRole),
        TimeRangeRole = quint16(Roles::TimeRangeRole),
        WeeksIdleRole = quint16(Roles::WeeksIdleRole),
        StudyPointRole = quint16(Roles::StudyPointRole)
    };

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role) const;

    Q_INVOKABLE QVariantMap get(int row) const;

    void addPersonDetail(PersonDetail *personDetail);
    Q_INVOKABLE void clear();
    Q_INVOKABLE void loadPersonDetails(int listType, int personId = 0, int includePersonId = 0, MeetingTypeClass::Type meetingType = MeetingType::None, int lmmClass = 0, MeetingPartClass::Part meetingPart = MeetingPart::None,
                                       bool isAssistant = false, int assistantToPersonId = 0,
                                       bool loadWeekendMeetingParts = true,
                                       bool loadMidweekMeetingParts = true,
                                       bool loadNonStudentMeetingParts = true,
                                       bool loadOtherAssignments = true,
                                       bool loadStudentParts = true,
                                       bool loadStudentPartAssistants = true,
                                       bool loadSimilarAssignentOnly = false,
                                       QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1),
                                       AssignmentSubtypeClass::Subtype assignmentSubtype = AssignmentSubtype::None);
    Q_INVOKABLE void loadLocalPublicTalkPersonDetails(int includePersonId = 0, int themeId = 0,
                                                      QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1));
    Q_INVOKABLE void loadPublicTalkPersonDetails(int includePersonId = 0, int congregationId = 0, int themeId = 0,
                                                 QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1));
    Q_INVOKABLE void loadHospitalityPersonDetails(int includePersonId = 0,
                                                  QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1));
    int personCount() { return m_personCount; }
    int minAssignmentCount() { return m_minAssignmentCount; }
    int maxAssignmentCount() { return m_maxAssignmentCount; }

signals:
    void modelChanged();

private:
    AssignmentInfos *assignmentInfos;
    QList<PersonDetail *> personDetails;
    int m_personCount;
    int m_minAssignmentCount;
    int m_maxAssignmentCount;
    QString displayNameFormat;

    QString getDbTalkIds(QList<MeetingType> meetingTypes, QList<AssignmentCategory> assignmentCategories, AssignmentType assignmentType);
    void loadPersonDetails_WEMeeting(person::UseFor role,
                                     bool loadMainAssignments = true,
                                     bool loadAuxiliaryAssignments = true,
                                     bool loadSimilarAssignentOnly = true,
                                     QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1), int includePersonId = 0);
    void updateModel(QDate date, person::UseFor role, const sql_items &assignmentRows, tbAvailability::Availability &a);
};

class PersonDetailSFProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *source READ source WRITE setSource)
    Q_PROPERTY(QString filterText READ filterText WRITE setFilterText NOTIFY filterChanged)
    Q_PROPERTY(QByteArray sortRole READ sortRole WRITE setSortRole NOTIFY sortChanged)
    Q_PROPERTY(QByteArray groupByRole READ groupByRole WRITE setGroupByRole NOTIFY groupByChanged)
    Q_PROPERTY(bool areUnavailablesHidden READ areUnavailablesHidden WRITE setAreUnavailablesHidden NOTIFY areUnavailablesHiddenChanged)

public:
    PersonDetailSFProxyModel(QObject *parent = nullptr);

    QObject *source() const;
    void setSource(QObject *source);

    QString filterText() const;
    void setFilterText(QString value);

    QByteArray sortRole() const;
    void setSortRole(const QByteArray &role);

    QByteArray groupByRole() const;
    void setGroupByRole(const QByteArray &role);

    bool areUnavailablesHidden() const;
    void setAreUnavailablesHidden(bool newAreUnavailablesHidden);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    int roleKey(const QByteArray &role) const;

private:
    QString m_filterText;
    QByteArray m_groupByRole;
    bool m_areUnavailablesHidden;

signals:
    void filterChanged();
    void sortChanged();
    void groupByChanged();
    void areUnavailablesHiddenChanged();
};

#endif // PERSONDETAILMODEL_H
