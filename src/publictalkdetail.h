#ifndef PUBLICTALKDETAIL_H
#define PUBLICTALKDETAIL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include <QtMath>
#include "qabstractproxymodel.h"
#include "ccongregation.h"

class PublicTalkDetail : public QObject
{
    Q_OBJECT
public:
    PublicTalkDetail();
    PublicTalkDetail(const int themeId, const int themeNumber, const QString theme,
                     const QDate date, const QString congregationName,
                     const int assigneeId, const QString assigneeFirstName, const QString assigneeLastName,
                     const QDate previous1Date, const QString previous1CongregationName,
                     const int previous1AssigneeId, const QString previous1AssigneeFirstName, const QString previous1AssigneeLastName,
                     const QDate previous2Date, const QString previous2CongregationName,
                     const int previous2AssigneeId, const QString previous2AssigneeFirstName, const QString previous2AssigneeLastName,
                     const int assignmentCount, const int assignmentFrequencyRange, const int timeRange,
                     QObject *parent = nullptr);

    int themeId() const;
    int themeNumber() const;
    const QString themeNumberRange(int step) const;
    const QString &theme() const;
    const QString numberAndTheme(QString format = "%1. %2");

    const QDate &date() const;
    const QString &congregationName() const;
    int assigneeId() const;
    const QString &assigneeFirstName() const;
    const QString &assigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    const QString assigneeFullName(QString format = "FirstName LastName") const;

    const QDate &previous1Date() const;
    const QString &previous1CongregationName() const;
    const QString &previous1AssigneeFirstName() const;
    const QString &previous1AssigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    const QString previous1AssigneeFullName(QString format = "FirstName LastName") const;

    const QDate &previous2Date() const;
    const QString &previous2CongregationName() const;
    const QString &previous2AssigneeFirstName() const;
    const QString &previous2AssigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    const QString previous2AssigneeFullName(QString format = "FirstName LastName") const;

    int assignmentCount() const;
    int assignmentFrequencyRange() const;

    int timeRange() const;

signals:
    void notification();

private:
    int m_themeId;
    int m_themeNumber;
    QString m_theme;
    QDate m_date;
    QString m_congregationName;
    int m_assigneeId;
    QString m_assigneeFirstName;
    QString m_assigneeLastName;
    QDate m_previous1Date;
    QString m_previous1CongregationName;
    int m_previous1AssigneeId;
    QString m_previous1AssigneeFirstName;
    QString m_previous1AssigneeLastName;
    QDate m_previous2Date;
    QString m_previous2CongregationName;
    int m_previous2AssigneeId;
    QString m_previous2AssigneeFirstName;
    QString m_previous2AssigneeLastName;
    int m_assignmentCount;
    int m_assignmentFrequencyRange;
    int m_timeRange;

    const QString getFullName(QString firstName, QString lastName, QString format = "FirstName LastName") const;
};

class PublicTalkDetailModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
    Q_PROPERTY(int publicTalkCount READ publicTalkCount)
    Q_PROPERTY(int minAssignmentCount READ minAssignmentCount)
    Q_PROPERTY(int maxAssignmentCount READ maxAssignmentCount)
public:
    explicit PublicTalkDetailModel(QObject *parent = nullptr);
    ~PublicTalkDetailModel();

    enum Roles {
        ThemeIdRole = Qt::UserRole,
        ThemeNumberRole,
        ThemeNumberRangeRole,
        ThemeRole,
        NumberAndThemeRole,
        ThemeAndNumberRole,
        AlphabetRole,
        DateRole,
        YearRole,
        CongregationNameRole,
        AssigneeFullNameRole,
        Previous1DateRole,
        Previous1CongregationNameRole,
        Previous1AssigneeFullNameRole,
        Previous2DateRole,
        Previous2CongregationNameRole,
        Previous2AssigneeFullNameRole,
        AssignmentCountRole,
        AssignmentFrequencyRangeRole,
        TimeRangeRole
    };

    enum class GroupByRoles : quint16 {
        AlphabetRole = quint16(Roles::AlphabetRole),
        ThemeNumberRangeRole = quint16(Roles::ThemeNumberRangeRole),
        YearRole = quint16(Roles::YearRole),
        AssignmentFrequencyRangeRole = quint16(Roles::AssignmentFrequencyRangeRole),
        TimeRangeRole = quint16(Roles::TimeRangeRole)
    };

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role) const;

    Q_INVOKABLE QVariantMap get(int row) const;

    void addPublicTalkDetail(PublicTalkDetail *publicTalkDetail);
    Q_INVOKABLE void clear();
    Q_INVOKABLE void loadPublicTalkDetails(int includeThemeId = 0, int personId = 0, QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1));
    int publicTalkCount() { return m_publicTalkCount; }
    int minAssignmentCount() { return m_minAssignmentCount; }
    int maxAssignmentCount() { return m_maxAssignmentCount; }

signals:
    void modelChanged();

private:
    QList<PublicTalkDetail *> publicTalkDetails;
    int m_publicTalkCount;
    int m_minAssignmentCount;
    int m_maxAssignmentCount;
    QString displayNameFormat;

    void updateModel(QDate date, const sql_items &assignmentRows);
};

class PublicTalkDetailSFProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *source READ source WRITE setSource)
    Q_PROPERTY(QString filterText READ filterText WRITE setFilterText NOTIFY filterChanged)
    Q_PROPERTY(QByteArray sortRole READ sortRole WRITE setSortRole NOTIFY sortChanged)
    Q_PROPERTY(QByteArray groupByRole READ groupByRole WRITE setGroupByRole NOTIFY groupByChanged)

public:
    PublicTalkDetailSFProxyModel(QObject *parent = nullptr);

    QObject *source() const;
    void setSource(QObject *source);

    QString filterText() const;
    void setFilterText(QString value);

    QByteArray sortRole() const;
    void setSortRole(const QByteArray &role);

    QByteArray groupByRole() const;
    void setGroupByRole(const QByteArray &role);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    int roleKey(const QByteArray &role) const;

private:
    QString m_filterText;
    QByteArray m_groupByRole;

signals:
    void filterChanged();
    void sortChanged();
    void groupByChanged();
};

#endif // PUBLICTALKDETAIL_H
