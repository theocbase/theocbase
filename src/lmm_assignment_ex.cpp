#include "lmm_assignment_ex.h"
#include "sqlcombo.h"

LMM_Assignment_ex::LMM_Assignment_ex(QObject *parent)
    : LMM_Assignment(parent)
{
}

LMM_Assignment_ex::LMM_Assignment_ex(AssignmentInfo *assignmentInfo, AssignmentInfo *subAssignmentInfo, int sequence, int scheduleDbId, int assignmentDbId, QObject *parent)
    : LMM_Assignment(assignmentInfo, sequence, scheduleDbId, assignmentDbId, parent), m_subAssignmentInfo(subAssignmentInfo)
{
}

AssignmentInfo *LMM_Assignment_ex::subAssignmentInfo() const
{
    return m_subAssignmentInfo;
}

person *LMM_Assignment_ex::assistant() const
{
    return m_assistant;
}

void LMM_Assignment_ex::setAssistant(person *assistant)
{
    m_assistant = assistant;
    if (m_assistant)
        m_assistant->setParent(this);
    emit assistantChanged();
}

person *LMM_Assignment_ex::volunteer() const
{
    return m_volunteer;
}

void LMM_Assignment_ex::setVolunteer(person *volunteer)
{
    m_volunteer = volunteer;
    if (m_volunteer)
        m_volunteer->setParent(this);
    emit volunteerChanged();
}

QString LMM_Assignment_ex::timing() const
{
    return m_timing;
}

void LMM_Assignment_ex::setTiming(QString time)
{
    m_timing = time;
    emit timingChanged(time);
}

bool LMM_Assignment_ex::completed() const
{
    return m_completed;
}

void LMM_Assignment_ex::setCompleted(bool completed)
{
    m_completed = completed;
    emit completedChanged(completed);
}

QString LMM_Assignment_ex::setting() const
{
    return m_setting;
}

void LMM_Assignment_ex::setSetting(QString setting)
{
    m_setting = setting;
}

SortFilterProxyModel *LMM_Assignment_ex::getAssistantList()
{
    QHash<int, QByteArray> roles;
    roles[MySortFilterProxyModel::MyRoles::id] = "id";
    roles[MySortFilterProxyModel::MyRoles::name] = "name";
    roles[MySortFilterProxyModel::MyRoles::date] = "date";
    roles[MySortFilterProxyModel::MyRoles::date_val] = "date_val";
    roles[MySortFilterProxyModel::MyRoles::date2] = "date2";
    roles[MySortFilterProxyModel::MyRoles::date2_val] = "date2_val";
    roles[MySortFilterProxyModel::MyRoles::icon] = "icon";

    QStandardItemModel *itemmodel = new QStandardItemModel(0, 5, this);
    itemmodel->setItemRoleNames(roles);
    itemmodel->setHorizontalHeaderItem(0, new QStandardItem("Id"));
    itemmodel->setHorizontalHeaderItem(1, new QStandardItem("Name"));
    itemmodel->setHorizontalHeaderItem(2, new QStandardItem("Date"));
    itemmodel->setHorizontalHeaderItem(3, new QStandardItem("Date2"));
    itemmodel->setHorizontalHeaderItem(4, new QStandardItem("Icon"));

    sql_class *sql = &Singleton<sql_class>::Instance();

    SqlComboHelper sch;

    sch.set_talk_conductor(meetingPart());
    sch.set_classnumber(static_cast<short>(m_classnumber));
    sch.set_date(LMM_Schedule::date());
    sch.set_assisting_id(this->speaker() ? this->speaker()->id() : -1);
    sql_items items = sql->selectSql(sch.get_sql());

    // empty row
    itemmodel->setRowCount(1);

    QVector<QList<int>> lists;
    lists.resize(3);

    loadPersonsOnBreak(lists[0]);
    loadMultiAssignedPersons(lists[1]);
    loadFamilyMembers(lists[2]);
    QVector<int> person_ids;
    QVector<short> pris;
    getPersonIdsFromQryResult(person_ids, items, "id");
    getPersonsIconAlertValues(pris, person_ids, lists);
    ccongregation c;

    QString nameFormat = sql->getSetting("nameFormat", "%2, %1");

    for (unsigned int i = 0; i < items.size(); i++) {
        QStandardItem *item = new QStandardItem();
        item->setData(items[i].value("id").toInt(), MySortFilterProxyModel::MyRoles::id);
        item->setData(nameFormat.arg(items[i].value("firstname").toString(),
                                     items[i].value("lastname").toString()),
                      MySortFilterProxyModel::MyRoles::name);
        QDate dateany = items[i].value("date_any").toDate();
        dateany = dateany.addDays(c.getMeetingDay(dateany, ccongregation::tms) - 1);
        item->setData(dateany, MySortFilterProxyModel::MyRoles::date2_val);

        QString str = dateany.toString("yyyy-MM-dd");
        if (str != "") {
            str += " " + LMM_Schedule::getStringTalkType(items[i].value("talk_id").toString().split(" ")[0].toInt());
            str += "c";
            str += items[i].value("classnumber_any").toString();
            str += (items[i].value("assignee_id").toInt() == 0 ? "S" : "A");
        }
        item->setData(str, MySortFilterProxyModel::MyRoles::date2);

        QDate datespecific = items[i].value("date_specific").toDate();
        datespecific = datespecific.addDays(c.getMeetingDay(datespecific, ccongregation::tms) - 1);
        item->setData(datespecific, MySortFilterProxyModel::MyRoles::date_val);
        str = datespecific.toString("yyyy-MM-dd");
        if (str != "") {
            str += " c";
            str += items[i].value("classnumber_specific").toString();
        }
        item->setData(str, MySortFilterProxyModel::MyRoles::date);
        item->setData(items[i].value("firstname"), MySortFilterProxyModel::MyRoles::h_firstname);
        item->setData(items[i].value("lastname"), MySortFilterProxyModel::MyRoles::h_lastname);
        item->setData(items[i].value("date_offset_any"), MySortFilterProxyModel::MyRoles::h_offset_any);
        item->setData(items[i].value("date_offset_specific"), MySortFilterProxyModel::MyRoles::h_offset_specific);

        switch (pris[static_cast<int>(i)]) {
        case 0:
            break;
        case 1:
            item->setData("qrc:///icons/warning_inactive.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        case 2:
            item->setData("qrc:///icons/warning_busy.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        case 3:
            item->setData("qrc:///icons/warning_family_member.svg", MySortFilterProxyModel::MyRoles::icon);
            break;
        }

        itemmodel->setRowCount(itemmodel->rowCount() + 1);
        itemmodel->setItem(static_cast<int>(i) + 1, item);
    }
    SortFilterProxyModel *sortmodel = new MySortFilterProxyModel(this, itemmodel, MySortFilterProxyModel::MyRoles::name, "assistant");
    return sortmodel;
}

QString LMM_Assignment_ex::getReminderText() const
{
    QDate d = date();
    ccongregation c;
    d = d.addDays(c.getMeetingDay(d, ccongregation::tms) - 1);

    QString rowTemplate("%1 : %2\n");
    QString text(tr("Please find below details of your assignment:") + "\n\n");
    text.append(rowTemplate.arg(tr("Date"), QLocale().toString(d, QLocale::ShortFormat)));
    text.append(rowTemplate.arg(tr("Name"), (speaker() ? speakerFullName() : "")));

    if (meetingPart() == MeetingPart::LMM_CBS) {
        text.append(rowTemplate.arg(tr("Reader"), (assistant() ? assistant()->fullname() : "")));
    } else {
        text.append(rowTemplate.arg(tr("Assistant"), (assistant() ? assistant()->fullname() : "")));
    }
    text.append(rowTemplate.arg(tr("Assignment"), assignmentInfo()->meetingPartName()));

    if (!(meetingPart() == MeetingPart::LMM_CBS)) {
        text.append(rowTemplate.arg(tr("Study"), QString::number(study_number())) + "\n");
    }
    text.append(rowTemplate.arg(tr("Theme"), theme()) + "\n");
    text.append(rowTemplate.arg(tr("Source material"), source()));
    if (meetingPart() != MeetingPart::LMM_CBS) {
        QString classText = "";
        switch (classnumber()) {
        case 1:
            classText = tr("Main hall");
            break;
        case 2:
            classText = tr("Auxiliary classroom 1");
            break;
        case 3:
            classText = tr("Auxiliary classroom 2");
            break;
        }
        text.append(rowTemplate.arg(tr("To be given in", "Refer to main hall or aux. classroom. See S-89"), classText));
    }
    return text;
}

bool LMM_Assignment_ex::save()
{
    m_assistant_id = assistant() ? assistant()->id() : -1;
    m_volunteer_id = volunteer() ? volunteer()->id() : -1;
    m_completed = completed();
    m_timing = timing();
    m_setting = setting();
    m_completed = completed();
    m_classnumber = classnumber();
    return LMM_Assignment::save();
}

LMMAssignmentValidator::LMMAssignmentValidator(QObject *parent)
    : QValidator(parent), m_assignment(nullptr), m_assignment_ex(nullptr), m_isAssistant(false), m_isVolunteer(false)
{
}

LMMAssignmentValidator::~LMMAssignmentValidator()
{
}

QValidator::State LMMAssignmentValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos)

    if (input.isEmpty()) {
        emit errorChanged("");
        return Acceptable;
    }

    if (!assignment()) {
        emit errorChanged("");
        return Intermediate;
    }

    if (assignment()->meetingPart() == MeetingPart::Service_Talk) {
        emit errorChanged("");
        return Acceptable;
    }

    ccongregation cc;
    QDate meetingDate = assignment()->date().addDays(cc.getMeetingDay(MeetingType::MidweekMeeting, assignment()->date()) - 1);
    if (meetingDate < QDate::currentDate()) {
        // do not validate historical assignments
        emit errorChanged("");
        return Acceptable;
    }

    if (!isAssistant() && !isVolunteer() && m_assignment_ex && m_assignment_ex->volunteer()) {
        // do not validate cancelled assignee
        emit errorChanged("");
        return Acceptable;
    }

    person *assignee = isVolunteer() || (isAssistant() && m_assignment_ex && m_assignment_ex->volunteer())
            ? m_assignment_ex->volunteer()
            : assignment()->speaker();

    // is in own congregation
    int ownCongregationId = cc.getMyCongregation().id;
    bool isNoMemberOfOwnCongregation =
            isAssistant()
            ? (m_assignment_ex && m_assignment_ex->assistant()
                       ? m_assignment_ex->assistant()->congregationid() != ownCongregationId
                       : false)
            : (assignee
                       ? assignee->congregationid() != ownCongregationId
                       : false);
    if (isNoMemberOfOwnCongregation) {
        emit errorChanged(tr("The assigned person is no member of the congregation."));
        return Invalid;
    }

    // check admission
    bool isRoleMissing =
            isAssistant()
            ? (m_assignment_ex && m_assignment_ex->assistant() && m_assignment_ex->subAssignmentInfo()
                       ? !(m_assignment_ex->assistant()->usefor() & m_assignment_ex->subAssignmentInfo()->role())
                       : false)
            : (assignee && assignment()->assignmentInfo()
                       ? !(assignee->usefor() & assignment()->assignmentInfo()->role())
                       : false);
    if (isRoleMissing) {
        emit errorChanged(tr("Unassignable. Please check the publisher's settings."));
        return Invalid;
    }

    // check availability
    bool isUnavailable = isAssistant()
            ? (m_assignment_ex && m_assignment_ex->assistant()
                       ? (person::UseFor::IsBreak & m_assignment_ex->assistant()->usefor()) > 0
                       : false)
            : (assignee
                       ? (person::UseFor::IsBreak & assignee->usefor()) > 0
                       : false);
    if (!isUnavailable) {
        QList<QPair<QDate, QDate>> unavailabilityList =
                isAssistant()
                ? (m_assignment_ex && m_assignment_ex->assistant()
                           ? m_assignment_ex->assistant()->getUnavailabilities()
                           : QList<QPair<QDate, QDate>>())
                : (assignee
                           ? assignee->getUnavailabilities()
                           : QList<QPair<QDate, QDate>>());
        if (!unavailabilityList.empty()) {
            auto iterator = std::find_if(
                    unavailabilityList.begin(), unavailabilityList.end(),
                    [&meetingDate](const QPair<QDate, QDate> &unavailabilityItem) {
                        return unavailabilityItem.first <= meetingDate
                                && unavailabilityItem.second >= meetingDate;
                    });
            isUnavailable = iterator != unavailabilityList.end();
        }
    }
    if (isUnavailable) {
        emit errorChanged(tr("The assigned person is not available on this day."));
        return Invalid;
    }

    if (assignment()->canHaveAssistant() && m_assignment_ex) {
        // check if assignment is complete
        if (!isAssistant() && assignee && !m_assignment_ex->assistant()) {
            emit errorChanged(tr("Assignment incomplete. Please assign an assistant."));
            return Intermediate;
        }
        if (isAssistant() && !assignee && m_assignment_ex->assistant()) {
            emit errorChanged(tr("Assignment incomplete. Please assign a student."));
            return Intermediate;
        }
        if (!assignee || !m_assignment_ex->assistant()) {
            // do not display warning in the current field, if it is empty
            emit errorChanged("");
            return Intermediate;
        }
        if (assignee->fullname() == m_assignment_ex->assistant()->fullname() && isAssistant()) {
            emit errorChanged(tr("Assignment incomplete. Assign a different person as student or assistant."));
            return Invalid;
        }
        // check gender (and family); display warning only for the assistant
        if (assignee->gender() == m_assignment_ex->assistant()->gender() || !isAssistant()) {
            emit errorChanged("");
            return Acceptable;
        } else {
            switch (assignment()->meetingPart()) {
            case MeetingPart::LMM_InitialCall:
                family *assigneeFamily;
                assigneeFamily = family::getPersonFamily(assignee->id());
                family *assistantFamily;
                assistantFamily = family::getPersonFamily(m_assignment_ex->assistant()->id());
                if (!assigneeFamily || !assistantFamily || assigneeFamily->getHeadId() != assistantFamily->getHeadId()) {
                    emit errorChanged(tr("The assistant should be of the same sex or should be a family member."));
                    return Invalid;
                }
                break;
            case MeetingPart::LMM_ReturnVisit:
            case MeetingPart::LMM_BibleStudy:
                emit errorChanged(tr("The assistant should not be someone of the opposite sex."));
                return Invalid;
            default:
                break;
            }
        }
    }
    emit errorChanged("");
    return Acceptable;
}

LMM_Assignment *LMMAssignmentValidator::assignment() const
{
    return m_assignment;
}

void LMMAssignmentValidator::setAssignment(LMM_Assignment *newAssignment)
{
    if (m_assignment == newAssignment)
        return;
    m_assignment = newAssignment;
    if (newAssignment && typeid(LMM_Assignment_ex).name() == typeid(*newAssignment).name())
        m_assignment_ex = (LMM_Assignment_ex *)m_assignment;
    emit assignmentChanged();
}

bool LMMAssignmentValidator::isAssistant() const
{
    return m_isAssistant;
}

void LMMAssignmentValidator::setIsAssistant(bool newIsAssistant)
{
    if (m_isAssistant == newIsAssistant)
        return;
    m_isAssistant = newIsAssistant;
    emit isAssistantChanged();
}

bool LMMAssignmentValidator::isVolunteer() const
{
    return m_isVolunteer;
}

void LMMAssignmentValidator::setIsVolunteer(bool newIsVolunteer)
{
    if (m_isVolunteer == newIsVolunteer)
        return;
    m_isVolunteer = newIsVolunteer;
    emit isVolunteerChanged();
}
