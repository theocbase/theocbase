#include "talkTypeComboBox.h"

talkTypeComboBox::talkTypeComboBox(QObject *parent)
    : QItemDelegate(parent)
{
}

QWidget *talkTypeComboBox::createEditor(QWidget *parent,
                                        const QStyleOptionViewItem &option,
                                        const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    QComboBox *editor = new QComboBox(parent);

    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_Treasures));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_Digging));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_BibleReading));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_SampleConversationVideo));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_InitialCall));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_ReturnVisit));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_BibleStudy));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_StudentTalk));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_LivingTalk1));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_LivingTalk2));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_LivingTalk3));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_CBS));
    editor->addItem(MeetingPartClass::toString(MeetingPart::Service_Talk));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_MemorialInvitation));
    editor->addItem(MeetingPartClass::toString(MeetingPart::LMM_OtherFMVideoPart));
    QTimer::singleShot(0, editor, &QComboBox::showPopup);
    return editor;
}

void talkTypeComboBox::setEditorData(QWidget *editor,
                                     const QModelIndex &index) const
{
    QComboBox *cb = static_cast<QComboBox *>(editor);
    cb->setCurrentText(index.model()->data(index, Qt::EditRole).toString());
    //cb->showPopup();
}

void talkTypeComboBox::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QComboBox *cb = static_cast<QComboBox *>(editor);
    model->setData(index, cb->currentText(), Qt::EditRole);
}

void talkTypeComboBox::updateEditorGeometry(QWidget *editor,
                                            const QStyleOptionViewItem &option,
                                            const QModelIndex & /* index */) const
{
    editor->setGeometry(option.rect);
}

talkTypeTextEdit::talkTypeTextEdit(QObject *parent)
    : QItemDelegate(parent)
{
}

QWidget *talkTypeTextEdit::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    return new QTextEdit(parent);
}

void talkTypeTextEdit::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QTextEdit *txt = static_cast<QTextEdit *>(editor);
    txt->setHtml("<HTML><BODY>" + index.model()->data(index, Qt::EditRole).toString().replace("\r\n", "<br>").replace("\n", "<br>") + "</BODY></HTML>");
}

void talkTypeTextEdit::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QTextEdit *txt = static_cast<QTextEdit *>(editor);
    model->setData(index, txt->toPlainText(), Qt::EditRole);
}

void talkTypeTextEdit::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &) const
{
    editor->setGeometry(option.rect);
}
