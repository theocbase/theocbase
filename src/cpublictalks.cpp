/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpublictalks.h"

// constructor for cpublictalks
cpublictalks::cpublictalks()
{
    sql = &Singleton<sql_class>::Instance();
    langId = QVariant(sql->getLanguageDefaultId()).toString();
}

// get public meeting by date
// param date
// return cpmeeting object
cptmeeting *cpublictalks::getMeeting(QDate date)
{
    auto meeting = new cptmeeting(date);

    sql_item queryitem;
    queryitem.insert(":date", date);
    meeting->notes = sql->selectScalar("SELECT notes FROM notes WHERE date = :date and type_id = 2 and active", &queryitem, "").toString();

    sql_items es = sql->selectSql("SELECT * FROM publicmeetinghistory where weekof = '" + date.toString(Qt::ISODate) + "'");
    if (!es.empty()) {
        meeting->theme = getThemeById(es[0].value("theme_id").toInt());

        person *speaker = cpersons::getPerson(es[0].value("speaker_id").toInt());
        meeting->setSpeaker(speaker);
        person *chairman = cpersons::getPerson(es[0].value("chairman_id").toInt());
        meeting->setChairman(chairman);
        person *wtreader = cpersons::getPerson(es[0].value("wtreader_id").toInt());
        meeting->setWtReader(wtreader);
        person *wtconductor = cpersons::getPerson(es[0].value("wt_conductor_id").toInt());
        if (!wtconductor)
            wtconductor = defaultWtConductor();
        meeting->setWtConductor(wtconductor);

        meeting->wtissue = es[0].value("wt_source").toString();
        int split(meeting->wtissue.indexOf('/'));
        if (split > -1) {
            meeting->wtsource = meeting->wtissue.mid(split + 1);
            meeting->wtissue = meeting->wtissue.left(split);
        }

        meeting->wttheme = es[0].value("wt_theme").toString();
        meeting->setSongTalk(es[0].value("song_pt").toInt());
        if (meeting->songTalk() > 154)
            meeting->setSongTalk(0);
        meeting->setSongWtStart(es[0].value("song_wt_start").toInt());
        if (meeting->songWtStart() > 154)
            meeting->setSongWtStart(0);
        meeting->setSongWtEnd(es[0].value("song_wt_end").toInt());
        if (meeting->songWtEnd() > 154)
            meeting->setSongWtEnd(0);

        ccongregation c;
        SpecialEventRule *specialEventRule = c.getSpecialEventRule(date);
        meeting->start_time = date.startOfDay();
        meeting->start_time.setTime(QTime::fromString(c.getMyCongregation().getPublicmeeting(date).getMeetingtime(), "hh:mm"));

        meeting->wt_time = specialEventRule->isWatchtowerStudyAbbreviated() ? 30 : 60;
        meeting->final_talk_speaker = specialEventRule->isWithFinalTalk()
                ? (specialEventRule->isCircuitOverseersVisit()
                           ? sql->getSetting("circuitoverseer")
                           : (meeting->speaker() ? meeting->speaker()->fullname() : ""))
                : "";
        if (specialEventRule->isWithFinalTalk())
            meeting->setFinalTalk(es[0].value("final_talk").toString().isEmpty() ? "-" : es[0].value("final_talk").toString());
        else
            meeting->setFinalTalk("");

        meeting->id = es[0].value("id").toInt();

        person *host = cpersons::getPerson(es[0].value("hospitality_id").toInt());
        meeting->setHospitalityhost(host);
        person *opening_prayer = cpersons::getPerson(es[0].value("opening_prayer_id").toInt());
        meeting->setOpeningPrayer(opening_prayer);
        person *final_prayer = cpersons::getPerson(es[0].value("final_prayer_id").toInt());
        meeting->setFinalPrayer(final_prayer);
    }
    return meeting;
}

// get publictalk theme
// param id: theme id in database
// return theme
cpttheme cpublictalks::getThemeById(int id)
{
    sql_items themes;
    themes = sql->selectSql(QString("SELECT * FROM publictalks WHERE id = %1")
                                    .arg(QVariant(id).toString()));
    cpttheme t;
    if (!themes.empty()) {
        t.id = themes[0].value("id").toInt();
        t.number = themes[0].value("theme_number").toInt();
        t.theme = themes[0].value("theme_name").toString();
        t.discontinueDate = themes[0].value("discontinue_date").toDate();
    }
    return t;
}

// get publictalk theme
// param id: theme number
// param date: optional to filter themes by their released/discontinued date
// return theme
cpttheme cpublictalks::getThemeByNumber(int number, QDate date)
{
    sql_items themes;
    QString sqlQuery = QString("SELECT * FROM publictalks WHERE theme_number = %1 AND lang_id = %2 AND active "
                               "AND (discontinue_date IS NULL OR discontinue_date = '' OR discontinue_date > '"
                               + date.toString(Qt::ISODate) + "') "
                                                              "AND (release_date IS NULL OR release_date = '' OR release_date <= '"
                               + date.toString(Qt::ISODate) + "') "
                                                              "GROUP BY publictalks.theme_number")
                               .arg(QVariant(number).toString(), langId);
    themes = sql->selectSql(sqlQuery);
    cpttheme t;
    if (!themes.empty()) {
        t.id = themes[0].value("id").toInt();
        t.number = themes[0].value("theme_number").toInt();
        t.theme = themes[0].value("theme_name").toString();
        t.discontinueDate = themes[0].value("discontinue_date").toDate();
    }
    return t;
}

sql_items cpublictalks::getThemeList(person *speaker)
{
    sql_items pe = sql->selectSql(QString("SELECT * FROM speaker_publictalks "
                                          "WHERE speaker_id = %1 AND lang_id = %3 AND active")
                                          .arg(speaker->id())
                                          .arg(langId));
    return pe;
}

std::vector<ThemeListItem> cpublictalks::getThemeList(const QString &customFilter, person *speaker, QDate date, bool addBlankRow)
{
    sql_items themes;

    QString filter;
    if (!customFilter.isEmpty()) {
        QString cleaned(QString(customFilter).replace("\"", "").replace("'", "").replace(";", ""));

        QRegularExpression rx(".+\\((\\d+)\\)");
        if (cleaned.indexOf(rx) > -1) {
            QRegularExpressionMatch match = rx.match(cleaned);
            cleaned = match.captured(1);
        }

        filter = " and (pt.theme_number = '" + cleaned + "' or pt.theme_name like '%" + cleaned + "%')";
    }

    if (speaker) {
        // by speaker
        themes = sql->selectSql("SELECT pt.*, spt.speaker_id, MAX(m.date) as weekof, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') as mtg_date "
                                "FROM publictalks pt "
                                "LEFT JOIN publicmeeting m ON pt.id = m.theme_id "
                                "LEFT JOIN speaker_publictalks spt ON pt.id = spt.theme_id "
                                "LEFT JOIN congregationmeetingtimes cmt ON "
                                "cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') and "
                                "strftime('%Y', m.date) = cmt.mtg_year "
                                "WHERE spt.speaker_id = '"
                                + QVariant(speaker->id()).toString() + "' "
                                                                       "AND pt.lang_id = '"
                                + langId + "' AND pt.active AND spt.active "
                                           "AND (pt.discontinue_date IS NULL OR pt.discontinue_date = '' OR pt.discontinue_date > '"
                                + date.toString(Qt::ISODate) + "') "
                                                               "AND (pt.release_date IS NULL OR pt.release_date = '' OR pt.release_date <= '"
                                + date.toString(Qt::ISODate) + "') " + filter + " GROUP BY pt.theme_number");
    } else {
        // all themes
        themes = sql->selectSql("SELECT pt.*, MAX(pm.date) weekof, date(pm.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') as mtg_date "
                                "FROM publictalks pt "
                                "LEFT JOIN publicmeeting pm ON pt.id = pm.theme_id "
                                "LEFT JOIN congregationmeetingtimes cmt ON "
                                "cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') and "
                                "strftime('%Y', pm.date) = cmt.mtg_year "
                                "WHERE lang_id = '"
                                + langId + "' AND pt.active "
                                           "AND (pt.discontinue_date IS NULL OR pt.discontinue_date = '' OR pt.discontinue_date > '"
                                + date.toString(Qt::ISODate) + "') "
                                                               "AND (pt.release_date IS NULL OR pt.release_date = '' OR pt.release_date <= '"
                                + date.toString(Qt::ISODate) + "') " + filter + " GROUP BY pt.theme_number");
    }
    std::vector<ThemeListItem> list;
    if (!themes.empty()) {
        if (addBlankRow) {
            ThemeListItem item;
            item.id = -1;
            item.number = 0;
            item.theme = "";
            item.revision = "";
            list.push_back(item);
        }
        for (unsigned int i = 0; i < themes.size(); i++) {
            ThemeListItem item;
            item.id = themes[i].value("id").toInt();
            item.number = themes[i].value("theme_number").toInt();
            item.theme = themes[i].value("theme_name").toString();
            item.last = themes[i].value("mtg_date").toDate();
            item.revision = themes[i].value("revision").toString();
            list.push_back(item);
        }
    }
    return list;
}

// get publictalk themes
// params:
//   speaker: optional to get themes by speaker
//   customFilter: optional to get themes by number or partial text
//   date: optional to filter themes by their released/discontinued date
// return themes is QStandardItemModel table
QStandardItemModel *cpublictalks::getThemesTable(const QString &customFilter, person *speaker, QDate date, bool addBlankRow)
{
    sql_items themes;

    QString filter;
    if (!customFilter.isEmpty()) {
        QString cleaned(QString(customFilter).replace("\"", "").replace("'", "").replace(";", ""));

        QRegularExpression rx(".+\\((\\d+)\\)");
        if (cleaned.indexOf(rx) > -1) {
            QRegularExpressionMatch match = rx.match(cleaned);
            cleaned = match.captured(1);
        }

        filter = " and (pt.theme_number = '" + cleaned + "' or pt.theme_name like '%" + cleaned + "%')";
    }

    if (speaker) {
        // by speaker
        themes = sql->selectSql("SELECT pt.*, spt.speaker_id, m.date as weekof, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') as mtg_date "
                                "FROM publictalks pt "
                                "LEFT JOIN publicmeeting m ON pt.id = m.theme_id "
                                "LEFT JOIN speaker_publictalks spt ON pt.id = spt.theme_id "
                                "LEFT JOIN congregationmeetingtimes cmt ON "
                                "cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') and "
                                "strftime('%Y', m.date) = cmt.mtg_year "
                                "WHERE spt.speaker_id = '"
                                + QVariant(speaker->id()).toString() + "' "
                                                                       "AND pt.lang_id = '"
                                + langId + "' AND pt.active AND spt.active "
                                           "AND (pt.discontinue_date IS NULL OR pt.discontinue_date = '' OR pt.discontinue_date > '"
                                + date.toString(Qt::ISODate) + "') "
                                                               "AND (pt.release_date IS NULL OR pt.release_date = '' OR pt.release_date <= '"
                                + date.toString(Qt::ISODate) + "') " + filter + " GROUP BY pt.theme_number");
    } else {
        // all themes
        themes = sql->selectSql("SELECT pt.*, pm.date weekof, date(pm.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') as mtg_date "
                                "FROM publictalks pt "
                                "LEFT JOIN publicmeeting pm ON pt.id = pm.theme_id "
                                "LEFT JOIN congregationmeetingtimes cmt ON "
                                "cmt.congregation_id = (SELECT value FROM settings WHERE name = 'congregation_id') and "
                                "strftime('%Y', pm.date) = cmt.mtg_year "
                                "WHERE lang_id = '"
                                + langId + "' AND pt.active "
                                           "AND (pt.discontinue_date IS NULL OR pt.discontinue_date = '' OR pt.discontinue_date > '"
                                + date.toString(Qt::ISODate) + "') "
                                                               "AND (pt.release_date IS NULL OR pt.release_date = '' OR pt.release_date <= '"
                                + date.toString(Qt::ISODate) + "') " + filter + " GROUP BY pt.theme_number");
    }
    auto model = new QStandardItemModel(0, 3);
    model->setHorizontalHeaderItem(0, new QStandardItem("id"));
    model->setHorizontalHeaderItem(1, new QStandardItem(""));
    model->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("Theme")));
    model->setHorizontalHeaderItem(3, new QStandardItem(QObject::tr("Last")));
    if (!themes.empty()) {
        if (addBlankRow) {
            QList<QStandardItem *> cells;
            cells.append(new QStandardItem(""));
            cells.append(new QStandardItem(""));
            cells.append(new QStandardItem(""));
            cells.append(new QStandardItem(""));
            model->appendRow(cells);
        }
        for (unsigned int i = 0; i < themes.size(); i++) {
            QList<QStandardItem *> cells;
            auto item = new QStandardItem();
            item->setData(themes[i].value("id").toInt(), Qt::DisplayRole);
            cells.append(item);
            auto themeNumberItem = new QStandardItem();
            themeNumberItem->setData(themes[i].value("theme_number").toInt(), Qt::DisplayRole);
            cells.append(themeNumberItem);
            cells.append(new QStandardItem(themes[i].value("theme_name").toString() + " (" + themes[i].value("theme_number").toString() + ")"));
            cells.append(new QStandardItem(themes[i].value("mtg_date").toDate().toString(Qt::ISODate)));
            model->appendRow(cells);
        }
    }
    return model;
}

// --------------------------------
// get list of public talk speakers
// --------------------------------
// param themeid: optional
// param onlyOwnCongregation: optional
// param date: optional
// return list of speakers in QStandardItemModel
QStandardItemModel *cpublictalks::getSpeakers(int themeid, int congregationid, const QString &customFilter, bool onlyOwnCongregation, bool addBlankRow, QDate date)
{
    sql_items speakers;
    sql_items out;
    QString sqlquery = "";

    if (themeid <= 0) {
        // all speaker
        sqlquery.append("SELECT p.*, m.date, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') as mtg_date, c.name AS congregation, c.circuit "
                        "FROM persons p "
                        "LEFT JOIN publicmeeting m ON p.id = m.speaker_id "
                        "LEFT JOIN congregations c ON p.congregation_id = c.id "
                        "LEFT JOIN congregationmeetingtimes cmt ON cmt.congregation_id = c.id and strftime('%Y', m.date) = cmt.mtg_year "
                        "LEFT JOIN unavailables u on p.id = u.person_id and date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') between u.start_date and u.end_date "
                        "WHERE p.active AND p.usefor & 512 and u.person_id is null");
    } else {
        // speakers by theme
        sqlquery.append(QString("SELECT p.*, m.date, date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') as mtg_date, c.name AS congregation, c.circuit "
                                "FROM persons p "
                                "LEFT JOIN speaker_publictalks spt ON p.id = spt.speaker_id "
                                "LEFT JOIN publicmeeting m ON p.id = m.speaker_id "
                                "LEFT JOIN congregations c ON p.congregation_id = c.id "
                                "LEFT JOIN congregationmeetingtimes cmt ON cmt.congregation_id = c.id and strftime('%Y', m.date) = cmt.mtg_year "
                                "LEFT JOIN unavailables u on p.id = u.person_id and date(m.date, (ifnull(cmt.mtg_day, 7) - 1) || ' day') between u.start_date and u.end_date "
                                "WHERE spt.theme_id = '%1' AND spt.active AND p.active AND p.usefor & 512 and u.person_id is null")
                                .arg(QVariant(themeid).toString()));
    }
    if (onlyOwnCongregation) {
        ccongregation cong;
        sqlquery.append(QString(" AND p.congregation_id = %1").arg(QVariant(cong.getMyCongregation().id).toString()));
        if (!date.isNull()) {
            out = sql->selectSql("SELECT * FROM publicmeeting WHERE date = '" + date.toString(Qt::ISODate) + "' AND active");
        }
    } else if (congregationid > 0) {
        sqlquery.append(" AND p.congregation_id = " + QVariant(congregationid).toString());
    }
    if (!customFilter.isEmpty()) {
        QString cleaned(QString(customFilter).replace("\"", "").replace("'", "").replace(";", ""));
        QStringList parts = cleaned.split(" ", Qt::SkipEmptyParts);
        if (parts.length() == 1)
            sqlquery.append(" AND (p.firstname like '%" + cleaned + "%' OR p.lastname like '%" + cleaned + "%')");
        else
            sqlquery.append(" AND p.firstname like '%" + parts[0] + "%' AND p.lastname like '%" + parts[1] + "%'");
    }
    sqlquery.append(" GROUP BY p.id ORDER BY lastname, firstname");

    speakers = sql->selectSql(sqlquery);
    auto model = new QStandardItemModel(0, 7);
    model->setHorizontalHeaderItem(1, new QStandardItem(QObject::tr("Name")));
    model->setHorizontalHeaderItem(2, new QStandardItem(QObject::tr("Last")));
    model->setHorizontalHeaderItem(3, new QStandardItem(""));
    model->setHorizontalHeaderItem(4, new QStandardItem(QObject::tr("Congregation")));
    model->setHorizontalHeaderItem(5, new QStandardItem(QObject::tr("Circuit")));
    model->setHorizontalHeaderItem(6, new QStandardItem(""));

    if (addBlankRow) {
        model->appendRow(new QStandardItem(""));
    }
    for (unsigned int i = 0; i < speakers.size(); i++) {
        QList<QStandardItem *> items;
        items.append(new QStandardItem(speakers[i].value("id").toString()));
        items.append(new QStandardItem(speakers[i].value("lastname").toString() + ", " + speakers[i].value("firstname").toString()));
        items.append(new QStandardItem(speakers[i].value("mtg_date").toDate().toString(Qt::ISODate)));
        items.append(new QStandardItem(speakers[i].value("firstname").toString() + " " + speakers[i].value("lastname").toString()));
        items.append(new QStandardItem(speakers[i].value("congregation").toString()));
        items.append(new QStandardItem(speakers[i].value("circuit").toString()));
        items.append(new QStandardItem());

        if (!out.empty()) {
            if (out[0].value("chairman_id").toInt() == speakers[i].value("id").toInt() || out[0].value("wtreader_id").toInt() == speakers[i].value("id").toInt() || out[0].value("wt_conductor_id").toInt() == speakers[i].value("id").toInt()) {
                auto item = new QStandardItem();
                item->setBackground(QBrush(QPixmap(":/images/dialog-warning.png").scaled(25, 25)));
                item->setFlags(Qt::NoItemFlags);
                items.replace(6, item);
            }
        }

        model->appendRow(items);
    }
    return model;
}

// get default WT conductor
// return person object
person *cpublictalks::defaultWtConductor()
{
    person *conductor = cpersons::getPerson(sql->getSetting("wtconductor", "-1").toInt());
    return conductor;
}

// constuctor for cpmeeting class
// param date: optional
cptmeeting::cptmeeting(QDate date, QObject *parent)
    : QObject(parent)
{
    initMeeting();
    this->id = -1;
    this->start_time = date.startOfDay();
    if (!date.isNull()) {
        _date = date;
    }
    sql = &Singleton<sql_class>::Instance();
}

void cptmeeting::initMeeting()
{
    this->m_chairman = nullptr;
    this->m_speaker = nullptr;
    this->m_wtreader = nullptr;
    this->m_wtconductor = nullptr;
    this->m_openingPrayer = nullptr;
    this->m_finalPrayer = nullptr;
    this->theme = cpttheme();
    this->m_songTalk = 0;
    this->m_songWtStart = 0;
    this->m_songWtEnd = 0;
    this->notes = "";
    this->wtsource = "";
    this->wttheme = "";
    this->wt_time = 60;
    this->m_final_talk = "";
    this->m_hospitalityhost = nullptr;
}

QDate cptmeeting::date() const
{
    return _date;
}

person *cptmeeting::speaker()
{
    return m_speaker;
}

void cptmeeting::setSpeaker(person *s)
{
    m_speaker = s;
    if (s)
        s->setParent(this);
    emit meetingChanged();
}

person *cptmeeting::chairman()
{
    return m_chairman;
}

void cptmeeting::setChairman(person *c)
{
    m_chairman = c;
    if (c)
        c->setParent(this);
    emit meetingChanged();
}

person *cptmeeting::wtConductor()
{
    return m_wtconductor;
}

void cptmeeting::setWtConductor(person *c)
{
    m_wtconductor = c;
    if (c)
        c->setParent(this);
    emit meetingChanged();
}

person *cptmeeting::wtReader()
{
    return m_wtreader;
}

void cptmeeting::setWtReader(person *r)
{
    m_wtreader = r;
    if (r)
        r->setParent(this);
    emit meetingChanged();
}

int cptmeeting::themeId() const
{
    return theme.id;
}

int cptmeeting::themeNumber() const
{
    return theme.number;
}

QString cptmeeting::themeName() const
{
    return theme.theme;
}

void cptmeeting::setTheme(int id)
{
    cpublictalks c;
    this->theme = c.getThemeById(id);
    emit meetingChanged();
}

// get outgoing speakers
// return list
QList<cpoutgoing *> cpublictalks::getOutgoingSpeakers(const QDate d)
{
    QList<cpoutgoing *> list;
    cpublictalks ptalkclass;
    sql_items values;
    values = sql->selectSql(QString("SELECT * FROM outgoing WHERE date = '%1' AND active").arg(d.toString(Qt::ISODate)));
    for (unsigned int i = 0; i < values.size(); i++) {
        QDate currentdate = values[i].value("date").toDate();
        auto out = new cpoutgoing(values[i].value("id").toInt(), values[i].value("congregation_id").toInt(), currentdate);
        out->setTheme(ptalkclass.getThemeById(values[i].value("theme_id").toInt()));
        out->setSpeaker(cpersons::getPerson(values[i].value("speaker_id").toInt()));
        list.append(out);
    }
    return list;
}

// get speakers outgoing talks
// param speakerid: speakers' id number in database
// param onlythismonth: optional
// return list of outgoing objects
QList<cpoutgoing *> cpublictalks::getOutgoingBySpeaker(QDate d, int speakerid, bool onlythismonth)
{
    QList<cpoutgoing *> list;
    cpublictalks ptalkclass;
    ccongregation congclass;
    sql_items values;
    QString qstr = "";
    if (onlythismonth) {
        sql_item mInfo(sql->getPublicMeetingInfo(congclass.getMyCongregation().id, d));
        int pmday = mInfo.empty() ? 6 : mInfo.value("mtg_day").toInt() - 1;
        qstr = QString("SELECT *, date(date, '+%1 day') as tempdate FROM outgoing WHERE speaker_id = %2 AND strftime('%Y-%m',tempdate) = '%3' AND active")
                       .arg(QVariant(pmday).toString(), QVariant(speakerid).toString(), d.addDays(pmday).toString(Qt::ISODate).left(7));
        //SELECT *, date(date, '+6 day') as tempdate FROM outgoing WHERE speaker_id = 12 AND strftime('%Y-%m',tempdate) = '2013-02'
    } else {
        qstr = QString("SELECT * FROM outgoing WHERE speaker_id = %1 AND active")
                       .arg(QVariant(speakerid).toString());
    }

    values = sql->selectSql(qstr);
    for (unsigned int i = 0; i < values.size(); i++) {
        cpoutgoing *out = new cpoutgoing(values[i].value("id").toInt(),
                                         values[i].value("congregation_id").toInt(),
                                         values[i].value("date").toDate());
        out->setTheme(ptalkclass.getThemeById(values[i].value("theme_id").toInt()));
        out->setSpeaker(cpersons::getPerson(values[i].value("speaker_id").toInt()));
        list.append(out);
    }
    return list;
}

// get speakers outgoing talks on selected period, ordered by date
// param speakerid: speakers' id number in database
// param fromdate: from date
// param todate: to date
// return list of outgoing objects
QList<cpoutgoing *> cpublictalks::getOutgoingBySpeaker(int speakerid, QDate fromdate, QDate todate)
{
    QList<cpoutgoing *> list;
    cpublictalks ptalkclass;
    ccongregation congclass;
    sql_items values = sql->selectSql(
            QString("SELECT * FROM outgoing WHERE speaker_id = %1 AND date BETWEEN '%2' AND '%3' AND active ORDER BY date").arg(QVariant(speakerid).toString(), fromdate.toString(Qt::ISODate), todate.toString(Qt::ISODate)));
    for (unsigned int i = 0; i < values.size(); i++) {
        cpoutgoing *out = new cpoutgoing(values[i].value("id").toInt(),
                                         values[i].value("congregation_id").toInt(),
                                         values[i].value("date").toDate());
        out->setTheme(ptalkclass.getThemeById(values[i].value("theme_id").toInt()));
        out->setSpeaker(cpersons::getPerson(values[i].value("speaker_id").toInt()));
        list.append(out);
    }
    return list;
}

cpoutgoing *cpublictalks::getOutgoingByCongregation(const QDate d, const QString &congregationName)
{
    cpoutgoing *out = nullptr;
    sql_item args;
    args.insert(":congname", congregationName);
    args.insert(":date", d);
    sql_items values = sql->selectSql("SELECT * FROM outgoing WHERE congregation_id = (select id from congregations where name = :congname) AND date = :date AND active", &args);
    if (!values.empty()) {
        cpublictalks ptalkclass;
        out = new cpoutgoing(values[0].value("id").toInt(),
                             values[0].value("congregation_id").toInt(),
                             values[0].value("date").toDate());
        out->setTheme(ptalkclass.getThemeById(values[0].value("theme_id").toInt()));
        out->setSpeaker(cpersons::getPerson(values[0].value("speaker_id").toInt()));
    }
    return out;
}

cpoutgoing *cpublictalks::addOutgoingSpeaker(const QDate d, int speakerid, int themeid, int congregationid)
{
    sql_item s;
    s.insert("date", d);
    s.insert("speaker_id", speakerid);
    s.insert("congregation_id", congregationid);
    s.insert("theme_id", themeid);
    int id = -1;
    id = sql->insertSql("outgoing", &s, "id");
    cpoutgoing *o = nullptr;
    if (id > 0) {
        cpublictalks ptalkclass;
        ccongregation congclass;
        o = new cpoutgoing(id, congregationid, d);
        person *cs = cpersons::getPerson(speakerid);
        o->setSpeaker(cs);
        cpttheme ct = ptalkclass.getThemeById(themeid);
        o->setTheme(ct);
    }
    return o;
}

bool cpublictalks::removeOutgoingSpeaker(int id)
{
    sql_item s;
    s.insert("id", id);
    s.insert("time_stamp", 0);
    return sql->execSql("UPDATE outgoing SET active = 0, time_stamp = :time_stamp WHERE id = :id", &s, true);
    //return sql->removeSql("outgoing","id = " + QVariant(id).toString());
}

QString cptmeeting::getSong1Title() const
{
    return getSongTitle(m_songTalk);
}

QString cptmeeting::getSong2Title() const
{
    return getSongTitle(m_songWtStart);
}

QString cptmeeting::getSong3Title() const
{
    return getSongTitle(m_songWtEnd);
}

QString cptmeeting::getFinalTalk() const
{
    return m_final_talk;
}

void cptmeeting::setFinalTalk(const QString &talktheme)
{
    m_final_talk = talktheme;
    emit meetingChanged();
}

QColor cptmeeting::getWtIssueColor()
{
    // format of the setting: <reference pixel x position>|<reference pixel y position>,<January color>,..,<December color>
    // reference pixel position is used to read the color of the given pixel from the cover image
    QString defaultMonthlyColors(QString("600|1520,#,#,#,#,#,#,#,#,#,#,#,#").replace("#", "#656164"));
    QString epubColors(sql->getSetting("wt_colors", defaultMonthlyColors));
    QStringList colors = epubColors.split(",");
    if (colors.size() != 13)
        colors = defaultMonthlyColors.split(",");
    int wtNumber = QVariant(wtissue).toInt();
    if (wtNumber > 0 && wtNumber < colors.size())
        return colors[wtNumber];
    else
        return "#4c4c4e";
}

QColor cptmeeting::getWtIssueLightColor()
{
    QColor color(getWtIssueColor());
    int h = color.hue();
    int s = (int)(color.saturation() * 0.15);
    color.setHsv(h, s, 240);
    return color;
}

person *cptmeeting::openingPrayer() const
{
    return m_openingPrayer;
}

void cptmeeting::setOpeningPrayer(person *newOpeningPrayer)
{
    if (m_openingPrayer == newOpeningPrayer)
        return;
    m_openingPrayer = newOpeningPrayer;
    if (openingPrayer())
        openingPrayer()->setParent(this);
    emit meetingChanged();
}

person *cptmeeting::finalPrayer() const
{
    return m_finalPrayer;
}

void cptmeeting::setFinalPrayer(person *newFinalPrayer)
{
    if (m_finalPrayer == newFinalPrayer)
        return;
    m_finalPrayer = newFinalPrayer;
    if (finalPrayer())
        finalPrayer()->setParent(this);
    emit meetingChanged();
}

int cptmeeting::songTalk() const
{
    return m_songTalk;
}

void cptmeeting::setSongTalk(int newSongTalk)
{
    if (m_songTalk == newSongTalk)
        return;
    m_songTalk = newSongTalk;
    emit songTalkChanged();
}

int cptmeeting::songWtStart() const
{
    return m_songWtStart;
}

void cptmeeting::setSongWtStart(int newSongWtStart)
{
    if (m_songWtStart == newSongWtStart)
        return;
    m_songWtStart = newSongWtStart;
    emit songWtStartChanged();
}

int cptmeeting::songWtEnd() const
{
    return m_songWtEnd;
}

void cptmeeting::setSongWtEnd(int newSongWtEnd)
{
    if (m_songWtEnd == newSongWtEnd)
        return;
    m_songWtEnd = newSongWtEnd;
    emit songWtEndChanged();
}

QString cptmeeting::getSongTitle(int song) const
{
    if (song < 1)
        return "";
    sql_class *sql = &Singleton<sql_class>::Instance();
    int defaultlang = sql->getLanguageDefaultId();
    sql_item parameters;
    parameters.insert(":song_number", QVariant(song));
    parameters.insert(":lang_id", defaultlang);
    QString songTitle = sql->selectScalar("select title from song where song_number = :song_number and lang_id = :lang_id and active", &parameters, "").toString();
    return songTitle;
}

// save publicmeeting data
void cptmeeting::save()
{
    sql_item s;
    if (this->chairman()) {
        s.insert("chairman_id", this->chairman()->id());
    } else {
        s.insert("chairman_id", -1);
    }
    if (this->wtReader()) {
        s.insert("wtreader_id", this->wtReader()->id());
    } else {
        s.insert("wtreader_id", -1);
    }

    if (this->theme.id > 0) {
        s.insert("theme_id", this->theme.id);
    } else {
        s.insert("theme_id", 0);
    }

    if (this->speaker()) {
        s.insert("speaker_id", this->speaker()->id());
    } else {
        s.insert("speaker_id", -1);
    }
    if (this->wtConductor()) {
        s.insert("wt_conductor_id", this->wtConductor()->id());
    } else {
        s.insert("wt_conductor_id", -1);
    }

    s.insert("hospitality_id", this->getHospitalityhost() ? this->getHospitalityhost()->id() : -1);
    s.insert("opening_prayer_id", this->openingPrayer() ? this->openingPrayer()->id() : -1);
    s.insert("final_prayer_id", this->finalPrayer() ? this->finalPrayer()->id() : -1);

    if (this->songTalk() >= 0)
        s.insert("song_pt", this->songTalk());
    if (this->songWtStart() >= 0)
        s.insert("song_wt_start", this->songWtStart());
    if (this->songWtEnd() >= 0)
        s.insert("song_wt_end", this->songWtEnd());

    s.insert("wt_source", this->wtissue + "/" + this->wtsource);
    s.insert("wt_theme", this->wttheme);
    s.insert("final_talk", this->m_final_talk);

    if (this->id < 0) {
        s.insert("date", _date);
        this->id = sql->insertSql("publicmeeting", &s, "id");
    } else {
        sql->updateSql("publicmeeting", "id", QVariant(this->id).toString(), &s);
    }
}

void cptmeeting::saveNotes()
{
    if (!date().isValid())
        return;
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item items;
    items.insert(":date", date());
    int id = sql->selectScalar("SELECT id FROM notes WHERE date = :date AND type_id = 2 ORDER BY active DESC", &items, -1).toInt();
    sql_item insertitems;
    insertitems.insert("date", date());
    insertitems.insert("notes", notes);
    insertitems.insert("type_id", 2);
    insertitems.insert("active", 1);
    if (id > 0)
        sql->updateSql("notes", "id", QString::number(id), &insertitems);
    else
        sql->insertSql("notes", &insertitems, "id");
}

// constructor for cpttheme
//cpublictalks::cpttheme::cpttheme()
//{
//    this->theme = "";
//    this->id = -1;
//    this->number = -1;
//}

///
/// \brief cpublictalks::cpttheme::themeInLanguage
/// \param lang_id language id number in database
/// \param date optional to filter themes by their released/discontinued date
/// \return theme name in specific language
///
//QString cpublictalks::cpttheme::themeInLanguage(int lang_id, QDate date)
//{
//    sql_class *sql = &Singleton<sql_class>::Instance();
//    //SELECT theme_number FROM publictalks WHERE theme_number = 2 AND lang_id = 1
//    sql_items es = sql->selectSql("SELECT theme_name FROM publictalks WHERE theme_number = " +
//                                  QVariant(this->number).toString() + " AND lang_id = " +
//                                  QVariant(lang_id).toString() + " AND active "
//                                  "AND (discontinue_date IS NULL OR discontinue_date = '' OR discontinue_date > '" + date.toString(Qt::ISODate) + "') "
//                                  "AND (release_date IS NULL OR release_date = '' OR release_date <= '" + date.toString(Qt::ISODate) + "')");
//    QString theme = "";
//    if(!es.empty()) theme = es[0].value("theme_name").toString();
//    return theme;
//}

cpoutgoing::cpoutgoing(int id, int congregation_id, QDate weekOf)
{
    ccongregation c;
    _speaker = nullptr;
    _congregation = c.getCongregationById(congregation_id);
    _id = id;
    _weekOf = weekOf;
    sql = &Singleton<sql_class>::Instance();
}

cpttheme cpoutgoing::theme() const
{
    return _ctheme;
}

void cpoutgoing::setTheme(const cpttheme &t)
{
    _ctheme = t;
}

person *cptmeeting::getHospitalityhost() const
{
    return m_hospitalityhost;
}

void cptmeeting::setHospitalityhost(person *hospitalityhost)
{
    m_hospitalityhost = hospitalityhost;
}

person *cpoutgoing::speaker() const
{
    return _speaker.data();
}

void cpoutgoing::setSpeaker(person *p)
{
    _speaker.reset(p);
}

ccongregation::congregation const &cpoutgoing::congregation() const
{
    return _congregation;
}

void cpoutgoing::setCongregation(const int id)
{
    ccongregation c;
    _congregation = c.getCongregationById(id);
}

int cpoutgoing::id() const
{
    return _id;
}

QDate cpoutgoing::weekOf() const
{
    return _weekOf;
}

void cpoutgoing::setWeekOf(const QDate &newWeekOf)
{
    _weekOf = newWeekOf;
}

QDate cpoutgoing::date() const
{
    return _weekOf.addDays(_congregation.getPublicmeeting(_weekOf).getMeetingday() - 1);
}

QString cpoutgoing::time() const
{
    return _congregation.getPublicmeeting(_weekOf).getMeetingtime();
}

bool cpoutgoing::save()
{
    sql_item s;
    s.insert("date", _weekOf);
    s.insert("speaker_id", _speaker.data() ? _speaker.data()->id() : -1);
    s.insert("congregation_id", _congregation.id);
    s.insert("theme_id", _ctheme.id);
    return sql->updateSql("outgoing", "id", QString::number(id()), &s);
}

///
/// \brief cpttheme::themeInLanguage
/// \param lang_id language id number in database
/// \param date optional to filter themes by their released/discontinued date
/// \return theme name in specific language
///
QString cpttheme::themeInLanguage(int lang_id, QDate date)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_items es = sql->selectSql("SELECT theme_name FROM publictalks WHERE theme_number = " + QVariant(this->number).toString() + " AND lang_id = " + QVariant(lang_id).toString() + " AND active "
                                                                                                                                                                                      "AND (discontinue_date IS NULL OR discontinue_date = '' OR discontinue_date > '"
                                  + date.toString(Qt::ISODate) + "') "
                                                                 "AND (release_date IS NULL OR release_date = '' OR release_date <= '"
                                  + date.toString(Qt::ISODate) + "')");
    return (es.empty() ? "" : es[0].value("theme_name").toString());
}

WEMeetingValidator::WEMeetingValidator(QObject *parent)
    : QValidator(parent), m_meeting(nullptr), m_field(Field::None)
{
}

WEMeetingValidator::~WEMeetingValidator()
{
}

QValidator::State WEMeetingValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos)

    if (input.isEmpty()) {
        emit errorChanged("");
        return Acceptable;
    }

    if (!meeting()) {
        emit errorChanged("");
        return Intermediate;
    }

    QDate meetingDate = meeting()->date();
    if (!meetingDate.isValid()) {
        emit errorChanged("");
        return Intermediate;
    }

    ccongregation cc;
    meetingDate = meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, meetingDate) - 1);
    if (meetingDate < QDate::currentDate()) {
        // do not validate historical assignments
        emit errorChanged("");
        return Acceptable;
    }

    person *assignee = nullptr;
    person::UseFor role;
    switch (field()) {
    case Field::Chairman:
        assignee = meeting()->chairman();
        role = person::Chairman;
        break;
    case Field::Congregation:
        assignee = meeting()->speaker();
        role = person::PublicTalk;
        break;
    case Field::Speaker:
    case Field::Theme:
        assignee = meeting()->speaker();
        role = person::PublicTalk;
        break;
    case Field::WtConductor:
        assignee = meeting()->wtConductor();
        role = person::WtCondoctor;
        break;
    case Field::WtReader:
        assignee = meeting()->wtReader();
        role = person::WtReader;
        break;
    case Field::OpeningPrayer:
        assignee = meeting()->openingPrayer();
        role = person::Prayer;
        break;
    case Field::FinalPrayer:
        assignee = meeting()->finalPrayer();
        role = person::Prayer;
        break;
    case Field::HospitalityHost:
        assignee = meeting()->getHospitalityhost();
        role = person::Hospitality;
        break;
    default:
        break;
    }

    if (field() == Field::Theme) {
        if (meeting()->theme.discontinueDate.isValid() && meeting()->theme.discontinueDate < meetingDate) {
            emit errorChanged(tr("This talk has been discontinued. Please choose another talk."));
            return Invalid;
        }
        emit errorChanged("");
        return Acceptable;
    }

    if (assignee) {
        if (field() == Field::Congregation) {
            if (assignee->congregationName().compare(input, Qt::CaseInsensitive)) {
                emit errorChanged(tr("The speaker is no member of this congregation. Please choose another speaker or congregation."));
                return Invalid;
            }
            emit errorChanged("");
            return Acceptable;
        }

        if (field() == Field::Theme) {
            cpublictalks cpt;
            sql_items speakersThemes = cpt.getThemeList(assignee);
            int themeId = meeting()->theme.id;
            auto iterator = std::find_if(speakersThemes.begin(), speakersThemes.end(),
                                         [&themeId](const sql_item &themeItem) { return themeItem.value("theme_id").toInt() == themeId; });
            if (iterator == speakersThemes.end()) {
                emit errorChanged(tr("This talk cannot be delivered by the speaker. Please choose another talk or speaker."));
                return Invalid;
            }
            emit errorChanged("");
            return Acceptable;
        }

        if (!input.startsWith(assignee->fullname())) {
            emit errorChanged("");
            return Intermediate;
        }

        // is in own congregation
        int ownCongregationId = cc.getMyCongregation().id;
        bool isMemberOfOwnCongregation = assignee->congregationid() == ownCongregationId;
        if (!isMemberOfOwnCongregation && role != person::PublicTalk
            && !(field() == Field::FinalPrayer && meeting()->speaker() && assignee->id() == meeting()->speaker()->id())) {
            emit errorChanged(tr("The assigned person is no member of the congregation."));
            return Invalid;
        }

        // check admission
        if (!(assignee->usefor() & role)) {
            emit errorChanged(tr("Unassignable. Please check the publisher's settings."));
            return Invalid;
        }

        if (isMemberOfOwnCongregation) {
            // check availability
            bool isUnavailable = (person::UseFor::IsBreak & assignee->usefor()) > 0;
            if (!isUnavailable) {
                QList<QPair<QDate, QDate>> unavailabilityList = assignee->getUnavailabilities();
                if (!unavailabilityList.empty()) {
                    auto iterator = std::find_if(unavailabilityList.begin(), unavailabilityList.end(),
                                                 [&meetingDate](const QPair<QDate, QDate> &unavailabilityItem) {
                                                     return unavailabilityItem.first <= meetingDate
                                                             && unavailabilityItem.second >= meetingDate;
                                                 });
                    isUnavailable = iterator != unavailabilityList.end();
                }
            }
            if (isUnavailable) {
                emit errorChanged(tr("The assigned person is not available on this day."));
                return Invalid;
            }
        }
    } else {
        emit errorChanged("");
        return Acceptable;
    }
    emit errorChanged("");
    return Acceptable;
}

cptmeeting *WEMeetingValidator::meeting() const
{
    return m_meeting;
}

void WEMeetingValidator::setMeeting(cptmeeting *newMeeting)
{
    if (m_meeting == newMeeting)
        return;
    m_meeting = newMeeting;
    emit meetingChanged();
}

WEMeetingValidator::Field WEMeetingValidator::field() const
{
    return m_field;
}

void WEMeetingValidator::setField(Field newField)
{
    if (m_field == newField)
        return;
    m_field = newField;
    emit fieldChanged();
}
