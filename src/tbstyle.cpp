/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2022, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tbstyle.h"

TBStyle::TBStyle(QObject *parent)
    : QObject(parent)
{
    // initialize colors
    QPalette palette = qApp->palette();

    // create primary color
    OKLCh highlightColor = convertColor2OKLCh(palette.highlight().color());
    OKLCh primaryColor = convertColor2OKLCh(QColor(37, 79, 142, 255)); // #254f8e
    if (!qIsNaN(highlightColor.h)) {
        // derive primary color from highlight color, if it is not gray, to improve color harmony
        primaryColor.h = highlightColor.h;
    }
    colorMap[Primary] = convertOKLCh2Color(primaryColor);
    colorMap[PrimaryText] = findForegroundColor(palette.base().color(), colorMap[Primary]);
    colorMap[OnPrimary] = findForegroundColor(colorMap[Primary], QColor(255, 255, 255, 255));
    // create alert color
    colorMap[Alert] = findForegroundColor(palette.base().color(), QColor(176, 0, 32, 255));
    // create medium emphasis color
    QColor mediumSourceColor = palette.windowText().color();
    mediumSourceColor.setAlphaF(0.6);

    colorMap[Medium] = getCompositeColor(palette.base().color(), mediumSourceColor);
    colorMap[LMMSection1] = getCompositeColor(palette.base().color(), QColor(98, 98, 98, 20)); // treasures; #626262;
    colorMap[LMMSection1Text] = findForegroundColor(colorMap[LMMSection1], QColor(98, 98, 98, 255));
    colorMap[LMMSection2] = getCompositeColor(palette.base().color(), QColor(196, 132, 48, 20)); // ministry; #c48430;
    colorMap[LMMSection2Text] = findForegroundColor(colorMap[LMMSection2], QColor(196, 132, 48, 255));
    colorMap[LMMSection3] = getCompositeColor(palette.base().color(), QColor(128, 24, 22, 20)); // christian living; #801816
    colorMap[LMMSection3Text] = findForegroundColor(colorMap[LMMSection3], QColor(128, 24, 22, 255));
    colorMap[PublicTalk] = getCompositeColor(palette.base().color(), QColor(47, 72, 112, 20)); // public talk; #2f4870
    colorMap[PublicTalkText] = findForegroundColor(colorMap[PublicTalk], QColor(47, 72, 112, 255));
    colorMap[WatchtowerStudy] = getCompositeColor(palette.base().color(), QColor(77, 101, 77, 20)); // watchtower study; #4d654d
    colorMap[WatchtowerStudyText] = findForegroundColor(colorMap[WatchtowerStudy], QColor(77, 101, 77, 255));
    colorMap[OutgoingSpeakers] = getCompositeColor(palette.base().color(), QColor(169, 146, 140, 20)); // outgoing speakers
    colorMap[OutgoingSpeakersText] = findForegroundColor(colorMap[OutgoingSpeakers], QColor(169, 146, 140, 255));

    // initialize text styles
    QFont defaultFont = QApplication::font();
    fontMap[BodySmall] = QFont(defaultFont);
    fontMap[BodyMedium] = QFont(defaultFont);
    fontMap[BodyLarge] = QFont(defaultFont);
    fontMap[TitleSmall] = QFont(defaultFont);
    fontMap[TitleMedium] = QFont(defaultFont);
    fontMap[TitleLarge] = QFont(defaultFont); // Reserved for section headers of meeting parts
    fontMap[TitleLarge].setWeight(QFont::Bold);
    fontMap[TitleLarge].setCapitalization(QFont::AllUppercase);
    fontMap[TitleLarge].setPointSizeF(defaultFont.pointSizeF() * 1.5);
    fontMap[HeadlineSmall] = QFont(defaultFont);
    fontMap[HeadlineSmall].setCapitalization(QFont::AllUppercase);

#ifdef Q_OS_LINUX
    fontMap[BodySmall].setPointSizeF(defaultFont.pointSizeF() * 0.8); // Small
    fontMap[BodyLarge].setPointSizeF(defaultFont.pointSizeF() * 1.1); // Header 4
    fontMap[TitleSmall].setPointSizeF(defaultFont.pointSizeF() * 1.2); // Header 3
    fontMap[TitleMedium].setPointSizeF(defaultFont.pointSizeF() * 1.3); // Header 2
    fontMap[HeadlineSmall].setPointSizeF(defaultFont.pointSizeF() * 1.8); // Header 1
#endif
#ifdef Q_OS_MAC
    fontMap[BodySmall].setPointSizeF(defaultFont.pointSizeF() / 13.0 * 10.0); // Caption 1
    fontMap[BodyLarge].setWeight(QFont::Bold); // Headline
    fontMap[TitleSmall].setPointSizeF(defaultFont.pointSizeF() / 13.0 * 15.0); // Title 3
    fontMap[TitleMedium].setPointSizeF(defaultFont.pointSizeF() / 13.0 * 17.0); // Title 2
    fontMap[HeadlineSmall].setPointSizeF(defaultFont.pointSizeF() / 13.0 * 22.0); // Title 1
#endif
#ifdef Q_OS_WIN
    fontMap[BodySmall].setPointSizeF(defaultFont.pointSizeF() / 14.0 * 12.0); // Caption
    fontMap[BodyLarge].setWeight(QFont::DemiBold); // Body Large
    fontMap[TitleSmall].setPointSizeF(defaultFont.pointSizeF() / 14.0 * 18.0); // Subtitle
    fontMap[TitleMedium].setPointSizeF(defaultFont.pointSizeF() / 14.0 * 20.0); // Title
    fontMap[TitleMedium].setWeight(QFont::DemiBold);
    fontMap[HeadlineSmall].setPointSizeF(defaultFont.pointSizeF() / 14.0 * 28.0); // Subheader
    fontMap[HeadlineSmall].setWeight(QFont::DemiBold);
#endif
}

QColor TBStyle::getCompositeColor(QColor backgroundColor, QColor overlayColor)
{
    if (overlayColor.alpha() == 0)
        return backgroundColor;
    else if (overlayColor.alpha() == 1)
        return overlayColor;

    qreal alpha = overlayColor.alphaF();
    QColor compositeColor;
    qreal red = (((overlayColor.redF() * alpha) + (backgroundColor.redF() * backgroundColor.alphaF() * (1 - alpha))));
    qreal green = (((overlayColor.greenF() * alpha) + (backgroundColor.greenF() * backgroundColor.alphaF() * (1 - alpha))));
    qreal blue = (((overlayColor.blueF() * alpha) + (backgroundColor.blueF() * backgroundColor.alphaF() * (1 - alpha))));
    alpha = 1 - (1 - backgroundColor.alphaF()) * (1 - alpha);
    compositeColor.setRgbF(red, green, blue, alpha);
    return compositeColor;
}

TBStyle::OKLCh TBStyle::convertOKLab2OKLCh(OKLab sourceColor)
{
    const qreal e = 0.0005;
    OKLCh color;
    color.L = sourceColor.L;
    if (qAbs(sourceColor.a) < e && qAbs(sourceColor.b) < e) {
        color.C = 0;
        color.h = qQNaN();
    } else {
        color.C = sqrt(pow(sourceColor.a, 2) + pow(sourceColor.b, 2));
        color.h = atan2(sourceColor.b, sourceColor.a) >= 0
                ? qRadiansToDegrees(atan2(sourceColor.b, sourceColor.a))
                : 360.0 - abs(qRadiansToDegrees(atan2(sourceColor.b, sourceColor.a)));
    }
    return color;
}

TBStyle::OKLab TBStyle::convertOKLCh2OKLab(OKLCh sourceColor)
{
    OKLab color;
    color.L = sourceColor.L;
    if (qIsNaN(sourceColor.h)) {
        color.a = 0;
        color.b = 0;
    } else {
        color.a = sourceColor.C * cos(qDegreesToRadians(sourceColor.h));
        color.b = sourceColor.C * sin(qDegreesToRadians(sourceColor.h));
    }
    return color;
}

// Convert QColor to OKLCh (Oklab color space)
// Values taken from here: https://bottosson.github.io/posts/oklab/
TBStyle::OKLCh TBStyle::convertColor2OKLCh(QColor sourceColor)
{
    qreal r = sourceColor.redF();
    qreal g = sourceColor.greenF();
    qreal b = sourceColor.blueF();

    // convert to linear rgb
    r = r <= 0.04045
            ? r / 12.92
            : pow(((r + 0.055) / 1.055), 2.4);
    g = g <= 0.04045
            ? g / 12.92
            : pow(((g + 0.055) / 1.055), 2.4);
    b = b <= 0.04045
            ? b / 12.92
            : pow(((b + 0.055) / 1.055), 2.4);

    qreal l = 0.4122214708 * r + 0.5363325363 * g + 0.0514459929 * b;
    qreal m = 0.2119034982 * r + 0.6806995451 * g + 0.1073969566 * b;
    qreal s = 0.0883024619 * r + 0.2817188376 * g + 0.6299787005 * b;

    l = pow(l, 1.0 / 3.0);
    m = pow(m, 1.0 / 3.0);
    s = pow(s, 1.0 / 3.0);

    OKLab color;
    color.L = 0.2104542553 * l + 0.7936177850 * m - 0.0040720468 * s;
    color.a = 1.9779984951 * l - 2.4285922050 * m + 0.4505937099 * s;
    color.b = 0.0259040371 * l + 0.7827717662 * m - 0.8086757660 * s;
    return convertOKLab2OKLCh(color);
}

// Convert OKLCh (Oklab color space) to QColor
// Values taken from here: https://bottosson.github.io/posts/oklab/
QColor TBStyle::convertOKLCh2Color(OKLCh sourceColor)
{
    OKLab color = convertOKLCh2OKLab(sourceColor);

    qreal l = pow(1.0 * color.L + 0.3963377774 * color.a + 0.2158037573 * color.b, 3);
    qreal m = pow(1.0 * color.L - 0.1055613458 * color.a - 0.0638541728 * color.b, 3);
    qreal s = pow(1.0 * color.L - 0.0894841775 * color.a - 1.2914855480 * color.b, 3);

    qreal r = +4.0767416621 * l - 3.3077115913 * m + 0.2309699292 * s;
    qreal g = -1.2684380046 * l + 2.6097574011 * m - 0.3413193965 * s;
    qreal b = -0.0041960863 * l - 0.7034186147 * m + 1.7076147010 * s;

    // apply gamma
    r = r <= 0.0031308
            ? 12.92 * r
            : 1.055 * pow(r, 1.0 / 2.4) - 0.055;
    g = g <= 0.0031308
            ? 12.92 * g
            : 1.055 * pow(g, 1.0 / 2.4) - 0.055;
    b = b <= 0.0031308
            ? 12.92 * b
            : 1.055 * pow(b, 1.0 / 2.4) - 0.055;

    QColor rgbColor;
    rgbColor.setRgbF(qMax(0.0, qMin(1.0, r)),
                     qMax(0.0, qMin(1.0, g)),
                     qMax(0.0, qMin(1.0, b)));
    return rgbColor;
}

qreal TBStyle::getContrastRatio(QColor lightColor, QColor darkColor)
{
    qreal fR = lightColor.redF() <= 0.03928 ? lightColor.redF() / 12.92 : qPow((lightColor.redF(), 0.055) / 1.055, 2.4);
    qreal fG = lightColor.greenF() <= 0.03928 ? lightColor.greenF() / 12.92 : qPow((lightColor.greenF() + 0.055) / 1.055, 2.4);
    qreal fB = lightColor.blueF() <= 0.03928 ? lightColor.blueF() / 12.92 : qPow((lightColor.blueF() + 0.055) / 1.055, 2.4);
    qreal L1 = 0.2126 * fR + 0.7152 * fG + 0.0722 * fB;
    qreal bR = darkColor.redF() <= 0.03928 ? darkColor.redF() / 12.92 : qPow((darkColor.redF() + 0.055) / 1.055, 2.4);
    qreal bG = darkColor.greenF() <= 0.03928 ? darkColor.greenF() / 12.92 : qPow((darkColor.greenF() + 0.055) / 1.055, 2.4);
    qreal bB = darkColor.blueF() <= 0.03928 ? darkColor.blueF() / 12.92 : qPow((darkColor.blueF() + 0.055) / 1.055, 2.4);
    qreal L2 = 0.2126 * bR + 0.7152 * bG + 0.0722 * bB;
    return (L1 + 0.05) / (L2 + 0.05);
}

qreal TBStyle::getOKLChColorDifference(OKLCh color1, OKLCh color2)
{
    return sqrt(pow(color1.L - color2.L, 2)
                + pow(color1.C - color2.C, 2)
                + pow((qIsNaN(color1.h) ? 0.0 : color1.h) - (qIsNaN(color2.h) ? 0.0 : color2.h), 2));
}

QColor TBStyle::findForegroundColor(QColor backgroundColor, QColor sourceColor, qreal minimumContrastRatio)
{
    OKLCh backgroundOKLCh = convertColor2OKLCh(backgroundColor);
    OKLCh sourceOKLCh = convertColor2OKLCh(sourceColor);
    QColor lightColor = backgroundColor;
    QColor darkColor = sourceColor;
    bool isSourceColorLighter = sourceOKLCh.L > backgroundOKLCh.L;
    if (isSourceColorLighter) {
        // probably dark theme, and we are looking for a lighter color
        lightColor = sourceColor;
        darkColor = backgroundColor;
    }
    // check if contrast is sufficient already
    qreal cr = getContrastRatio(lightColor, darkColor);
    if (cr > minimumContrastRatio) {
        return sourceColor;
    }
    // contrast is insufficient; find new color
    QColor newColor = sourceColor;
    // Dark mode: Lightness between L and L + 0.10; Chroma: between C or 0.07 and 0.12
    // Light mode:          between 0 and L                 between C + 0.10 and 0.20
    int minL = isSourceColorLighter ? int(sourceOKLCh.L * 100) : 0;
    int maxL = isSourceColorLighter ? qMax(int(sourceOKLCh.L * 100) + 10, 100) : int(sourceOKLCh.L * 100);
    int minC = isSourceColorLighter ? qMin(int(sourceOKLCh.C * 100), 7) : int(sourceOKLCh.C * 100);
    int maxC = isSourceColorLighter ? 12 : qMax(int(sourceOKLCh.C * 100) + 10, 20);
    qreal minColorDifference = std::numeric_limits<qreal>::max();
    OKLCh currentOKLCh;
    currentOKLCh.h = sourceOKLCh.h;
    for (int C = minC; C <= maxC; C++) {
        currentOKLCh.C = C * 0.01;
        for (int L = minL; L <= maxL; L++) {
            currentOKLCh.L = L * 0.01;
            QColor currColor = convertOKLCh2Color(currentOKLCh);
            qreal currCR;
            if (isSourceColorLighter)
                currCR = getContrastRatio(currColor, darkColor);
            else
                currCR = getContrastRatio(lightColor, currColor);
            qreal currColorDifference = getOKLChColorDifference(sourceOKLCh, currentOKLCh);
            // check for sufficient contrast to pass WCAG’s standard for text
            // and save the closest value to the source color
            if (currCR > minimumContrastRatio && currColorDifference < minColorDifference) {
                newColor = currColor;
                cr = currCR;
                minColorDifference = currColorDifference;
            }
        }
    }
    return newColor;
}
