#ifndef XML_READER_H
#define XML_READER_H

#include <QString>
#include <QFile>
#include <QList>
#include <QMap>
#include <QRegularExpression>
#include <QXmlStreamReader>
#include <QDebug>

#define XLM_READER_CONNECT_TO(reader, callback) connect(&reader, SIGNAL(found(QXmlStreamReader*, QXmlStreamReader::TokenType, int, int)), this, SLOT(callback(QXmlStreamReader*, QXmlStreamReader::TokenType, int, int)));
#define XLM_READER_CONNECT(reader) XLM_READER_CONNECT_TO(reader, xmlPartFound);

class xml_reader : public QObject
{
    Q_OBJECT

    class search
    {
    public:
        QString element_name;
        QString attribute_name;
        QString attribute_value;
        int context;
        int searchContext;
        bool emitAllChildren;

        search(QString element_name, QString attribute_name, QString attribute_value, int context, int searchContext, bool emitAllChildren) :
            element_name(element_name),
            attribute_name(attribute_name),
            attribute_value(attribute_value),
            context(context),
            searchContext(searchContext),
            emitAllChildren(emitAllChildren)
        {}
    };

public:

    xml_reader(QString fileName);

    void register_elementsearch(QString name, int context, int searchContext = -1, bool emitAllChildren = false);
    void register_elementsearch(QXmlStreamReader::TokenType tokenType, QString name, int context, int searchContext = -1, bool emitAllChildren = false);
    void register_attributesearch(QString element_name, QString attribute_name, QString attribute_value, int context, int searchContext = -1, bool emitAllChildren = false);
    void register_attributesearch(QXmlStreamReader::TokenType tokenType, QString element_name, QString attribute_name, QString attribute_value, int context, int searchContext = -1, bool emitAllChildren = false);

    // returns true if unable to read file
    bool read();

    bool quitRequest;

signals:
    void found(QXmlStreamReader*, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth);

private:
    QString fileName;
    QMap<QXmlStreamReader::TokenType, QList<search*>*> searches;
};

#endif // XML_READER_H
