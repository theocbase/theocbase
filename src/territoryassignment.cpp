/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "territoryassignment.h"
#include <QDebug>

TerritoryAssignment::TerritoryAssignment(int territoryId)
    : m_id(0), m_territoryId(territoryId), m_personId(0), m_publisher(""), m_isDirty(false)
{
}

TerritoryAssignment::TerritoryAssignment(const int id, const int territoryId, const int personId, const QString publisher, const QDate checkedOutDate, const QDate checkedBackInDate)
    : m_id(id), m_territoryId(territoryId), m_personId(personId), m_publisher(publisher), m_checkedOutDate(checkedOutDate), m_checkedBackInDate(checkedBackInDate), m_isDirty(false)
{
}

int TerritoryAssignment::id() const
{
    return m_id;
}

int TerritoryAssignment::territoryId() const
{
    return m_territoryId;
}

int TerritoryAssignment::personId() const
{
    return m_personId;
}

void TerritoryAssignment::setPersonId(const int value)
{
    if (m_personId != value) {
        m_personId = value;

        cpersons persons;
        person *publisher = persons.getPerson(m_personId);
        if (publisher)
            m_publisher = publisher->fullname();

        setIsDirty(true);
    }
}

QString TerritoryAssignment::publisher() const
{
    return m_publisher;
}

//void TerritoryRecordItem::setPublisher(const QString &value)
//{
//    publisher = value;
//}

QDate TerritoryAssignment::checkedOutDate() const
{
    return m_checkedOutDate;
}

void TerritoryAssignment::setCheckedOutDate(const QDate value)
{
    if (m_checkedOutDate != value) {
        m_checkedOutDate = value;
        setIsDirty(true);
    }
}

QDate TerritoryAssignment::checkedBackInDate() const
{
    return m_checkedBackInDate;
}

void TerritoryAssignment::setCheckedBackInDate(const QDate value)
{
    if (m_checkedBackInDate != value) {
        m_checkedBackInDate = value;
        setIsDirty(true);
    }
}

void TerritoryAssignment::setIsDirty(bool value)
{
    m_isDirty = value;
}

bool TerritoryAssignment::save()
{
    if (personId() < 1)
        return false;

    // save changes to database
    sql_class *sql = &Singleton<sql_class>::Instance();

    int lang_id = sql->getLanguageDefaultId();

    sql_item queryitems;
    queryitems.insert(":id", m_id);
    int assignmentId = m_id > 0 ? sql->selectScalar("SELECT id FROM territory_assignment WHERE id = :id", &queryitems, -1).toInt() : -1;

    sql_item insertItems;
    insertItems.insert("territory_id", territoryId());
    insertItems.insert("person_id", personId());
    insertItems.insert("checkedout_date", checkedOutDate());
    insertItems.insert("checkedbackin_date", checkedBackInDate());
    insertItems.insert("lang_id", lang_id);

    bool ret = false;
    if (assignmentId > 0) {
        // update
        ret = sql->updateSql("territory_assignment", "id", QString::number(assignmentId), &insertItems);
    } else {
        // insert new row
        int newId = sql->insertSql("territory_assignment", &insertItems, "id");
        ret = newId > 0;
        if (newId > 0)
            m_id = newId;
    }
    setIsDirty(!ret);

    return ret;
}

TerritoryAssignmentModel::TerritoryAssignmentModel()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString w("SELECT p.id, p.firstname, p.lastname, p.active "
              "FROM persons p "
              "LEFT JOIN territory_assignment a ON p.id = a.person_id "
              "WHERE (p.active OR a.territory_id IS NOT NULL) AND (p.congregation_id IS NULL OR p.congregation_id = %1) "
              "GROUP BY p.id, p.lastname, p.firstname "
              "ORDER BY p.lastname, p.firstname, p.id");
    w.replace("%1", sql->getSetting("congregation_id"));
    sql_items items = sql->selectSql(w);

    DataObjectListModel *publisherListModel = new DataObjectListModel();
    // empty row
    publisherListModel->addDataObject(DataObject(0,
                                                 "",
                                                 "#7F7F7F"));
    for (unsigned int i = 0; i < items.size(); i++) {
        publisherListModel->addDataObject(DataObject(items[i].value("id").toInt(),
                                                     items[i].value("firstname").toString() + " " + items[i].value("lastname").toString(),
                                                     items[i].value("active").toBool() ? "#000000" : "#7F7F7F"));
    }
    m_publisherList = publisherListModel;
}

TerritoryAssignmentModel::TerritoryAssignmentModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

QHash<int, QByteArray> TerritoryAssignmentModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[AssignmentIdRole] = "id";
    items[PersonIdRole] = "personId";
    items[PublisherIndexRole] = "publisherIndex";
    items[CheckedOutRole] = "checkedOutDate";
    items[CheckedBackInRole] = "checkedBackInDate";
    items[PublisherRole] = "publisher";
    return items;
}

int TerritoryAssignmentModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return territoryAssignments.empty() ? 0 : territoryAssignments.count();
}

int TerritoryAssignmentModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 5;
}

QVariantMap TerritoryAssignmentModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QVariant TerritoryAssignmentModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > territoryAssignments.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return territoryAssignments[index.row()].id();
        case 1:
            return territoryAssignments[index.row()].personId();
        case 2: {
            int personId = territoryAssignments[index.row()].personId();
            return m_publisherList->getIndex(personId);
        }
        case 3:
            return territoryAssignments[index.row()].checkedOutDate().startOfDay();
        case 4:
            return territoryAssignments[index.row()].checkedBackInDate().startOfDay();
        case 5:
            return territoryAssignments[index.row()].publisher();
        }
    }

    switch (role) {
    case AssignmentIdRole:
        return territoryAssignments[index.row()].id();
    case PersonIdRole:
        return territoryAssignments[index.row()].personId();
    case PublisherIndexRole: {
        int personId = territoryAssignments[index.row()].personId();
        return m_publisherList->getIndex(personId);
    }
    case CheckedOutRole:
        return territoryAssignments[index.row()].checkedOutDate().startOfDay();
    case CheckedBackInRole:
        return territoryAssignments[index.row()].checkedBackInDate().startOfDay();
    case PublisherRole:
        return territoryAssignments[index.row()].publisher();
    }

    return QVariant();
}

bool TerritoryAssignmentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.row() < 0 || index.row() > territoryAssignments.count())
        return false;
    TerritoryAssignment *currTerritoryAssignment = &territoryAssignments[index.row()];

    if (role == Qt::EditRole) {
        int column = index.column();

        switch (column) {
        case 0:
            //assignmentId
            break;
        case 1:
            currTerritoryAssignment->setPersonId(value.toInt());
            break;
        case 2:
            currTerritoryAssignment->setPersonId(m_publisherList->getId(value.toInt()));
            break;
        case 3: {
            QString dateFormat = QLocale().dateFormat(QLocale::ShortFormat);
            QDate date = QDate::fromString(value.toString(), dateFormat);
            if (!dateFormat.contains("yyyy"))
                date = date.addYears(99).year() > QDate::currentDate().year() ? date : date.addYears(100);
            currTerritoryAssignment->setCheckedOutDate(date);
            break;
        }
        case 4: {
            QString dateFormat = QLocale().dateFormat(QLocale::ShortFormat);
            QDate date = QDate::fromString(value.toString(), dateFormat);
            if (!dateFormat.contains("yyyy"))
                date = date.addYears(99).year() > QDate::currentDate().year() ? date : date.addYears(100);
            currTerritoryAssignment->setCheckedBackInDate(date);
            break;
        }
        case 5: {
            int index = m_publisherList->getIndex(value.toString());
            int id = m_publisherList->getId(index);
            currTerritoryAssignment->setPersonId(id);
            break;
        }
        default:
            break;
        }

        emit editCompleted();
    } else {
        switch (role) {
        case Roles::PersonIdRole:
            currTerritoryAssignment->setPersonId(value.toInt());
            emit dataChanged(this->index(index.row(), 1), this->index(index.row(), 2));
            break;
        case Roles::PublisherIndexRole: {
            int personId = m_publisherList->getId(value.toInt());
            currTerritoryAssignment->setPersonId(personId);
            break;
        }
        case Roles::CheckedOutRole:
            currTerritoryAssignment->setCheckedOutDate(value.toDate());
            emit dataChanged(this->index(index.row(), 3), this->index(index.row(), 3));
            break;
        case Roles::CheckedBackInRole:
            currTerritoryAssignment->setCheckedBackInDate(value.toDate());
            emit dataChanged(this->index(index.row(), 4), this->index(index.row(), 4));
            break;
        case Roles::PublisherRole: {
            int index = m_publisherList->getIndex(value.toString());
            int id = m_publisherList->getId(index);
            currTerritoryAssignment->setPersonId(id);
            break;
        }
        default:
            break;
        }
    }

    return true;
}

Qt::ItemFlags TerritoryAssignmentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

QModelIndex TerritoryAssignmentModel::index(int row, int column, const QModelIndex &parent)
        const
{
    if (hasIndex(row, column, parent)) {
        return createIndex(row, column);
    }
    return QModelIndex();
}

QModelIndex TerritoryAssignmentModel::getAssignmentIndex(int assignmentId) const
{
    for (int row = 0; row < this->rowCount(); ++row) {
        QModelIndex rowIndex = this->index(row, 0);
        if (rowIndex.data(AssignmentIdRole) == assignmentId)
            return rowIndex;
    }
    return QModelIndex();
}

bool TerritoryAssignmentModel::addAssignment(const TerritoryAssignment &territoryAssignment)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    territoryAssignments << territoryAssignment;
    endInsertRows();
    return true;
}

bool TerritoryAssignmentModel::addAssignment(int territoryId)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    TerritoryAssignment *newTerritoryAssignment = new TerritoryAssignment(territoryId);

    if (!territoryAssignments.isEmpty()) {
        // sort by checked out date
        std::sort(territoryAssignments.begin(), territoryAssignments.end(),
                  [](TerritoryAssignment const &a, TerritoryAssignment const &b) {
                      return a.checkedOutDate() < b.checkedOutDate();
                  });

        TerritoryAssignment *lastAssignment = &territoryAssignments.last();
        if (!lastAssignment->checkedBackInDate().isValid()) {
            QModelIndex lastAssignmentIndex = this->index(territoryAssignments.count() - 1, 0);
            this->setData(lastAssignmentIndex, QDate::currentDate(), Roles::CheckedBackInRole);
        }

        newTerritoryAssignment->setPersonId(lastAssignment->personId());
        newTerritoryAssignment->setCheckedOutDate(lastAssignment->checkedBackInDate().addDays(1));
    }

    territoryAssignments << *newTerritoryAssignment;
    endInsertRows();
    return true;
}

void TerritoryAssignmentModel::removeAssignment(int id)
{
    if (id > 0) {
        sql_class *sql = &Singleton<sql_class>::Instance();

        sql_item s;
        s.insert(":id", id);
        s.insert(":active", 0);
        if (sql->execSql("UPDATE territory_assignment SET active = :active WHERE id = :id", &s, true)) {
            QModelIndex assignmentIndex = getAssignmentIndex(id);
            if (assignmentIndex.isValid()) {
                int row = assignmentIndex.row();
                beginRemoveRows(QModelIndex(), row, row);
                territoryAssignments.erase(std::next(territoryAssignments.begin(), row));
                endRemoveRows();
            }
        }
    }
}

bool TerritoryAssignmentModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    if (row < 0 || count < 1 || (row + count) > territoryAssignments.size())
        return false;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    for (int i = 0; i < count; i++) {
        territoryAssignments.removeAt(row);
    }
    endRemoveRows();
    return true;
}

void TerritoryAssignmentModel::loadAssignments(int territoryId)
{
    removeRows(0, territoryAssignments.count());
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_items territoryAssignmentRows = sql->selectSql("SELECT * FROM territoryassignments WHERE territory_id = " + QVariant(territoryId).toString() + " ORDER BY checkedout_date");

    if (!territoryAssignmentRows.empty()) {
        for (unsigned int i = 0; i < territoryAssignmentRows.size(); i++) {
            sql_item s = territoryAssignmentRows[i];

            addAssignment(TerritoryAssignment(s.value("id").toInt(),
                                              s.value("territory_id").toInt(),
                                              s.value("person_id").toInt(),
                                              s.value("firstname").toString() + " " + s.value("lastname").toString(),
                                              s.value("checkedout_date").toDate(),
                                              s.value("checkedbackin_date").toDate()));
        }
    }
}

void TerritoryAssignmentModel::saveAssignments()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();
    for (int i = 0; i < territoryAssignments.count(); i++) {
        TerritoryAssignment t = territoryAssignments[i];
        if (t.isDirty()) {
            t.save();
        }
    }
    sql->commitTransaction();
}

DataObjectListModel *TerritoryAssignmentModel::getPublisherList() const
{
    return m_publisherList;
}

bool TerritoryAssignmentModel::MySortFilterProxyModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    int row1 = source_left.row();
    int row2 = source_right.row();

    if (row1 == 0 || row2 == 0)
        return row1 < row2;
    bool ret = false;

    switch (this->sortRole()) {
    case MySortFilterProxyModel::MyRoles::name:
        break;
    case MySortFilterProxyModel::MyRoles::date: {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_offset);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_offset);
        float f1 = val1.toFloat();
        float f2 = val2.toFloat();
        if (f1 > f2)
            return false;
        ret = f1 < f2;
        break;
    }
    default:
        return SortFilterProxyModel::lessThan(source_left, source_right);
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_lastname);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_lastname);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::h_firstname);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::h_firstname);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    if (!ret) {
        QVariant val1 = sourceModel()->data(source_left, MySortFilterProxyModel::MyRoles::id);
        QVariant val2 = sourceModel()->data(source_right, MySortFilterProxyModel::MyRoles::id);
        if (val1.toString() > val2.toString())
            return false;
        ret = val1.toString() < val2.toString();
    }
    return ret;
}
