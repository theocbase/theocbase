/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IMPORTWINTM_H
#define IMPORTWINTM_H

#include <QString>
#include <QFile>
#include <QDebug>
#include "person.h"

class importwintm
{
public:
    importwintm(QString filename);
    void Go();

private:
    void importpeople(QString filename);
    sql_class *sql;
    QDate thisweek;

    /**
     * @brief m_directory - directory of WINTM data selected by user
     */
    QDir m_directory;

    /**
     * @brief data - contents of file being imported
     */
    QByteArray data;

    /**
     * @brief pos - next position to look at within "data"
     */
    int pos;

    /**
     * @brief oldIdToNewID - key is WINTM person ID, value is our person ID
     */
    QHash<int, int> oldIdToNewID;

    /**
     * @brief readstring - reads a string from "data"
     * @param maxlen - maximum string length to read
     *        uses and advances pos
     * @return QString
     */
    QString readstring(int maxlen);

    /**
     * @brief readint4 - read next 4 byte int in "data"
     *        uses and advances pos
     * @param LittleEndian - if data is Little Endian. False if Big Endian
     * @return uint - data that was read
     */
    uint readint4(bool LittleEndian);

    /**
     * @brief readchar - read next character in "data"
     *        uses and advances pos
     * @return - char at "pos"
     */
    char readchar();
};

#endif // IMPORTWINTM_H
