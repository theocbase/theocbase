#ifndef LMM_ASSIGNMENT_H
#define LMM_ASSIGNMENT_H

#include "lmm_schedule.h"
#include "person.h"
#include "ccongregation.h"
#include <QAbstractListModel>
#include <QStandardItemModel>
#include "sortfilterproxymodel.h"
#include <QDebug>
#include <QList>
#include <QVector>

class LMM_Assignment : public LMM_Schedule
{
    Q_OBJECT
    Q_PROPERTY(AssignmentInfo *assignmentInfo READ assignmentInfo CONSTANT)
    Q_PROPERTY(person *speaker READ speaker WRITE setSpeaker NOTIFY speakerChanged)
    Q_PROPERTY(QString speakerFullName READ speakerFullName NOTIFY speakerChanged)
    Q_PROPERTY(QString note READ note WRITE setNote NOTIFY noteChanged)
    Q_PROPERTY(int classnumber READ classnumber WRITE setClassnumber NOTIFY classnumberChanged)

public:
    class MySortFilterProxyModel : public SortFilterProxyModel
    {
    public:
        enum MyRoles {
            id = Qt::UserRole + 1,
            name,
            date,
            date2,
            icon,
            // hidden fields
            date_val,
            date2_val,
            h_firstname,
            h_lastname,
            h_offset_any,
            h_offset_specific
        };
        explicit MySortFilterProxyModel(QObject *parent = nullptr, QStandardItemModel *itemmodel = nullptr, int defaultSortRole = 0, QString settingName = "")
            : SortFilterProxyModel(parent, itemmodel, defaultSortRole, settingName) { }
        virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const;
    };

public:
    LMM_Assignment(QObject *parent = nullptr);
    LMM_Assignment(AssignmentInfo *assignmentInfo, int sequence, int scheduleDbId, int assignmentDbId, QObject *parent = nullptr);
    ~LMM_Assignment();

    AssignmentInfo *assignmentInfo() const;

    int classnumber() const;
    void setClassnumber(int n);

    person *speaker() const;
    void setSpeaker(person *speaker);
    QString speakerFullName() const;

    QString note() const;
    void setNote(QString note);

    Q_INVOKABLE SortFilterProxyModel *getSpeakerList(); // for mobile only
    Q_INVOKABLE QString getReminderText() const;
    Q_INVOKABLE bool save();
signals:
    void speakerChanged();
    void noteChanged(QString note);
    void classnumberChanged(int cls);
public slots:

protected:
    AssignmentInfo *m_assignmentInfo;
    int m_classnumber;
    int m_assistant_id;
    int m_volunteer_id;
    int m_assignment_db_id;
    bool m_completed;
    QString m_timing;
    QString m_setting;

public:
    static QList<int> &loadPersonsOnBreak(QList<int> &, const QDate &week_starting);
    static QList<int> &loadMultiAssignedPersons(QList<int> &, const QDate &week_starting);
    static QList<int> &loadFamilyMembers(QList<int> &, const QDate &week_starting);
    static void getPersonsIconAlertValues(QVector<short> &, const QVector<int> &person_ids, const QVector<QList<int>> &);
    static void getPersonIdsFromQryResult(QVector<int> &person_ids, const sql_items &items, const QString &person_fldnm);

protected:
    QList<int> &loadFamilyMembers(QList<int> &dst)
    {
        return LMM_Assignment::loadFamilyMembers(dst, date());
    }
    QList<int> &loadPersonsOnBreak(QList<int> &dst)
    {
        ccongregation c;
        return LMM_Assignment::loadPersonsOnBreak(dst, date().addDays(c.getMeetingDay(date(), ccongregation::tms) - 1));
    }
    QList<int> &loadMultiAssignedPersons(QList<int> &dst)
    {
        return LMM_Assignment::loadMultiAssignedPersons(dst, date());
    }

private:
    QString m_note;
    person *m_speaker;
};

#endif // LMM_ASSIGNMENT_H
