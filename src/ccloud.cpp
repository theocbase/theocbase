/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ccloud.h"

ccloud::ccloud(QObject *parent) : QThread(parent)
{
    QSettings settings;
    mUserId = settings.value("cloud/id",-1).toInt();
}

bool ccloud::isLogged()
{
    return (mUserId > -1);
}

QString ccloud::generateQRCode(QString scheduleName)
{
    qDebug() << "qr code generation started";
    QSettings settings;

    ccongregation cong;
    QString value = scheduleName +
            settings.value("cloud/id").toString() +
            cong.getMyCongregation().name;
    QString md5hash = QCryptographicHash::hash(
                value.toLatin1(),QCryptographicHash::Md5).toHex();
    QString path = QDir::tempPath() + "/" + md5hash + ".png";
    if (QFile::exists(path) ) return path;

    QNetworkAccessManager *m_manager = new QNetworkAccessManager(this);
    QNetworkReply *reply = m_manager->get(
                QNetworkRequest(QUrl(QString("https://api.theocbase.net/api.php?qrcode=qrcode&data=%1").arg(
                                         "https://schedule.theocbase.net/" + md5hash + ".html") )));

    QEventLoop loop;
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();

    if (reply->error() == QNetworkReply::NoError) {
        QFile file( path );
        file.open(QIODevice::WriteOnly);
        file.write(reply->readAll());
        qDebug() << "generateQRCode OK";
        reply->deleteLater();
        return path;
    } else {
        qDebug() << "generateQRCode Error" << reply->errorString();
        QMessageBox::information(0,"","Error when generating QR Code\n" + reply->errorString());
        reply->deleteLater();
        return "";
    }
}

QString ccloud::uploadHtmlSchedule(QString htmlpath)
{
    qDebug() << "upload test";
    QFile *file = new QFile( htmlpath );
    QFileInfo info( htmlpath );
    if (!file->exists()){
        qDebug() << "file not exists";
        return "";
    }

    QSettings settings;

    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"upload\""));
    textPart.setBody("upload");

    QHttpPart userPart;
    userPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"user_id\""));
    userPart.setBody( settings.value("cloud/id").toString().toLatin1() );

    QHttpPart filePart;
    filePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/html"));
    filePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(
                            QString("form-data; name=\"userfile\"; filename=\"%1\"").arg( info.fileName() )));

    file->open(QIODevice::ReadOnly);
    filePart.setBodyDevice(file);
    file->setParent(multiPart);

    multiPart->append(textPart);
    multiPart->append(userPart);
    multiPart->append(filePart);


    QUrl url("https://api.theocbase.net/api.php");
    QNetworkRequest request(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkReply *reply = manager->post(request,multiPart);
    multiPart->setParent(reply);

    QEventLoop loop;
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();
    QString replystr = reply->readAll();

    if (reply->error() == QNetworkReply::NoError) {
        if (replystr == "true"){
            return "https://schedule.theocbase.net/" + info.fileName();
        }else{
            return "";
        }
    } else {
        QMessageBox::information(0,"","Error when uploding html-schedule to cloud\n" + reply->errorString());
        return "";
    }
}

bool ccloud::convertPdfToJpg(QString input, QString output)
{
    QFile inputfile(input);    
    if (!inputfile.exists()) return false;

    QHttpMultiPart *multipart = new QHttpMultiPart(QHttpMultiPart::FormDataType,this);
    QHttpPart namePart;
    namePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"pdftojpg\""));
    QHttpPart filePart;
    filePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/pdf"));
    filePart.setHeader(QNetworkRequest::ContentDispositionHeader,
                       QVariant(QString("form-data; name=\"pdf\"; filename=\"%1\"").arg(QFileInfo(input).fileName())));

    inputfile.open(QIODevice::ReadOnly);
    filePart.setBodyDevice(&inputfile);
    inputfile.setParent(multipart);

    multipart->append(namePart);
    multipart->append(filePart);

    QUrl url("https://api.theocbase.net/api.php");
    QNetworkRequest request(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    QNetworkReply *reply = manager->post(request,multipart);
    multipart->setParent(reply);

    QEventLoop loop;
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();

    if (reply->error() == QNetworkReply::NoError){
        QFile outputfile(output);
        outputfile.open(QIODevice::WriteOnly);
        outputfile.write(reply->readAll());
        reply->deleteLater();
        return true;
    }else{
        qDebug() << reply->errorString();
        reply->deleteLater();
        return false;
    }
}

int ccloud::userId()
{
    QSettings settings;
    return settings.value("cloud/id",-1).toInt();
}
