/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.0
import Qt.labs.calendar 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

ColumnLayout {
    id: todoEdit

    // property declarations
    property bool isIncomingSpeaker: true
    property bool isLoading: true
    property var editId

    // signal declarations
    signal saveChanges()
    signal movedToSchedule(var date, var incoming)

    // javascript functions
    function reloadTodoList() {
        todoModel.loadList()
    }

    function resetDefaultTodoLookupControlSettings() {
        settings.todo_groupByIndex = 1;
        todoLookupControl.groupByIndex = settings.todo_groupByIndex;
        settings.todo_sortOrder = Qt.AscendingOrder;
        todoLookupControl.sortOrder = settings.todo_sortOrder;
        settings.sync();
        reloadTodoList();
    }

    // object properties
    Layout.fillWidth: true

    Settings {
        id: settings
        category: "LookupControl"
        property int todo_groupByIndex: 1
        property bool todo_sortOrder: Qt.AscendingOrder
        property int todo_detailRowCount: 3
        property int publicTalk_groupByIndex: 1
        property bool publicTalk_sortOrder: Qt.AscendingOrder
        property int publicTalk_detailRowCount: 3
        property int congregation_groupByIndex: 1
        property bool congregation_sortOrder: Qt.AscendingOrder
        property int congregation_detailRowCount: 3
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property int nonstudentAssignment_detailRowCount: 3
        property bool nonstudentAssignment_hideUnavailables: true
    }

    // child objects
    TodoModel { id: todoModel }
    TodoSFProxyModel {
        id: todoProxyModel
        source: todoModel
        isIncoming: isIncomingSpeaker
        onGroupByChanged: todoProxyModel.sort(0, todoLookupControl.sortOrder)
    }

    PublicTalkDetailGroupByModel { id: todoGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    LookupControlMoreMenu {
        id: todoLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isRowCountChangingAllowed: false
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        isHideUnavailablesAllowed: false
        onResetDefaultSettings: resetDefaultTodoLookupControlSettings()
    }

    MessageDialog {
        id: warningDialog
    }

    Component.onCompleted: {
        todoGroupByModel.remove(4); // remove assignment
        todoGroupByModel.remove(3); // remove frequency
        todoGroupByModel.remove(2); // remove meeting part
        todoGroupByModel.setProperty(0, "value", "speakerFullName");
        todoLookupControl.groupByIndex = settings.todo_groupByIndex;
        todoLookupControl.sortOrder = settings.todo_sortOrder;

        reloadTodoList();
    }

    Component.onDestruction: {
        settings.todo_groupByIndex = todoLookupControl.groupByIndex;
        settings.todo_sortOrder = todoLookupControl.sortOrder;
        settings.sync();
    }

    StackView {
        id: todoStack

        Layout.fillWidth: true
        Layout.fillHeight: true

        initialItem: ColumnLayout {
            Label {
                text: qsTr("To Do List")
                Layout.fillWidth: true
                font: TBStyle.titleSmallFont
            }

            LookupControl {
                id: todoLookupControl

                Layout.fillWidth: true
                Layout.fillHeight: true
                clip: true
                focus: true

                showFilterControls: false
                showGroupControls: true
                groupByIndex: -1
                showEditButton: true
                isEditing: false

                model: todoProxyModel
                groupByModel: todoGroupByModel
                moreMenu: todoLookupControlMoreMenu

                delegate: PublicTalkTodoItemDelegate {
                    id: itemDelegate

                    onDeleteTodoItem: {
                        var row = itemDelegate.ListView.view.model.source.getTodoIndex(id).row;
                        itemDelegate.ListView.view.model.source.removeRow(row);
                    }

                    onMoveTodoItem: {
                        var inout = isIncoming;
                        var row = itemDelegate.ListView.view.model.source.getTodoIndex(id).row;
                        var retVal = itemDelegate.ListView.view.model.source.moveToSchedule(row);
                        if (retVal === "") {
                            movedToSchedule(date, inout);
                        } else {
                            warningDialog.text = qsTr("Cannot schedule this item until these fields are fixed: %1").arg(retVal);
                            warningDialog.icon = StandardIcon.Warning;
                            warningDialog.open();
                        }
                    }

                    onEditTodoItem: {
                        todoEdit.editId = id;
                        todoStack.push(todoItem);
                    }
                }

                section.property: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                section.criteria: ViewSection.FullString
                section.delegate: personDetailSectionDelegate

                footer: Pane {
                    width: ListView.view.width;
                    z: 3

                    RowLayout {
                        anchors.right: parent.right
                        Label {
                            verticalAlignment: Text.AlignVCenter
                            //: Number of rows in a list
                            //~ Context the '%1'-part will be replaced by the actual number
                            text: qsTr("Count=%1", "Lookup control").arg(todoLookupControl.count)
                        }

                        ToolButton {
                            id: addButton
                            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                            visible: todoLookupControl.showEditButton
                            icon.source: "qrc:///icons/add.svg"
                            onClicked: {
                                var newId = todoModel.addRow(isIncomingSpeaker);
                                var newIndex = todoModel.getTodoIndex(newId);
                                var newProxyIndex = todoProxyModel.mapFromSource(newIndex);
                                todoLookupControl.currentIndex = newProxyIndex.row;
                            }
                        }

                        ToolButton {
                            id: moreMenuButton
                            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                            visible: todoLookupControl.moreMenu
                            ToolTip.visible: hovered
                            ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                            ToolTip.text: qsTr("Settings", "Lookup control")
                            icon.source: "qrc:///icons/more.svg"

                            onClicked: todoLookupControl.moreMenu.popup()
                        }
                    }
                }

                onSearchTextChanged: todoProxyModel.filterText = text
                onGroupByIndexChanged: {
                    if (groupByIndex >= 0) {
                        settings.todo_groupByIndex = groupByIndex;
                        todoProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                    }
                }
                onSortOrderChanged: {
                    settings.todo_sortOrder = sortOrder;
                    todoProxyModel.sort(0, sortOrder);
                }
                ScrollBar.vertical: ScrollBar {}
            }
        }

        Component {
            id: todoItem

            ColumnLayout {
                Layout.fillWidth: true

                function reloadCongregationDetailList() {
                    var modelIndex = todoModel.getTodoIndex(editId);
                    var date = todoModel.get(modelIndex.row).date
                    if (isNaN(date.getTime()))
                        date = new Date();
                    congregationDetailModel.loadCongregationDetails(isIncomingSpeaker, date);
                    congregationDetailProxyModel.sort(0, congregationLookupControl.sortOrder);
                }
                function reloadAssigneeDetailList() {
                    var modelIndex = todoModel.getTodoIndex(editId);
                    var date = todoModel.get(modelIndex.row).date
                    if (isNaN(date.getTime()))
                        date = new Date();
                    var speakerId = todoModel.get(modelIndex.row).speakerId
                    var congregationId = todoModel.get(modelIndex.row).congregationId
                    if (isIncomingSpeaker)
                        assigneeDetailModel.loadPublicTalkPersonDetails(speakerId, congregationId, 0, date);
                    else
                        assigneeDetailModel.loadLocalPublicTalkPersonDetails(speakerId, 0, date);
                    assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
                    assigneeLookupControl.currentIndex = assigneeLookupControl.indexOfValue(todoModel.get(modelIndex.row).speakerFullName); // restore currentIndex to selected value
                }
                function reloadPublicTalkDetailList() {
                    var modelIndex = todoModel.getTodoIndex(editId);
                    var date = todoModel.get(modelIndex.row).date;
                    if (isNaN(date.getTime()))
                        date = new Date();
                    var themeId = todoModel.get(modelIndex.row).themeId;
                    var speakerId = todoModel.get(modelIndex.row).speakerId;
                    var talkId = publicTalkLookupControl.currentValue;
                    var themeAndNumber = todoModel.get(modelIndex.row).themeAndNumber;
                    publicTalkDetailModel.loadPublicTalkDetails(themeId, speakerId, date);
                    publicTalkDetailProxyModel.sort(0, publicTalkLookupControl.sortOrder);
                    publicTalkLookupControl.currentIndex = publicTalkLookupControl.indexOfValue(themeAndNumber); // restore currentIndex to selected value
                }
                function resetDefaultCongregationLookupControlSettings() {
                    settings.congregation_groupByIndex = 1;
                    congregationLookupControl.groupByIndex = settings.congregation_groupByIndex;
                    settings.congregation_sortOrder = Qt.AscendingOrder;
                    congregationLookupControl.sortOrder = settings.congregation_sortOrder;
                    settings.congregation_detailRowCount = 3;
                    congregationLookupControlMoreMenu.detailRowCount = settings.congregation_detailRowCount;
                    settings.sync();
                    reloadCongregationDetailList();
                }
                function resetDefaultAssigneeLookupControlSettings() {
                    settings.nonstudentAssignment_groupByIndex = 1;
                    assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
                    settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
                    assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
                    settings.nonstudentAssignment_detailRowCount = 3;
                    settings.nonstudentAssignment_hideUnavailables = true;
                    assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
                    assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
                    settings.sync();
                    reloadAssigneeDetailList();
                }
                function resetDefaultPublicTalkLookupControlSettings() {
                    settings.publicTalk_groupByIndex = 1;
                    publicTalkLookupControl.groupByIndex = settings.publicTalk_groupByIndex;
                    settings.publicTalk_sortOrder = Qt.AscendingOrder;
                    publicTalkLookupControl.sortOrder = settings.publicTalk_sortOrder;
                    settings.publicTalk_detailRowCount = 3;
                    publicTalkLookupControlMoreMenu.detailRowCount = settings.publicTalk_detailRowCount;
                    settings.sync();
                    reloadPublicTalkDetailList();
                }

                CongregationDetailModel { id: congregationDetailModel }
                CongregationDetailSFProxyModel {
                    id: congregationDetailProxyModel
                    source: congregationDetailModel
                    onGroupByChanged: congregationDetailProxyModel.sort(0, congregationLookupControl.sortOrder)
                }
                PersonDetailModel { id: assigneeDetailModel }
                PersonDetailSFProxyModel {
                    id: assigneeDetailProxyModel
                    source: assigneeDetailModel
                    onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
                    areUnavailablesHidden: assigneeLookupControlMoreMenu.hideUnavailables
                }
                PublicTalkDetailModel { id: publicTalkDetailModel }
                PublicTalkDetailSFProxyModel {
                    id: publicTalkDetailProxyModel
                    source: publicTalkDetailModel
                    onGroupByChanged: publicTalkDetailProxyModel.sort(0, publicTalkLookupControl.sortOrder)
                }

                TodoValidator {
                    id: dateValidator
                    model: todoModel
                    todoId: editId
                    role: TodoModel.DateRole
                    onErrorChanged: dateErrorLabel.text = error
                }
                TodoValidator {
                    id: congregationValidator
                    model: todoModel
                    todoId: editId
                    role: TodoModel.CongregationNameRole
                    onErrorChanged: congregationErrorLabel.text = error
                }
                TodoValidator {
                    id: assigneeValidator
                    model: todoModel
                    todoId: editId
                    role: TodoModel.SpeakerFullNameRole
                    onErrorChanged: assigneeErrorLabel.text = error
                }
                TodoValidator {
                    id: publicTalkValidator
                    model: todoModel
                    todoId: editId
                    role: TodoModel.ThemeAndNumberRole
                    onErrorChanged: publicTalkErrorLabel.text = error
                }
                PublicTalkDetailGroupByModel { id: congregationDetailGroupByModel }
                PublicTalkDetailGroupByModel { id: assigneeDetailGroupByModel }
                PublicTalkDetailGroupByModel { id: publicTalkDetailGroupByModel }

                LookupControlMoreMenu {
                    id: congregationLookupControlMoreMenu
                    meetingType: MeetingType.WeekendMeeting
                    isIncludingPartsOfOtherMeetingAllowed: false
                    isLabelChangingAllowed: false
                    isHideUnavailablesAllowed: false
                    onDetailRowCountChanged: settings.congregation_detailRowCount = detailRowCount
                    onResetDefaultSettings: resetDefaultCongregationLookupControlSettings()
                }
                LookupControlMoreMenu {
                    id: assigneeLookupControlMoreMenu
                    meetingType: MeetingType.WeekendMeeting
                    isIncludingPartsOfOtherMeetingAllowed: false
                    isLabelChangingAllowed: false
                    onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
                    onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
                    onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
                }
                LookupControlMoreMenu {
                    id: publicTalkLookupControlMoreMenu
                    meetingType: MeetingType.WeekendMeeting
                    isIncludingPartsOfOtherMeetingAllowed: false
                    isLabelChangingAllowed: false
                    isHideUnavailablesAllowed: false
                    onDetailRowCountChanged: settings.publicTalk_detailRowCount = detailRowCount
                    onResetDefaultSettings: resetDefaultPublicTalkLookupControlSettings()
                }

                StackView.onActivating: {
                    congregationDetailGroupByModel.remove(5); // remove congregation
                    congregationDetailGroupByModel.remove(4); // remove public talk number
                    congregationDetailGroupByModel.remove(3); // remove public talk theme
                    congregationLookupControl.groupByIndex = settings.congregation_groupByIndex;
                    congregationLookupControl.sortOrder = settings.congregation_sortOrder;
                    congregationLookupControlMoreMenu.detailRowCount = settings.congregation_detailRowCount;
                    assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
                    assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
                    assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
                    assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
                    assigneeDetailGroupByModel.remove(4); // remove public talk number
                    assigneeDetailGroupByModel.remove(3); // remove public talk theme
                    publicTalkDetailGroupByModel.remove(5); // remove congregation
                    publicTalkDetailGroupByModel.setProperty(3, "value", "alphabet");
                    publicTalkDetailGroupByModel.remove(0); // remove name
                    publicTalkLookupControl.groupByIndex = settings.publicTalk_groupByIndex;
                    publicTalkLookupControl.sortOrder = settings.publicTalk_sortOrder;
                    publicTalkLookupControlMoreMenu.detailRowCount = settings.publicTalk_detailRowCount;

                    reloadCongregationDetailList();
                    reloadAssigneeDetailList();
                    reloadPublicTalkDetailList();

                    // Set the initial date and currentIndex to the value stored in the backend.
                    var modelIndex = todoModel.getTodoIndex(editId);
                    dateEdit.selectedDate = todoModel.get(modelIndex.row).date;
                    congregationLookupControl.currentIndex = congregationLookupControl.indexOfValue(todoModel.get(modelIndex.row).congregationName);
                    assigneeLookupControl.currentIndex = assigneeLookupControl.indexOfValue(todoModel.get(modelIndex.row).speakerFullName);
                    publicTalkLookupControl.currentIndex = publicTalkLookupControl.indexOfValue(todoModel.get(modelIndex.row).themeAndNumber);
                    textAreaNotes.text = todoModel.get(modelIndex.row).notes;
                }

                StackView.onDeactivating: {
                    settings.congregation_groupByIndex = congregationLookupControl.groupByIndex;
                    settings.congregation_sortOrder = congregationLookupControl.sortOrder;
                    settings.congregation_detailRowCount = congregationLookupControlMoreMenu.detailRowCount;
                    settings.nonstudentAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
                    settings.nonstudentAssignment_sortOrder = assigneeLookupControl.sortOrder;
                    settings.nonstudentAssignment_detailRowCount = assigneeLookupControlMoreMenu.detailRowCount;
                    settings.nonstudentAssignment_hideUnavailables = assigneeLookupControlMoreMenu.hideUnavailables;
                    settings.publicTalk_groupByIndex = publicTalkLookupControl.groupByIndex;
                    settings.publicTalk_sortOrder = publicTalkLookupControl.sortOrder;
                    settings.publicTalk_detailRowCount = publicTalkLookupControlMoreMenu.detailRowCount;
                    settings.sync();
                }

                GridLayout {
                    columns: 4
                    rows: 10

                    // Title
                    ToolButton {
                        Layout.alignment: Qt.AlignRight
                        Layout.column: 1
                        Layout.row: 0
                        icon.source: "qrc:/icons/chevron_left.svg"
                        onClicked: {
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.saveRow(modelIndex.row);
                            todoStack.pop();
                        }
                    }
                    Label {
                        Layout.fillWidth: true
                        Layout.column: 2
                        Layout.row: 0
                        text: qsTr("Edit To Do Item")
                        font: TBStyle.titleSmallFont
                    }
                    ToolButton {
                        Layout.alignment: Qt.AlignRight
                        Layout.column: 3
                        Layout.row: 0
                        icon.source: "qrc:/icons/close.svg"
                        onClicked: {
                            reloadTodoList();
                            todoStack.pop();
                        }
                    }

                    // Meeting day
                    ToolButton {
                        icon.source: "qrc:/icons/meeting_day.svg"
                        background: null
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.column: 0
                        Layout.row: 1
                        ToolTip.text: qsTr("Meeting day")
                        ToolTip.visible: hovered
                    }
                    RowLayout {
                        Layout.column: 1
                        Layout.row: 1
                        Layout.columnSpan: 3
                        Layout.fillWidth: true
                        DateEdit {
                            id: dateEdit
                            implicitWidth: 200
                            readOnly: true
                            text: selectedDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat);
                            validator: isLoading ? null : dateValidator

                            onSelectedDateChanged: {
                                var modelIndex = todoModel.getTodoIndex(editId);
                                todoModel.setData(modelIndex, selectedDate, TodoModel.DateRole);
                                reloadAssigneeDetailList();
                                isLoading = true;
                                isLoading = false;
                            }
                        }
                    }
                    Label {
                        id: dateErrorLabel
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: 2
                        Layout.columnSpan: 3
                        verticalAlignment: Text.AlignTop
                        font: TBStyle.bodySmallFont
                        color: TBStyle.alertColor
                        visible: text
                        wrapMode: Text.Wrap
                    }

                    // Congregation
                    ToolButton {
                        icon.source: "qrc:/icons/home.svg"
                        background: null
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.column: 0
                        Layout.row: isIncomingSpeaker ? 3 : 7
                        ToolTip.text: qsTr("Congregation")
                        ToolTip.visible: hovered
                    }
                    LookupComboBox {
                        id: congregationLookupControl

                        height: 40
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: isIncomingSpeaker ? 3 : 7
                        Layout.columnSpan: 3

                        showFilterControls: false
                        showGroupControls: true
                        showEditButton: false
                        isEditing: false

                        model: congregationDetailProxyModel
                        groupByModel: congregationDetailGroupByModel
                        moreMenu: congregationLookupControlMoreMenu

                        groupByIndex: -1

                        textRole: "congregationName"
                        valueRole: "congregationName"

                        validator: isLoading ? null : congregationValidator

                        delegate: CongregationDetailDelegate {
                            id: todoCongregationDetailItem

                            detailRowCount: congregationLookupControlMoreMenu.detailRowCount
                            background: Rectangle {
                                implicitWidth: 200
                                implicitHeight: 40
                                x: 1
                                y: 1
                                width: todoCongregationDetailItem.width - 2
                                height: todoCongregationDetailItem.height - 2
                                color: congregationLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: todoCongregationDetailItem.down || todoCongregationDetailItem.highlighted ? myPalette.highlight : "transparent"
                                    opacity: todoCongregationDetailItem.down ? 1.0 : 0.25
                                }
                            }
                        }

                        sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                        sectionCriteria: ViewSection.FullString
                        sectionDelegate: personDetailSectionDelegate

                        // When an item is selected, update the backend.
                        onActivated: {
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, currentValue, TodoModel.CongregationNameRole);
                            if (isIncomingSpeaker)
                                reloadAssigneeDetailList();
                            // trigger revalidation by switching isLoaded value
                            isLoading = true;
                            isLoading = false;
                        }
                        popup.onAboutToHide: {
                            var id = currentValue;
                            congregationDetailProxyModel.filterText = ""; // reset filter when closing the popup
                            currentIndex = indexOfValue(id); // restore currentIndex to selected value
                        }
                        onSearchTextChanged: congregationDetailProxyModel.filterText = text
                        onGroupByIndexChanged: {
                            if (groupByIndex >= 0) {
                                settings.congregation_groupByIndex = groupByIndex;
                                congregationDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                            }
                        }
                        onSortOrderChanged: {
                            settings.congregation_sortOrder = sortOrder;
                            congregationDetailProxyModel.sort(0, sortOrder);
                        }
                        onCleared: {
                            isLoading = true;
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, "", TodoModel.CongregationNameRole);
                            isLoading = false;
                        }
                    }
                    Label {
                        id: congregationErrorLabel
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: isIncomingSpeaker ? 4 : 8
                        Layout.columnSpan: 3
                        verticalAlignment: Text.AlignTop
                        font: TBStyle.bodySmallFont
                        color: TBStyle.alertColor
                        visible: text
                        wrapMode: Text.Wrap
                    }

                    // Speaker
                    ToolButton {
                        icon.source: "qrc:/icons/servant.svg"
                        background: null
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.column: 0
                        Layout.row: isIncomingSpeaker ? 5 : 3
                        ToolTip.text: qsTr("Speaker")
                        ToolTip.visible: hovered
                    }
                    LookupComboBox {
                        id: assigneeLookupControl

                        height: 40
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: isIncomingSpeaker ? 5 : 3
                        Layout.columnSpan: 3

                        showFilterControls: false
                        showGroupControls: true
                        groupByIndex: -1
                        showEditButton: false
                        isEditing: false

                        model: assigneeDetailProxyModel
                        groupByModel: assigneeDetailGroupByModel
                        moreMenu: assigneeLookupControlMoreMenu

                        textRole: "personFullName"
                        valueRole: "personFullName"
                        // TODO: find a proper way to update the displayed text after model changes (e.g. clearing) without usage of 'isLoading'
                        displayText: currentIndex === -1
                                     ? (!isLoading && todoModel.getTodoIndex(editId)
                                        ? todoModel.get(todoModel.getTodoIndex(editId).row).speakerFullName
                                        : "")
                                     : currentText

                        validator: isLoading ? null : assigneeValidator

                        delegate: PersonDetailDelegate {
                            id: todoAssigneeDetailItem

                            detailRowCount: assigneeLookupControlMoreMenu.detailRowCount
                            labelingMode: 2
                            includePartsOfOtherMeeting: assigneeLookupControlMoreMenu.includePartsOfOtherMeeting
                            displayAssigneeLabel: false
                            displayTimeLine: false
                            isOutgoingSpeaker: true
                            background: Rectangle {
                                implicitWidth: 200
                                implicitHeight: 40
                                x: 1
                                y: 1
                                width: todoAssigneeDetailItem.width - 2
                                height: todoAssigneeDetailItem.height - 2
                                color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: todoAssigneeDetailItem.down || todoAssigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                    opacity: todoAssigneeDetailItem.down ? 1.0 : 0.25
                                }
                            }
                        }

                        sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                        sectionCriteria: ViewSection.FullString
                        sectionDelegate: personDetailSectionDelegate

                        // When an item is selected, update the backend.
                        onActivated: {
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, currentValue, TodoModel.SpeakerFullNameRole);
                            reloadPublicTalkDetailList();
                            // trigger revalidation by switching isLoaded value
                            isLoading = true;
                            isLoading = false;
                        }
                        popup.onAboutToHide: {
                            var modelIndex = todoModel.getTodoIndex(editId);
                            var id = todoModel.get(modelIndex.row).speakerFullName;
                            assigneeDetailProxyModel.filterText = ""; // reset filter when closing the popup
                            currentIndex = indexOfValue(id); // restore currentIndex to selected value
                        }
                        onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                        onGroupByIndexChanged: {
                            if (groupByIndex >= 0) {
                                settings.nonstudentAssignment_groupByIndex = groupByIndex;
                                assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                            }
                        }
                        onSortOrderChanged: {
                            settings.nonstudentAssignment_sortOrder = sortOrder;
                            assigneeDetailProxyModel.sort(0, sortOrder);
                        }
                        onCleared: {
                            isLoading = true;
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, "", TodoModel.SpeakerFullNameRole);
                            isLoading = false;
                        }
                    }
                    Label {
                        id: assigneeErrorLabel
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: isIncomingSpeaker ? 6 : 4
                        Layout.columnSpan: 3
                        verticalAlignment: Text.AlignTop
                        font: TBStyle.bodySmallFont
                        color: TBStyle.alertColor
                        visible: text
                        wrapMode: Text.Wrap
                    }

                    // Theme
                    ToolButton {
                        icon.source: "qrc:/icons/title.svg"
                        background: null
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.column: 0
                        Layout.row: isIncomingSpeaker ? 7 : 5
                        ToolTip.text: qsTr("Theme")
                        ToolTip.visible: hovered
                    }
                    LookupComboBox {
                        id: publicTalkLookupControl

                        height: 40
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: isIncomingSpeaker ? 7 : 5
                        Layout.columnSpan: 3

                        showFilterControls: false
                        showGroupControls: true
                        groupByIndex: -1
                        showEditButton: false
                        isEditing: false

                        model: publicTalkDetailProxyModel
                        groupByModel: publicTalkDetailGroupByModel
                        moreMenu: publicTalkLookupControlMoreMenu

                        textRole: "themeAndNumber"
                        valueRole: "themeAndNumber"
                        // TODO: find a proper way to update the displayed text after model changes (e.g. clearing) without usage of 'isLoading'
                        displayText: currentIndex === -1
                                     ? (!isLoading && todoModel.getTodoIndex(editId)
                                        ? todoModel.get(todoModel.getTodoIndex(editId).row).themeAndNumber
                                        : "")
                                     : currentText

                        validator: isLoading ? null : publicTalkValidator

                        delegate: PublicTalkDetailDelegate {
                            id: todoPublicTalkDetailItem

                            detailRowCount: publicTalkLookupControlMoreMenu.detailRowCount
                            background: Rectangle {
                                implicitWidth: 200
                                implicitHeight: 40
                                x: 1
                                y: 1
                                width: todoPublicTalkDetailItem.width - 2
                                height: todoPublicTalkDetailItem.height - 2
                                color: publicTalkLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: todoPublicTalkDetailItem.down || todoPublicTalkDetailItem.highlighted ? myPalette.highlight : "transparent"
                                    opacity: todoPublicTalkDetailItem.down ? 1.0 : 0.25
                                }
                            }
                        }

                        sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                        sectionCriteria: ViewSection.FullString
                        sectionDelegate: personDetailSectionDelegate

                        // When an item is selected, update the backend.
                        onActivated: {
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, currentValue, TodoModel.ThemeAndNumberRole);
                            // trigger revalidation by switching isLoaded value
                            isLoading = true;
                            isLoading = false;
                        }
                        popup.onAboutToHide: {
                            var id = currentValue;
                            publicTalkDetailProxyModel.filterText = ""; // reset filter when closing the popup
                            currentIndex = indexOfValue(id); // restore currentIndex to selected value
                        }
                        onSearchTextChanged: publicTalkDetailProxyModel.filterText = text
                        onGroupByIndexChanged: {
                            if (groupByIndex >= 0) {
                                settings.publicTalk_groupByIndex = groupByIndex;
                                publicTalkDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                            }
                        }
                        onSortOrderChanged: {
                            settings.publicTalk_sortOrder = sortOrder;
                            publicTalkDetailProxyModel.sort(0, sortOrder);
                        }
                        onCleared: {
                            isLoading = true;
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, "", TodoModel.ThemeAndNumberRole);
                            isLoading = false;
                        }
                    }
                    Label {
                        id: publicTalkErrorLabel
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: isIncomingSpeaker ? 8 : 6
                        Layout.columnSpan: 3
                        verticalAlignment: Text.AlignTop
                        font: TBStyle.bodySmallFont
                        color: TBStyle.alertColor
                        visible: text
                        wrapMode: Text.Wrap
                    }

                    // Note
                    ToolButton {
                        icon.source: "qrc:/icons/notes-text.svg"
                        background: null
                        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                        Layout.column: 0
                        Layout.row: 9
                        ToolTip.text: qsTr("Speaker")
                        ToolTip.visible: hovered
                    }
                    TextArea {
                        id: textAreaNotes
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.column: 1
                        Layout.row: 9
                        Layout.columnSpan: 3
                        selectByMouse: true
                        onEditingFinished: {
                            var modelIndex = todoModel.getTodoIndex(editId);
                            todoModel.setData(modelIndex, text, TodoModel.NotesRole)
                        }
                    }
                }
            }
        }
    }
}
