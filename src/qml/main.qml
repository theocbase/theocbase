/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Dialogs 1.3
import QtQuick.Window 2.15
import net.theocbase 1.0
import QtGraphicalEffects 1.15
import QtWebView 1.1
import Qt.labs.settings 1.1
import "controls"

Item {
    id: root

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    property bool canViewMidweekMeetingSchedule: false
    property bool canEditMidweekMeetingSchedule: false
    property bool canViewWeekendMeetingSchedule: false
    property bool canEditWeekendMeetingSchedule: false
    property bool canSendMidweekMeetingReminders: false
    property bool canViewMeetingNotes: false
    property bool canEditMeetingNotes: false
    property date currentdate
    property date mwDate
    property date wkDate
    property alias bibleReading: midweekMeeting.bibleReading
    property bool showSongTitles: false
    property SpecialEventRule specialEventRule

    signal importClicked()
    signal fileDropped(url fileurl)
    signal gotoNextWeek()
    signal gotoPreviousWeek()
    signal showCalendar()
    signal showTimeline()
    signal sidebarWidthChanged(int width)

    function setDate(newdate){
        loadUISettings()
        root.forceActiveFocus()
        console.log("setDate function " + newdate.toString())
        currentdate = newdate
        // load Life and Ministry Meeting data
        midweekMeeting.loadSchedule(currentdate)
        weekendMeeting.loadSchedule(currentdate)
        outgoingSpeakers.reload()

        // show exception text
        congCtrl.clearExceptionCache()
        exceptionText.text = congCtrl.getExceptionText(currentdate, true)
        specialEventRule = SpecialEvents.findSpecialEventRule(congCtrl.getSpecialEventId(currentdate))

        // get meeting days
        var mDay = congCtrl.getMeetingDay(MeetingType.MidweekMeeting, currentdate)
        var mDDay = congCtrl.getMeetingDay(MeetingType.MidweekMeeting)
        var wDay = congCtrl.getMeetingDay(MeetingType.WeekendMeeting, currentdate)
        var wDDay = congCtrl.getMeetingDay(MeetingType.WeekendMeeting)

        midweekMeeting.editpossible = mDay > 0
        weekendMeeting.editpossible = wDay > 0

        var mDate = new Date(currentdate)
        mDate.setDate(mDate.getDate() + (mDay === 0 ? mDDay : mDay) -1)
        mwDate = mDate
        var wDate = new Date(currentdate)
        wDate.setDate(wDate.getDate() + (wDay === 0 ? wDDay : wDay) -1)
        wkDate = wDate

        flowView.calculateHeight()

        return true
    }
    function openStartPage(ignoreSettings) {
        startpageLoader.active = true
        if (ignoreSettings || settings_ui.show_start_page)
            startPagePopup.open()
    }
    function loadUISettings() {
        var settingsObj = Qt.createQmlObject('import Qt.labs.settings 1.0; Settings {category: "ui"; property bool show_song_titles: false}',
                                           root,
                                           "Settings");
        showSongTitles = settingsObj.show_song_titles
    }

    Keys.forwardTo: weekendMeeting

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    Settings {
        id: settings_ui
        category: "ui"
        property bool show_start_page: true
        property string latestArticle: ""
        property var splitView
    }

    CongregationCtrl { id: congCtrl }

    Component {
        id: monochromeEffect
        Colorize {
            objectName: "Colorize"
            hue: 0.5
            saturation: 0
            lightness: 0.5
        }
    }

    Component.onCompleted: splitView.restoreState(settings_ui.splitView)

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (contextMenu.opened)
                contextMenu.close()
            if (mouse.button === Qt.RightButton)
                contextMenu.popup()
        }
        Menu {
            id: contextMenu
            MenuItem {
                text: "Midweek Meeting"
                checked: midweekMeeting.visible
                onTriggered: {
                    lmMeeting.visible = !lmMeeting.visible
                    flowView.calculateHeight()
                }
            }
            MenuItem {
                text: "Weekend Meeting"
                checked: weekendMeeting.visible
                onTriggered: {
                    publicMeeting.visible = !publicMeeting.visible
                    flowView.calculateHeight()
                }
            }
            MenuItem {
                text: "Outgoing Speakers"
                checked: outgoingSpeakers.visible
                onTriggered: {
                    outgoingSpeakers.visible = !outgoingSpeakers.visible
                    flowView.calculateHeight()
                }
            }
        }
    }

    SplitView {
        id: splitView

        anchors.fill: parent
        orientation: Qt.Horizontal

        onResizingChanged: {
            if (!resizing) {
                settings_ui.splitView = splitView.saveState();
            }
        }

        ColumnLayout {
            SplitView.minimumWidth: root.width * 0.5
            SplitView.fillWidth: true

            spacing: 0

            Rectangle {
                id: exceptionBar
                color: TBStyle.alertColor
                height: visible ? 30 : 0
                visible: exceptionText.text != ""
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                Text {
                    id: exceptionText
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: ""
                    color: myPalette.window
                    font: TBStyle.titleMediumFont
                    visible: text != ""
                }
            }

            Pane {
                Layout.fillWidth: true
                Layout.fillHeight: true

                ScrollView {
                    id: scrollView
                    anchors.fill: parent
                    clip: true
                    contentHeight: flowView.height + 20

                    Flow {
                        id: flowView
                        flow: Flow.TopToBottom
                        height: 500
                        x: 10
                        y: 10
                        width: scrollView.width - 20
                        spacing: 10
                        Layout.bottomMargin: 10
                        property int columns: scrollView.width < 1000 ? 1 : 2
                        property int columnWidth: columns == 1 ? width : (width-spacing)/2

                        onWidthChanged: calculateHeight()
                        onPositioningComplete: {
                            if (childrenRect.width > width)
                                calculateHeight()
                        }

                        function calculateHeight()
                        {
                            var fullHeight = 0
                            var i
                            var firstHeight = 0
                            var arr = []
                            for (i = 0; i < children.length; i++) {
                                if (children[i].visible && children[i].objectName !== "Colorize") {
                                    fullHeight += children[i].height + spacing
                                    if (firstHeight == 0)
                                        firstHeight = children[i].height
                                    arr.push(children[i].height + spacing)
                                }
                            }
                            if (columns == 1 || fullHeight == 0) {
                                height = fullHeight
                            } else {
                                var oneCol = fullHeight / 2
                                var newHeight = 0
                                for (i = 0; i < arr.length; i++) {
                                    if (i < arr.length -1) {
                                        var nextHeight = newHeight + arr[i]
                                        if (nextHeight > oneCol) {
                                            newHeight = Math.min(nextHeight, (fullHeight - newHeight)) - spacing
                                            break
                                        }
                                    }
                                    newHeight = arr[i]
                                }
                                height = newHeight
                            }
                        }

                        // Midweek Meeting
                        MWMeetingModule {
                            id: midweekMeeting
                            width: flowView.columnWidth
                            layer.effect: monochromeEffect
                            layer.enabled: !editpossible
                            visible: canViewMidweekMeetingSchedule && currentdate.getFullYear() > 2015
                            onReloadSidebar: {
                                sidebar.loadPage(name, args)
                                if (setVisible)
                                {
                                    // open the sidebar since reloading was triggered e.g. by the edit button
                                    sidebar.isVisible = true
                                }
                                weekendMeeting.lastEditPage = ""
                            }
                        }
                        // Weekend Meeting
                        WEMeetingModule {
                            id: weekendMeeting
                            width: flowView.columnWidth
                            layer.effect: monochromeEffect
                            layer.enabled: !editpossible
                            visible: canViewWeekendMeetingSchedule
                            onReloadSidebar: {
                                sidebar.loadPage(name, args)
                                if (setVisible)
                                {
                                    // open the sidebar since reloading was triggered e.g. by the edit button
                                    sidebar.isVisible = true
                                }
                                midweekMeeting.lastEditAssignment = 0
                            }
                        }

                        OutgoingSpeakersModule {
                            id: outgoingSpeakers
                            width: flowView.columnWidth
                            currentDate: root.currentdate
                            enabled: specialEventRule ? !specialEventRule.isWithoutPublicTalk : false
                            onReloadSidebar: {
                                sidebar.loadPage(name, args)
                                if (setVisible)
                                {
                                    // open the sidebar since reloading was triggered e.g. by the edit button
                                    sidebar.isVisible = true
                                }
                                midweekMeeting.lastEditAssignment = 0
                                weekendMeeting.lastEditPage = ""
                            }
                        }
                    }
                }

                ColumnLayout {
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom

                    // Sidebar open/close button
                    RoundButton {
                        Layout.alignment: Qt.AlignRight + Qt.AlignBottom
                        icon.source: sidebar.isVisible ? "qrc:/icons/sidebar_close.svg" : "qrc:/icons/sidebar_open.svg"
                        icon.color: TBStyle.primaryTextColor
                        visible: canEditMidweekMeetingSchedule || canEditWeekendMeetingSchedule
                        onClicked: {
                            root.forceActiveFocus()
                            sidebar.isVisible = !sidebar.isVisible;
                        }
                    }

                    // Timeline Button
                    RoundButton {
                        Layout.alignment: Qt.AlignRight + Qt.AlignBottom
                        icon.source: "qrc:/icons/timeline.svg"
                        icon.color: TBStyle.primaryTextColor
                        visible: canEditMidweekMeetingSchedule
                        onClicked: showTimeline()
                    }
                }
            }
        }

        Pane {
            implicitWidth: (root.width * 0.3) < 400 ? 400 : (root.width * 0.3)
            SplitView.minimumWidth: (root.width * 0.3) < 400 ? 400 : (root.width * 0.3)
            SplitView.maximumWidth: root.width * 0.5

            visible: sidebar.isVisible

            Sidebar {
                id: sidebar
                anchors.fill: parent
            }
        }
    }

    Popup {
        id: startPagePopup
        width: 480
        height: root.height > 640 ? 640 : root.height
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        clip: true
        padding: 0
        Loader {
            id: startpageLoader
            anchors.fill: parent
            active: false
            source: "qrc:/startup/StartSlider.qml"
        }
        Connections {
            target: startpageLoader.item
            function onClose() { startPagePopup.close() }
            function onNewsFound() { if (!startPagePopup.opened) startPagePopup.open() }
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
