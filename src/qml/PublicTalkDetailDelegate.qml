import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.15
import net.theocbase 1.0
import Qt.labs.qmlmodels 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

ItemDelegate {
    property int detailRowCount: -1

    width: LookupControl.view.width
    highlighted: LookupControl.view.currentIndex === index
    hoverEnabled: LookupControl.view.hoverEnabled

    contentItem: ColumnLayout {
        width: parent.width

        RowLayout {
            Layout.fillWidth: true

            Item {
                implicitWidth: 25
                height: childrenRect.height
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                ToolButton {
                    background: null
                    icon.source: "qrc:///icons/warning_inactive.svg"
                    icon.color: TBStyle.alertColor
                    visible: false
                }
            }

            Item {
                implicitHeight: 30
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                Label {
                    width: parent.width
                    anchors.centerIn: parent
                    text: numberAndTheme
                    elide: Text.ElideRight
                    font: TBStyle.bodyLargeFont
                }
            }

            Item {
                Layout.minimumWidth: 100
                implicitWidth: 100
                implicitHeight: 30
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.rightMargin: 0

                Label {
                    width: parent.width
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignRight
                    text: SPScripts.formatRating(assignmentFrequencyRange)
                    font: TBStyle.bodyLargeFont
                }
            }

            Item {
                width: 80
                implicitWidth: 80
                implicitHeight: 30
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.rightMargin: 0

                Label {
                    width: parent.width
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    text: SPScripts.formatTimeRange(timeRange)
                    font: TBStyle.bodyLargeFont
                }
            }
        }

        ColumnLayout {
            height: childrenRect.height
            Layout.fillWidth: true
            Layout.leftMargin: 5
            visible: detailRowCount >= 1 || detailRowCount == -1

            RowLayout {
                Layout.fillWidth: true
                height: childrenRect.height

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                    Label {
                        width: parent.width
                        verticalAlignment: Text.AlignVCenter
                        anchors.centerIn: parent
                        text: congregationName
                        elide: Text.ElideRight
                    }
                }

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                    Label {
                        width: parent.width
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        text: assigneeFullName
                        elide: Text.ElideMiddle
                    }
                }

                Item {
                    width: 80
                    implicitWidth: 80
                    height: 20
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.rightMargin: 0

                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors.centerIn: parent
                        text: date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                        elide: Text.ElideMiddle
                    }
                }
            }
        }

        ColumnLayout {
            height: childrenRect.height
            Layout.fillWidth: true
            Layout.leftMargin: 5
            visible: detailRowCount >= 2 || detailRowCount == -1

            RowLayout {
                Layout.fillWidth: true
                height: childrenRect.height

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Label {
                        width: parent.width
                        verticalAlignment: Text.AlignVCenter
                        anchors.centerIn: parent
                        text: previous1CongregationName
                        elide: Text.ElideRight
                    }
                }

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Label {
                        width: parent.width
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        text: previous1AssigneeFullName
                        elide: Text.ElideMiddle
                    }
                }

                Item {
                    width: 80
                    implicitWidth: 80
                    height: 20
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.rightMargin: 0
                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors.centerIn: parent
                        text: previous1Date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                        elide: Text.ElideMiddle
                    }
                }
            }
        }

        ColumnLayout {
            height: childrenRect.height
            Layout.fillWidth: true
            Layout.leftMargin: 5
            Layout.bottomMargin: 5
            visible: detailRowCount >= 3 || detailRowCount == -1

            RowLayout {
                Layout.fillWidth: true
                height: childrenRect.height

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Label {
                        width: parent.width
                        verticalAlignment: Text.AlignVCenter
                        anchors.centerIn: parent
                        textFormat: Text.MarkdownText
                        text: textMetrics.elidedText
                        font: TBStyle.bodySmallFont

                        TextMetrics {
                            id: textMetrics
                            font: TBStyle.bodySmallFont
                            elide: Text.ElideRight
                            elideWidth: 100
                            text: previous2CongregationName
                        }
                    }
                }

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Label {
                        width: parent.width
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        text: previous2AssigneeFullName
                        elide: Text.ElideMiddle
                        font: TBStyle.bodySmallFont
                    }
                }

                Item {
                    width: 80
                    implicitWidth: 80
                    height: 20
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.rightMargin: 0
                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors.centerIn: parent
                        text: previous2Date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                        textFormat: Text.MarkdownText
                        elide: Text.ElideMiddle
                        font: TBStyle.bodySmallFont
                    }
                }
            }
        }
    }

    background: Item {}
}
