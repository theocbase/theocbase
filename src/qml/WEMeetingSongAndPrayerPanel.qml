import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    title: isPrayerDisplayed
           ? qsTr("Song and Prayer")
           : qsTr("Song")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property SpecialEventRule specialEventRule
    property CPTMeeting meeting
    property bool isWtStartSong: true
    property bool isPrayerDisplayed: !(meetingSection === MeetingSection.WatchtowerStudy && isWtStartSong && !specialEventRule.isWithoutPublicTalk)
    property bool isOpeningPrayer: meetingSection === MeetingSection.PublicTalk || (specialEventRule.isWithoutPublicTalk && isWtStartSong)
    property int meetingSection: MeetingSection.PublicTalk
    property bool isLoading: true

    // javascript functions
    function reloadAssigneeDetailList(){
        if (!isPrayerDisplayed)
            return;
        var assigneeId = isOpeningPrayer
                ? (meeting && meeting.openingPrayer ? meeting.openingPrayer.id : 0)
                : (meeting && meeting.finalPrayer ? meeting.finalPrayer.id : 0);
        assigneeDetailModel.loadPersonDetails(0, 0,
                                              assigneeId, // include current assignment
                                              MeetingType.WeekendMeeting,
                                              1, MeetingPart.SongAndPrayer, false, 0,
                                              true, // show weekend parts
                                              lookupControlMoreMenu.includePartsOfOtherMeeting,
                                              personDetailFilterModel.get(0).checked, // nonstudent parts
                                              personDetailFilterModel.get(1).checked, // other assignments
                                              false, // student parts
                                              false, // assistant in student parts
                                              personDetailFilterModel.get(4).checked, // similar assignments only
                                              meeting.date,
                                              isOpeningPrayer ? AssignmentSubtype.OpeningPrayer : AssignmentSubtype.ConcludingPrayer);
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
    }

    function refreshAssignments(meetingDate) {
        if (!isPrayerDisplayed)
            return;
        reloadAssigneeDetailList();
        // update index to display the assigned persons
        var assigneeId = isOpeningPrayer
                ? (meeting && meeting.openingPrayer ? meeting.openingPrayer.id : 0)
                : (meeting && meeting.finalPrayer ? meeting.finalPrayer.id : 0);
        assigneeLookupControl.currentIndex = assigneeLookupControl.indexOfValue(assigneeId)
    }

    function resetDefaultLookupControlSettings() {
        personDetailFilterModel.setProperty(0, "checked", false);
        personDetailFilterModel.setProperty(1, "checked", true);
        personDetailFilterModel.setProperty(2, "visible", false);
        personDetailFilterModel.setProperty(3, "visible", false);
        personDetailFilterModel.setProperty(4, "checked", true);
        settings.otherAssignment_filter1 = personDetailFilterModel.get(0).checked;
        settings.otherAssignment_filter2 = personDetailFilterModel.get(1).checked;
        settings.otherAssignment_filter5 = personDetailFilterModel.get(4).checked;
        settings.otherAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.otherAssignment_groupByIndex;
        settings.otherAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.otherAssignment_sortOrder;
        settings.otherAssignment_includeMidweekParts = true;
        settings.otherAssignment_detailRowCount = 3;
        settings.otherAssignment_labelingMode = 2;
        settings.otherAssignment_hideUnavailables = true;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.otherAssignment_includeMidweekParts;
        lookupControlMoreMenu.detailRowCount = settings.otherAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.otherAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.otherAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }

    Settings {
        id: settings
        category: "LookupControl"
        property bool otherAssignment_filter1: false
        property bool otherAssignment_filter2: true
        property bool otherAssignment_filter5: true
        property int otherAssignment_groupByIndex: 1
        property bool otherAssignment_sortOrder: Qt.AscendingOrder
        property bool otherAssignment_includeMidweekParts: true
        property int otherAssignment_detailRowCount: 3
        property int otherAssignment_labelingMode: 2
        property bool otherAssignment_hideUnavailables: true
    }

    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: personDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    WEMeetingValidator {
        id: assigneeValidator
        field: isOpeningPrayer
               ? WEMeetingValidator.OpeningPrayer
               : WEMeetingValidator.FinalPrayer
        onErrorChanged: assigneeErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: lookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.otherAssignment_includeMidweekParts = includePartsOfOtherMeeting;
            reloadAssigneeDetailList();
        }
        onDetailRowCountChanged: settings.otherAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.otherAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.otherAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultLookupControlSettings()
    }

    Connections {
        target: meeting
        function onDateChanged(date) { refreshAssignments(date) }
    }

    Component.onCompleted: {
        personDetailFilterModel.setProperty(0, "checked", settings.otherAssignment_filter1);
        personDetailFilterModel.setProperty(1, "checked", settings.otherAssignment_filter2);
        personDetailFilterModel.setProperty(2, "visible", false);
        personDetailFilterModel.setProperty(3, "visible", false);
        personDetailFilterModel.setProperty(4, "checked", settings.otherAssignment_filter5);
        assigneeLookupControl.groupByIndex = settings.otherAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.otherAssignment_sortOrder;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.otherAssignment_includeMidweekParts;
        lookupControlMoreMenu.detailRowCount = settings.otherAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.otherAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.otherAssignment_hideUnavailables;
        assigneeLookupControl.filterModel = personDetailFilterModel;
        reloadAssigneeDetailList();
    }

    Component.onDestruction: {
        settings.otherAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.otherAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.otherAssignment_includeMidweekParts = lookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.otherAssignment_detailRowCount = lookupControlMoreMenu.detailRowCount;
        settings.otherAssignment_labelingMode = lookupControlMoreMenu.labelingMode;
        settings.otherAssignment_hideUnavailables = lookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onMeetingChanged: {
        isLoading = true
        if (!meeting) return
        controller.date = meeting.date

        // set validator after meeting is loaded to avoid binding removal
        assigneeValidator.meeting = meeting
        isLoading = false
    }

    PublicMeetingController {
        id: controller
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10

        ColumnLayout {
            anchors.fill: parent

            // Prayer
            GridLayout {
                columns: 2
                rows: 2
                visible: isPrayerDisplayed
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Prayer")
                    ToolTip.visible: hovered
                }
                ColumnLayout {
                    LookupComboBox {
                        id: assigneeLookupControl

                        height: 40
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: 0

                        showFilterControls: true
                        showGroupControls: true
                        groupByIndex: -1
                        showEditButton: false
                        isEditing: false

                        model: assigneeDetailProxyModel
                        groupByModel: personDetailGroupByModel
                        moreMenu: lookupControlMoreMenu

                        textRole: "personFullName"
                        valueRole: "personId"
                        displayText: currentIndex === -1
                                     ? (isOpeningPrayer
                                        ? (meeting && meeting.openingPrayer ? meeting.openingPrayer.fullname : "")
                                        : (meeting && meeting.finalPrayer ? meeting.finalPrayer.fullname : ""))
                                     : currentText

                        validator: isLoading ? null : assigneeValidator

                        delegate: PersonDetailDelegate {
                            id: assigneeDetailItem

                            meetingDate: meeting.date
                            detailRowCount: lookupControlMoreMenu.detailRowCount
                            labelingMode: lookupControlMoreMenu.labelingMode
                            includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                            background: Rectangle {
                                implicitWidth: 200
                                implicitHeight: 40
                                x: 1
                                y: 1
                                width: assigneeDetailItem.width - 2
                                height: assigneeDetailItem.height - 2
                                color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                    opacity: assigneeDetailItem.down ? 1.0 : 0.25
                                }
                            }
                        }

                        sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                        sectionCriteria: ViewSection.FullString
                        sectionDelegate: personDetailSectionDelegate

                        // When an item is selected, update the backend.
                        onActivated: {
                            isLoading = true
                            var prayer = CPersons.getPerson(currentValue)
                            if (isOpeningPrayer)
                                meeting.openingPrayer = prayer;
                            else
                                meeting.finalPrayer = prayer;
                            meeting.save();
                            // trigger re-validation
                            isLoading = false
                        }
                        // Set the initial currentIndex to the value stored in the backend.
                        Component.onCompleted: {
                            currentIndex = isOpeningPrayer
                                    ? (meeting && meeting.openingPrayer ? indexOfValue(meeting.openingPrayer.id) : -1)
                                    : (meeting && meeting.finalPrayer ? indexOfValue(meeting.finalPrayer.id) : -1)
                        }
                        popup.onAboutToHide: {
                            var id = isOpeningPrayer
                                    ? (meeting && meeting.openingPrayer ? meeting.openingPrayer.id : -1)
                                    : (meeting && meeting.finalPrayer ? meeting.finalPrayer.id : -1);
                            assigneeDetailProxyModel.filterText = ""; // reset filter when closing the popup
                            currentIndex = indexOfValue(id); // restore currentIndex to selected value
                        }
                        onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                        onFilterChanged: {
                            settings.setValue("otherAssignment_filter" + (index + 1), checked);
                            settings.sync();
                            reloadAssigneeDetailList();
                        }
                        onGroupByIndexChanged: {
                            if (groupByIndex >= 0) {
                                settings.otherAssignment_groupByIndex = groupByIndex;
                                assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                            }
                        }
                        onSortOrderChanged: {
                            settings.otherAssignment_sortOrder = sortOrder;
                            assigneeDetailProxyModel.sort(0, sortOrder)
                        }
                        onCleared: {
                            isLoading = true
                            if (isOpeningPrayer)
                                meeting.openingPrayer = null;
                            else
                                meeting.finalPrayer = null;
                            meeting.save();
                            isLoading = false
                        }
                    }
                    Label {
                        id: assigneeErrorLabel
                        Layout.fillWidth: true
                        Layout.column: 1
                        Layout.row: 1
                        verticalAlignment: Text.AlignTop
                        font: TBStyle.bodySmallFont
                        color: TBStyle.alertColor
                        visible: text
                        wrapMode: Text.Wrap
                    }
                }
            }

            // Song
            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    icon.source: "qrc:/icons/song.svg"
                    background: null
                    ToolTip.text: qsTr("Song")
                    ToolTip.visible: hovered
                }

                NumberSelector {
                    Layout.fillWidth: true
                    Layout.preferredHeight: height
                    maxValue: 151
                    selectedValue: meeting
                                   ? (meetingSection === MeetingSection.PublicTalk
                                      ? meeting.songTalk
                                      : (isWtStartSong ? meeting.songWtStart : meeting.songWtEnd))
                                   : 0
                    onSelectedValueChanged: {
                        if (!meeting)
                            return;
                        if (meetingSection === MeetingSection.PublicTalk)
                        {
                            if (selectedValue === meeting.songTalk)
                                return;
                            meeting.songTalk = selectedValue;
                        } else {
                            if (isWtStartSong) {
                                if (selectedValue === meeting.songWtStart)
                                    return;
                                meeting.songWtStart = selectedValue;
                            } else {
                                if (selectedValue === meeting.songWtEnd)
                                    return;
                                meeting.songWtEnd = selectedValue;
                            }
                        }
                        meeting.save();
                    }
                }
            }
        }
    }
}

