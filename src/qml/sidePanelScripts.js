.import net.theocbase 1.0 as TB

function formatRating(value)
{
    var ratingString = "";
    for (var i = 0; i < 5; ++i)
    {
        ratingString += i < value ? "\u2611" : "\u2610"; // ☑☐
    }
    return ratingString;
}

function formatTimeRange(value)
{
    return value <= 1
            ? "🌑"
            : value <= 2
              ? "🌒"
              : value <= 3
                ? "🌓"
                : value <= 4
                  ? "🌔"
                  : "🌕";
}

function formatNumberRange(value, step)
{
    var floor = Math.floor((value - 1) / step) * step + 1;
    var ceiling = Math.floor((value - 1) / step) * step + step;
    return floor + " - " + ceiling;
}

function formatTheme(value)
{
    return value === {} || value === ""
            ? qsTr("No recent assignment", "Meeting assigment")
            : value;
}

function formatWeeksIdle(value)
{
    return value <= 0
            ? qsTr("No weeks idle", "Meeting assigment")
            : value < 4
              ? qsTr("%n week(s)", "Meeting assigment", Math.abs(value))
              : qsTr("%1 or more weeks", "Meeting assigment").arg(4);
}

function getMeetingIndicator(meetingType)
{
    switch (meetingType) {
    case TB.MeetingType.WeekendMeeting: return "⚪⚪⚫";
    case TB.MeetingType.MidweekMeeting: return "⚫⚫⚪";
    default: return "";
    }
}

function getClassName(classNumber)
{
    switch (classNumber) {
    case 2: return qsTr("A1", "abbreviation for auxiliary classroom 1");
    case 3: return qsTr("A2", "abbreviation for auxiliary classroom 2");
    default: return qsTr("MH","abbreviation for main hall");
    }
}

function getRelativeWeekOf(referenceDate, targetDate)
{
    var t1Date = new Date(referenceDate);
    t1Date.setDate(t1Date.getDate() - (t1Date.getDay() + 6) % 7);
    var t1 = t1Date.getTime();
    var t2Date = new Date(targetDate);
    t2Date.setDate(t2Date.getDate() - (t2Date.getDay() + 6) % 7);
    var t2 = t2Date.getTime();
    return parseInt((t2 - t1) / (24 * 3600 * 1000 * 7));
}

function drawTimeRange(ctx, cx, cy, radius, color, timeRange)
{
    var shift = radius * 0.6;
    var angle = Math.asin(shift / 2.0 / radius);
    ctx.lineWidth = 0.75;
    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
    ctx.arc(cx, cy, radius, 0, 2 * Math.PI);
    ctx.stroke();

    if (timeRange <= 1) {
      // 🌑
      ctx.fill();
    } else if (timeRange <= 2) {
      // 🌒
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(cx, cy, radius, 0.5 * Math.PI + angle, 1.5 * Math.PI - angle);
      ctx.arc(cx - shift, cy, radius, 1.5 * Math.PI + angle, 0.5 * Math.PI - angle);
      ctx.fill();
    } else if (timeRange <= 3) {
      // 🌓
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(cx, cy, radius, 0.5 * Math.PI, 1.5 * Math.PI);
      ctx.closePath();
      ctx.fill();
    } else if ( timeRange <= 4) {
      // 🌔
      ctx.stroke();
      ctx.beginPath();
      ctx.arc(cx, cy, radius, 0.5 * Math.PI - angle, 1.5 * Math.PI + angle, false);
      ctx.arc(cx + shift, cy, radius, 1.5 * Math.PI - angle, 0.5 * Math.PI + angle, true);
      ctx.fill();
    } else {
      // 🌕
    }
    ctx.stroke();
}

function drawSection(ctx, cx, cy, radius, color, sectionNr, sections, isFlipped, isFilled, isThick)
{
    var rotation = isFlipped
            ? ((1.25 + 0.5 / sections) - sectionNr * 0.5 / sections) * Math.PI
            : (sectionNr - 1) * 0.5 / sections * Math.PI;
    var startAngle = 1.25 * Math.PI;
    var endAngle = startAngle + 0.5 / sections * Math.PI;
    ctx.save();
    ctx.translate(cx, cy);
    ctx.rotate(rotation);
    ctx.strokeStyle = color;
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.lineWidth = isThick ? 1.5 : 1;
    ctx.arc(0, 0, radius + (isThick ? 0 : 0), startAngle, endAngle);
    ctx.arc(0, 0, radius + 4 - (isThick ? 0 : 0), endAngle, startAngle, true);
    ctx.closePath();
    ctx.stroke();
    if (isFilled)
        ctx.fill();
    ctx.restore();
}

function drawAnyAssignmentMarker(ctx, cx, cy, radius, weekOf, assignmentInfo)
{
    if (assignmentInfo && Math.abs(weekOf) > 0 && Math.abs(weekOf) <= 4)
    {
        var sections = assignmentInfo.meetingType === TB.MeetingType.MidweekMeeting
                ? 3
                : assignmentInfo.meetingType === TB.MeetingType.WeekendMeeting
                  ? 2
                  : 1;
        var assignmentSection = 1;
        switch (assignmentInfo.meetingSection){
        case TB.MeetingSection.ApplyYourselfToTheFieldMinistry:
        case TB.MeetingSection.WatchtowerStudy:
            assignmentSection = 2;
            break;
        case TB.MeetingSection.LivingAsChristians:
            assignmentSection = 3;
            break;
        default:
            assignmentSection = 1;
            break;
        }
        // draw sections
        for (var i = 1; i <= sections; i++)
        {
            var color = i === assignmentSection
                    ? assignmentInfo.meetingSectionTextColor
                    : TB.TBStyle.mediumColor;
            var isFilled = assignmentSection == i && assignmentInfo.isSupplementary;
            var isThick = assignmentSection == i && !assignmentInfo.isSupplementary;
            ctx.lineWidth = assignmentSection == i ? 2 : 1;
            drawSection(ctx, cx, cy, radius + 2, color, i, sections, assignmentInfo.meetingType === TB.MeetingType.WeekendMeeting, isFilled, isThick);
        }
    }
}
