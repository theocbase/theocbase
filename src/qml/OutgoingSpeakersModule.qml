/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import net.theocbase 1.0
import "controls"

Rectangle {
    id: outgoingSpeakersEdit

    radius: 5
    color: myPalette.base
    implicitHeight: childrenRect.height

    property string title: "Outgoing Speakers This Weekend"
    property date currentDate
    property int nameColWidth: 10
    property int lastEditIndex: -1

    signal reloadSidebar(var name, var args, var setVisible)
    signal movedTodoList()

    function reload() {
        outModel.loadList(currentDate)
    }

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    OutgoingSpeakersModel {
        id: outModel
        Component.onCompleted: outModel.loadList(currentDate)
        onModelChanged: {
            var nameTest = ""
            var i
            for (i = 0; i < outModel.rowCount(); i++) {
                if (outModel.get(i).speaker.length > nameTest.length)
                    nameTest = outModel.get(i).speaker
            }
            // change edit page in the sidebar ?
            if (lastEditIndex > -1) {
                if (outModel.rowCount() -1 >= lastEditIndex) {
                    reloadSidebar("../OutgoingSpeakerEdit.qml", { "outModel": outModel, "modelIndex": lastEditIndex,
                                  "currentDate": pmController.date }, false)
                } else {
                    reloadSidebar("",{}, false)
                    lastEditIndex = -1
                }
            }
        }
        onMovedToWeek: {
            reload();
        }

        onMovedTodoList: outgoingSpeakersEdit.movedTodoList()
    }
    PublicMeetingController { id: pmController }
    onCurrentDateChanged: {
        pmController.date = currentDate
    }

    RowLayout {
        width: parent.width
        ColumnLayout {
            Layout.margins: 10
            spacing: 0
            Rectangle {
                color: TBStyle.outgoingSpeakersColor
                Layout.fillWidth: true
                implicitHeight: childrenRect.height
                ColumnLayout {
                    width: parent.width
                    spacing: 0
                    RowLayout {
                        Rectangle {
                            height: 40
                            width: 40
                            color: TBStyle.outgoingSpeakersTextColor
                            Image {
                                anchors.fill: parent
                                anchors.margins: 4

                                source: "qrc:/icons/speaker_outgoing.svg"
                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: myPalette.window
                                }
                            }
                        }
                        Label {
                            text: qsTr("Outgoing speakers")
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.leftMargin: 4
                            verticalAlignment: Text.AlignVCenter
                            font: TBStyle.titleLargeFont
                            color: TBStyle.outgoingSpeakersTextColor
                            elide: Text.ElideRight
                        }
                    }

                    ListView {
                        id: listView
                        model: outModel
                        implicitHeight: childrenRect.height
                        Layout.fillWidth: true
                        interactive: false
                        delegate: ScheduleRowItem {
                            width: listView.width
                            timeText: Qt.locale().dayName(date.getDay(), Locale.ShortFormat)
                            timeColor: myPalette.window
                            timeBackgroundColor: TBStyle.outgoingSpeakersTextColor
                            editable: canEditWeekendMeetingSchedule
                            themeText: theme === "" ? "" : theme + " (" + themeNo + ")"
                            nameText1.text: speaker
                            nameText1.validator: OutgoingSpeakerValidator {
                                model: outModel
                                row: index
                                role: OutgoingSpeakersModel.SpeakerRole
                            }
                            nameText2.text: congregation
                            nameText2.validator: OutgoingSpeakerValidator {
                                model: outModel
                                row: index
                                role: OutgoingSpeakersModel.CongregationRole
                            }

                            onClicked: {
                                if (!canEditWeekendMeetingSchedule)
                                    return
                                reloadSidebar("../OutgoingSpeakerEdit.qml", { "outModel": outModel, "modelIndex": index,
                                              "currentDate": pmController.date }, true)
                                lastEditIndex = index
                            }
                        }
                    }
                }
            }

            ScheduleRowItem {
                timeBackgroundColor: "transparent"
                timeText: ""
                themeText: outModel.length > 0
                           ? qsTr("%1 speakers away this weekend","", outModel.length).arg(outModel.length)
                           : qsTr("No speakers away this weekend")
                themeColor: TBStyle.mediumColor
                buttonIcon: "qrc:/icons/add_circle.svg"
                editable: canEditWeekendMeetingSchedule
                onClicked: {
                    outModel.addRow(-1, -1, -1);
                    reloadSidebar("../OutgoingSpeakerEdit.qml", { "outModel": outModel, "modelIndex": outModel.rowCount()-1,
                                  "currentDate": pmController.date }, true);
                    lastEditIndex = outModel.rowCount() - 1;
                }
            }
        }
    }
}
