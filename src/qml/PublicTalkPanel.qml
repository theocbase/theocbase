/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    id: weekendMeetingEdit

    title: qsTr("Public Talk")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property CPTMeeting meeting
    property PublicMeetingController controller
    property bool isLoading: true

    // javascript functions
    function reloadPublicTalkDetailList() {
        publicTalkDetailModel.loadPublicTalkDetails(meeting ? meeting.themeId : 0,
                                                    (meeting && meeting.speaker) ? meeting.speaker.id : 0,
                                                    meeting ? meeting.date : new Date());
        publicTalkDetailProxyModel.sort(0, publicTalkLookupControl.sortOrder);
    }
    function reloadCongregationDetailList() {
        congregationDetailModel.loadCongregationDetails(true,
                                                        meeting ? meeting.date : new Date());
        congregationDetailProxyModel.sort(0, congregationLookupControl.sortOrder);
    }
    function reloadAssigneeDetailList() {
        var congregationId = congregationLookupControl.currentValue > 0
                ? congregationLookupControl.currentValue
                : ((meeting && meeting.speaker)
                   ? meeting.speaker.congregationid
                   : 0);
        var talkId = publicTalkLookupControl.currentValue;
        var speakerId = assigneeLookupControl.currentValue;
        assigneeDetailModel.loadPublicTalkPersonDetails(speakerId, // include current assignment
                                                        congregationId,
                                                        talkId,
                                                        meeting ? meeting.date : new Date());
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
        assigneeLookupControl.currentIndex = assigneeLookupControl.indexOfValue(speakerId); // restore currentIndex to selected value
    }
    function reloadSpeakerData() {
        // reload list of talks, to display only those, delivered by the current speaker
        var themeId = publicTalkLookupControl.currentValue > 0
                ? publicTalkLookupControl.currentValue
                : (meeting
                   ? meeting.themeId
                   : 0);
        reloadPublicTalkDetailList();
        publicTalkLookupControl.currentIndex = publicTalkLookupControl.indexOfValue(themeId); // restore currentIndex to selected value
        // reload list of congregations
        var congregationId = congregationLookupControl.currentValue > 0
                ? congregationLookupControl.currentValue
                : ((meeting && meeting.speaker)
                   ? meeting.speaker.congregationid
                   : 0);
        reloadCongregationDetailList();
        congregationLookupControl.currentIndex = congregationLookupControl.indexOfValue(congregationId); // restore currentIndex to selected value
        // reload list of speakers, to display only those, who are part of the selected congregation and deliver the selected talk
        var speakerId = assigneeLookupControl.currentValue > 0
                ? assigneeLookupControl.currentValue
                : ((meeting && meeting.speaker)
                   ? meeting.speaker.id
                   : 0);
        reloadAssigneeDetailList();
        assigneeLookupControl.currentIndex = assigneeLookupControl.indexOfValue(speakerId); // restore currentIndex to selected value
    }

    function reloadHospitalityDetailList() {
        hospitalityDetailModel.loadHospitalityPersonDetails((meeting && meeting.hospitalityHost) ? meeting.hospitalityHost.id : 0, // include current assignment
                                                            meeting ? meeting.date : new Date());
        hospitalityDetailProxyModel.sort(0, hospitalityLookupControl.sortOrder);
    }

    function resetDefaultPublicTalkLookupControlSettings() {
        settings.publicTalk_groupByIndex = 1;
        publicTalkLookupControl.groupByIndex = settings.publicTalk_groupByIndex;
        settings.publicTalk_sortOrder = Qt.AscendingOrder;
        publicTalkLookupControl.sortOrder = settings.publicTalk_sortOrder;
        settings.publicTalk_detailRowCount = 3;
        publicTalkLookupControlMoreMenu.detailRowCount = settings.publicTalk_detailRowCount;
        settings.sync();
        reloadPublicTalkDetailList();
    }
    function resetDefaultCongregationLookupControlSettings() {
        settings.congregation_groupByIndex = 1;
        congregationLookupControl.groupByIndex = settings.congregation_groupByIndex;
        settings.congregation_sortOrder = Qt.AscendingOrder;
        congregationLookupControl.sortOrder = settings.congregation_sortOrder;
        settings.congregation_detailRowCount = 3;
        congregationLookupControlMoreMenu.detailRowCount = settings.congregation_detailRowCount;
        settings.sync();
        reloadCongregationDetailList();
    }
    function resetDefaultAssigneeLookupControlSettings() {
        settings.nonstudentAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        settings.nonstudentAssignment_detailRowCount = 3;
        settings.nonstudentAssignment_hideUnavailables = true;
        assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }
    function resetDefaultHospitalityLookupControlSettings() {
        settings.hospitality_groupByIndex = 1;
        hospitalityLookupControl.groupByIndex = settings.hospitality_groupByIndex;
        settings.hospitality_sortOrder = Qt.AscendingOrder;
        hospitalityLookupControl.sortOrder = settings.hospitality_sortOrder;
        settings.hospitality_detailRowCount = 3;
        settings.hospitality_hideUnavailables = true;
        hospitalityLookupControlMoreMenu.detailRowCount = settings.hospitality_detailRowCount;
        hospitalityLookupControlMoreMenu.hideUnavailables = settings.hospitality_hideUnavailables;
        settings.sync();
        reloadHospitalityDetailList();
    }

    CalendarPopup {
        id: calPopup
        onActivated: {
            controller.moveTo(selectedDate)
        }
    }

    Settings {
        id: settings
        category: "LookupControl"
        property int publicTalk_groupByIndex: 1
        property bool publicTalk_sortOrder: Qt.AscendingOrder
        property int publicTalk_detailRowCount: 3
        property int congregation_groupByIndex: 1
        property bool congregation_sortOrder: Qt.AscendingOrder
        property int congregation_detailRowCount: 3
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property int nonstudentAssignment_detailRowCount: 3
        property bool nonstudentAssignment_hideUnavailables: true
        property int hospitality_groupByIndex: 1
        property bool hospitality_sortOrder: Qt.AscendingOrder
        property int hospitality_detailRowCount: 3
        property bool hospitality_hideUnavailables: true
    }

    PublicTalkDetailModel { id: publicTalkDetailModel }
    PublicTalkDetailSFProxyModel {
        id: publicTalkDetailProxyModel
        source: publicTalkDetailModel
        onGroupByChanged: publicTalkDetailProxyModel.sort(0, publicTalkLookupControl.sortOrder)
    }
    CongregationDetailModel { id: congregationDetailModel }
    CongregationDetailSFProxyModel {
        id: congregationDetailProxyModel
        source: congregationDetailModel
        onGroupByChanged: congregationDetailProxyModel.sort(0, congregationLookupControl.sortOrder)
    }
    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: assigneeLookupControlMoreMenu.hideUnavailables
    }
    PersonDetailModel { id: hospitalityDetailModel }
    PersonDetailSFProxyModel {
        id: hospitalityDetailProxyModel
        source: hospitalityDetailModel
        onGroupByChanged: hospitalityDetailProxyModel.sort(0, hospitalityLookupControl.sortOrder)
        areUnavailablesHidden: hospitalityLookupControlMoreMenu.hideUnavailables
    }

    PublicTalkDetailGroupByModel { id: publicTalkDetailGroupByModel }
    PublicTalkDetailGroupByModel { id: congregationDetailGroupByModel }
    PublicTalkDetailGroupByModel { id: assigneeDetailGroupByModel }
    PublicTalkDetailGroupByModel { id: hospitalityDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    WEMeetingValidator {
        id: publicTalkValidator
        field: WEMeetingValidator.Theme
        onErrorChanged: publicTalkErrorLabel.text = error
    }

    WEMeetingValidator {
        id: congregationValidator
        field: WEMeetingValidator.Congregation
        onErrorChanged: congregationErrorLabel.text = error
    }

    WEMeetingValidator {
        id: assigneeValidator
        field: WEMeetingValidator.Speaker
        onErrorChanged: assigneeErrorLabel.text = error
    }

    WEMeetingValidator {
        id: hospitalityValidator
        field: WEMeetingValidator.HospitalityHost
        onErrorChanged: hospitalityErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: publicTalkLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        isHideUnavailablesAllowed: false
        onDetailRowCountChanged: settings.publicTalk_detailRowCount = detailRowCount
        onResetDefaultSettings: resetDefaultPublicTalkLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: congregationLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        isHideUnavailablesAllowed: false
        onDetailRowCountChanged: settings.congregation_detailRowCount = detailRowCount
        onResetDefaultSettings: resetDefaultCongregationLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: assigneeLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
        onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: hospitalityLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        onDetailRowCountChanged: settings.hospitality_detailRowCount = detailRowCount
        onHideUnavailablesChanged: settings.hospitality_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultHospitalityLookupControlSettings()
    }

    Component.onCompleted: {
        publicTalkDetailGroupByModel.remove(5); // remove congregation
        publicTalkDetailGroupByModel.setProperty(3, "value", "alphabet");
        publicTalkDetailGroupByModel.remove(0); // remove name
        publicTalkLookupControl.groupByIndex = settings.publicTalk_groupByIndex;
        publicTalkLookupControl.sortOrder = settings.publicTalk_sortOrder;
        publicTalkLookupControlMoreMenu.detailRowCount = settings.publicTalk_detailRowCount;
        congregationDetailGroupByModel.remove(5); // remove congregation
        congregationDetailGroupByModel.remove(4); // remove public talk number
        congregationDetailGroupByModel.remove(3); // remove public talk theme
        congregationLookupControl.groupByIndex = settings.congregation_groupByIndex;
        congregationLookupControl.sortOrder = settings.congregation_sortOrder;
        congregationLookupControlMoreMenu.detailRowCount = settings.congregation_detailRowCount;
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        assigneeDetailGroupByModel.remove(4); // remove public talk number
        assigneeDetailGroupByModel.remove(3); // remove public talk theme
        hospitalityLookupControl.groupByIndex = settings.hospitality_groupByIndex;
        hospitalityLookupControl.sortOrder = settings.hospitality_sortOrder;
        hospitalityLookupControlMoreMenu.detailRowCount = settings.hospitality_detailRowCount;
        hospitalityLookupControlMoreMenu.hideUnavailables = settings.hospitality_hideUnavailables;
        reloadPublicTalkDetailList();
        reloadCongregationDetailList();
        reloadAssigneeDetailList();
        reloadHospitalityDetailList();
    }

    Component.onDestruction: {
        settings.publicTalk_groupByIndex = publicTalkLookupControl.groupByIndex;
        settings.publicTalk_sortOrder = publicTalkLookupControl.sortOrder;
        settings.publicTalk_detailRowCount = publicTalkLookupControlMoreMenu.detailRowCount;
        settings.congregation_groupByIndex = congregationLookupControl.groupByIndex;
        settings.congregation_sortOrder = congregationLookupControl.sortOrder;
        settings.congregation_detailRowCount = congregationLookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.nonstudentAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.nonstudentAssignment_detailRowCount = assigneeLookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_hideUnavailables = assigneeLookupControlMoreMenu.hideUnavailables;
        settings.hospitality_groupByIndex = hospitalityLookupControl.groupByIndex;
        settings.hospitality_sortOrder = hospitalityLookupControl.sortOrder;
        settings.hospitality_detailRowCount = hospitalityLookupControlMoreMenu.detailRowCount;
        settings.hospitality_hideUnavailables = hospitalityLookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    Connections {
        target: controller
        function onMovedTodoList() {
            todo.reloadTodoList();
        }
        function onMeetingChanged() {
            meeting = controller.meeting
            reloadSpeakerData();
        }
    }

    onMeetingChanged: {
        isLoading = true;
        if (!meeting) return;

        // set validator after meeting is loaded to avoid binding removal
        publicTalkValidator.meeting = meeting;
        congregationValidator.meeting = meeting;
        assigneeValidator.meeting = meeting;
        hospitalityValidator.meeting = meeting;
        //controller.date = meeting.date;
        reloadHospitalityDetailList();
        isLoading = false;
    }

    Connections {
        target: meeting
        function onMeetingChanged() {
            reloadSpeakerData();
        }
    }

    Pane {
        anchors.fill: parent
        clip: true
        padding: 10

        ColumnLayout {
            anchors.fill: parent

            // Congregation
            GridLayout {
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/home.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Congregation")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: congregationLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: congregationDetailProxyModel
                    groupByModel: congregationDetailGroupByModel
                    moreMenu: congregationLookupControlMoreMenu

                    textRole: "congregationName"
                    valueRole: "congregationId"

                    validator: isLoading ? null : congregationValidator

                    delegate: CongregationDetailDelegate {
                        id: congregationDetailItem

                        detailRowCount: congregationLookupControlMoreMenu.detailRowCount
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: congregationDetailItem.width - 2
                            height: congregationDetailItem.height - 2
                            color: congregationLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: congregationDetailItem.down || congregationDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: congregationDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        // reload list of speakers, to display only those from the selected congregation
                        reloadAssigneeDetailList();
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = meeting && meeting.speaker ? indexOfValue(meeting.speaker.congregationid) : -1
                    }
                    popup.onAboutToHide: {
                        var id = currentValue;
                        congregationDetailProxyModel.filterText = "" // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: congregationDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.congregation_groupByIndex = groupByIndex;
                            congregationDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.congregation_sortOrder = sortOrder;
                        congregationDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true;
                        reloadAssigneeDetailList();
                        isLoading = false;
                    }
                }
                Label {
                    id: congregationErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    width: 200
                    wrapMode: Text.Wrap
                }
            }

            // Speaker
            GridLayout {
                columns: 4
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Speaker")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assigneeLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assigneeDetailProxyModel
                    groupByModel: assigneeDetailGroupByModel
                    moreMenu: assigneeLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (meeting && meeting.speaker
                                    ? meeting.speaker.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assigneeValidator

                    delegate: PersonDetailDelegate {
                        id: assigneeDetailItem

                        detailRowCount: assigneeLookupControlMoreMenu.detailRowCount
                        labelingMode: 2
                        includePartsOfOtherMeeting: assigneeLookupControlMoreMenu.includePartsOfOtherMeeting
                        displayAssigneeLabel: false
                        displayTimeLine: false
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assigneeDetailItem.width - 2
                            height: assigneeDetailItem.height - 2
                            color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assigneeDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        var speaker = CPersons.getPerson(currentValue);
                        meeting.speaker = speaker;
                        meeting.save();
                        if (congregationLookupControl.currentIndex === -1 && meeting && meeting.speaker)
                            congregationLookupControl.currentIndex = congregationLookupControl.indexOfValue(meeting.speaker.congregationid)
                        // trigger revalidation by switching isLoaded value
                        isLoading = true;
                        isLoading = false;
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = meeting && meeting.speaker ? indexOfValue(meeting.speaker.id) : -1
                    }
                    popup.onAboutToHide: {
                        var id = meeting && meeting.speaker ? meeting.speaker.id : -1;
                        assigneeDetailProxyModel.filterText = "" // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.nonstudentAssignment_groupByIndex = groupByIndex;
                            assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.nonstudentAssignment_sortOrder = sortOrder;
                        assigneeDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true;
                        meeting.speaker = null;
                        meeting.save();
                        isLoading = false;
                    }
                }
                Label {
                    id: assigneeErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
                ToolButton {
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 2
                    Layout.row: 0
                    icon.source: "qrc:/icons/move_to_week.svg"
                    ToolTip.text: qsTr("Move to different week")
                    ToolTip.visible: hovered
                    onClicked: {
                        if (calPopup.opened) {
                            calPopup.close()
                        } else {
                            var pos = mapToItem(weekendMeetingEdit, 0, 0)
                            calPopup.selectedDate = controller.date
                            calPopup.x = Math.min(pos.x, weekendMeetingEdit.width - calPopup.width - 10)
                            calPopup.y = pos.y + height
                            calPopup.open()
                        }
                    }
                }
                ToolButton {
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 3
                    Layout.row: 0
                    icon.source: "qrc:/icons/move_to_todo.svg"
                    ToolTip.text: qsTr("Send to To Do List")
                    ToolTip.visible: hovered
                    onClicked: {
                        isLoading = true;
                        controller.moveToTodo();
                        congregationLookupControl.currentIndex = -1;
                        assigneeLookupControl.currentIndex = -1;
                        publicTalkLookupControl.currentIndex = -1;
                        isLoading = false;
                    }
                }
            }

            // Theme
            GridLayout {
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/title.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Theme")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: publicTalkLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: publicTalkDetailProxyModel
                    groupByModel: publicTalkDetailGroupByModel
                    moreMenu: publicTalkLookupControlMoreMenu

                    textRole: "numberAndTheme"
                    valueRole: "themeId"

                    validator: isLoading ? null : publicTalkValidator

                    delegate: PublicTalkDetailDelegate {
                        id: publicTalkDetailItem

                        detailRowCount: publicTalkLookupControlMoreMenu.detailRowCount
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: publicTalkDetailItem.width - 2
                            height: publicTalkDetailItem.height - 2
                            color: publicTalkLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: publicTalkDetailItem.down || publicTalkDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: publicTalkDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        meeting.setTheme(currentValue)
                        meeting.save();
                        // trigger revalidation by switching isLoaded value
                        isLoading = true;
                        isLoading = false;
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = meeting && meeting.themeId ? indexOfValue(meeting.themeId) : -1
                    }
                    popup.onAboutToHide: {
                        var id = currentValue;
                        publicTalkDetailProxyModel.filterText = "" // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: publicTalkDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.publicTalk_groupByIndex = groupByIndex;
                            publicTalkDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.publicTalk_sortOrder = sortOrder;
                        publicTalkDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true;
                        meeting.setTheme(0);
                        meeting.save();
                        isLoading = false;
                    }
                }
                Label {
                    id: publicTalkErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/contact_mobile.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Mobile")
                    ToolTip.visible: hovered
                }
                TextArea {
                    Layout.fillWidth: true
                    bottomPadding: 8
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    text: meeting && meeting.speaker ? meeting.speaker.mobile : ""
                    onEditingFinished: { meeting.speaker.mobile = text; meeting.speaker.update() }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/contact_phone.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Phone")
                    ToolTip.visible: hovered
                }
                TextArea {
                    Layout.fillWidth: true
                    bottomPadding: 8
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    text: meeting && meeting.speaker ? meeting.speaker.phone : ""
                    onEditingFinished: { meeting.speaker.phone = text; meeting.speaker.update() }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/contact_email.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Email")
                    ToolTip.visible: hovered
                }
                TextArea {
                    Layout.fillWidth: true
                    bottomPadding: 8
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    text: meeting && meeting.speaker ? meeting.speaker.email : ""
                    onEditingFinished: { meeting.speaker.email = text; meeting.speaker.update() }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/contact_info.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Info")
                    ToolTip.visible: hovered
                }
                TextArea {
                    Layout.fillWidth: true
                    bottomPadding: 8
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    text: meeting && meeting.speaker ? meeting.speaker.info : ""
                    onEditingFinished: { meeting.speaker.info = text; meeting.speaker.update() }
                }
            }

            // Hospitality
            GridLayout {
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/hospitality.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Host")
                    ToolTip.visible: hovered
                }

                LookupComboBox {
                    id: hospitalityLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: hospitalityDetailProxyModel
                    groupByModel: hospitalityDetailGroupByModel
                    moreMenu: hospitalityLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (meeting && meeting.hospitalityHost
                                    ? meeting.hospitalityHost.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : hospitalityValidator

                    delegate: PersonDetailDelegate {
                        id: hospitalityDetailItem

                        detailRowCount: hospitalityLookupControlMoreMenu.detailRowCount
                        labelingMode: 2
                        displayTimeLine: false
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: hospitalityDetailItem.width - 2
                            height: hospitalityDetailItem.height - 2
                            color: hospitalityLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: hospitalityDetailItem.down || hospitalityDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: hospitalityDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true;
                        var assignee = currentValue < 1 ? null : CPersons.getPerson(currentValue);
                        meeting.hospitalityHost = assignee;
                        meeting.save();
                        // trigger re-validation
                        isLoading = false;
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = meeting && meeting.hospitalityHost ? indexOfValue(meeting.hospitalityHost.id) : -1
                    }
                    popup.onAboutToHide: {
                        var id = meeting && meeting.hospitalityHost ? meeting.hospitalityHost.id : -1;
                        hospitalityDetailProxyModel.filterText = "" // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: hospitalityDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0)
                            settings.hospitality_groupByIndex = groupByIndex;
                            hospitalityDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                    }
                    onSortOrderChanged: {
                        settings.hospitality_sortOrder = sortOrder;
                        hospitalityDetailProxyModel.sort(0, sortOrder);
                    }
                    onCleared: {
                        isLoading = true;
                        meeting.hospitalityHost = null;
                        meeting.save();
                        isLoading = false;
                    }
                }
                Label {
                    id: hospitalityErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            Item {
                height: 20
            }

            // To do list
            TodoPanel {
                id: todo
                Layout.fillWidth: true

                isIncomingSpeaker: true

                onMovedToSchedule: {
                    controller.reload();
                }
            }
        }
    }
}
