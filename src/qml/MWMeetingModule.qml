/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.12
import net.theocbase 1.0
import "controls"

Rectangle {
    id: lmmpage

    radius: 5
    color: myPalette.base
    implicitHeight: childrenRect.height

    LMM_Meeting {
        id: myMeeting

        function updateOnCounselorChange() {
            listView.model = myMeeting.getAssignmentsVariant()
        }

        onCounselor2Changed: updateOnCounselorChange()
        onCounselor3Changed: updateOnCounselorChange()
    }

    AssignmentController { id: myController }
    property string bibleReading
    property string cbsSource
    property int selectedMeetingPart
    property int selectedSequence
    property int selectedClass
    property bool editpossible: true
    property bool isLoading: false

    property string chairmanName: "ChairmanName"

    property string lastEditPage: ""
    property int lastEditAssignment: 0
    signal reloadSidebar(var name, var args, var setVisible)

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    function loadSchedule(currentdate){
        // load meeting data
        isLoading = true
        myMeeting.loadMeeting(currentdate)
        listView.model = myMeeting.getAssignmentsVariant()
        chairmanRow.visible = (listView.model.length > 0)
        // song texts
        songBeginningRow.visible = (listView.model.length > 0)
        songEndRow.visible = (listView.model.length > 0)
        // comments
        openingCommentsRow.visible = (listView.model.length > 0)
        reviewCommentsRow.visible = (listView.model.length > 0)
        // Bible reading
        bibleReading = myMeeting.bibleReading
        cbsSource = myMeeting.getAssignment(MeetingPart.LMM_CBS) ? myMeeting.getAssignment(MeetingPart.LMM_CBS).source : ""
        reviewCommentsRow.cotalkfound = myMeeting.getAssignment(MeetingPart.LMM_COTalk) ? true : false

        if (lastEditAssignment > 0) {
            if (lastEditAssignment == 100) {
                openingCommentsRow.clicked()
            } else if (lastEditAssignment == 200) {
                reviewCommentsRow.clicked()
            } else {
                var meetingPart = 0
                for (var i = 0; i < listView.model.length; i++){
                    if (listView.model[i].meetingPart === lastEditAssignment || listView.model[i].meetingPart > lastEditAssignment) {
                        initSidebar(listView.model[i].meetingPart, listView.model[i].sequence, 1, false)
                        break
                    }
                }
            }
        }
        isLoading = false
    }

    function getMonthlyColor(){
        return myMeeting.monthlyColor()
    }

    function initSidebar(index, sequence, classnumber, setVisible){
        var dialogType = ""
        switch(index){
        case MeetingPart.LMM_BibleReading:
        case MeetingPart.LMM_InitialCall:
        case MeetingPart.LMM_ReturnVisit:
        case MeetingPart.LMM_BibleStudy:
        case MeetingPart.LMM_StudentTalk:
        case MeetingPart.LMM_MemorialInvitation:
            dialogType = "../StudentAssignmentPanel.qml";
            break;
        case MeetingPart.LMM_CBS:
            dialogType = "../CBSPanel.qml";
            break;
        default:
            dialogType =  "../AssignmentPanel.qml";
            break;
        }
        reloadSidebar(dialogType, { "currentAssignment" : myMeeting.getAssignment(index, sequence, classnumber) }, setVisible)
        lastEditAssignment = index
    }

    Connections {
        target: openingCommentsRow
        function onClicked() {
            reloadSidebar("../LMMNotesPanel.qml", { "meeting" : myMeeting, "openComments" : true }, true)
            lastEditAssignment = 100
        }
    }

    Connections {
        target: reviewCommentsRow
        function onClicked() {
            reloadSidebar("../LMMNotesPanel.qml", { "meeting" : myMeeting, "openComments" : false }, true)
            lastEditAssignment = 200
        }
    }

    function getIconSource(index){
        switch(index){
        case MeetingPart.LMM_Treasures: return "qrc:///icons/lmm-gw.svg"
        case MeetingPart.LMM_SampleConversationVideo:
        case MeetingPart.LMM_InitialCall:
        case MeetingPart.LMM_ReturnVisit:
        case MeetingPart.LMM_BibleStudy:
        case MeetingPart.LMM_StudentTalk:
        case MeetingPart.LMM_MemorialInvitation:
        case MeetingPart.LMM_OtherFMVideoPart: return "qrc:///icons/lmm-fm.svg"
        case MeetingPart.LMM_LivingTalk1: return "qrc:///icons/lmm-cl.svg"
        default: return ""
        }
    }

    function getTextColor(index){
        switch(index){
        case MeetingPart.LMM_Treasures:
        case MeetingPart.LMM_Digging:
        case MeetingPart.LMM_BibleReading: return TBStyle.lmmSection1TextColor
        case MeetingPart.LMM_SampleConversationVideo:
        case MeetingPart.LMM_InitialCall:
        case MeetingPart.LMM_ReturnVisit:
        case MeetingPart.LMM_BibleStudy:
        case MeetingPart.LMM_StudentTalk:
        case MeetingPart.LMM_MemorialInvitation:
        case MeetingPart.LMM_OtherFMVideoPart: return TBStyle.lmmSection2TextColor
        case MeetingPart.LMM_LivingTalk1:
        case MeetingPart.LMM_LivingTalk2:
        case MeetingPart.LMM_LivingTalk3:
        case MeetingPart.LMM_CBS: return TBStyle.lmmSection3TextColor
        case MeetingPart.Service_Talk: return myPalette.windowText;
        default: return myPalette.windowText;
        }
    }

    function getBackgroundColor(index){
        switch(index){
        case MeetingPart.LMM_Treasures:
        case MeetingPart.LMM_Digging:
        case MeetingPart.LMM_BibleReading: return TBStyle.lmmSection1Color
        case MeetingPart.LMM_SampleConversationVideo:
        case MeetingPart.LMM_InitialCall:
        case MeetingPart.LMM_ReturnVisit:
        case MeetingPart.LMM_BibleStudy:
        case MeetingPart.LMM_StudentTalk:
        case MeetingPart.LMM_MemorialInvitation:
        case MeetingPart.LMM_OtherFMVideoPart: return TBStyle.lmmSection2Color
        case MeetingPart.LMM_LivingTalk1:
        case MeetingPart.LMM_LivingTalk2:
        case MeetingPart.LMM_LivingTalk3:
        case MeetingPart.LMM_CBS: return TBStyle.lmmSection3Color
        case MeetingPart.Service_Talk: return "transparent"
        default: return Qt.tint(myPalette.window, Qt.rgba(255/255, 255/255, 255/255, 0.08))
        }
    }

    function getHeaderText(index){
        switch(index){
        case MeetingPart.LMM_Treasures:
        case MeetingPart.LMM_Digging:
        case MeetingPart.LMM_BibleReading: return qsTr("TREASURES FROM GOD'S WORD")
        case MeetingPart.LMM_SampleConversationVideo:
        case MeetingPart.LMM_InitialCall:
        case MeetingPart.LMM_ReturnVisit:
        case MeetingPart.LMM_BibleStudy:
        case MeetingPart.LMM_StudentTalk:
        case MeetingPart.LMM_MemorialInvitation:
        case MeetingPart.LMM_OtherFMVideoPart: return qsTr("APPLY YOURSELF TO THE FIELD MINISTRY")
        case MeetingPart.LMM_LivingTalk1:
        case MeetingPart.LMM_LivingTalk2:
        case MeetingPart.LMM_LivingTalk3:
        case MeetingPart.LMM_CBS:
        case MeetingPart.Service_Talk: return qsTr("LIVING AS CHRISTIANS")
        default: return ""
        }
    }

    RowLayout {
        width: parent.width
        ColumnLayout {
            id: layout
            Layout.margins: 10
            spacing: 0

            RowLayout {
                Label {
                    text: "●●○ " + Qt.locale().dayName(mwDate.getDay(), Locale.LongFormat) + ", " +
                          mwDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat) + " | " + qsTr("Midweek Meeting").toUpperCase()
                    color: TBStyle.primaryTextColor
                    font: TBStyle.titleSmallFont
                    Layout.alignment: Qt.AlignVCenter
                    verticalAlignment: Text.AlignVCenter
                    Layout.fillWidth: true
                    elide: Text.ElideRight
                }
                RowLayout {
                    Layout.minimumHeight: 40
                    Layout.minimumWidth: 40
                    Layout.alignment: Qt.AlignVCenter
                    ToolButton {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        icon.source: myMeeting.notes ? "qrc:/icons/notes-text.svg" : "qrc:/icons/notes.svg"
                        icon.color: TBStyle.primaryTextColor
                        ToolTip.text: qsTr("Notes", "Meeting Notes")
                        ToolTip.visible: hovered
                        enabled: canViewMeetingNotes
                        onClicked: {
                            reloadSidebar("../MeetingNotes.qml", { "title" : qsTr("Midweek Meeting"), "meeting" : myMeeting, "editable" : canEditMeetingNotes }, true)
                        }
                    }
                }
            }

            Label {
                id: importText
                visible : canEditMidweekMeetingSchedule && (listView.model === undefined || listView.model.length === 0) && editpossible && myMeeting.date.getFullYear() > 2015
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("<html><a href='#'>" + qsTr("Import Schedule...") + "</a></html>")
                onLinkActivated: {
                    console.log("Import schedule link clicked!")
                    root.importClicked()
                }

                DropArea {
                    anchors.fill: parent
                    onEntered: {
                        if (!drag.hasUrls || !drag.urls[0].endsWith(".epub"))
                            drag.accepted = false
                    }
                    onDropped: {
                        console.log("Dropped " + drop.urls)
                        if (drop.urls.length > 0)
                            root.fileDropped(drop.urls[0])
                    }
                }
            }

            ScheduleRowItem {
                id: chairmanRow
                isTimeVisible: false
                clickable: true
                editable: canEditMidweekMeetingSchedule && editpossible
                RowLayout {
                    anchors.fill: parent
                    anchors.rightMargin: parent.buttonRowWidth

                    Repeater {
                        id: chairmanRepeater
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        model: myMeeting.classes
                        Rectangle {
                            id: repeaterRect
                            color: Qt.rgba(myPalette.windowText.r, myPalette.windowText.g, myPalette.windowText.b, 0.1 * index)
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Layout.alignment: Qt.AlignVCenter
                            RowLayout {
                                anchors.fill: parent
                                anchors.verticalCenter: parent.verticalCenter
                                Rectangle {
                                    width: 40 + 8
                                    Layout.fillHeight: true
                                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                                    Layout.rowSpan: 2
                                    color: "transparent"
                                    InfoBox {
                                        visible: myMeeting.classes > 1
                                        boxSize: InfoBox.BoxSize.Small
                                        backgroundColor: TBStyle.primaryTextColor
                                        text: index === 0 ? qsTr("MH", "abbreviation for main hall") :
                                                            (index === 1 ? qsTr("A1", "abbreviation for auxiliary classroom 1") :
                                                                          qsTr("A2", "abbreviation for auxiliary classroom 2"))
                                        anchors.verticalCenter: parent.verticalCenter
                                        anchors.right: parent.right
                                        textColor: myPalette.window
                                    }
                                }
                                Label {
                                    id: counselorTitle
                                    text: index === 0 ? qsTr("Chairman") : qsTr("Counselor")
                                    color: TBStyle.mediumColor
                                    Layout.fillHeight: true
                                    Layout.alignment: Qt.AlignVCenter
                                    verticalAlignment: Text.AlignVCenter
                                }
                                Item {
                                    Layout.fillWidth: true
                                    Layout.fillHeight: true
                                }
                                ValidationTextField {
                                    id: textChairman
                                    Layout.rightMargin: 5
                                    Layout.fillHeight: true
                                    Layout.alignment: Qt.AlignVCenter
                                    horizontalAlignment: TextInput.AlignRight
                                    verticalAlignment: TextInput.AlignVCenter
                                    validator: LMMeetingValidator {
                                        meeting: myMeeting
                                        field: index === 0
                                               ? LMMeetingValidator.Chairman
                                               : (index === 1
                                                  ? LMMeetingValidator.Counselor2
                                                  : LMMeetingValidator.Counselor3)
                                    }
                                    text: index === 0
                                          ? (!isLoading && myMeeting.chairman
                                             ? myMeeting.chairman.fullname
                                             : "")
                                          : (!isLoading && index === 1
                                             ? (myMeeting.counselor2
                                                ? myMeeting.counselor2.fullname
                                                : "")
                                             : (!isLoading && myMeeting.counselor3
                                                ? myMeeting.counselor3.fullname
                                                : ""))
                                    readOnly: true

                                    background: null
                                    padding: 0
                                }
                            }
                        }
                    }
                }
                onClicked: reloadSidebar("../MWMeetingChairmanPanel.qml", { "meeting" : myMeeting }, true)
            }

            ScheduleRowItem {
                id: songBeginningRow
                themeText: qsTr("Song %1 & Prayer").arg(myMeeting.songBeginning.toString() +
                           (showSongTitles ? ": " + myMeeting.songBeginningTitle : ""))
                themeColor: TBStyle.mediumColor
                timeText: "\u266B"
                timeColor: TBStyle.primaryTextColor
                timeBackgroundColor: "transparent"
                timePointSize: TBStyle.bodyLargeFont.pointSize
                nameText1.validator: LMMeetingValidator {
                    meeting: myMeeting
                    field: LMMeetingValidator.OpeningPrayer
                }
                nameText1.text: !isLoading && myMeeting && myMeeting.prayerBeginning
                                ? myMeeting.prayerBeginning.fullname
                                : ""
                editable: canEditMidweekMeetingSchedule && editpossible
                onClicked: reloadSidebar("../MWMeetingPrayerPanel.qml", { "beginningPrayer" : true, "meeting" : myMeeting }, true)
            }

            ScheduleRowItem {
                id: openingCommentsRow
                themeText: qsTr("Opening Comments")
                themeColor: TBStyle.mediumColor
                timeText: "1"
                timeColor: myPalette.window
                timeBackgroundColor: TBStyle.lmmSection1TextColor
                editable: canEditMidweekMeetingSchedule && editpossible
            }

            ListView {
                id: listView
                implicitHeight: childrenRect.height
                delegate: listRow
                Layout.fillWidth: true
                interactive: false
            }

            ScheduleRowItem {
                id: reviewCommentsRow
                themeText: qsTr("Concluding Comments")
                themeColor: TBStyle.mediumColor
                timeText: "3"
                timeColor: myPalette.window
                timeBackgroundColor: TBStyle.lmmSection3TextColor
                editable: canEditMidweekMeetingSchedule && editpossible
                property bool cotalkfound: false
                // Adjust margins when CO visit -> review should be before service talk
                Layout.topMargin: cotalkfound ? -2 * height : 0
                Layout.bottomMargin: cotalkfound ? height : 0
            }

            ScheduleRowItem {
                id: songEndRow
                themeText: qsTr("Song %1 & Prayer").arg(myMeeting.songEnd.toString() +
                           (showSongTitles ? ": " + myMeeting.songEndTitle : ""))
                themeColor: TBStyle.mediumColor
                timeText: "\u266B"
                timeColor: TBStyle.lmmSection3TextColor
                timeBackgroundColor: "transparent"
                timePointSize: TBStyle.bodyLargeFont.pointSize
                nameText1.validator: LMMeetingValidator {
                    meeting: myMeeting
                    field: LMMeetingValidator.FinalPrayer
                }
                nameText1.text: !isLoading && myMeeting && myMeeting.prayerEnd
                                ? myMeeting.prayerEnd.fullname
                                : ""
                editable: canEditMidweekMeetingSchedule && editpossible
                onClicked: reloadSidebar("../MWMeetingPrayerPanel.qml", { "beginningPrayer" : false, "meeting" : myMeeting }, true)
            }
        }
    }

    Component {
        id: listRow
        Rectangle {
            id: headerBackground

            property bool hdr: typeof model.modelData.meetingPart==="undefined" ?
                                   false :
                                   (model.modelData.meetingPart === MeetingPart.LMM_Treasures ||
                                    (index > 0 && model.modelData.meetingPart !== listView.model[index - model.modelData.classnumber].meetingPart &&
                                     listView.model[index - model.modelData.classnumber].meetingPart === MeetingPart.LMM_BibleReading) ||
                                    model.modelData.meetingPart === MeetingPart.LMM_LivingTalk1) && (model.modelData.classnumber === 1)
            property string hdr_text_color
            property bool isSpeakerChanging: false
            property bool isAssistantChanging: false

            Connections {
                target: model.modelData
                ignoreUnknownSignals: true
                function onSpeakerChanged() { revalidateAssistant() }
                function onAssistantChanged() { revalidateSpeaker() }
                function onVolunteerChanged() {
                    revalidateSpeaker();
                    revalidateAssistant();
                }
            }

            function revalidateSpeaker()
            {
                // when this property is changed, revalidation for nameText1 is triggered,
                // because its actual text switches to empty and back, due to its binding
                isAssistantChanging = true
                isAssistantChanging = false
            }

            function revalidateAssistant()
            {
                // when this property is changed, revalidation for nameText2 is triggered,
                // because its actual text switches to empty and back, due to its binding
                isSpeakerChanging = true
                isSpeakerChanging = false
            }

            implicitHeight: childrenRect.height + (model.modelData.meetingPart === MeetingPart.LMM_COTalk ? childrenRect.height : 0)
            width: listView.width
            Component.onCompleted: {
                headerBackground.color = getBackgroundColor(model.modelData.meetingPart)
                rowIcon.source = getIconSource(model.modelData.meetingPart)
                hdr_text_color = getTextColor(model.modelData.meetingPart)
                headerText.text = getHeaderText(model.modelData.meetingPart)
            }

            ColumnLayout {
                width: parent.width
                y: model.modelData.meetingPart === MeetingPart.LMM_COTalk ? childrenRect.height : 0
                spacing: 0

                Rectangle {
                    Layout.fillWidth: true
                    height: 10
                    color: lmmpage.color
                    visible: hdr && model.index > 0
                }

                RowLayout {
                    Rectangle {
                        color: hdr_text_color
                        height: 40
                        width: 40
                        visible: hdr
                        Image {
                            id: rowIcon
                            anchors.fill: parent
                            anchors.margins: 4
                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: myPalette.window
                            }
                        }
                    }
                    // header for meeting part
                    Text {
                        id: headerText
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                        Layout.leftMargin: 4
                        verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                        font: TBStyle.titleLargeFont
                        color: hdr_text_color
                        visible: hdr
                    }
                }
                ScheduleRowItem {
                    themeText: qsTr("Song %1").arg(myMeeting.songMiddle.toString()) +
                               (showSongTitles ? ": " + myMeeting.songMiddleTitle : "")

                    themeColor: TBStyle.mediumColor
                    timeText: "\u266B"
                    timeColor: hdr_text_color
                    timeBackgroundColor: "transparent"
                    timePointSize: TBStyle.bodyLargeFont.pointSize
                    enabled: false
                    editable: canEditMidweekMeetingSchedule
                    visible: hdr && (model.modelData.meetingPart === MeetingPart.LMM_LivingTalk1)
                }

                ScheduleRowItem {
                    themeText: model.modelData.classnumber === 1 ? model.modelData.theme : ""
                    themeColor: model.modelData.classnumber === 1 ? myPalette.windowText : TBStyle.mediumColor
                    timeText: model.modelData.classnumber !== 1 || modelData.time === null ? "" : model.modelData.time
                    timeColor: myPalette.window
                    timeBackgroundColor: hdr_text_color
                    nameText1.validator: LMMAssignmentValidator { assignment: model.modelData; isAssistant: false;
                        isVolunteer: typeof(model.modelData.volunteer) === "undefined" ? false : model.modelData.volunteer }
                    nameText1.text: parent.parent.isAssistantChanging
                                    ? ""
                                    : (model.modelData.volunteer
                                       ? ">" + model.modelData.volunteer.fullname
                                       : model.modelData.speakerFullName
                                         + (model.modelData.meetingPart === MeetingPart.LMM_CBS && model.modelData.speaker
                                            ? " (" + qsTr("Conductor") + ")"
                                            : ""))

                    nameText2.validator: LMMAssignmentValidator { assignment: model.modelData; isAssistant: true }
                    nameText2.text: parent.parent.isSpeakerChanging
                                    ? ""
                                    : (model.modelData.assistant
                                       ? model.modelData.assistant.fullname
                                         + (model.modelData.meetingPart === MeetingPart.LMM_CBS
                                            ? " (" + qsTr("Reader") + ")"
                                            : "")
                                       : "")

                    nameText2.color: TBStyle.mediumColor
                    Item {
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        width: parent.buttonRowWidth
                        visible: !isLoading ? (myMeeting.classes > 1 && model.modelData.canMultiSchool && !parent.isRowHovered) : false
                        InfoBox {
                            boxSize: InfoBox.BoxSize.Small
                            backgroundColor: TBStyle.primaryTextColor
                            text: model.modelData.classnumber === 1 ? qsTr("MH","abbreviation for main hall") :
                                  model.modelData.classnumber === 2 ? qsTr("A1", "abbreviation for auxiliary classroom 1") :
                                                                      qsTr("A2", "abbreviation for auxiliary classroom 2")
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            textColor: myPalette.window
                        }
                    }
                    editable: canEditMidweekMeetingSchedule && editpossible

                    onClicked: {
                        if (!editpossible) return
                        selectedMeetingPart = model.modelData.meetingPart
                        selectedSequence = model.modelData.sequence
                        selectedClass = model.modelData.classnumber
                        initSidebar(model.modelData.meetingPart, model.modelData.sequence, model.modelData.classnumber, true)
                    }
                }
            }
        }
    }
}

