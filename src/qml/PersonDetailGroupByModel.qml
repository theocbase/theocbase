import QtQuick 2.10
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.12
import net.theocbase 1.0
import "controls"

ListModel {
    id: personDetailGroupByModel
    ListElement {
        key: qsTr("Name")
        value: "alphabet"
    }
    ListElement {
        key: qsTr("Date")
        value: "timeRange"
    }
    ListElement {
        key: qsTr("Frequency", "Assignment frequency")
        value: "assignmentFrequencyRange"
    }
    ListElement {
        key: qsTr("Assignment", "Task, e.g.: Study conductor")
        value: "assignmentType"
    }
    ListElement {
        key: qsTr("Meeting part", "Program part, e.g.: Watchtower Study")
        value: "meetingPart"
    }
    ListElement {
        key: qsTr("Theme")
        value: "theme"
    }
    ListElement {
        key: qsTr("Weeks idle", "Without any assignment")
        value: "weeksIdle"
    }
}
