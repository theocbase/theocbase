/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    title: qsTr("Chairman")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property SpecialEventRule specialEventRule
    property CPTMeeting meeting
    property bool isLoading: true

    // javascript functions
    function reloadChairmanDetailList(){
        chairmanDetailModel.loadPersonDetails(0, 0,
                                              meeting.chairman ? meeting.chairman.id : 0, // include current assignment
                                              MeetingType.WeekendMeeting,
                                              1, MeetingPart.PM_Chairman, false, 0,
                                              true, // show weekend parts
                                              lookupControlMoreMenu.includePartsOfOtherMeeting,
                                              personDetailFilterModel.get(0).checked, // nonstudent parts
                                              personDetailFilterModel.get(1).checked, // other assignments
                                              false, // student parts
                                              false, // assistant in student parts
                                              personDetailFilterModel.get(4).checked, // similar assignments only
                                              meeting.date);
        chairmanDetailProxyModel.sort(0, chairmanLookupControl.sortOrder);
    }

    function refreshAssignments(meetingDate) {
        reloadChairmanDetailList();
        // update index to display the assigned persons
        chairmanLookupControl.currentIndex = meeting && meeting.chairman ? chairmanLookupControl.indexOfValue(meeting.chairman.id) : -1;
    }

    function resetDefaultAssigneeLookupControlSettings() {
        personDetailFilterModel.setProperty(0, "checked", true);
        personDetailFilterModel.setProperty(1, "checked", false);
        personDetailFilterModel.setProperty(2, "visible", false);
        personDetailFilterModel.setProperty(3, "visible", false);
        personDetailFilterModel.setProperty(4, "checked", true);
        settings.nonstudentAssignment_filter1 = personDetailFilterModel.get(0).checked;
        settings.nonstudentAssignment_filter2 = personDetailFilterModel.get(1).checked;
        settings.nonstudentAssignment_filter5 = personDetailFilterModel.get(4).checked;
        settings.nonstudentAssignment_groupByIndex = 1;
        chairmanLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
        chairmanLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        settings.nonstudentAssignment_includeMidweekParts = false;
        settings.nonstudentAssignment_detailRowCount = 3;
        settings.nonstudentAssignment_hideUnavailables = true;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeMidweekParts;
        lookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        lookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        settings.sync();
        reloadChairmanDetailList();
    }

    Settings {
        id: settings
        category: "LookupControl"
        property bool nonstudentAssignment_filter1: true
        property bool nonstudentAssignment_filter2: false
        property bool nonstudentAssignment_filter5: true
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property bool nonstudentAssignment_includeMidweekParts: false
        property int nonstudentAssignment_detailRowCount: 3
        property bool nonstudentAssignment_hideUnavailables: true
    }

    PersonDetailModel { id: chairmanDetailModel }
    PersonDetailSFProxyModel {
        id: chairmanDetailProxyModel
        source: chairmanDetailModel
        onGroupByChanged: chairmanDetailProxyModel.sort(0, chairmanLookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: personDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    WEMeetingValidator {
        id: chairmanValidator
        field: WEMeetingValidator.Chairman
        onErrorChanged: chairmanErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: lookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isLabelChangingAllowed: false
        onIncludePartsOfOtherMeetingChanged: {
            settings.nonstudentAssignment_includeMidweekParts = includePartsOfOtherMeeting;
            reloadChairmanDetailList();
        }
        onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
        onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
    }

    Component.onCompleted: {
        personDetailFilterModel.setProperty(0, "checked", settings.nonstudentAssignment_filter1);
        personDetailFilterModel.setProperty(1, "checked", settings.nonstudentAssignment_filter2);
        personDetailFilterModel.setProperty(2, "visible", false);
        personDetailFilterModel.setProperty(3, "visible", false);
        personDetailFilterModel.setProperty(4, "checked", settings.nonstudentAssignment_filter5);
        chairmanLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        chairmanLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeMidweekParts;
        lookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        lookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        chairmanLookupControl.filterModel = personDetailFilterModel;
        personDetailGroupByModel.remove(5); // remove theme
        personDetailGroupByModel.remove(4); // remove meeting part
        personDetailGroupByModel.remove(3); // remove assignment
        reloadChairmanDetailList();
    }

    Component.onDestruction: {
        settings.nonstudentAssignment_groupByIndex = chairmanLookupControl.groupByIndex;
        settings.nonstudentAssignment_sortOrder = chairmanLookupControl.sortOrder;
        settings.nonstudentAssignment_includeMidweekParts = lookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.nonstudentAssignment_detailRowCount = lookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_hideUnavailables = lookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onMeetingChanged: {
        isLoading = true
        if (!meeting) return

        // set validator after meeting is loaded to avoid binding removal
        chairmanValidator.meeting = meeting
        isLoading = false
    }

    Connections {
        target: meeting
        function onDateChanged(date) { refreshAssignments(date) }
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10
        ColumnLayout {
            anchors.fill: parent

            // Chairman
            GridLayout {
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Chairman")
                    ToolTip.visible: hovered
                }

                LookupComboBox {
                    id: chairmanLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: chairmanDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: lookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (meeting && meeting.chairman
                                    ? meeting.chairman.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : chairmanValidator

                    delegate: PersonDetailDelegate {
                        id: chairmanDetailItem

                        meetingDate: meeting.date
                        detailRowCount: lookupControlMoreMenu.detailRowCount
                        labelingMode: 0
                        includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: chairmanDetailItem.width - 2
                            height: chairmanDetailItem.height - 2
                            color: chairmanLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: chairmanDetailItem.down || chairmanDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: chairmanDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true;
                        var assignee = currentValue < 1 ? null : CPersons.getPerson(currentValue);
                        meeting.chairman = assignee;
                        meeting.save();
                        // trigger re-validation
                        isLoading = false;
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = meeting && meeting.chairman ? indexOfValue(meeting.chairman.id) : -1
                    }
                    popup.onAboutToHide: {
                        var id = meeting && meeting.chairman ? meeting.chairman.id : -1;
                        chairmanDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: chairmanDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("nonstudentAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadChairmanDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.nonstudentAssignment_groupByIndex = groupByIndex;
                            chairmanDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.nonstudentAssignment_sortOrder = sortOrder;
                        chairmanDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true;
                        meeting.chairman = null;
                        meeting.save();
                        isLoading = false;
                    }
                }
                Label {
                    id: chairmanErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }
        }
    }
}
