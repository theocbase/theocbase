/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    title: currentAssignment ? currentAssignment.theme : qsTr("Congregation Bible Study")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property LMM_Assignment currentAssignment
    property bool isLoading: true

    // javascript functions
    function reloadAssigneeDetailList() {
        assigneeDetailModel.loadPersonDetails(0, 0,
                                              currentAssignment.speaker ? currentAssignment.speaker.id : 0, // include current assignment
                                              MeetingType.MidweekMeeting,
                                              currentAssignment.classnumber, currentAssignment.meetingPart, false, 0,
                                              assigneeLookupControlMoreMenu.includePartsOfOtherMeeting,
                                              true, // show midweek parts
                                              assigneeDetailFilterModel.get(0).checked, // nonstudent parts
                                              assigneeDetailFilterModel.get(1).checked, // other assignments
                                              assigneeDetailFilterModel.get(2).checked, // student parts
                                              assigneeDetailFilterModel.get(3).checked, // assistant in student parts
                                              assigneeDetailFilterModel.get(4).checked, // similar assignments only
                                              currentAssignment.date);
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
    }

    function reloadAssistantDetailList() {
        assistantDetailModel.loadPersonDetails(0, 0,
                                               currentAssignment.assistant ? currentAssignment.assistant.id : 0, // include current assignment
                                               MeetingType.MidweekMeeting,
                                               currentAssignment.classnumber, currentAssignment.meetingPart, true, 0,
                                               assistantLookupControlMoreMenu.includePartsOfOtherMeeting,
                                               true, // show midweek parts
                                               assistantDetailFilterModel.get(0).checked, // nonstudent parts
                                               assistantDetailFilterModel.get(1).checked, // other assignments
                                               assistantDetailFilterModel.get(2).checked, // student parts
                                               assistantDetailFilterModel.get(3).checked, // assistant in student parts
                                               assistantDetailFilterModel.get(4).checked, // similar assignments only
                                               currentAssignment.date);
        assistantDetailProxyModel.sort(0, assistantLookupControl.sortOrder);
    }

    function resetDefaultAssigneeLookupControlSettings() {
        assigneeDetailFilterModel.setProperty(0, "checked", true);
        assigneeDetailFilterModel.setProperty(1, "checked", false);
        assigneeDetailFilterModel.setProperty(2, "checked", false);
        assigneeDetailFilterModel.setProperty(3, "checked", false);
        assigneeDetailFilterModel.setProperty(4, "checked", true);
        settings.nonstudentAssignment_filter1 = assigneeDetailFilterModel.get(0).checked;
        settings.nonstudentAssignment_filter2 = assigneeDetailFilterModel.get(1).checked;
        settings.nonstudentAssignment_filter3 = assigneeDetailFilterModel.get(2).checked;
        settings.nonstudentAssignment_filter4 = assigneeDetailFilterModel.get(3).checked;
        settings.nonstudentAssignment_filter5 = assigneeDetailFilterModel.get(4).checked;
        settings.nonstudentAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        settings.nonstudentAssignment_includeWeekendParts = false;
        settings.nonstudentAssignment_detailRowCount = 3;
        settings.nonstudentAssignment_labelingMode = 1;
        settings.nonstudentAssignment_hideUnavailables = true;
        assigneeLookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeWeekendParts;
        assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.labelingMode = settings.nonstudentAssignment_labelingMode;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }

    function resetDefaultAssistantLookupControlSettings() {
        assistantDetailFilterModel.setProperty(0, "checked", false);
        assistantDetailFilterModel.setProperty(1, "checked", true);
        assistantDetailFilterModel.setProperty(2, "checked", false);
        assistantDetailFilterModel.setProperty(3, "checked", false);
        assistantDetailFilterModel.setProperty(4, "checked", true);
        settings.otherAssignment_filter1 = assistantDetailFilterModel.get(0).checked;
        settings.otherAssignment_filter2 = assistantDetailFilterModel.get(1).checked;
        settings.otherAssignment_filter3 = assistantDetailFilterModel.get(2).checked;
        settings.otherAssignment_filter4 = assistantDetailFilterModel.get(3).checked;
        settings.otherAssignment_filter5 = assistantDetailFilterModel.get(4).checked;
        settings.otherAssignment_groupByIndex = 1;
        assistantLookupControl.groupByIndex = settings.otherAssignment_groupByIndex;
        settings.otherAssignment_sortOrder = Qt.AscendingOrder;
        assistantLookupControl.sortOrder = settings.otherAssignment_sortOrder;
        settings.otherAssignment_includeWeekendParts = true;
        settings.otherAssignment_detailRowCount = 3;
        settings.otherAssignment_labelingMode = 2;
        settings.otherAssignment_hideUnavailables = true;
        assistantLookupControlMoreMenu.includePartsOfOtherMeeting = settings.otherAssignment_includeWeekendParts;
        assistantLookupControlMoreMenu.detailRowCount = settings.otherAssignment_detailRowCount;
        assistantLookupControlMoreMenu.labelingMode = settings.otherAssignment_labelingMode;
        assistantLookupControlMoreMenu.hideUnavailables = settings.otherAssignment_hideUnavailables;
        settings.sync();
        reloadAssistantDetailList();
    }

    Settings {
        id: settings
        category: "LookupControl"
        property bool nonstudentAssignment_filter1: true
        property bool nonstudentAssignment_filter2: false
        property bool nonstudentAssignment_filter3: false
        property bool nonstudentAssignment_filter4: false
        property bool nonstudentAssignment_filter5: true
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property bool nonstudentAssignment_includeWeekendParts: false
        property int nonstudentAssignment_detailRowCount: 3
        property int nonstudentAssignment_labelingMode: 1
        property bool nonstudentAssignment_hideUnavailables: true
        property bool otherAssignment_filter1: false
        property bool otherAssignment_filter2: true
        property bool otherAssignment_filter3: false
        property bool otherAssignment_filter4: false
        property bool otherAssignment_filter5: true
        property int otherAssignment_groupByIndex: 1
        property bool otherAssignment_sortOrder: Qt.AscendingOrder
        property bool otherAssignment_includeWeekendParts: true
        property int otherAssignment_detailRowCount: 3
        property int otherAssignment_labelingMode: 2
        property bool otherAssignment_hideUnavailables: true
    }

    AssignmentController { id: myController }

    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: assigneeLookupControlMoreMenu.hideUnavailables
    }

    PersonDetailModel { id: assistantDetailModel }
    PersonDetailSFProxyModel {
        id: assistantDetailProxyModel
        source: assistantDetailModel
        onGroupByChanged: assistantDetailProxyModel.sort(0, assistantLookupControl.sortOrder)
        areUnavailablesHidden: assistantLookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: assigneeDetailFilterModel }
    PersonDetailFilterModel { id: assistantDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    LMMAssignmentValidator {
        id: assigneeValidator
        assignment: currentAssignment
        isAssistant: false
        onErrorChanged: assigneeErrorLabel.text = error
    }

    LMMAssignmentValidator {
        id: assistantValidator
        assignment: currentAssignment
        isAssistant: true
        onErrorChanged: assistantErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: assigneeLookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.nonstudentAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAssigneeDetailList();
        }
        onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.nonstudentAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: assistantLookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.otherAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAssistantDetailList();
        }
        onDetailRowCountChanged: settings.otherAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.otherAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.otherAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssistantLookupControlSettings()
    }

    Component.onCompleted: {
        assigneeDetailFilterModel.setProperty(0, "checked", settings.nonstudentAssignment_filter1);
        assigneeDetailFilterModel.setProperty(1, "checked", settings.nonstudentAssignment_filter2);
        assigneeDetailFilterModel.setProperty(2, "checked", settings.nonstudentAssignment_filter3);
        assigneeDetailFilterModel.setProperty(3, "checked", settings.nonstudentAssignment_filter4);
        assigneeDetailFilterModel.setProperty(4, "checked", settings.nonstudentAssignment_filter5);
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        assigneeLookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeWeekendParts;
        assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.labelingMode = settings.nonstudentAssignment_labelingMode;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        assigneeLookupControl.filterModel = assigneeDetailFilterModel;
        assistantDetailFilterModel.setProperty(0, "checked", settings.otherAssignment_filter1);
        assistantDetailFilterModel.setProperty(1, "checked", settings.otherAssignment_filter2);
        assistantDetailFilterModel.setProperty(2, "checked", settings.otherAssignment_filter3);
        assistantDetailFilterModel.setProperty(3, "checked", settings.otherAssignment_filter4);
        assistantDetailFilterModel.setProperty(4, "checked", settings.otherAssignment_filter5);
        assistantLookupControl.groupByIndex = settings.otherAssignment_groupByIndex;
        assistantLookupControl.sortOrder = settings.otherAssignment_sortOrder;
        assistantLookupControlMoreMenu.includePartsOfOtherMeeting = settings.otherAssignment_includeWeekendParts;
        assistantLookupControlMoreMenu.detailRowCount = settings.otherAssignment_detailRowCount;
        assistantLookupControlMoreMenu.labelingMode = settings.otherAssignment_labelingMode;
        assistantLookupControlMoreMenu.hideUnavailables = settings.otherAssignment_hideUnavailables;
        assistantLookupControl.filterModel = assistantDetailFilterModel;
        reloadAssigneeDetailList();
        reloadAssistantDetailList();
    }

    Component.onDestruction: {
        settings.nonstudentAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.nonstudentAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.nonstudentAssignment_includeWeekendParts = assigneeLookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.nonstudentAssignment_detailRowCount = assigneeLookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_labelingMode = assigneeLookupControlMoreMenu.labelingMode;
        settings.nonstudentAssignment_hideUnavailables = assigneeLookupControlMoreMenu.hideUnavailables;
        settings.otherAssignment_groupByIndex = assistantLookupControl.groupByIndex;
        settings.otherAssignment_sortOrder = assistantLookupControl.sortOrder;
        settings.otherAssignment_includeWeekendParts = assistantLookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.otherAssignment_detailRowCount = assistantLookupControlMoreMenu.detailRowCount;
        settings.otherAssignment_labelingMode = assistantLookupControlMoreMenu.labelingMode;
        settings.otherAssignment_hideUnavailables = assistantLookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onCurrentAssignmentChanged: {
        isLoading = true;
        if (!currentAssignment)
        {
            assigneeDetailModel.clear();
            assistantDetailModel.clear();
            return;
        }
        isLoading = false;
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10

        ColumnLayout {
            anchors.fill: parent

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/wt_source.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Source")
                    ToolTip.visible: hovered
                }
                TextArea {
                    Layout.fillWidth: true
                    id: textareaSource
                    text: currentAssignment ? currentAssignment.source : ""
                    readOnly: true
                    wrapMode: Text.WordWrap
                    topPadding: 0
                    bottomPadding: 0
                    background: null
                }
            }

            GridLayout {
                Layout.fillWidth: true
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Conductor")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assigneeLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assigneeDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: assigneeLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (currentAssignment && currentAssignment.speaker
                                    ? currentAssignment.speaker.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assigneeValidator

                    delegate: PersonDetailDelegate {
                        id: assigneeDetailItem

                        meetingDate: currentAssignment.date
                        detailRowCount: assigneeLookupControlMoreMenu.detailRowCount
                        labelingMode: assigneeLookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: assigneeLookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assigneeDetailItem.width - 2
                            height: assigneeDetailItem.height - 2
                            color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assigneeDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        // trigger revalidation by switching isLoaded value
                        isLoading = true
                        currentAssignment.speaker = currentValue < 1 ? null : myController.getPublisherById(currentValue)
                        currentAssignment.save();
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = currentAssignment && currentAssignment.speaker ? indexOfValue(currentAssignment.speaker.id) : -1
                    }
                    popup.onAboutToHide: {
                        var id = currentAssignment && currentAssignment.speaker ? currentAssignment.speaker.id : -1;
                        assigneeDetailProxyModel.filterText = "" // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("nonstudentAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadAssigneeDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.nonstudentAssignment_groupByIndex = groupByIndex;
                            assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.nonstudentAssignment_sortOrder = sortOrder;
                        assigneeDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        currentAssignment.speaker = null
                        currentAssignment.save()
                        isLoading = false
                    }
                }
                Label {
                    id: assigneeErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            GridLayout {
                Layout.fillWidth: true
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/reader.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Reader")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assistantLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assistantDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: assistantLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (currentAssignment && currentAssignment.assistant
                                    ? currentAssignment.assistant.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assistantValidator

                    delegate: PersonDetailDelegate {
                        id: assistantDetailItem

                        meetingDate: currentAssignment.date
                        detailRowCount: assistantLookupControlMoreMenu.detailRowCount
                        labelingMode: assistantLookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: assistantLookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assistantDetailItem.width - 2
                            height: assistantDetailItem.height - 2
                            color: assistantLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assistantDetailItem.down || assistantDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assistantDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        // trigger revalidation by switching isLoaded value
                        isLoading = true
                        currentAssignment.assistant = currentValue < 1 ? null : myController.getPublisherById(currentValue);
                        currentAssignment.save();
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = currentAssignment && currentAssignment.assistant ? indexOfValue(currentAssignment.assistant.id) : -1
                    }
                    popup.onAboutToHide: {
                        var id = currentAssignment && currentAssignment.assistant ? currentAssignment.assistant.id : -1;
                        assistantDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assistantDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("otherAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadAssistantDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.otherAssignment_groupByIndex = groupByIndex;
                            assistantDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.otherAssignment_sortOrder = sortOrder;
                        assistantDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        currentAssignment.assistant = null
                        currentAssignment.save()
                        isLoading = false
                    }
                }
                Label {
                    id: assistantErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    icon.source: "qrc:/icons/notes.svg"
                    background: null
                    ToolTip.text: qsTr("Note")
                    ToolTip.visible: hovered
                }
                TextArea{
                    id: texteditNote
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    selectByMouse: true
                    text: currentAssignment ? currentAssignment.note : ""
                    onEditingFinished: {
                        if (currentAssignment.note !== text) {
                            currentAssignment.note = text
                            currentAssignment.save()
                        }
                    }
                }
            }
        }
    }
}
