/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "controls"

Item {
    id: _numberSelector
    property int maxValue: 50
    property int selectedValue: 0
    property int boxSize: 40
    QtObject {
      id: internal
      property int neededBoxSize: boxSize + 5
    }
    width: 300
    height: Math.ceil( (maxValue+1) / Math.floor(width / internal.neededBoxSize ) ) * internal.neededBoxSize

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    GridView {
        id: grid
        anchors.fill: parent
        clip: true

        model: maxValue + 1

        cellWidth: boxSize
        cellHeight: boxSize

        delegate: numberDelegate
    }

    Component {
        id: numberDelegate

        ToolButton {
            width: boxSize
            height: boxSize
            text: index

            checked: index == selectedValue
            onClicked: {
                selectedValue = index
            }
        }
    }
}

