import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

ItemDelegate {
    id: todoItemDelegate

    // property declarations
    property bool isValidationTextVisible: false
    property bool isDateLoading: true
    property bool isCongregationLoading: true
    property bool isSpeakerLoading: true
    property bool isThemeLoading: true

    // signal declarations
    signal deleteTodoItem(int id)
    signal moveTodoItem(int id)
    signal editTodoItem(int id)

    // object properties
    width: LookupControl.view.width
    highlighted: LookupControl.view.currentIndex === index
    hoverEnabled: LookupControl.view.hoverEnabled

    // child objects
    TodoValidator {
        id: congregationValidator
        model: todoItemDelegate.ListView.view.model.source
        todoId: id
        role: TodoModel.CongregationNameRole
        onErrorChanged: congregationErrorLabel.text = error
        onTodoIdChanged: {
            isValidationTextVisible = false;
            isCongregationLoading = true;
            isCongregationLoading = false;
        }
    }
    TodoValidator {
        id: speakerValidator
        model: todoItemDelegate.ListView.view.model.source
        todoId: id
        role: TodoModel.SpeakerFullNameRole
        onErrorChanged: speakerErrorLabel.text = error
        onTodoIdChanged: {
            isValidationTextVisible = false;
            isSpeakerLoading = true;
            isSpeakerLoading = false;
        }
    }
    TodoValidator {
        id: themeValidator
        model: todoItemDelegate.ListView.view.model.source
        todoId: id
        role: TodoModel.ThemeAndNumberRole
        onErrorChanged: themeErrorLabel.text = error
        onTodoIdChanged: {
            isValidationTextVisible = false;
            isThemeLoading = true;
            isThemeLoading = false;
        }
    }
    TodoValidator {
        id: dateValidator
        model: todoItemDelegate.ListView.view.model.source
        todoId: id
        role: TodoModel.DateRole
        onErrorChanged: dateErrorLabel.text = error
        onTodoIdChanged: {
            isValidationTextVisible = false;
            isDateLoading = true;
            isDateLoading = false;
        }
    }

    Connections {
        target: todoItemDelegate.LookupControl.view.model
        function onSortChanged() {
            isValidationTextVisible = false;
        }
        function onGroupByChanged() {
            isValidationTextVisible = false;
        }
        function onFilterChanged() {
            isValidationTextVisible = false;
        }
    }
    Connections {
        target: todoItemDelegate.LookupControl.view.model.source
        function onModelChanged() {
            isValidationTextVisible = false;
        }
    }

    contentItem: MouseArea {
        id: mouseRowArea
        width: parent.width
        implicitHeight: childrenRect.height
        hoverEnabled: true
        onHoveredChanged: {
            if (hovered)
                todoItemDelegate.ListView.view.currentIndex = index;
            else
                todoItemDelegate.ListView.view.currentIndex = -1;
        }

        ColumnLayout {
            width: parent.width

            RowLayout {
                Layout.fillWidth: true
                spacing: 6
                Layout.margins: 0

                ValidationTextField {
                    id: speakerTextField
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop
                    Layout.margins: 0
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter

                    text: speakerFullName
                    wrapMode: TextInput.WordWrap
                    validator: isSpeakerLoading ? null : speakerValidator
                    readOnly: true
                    font: TBStyle.bodyLargeFont
                    background: null
                    activeFocusOnPress: false
                }
                ValidationTextField {
                    id: congregationTextField
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop | Qt.AlignRight
                    Layout.margins: 0
                    Layout.rightMargin: 5
                    horizontalAlignment: Text.AlignRight
                    verticalAlignment: Text.AlignVCenter

                    text: congregationName
                    wrapMode: TextInput.WordWrap
                    validator: isCongregationLoading ? null : congregationValidator
                    readOnly: true
                    font: TBStyle.bodyLargeFont
                    background: null
                    activeFocusOnPress: false
                }
                Item {
                    implicitWidth: Math.max(buttonEdit.implicitBackgroundWidth + buttonEdit.leftInset + buttonEdit.rightInset,
                                            buttonEdit.implicitContentWidth + buttonEdit.leftPadding + buttonEdit.rightPadding) * 3
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                    Layout.margins: 0

                    RowLayout {
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        ToolButton {
                            id: buttonMove
                            icon.source: "qrc:/icons/move_to_week.svg"
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                            z: 2
                            enabled: speakerFullName !== "" && themeAndNumber !== "" && congregationName !== ""
                            visible: mouseRowArea.containsMouse
                            onClicked: {
                                isValidationTextVisible = !(dateTextField.acceptableInput
                                                            && congregationTextField.acceptableInput
                                                            && speakerTextField.acceptableInput
                                                            && themeTextField.acceptableInput);
                                if (!isValidationTextVisible)
                                    moveTodoItem(id);
                            }
                        }
                        ToolButton {
                            id: buttonDelete
                            icon.source: "qrc:/icons/delete.svg"
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignVCenter
                            z: 2
                            visible: mouseRowArea.containsMouse
                            onClicked: deleteTodoItem(id)
                        }
                        ToolButton {
                            id: buttonEdit
                            icon.source: "qrc:/icons/edit.svg"
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignVCenter
                            z: 2
                            visible: mouseRowArea.containsMouse
                            onClicked: editTodoItem(id)
                        }
                    }
                }
            }
            RowLayout {
                Layout.fillWidth: true
                Layout.margins: 0

                ValidationTextField {
                    id: themeTextField
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                    Layout.margins: 0
                    horizontalAlignment: Text.AlignLeft
                    implicitWidth: todoItemDelegate.ListView.view.width - 80

                    text: themeAndNumber
                    wrapMode: TextInput.WordWrap
                    validator: isThemeLoading ? null : themeValidator
                    readOnly: true
                    background: null
                    activeFocusOnPress: false
                }
                ValidationTextField {
                    id: dateTextField
                    implicitWidth: 80
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignRight
                    Layout.margins: 0
                    horizontalAlignment: Text.AlignRight

                    text: date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                    validator: isDateLoading ? null : dateValidator
                    readOnly: true
                    background: null
                    activeFocusOnPress: false
                }
            }

            ColumnLayout {
                Layout.fillWidth: true
                visible: isValidationTextVisible

                Label {
                    id: dateErrorLabel
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                }
                Label {
                    id: congregationErrorLabel
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                }
                Label {
                    id: speakerErrorLabel
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                }
                Label {
                    id: themeErrorLabel
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                }
            }
        }
    }

    background: Rectangle {
        width: todoItemDelegate.ListView.view.width
        color: index % 2 == 0 ? myPalette.base : myPalette.alternateBase
    }
}
