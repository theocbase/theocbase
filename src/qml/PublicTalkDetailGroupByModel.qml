import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

ListModel {
    id: publicTalkDetailGroupByModel
    ListElement {
        key: qsTr("Name")
        value: "alphabet"
    }
    ListElement {
        key: qsTr("Date")
        value: "timeRange"
    }
    ListElement {
        key: qsTr("Frequency", "Assignment frequency")
        value: "assignmentFrequencyRange"
    }
    ListElement {
        key: qsTr("Theme")
        value: "theme"
    }
    ListElement {
        key: qsTr("Number", "Public talk number")
        value: "themeNumberRange"
    }
    ListElement {
        key: qsTr("Congregation")
        value: "congregationName"
    }
}
