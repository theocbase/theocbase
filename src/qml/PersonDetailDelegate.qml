import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Dialogs 1.3
import QtQuick.Shapes 1.15
import QtGraphicalEffects 1.15
import net.theocbase 1.0
import Qt.labs.qmlmodels 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

ItemDelegate {
    property int detailRowCount: -1
    property int labelingMode: 1
    property bool includePartsOfOtherMeeting
    property bool displayAssigneeLabel: true
    property bool displayTimeLine: true
    property bool isOutgoingSpeaker: false
    property date meetingDate

    width: LookupControl.view.width
    highlighted: LookupControl.view.currentIndex === index
    hoverEnabled: LookupControl.view.hoverEnabled

    function formatAssignmentLabel(theme, assignmentInfo, congregationName, labelingMode) {
        if (!assignmentInfo)
            return "";
        return (includePartsOfOtherMeeting
                ? SPScripts.getMeetingIndicator(assignmentInfo.meetingType) + " "
                : "")
                + (labelingMode === 0
                   ? assignmentInfo.assignmentTypeName
                   : (labelingMode === 1
                      ? assignmentInfo.meetingPartName
                      : (labelingMode === 2
                         ? theme
                         : congregationName)))
                + (assignmentInfo.roomNumber > 1
                   ? " [" + SPScripts.getClassName(assignmentInfo.roomNumber) + "]"
                   : "");
    }
    function formatAssigneeLabel(personFullName, assigneeFullName, volunteerFullName, assistantFullName) {
        if (!assignmentInfo || !displayAssigneeLabel)
            return "";
        return (assigneeFullName === personFullName || volunteerFullName === personFullName ? "\u2611" : (volunteerFullName !== "" ? volunteerFullName : assigneeFullName)) +
                (assistantFullName !== "" ? (" | " + (assistantFullName === personFullName ? "\u2611" : "(" + assistantFullName + ")")) : "");
    }

    contentItem: ColumnLayout {
        width: parent.width

        RowLayout {
            Layout.fillWidth: true

            Item {
                implicitWidth: 25
                height: childrenRect.height
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                ToolButton {
                    background: null
                    icon.source: isUnavailable
                                 ? "qrc:///icons/warning_inactive.svg"
                                 : (isMultiAssigned || (isOutgoingSpeaker && isSpeakerAwayInSameMonth)
                                    ? "qrc:///icons/warning_busy.svg"
                                    : (isFamilyMemberAssigned
                                       ? "qrc:///icons/warning_family_member.svg"
                                       : ""))
                    icon.color: TBStyle.alertColor
                    visible: isUnavailable || isMultiAssigned || (isOutgoingSpeaker && isSpeakerAwayInSameMonth) || isFamilyMemberAssigned
                }
            }

            Item {
                implicitHeight: 30
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                Label {
                    width: parent.width
                    anchors.centerIn: parent
                    text: personDisplayName
                    elide: Text.ElideRight
                    font: TBStyle.bodyLargeFont
                }
            }

            Item {
                Layout.minimumWidth: 100
                implicitWidth: 100
                implicitHeight: 30
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.rightMargin: 0

                Label {
                    width: parent.width
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignRight
                    text: SPScripts.formatRating(assignmentFrequencyRange)
                    font: TBStyle.bodyLargeFont
                }
            }

            Item {
                width: 90
                implicitWidth: 90
                implicitHeight: 30
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                Layout.rightMargin: 0

                Canvas {
                    id: timeline

                    width: 90
                    height: 30
                    anchors.centerIn: parent
                    onPaint: {
                        var startX = 45;
                        var startY = 15;
                        var weekOfPrevious0 = SPScripts.getRelativeWeekOf(meetingDate, date);
                        var weekOfPrevious1 = SPScripts.getRelativeWeekOf(meetingDate, previous1Date);
                        var weekOfPrevious2 = SPScripts.getRelativeWeekOf(meetingDate, previous2Date);

                        var ctx = getContext("2d");
                        ctx.translate(0.5, 0.5); // shift the coordinates to draw on pixels instead of the border between pixels
                        var radius = 3;
                        // draw timeline
                        for (var i = -4; i <= 4; i++)
                        {
                            if (i == 0)
                                SPScripts.drawTimeRange(ctx, startX, startY, 5, myPalette.windowText, timeRange);
                            else
                            {
                                var currentAssignmentInfo = null;
                                if (i === weekOfPrevious2)
                                    currentAssignmentInfo = previous2AssignmentInfo;
                                if (i === weekOfPrevious1)
                                    currentAssignmentInfo = previous1AssignmentInfo;
                                if (i === weekOfPrevious0)
                                    currentAssignmentInfo = assignmentInfo;
                                var isHighlighted = currentAssignmentInfo;
                                var isFilled = currentAssignmentInfo && !currentAssignmentInfo.isSupplementary;
                                ctx.strokeStyle = myPalette.windowText;
                                ctx.fillStyle = ctx.strokeStyle;
                                ctx.lineWidth = isHighlighted ? 2 : 1;
                                ctx.beginPath();
                                ctx.arc(startX + i * 8 + (i < 0 ? -2 : (i > 0 ? 2 : 0)), startY, radius - (isHighlighted ? 0.5 : 0), 0, 2 * Math.PI);
                                ctx.stroke();
                                if (isFilled)
                                    ctx.fill();

                                // connecting lines
                                ctx.lineWidth = 1.5;
                                ctx.strokeStyle = myPalette.windowText;
                                ctx.beginPath();
                                ctx.moveTo(startX + i * 8 + (i < 0 ? -2 : (i > 0 ? -6 : 0)) + radius, startY);
                                ctx.lineTo(startX + (i + 1) * 8 + (i < 0 ? -2 : (i > 0 ? -6 : 0)) - radius, startY);
                                ctx.stroke();
                            }
                        }
                        SPScripts.drawAnyAssignmentMarker(ctx,
                                                          startX - weekOfAnyPreviousAssignment * 8 - (weekOfAnyPreviousAssignment < 0 ? -2 : weekOfAnyPreviousAssignment > 0 ? 2 : 0),
                                                          startY, radius + 2, -1 * weekOfAnyPreviousAssignment, anyPreviousAssignmentInfo);
                        SPScripts.drawAnyAssignmentMarker(ctx,
                                                          startX + weekOfAnyFollowingAssignment * 8 + (weekOfAnyFollowingAssignment < 0 ? -2 : weekOfAnyFollowingAssignment > 0 ? 2 : 0),
                                                          startY, radius + 2, weekOfAnyFollowingAssignment, anyFollowingAssignmentInfo);
                    }
                }
            }
        }

        ColumnLayout {
            height: childrenRect.height
            Layout.fillWidth: true
            Layout.leftMargin: 5
            visible: detailRowCount >= 1 || detailRowCount == -1 && theme !== ""

            RowLayout {
                Layout.fillWidth: true
                height: childrenRect.height

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

                    Label {
                        width: parent.width
                        verticalAlignment: Text.AlignVCenter
                        anchors.centerIn: parent
                        text: formatAssignmentLabel(theme, assignmentInfo, congregationName, labelingMode)
                        elide: Text.ElideRight
                        font.strikeout: assigneeFullName === personFullName
                                        && volunteerFullName !== ""
                        visible: theme
                    }
                }

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    visible: displayAssigneeLabel

                    Label {
                        width: parent.width
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        text: formatAssigneeLabel(personFullName, assigneeFullName, volunteerFullName, assistantFullName)
                        elide: Text.ElideMiddle
                        visible: !(assigneeFullName === personFullName
                                   && volunteerFullName !== "")
                    }
                }

                Item {
                    width: 90
                    implicitWidth: 90
                    height: 20
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.rightMargin: 0

                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors.centerIn: parent
                        text: date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                        elide: Text.ElideMiddle
                        font.strikeout: assigneeFullName === personFullName
                                        && volunteerFullName !== ""
                    }
                }
            }
        }

        ColumnLayout {
            height: childrenRect.height
            Layout.fillWidth: true
            Layout.leftMargin: 5
            visible: detailRowCount >= 2 || detailRowCount == -1 && previous1Theme !== ""

            RowLayout {
                Layout.fillWidth: true
                height: childrenRect.height

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Label {
                        width: parent.width
                        verticalAlignment: Text.AlignVCenter
                        anchors.centerIn: parent
                        text: formatAssignmentLabel(previous1Theme, previous1AssignmentInfo, previous1CongregationName, labelingMode)
                        elide: Text.ElideRight
                        font.strikeout: previous1AssigneeFullName === personFullName
                                        && previous1VolunteerFullName !== ""
                        visible: previous1Theme
                    }
                }

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    visible: displayAssigneeLabel

                    Label {
                        width: parent.width
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        text: formatAssigneeLabel(personFullName, previous1AssigneeFullName, previous1VolunteerFullName, previous1AssistantFullName)
                        elide: Text.ElideMiddle
                        visible: !(previous1AssigneeFullName === personFullName
                                   && previous1VolunteerFullName !== "")
                    }
                }

                Item {
                    width: 90
                    implicitWidth: 90
                    height: 20
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.rightMargin: 0
                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors.centerIn: parent
                        text: previous1Date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                        elide: Text.ElideMiddle
                        font.strikeout: previous1AssigneeFullName === personFullName
                                        && previous1VolunteerFullName !== ""
                    }
                }
            }
        }

        ColumnLayout {
            height: childrenRect.height
            Layout.fillWidth: true
            Layout.leftMargin: 5
            Layout.bottomMargin: 5
            visible: detailRowCount >= 3 || detailRowCount == -1 && previous2Theme !== ""

            RowLayout {
                Layout.fillWidth: true
                height: childrenRect.height

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    Label {
                        id: previous2Label
                        width: parent.width
                        verticalAlignment: Text.AlignVCenter
                        anchors.centerIn: parent
                        textFormat: Text.MarkdownText
                        text: previous2AssigneeFullName === personFullName && previous2VolunteerFullName !== ""
                              ? "~~" + textMetrics.elidedText + "~~"
                              : textMetrics.elidedText
                        font: TBStyle.bodySmallFont
                        visible: previous2Theme

                        TextMetrics {
                            id: textMetrics
                            font: TBStyle.bodySmallFont
                            elide: Text.ElideRight
                            elideWidth: previous2Label.width
                            text: formatAssignmentLabel(previous2Theme, previous2AssignmentInfo, previous2CongregationName, labelingMode)
                        }
                    }
                }

                Item {
                    height: 20
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                    visible: displayAssigneeLabel

                    Label {
                        width: parent.width
                        anchors.centerIn: parent
                        horizontalAlignment: Text.AlignRight
                        text: formatAssigneeLabel(personFullName, previous2AssigneeFullName, previous2VolunteerFullName, previous2AssistantFullName)
                        elide: Text.ElideMiddle
                        visible: !(previous2AssigneeFullName === personFullName
                                   && previous2VolunteerFullName !== "")
                        font: TBStyle.bodySmallFont
                    }
                }

                Item {
                    width: 90
                    implicitWidth: 90
                    height: 20
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.rightMargin: 0
                    Label {
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        anchors.centerIn: parent
                        text: previous2AssigneeFullName === personFullName && previous2VolunteerFullName !== ""
                              ? "~~" + previous2Date.toLocaleDateString(Qt.locale(), Locale.ShortFormat) + "~~"
                              : previous2Date.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                        textFormat: Text.MarkdownText
                        elide: Text.ElideMiddle
                        font: TBStyle.bodySmallFont
                    }
                }
            }
        }
    }

    background: Item {}
}
