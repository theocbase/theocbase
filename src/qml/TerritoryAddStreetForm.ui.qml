import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import QtQuick.Window 2.1
import QtQml.Models 2.2
import QtQuick.Dialogs 1.2
import net.theocbase 1.0

Item {
    id: item1
    property alias okButton: okButton
    property alias cancelButton: cancelButton
    property alias searchStreetTextField: searchStreetTextField
    property alias hideAddedStreets: hideAddedStreets
    property alias streetResultTableView: streetResultTableView
    property alias isBusy: busyIndicator.running
    property alias copyShortCut: copyShortcut
    property int territoryId: 0

    anchors.fill: parent

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        spacing: 10

        Label {
            id: name
            text: qsTr("Select the streets to be added to the territory:",
                       "Add street names to territoy")
        }

        TextField {
            id: searchStreetTextField
            width: 400
            Layout.maximumWidth: 500
            Layout.fillWidth: true
            placeholderText: qsTr("search",
                                  "Search in the list of new street names")
            focus: true
        }

        TableView {
            id: streetResultTableView
            Layout.fillWidth: true
            Layout.fillHeight: true

            Layout.columnSpan: 4

            model: streetResultSFProxyModel

            selectionMode: SelectionMode.ExtendedSelection

            Shortcut {
                id: copyShortcut
                sequence: StandardKey.Copy
            }

            TableViewColumn {
                width: 60
                visible: true
                role: "isChecked"
                delegate: streetSelectionDelegate
            }

            TableViewColumn {
                title: qsTr("Street", "Street name")
                width: 400
                visible: true
                role: "streetName"
                elideMode: Text.ElideRight
            }

            rowDelegate: Rectangle {
                id: tableViewRowDelegate
                height: 25
                color: styleData.selected ? myPalette.highlight : (styleData.alternate ? myPalette.alternateBase : myPalette.base)
            }
        }

        RowLayout {
            Layout.fillWidth: true
            CheckBox {
                id: hideAddedStreets
                text: qsTr("Hide already added streets",
                           "Hide those streets that are already in the territory's street list")
                checked: true
            }

            ColumnLayout {
                Layout.fillWidth: true
            }

            RowLayout {
                id: rowLayout
                height: 33
                Layout.alignment: Qt.AlignRight | Qt.AlignBottom

                Button {
                    id: okButton
                    text: qsTr("OK")
                }

                Button {
                    id: cancelButton
                    text: qsTr("Cancel")
                }
            }
        }
    }

    BusyIndicator {
        id: busyIndicator
        anchors.centerIn: parent
        running: true
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

