import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.15
import Qt.labs.qmlmodels 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0

Menu {
    id: lookupControlMoreMenu

    property bool isRowCountChangingAllowed: true
    property int detailRowCount
    property bool isLabelChangingAllowed: true
    property int allowedLabelingModes: 7
    property var labelingModes: [
        //: Task, e.g.: Study conductor
        qsTr("Assignment", "Lookup control settings"),
        //: Program part, e.g.: Watchtower Study
        qsTr("Meeting part", "Lookup control settings"),
        //: Theme of a meeting part, e.g. title of a talk
        qsTr("Theme", "Lookup control settings")]
    property int labelingMode
    property int meetingType
    property bool isIncludingPartsOfOtherMeetingAllowed: true
    property bool includePartsOfOtherMeeting
    property bool hideUnavailables: true
    property bool isHideUnavailablesAllowed: true

    signal resetDefaultSettings

    MenuItem { indicator: null; background: null
        enabled: isRowCountChangingAllowed || isIncludingPartsOfOtherMeetingAllowed
        visible: isRowCountChangingAllowed || isIncludingPartsOfOtherMeetingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        contentItem: Label {
            //: Supplementary lines of details in a list item
            text: qsTr("Display details", "Lookup control settings")
            font: TBStyle.bodyLargeFont
        }
    }
    MenuItem {
        //: List item with no line of details
        text: qsTr("Off", "Lookup control settings")
        enabled: isRowCountChangingAllowed
        visible: isRowCountChangingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isRowCountChangingAllowed
        checked: detailRowCount === 0
        onTriggered: detailRowCount = 0
    }
    MenuItem {
        //: List item with one line of details
        text: qsTr("1 Line", "Lookup control settings")
        enabled: isRowCountChangingAllowed
        visible: isRowCountChangingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isRowCountChangingAllowed
        checked: detailRowCount === 1
        onTriggered: detailRowCount = 1
    }
    MenuItem {
        //: List item with two lines of details
        text: qsTr("2 Lines", "Lookup control settings")
        enabled: isRowCountChangingAllowed
        visible: isRowCountChangingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isRowCountChangingAllowed
        checked: detailRowCount === 2
        onTriggered: detailRowCount = 2
    }
    MenuItem {
        //: List item with three lines of details
        text: qsTr("3 Lines", "Lookup control settings")
        enabled: isRowCountChangingAllowed
        visible: isRowCountChangingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isRowCountChangingAllowed
        checked: detailRowCount === 3
        onTriggered: detailRowCount = 3
    }
    MenuItem {
        //: List item with a maximum of three lines of details
        text: qsTr("Automatically", "Lookup control settings")
        enabled: isRowCountChangingAllowed
        visible: isRowCountChangingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isRowCountChangingAllowed
        checked: detailRowCount === -1
        onTriggered: detailRowCount = -1
    }
    MenuSeparator {
        height: isRowCountChangingAllowed && isIncludingPartsOfOtherMeetingAllowed ? implicitHeight : 0
        width: isRowCountChangingAllowed && isIncludingPartsOfOtherMeetingAllowed ? implicitWidth : 0
    }
    MenuItem {
        text: meetingType === MeetingType.WeekendMeeting
              ? //: Midweek meeting assignments
                qsTr("Include midweek parts", "Lookup control settings")
              : //: Weekend meeting assignments
                qsTr("Include weekend parts", "Lookup control settings");
        enabled: isIncludingPartsOfOtherMeetingAllowed
        visible: isIncludingPartsOfOtherMeetingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isIncludingPartsOfOtherMeetingAllowed
        checked: includePartsOfOtherMeeting
        onTriggered: includePartsOfOtherMeeting = checked
    }
    MenuItem {
        indicator: null
        background: null
        contentItem: Label {
            //: To select which text to display, e.g. the theme or name of a meeting part
            text: qsTr("Labeling", "Lookup control settings")
            font: TBStyle.bodyLargeFont
            visible: isLabelChangingAllowed
        }
        enabled: isLabelChangingAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
    }
    MenuItem {
        text: labelingModes && labelingModes.length > 0 ? labelingModes[0] : ""
        enabled: isLabelChangingAllowed && (allowedLabelingModes & 1) > 0
        visible: isLabelChangingAllowed && (allowedLabelingModes & 1) > 0
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: true
        checked: labelingMode === 0
        onTriggered: labelingMode = 0
    }
    MenuItem {
        text: labelingModes && labelingModes.length > 1 ? labelingModes[1] : ""
        enabled: isLabelChangingAllowed && (allowedLabelingModes & 2) > 0
        visible: isLabelChangingAllowed && (allowedLabelingModes & 2) > 0
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: true
        checked: labelingMode === 1
        onTriggered: labelingMode = 1
    }
    MenuItem {
        text: labelingModes && labelingModes.length > 2 ? labelingModes[2] : ""
        enabled: isLabelChangingAllowed && (allowedLabelingModes & 4) > 0
        visible: isLabelChangingAllowed && (allowedLabelingModes & 4) > 0
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: true
        checked: labelingMode === 2
        onTriggered: labelingMode = 2
    }
    MenuSeparator {
        visible: isRowCountChangingAllowed || isIncludingPartsOfOtherMeetingAllowed || isLabelChangingAllowed
        height: visible ? implicitHeight : 0
        width: visible ? implicitWidth : 0
    }
    MenuItem {
        text: qsTr("Hide unavailables", "Lookup control settings");
        enabled: isHideUnavailablesAllowed
        visible: isHideUnavailablesAllowed
        height: enabled ? implicitHeight : 0
        width: enabled ? implicitWidth : 0
        checkable: isHideUnavailablesAllowed
        checked: hideUnavailables
        onTriggered: hideUnavailables = checked
    }
    MenuItem { text: qsTr("Reset default settings", "Lookup control settings"); onTriggered: resetDefaultSettings() }
}
