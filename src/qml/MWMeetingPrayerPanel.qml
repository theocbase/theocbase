/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    id: mwPrayerPanel

    title: qsTr("Song %1 and Prayer").arg(beginningPrayer ? meeting.songBeginning.toString() : meeting.songEnd.toString())
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property bool beginningPrayer: true
    property LMM_Meeting meeting
    property bool isLoading: true

    // javascript functions
    function reloadAssigneeDetailList(){
        assigneeDetailModel.loadPersonDetails(0, 0,
                                              beginningPrayer // include current assignment
                                              ? (meeting && meeting.prayerBeginning ? meeting.prayerBeginning.id : 0)
                                              : (meeting && meeting.prayerEnd ? meeting.prayerEnd.id : 0),
                                              MeetingType.MidweekMeeting,
                                              1, MeetingPart.SongAndPrayer, false, 0,
                                              lookupControlMoreMenu.includePartsOfOtherMeeting,
                                              true, // show midweek parts
                                              personDetailFilterModel.get(0).checked, // nonstudent parts
                                              personDetailFilterModel.get(1).checked, // other assignments
                                              personDetailFilterModel.get(2).checked, // student parts
                                              personDetailFilterModel.get(3).checked, // assistant in student parts
                                              personDetailFilterModel.get(4).checked, // similar assignments only
                                              meeting.date);
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
    }

    function refreshAssignments(meetingDate) {
        reloadAssigneeDetailList();
        // update index to display the assigned persons
        assigneeLookupControl.currentIndex = beginningPrayer
                ? (meeting && meeting.prayerBeginning ? assigneeLookupControl.indexOfValue(meeting.prayerBeginning.id) : -1)
                : (meeting && meeting.prayerEnd ? assigneeLookupControl.indexOfValue(meeting.prayerEnd.id) : -1)
    }

    function resetDefaultLookupControlSettings() {
        personDetailFilterModel.setProperty(0, "checked", false);
        personDetailFilterModel.setProperty(1, "checked", true);
        personDetailFilterModel.setProperty(2, "checked", false);
        personDetailFilterModel.setProperty(3, "checked", false);
        personDetailFilterModel.setProperty(4, "checked", true);
        settings.otherAssignment_filter1 = personDetailFilterModel.get(0).checked;
        settings.otherAssignment_filter2 = personDetailFilterModel.get(1).checked;
        settings.otherAssignment_filter3 = personDetailFilterModel.get(2).checked;
        settings.otherAssignment_filter4 = personDetailFilterModel.get(3).checked;
        settings.otherAssignment_filter5 = personDetailFilterModel.get(4).checked;
        settings.otherAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.otherAssignment_groupByIndex;
        settings.otherAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.otherAssignment_sortOrder;
        settings.otherAssignment_includeWeekendParts = true;
        settings.otherAssignment_detailRowCount = 3;
        settings.otherAssignment_labelingMode = 2;
        settings.otherAssignment_hideUnavailables = true;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.otherAssignment_includeWeekendParts;
        lookupControlMoreMenu.detailRowCount = settings.otherAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.otherAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.otherAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }

    Settings {
        id: settings
        category: "LookupControl"
        property bool otherAssignment_filter1: false
        property bool otherAssignment_filter2: true
        property bool otherAssignment_filter3: false
        property bool otherAssignment_filter4: false
        property bool otherAssignment_filter5: true
        property int otherAssignment_groupByIndex: 1
        property bool otherAssignment_sortOrder: Qt.AscendingOrder
        property bool otherAssignment_includeWeekendParts: true
        property int otherAssignment_detailRowCount: 3
        property int otherAssignment_labelingMode: 2
        property bool otherAssignment_hideUnavailables: true
    }

    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: personDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    LMMeetingValidator {
        id: assigneeValidator
        field: beginningPrayer ? LMMeetingValidator.OpeningPrayer : LMMeetingValidator.FinalPrayer
        onErrorChanged: assigneeErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: lookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.otherAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAssigneeDetailList();
        }
        onDetailRowCountChanged: settings.otherAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.otherAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.otherAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultLookupControlSettings()
    }

    Connections {
        target: meeting
        function onDateChanged(date) { refreshAssignments(date) }
    }

    Component.onCompleted: {
        personDetailFilterModel.setProperty(0, "checked", settings.otherAssignment_filter1);
        personDetailFilterModel.setProperty(1, "checked", settings.otherAssignment_filter2);
        personDetailFilterModel.setProperty(2, "checked", settings.otherAssignment_filter3);
        personDetailFilterModel.setProperty(3, "checked", settings.otherAssignment_filter4);
        personDetailFilterModel.setProperty(4, "checked", settings.otherAssignment_filter5);
        assigneeLookupControl.groupByIndex = settings.otherAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.otherAssignment_sortOrder;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.otherAssignment_includeWeekendParts;
        lookupControlMoreMenu.detailRowCount = settings.otherAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.otherAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.otherAssignment_hideUnavailables;
        assigneeLookupControl.filterModel = personDetailFilterModel;
        reloadAssigneeDetailList();
    }

    Component.onDestruction: {
        settings.otherAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.otherAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.otherAssignment_includeWeekendParts = lookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.otherAssignment_detailRowCount = lookupControlMoreMenu.detailRowCount;
        settings.otherAssignment_labelingMode = lookupControlMoreMenu.labelingMode;
        settings.otherAssignment_hideUnavailables = lookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onMeetingChanged: {
        isLoading = true
        if (!meeting) return

        // set validator after meeting is loaded to avoid binding removal
        assigneeValidator.meeting = meeting
        isLoading = false
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10

        // Prayer
        GridLayout {
            columns: 2
            rows: 2
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.column: 0
                Layout.row: 0
                ToolTip.text: qsTr("Prayer")
                ToolTip.visible: hovered
            }
            ColumnLayout {
                LookupComboBox {
                    id: assigneeLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assigneeDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: lookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (beginningPrayer
                                    ? (meeting && meeting.prayerBeginning ? meeting.prayerBeginning.fullname : "")
                                    : (meeting && meeting.prayerEnd ? meeting.prayerEnd.fullname : ""))
                                 : currentText

                    validator: isLoading ? null : assigneeValidator

                    delegate: PersonDetailDelegate {
                        id: assigneeDetailItem

                        meetingDate: meeting.date
                        detailRowCount: lookupControlMoreMenu.detailRowCount
                        labelingMode: lookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assigneeDetailItem.width - 2
                            height: assigneeDetailItem.height - 2
                            color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assigneeDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true
                        var assignee = currentValue < 1 ? null : CPersons.getPerson(currentValue)
                        if (beginningPrayer)
                            meeting.prayerBeginning = assignee;
                        else
                            meeting.prayerEnd = assignee;
                        meeting.save();
                        // trigger re-validation
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = beginningPrayer
                                ? (meeting && meeting.prayerBeginning ? indexOfValue(meeting.prayerBeginning.id) : -1)
                                : (meeting && meeting.prayerEnd ? indexOfValue(meeting.prayerEnd.id) : -1)
                    }
                    popup.onAboutToHide: {
                        var id = beginningPrayer
                                ? (meeting && meeting.prayerBeginning ? meeting.prayerBeginning.id : -1)
                                : (meeting && meeting.prayerEnd ? meeting.prayerEnd.id : -1);
                        assigneeDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("otherAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadAssigneeDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.otherAssignment_groupByIndex = groupByIndex;
                            assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.otherAssignment_sortOrder = sortOrder;
                        assigneeDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        if (beginningPrayer)
                            meeting.prayerBeginning = null;
                        else
                            meeting.prayerEnd = null;
                        meeting.save();
                        isLoading = false
                    }
                }
                Label {
                    id: assigneeErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }
        }
    }
}
