/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import net.theocbase 1.0
import "controls"

Page {
    title: qsTr("Service Talk", "Circuit overseer's talk in the wekeend meeting")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }

    PublicMeetingController { id: controller }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10
        ColumnLayout {
            anchors.fill: parent

            // Theme
            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/title.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Theme")
                    ToolTip.visible: hovered
                }
                TextArea {
                    id: textTheme
                    text: meeting ? meeting.finalTalk : ""
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                    onEditingFinished: {
                        // save theme
                        if (text != meeting.finalTalk) {
                            meeting.finalTalk = text
                            meeting.save()
                        }
                    }
                }
            }

            // Speaker
            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Speaker")
                    ToolTip.visible: hovered
                }

                ComboBox {
                    id: comboSpeaker
                    currentIndex: 0
                    model: [meeting ? meeting.finalTalkSpeakerName : ""]
                    Layout.fillWidth: true
                    enabled: false
                }
            }
        }
    }
}

