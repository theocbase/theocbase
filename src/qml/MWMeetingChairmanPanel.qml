/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    id: mwChairmanPanel

    title: qsTr("Chairman")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property LMM_Meeting meeting
    property bool isLoading: true

    // javascript functions
    function reloadChairmanDetailList(){
        chairmanDetailModel.loadPersonDetails(0, 0,
                                              meeting.chairman ? meeting.chairman.id : 0, // include current assignment
                                              MeetingType.MidweekMeeting,
                                              1, MeetingPart.LMM_Chairman, false, 0,
                                              lookupControlMoreMenu.includePartsOfOtherMeeting,
                                              true, // show midweek parts
                                              personDetailFilterModel.get(0).checked, // nonstudent parts
                                              personDetailFilterModel.get(1).checked, // other assignments
                                              personDetailFilterModel.get(2).checked, // student parts
                                              personDetailFilterModel.get(3).checked, // assistant in student parts
                                              personDetailFilterModel.get(4).checked, // similar assignments only
                                              meeting.date);
        chairmanDetailProxyModel.sort(0, chairmanLookupControl.sortOrder);
    }

    function reloadCounselor2DetailList(){
        counselor2DetailModel.loadPersonDetails(0, 0,
                                                meeting.counselor2 ? meeting.counselor2.id : 0, // include current assignment
                                                MeetingType.MidweekMeeting,
                                                2, MeetingPart.LMM_Counselor, false, 0,
                                                lookupControlMoreMenu.includePartsOfOtherMeeting,
                                                true, // show midweek parts
                                                personDetailFilterModel.get(0).checked, // nonstudent parts
                                                personDetailFilterModel.get(1).checked, // other assignments
                                                personDetailFilterModel.get(2).checked, // student parts
                                                personDetailFilterModel.get(3).checked, // assistant in student parts
                                                personDetailFilterModel.get(4).checked, // similar assignments only
                                                meeting.date);
        counselor2DetailProxyModel.sort(0, counselor2LookupControl.sortOrder);
    }

    function reloadCounselor3DetailList(){
        counselor3DetailModel.loadPersonDetails(0, 0,
                                                meeting.counselor3 ? meeting.counselor3.id : 0, // include current assignment
                                                MeetingType.MidweekMeeting,
                                                3, MeetingPart.LMM_Counselor, false, 0,
                                                lookupControlMoreMenu.includePartsOfOtherMeeting,
                                                true, // show midweek parts
                                                personDetailFilterModel.get(0).checked, // nonstudent parts
                                                personDetailFilterModel.get(1).checked, // other assignments
                                                personDetailFilterModel.get(2).checked, // student parts
                                                personDetailFilterModel.get(3).checked, // assistant in student parts
                                                personDetailFilterModel.get(4).checked, // similar assignments only
                                                meeting.date);
        counselor3DetailProxyModel.sort(0, counselor3LookupControl.sortOrder);
    }

    function reloadAllDetailLists(){
        reloadChairmanDetailList();
        reloadCounselor2DetailList();
        reloadCounselor3DetailList();
    }

    function refreshAssignments(meetingDate) {
        reloadAllDetailLists();

        // update index to display the assigned persons
        chairmanLookupControl.currentIndex = meeting && meeting.chairman ? chairmanLookupControl.indexOfValue(meeting.chairman.id) : -1;
        counselor2LookupControl.currentIndex = meeting && meeting.counselor2 ? counselor2LookupControl.indexOfValue(meeting.counselor2.id) : -1;
        counselor3LookupControl.currentIndex = meeting && meeting.counselor3 ? counselor3LookupControl.indexOfValue(meeting.counselor3.id) : -1;
    }

    function save(id, classnumber)
    {
        var b = id < 1 ? null : CPersons.getPerson(id)
        switch(classnumber){
        case 1:
            meeting.chairman = b
            break;
        case 2:
            meeting.counselor2 = b
            break;
        case 3:
            meeting.counselor3 = b
            break;
        }
        if (meeting.date.getFullYear() >= 2018) {
            // sample conversation video
            var video = meeting.getAssignment(MeetingPart.LMM_SampleConversationVideo, 0, classnumber)
            if (video !== null) {
                video.speaker = b
                video.save()
            }
        }
        meeting.save()
    }

    function resetDefaultAssigneeLookupControlSettings() {
        personDetailFilterModel.setProperty(0, "checked", true);
        personDetailFilterModel.setProperty(1, "checked", false);
        personDetailFilterModel.setProperty(2, "checked", false);
        personDetailFilterModel.setProperty(3, "checked", false);
        personDetailFilterModel.setProperty(4, "checked", true);
        settings.nonstudentAssignment_filter1 = personDetailFilterModel.get(0).checked;
        settings.nonstudentAssignment_filter2 = personDetailFilterModel.get(1).checked;
        settings.nonstudentAssignment_filter3 = personDetailFilterModel.get(2).checked;
        settings.nonstudentAssignment_filter4 = personDetailFilterModel.get(3).checked;
        settings.nonstudentAssignment_filter5 = personDetailFilterModel.get(4).checked;
        settings.nonstudentAssignment_groupByIndex = 1;
        chairmanLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        counselor2LookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        counselor3LookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
        chairmanLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        counselor2LookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        counselor3LookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        settings.nonstudentAssignment_includeWeekendParts = false;
        settings.nonstudentAssignment_detailRowCount = 3;
        settings.nonstudentAssignment_labelingMode = 1;
        settings.nonstudentAssignment_hideUnavailables = true;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeWeekendParts;
        lookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.nonstudentAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        settings.sync();
        reloadAllDetailLists();
    }

    Settings {
        id: settings
        category: "LookupControl"
        property bool nonstudentAssignment_filter1: true
        property bool nonstudentAssignment_filter2: false
        property bool nonstudentAssignment_filter3: false
        property bool nonstudentAssignment_filter4: false
        property bool nonstudentAssignment_filter5: true
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property bool nonstudentAssignment_includeWeekendParts: false
        property int nonstudentAssignment_detailRowCount: 3
        property int nonstudentAssignment_labelingMode: 1
        property bool nonstudentAssignment_hideUnavailables: true
    }

    PersonDetailModel { id: chairmanDetailModel }
    PersonDetailSFProxyModel {
        id: chairmanDetailProxyModel
        source: chairmanDetailModel
        onGroupByChanged: chairmanDetailProxyModel.sort(0, chairmanLookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailModel { id: counselor2DetailModel }
    PersonDetailSFProxyModel {
        id: counselor2DetailProxyModel
        source: counselor2DetailModel
        onGroupByChanged: counselor2DetailProxyModel.sort(0, counselor2LookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailModel { id: counselor3DetailModel }
    PersonDetailSFProxyModel {
        id: counselor3DetailProxyModel
        source: counselor3DetailModel
        onGroupByChanged: counselor3DetailProxyModel.sort(0, counselor3LookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: personDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    LMMeetingValidator {
        id: chairmanValidator
        field: LMMeetingValidator.Chairman
        onErrorChanged: chairmanErrorLabel.text = error
    }

    LMMeetingValidator {
        id: counselor2Validator
        field: LMMeetingValidator.Counselor2
        onErrorChanged: counselor2ErrorLabel.text = error
    }

    LMMeetingValidator {
        id: counselor3Validator
        field: LMMeetingValidator.Counselor3
        onErrorChanged: counselor3ErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: lookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.nonstudentAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAllDetailLists();
        }
        onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.nonstudentAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
    }

    Component.onCompleted: {
        personDetailFilterModel.setProperty(0, "checked", settings.nonstudentAssignment_filter1);
        personDetailFilterModel.setProperty(1, "checked", settings.nonstudentAssignment_filter2);
        personDetailFilterModel.setProperty(2, "checked", settings.nonstudentAssignment_filter3);
        personDetailFilterModel.setProperty(3, "checked", settings.nonstudentAssignment_filter4);
        personDetailFilterModel.setProperty(4, "checked", settings.nonstudentAssignment_filter5);
        chairmanLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        counselor2LookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        counselor3LookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        chairmanLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        counselor2LookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        counselor3LookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeWeekendParts;
        lookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.nonstudentAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        chairmanLookupControl.filterModel = personDetailFilterModel;
        counselor2LookupControl.filterModel = personDetailFilterModel;
        counselor3LookupControl.filterModel = personDetailFilterModel;
        reloadAllDetailLists();
    }

    Component.onDestruction: {
        settings.nonstudentAssignment_groupByIndex = chairmanLookupControl.groupByIndex;
        settings.nonstudentAssignment_sortOrder = chairmanLookupControl.sortOrder;
        settings.nonstudentAssignment_includeWeekendParts = lookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.nonstudentAssignment_detailRowCount = lookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_labelingMode = lookupControlMoreMenu.labelingMode;
        settings.nonstudentAssignment_hideUnavailables = lookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onMeetingChanged: {
        isLoading = true
        if (!meeting) return

        // set validator after meeting is loaded to avoid binding removal
        chairmanValidator.meeting = meeting;
        counselor2Validator.meeting = meeting;
        counselor3Validator.meeting = meeting;
        isLoading = false
    }

    Connections {
        target: meeting
        function onDateChanged(date) { refreshAssignments(date) }
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 10

        // Chairman
        GridLayout {
            columns: 2
            rows: 2
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.column: 0
                Layout.row: 0
                ToolTip.text: qsTr("Chairman")
                ToolTip.visible: hovered
            }
            LookupComboBox {
                id: chairmanLookupControl

                height: 40
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 0

                showFilterControls: true
                showGroupControls: true
                groupByIndex: -1
                showEditButton: false
                isEditing: false

                model: chairmanDetailProxyModel
                groupByModel: personDetailGroupByModel
                moreMenu: lookupControlMoreMenu

                textRole: "personFullName"
                valueRole: "personId"
                displayText: currentIndex === -1
                             ? (meeting && meeting.chairman
                                ? meeting.chairman.fullname
                                : "")
                             : currentText

                validator: isLoading ? null : chairmanValidator

                delegate: PersonDetailDelegate {
                    id: chairmanDetailItem

                    meetingDate: meeting.date
                    detailRowCount: lookupControlMoreMenu.detailRowCount
                    labelingMode: lookupControlMoreMenu.labelingMode
                    includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                    background: Rectangle {
                        implicitWidth: 200
                        implicitHeight: 40
                        x: 1
                        y: 1
                        width: chairmanDetailItem.width - 2
                        height: chairmanDetailItem.height - 2
                        color: chairmanLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                        ColorOverlay {
                            anchors.fill: parent
                            source: parent
                            color: chairmanDetailItem.down || chairmanDetailItem.highlighted ? myPalette.highlight : "transparent"
                            opacity: chairmanDetailItem.down ? 1.0 : 0.25
                        }
                    }
                }

                sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                sectionCriteria: ViewSection.FullString
                sectionDelegate: personDetailSectionDelegate

                // When an item is selected, update the backend.
                onActivated: {
                    isLoading = true
                    save(currentValue, 1);
                    // trigger re-validation
                    isLoading = false
                }
                // Set the initial currentIndex to the value stored in the backend.
                Component.onCompleted: {
                    currentIndex = meeting && meeting.chairman ? indexOfValue(meeting.chairman.id) : -1
                }
                popup.onAboutToHide: {
                    var id = meeting && meeting.chairman ? meeting.chairman.id : -1;
                    chairmanDetailProxyModel.filterText = ""; // reset filter when closing the popup
                    currentIndex = indexOfValue(id); // restore currentIndex to selected value
                }
                onSearchTextChanged: chairmanDetailProxyModel.filterText = text
                onFilterChanged: {
                    settings.setValue("nonstudentAssignment_filter" + (index + 1), checked);
                    settings.sync();
                    reloadChairmanDetailList();
                }
                onGroupByIndexChanged: {
                    if (groupByIndex >= 0) {
                        settings.nonstudentAssignment_groupByIndex = groupByIndex;
                        chairmanDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                    }
                }
                onSortOrderChanged: {
                    settings.nonstudentAssignment_sortOrder = sortOrder;
                    chairmanDetailProxyModel.sort(0, sortOrder)
                }
                onCleared: {
                    isLoading = true
                    save(0, 1);
                    chairmanLookupControl.currentIndex = -1
                    isLoading = false
                }
            }
            Label {
                id: chairmanErrorLabel
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 1
                verticalAlignment: Text.AlignTop
                font: TBStyle.bodySmallFont
                color: TBStyle.alertColor
                visible: text
                wrapMode: Text.Wrap
            }
        }
        // Counselor 2
        GridLayout {
            columns: 2
            rows: 2
            visible: meeting.classes > 1
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.column: 0
                Layout.row: 0
                ToolTip.text: qsTr("Auxiliary Classroom Counselor II")
                ToolTip.visible: hovered
            }
            LookupComboBox {
                id: counselor2LookupControl

                height: 40
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 0

                showFilterControls: true
                showGroupControls: true
                groupByIndex: -1
                showEditButton: false
                isEditing: false

                model: counselor2DetailProxyModel
                groupByModel: personDetailGroupByModel
                moreMenu: lookupControlMoreMenu

                textRole: "personFullName"
                valueRole: "personId"
                displayText: currentIndex === -1
                             ? (meeting && meeting.counselor2
                                ? meeting.counselor2.fullname
                                : "")
                             : currentText

                validator: isLoading ? null : counselor2Validator

                delegate: PersonDetailDelegate {
                    id: counselor2DetailItem

                    meetingDate: meeting.date
                    detailRowCount: lookupControlMoreMenu.detailRowCount
                    labelingMode: lookupControlMoreMenu.labelingMode
                    includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                    background: Rectangle {
                        implicitWidth: 200
                        implicitHeight: 40
                        x: 1
                        y: 1
                        width: counselor2DetailItem.width - 2
                        height: counselor2DetailItem.height - 2
                        color: counselor2LookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                        ColorOverlay {
                            anchors.fill: parent
                            source: parent
                            color: counselor2DetailItem.down || counselor2DetailItem.highlighted ? myPalette.highlight : "transparent"
                            opacity: counselor2DetailItem.down ? 1.0 : 0.25
                        }
                    }
                }

                sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                sectionCriteria: ViewSection.FullString
                sectionDelegate: personDetailSectionDelegate

                // When an item is selected, update the backend.
                onActivated: {
                    isLoading = true
                    save(currentValue, 2);
                    // trigger re-validation
                    isLoading = false
                }
                // Set the initial currentIndex to the value stored in the backend.
                Component.onCompleted: {
                    currentIndex = meeting && meeting.counselor2 ? indexOfValue(meeting.counselor2.id) : -1
                }
                popup.onAboutToHide: {
                    var id = meeting && meeting.counselor2 ? meeting.counselor2.id : -1;
                    counselor2DetailProxyModel.filterText = ""; // reset filter when closing the popup
                    currentIndex = indexOfValue(id); // restore currentIndex to selected value
                }
                onSearchTextChanged: counselor2DetailProxyModel.filterText = text
                onFilterChanged: {
                    settings.setValue("nonstudentAssignment_filter" + (index + 1), checked);
                    settings.sync();
                    reloadCounselor2DetailList();
                }
                onGroupByIndexChanged: {
                    if (groupByIndex >= 0) {
                        settings.nonstudentAssignment_groupByIndex = groupByIndex;
                        counselor2DetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                    }
                }
                onSortOrderChanged: {
                    settings.nonstudentAssignment_sortOrder = sortOrder;
                    counselor2DetailProxyModel.sort(0, sortOrder)
                }
                onCleared: {
                    isLoading = true
                    save(0, 2);
                    isLoading = false
                }
            }
            Label {
                id: counselor2ErrorLabel
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 1
                verticalAlignment: Text.AlignTop
                font: TBStyle.bodySmallFont
                color: TBStyle.alertColor
                visible: text
                wrapMode: Text.Wrap
            }
        }
        // Counselor 3
        GridLayout {
            columns: 2
            rows: 2
            visible: meeting.classes > 2
            ToolButton {
                icon.source: "qrc:/icons/servant.svg"
                background: null
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                Layout.column: 0
                Layout.row: 0
                ToolTip.text: qsTr("Auxiliary Classroom Counselor III")
                ToolTip.visible: hovered
            }
            LookupComboBox {
                id: counselor3LookupControl

                height: 40
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 0

                showFilterControls: true
                showGroupControls: true
                groupByIndex: -1
                showEditButton: false
                isEditing: false

                model: counselor3DetailProxyModel
                groupByModel: personDetailGroupByModel
                moreMenu: lookupControlMoreMenu

                textRole: "personFullName"
                valueRole: "personId"
                displayText: currentIndex === -1
                             ? (meeting && meeting.counselor3
                                ? meeting.counselor3.fullname
                                : "")
                             : currentText

                validator: isLoading ? null : counselor3Validator

                delegate: PersonDetailDelegate {
                    id: counselor3DetailItem

                    meetingDate: meeting.date
                    detailRowCount: lookupControlMoreMenu.detailRowCount
                    labelingMode: lookupControlMoreMenu.labelingMode
                    includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                    background: Rectangle {
                        implicitWidth: 200
                        implicitHeight: 40
                        x: 1
                        y: 1
                        width: counselor3DetailItem.width - 2
                        height: counselor3DetailItem.height - 2
                        color: counselor3LookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                        ColorOverlay {
                            anchors.fill: parent
                            source: parent
                            color: counselor3DetailItem.down || counselor3DetailItem.highlighted ? myPalette.highlight : "transparent"
                            opacity: counselor3DetailItem.down ? 1.0 : 0.25
                        }
                    }
                }

                sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                sectionCriteria: ViewSection.FullString
                sectionDelegate: personDetailSectionDelegate

                // When an item is selected, update the backend.
                onActivated: {
                    isLoading = true
                    save(currentValue, 3);
                    // trigger re-validation
                    isLoading = false
                }
                // Set the initial currentIndex to the value stored in the backend.
                Component.onCompleted: {
                    currentIndex = meeting && meeting.counselor3 ? indexOfValue(meeting.counselor3.id) : -1
                }
                popup.onAboutToHide: {
                    var id = meeting && meeting.counselor3 ? meeting.counselor3.id : -1;
                    counselor3DetailProxyModel.filterText = ""; // reset filter when closing the popup
                    currentIndex = indexOfValue(id); // restore currentIndex to selected value
                }
                onSearchTextChanged: counselor3DetailProxyModel.filterText = text
                onFilterChanged: {
                    settings.setValue("nonstudentAssignment_filter" + (index + 1), checked);
                    settings.sync();
                    reloadCounselor3DetailList();
                }
                onGroupByIndexChanged: {
                    if (groupByIndex >= 0) {
                        settings.nonstudentAssignment_groupByIndex = groupByIndex;
                        counselor3DetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                    }
                }
                onSortOrderChanged: {
                    settings.nonstudentAssignment_sortOrder = sortOrder;
                    counselor3DetailProxyModel.sort(0, sortOrder)
                }
                onCleared: {
                    isLoading = true
                    save(0, 3);
                    isLoading = false
                }
            }
            Label {
                id: counselor3ErrorLabel
                Layout.fillWidth: true
                Layout.column: 1
                Layout.row: 1
                verticalAlignment: Text.AlignTop
                font: TBStyle.bodySmallFont
                color: TBStyle.alertColor
                visible: text
                wrapMode: Text.Wrap
            }
        }
    }
}
