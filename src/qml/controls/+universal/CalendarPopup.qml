/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.calendar 1.0
import net.theocbase 1.0
import "./"

Popup {
    id: calendar
    y: parent.height

    property date selectedDate: new Date()
    signal activated(var date)

    implicitWidth: Math.max(contentWidth + leftPadding + rightPadding,
                            implicitBackgroundWidth + leftInset + rightInset)
    implicitHeight: Math.max(contentHeight + topPadding + bottomPadding,
                             implicitBackgroundHeight + topInset + bottomInset)

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    GridLayout {
        id: columnLayout
        anchors.fill: parent
        columns: 5
        rows: 3

        Label {
            id: label
            Layout.fillWidth: true
            Layout.row: 0
            Layout.column: 0
            Layout.columnSpan: 2
            font: TBStyle.bodyLargeFont
            text: (new Date()).getFullYear() === calendarGrid.year
                  ? selectedDate.toLocaleDateString(Qt.locale(), "MMMM")
                  : selectedDate.toLocaleDateString(Qt.locale(), "MMMM, yyyy")
        }

        ToolButton {
            Layout.row: 0
            Layout.column: 2
            icon.source: "qrc:/icons/chevron_left.svg"
            onClicked: {
                var newDate = new Date(selectedDate)
                newDate.setMonth(newDate.getMonth() -1)
                selectedDate = newDate
            }
        }

        ToolButton {
            Layout.row: 0
            Layout.column: 3
            icon.source: "qrc:/icons/calendar.svg"
            ToolTip.text: qsTr("Today")
            ToolTip.visible: hovered
            onClicked: {
                selectedDate = new Date()
            }
        }

        ToolButton {
            Layout.row: 0
            Layout.column: 4
            icon.source: "qrc:/icons/chevron_right.svg"
            onClicked: {
                var newDate = new Date(selectedDate)
                newDate.setMonth(newDate.getMonth() +1)
                selectedDate = newDate
            }
        }

        DayOfWeekRow {
            locale: calendarGrid.locale
            Layout.row: 1
            Layout.column: 1
            Layout.columnSpan: 4
            Layout.fillWidth: true

            spacing: 8
            delegate: Label {
                text: shortName
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                required property string shortName
            }
        }

        WeekNumberColumn {
            month: calendarGrid.month
            year: calendarGrid.year
            locale: calendarGrid.locale
            Layout.column: 0
            Layout.row: 2
            Layout.fillHeight: true

            spacing: 8
            delegate: Label {
                text: weekNumber
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                required property int weekNumber
            }
        }

        MonthGrid {
            id: calendarGrid
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.column: 1
            Layout.row: 2
            Layout.columnSpan: 4
            month: calendar.selectedDate.getMonth()
            year: calendar.selectedDate.getFullYear()
            spacing: 8
            delegate: ToolButton {
                contentItem: Label {
                    text: model.day
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    opacity: model.month === calendarGrid.month ? 1 : 0.5
                    color: (((model.date / 1) + 3) % 7) > 4 ? TBStyle.alertColor : myPalette.windowText
                }

                checkable: true
                checked: model.day === calendar.selectedDate.getDate() &&
                         model.month === calendar.selectedDate.getMonth() &&
                         model.year === calendar.selectedDate.getFullYear()
                onClicked: {
                    calendar.selectedDate = model.date;
                    activated(calendar.selectedDate);
                    calendar.close();
                }
            }
        }
    }
}
