/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the Qt Quick Controls 2 module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL3$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see http://www.qt.io/terms-conditions. For further
** information use the contact form at http://www.qt.io/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 3 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPLv3 included in the
** packaging of this file. Please review the following information to
** ensure the GNU Lesser General Public License version 3 requirements
** will be met: https://www.gnu.org/licenses/lgpl.html.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 2.0 or later as published by the Free
** Software Foundation and appearing in the file LICENSE.GPL included in
** the packaging of this file. Please review the following information to
** ensure the GNU General Public License version 2.0 requirements will be
** met: http://www.gnu.org/licenses/gpl-2.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.impl 2.12
import QtGraphicalEffects 1.15
import Qt.labs.calendar 1.0
import net.theocbase 1.0
import "./"

TextField {
    id: control

    property string error
    property date selectedDate

    leftPadding: acceptableInput ? 4 : 24

    ToolTip.visible: ToolTip.text !== "" && hovered
    ToolTip.text: error

    AlertIndicator {
        id: errorIndicator

        x: 2
        y: 3
        visible: !parent.acceptableInput

        states:
            [
            State {
                name: "Valid"; when: acceptableInput
                PropertyChanges { target: errorIndicator; scale: 1.2; }
            },
            State {
                name: "Invalid"; when: !acceptableInput
                PropertyChanges { target: errorIndicator; scale: 1; }
            }
        ]

        transitions: Transition {
            NumberAnimation {
                properties: "scale"
                from: 1.2;
                to: 1;
                duration: 100
                loops: 2
            }
        }
    }

    Rectangle {
        id: calendarButton
        anchors { right: parent.right; verticalCenter: control.verticalCenter; rightMargin: 2 }
        height: 16
        width: 16
        color: "transparent"

        Image {
            anchors.fill: parent
            anchors.margins: 0

            source: "qrc:/icons/calendar-month.svg"
            ColorOverlay {
                anchors.fill: parent
                source: parent
                color: myPalette.windowText
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                cal.selectedDate = isNaN(control.selectedDate.getTime()) ? new Date() : control.selectedDate;
                cal.open();
            }
        }
    }

    CalendarPopup {
        id: cal
        onActivated: {
            control.selectedDate = this.selectedDate
        }
    }
}
