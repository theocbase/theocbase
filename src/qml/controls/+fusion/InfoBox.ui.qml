import QtQuick 2.4
import QtQuick.Controls 2.12

Pane {
    enum BoxSize {
        Small,
        Normal
    }
    property int boxSize: InfoBox.BoxSize.Small
    property string backgroundColor: palette.highlight
    property string text: ""
    property string textColor: palette.highlightedText
    property int pointSize: Qt.application.font.pointSize

    implicitHeight: (boxSize === InfoBox.BoxSize.Normal ? 24 : 20) + topInset + bottomInset
    implicitWidth: Math.max(height, contentWidth + leftInset + rightInset)

    topInset: 8
    bottomInset: 8
    leftInset: 8
    rightInset: 8

    hoverEnabled: false

    background: Rectangle {
        opacity: enabled ? 1 : 0.3
        color: parent.backgroundColor
        radius: 2
    }

    contentItem: Text {
        anchors.fill: parent
        anchors.centerIn: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.pointSize: parent.pointSize
        text: parent.text
        color: parent.textColor
    }
}
