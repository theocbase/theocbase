import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.calendar 1.0
import QtGraphicalEffects 1.15
import net.theocbase 1.0
import "./"

ListView {
    id: control

    property bool showSearchField: true
    property bool showFilterControls: true
    property bool showGroupControls: false
    property int groupByIndex
    property bool sortOrder
    property bool showEditButton: true
    property bool isEditing: true
    property ListModel filterModel
    property ListModel groupByModel
    property Menu moreMenu

    signal searchTextChanged(var text)
    signal filterChanged(int index, bool checked)

    headerPositioning: ListView.OverlayHeader
    header: FocusScope {
        width: ListView.view.width; height: childrenRect.height
        x:childrenRect.x; y: childrenRect.y
        z: 3

        Pane {
            width: parent.width
            GridLayout {
                width: parent.width
                rows: control.showSearchField && control.showFilterControls && control.showGroupControls ? 2 : 1
                columns: 2

                TextField {
                    id: searchTextField
                    placeholderText: qsTr("Search")
                    visible: control.showSearchField
                    Layout.fillWidth: true
                    Layout.columnSpan: control.showSearchField && control.showFilterControls && control.showGroupControls ? 2 : 1

                    onVisibleChanged: text = "";
                    Keys.onEnterPressed: searchTextChanged(text);
                    Keys.onReturnPressed: searchTextChanged(text);

                    Image {
                        anchors { top: parent.top; right: parent.right; margins: 5 }
                        id: clearText
                        fillMode: Image.PreserveAspectFit
                        smooth: true
                        visible: searchTextField.text
                        source: "qrc:///icons/backspace.svg"

                        height: 16
                        width: 16

                        ColorOverlay {
                            anchors.fill: parent
                            source: parent
                            color: myPalette.windowText
                        }

                        MouseArea {
                            id: clear
                            anchors.fill: parent
                            onClicked: {
                                searchTextField.text = "";
                                searchTextField.forceActiveFocus();
                                searchTextChanged(searchTextField.text);
                            }
                        }
                    }
                }

                RowLayout {
                    visible: control.showFilterControls
                    Repeater {
                        model: control.filterModel

                        ToolButton {
                            visible: model.visible
                            ToolTip.visible: hovered
                            ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                            ToolTip.text: model.text
                            checkable: true
                            icon.source: model.icon
                            checked: model.checked // read initial value from the model
                            focusPolicy: Qt.NoFocus

                            onCheckedChanged: {
                                if (model.checked !== checked)
                                {
                                    filterModel.setProperty(index, "checked", checked) // update model
                                    filterChanged(index, checked)
                                }
                                checked = Qt.binding(function() { return model.checked }) // reestablish binding and continue auto-updating from the model
                            }
                        }
                    }
                }

                RowLayout {
                    visible: control.showGroupControls
                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                    Layout.fillWidth: true

                    ComboBox {
                        id: groupByComboBox
                        Layout.maximumWidth: 100
                        model: control.groupByModel
                        textRole: "key"
                        //: Arrange list by the selected criteria
                        //~ Context the '%1'-part will be replaced by terms, such as date, name, ...
                        displayText: qsTr("By %1", "Lookup control").arg(currentText)
                        currentIndex: control.groupByIndex
                        focusPolicy: Qt.NoFocus

                        onActivated: {
                            if (control.groupByIndex !== currentIndex)
                                control.groupByIndex = currentIndex; // TODO: avoid qt.qml.binding.removal
                            currentIndex = Qt.binding(function() { return control.groupByIndex }); // reestablish binding and continue auto-updating from the model
                        }
                    }

                    ToolButton {
                        id: sortOrderToolButton
                        checkable: true
                        icon.source: checked ? "qrc:///icons/expanded.svg" : "qrc:///icons/collapsed.svg"
                        checked: control.sortOrder
                        focusPolicy: Qt.NoFocus

                        onCheckedChanged: {
                            if (control.sortOrder !== checked)
                                control.sortOrder = checked; // TODO: avoid qt.qml.binding.removal
                            checked = Qt.binding(function() { return control.sortOrder }); // reestablish binding and continue auto-updating from the model
                        }
                    }
                }
            }
        }
    }

    delegate: MenuItem {
        width: ListView.view.width
        text: modelData
        highlighted: ListView.view.currentIndex === index

        onClicked: {
            ListView.view.currentIndex = index
        }
    }

    footerPositioning: ListView.OverlayHeader
    footer: Pane {
        width: ListView.view.width;
        z: 3

        RowLayout {
            anchors.right: parent.right
            Label {
                verticalAlignment: Text.AlignVCenter
                //: Number of rows in a list
                //~ Context the '%1'-part will be replaced by the actual number
                text: qsTr("Count=%1", "Lookup control").arg(control.count)
            }

            ToolButton {
                id: toggleEditButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                visible: control.showEditButton
                highlighted: true
                checkable: true
                icon.source: checked ? "qrc:///icons/done.svg" : "qrc:///icons/edit.svg"
                checked: control.isEditing
                Binding { target: control; property: "isEditing"; value: toggleEditButton.checked }
            }

            ToolButton {
                id: moreMenuButton
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                visible: moreMenu
                ToolTip.visible: hovered
                ToolTip.delay: Qt.styleHints.mousePressAndHoldInterval
                ToolTip.text: qsTr("Settings", "Lookup control")
                icon.source: "qrc:///icons/more.svg"

                onClicked: moreMenu.popup()
            }
        }
    }

    clip: true
}
