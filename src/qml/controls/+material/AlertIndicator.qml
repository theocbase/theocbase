import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.impl 2.15
import QtQuick.Controls.Material 2.15
import net.theocbase 1.0
import "./"

Rectangle {
    id: control

    height: 20
    width: 20
    color: "transparent"

    Text {
        anchors.centerIn: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "●"
        font.pixelSize: 18
        color: TBStyle.alertColor
    }
    Text {
        anchors.centerIn: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        text: "!"
        font.pixelSize: 14
        font.bold: true
        color: Material.background
    }
}
