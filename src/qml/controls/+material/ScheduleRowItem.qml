/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import "./"

Item {
    id: rowitem
    width: 400
    height: visible ? 40 : 0
    Layout.fillWidth: true
    Layout.preferredHeight: visible ? 40 : 0

    Material.theme: Material.System

    property bool isTimeVisible: timeText !== ""
    property string timeText: ""
    property string timeColor: "white"
    property string timeBackgroundColor: ""
    property int timePointSize: Qt.application.font.pointSize
    property string themeText
    property alias themeValidator: textTheme.validator
    property alias themeColor: textTheme.color
    property alias nameText1: textSpeaker
    property alias nameText2: textAssistant
    property bool clickable: false
    property bool editable: false
    property alias buttonIcon: editButton.icon.source
    property bool isRowHovered: listRowMouseArea.containsMouse || textAssistant.hovered || editButton.hovered
    property int buttonRowWidth: Math.max(Material.touchTarget, buttonsRow.implicitWidth)

    signal clicked

    Rectangle {
        anchors.fill: parent
        color: "grey"
        opacity: isRowHovered ? 0.1 : 0
        MouseArea {
            anchors.fill: parent
            id: listRowMouseArea
            hoverEnabled: true
            visible: editable
            onClicked: {
                if (mouseX >= (width - 40))
                    rowitem.clicked()
                mouse.accepted = false
            }
        }
    }

    GridLayout {
        id: gridLayout1
        rowSpacing: 0
        rows: 2
        columns: 4
        anchors.fill: parent

        Rectangle {
            id: item1
            width: 40 + 8
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.column: 0
            Layout.row: 0
            Layout.rowSpan: 2
            color: "transparent"
            InfoBox {
                visible: rowitem.isTimeVisible
                boxSize: InfoBox.BoxSize.Small
                backgroundColor: rowitem.timeBackgroundColor
                text: rowitem.timeText
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                textColor: rowitem.timeColor
                pointSize: rowitem.timePointSize
            }
        }

        ValidationTextField {
            id: textTheme
            text: themeTextMetrics.elidedText
            Layout.fillWidth: true
            Layout.column: 1
            Layout.row: 0
            Layout.rowSpan: 2
            Layout.preferredHeight: gridLayout1.height
            wrapMode: Text.WordWrap
            verticalAlignment: TextInput.AlignVCenter
            readOnly: true

            background: null
            padding: 0
            topPadding: 0
            bottomPadding: 0

            TextMetrics {
                id: themeTextMetrics
                font: textTheme.font
                text: themeText
                elide: Text.ElideRight
                elideWidth: textTheme.width
            }
        }

        ValidationTextField {
            id: textSpeaker
            text:  ""
            horizontalAlignment: TextInput.AlignRight
            verticalAlignment: TextInput.AlignVCenter
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.fillHeight: true
            Layout.column: 2
            Layout.row: 0
            Layout.rowSpan: textAssistant.visible ? 1 : 2
            visible: text !== ""
            readOnly: true

            background: null
            padding: 0
            topPadding: 0
            bottomPadding: 0
        }
        ValidationTextField {
            id: textAssistant
            text: ""
            horizontalAlignment: TextInput.AlignRight
            verticalAlignment: TextInput.AlignVCenter
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.fillHeight: true
            Layout.column: 2
            Layout.row: 1
            visible: text !== ""
            readOnly: true

            background: null
            padding: 0
            topPadding: 0
            bottomPadding: 0
        }

        Item {
            width: 45
            Layout.column: 3
            Layout.row: 0
            Layout.rowSpan: 2
        }
    }

    RowLayout {
        id: buttonsRow
        //anchors.verticalCenterOffset: -parent.height/2
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 0
        spacing: 0
        visible: editable
        Item { Layout.fillWidth: true }
        ToolButton {
            id: editButton
            property string tooltipText: ""
            icon.source: "qrc:/icons/edit.svg"
            Layout.alignment: Qt.AlignVCenter
            width: height
            visible: isRowHovered
            onClicked: rowitem.clicked()
        }
    }
}
