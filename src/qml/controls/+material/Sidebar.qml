import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.12
import net.theocbase 1.0

Item {
    id: sidebar

    Material.theme: Material.System

    property bool isVisible: false

    function loadPage(filename, args)
    {
        root.forceActiveFocus()
        if (filename === "")
            sidebarLoader.source = ""
        else
            sidebarLoader.setSource(filename, args)
    }

    Label {
        anchors.fill: parent
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.WordWrap
        text: qsTr("Select an assignment on the left to edit")
        visible: parent.width > 0 && sidebarLoader.source == ""
    }

    Loader {
        id: sidebarLoader
        anchors.fill: parent
        onSourceChanged: {
            if (source == "" && isVisible)
                isVisible = false
        }
        onLoaded: {
            console.log("sidebar content loaded")
        }
    }
}
