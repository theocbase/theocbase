import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Dialogs 1.3 as QQD
import net.theocbase 1.0
import "controls"

Page {
    id: page
    height: 300
    width: 400

    Profiles { id: profiles }

    ColumnLayout {
        anchors.fill: parent

        ToolButton {
            text: qsTr("Add profile")
            icon.source: "qrc:/icons/add.svg"
            Layout.alignment: Qt.AlignRight
            font.capitalization: Font.MixedCase
            onClicked: dialog.open()
        }

        ListView {
            id: profileList
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: profiles.model            
            delegate: ItemDelegate {
                text: modelData
                property bool activeRow: (modelData === "default" && profiles.currentProfile == "") || modelData === profiles.currentProfile
                icon.source: activeRow ? "qrc:/icons/default.svg" : "qrc:/icons/contact.svg"
                icon.color: deleteButton.icon.color
                width: page.width
                height: 60

                RowLayout {
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    Item {
                        Layout.fillWidth: true
                    }
                    ToolButton {
                        id: deleteButton
                        Layout.alignment: Qt.AlignVCenter
                        icon.source: "qrc:/icons/delete.svg"
                        visible: parent.parent.hovered && index > 0 && !activeRow
                        onClicked: {
                            if (index > 0) {
                                profileList.currentIndex = index
                                msgDelete.open()
                            }
                        }
                    }
                    ToolButton {
                        Layout.alignment: Qt.AlignVCenter
                        text: qsTr("Switch")
                        font.capitalization: Font.MixedCase
                        visible: parent.parent.hovered && !activeRow
                        onClicked: {
                            profileList.currentIndex = index
                            console.log(index)
                            console.log(modelData)
                            if (modelData === "default")
                                profiles.currentProfile = ""
                            else
                                profiles.currentProfile = modelData
                            msgRestart.open()
                        }
                    }
                }
            }
        }
    }

    Dialog {
        id: dialog
        //modal: true
        title: qsTr("Enter new profile name")
        standardButtons: Dialog.Ok | Dialog.Cancel

        x: (parent.width - width) / 2
        y: (parent.height - height) / 2

        TextField {
            id: textField
        }
        onAccepted: {
            profiles.createProfile(textField.text.toLowerCase())
        }
    }
    QQD.MessageDialog {
        id: msgRestart
        text: qsTr("TheocBase needs to be restarted. Click OK to continue.")
        icon: QQD.StandardIcon.Information
        modality: Qt.ApplicationModal
        onButtonClicked: { profiles.restart() }
    }
    QQD.MessageDialog {
        id: msgDelete
        text: qsTr("Deleting a profile will remove the current database and settings files. " +
              "This option cannot be undone. " +
              "Would you like to delete the profile data files?")
        standardButtons: QQD.StandardButton.Yes | QQD.StandardButton.No
        icon: QQD.StandardIcon.Question
        modality: Qt.ApplicationModal
        onAccepted: {
            var name = profileList.model[profileList.currentIndex]
            profiles.deleteProfile(name)
        }
    }
}
