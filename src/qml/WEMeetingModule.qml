/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import net.theocbase 1.0
import "controls"

Rectangle {
    radius: 5
    color: myPalette.base
    implicitHeight: childrenRect.height

    property bool editpossible: true
    property CPTMeeting _cptMeeting: pmController.meeting
    property string lastEditPage: ""
    property bool lastStartSong: false
    property bool setSong: false
    property bool shiftPressed: false
    property var lastUsedArgs
    property bool isLoading: false

    signal reloadSidebar(var name, var args, var setVisible)

    function loadSchedule(date) {
        // load meeting data
        isLoading = true
        pmController.date = date

        if (lastEditPage !== "") {
            if (typeof lastUsedArgs !== "undefined") {
                lastUsedArgs.meeting = _cptMeeting
                reloadSidebar(lastEditPage, lastUsedArgs, false)
            }
        }
        isLoading = false
    }

    function reload() {
        pmController.reload()
    }

    onReloadSidebar: {
        lastEditPage = name
        lastUsedArgs = args
    }

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }
    PublicMeetingController { id: pmController }
    WTImport { id: wtImport }    
    FileDialog {
        id: fileDialog
        nameFilters: [ "Epub files (*.epub)" /*, "JWpub files (*.jwpub)" */ ]
        folder: shortcuts.desktop
        onAccepted: {
            console.log("File selected: " + fileUrls[0])
            var msg = wtImport.importFile(fileUrls[0])
            msgbox.text = msg
            msgbox.open()
            reload()
        }
    }

    Connections {
        target: _cptMeeting
        function onMeetingChanged() {
            // trigger re-validation
            isLoading = true;
            isLoading = false;
        }
    }

    focus: true
    Keys.onPressed: { if (event.key === Qt.Key_Shift) shiftPressed = true }
    Keys.onReleased: { if (event.key === Qt.Key_Shift) shiftPressed = false }

    MessageDialog {
        id: msgbox
        icon: MessageDialog.Warning
    }

    // public talk
    RowLayout {
        width: parent.width
        ColumnLayout {
            Layout.margins: 10
            spacing: 0

            RowLayout {
                Label {
                    text: "○○● " + Qt.locale().dayName(wkDate.getDay(), Locale.LongFormat) + ", " +
                          wkDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat) + " | " + qsTr("Weekend Meeting").toUpperCase()
                    color: TBStyle.primaryTextColor
                    font: TBStyle.titleSmallFont
                    Layout.alignment: Qt.AlignVCenter
                    verticalAlignment: Qt.AlignVCenter
                    Layout.fillWidth: true
                    elide: Text.ElideRight
                }
                RowLayout {
                    Layout.minimumHeight: 40
                    Layout.minimumWidth: 40
                    Layout.alignment: Qt.AlignVCenter
                    ToolButton {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        icon.source: _cptMeeting && _cptMeeting.notes ? "qrc:/icons/notes-text.svg" : "qrc:/icons/notes.svg"
                        icon.color: TBStyle.primaryTextColor
                        ToolTip.text: qsTr("Notes", "Meeting Notes")
                        ToolTip.visible: hovered
                        visible: canViewMeetingNotes
                        onClicked: {
                            reloadSidebar("../MeetingNotes.qml", { "title" : qsTr("Weekend Meeting"), "meeting" : _cptMeeting, "editable" : canEditMeetingNotes }, true)
                        }
                    }
                }
            }

            ScheduleRowItem {
                themeText: qsTr("Chairman")
                themeColor: TBStyle.mediumColor
                timeText: ""
                timeColor: TBStyle.publicTalkTextColor
                timeBackgroundColor: "transparent"
                timePointSize: TBStyle.bodyLargeFont.pointSize
                nameText1.text: !isLoading && _cptMeeting && _cptMeeting.chairman
                                ? _cptMeeting.chairman.fullname
                                : ""
                nameText1.validator: WEMeetingValidator {
                    meeting: _cptMeeting
                    field: WEMeetingValidator.Chairman
                }
                editable: canEditWeekendMeetingSchedule && editpossible
                onClicked: {
                    reloadSidebar("../WEMeetingChairmanPanel.qml", { "specialEventRule" : specialEventRule, "meeting" : _cptMeeting }, true)
                }
            }

            // song
            ScheduleRowItem {
                themeText: _cptMeeting
                           ? (_cptMeeting.songTalk < 1
                              ? qsTr("Song & Prayer")
                              : qsTr("Song %1 & Prayer").arg(_cptMeeting.songTalk +
                                (showSongTitles ? ": " + _cptMeeting.songTalkTitle : "")))
                           : ""
                themeColor: TBStyle.mediumColor
                timeText: "\u266B"
                timeColor: TBStyle.publicTalkTextColor
                timeBackgroundColor: "transparent"
                timePointSize: TBStyle.bodyLargeFont.pointSize
                nameText1.text: !isLoading && _cptMeeting && _cptMeeting.openingPrayer
                                ? _cptMeeting.openingPrayer.fullname
                                : ""
                nameText1.validator: WEMeetingValidator {
                    meeting: _cptMeeting
                    field: WEMeetingValidator.OpeningPrayer
                }
                editable: canEditWeekendMeetingSchedule && editpossible
                onClicked: {
                    if (!canEditWeekendMeetingSchedule)
                        return
                    reloadSidebar("../WEMeetingSongAndPrayerPanel.qml", { "specialEventRule" : specialEventRule, "meeting" : _cptMeeting, "meetingSection" : MeetingSection.PublicTalk, "isWtStartSong" : false }, true)
                }
            }

            Rectangle {
                Layout.fillWidth: true
                implicitHeight: childrenRect.height
                color: TBStyle.publicTalkColor
                ColumnLayout {
                    width: parent.width
                    spacing: 0
                    RowLayout {
                        Rectangle {
                            height: 40
                            width: 40
                            color: TBStyle.publicTalkTextColor
                            Image {
                                source: "qrc:///icons/pt.svg"
                                anchors.fill: parent
                                anchors.margins: 5
                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: myPalette.window
                                }
                            }
                        }
                        Label {
                            text: qsTr("PUBLIC TALK")
                            Layout.leftMargin: 4
                            verticalAlignment: Text.AlignVCenter
                            font: TBStyle.titleLargeFont
                            color: TBStyle.publicTalkTextColor
                        }
                    }
                    ScheduleRowItem {
                        timeText: "30"
                        timeColor: myPalette.window
                        timeBackgroundColor: TBStyle.publicTalkTextColor
                        themeText: !isLoading && _cptMeeting && _cptMeeting.themeNumber > 0
                                   ? _cptMeeting.theme + " (" + _cptMeeting.themeNumber + ")"
                                   : ""
                        themeValidator: WEMeetingValidator {
                            meeting: _cptMeeting
                            field: WEMeetingValidator.Theme
                        }
                        themeColor: myPalette.windowText
                        nameText1.text: !isLoading && _cptMeeting && _cptMeeting.speaker
                                        ? _cptMeeting.speaker.fullname
                                        : ""
                        nameText1.validator: WEMeetingValidator {
                            meeting: _cptMeeting
                            field: WEMeetingValidator.Speaker
                        }
                        nameText2.text: _cptMeeting && _cptMeeting.speaker ? _cptMeeting.speaker.congregationName : ""
                        editable: canEditWeekendMeetingSchedule && editpossible && (specialEventRule ? !specialEventRule.isWithoutPublicTalk : false)
                        onClicked: reloadSidebar("../PublicTalkPanel.qml", { "meeting" : _cptMeeting, "controller" : pmController }, true)
                    }
                }
            }

            // watchtower study
            Rectangle {
                Layout.fillWidth: true
                implicitHeight: childrenRect.height
                color: TBStyle.watchtowerStudyColor
                ColumnLayout {
                    width: parent.width
                    spacing: 0
                    RowLayout {
                        Rectangle {
                            height: 40
                            width: 40
                            color: TBStyle.watchtowerStudyTextColor
                            Image {
                                anchors.fill: parent
                                source: "qrc:///icons/wt.svg"
                                anchors.margins: 5
                                ColorOverlay {
                                    anchors.fill: parent
                                    source: parent
                                    color: myPalette.window
                                }
                            }
                        }
                        Text {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.leftMargin: 4
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("WATCHTOWER STUDY")
                            font: TBStyle.titleLargeFont
                            color: TBStyle.watchtowerStudyTextColor
                        }
                    }
                    ScheduleRowItem {
                        themeText: qsTr("Song %1").arg(_cptMeeting
                                                       ? (_cptMeeting.songWtStart < 1
                                                          ? ""
                                                          : _cptMeeting.songWtStart +
                                                            (showSongTitles ? ": " + _cptMeeting.songWtStartTitle : ""))
                                                       : "")
                        themeColor: TBStyle.mediumColor
                        timeText: "\u266B"
                        timeColor: TBStyle.watchtowerStudyTextColor
                        timeBackgroundColor: "transparent"
                        timePointSize: TBStyle.bodyLargeFont.pointSize
                        editable: canEditWeekendMeetingSchedule && editpossible
                        onClicked: reloadSidebar("../WEMeetingSongAndPrayerPanel.qml", { "specialEventRule" : specialEventRule, "meeting" : _cptMeeting, "meetingSection" : MeetingSection.WatchtowerStudy, "isWtStartSong" : true }, true)
                    }

                    ScheduleRowItem {
                        timeText: _cptMeeting ? _cptMeeting.wtTime : "60"
                        timeColor: myPalette.window
                        timeBackgroundColor: TBStyle.watchtowerStudyTextColor
                        themeText: _cptMeeting ? _cptMeeting.wtTheme : ""
                        themeColor: myPalette.windowText
                        Label {
                            anchors.left: parent.left
                            anchors.leftMargin: 50
                            anchors.verticalCenter: parent.verticalCenter
                            text: "<html><a href='#'>" + qsTr("Import WT...") + "</a></html>"
                            visible: canEditWeekendMeetingSchedule && editpossible && parent.themeText === "" &&
                                     _cptMeeting && _cptMeeting.date > new Date("2019-03-03")
                            onLinkActivated: {
                                if (shiftPressed)
                                    wtImport.exportAssistFiles()
                                else
                                    fileDialog.open()
                                shiftPressed = false
                            }
                            DropArea {
                                anchors.fill: parent
                                onEntered: {
                                    if (!drag.hasUrls || !drag.urls[0].endsWith(".epub"))
                                        drag.accepted = false
                                }
                                onDropped: {
                                    console.log("Dropped " + drop.urls)
                                    if (drop.urls.length > 0) {
                                        var msg = wtImport.importFile(drop.urls[0])
                                        msgbox.text = msg
                                        msgbox.open()
                                        reload()
                                    }
                                }
                            }
                        }
                        nameText1.text: !isLoading && _cptMeeting && _cptMeeting.wtConductor
                                        ? _cptMeeting.wtConductor.fullname +  " (" + qsTr("Conductor") + ")"
                                        : ""
                        nameText1.validator: WEMeetingValidator {
                            meeting: _cptMeeting
                            field: WEMeetingValidator.WtConductor
                        }
                        nameText2.text: !isLoading && _cptMeeting && _cptMeeting.wtReader
                                        ? _cptMeeting.wtReader.fullname + " (" + qsTr("Reader") + ")"
                                        : ""
                        nameText2.validator: WEMeetingValidator {
                            meeting: _cptMeeting
                            field: WEMeetingValidator.WtReader
                        }
                        nameText2.color: TBStyle.mediumColor
                        editable: canEditWeekendMeetingSchedule && editpossible
                        onClicked: {
                            reloadSidebar("../WatchtowerStudyPanel.qml", { "specialEventRule" : specialEventRule, "meeting" : _cptMeeting }, true)
                        }
                    }

                }
            }

            // final talk
            ScheduleRowItem {
                id: finalTalkRow
                visible: _cptMeeting && _cptMeeting.finalTalk !== ""
                timeText: "30"
                timeBackgroundColor: myPalette.windowText
                themeText: _cptMeeting ? _cptMeeting.finalTalk : ""
                themeColor: myPalette.windowText
                nameText1.text: _cptMeeting ? _cptMeeting.finalTalkSpeakerName : ""
                editable: canEditWeekendMeetingSchedule && editpossible
                onClicked: {
                    if (!canEditWeekendMeetingSchedule)
                        return
                    reloadSidebar("../WEMeetingFinalTalkPanel.qml", { "meeting" : _cptMeeting }, true)
                    lastEditPage = "../WEMeetingChairmanPanel.qml"
                }
            }
            // song
            ScheduleRowItem {
                themeText: _cptMeeting
                           ? (_cptMeeting.songWtEnd < 1
                              ? qsTr("Song & Prayer")
                              : qsTr("Song %1 & Prayer").arg(_cptMeeting.songWtEnd + (showSongTitles ? ": " + _cptMeeting.songWtEndTitle : "")))
                           : ""
                themeColor: TBStyle.mediumColor
                timeText: "\u266B"
                timeColor: TBStyle.watchtowerStudyTextColor
                timeBackgroundColor: "transparent"
                timePointSize: TBStyle.bodyLargeFont.pointSize
                nameText1.text: !isLoading && _cptMeeting && _cptMeeting.finalPrayer
                                ? _cptMeeting.finalPrayer.fullname
                                : ""
                nameText1.validator: WEMeetingValidator {
                    meeting: _cptMeeting
                    field: WEMeetingValidator.FinalPrayer
                }
                editable: canEditWeekendMeetingSchedule && editpossible
                onClicked: {
                    if (!canEditWeekendMeetingSchedule)
                        return;
                    reloadSidebar("../WEMeetingSongAndPrayerPanel.qml", { "specialEventRule" : specialEventRule, "meeting" : _cptMeeting, "meetingSection" : MeetingSection.WatchtowerStudy, "isWtStartSong" : false }, true);
                }
            }
        }
    }
}
