import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import net.theocbase 1.0
import "controls"

Item {
    id: validationTextInput

    property bool isUntouched: false
    property bool isTextValid: true
    property bool isReadOnly: false
    property string placeholderText: qsTr("Text")
    property alias textInput: textInput
    property alias text: textInput.text
    property alias validator: textInput.validator
    property alias color: textInput.color
    property alias horizontalAlignment: textInput.horizontalAlignment

    signal selectedRowChanged(var row)

    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    Rectangle {
        id: invalidTextRect

        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHLeft | Qt.AlignVCenter

        border.width: 1
        border.color: TBStyle.alertColor
        opacity: 1
        color: "transparent"

        states: [
            State {
                name: "Untouched"; when: isUntouched
                PropertyChanges { target: invalidTextRect; opacity: 0 }
            },
            State {
                name: "ValidText"; when: !isUntouched && isTextValid
                PropertyChanges { target: invalidTextRect; opacity: 0 }
            },
            State {
                name: "InvalidText"; when: !isUntouched && !isTextValid
                PropertyChanges { target: invalidTextRect; opacity: 1 }
            }]

            transitions:[
                Transition {
                    from: "ValidText"
                    to: "InvalidText"
                    animations: invalidDataAnimation
                },
                Transition {
                    from: "Untouched"
                    to: "InvalidText"
                    animations: invalidDataAnimation
                }]
    }

    Label {
        id: placeholderTextBox
        anchors.fill: parent
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

        color: Qt.rgba(myPalette.text.r, myPalette.text.g, myPalette.text.b, 0.5)
        text: placeholderText
        elide: Text.ElideRight

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                if (!styleData.selected)
                    selectedRowChanged(styleData.row)
            }
        }
    }

    TextInput {
        id: textInput

        anchors.fill: parent
        layer.enabled: true
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHLeft | Qt.AlignVCenter
        verticalAlignment: TextInput.AlignVCenter
        horizontalAlignment: TextInput.AlignLeft
        anchors.leftMargin: 5

        readOnly: isReadOnly
        selectByMouse: true

        onTextChanged: {
            placeholderTextBox.text = text === "" ? placeholderText : "";
        }

        onActiveFocusChanged: {
            if (!activeFocus)
                parent.isUntouched = false;
            else
                if (!styleData.selected)
                    selectedRowChanged(styleData.row)
        }
    }
}
