/**
* This file is part of TheocBase.
*
* Copyright (C) 2011-2016, TheocBase Development Team, see AUTHORS.
*
* TheocBase is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* TheocBase is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQml.Models 2.12
import QtQuick.Layouts 1.12
import QtPositioning 5.12
import QtLocation 5.12
import net.theocbase 1.0
import "controls"

Item {
    id: item1
    property alias map: map
    property alias addressTextField: addressTextField
    property alias importBoundariesButton: importBoundariesButton
    property alias createBoundaryButton: createBoundaryButton
    property alias splitTerritoryButton: splitTerritoryButton
    property alias removeBoundaryButton: removeBoundaryButton
    property alias zoomFullButton: zoomFullButton
    property alias zoomSlider: zoomSlider
    property alias showBoundariesButton: showBoundariesButton
    property alias showMarkersButton: showMarkersButton
    property alias showStreetsButton: showStreetsButton
    property alias editModeButton: editModeButton
    property alias mapMouseArea: _mouseArea

    RowLayout {
        id: rowLayout
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.left: parent.left
        anchors.leftMargin: 12
        anchors.top: parent.top
        anchors.topMargin: 12
        Layout.fillHeight: false
        Layout.maximumHeight: 1
        Layout.preferredHeight: -1
        Layout.fillWidth: true
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        Layout.rowSpan: 1

        TextField {
            id: addressTextField
            width: 400
            Layout.maximumWidth: 500
            Layout.fillWidth: true
            Layout.columnSpan: 1
            placeholderText: qsTr("search", "Search address in territory map")
            selectByMouse: true
        }
    }

    RowLayout {
        id: rowLayout1
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.left: parent.left
        anchors.leftMargin: 12
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 12
        anchors.top: rowLayout.bottom
        anchors.topMargin: 12
        Layout.rowSpan: 2
        Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        Layout.fillHeight: true
        Layout.fillWidth: true

        Item {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.columnSpan: 1
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.rowSpan: 1

            Map {
                id: map
                anchors.fill: parent
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillHeight: true
                Layout.fillWidth: true

                zoomLevel: 15
                property int editMode: 0
                property bool isDigitizing: false
                property TerritoryMapPolygon polygon
                property TerritoryMapPolygon currentBoundary
                property TerritoryMapMarker marker
                property TerritoryMapMarker addressSearchMarker
                property alias mapMouseArea: _mouseArea
                property var markers: []
                property var addressSearchMarkers: []
                property var streets: []
                property var boundaries: []

                signal refreshTerritoryMapMarker(var showAllMarkers)
                signal refreshTerritoryMapStreets(var showAllStreets)

                MouseArea {
                    id: _mouseArea
                    hoverEnabled: true
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                }
            }

            Column {
                topPadding: 30
                bottomPadding: 30
                height: parent.height

                Slider {
                    id: zoomSlider
                    orientation: Qt.Vertical
                    height: parent.height - parent.topPadding - parent.bottomPadding
                    from: map.minimumZoomLevel
                    to: map.maximumZoomLevel
                    value: map.zoomLevel
                }
            }
        }

        ColumnLayout {
            id: columnLayout2
            spacing: 5
            Layout.fillHeight: true
            Layout.fillWidth: false
            Layout.rowSpan: 1
            Layout.columnSpan: 1
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

            ToolButton {
                id: importBoundariesButton
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Import data",
                                   "Import territory data from a file")
                icon.source: "qrc:///icons/import.svg"
                opacity: parent.enabled ? 1.0 : 0.5
                visible: canEditTerritories
            }

            ToolButton {
                id: zoomFullButton
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Zoom full",
                                   "Zoom full to display all territories")
                icon.source: "qrc:///icons/fullscreen.svg"
                opacity: parent.enabled ? 1.0 : 0.5
            }

            ToolButton {
                id: showBoundariesButton
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Show/hide territories",
                                   "Show/hide boundaries of territories")
                checkable: false
                icon.source: "qrc:///icons/borders-all.svg"
                opacity: parent.enabled ? 1.0 : 0.5
            }

            ToolButton {
                id: showMarkersButton
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Show/hide markers",
                                   "Show/hide address markers of territories")
                icon.source: "qrc:///icons/map-marker-multiple.svg"
                opacity: parent.enabled ? 1.0 : 0.5
                checkable: false
                visible: canViewTerritoryAddresses
            }

            ToolButton {
                id: showStreetsButton
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Show/hide streets",
                                   "Show/hide streets of territories")
                icon.source: "qrc:///icons/street_all.svg"
                opacity: parent.enabled ? 1.0 : 0.5
                checkable: false
                visible: canViewTerritoryAddresses
            }

            ToolButton {
                id: editModeButton
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Switch edit mode",
                                   "Switch edit mode on territory map")
                icon.source: "qrc:///icons/territory_view.svg"
                opacity: parent.enabled ? 1.0 : 0.5
                checkable: false
                visible: canEditTerritories
            }

            ToolButton {
                id: createBoundaryButton
                visible: map.editMode === 2
                enabled: isTerritorySelected && isGdalAvailable
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Create boundary",
                                   "Create a new boundary for the territory")
                icon.source: "qrc:///icons/boundary_add.svg"
                opacity: parent.enabled ? 1.0 : 0.5
            }

            ToolButton {
                id: removeBoundaryButton
                visible: canEditTerritories && map.editMode === 2
                enabled: isTerritorySelected
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Remove boundary",
                                   "Remove boundary or geometry of the territory")
                icon.source: "qrc:///icons/boundary_remove.svg"
                opacity: parent.enabled ? 1.0 : 0.5
            }

            ToolButton {
                id: splitTerritoryButton
                visible: map.editMode === 2
                enabled: isTerritorySelected && isGdalAvailable
                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                ToolTip.text: qsTr("Split territory",
                                   "Cut territory in two parts")
                icon.source: "qrc:///icons/boundary_split.svg"
                opacity: parent.enabled ? 1.0 : 0.5
            }
        }
    }
}
