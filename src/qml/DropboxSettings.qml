import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2
import QtGraphicalEffects 1.0
import net.theocbase 1.0
import "controls"

Pane {
    id: dialog

    LayoutMirroring.enabled: Qt.application.layoutDirection === Qt.RightToLeft
    LayoutMirroring.childrenInherit: true

    property bool canDeleteCloudData: cloud.canResetCloudData()
    property DBAccount account: cloud.authentication.getAccountInfo()
    property string syncfile: account.syncFile
    property bool logged: false
    property bool isDarkMode: dialog.palette.windowText.hsvValue > dialog.palette.window.hsvValue

    property int preferredHeight: childrenRect.height

    ListModel { id: defaultList }

    function loadList(showAll) {
        if (showAll) {
            listView.model = cloud.authentication.searchFile("syncfile.json")
        } else {
            listView.model = defaultList
            listView.model.clear()
            listView.model.append({"path": syncfile, "checked":true})
        }
        dialog.enabled = true
    }

    Component.onCompleted: {
        logged = cloud.logged
        if (logged)
            loadList(false)
    }

    Connections {
        target: cloud
        function onLoggedChanged(ok) {
            logged = ok
            if (ok) {
                labelName.text = account.name
                labelEmail.text = account.email
                loadList()
            } else {
                labelName.text = ""
                labelEmail.text = ""
                listView.model = null
            }
        }
        function onSyncStarted() { dialog.enabled = false }
        function onSyncFinished() { dialog.enabled = true }
        function onCloudResetStarted() { dialog.enabled = false }
        function onCloudResetFinished() { dialog.enabled = true }
        function onError() { dialog.enabled = true }
    }

    MessageDialog {
        id: msgbox
        text: qsTr("Are you sure you want to permanently delete your cloud data?")
        standardButtons: MessageDialog.Yes | MessageDialog.No
        icon: StandardIcon.Question
        modality: Qt.ApplicationModal
        onYes: cloud.resetCloudData()
    }

    ColumnLayout {
        id: columnLayout1

        Frame {
            id: frame1
            Layout.fillWidth: true

            ColumnLayout {
                id: columnLayout
                anchors.fill: parent

                RowLayout {
                    id: rowLayout
                    Layout.fillWidth: true

                    Rectangle {
                        Layout.fillWidth: true
                        height: 40
                        width: 200
                        color: "transparent"
                        Image {
                            id: image
                            anchors.fill:parent
                            //Layout.fillWidth: true
                            sourceSize: Qt.size(width, height)
                            fillMode: Image.PreserveAspectFit
                            source: "qrc:/icons/Dropbox.svg"
                            ColorOverlay {
                                anchors.fill: parent
                                visible: isDarkMode
                                source: parent
                                color: "white"
                            }
                        }
                    }

                    ToolButton {
                        id: buttonLogin
                        text: ""
                        onClicked: logged ? cloud.logout(2) : cloud.login()
                        icon.source: "qrc:/icons/logout.svg"
                    }
                }
                Label {
                    id: labelName
                    text: account.name
                    Layout.fillWidth: true
                }
                Label {
                    id: labelEmail
                    text: account.email
                    Layout.fillWidth: true
                }
            }
        }

        Frame {
            id: frame2
            Layout.fillWidth: true
            enabled: logged

            RowLayout {
                id: rowLayout1
                anchors.fill: parent
                ListView {
                    id: listView
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    implicitHeight: childrenRect.height
                    implicitWidth: contentItem.childrenRect.width

                    model: ListModel {}
                    delegate: RowLayout {
                        CheckBox {
                            id: listCheckBox
                            enabled: buttonEditSyncFile.listLoaded
                            checked: account.syncFile === model.path
                            text: model.path+
                                  (typeof (model.sharedby) == "undefined" || model.sharedby === "" ?
                                       "" : "\n("+ model.sharedby + ")")

//                            ToolTip.visible: hovered
//                            ToolTip.text: model.path

                            onCheckedChanged: {
                                if (checked)
                                    account.syncFile = model.path
                            }
                        }
                        MouseArea {
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            hoverEnabled: true
                            id: mousearea
                        }
                        ToolTip {
                            text: model.path
                            visible: mousearea.containsMouse
                        }
                    }
                }
                ToolButton {
                    id: buttonEditSyncFile
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    icon.source: listLoaded ? "qrc:/icons/chevron_up.svg" : "qrc:/icons/chevron_down.svg"
                    property bool listLoaded: false
                    onClicked: {
                        dialog.enabled = false
                        loadList(!listLoaded)
                        listLoaded = !listLoaded
                    }
                }
            }
        }

        Frame {
            id: frame3
            Layout.fillWidth: true
            enabled: logged

            GridLayout {
                id: columnLayout2
                columns: 2
                anchors.fill: parent
                Label {
                    id: label
                    text: qsTr("Last synchronized: %1").arg(cloud.lastSyncTime.toLocaleString(Qt.locale(), Locale.ShortFormat))
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                }
                Label {
                    text: qsTr("Synchronize")
                    Layout.fillWidth: true
                }
                ToolButton {
                    id: buttonSync
                    icon.source: "qrc:/icons/cloud_sync.svg"
                    onClicked: {
                        cloud.synchronize(false)
                        console.log("sync ready")
                    }
                }

                Label {
                    text: qsTr("Delete Cloud Data")
                    Layout.fillWidth: true
                    visible: canDeleteCloudData
                }
                ToolButton {
                    id: buttonReset
                    icon.source: "qrc:/icons/cloud_delete.svg"
                    visible: canDeleteCloudData
                    onClicked: msgbox.open()
                }
            }
        }
    }
    states: [
        State {
            name: "logout"
            when: !logged

            PropertyChanges {
                target: buttonLogin
                icon.source: "qrc:/icons/login.svg"
            }

            PropertyChanges {
                target: labelEmail
                visible: false
            }

            PropertyChanges {
                target: labelName
                visible: false
            }

            PropertyChanges {
                target: frame2
                visible: false
            }

            PropertyChanges {
                target: frame3
                visible: false
            }
        }
    ]
}
