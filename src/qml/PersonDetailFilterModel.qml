import QtQuick 2.10
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.12
import net.theocbase 1.0
import "controls"

ListModel {
    id: personDetailFilterModel
    ListElement {
        text: qsTr("Nonstudent meeting parts", "Category of assignments for filtering")
        icon: "qrc:///icons/servant.svg"
        visible: true
        checked: false
    }
    ListElement {
        text: qsTr("Other assignments", "Category of assignments for filtering")
        icon: "qrc:///icons/servant2.svg"
        visible: true
        checked: false
    }
    ListElement {
        text: qsTr("Student parts", "Category of assignments for filtering")
        icon: "qrc:///icons/assigned_student.svg"
        visible: true
        checked: false
    }
    ListElement {
        text: qsTr("Assistant in student parts", "Category of assignments for filtering")
        icon: "qrc:///icons/assigned_assistant.svg"
        visible: true
        checked: false
    }
    ListElement {
        text: qsTr("Similar assignments only")
        icon: "qrc:///icons/equal-circle.svg"
        visible: true
        checked: false
    }
}
