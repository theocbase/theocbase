import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Component {
    id: personDetailSectionDelegate

    Pane {
        id: personDetailSection
        width: ListView.view.width
        background: Rectangle {
            anchors.fill: parent
            color: TBStyle.primaryColor
        }
        SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }
        Label {
            width: parent.width
            anchors.centerIn: parent
            text: switch(personDetailSection.ListView.view.section.property) {
                  case "assignmentFrequencyRange": return SPScripts.formatRating(section)
                  case "timeRange": return SPScripts.formatTimeRange(section)
                  case "theme": return SPScripts.formatTheme(section)
                  case "weeksIdle": return SPScripts.formatWeeksIdle(section)
                  default: return section;
                  }
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            font: TBStyle.bodyMediumFont
            color: TBStyle.onPrimaryColor
            elide: personDetailSection.ListView.view.section.property === "theme" ? Text.ElideCenter : Text.ElideNone
        }
    }
}
