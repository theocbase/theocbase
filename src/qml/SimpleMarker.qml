import QtQuick 2.5
import QtLocation 5.6
import QtGraphicalEffects 1.0
import QtPositioning 5.8
import QtQuick.Controls 1.4
import net.theocbase 1.0

MapQuickItem {
    id: simpleMarker

    anchorPoint: markerType < 2 ? Qt.point(sourceItem.width * 0.5, sourceItem.height) : Qt.point(sourceItem.width * 0.5, sourceItem.height * 0.5)

    property bool isMarkerSelected: false
    property bool isMarkerDragTarget: false
    property bool isClickable: true
    property alias canvas: _canvas
    property alias color: _canvas.color
    property alias markerScale: _canvas.markerScale
    property alias markerMouseArea: _canvas.mouseArea
    property int markerType: 0

    sourceItem: Canvas {
        id: _canvas

        property string color: "#ffffff";
        property string borderColor: isMarkerSelected ? "#ff0000" : "#000000";
        property int borderWidth: isMarkerSelected ? 2 : 1;
        property double markerScale: 1.0
        property alias mouseArea: _markerMouseArea

        width: 24 * markerScale;
        height: 24 * markerScale;
        onPaint: {
            var ctx = _canvas.getContext('2d');
            ctx.reset();
            ctx.fillStyle = color;
            ctx.strokeStyle = borderColor;
            switch (markerType)
            {
            case 1:
                ctx.beginPath();
                ctx.moveTo(12 * markerScale, 3 * markerScale);
                ctx.lineTo(2 * markerScale, 12 * markerScale);
                ctx.lineTo(5 * markerScale, 12 * markerScale);
                ctx.lineTo(5 * markerScale, 20 * markerScale);
                ctx.lineTo(19 * markerScale, 20 * markerScale);
                ctx.lineTo(19 * markerScale, 12 * markerScale);
                ctx.lineTo(22 * markerScale, 12 * markerScale);
                ctx.lineTo(12 * markerScale, 3 * markerScale);
                ctx.moveTo(12 * markerScale, 7.7 * markerScale);
                ctx.bezierCurveTo(14.1 * markerScale, 7.7 * markerScale, 15.8 * markerScale, 9.4 * markerScale, 15.8 * markerScale, 11.5 * markerScale);
                ctx.bezierCurveTo(15.8 * markerScale, 14.5 * markerScale, 12 * markerScale, 18 * markerScale, 12 * markerScale, 18 * markerScale);
                ctx.bezierCurveTo(12 * markerScale, 18 * markerScale, 8.2 * markerScale, 14.5 * markerScale, 8.2 * markerScale, 11.5 * markerScale);
                ctx.bezierCurveTo(8.2 * markerScale, 9.4 * markerScale, 9.9 * markerScale, 7.7 * markerScale, 12 * markerScale, 7.7 * markerScale);
                ctx.moveTo(12 * markerScale, 10 * markerScale);
                ctx.translate(12 * markerScale, 11.5 * markerScale);
                ctx.arc(0, 0, 1.5  * markerScale, -Math.PI / 2, -Math.PI, 1 * markerScale);
                ctx.arc(0 ,0 ,1.5 * markerScale, Math.PI, Math.PI / 2, 1 * markerScale);
                ctx.arc(0, 0, 1.5 * markerScale, Math.PI / 2, 0, 1 * markerScale);
                ctx.arc(0, 0, 1.5 * markerScale, 0, -Math.PI / 2, 1 * markerScale);
                ctx.translate(-12 * markerScale, -11.5 * markerScale);
                ctx.closePath();
                ctx.fill();
                break;
            case 2:
                ctx.fillStyle = 'red';
                ctx.globalAlpha = 0.5;
                ctx.beginPath();
                ctx.translate(12 * markerScale, 12 * markerScale);
                ctx.arc(0, 0, 6 * markerScale, 0, 2 * Math.PI, 0);
                ctx.translate(-12 * markerScale, -12 * markerScale);
                ctx.closePath();
                ctx.fill();
                break;
            case 3:
                ctx.fillStyle = 'red';
                ctx.beginPath();
                ctx.moveTo(0, 6 * markerScale);
                ctx.lineTo(0, 0);
                ctx.lineTo(6 * markerScale, 0);
                ctx.moveTo(18 * markerScale, 0);
                ctx.lineTo(24 * markerScale, 0);
                ctx.lineTo(24 * markerScale, 6 * markerScale);
                ctx.moveTo(24 * markerScale, 18 * markerScale);
                ctx.lineTo(24 * markerScale, 24 * markerScale);
                ctx.lineTo(18 * markerScale, 24 * markerScale);
                ctx.moveTo(6 * markerScale, 24 * markerScale);
                ctx.lineTo(0, 24 * markerScale);
                ctx.lineTo(0, 18 * markerScale);
                ctx.closePath();
                break;
            default:
                ctx.beginPath();
                ctx.moveTo(12 * markerScale, 11.5 * markerScale);
                ctx.translate(12 * markerScale, 9 * markerScale);
                ctx.arc(0, 0, 2.5 * markerScale, Math.PI / 2, Math.PI, 0);
                ctx.arc(0, 0, 2.5 * markerScale, Math.PI, Math.PI / 2 * 3, 0);
                ctx.arc(0, 0, 2.5 * markerScale, -Math.PI / 2, 0, 0);
                ctx.arc(0, 0, 2.5 * markerScale, 0, Math.PI / 2, 0);
                ctx.translate(-12 * markerScale, -9 * markerScale);
                ctx.moveTo(12 * markerScale, 2 * markerScale);
                ctx.translate(12 * markerScale, 9 * markerScale);
                ctx.arc(0, 0, 7 * markerScale, -Math.PI / 2, -Math.PI, 1 * markerScale);
                ctx.translate(-12 * markerScale, -9 * markerScale);
                ctx.bezierCurveTo(5 * markerScale, 14.25 * markerScale, 12 * markerScale, 22 * markerScale, 12 * markerScale, 22 * markerScale);
                ctx.bezierCurveTo(12 * markerScale, 22 * markerScale, 19 * markerScale, 14.25 * markerScale, 19 * markerScale, 9 * markerScale);
                ctx.translate(12 * markerScale, 9 * markerScale);
                ctx.arc(0, 0, 7 * markerScale, 0, -Math.PI / 2, 1 * markerScale);
                ctx.translate(-12 * markerScale, -9 * markerScale);
                ctx.closePath();
                ctx.fill();
                break;
            }

            ctx.fill();
            ctx.lineWidth = borderWidth;
            ctx.stroke();
            ctx.restore();
        }

        MouseArea {
            id: _markerMouseArea
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            anchors.fill: parent
            drag.target: isMarkerDragTarget ? simpleMarker : null
            preventStealing: true
            propagateComposedEvents: !isClickable
        }
    }

    onIsMarkerSelectedChanged: {
        _canvas.requestPaint()
    }
}
