/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    title: currentAssignment ? currentAssignment.theme : qsTr("Assignment")
    header: TextArea {
        id: textTheme
        background: null
        text: currentAssignment ? currentAssignment.theme : ""
        placeholderText: qsTr("Theme")
        wrapMode: Text.WordWrap
        font: TBStyle.titleSmallFont
        padding: 10
        leftPadding: 10
        rightPadding: 10
        topPadding: 10
        bottomPadding: 10
        selectByMouse: true
        readOnly: !editableTheme
        onEditingFinished: {
            // save theme if CO service talk or living talk
            if (editableTheme && currentAssignment.theme != text) {
                currentAssignment.theme = text
                currentAssignment.save()
            }
        }
    }

    property string returnValue: ""
    property LMM_Assignment currentAssignment
    property bool editableTheme: currentAssignment &&
                                 (currentAssignment.meetingPart === MeetingPart.Service_Talk ||
                                  currentAssignment.meetingPart === MeetingPart.LMM_LivingTalk1 ||
                                  currentAssignment.meetingPart === MeetingPart.LMM_LivingTalk2 ||
                                  currentAssignment.meetingPart === MeetingPart.LMM_LivingTalk3)
    property bool isLoading: true

    // javascript functions
    function reloadAssigneeDetailList() {
        assigneeDetailModel.loadPersonDetails(0, 0,
                                              currentAssignment.speaker ? currentAssignment.speaker.id : 0, // include current assignment
                                              MeetingType.MidweekMeeting,
                                              currentAssignment.classnumber, currentAssignment.meetingPart, false, 0,
                                              lookupControlMoreMenu.includePartsOfOtherMeeting,
                                              true, // show midweek parts
                                              personDetailFilterModel.get(0).checked, // nonstudent parts
                                              personDetailFilterModel.get(1).checked, // other assignments
                                              personDetailFilterModel.get(2).checked, // student parts
                                              personDetailFilterModel.get(3).checked, // assistant in student parts
                                              personDetailFilterModel.get(4).checked, // similar assignments only
                                              currentAssignment.date);
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
    }

    function resetDefaultLookupControlSettings() {
        personDetailFilterModel.setProperty(0, "checked", true);
        personDetailFilterModel.setProperty(1, "checked", false);
        personDetailFilterModel.setProperty(2, "checked", false);
        personDetailFilterModel.setProperty(3, "checked", false);
        personDetailFilterModel.setProperty(4, "checked", true);
        settings.nonstudentAssignment_filter1 = personDetailFilterModel.get(0).checked;
        settings.nonstudentAssignment_filter2 = personDetailFilterModel.get(1).checked;
        settings.nonstudentAssignment_filter3 = personDetailFilterModel.get(2).checked;
        settings.nonstudentAssignment_filter4 = personDetailFilterModel.get(3).checked;
        settings.nonstudentAssignment_filter5 = personDetailFilterModel.get(4).checked;
        settings.nonstudentAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        settings.nonstudentAssignment_includeWeekendParts = false;
        settings.nonstudentAssignment_detailRowCount = 3;
        settings.nonstudentAssignment_labelingMode = 1;
        settings.nonstudentAssignment_hideUnavailables = true;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeWeekendParts;
        lookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.nonstudentAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }

    // workaround to use the right color on Windows with dark theme
    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    Settings {
        id: settings
        category: "LookupControl"
        property bool nonstudentAssignment_filter1: true
        property bool nonstudentAssignment_filter2: false
        property bool nonstudentAssignment_filter3: false
        property bool nonstudentAssignment_filter4: false
        property bool nonstudentAssignment_filter5: true
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property bool nonstudentAssignment_includeWeekendParts: false
        property int nonstudentAssignment_detailRowCount: 3
        property int nonstudentAssignment_labelingMode: 1
        property bool nonstudentAssignment_hideUnavailables: true
    }

    AssignmentController { id: myController }
    ShareUtils { id: shareUtils }

    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: lookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: personDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    LMMAssignmentValidator {
        id: assigneeValidator
        assignment: currentAssignment
        isAssistant: false
        onErrorChanged: assigneeErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: lookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.nonstudentAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAssigneeDetailList();
        }
        onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.nonstudentAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultLookupControlSettings()
    }

    Component.onCompleted: {
        personDetailFilterModel.setProperty(0, "checked", settings.nonstudentAssignment_filter1);
        personDetailFilterModel.setProperty(1, "checked", settings.nonstudentAssignment_filter2);
        personDetailFilterModel.setProperty(2, "checked", settings.nonstudentAssignment_filter3);
        personDetailFilterModel.setProperty(3, "checked", settings.nonstudentAssignment_filter4);
        personDetailFilterModel.setProperty(4, "checked", settings.nonstudentAssignment_filter5);
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        lookupControlMoreMenu.includePartsOfOtherMeeting = settings.nonstudentAssignment_includeWeekendParts;
        lookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        lookupControlMoreMenu.labelingMode = settings.nonstudentAssignment_labelingMode;
        lookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        assigneeLookupControl.filterModel = personDetailFilterModel;
        reloadAssigneeDetailList();
    }

    Component.onDestruction: {
        settings.nonstudentAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.nonstudentAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.nonstudentAssignment_includeWeekendParts = lookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.nonstudentAssignment_detailRowCount = lookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_labelingMode = lookupControlMoreMenu.labelingMode;
        settings.nonstudentAssignment_hideUnavailables = lookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onCurrentAssignmentChanged: {
        isLoading = true;
        if (!currentAssignment)
        {
            assigneeDetailModel.clear();
            return;
        }
        isLoading = false;
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10
        ColumnLayout {
            anchors.fill: parent

            RowLayout {
                visible: currentAssignment ? currentAssignment.source : ""
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/wt_source.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Source")
                    ToolTip.visible: hovered
                }
                TextArea {
                    id: textareaSource
                    Layout.fillWidth: true
                    background: null
                    text: currentAssignment ? currentAssignment.source : ""
                    readOnly: !editableTheme
                    wrapMode: Text.WordWrap
                    topPadding: 0
                    bottomPadding: 0
                    selectByMouse: true
                    onEditingFinished: {
                        if (editableTheme && currentAssignment.source != text) {
                            currentAssignment.source = text
                            currentAssignment.save()
                        }
                    }
                }
            }

            GridLayout {
                Layout.fillWidth: true
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: currentAssignment && currentAssignment.meetingPart === MeetingPart.LMM_Chairman ? qsTr("Chairman") : qsTr("Speaker")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assigneeLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0
                    enabled: currentAssignment &&
                             currentAssignment.meetingPart !== MeetingPart.Service_Talk &&
                             currentAssignment.meetingPart !== MeetingPart.LMM_SampleConversationVideo

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assigneeDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: lookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (currentAssignment && currentAssignment.speaker
                                    ? currentAssignment.speaker.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assigneeValidator

                    delegate: PersonDetailDelegate {
                        id: assigneeDetailItem

                        meetingDate: currentAssignment.date
                        detailRowCount: lookupControlMoreMenu.detailRowCount
                        labelingMode: lookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: lookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assigneeDetailItem.width - 2
                            height: assigneeDetailItem.height - 2
                            color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assigneeDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true
                        currentAssignment.speaker = currentValue < 1 ? null : myController.getPublisherById(currentValue)
                        currentAssignment.save();
                        // trigger re-validation
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = currentAssignment && currentAssignment.meetingPart === MeetingPart.Service_Talk
                                ? indexOfValue(0)
                                : (currentAssignment && currentAssignment.speaker
                                   ? indexOfValue(currentAssignment.speaker.id)
                                   : -1)
                    }
                    popup.onAboutToHide: {
                        var id = currentAssignment && currentAssignment.meetingPart === MeetingPart.Service_Talk
                                ? 0
                                : (currentAssignment && currentAssignment.speaker
                                   ? currentAssignment.speaker.id
                                   : -1);
                        assigneeDetailProxyModel.filterText = "" // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("nonstudentAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadAssigneeDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.nonstudentAssignment_groupByIndex = groupByIndex;
                            assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.nonstudentAssignment_sortOrder = sortOrder;
                        assigneeDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        currentAssignment.speaker = null
                        currentAssignment.save()
                        // trigger revalidation
                        isLoading = false
                    }
                }
                Label {
                    id: assigneeErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/notes.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Note")
                    ToolTip.visible: hovered
                }
                TextArea {
                    id: texteditNote
                    Layout.fillWidth: true
                    text: currentAssignment ? currentAssignment.note : ""
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    onEditingFinished: {
                        if (currentAssignment.note !== text) {
                            currentAssignment.note = text
                            currentAssignment.save()
                        }
                    }
                }
            }
        }
    }

    footer: ColumnLayout {
        RowLayout {
            Layout.alignment: Qt.AlignRight
            Layout.margins: 10
            ToolButton {
                id: copyButton
                icon.source: "qrc:/icons/copy.svg"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    shareUtils.copyToClipboard(currentAssignment.getReminderText())
                }
            }

            ToolButton {
                id: shareButton
                icon.source: "qrc:/icons/share.svg"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    var pos = mapToGlobal(0, height)
                    shareUtils.share(currentAssignment.getReminderText(), "", pos)
                }
            }
        }
    }
}
