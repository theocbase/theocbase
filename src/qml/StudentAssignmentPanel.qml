/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQuick.Window 2.15
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.15
import Qt.labs.qmlmodels 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    id: studentAssignmentDialog

    title: currentAssignment ? currentAssignment.theme : qsTr("Student Assignment")
    header: Label {
       text: title
       font: TBStyle.titleSmallFont
       padding: 10
    }

    property string returnValue: ""
    property LMM_Assignment currentAssignment
    property bool isLoading: true

    // javascript functions
    function reloadAssigneeDetailList() {
        assigneeDetailModel.loadPersonDetails(0, 0,
                                              currentAssignment.speaker ? currentAssignment.speaker.id : 0, // include current assignment
                                              MeetingType.MidweekMeeting,
                                              currentAssignment.classnumber, currentAssignment.meetingPart, false, 0,
                                              assigneeLookupControlMoreMenu.includePartsOfOtherMeeting,
                                              true, // show midweek parts
                                              assigneeDetailFilterModel.get(0).checked, // nonstudent parts
                                              assigneeDetailFilterModel.get(1).checked, // other assignments
                                              assigneeDetailFilterModel.get(2).checked, // student parts
                                              assigneeDetailFilterModel.get(3).checked, // assistant in student parts
                                              assigneeDetailFilterModel.get(4).checked, // similar assignments only
                                              currentAssignment.date);
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
    }

    function reloadAssistantDetailList() {
        if (currentAssignment && currentAssignment.canHaveAssistant) {
            assistantDetailFilterModel.setProperty(4, "visible", currentAssignment && currentAssignment.speaker ? true : false);
            assistantDetailModel.loadPersonDetails(0, 0,
                                                   currentAssignment.assistant ? currentAssignment.assistant.id : 0, // include current assignment
                                                   MeetingType.MidweekMeeting,
                                                   currentAssignment.classnumber, currentAssignment.meetingPart, true,
                                                   currentAssignment.volunteer
                                                   ? currentAssignment.volunteer.id
                                                   : (currentAssignment.speaker ? currentAssignment.speaker.id : 0), // to compare gender and family
                                                   assistantLookupControlMoreMenu.includePartsOfOtherMeeting,
                                                   true, // show midweek parts
                                                   assistantDetailFilterModel.get(0).checked, // nonstudent parts
                                                   assistantDetailFilterModel.get(1).checked, // other assignments
                                                   assistantDetailFilterModel.get(2).checked, // student parts
                                                   assistantDetailFilterModel.get(3).checked, // assistant in student parts
                                                   currentAssignment.speaker ? assistantDetailFilterModel.get(4).checked : false, // similar assignments only: with same student
                                                   currentAssignment.date);
            assistantDetailProxyModel.sort(0, assistantLookupControl.sortOrder);
            assistantLookupControl.currentIndex = currentAssignment && currentAssignment.assistant
                    ? assistantLookupControl.indexOfValue(currentAssignment.assistant.id)
                    : -1;
        } else {
            assistantDetailModel.clear();
        }
    }

    function reloadVolunteerDetailList() {
        volunteerDetailModel.loadPersonDetails(0, 0,
                                               currentAssignment.volunteer ? currentAssignment.volunteer.id : 0, // include current assignment
                                               MeetingType.MidweekMeeting,
                                               currentAssignment.classnumber, currentAssignment.meetingPart, false, 0,
                                               assigneeLookupControlMoreMenu.includePartsOfOtherMeeting,
                                               true, // show midweek parts
                                               assigneeDetailFilterModel.get(0).checked, // nonstudent parts
                                               assigneeDetailFilterModel.get(1).checked, // other assignments
                                               assigneeDetailFilterModel.get(2).checked, // student parts
                                               assigneeDetailFilterModel.get(3).checked, // assistant in student parts
                                               assigneeDetailFilterModel.get(4).checked, // similar assignments only
                                               currentAssignment.date);
        volunteerDetailProxyModel.sort(0, volunteerLookupControl.sortOrder);
    }

    function resetDefaultAssigneeLookupControlSettings() {
        assigneeDetailFilterModel.setProperty(0, "checked", false);
        assigneeDetailFilterModel.setProperty(1, "checked", false);
        assigneeDetailFilterModel.setProperty(2, "checked", true);
        assigneeDetailFilterModel.setProperty(3, "checked", false);
        assigneeDetailFilterModel.setProperty(4, "checked", true);
        settings.studentAssignment_filter1 = assigneeDetailFilterModel.get(0).checked;
        settings.studentAssignment_filter2 = assigneeDetailFilterModel.get(1).checked;
        settings.studentAssignment_filter3 = assigneeDetailFilterModel.get(2).checked;
        settings.studentAssignment_filter4 = assigneeDetailFilterModel.get(3).checked;
        settings.studentAssignment_filter5 = assigneeDetailFilterModel.get(4).checked;
        settings.studentAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.studentAssignment_groupByIndex;
        settings.studentAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.studentAssignment_sortOrder;
        settings.studentAssignment_includeWeekendParts = false;
        settings.studentAssignment_detailRowCount = 3;
        settings.studentAssignment_labelingMode = 1;
        settings.studentAssignment_hideUnavailables = true;
        assigneeLookupControlMoreMenu.includePartsOfOtherMeeting = settings.studentAssignment_includeWeekendParts;
        assigneeLookupControlMoreMenu.detailRowCount = settings.studentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.labelingMode = settings.studentAssignment_labelingMode;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.studentAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }

    function resetDefaultAssistantLookupControlSettings() {
        assistantDetailFilterModel.setProperty(0, "checked", false);
        assistantDetailFilterModel.setProperty(1, "checked", false);
        assistantDetailFilterModel.setProperty(2, "checked", false);
        assistantDetailFilterModel.setProperty(3, "checked", true);
        assistantDetailFilterModel.setProperty(4, "checked", true);
        settings.assistantAssignment_filter1 = assistantDetailFilterModel.get(0).checked;
        settings.assistantAssignment_filter2 = assistantDetailFilterModel.get(1).checked;
        settings.assistantAssignment_filter3 = assistantDetailFilterModel.get(2).checked;
        settings.assistantAssignment_filter4 = assistantDetailFilterModel.get(3).checked;
        settings.assistantAssignment_filter5 = assistantDetailFilterModel.get(4).checked;
        settings.assistantAssignment_groupByIndex = 1;
        assistantLookupControl.groupByIndex = settings.assistantAssignment_groupByIndex;
        settings.assistantAssignment_sortOrder = Qt.AscendingOrder;
        assistantLookupControl.sortOrder = settings.assistantAssignment_sortOrder;
        settings.assistantAssignment_includeWeekendParts = false;
        settings.assistantAssignment_detailRowCount = 3;
        settings.assistantAssignment_labelingMode = 1;
        settings.assistantAssignment_hideUnavailables = true;
        assistantLookupControlMoreMenu.includePartsOfOtherMeeting = settings.assistantAssignment_includeWeekendParts;
        assistantLookupControlMoreMenu.detailRowCount = settings.assistantAssignment_detailRowCount;
        assistantLookupControlMoreMenu.labelingMode = settings.assistantAssignment_labelingMode;
        assistantLookupControlMoreMenu.hideUnavailables = settings.assistantAssignment_hideUnavailables;
        settings.sync();
        reloadAssistantDetailList();
    }

    // workaround to use the right color on Windows with dark theme
    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    Settings {
        id: settings
        category: "LookupControl"
        property bool studentAssignment_filter1: false
        property bool studentAssignment_filter2: false
        property bool studentAssignment_filter3: true
        property bool studentAssignment_filter4: false
        property bool studentAssignment_filter5: true
        property int studentAssignment_groupByIndex: 1
        property bool studentAssignment_sortOrder: Qt.AscendingOrder
        property bool studentAssignment_includeWeekendParts: false
        property int studentAssignment_detailRowCount: 3
        property int studentAssignment_labelingMode: 1
        property bool studentAssignment_hideUnavailables: true
        property bool assistantAssignment_filter1: false
        property bool assistantAssignment_filter2: false
        property bool assistantAssignment_filter3: false
        property bool assistantAssignment_filter4: true
        property bool assistantAssignment_filter5: true
        property int assistantAssignment_groupByIndex: 1
        property bool assistantAssignment_sortOrder: Qt.AscendingOrder
        property bool assistantAssignment_includeWeekendParts: false
        property int assistantAssignment_detailRowCount: 3
        property int assistantAssignment_labelingMode: 1
        property bool assistantAssignment_hideUnavailables: true
    }

    AssignmentController { id: myController }
    ShareUtils { id: shareUtils }

    Family { id: family }

    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: assigneeLookupControlMoreMenu.hideUnavailables
    }

    PersonDetailModel { id: assistantDetailModel }
    PersonDetailSFProxyModel {
        id: assistantDetailProxyModel
        source: assistantDetailModel
        onGroupByChanged: assistantDetailProxyModel.sort(0, assistantLookupControl.sortOrder)
        areUnavailablesHidden: assistantLookupControlMoreMenu.hideUnavailables
    }

    PersonDetailModel { id: volunteerDetailModel }
    PersonDetailSFProxyModel {
        id: volunteerDetailProxyModel
        source: volunteerDetailModel
        onGroupByChanged: volunteerDetailProxyModel.sort(0, volunteerLookupControl.sortOrder)
        areUnavailablesHidden: assigneeLookupControlMoreMenu.hideUnavailables
    }

    PersonDetailFilterModel { id: assigneeDetailFilterModel }
    PersonDetailFilterModel { id: assistantDetailFilterModel }
    PersonDetailGroupByModel { id: personDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    LMMAssignmentValidator {
        id: assigneeValidator
        assignment: currentAssignment
        isAssistant: false
        isVolunteer: false
        onErrorChanged: assigneeErrorLabel.text = error
    }

    LMMAssignmentValidator {
        id: assistantValidator
        assignment: currentAssignment
        isAssistant: true
        isVolunteer: false
        onErrorChanged: assistantErrorLabel.text = error
    }

    LMMAssignmentValidator {
        id: volunteerValidator
        assignment: currentAssignment
        isAssistant: false
        isVolunteer: true
        onErrorChanged: volunteerErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: assigneeLookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.studentAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAssigneeDetailList();
            reloadVolunteerDetailList();
        }
        onDetailRowCountChanged: settings.studentAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.studentAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.studentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: assistantLookupControlMoreMenu
        meetingType: MeetingType.MidweekMeeting
        onIncludePartsOfOtherMeetingChanged: {
            settings.assistantAssignment_includeWeekendParts = includePartsOfOtherMeeting;
            reloadAssistantDetailList();
        }
        onDetailRowCountChanged: settings.assistantAssignment_detailRowCount = detailRowCount
        onLabelingModeChanged: settings.assistantAssignment_labelingMode = labelingMode
        onHideUnavailablesChanged: settings.assistantAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssistantLookupControlSettings()
    }

    Connections {
        target: currentAssignment
        function onSpeakerChanged() { reloadAssistantDetailList() }
        function onVolunteerChanged() { reloadAssistantDetailList() }
    }

    Component.onCompleted: {
        assigneeDetailFilterModel.setProperty(0, "checked", settings.studentAssignment_filter1);
        assigneeDetailFilterModel.setProperty(1, "checked", settings.studentAssignment_filter2);
        assigneeDetailFilterModel.setProperty(2, "checked", settings.studentAssignment_filter3);
        assigneeDetailFilterModel.setProperty(3, "checked", settings.studentAssignment_filter4);
        assigneeDetailFilterModel.setProperty(4, "checked", settings.studentAssignment_filter5);
        assigneeLookupControl.groupByIndex = settings.studentAssignment_groupByIndex;
        volunteerLookupControl.groupByIndex = settings.studentAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.studentAssignment_sortOrder;
        volunteerLookupControl.sortOrder = settings.studentAssignment_sortOrder;
        assigneeLookupControlMoreMenu.includePartsOfOtherMeeting = settings.studentAssignment_includeWeekendParts;
        assigneeLookupControlMoreMenu.detailRowCount = settings.studentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.labelingMode = settings.studentAssignment_labelingMode;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.studentAssignment_hideUnavailables;
        assigneeLookupControl.filterModel = assigneeDetailFilterModel;
        volunteerLookupControl.filterModel = assigneeDetailFilterModel;

        assistantDetailFilterModel.setProperty(0, "checked", settings.assistantAssignment_filter1);
        assistantDetailFilterModel.setProperty(1, "checked", settings.assistantAssignment_filter2);
        assistantDetailFilterModel.setProperty(2, "checked", settings.assistantAssignment_filter3);
        assistantDetailFilterModel.setProperty(3, "checked", settings.assistantAssignment_filter4);
        assistantDetailFilterModel.setProperty(4, "checked", settings.assistantAssignment_filter5);
        assistantDetailFilterModel.setProperty(4, "text", qsTr("With the same student", "Filter for meeting assignments"));
        assistantLookupControl.groupByIndex = settings.assistantAssignment_groupByIndex;
        assistantLookupControl.sortOrder = settings.assistantAssignment_sortOrder;
        assistantLookupControlMoreMenu.includePartsOfOtherMeeting = settings.assistantAssignment_includeWeekendParts;
        assistantLookupControlMoreMenu.detailRowCount = settings.assistantAssignment_detailRowCount;
        assistantLookupControlMoreMenu.labelingMode = settings.assistantAssignment_labelingMode;
        assistantLookupControlMoreMenu.hideUnavailables = settings.assistantAssignment_hideUnavailables;
        assistantLookupControl.filterModel = assistantDetailFilterModel;
        reloadAssigneeDetailList();
        reloadAssistantDetailList();
        reloadVolunteerDetailList();
    }

    Component.onDestruction: {
        settings.studentAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.studentAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.studentAssignment_includeWeekendParts = assigneeLookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.studentAssignment_detailRowCount = assigneeLookupControlMoreMenu.detailRowCount;
        settings.studentAssignment_labelingMode = assigneeLookupControlMoreMenu.labelingMode;
        settings.studentAssignment_hideUnavailables = assigneeLookupControlMoreMenu.hideUnavailables;
        settings.assistantAssignment_groupByIndex = assistantLookupControl.groupByIndex;
        settings.assistantAssignment_sortOrder = assistantLookupControl.sortOrder;
        settings.assistantAssignment_includeWeekendParts = assistantLookupControlMoreMenu.includePartsOfOtherMeeting;
        settings.assistantAssignment_detailRowCount = assistantLookupControlMoreMenu.detailRowCount;
        settings.assistantAssignment_labelingMode = assistantLookupControlMoreMenu.labelingMode;
        settings.assistantAssignment_hideUnavailables = assistantLookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onCurrentAssignmentChanged: {
        isLoading = true
        if (!currentAssignment) return
        myController.assignment = currentAssignment
        isLoading = false
    }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10
        ColumnLayout {
            anchors.fill: parent

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/wt_source.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Source")
                    ToolTip.visible: hovered
                }
                TextArea {
                    id: textArea
                    Layout.fillWidth: true
                    background: null
                    text: currentAssignment ? currentAssignment.source : ""
                    readOnly: true
                    wrapMode: Text.WordWrap
                    topPadding: 0
                    bottomPadding: 0
                }
            }

            GridLayout {
                Layout.fillWidth: true
                columns: 2
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/contact.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Student")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assigneeLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assigneeDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: assigneeLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (currentAssignment && currentAssignment.speaker
                                    ? currentAssignment.speaker.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assigneeValidator

                    delegate: PersonDetailDelegate {
                        id: assigneeDetailItem

                        meetingDate: currentAssignment.date
                        detailRowCount: assigneeLookupControlMoreMenu.detailRowCount
                        labelingMode: assigneeLookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: assigneeLookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assigneeDetailItem.width - 2
                            height: assigneeDetailItem.height - 2
                            color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assigneeDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true
                        currentAssignment.speaker = currentValue < 1 ? null : myController.getPublisherById(currentValue)
                        currentAssignment.save();
                        // trigger revalidation
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = currentAssignment && currentAssignment.speaker
                                ? indexOfValue(currentAssignment.speaker.id)
                                : -1;
                    }
                    popup.onAboutToHide: {
                        var id = currentAssignment && currentAssignment.speaker
                                ? currentAssignment.speaker.id
                                : -1;
                        assigneeDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("studentAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadAssigneeDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.studentAssignment_groupByIndex = groupByIndex;
                            assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.studentAssignment_sortOrder = sortOrder;
                        assigneeDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        currentAssignment.speaker = null
                        currentAssignment.save()
                        // trigger revalidation
                        isLoading = false
                    }
                }
                Label {
                    id: assigneeErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            GridLayout {
                Layout.fillWidth: true
                columns: 2
                rows: 2
                visible: currentAssignment && currentAssignment.canHaveAssistant
                enabled: currentAssignment && (currentAssignment.speaker || currentAssignment.assistant)
                ToolButton {
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    icon.source: "qrc:/icons/assistant.svg"
                    ToolTip.text: qsTr("Assistant")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assistantLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assistantDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: assistantLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (currentAssignment && currentAssignment.assistant
                                    ? currentAssignment.assistant.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assistantValidator

                    delegate: PersonDetailDelegate {
                        id: assistantDetailItem

                        meetingDate: currentAssignment.date
                        detailRowCount: assistantLookupControlMoreMenu.detailRowCount
                        labelingMode: assistantLookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: assistantLookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assistantDetailItem.width - 2
                            height: assistantDetailItem.height - 2
                            color: assistantLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assistantDetailItem.down || assistantDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assistantDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true
                        currentAssignment.assistant = currentValue < 1 ? null : myController.getPublisherById(currentValue);
                        currentAssignment.save();
                        // trigger revalidation
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = currentAssignment && currentAssignment.assistant
                                ? indexOfValue(currentAssignment.assistant.id)
                                : -1
                    }

                    popup.onAboutToHide: {
                        var id = currentAssignment && currentAssignment.assistant
                                ? currentAssignment.assistant.id
                                : -1;
                        assistantDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assistantDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("assistantAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadAssistantDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.assistantAssignment_groupByIndex = groupByIndex;
                            assistantDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.assistantAssignment_sortOrder = sortOrder;
                        assistantDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        currentAssignment.assistant = null
                        currentAssignment.save()
                        // trigger revalidation
                        isLoading = false
                    }
                }
                Label {
                    id: assistantErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/study_point.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Study point")
                    ToolTip.visible: hovered
                    visible: currentAssignment
                }
                TextArea {
                    Layout.fillWidth: true
                    background: null
                    topPadding: 0
                    bottomPadding: 0
                    text: currentAssignment
                          ? currentAssignment.studyNumber + " " + currentAssignment.studyName
                          : ""
                    visible: currentAssignment
                    enabled: false
                }
            }

            // result
            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/study_done.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Result")
                    ToolTip.visible: hovered
                }
                CheckBox {
                    id: checkCompleted
                    text: qsTr("Completed")
                    Layout.fillWidth: true
                    topPadding: 0
                    bottomPadding: 0
                    leftPadding: 4
                    enabled: currentAssignment && currentAssignment.speaker
                    checked: currentAssignment && currentAssignment.completed ? true : false
                    onClicked: {
                        isLoading = true
                        if (currentAssignment.completed !== checked) {
                            currentAssignment.completed = checked;
                            currentAssignment.save();
                        }
                        volunteerLookupControl.currentIndex = -1
                        // trigger revalidation
                        isLoading = false
                    }
                }
            }
            GridLayout {
                Layout.fillWidth: true
                columns: 3
                rows: 2
                visible: checkCompleted.checked

                ToolButton {
                    icon.source: "qrc:/icons/volunteer.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Volunteer")
                    ToolTip.visible: hovered
                }
                CheckBox {
                    id: checkVolunteer
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 1
                    Layout.row: 0
                    checked: currentAssignment && currentAssignment.volunteer ? true : false
                    onCheckedChanged: {
                        isLoading = true;
                        if (!checked && currentAssignment && currentAssignment.volunteer) {
                            isLoading = true;
                            currentAssignment.volunteer = null;
                            currentAssignment.save();
                            volunteerLookupControl.currentIndex = -1;
                            // trigger revalidation
                            isLoading = false;
                        } else {
                            volunteerLookupControl.currentIndex = currentAssignment && currentAssignment.volunteer
                                    ? volunteerLookupControl.indexOfValue(currentAssignment.volunteer.id)
                                    : -1;
                        }
                        // trigger revalidation
                        isLoading = false;
                    }
                }
                LookupComboBox {
                    id: volunteerLookupControl

                    enabled: checkVolunteer.checked
                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 2
                    Layout.row: 0

                    showFilterControls: true
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: volunteerDetailProxyModel
                    groupByModel: personDetailGroupByModel
                    moreMenu: assigneeLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (currentAssignment && currentAssignment.volunteer
                                    ? currentAssignment.volunteer.fullname
                                    : "")
                                 : currentText

                    validator: isLoading ? null : volunteerValidator

                    delegate: PersonDetailDelegate {
                        id: volunteerDetailItem

                        meetingDate: currentAssignment.date
                        detailRowCount: assigneeLookupControlMoreMenu.detailRowCount
                        labelingMode: assigneeLookupControlMoreMenu.labelingMode
                        includePartsOfOtherMeeting: assigneeLookupControlMoreMenu.includePartsOfOtherMeeting
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: volunteerDetailItem.width - 2
                            height: volunteerDetailItem.height - 2
                            color: volunteerLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: volunteerDetailItem.down || volunteerDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: volunteerDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        isLoading = true
                        currentAssignment.volunteer = currentValue < 1 ? null : myController.getPublisherById(currentValue)
                        currentAssignment.save();
                        // trigger revalidation
                        isLoading = false
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = currentAssignment && currentAssignment.volunteer
                                ? indexOfValue(currentAssignment.volunteer.id)
                                : -1;
                    }
                    popup.onAboutToHide: {
                        var id = currentAssignment && currentAssignment.volunteer
                                ? currentAssignment.volunteer.id
                                : -1;
                        volunteerDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: volunteerDetailProxyModel.filterText = text
                    onFilterChanged: {
                        settings.setValue("studentAssignment_filter" + (index + 1), checked);
                        settings.sync();
                        reloadVolunteerDetailList();
                    }
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.studentAssignment_groupByIndex = groupByIndex;
                            volunteerDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.studentAssignment_sortOrder = sortOrder;
                        volunteerDetailProxyModel.sort(0, sortOrder)
                    }
                    onCleared: {
                        isLoading = true
                        currentAssignment.volunteer = null
                        currentAssignment.save()
                        // trigger revalidation
                        isLoading = false
                    }
                }
                Label {
                    id: volunteerErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 2
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/study_timing.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Timing")
                    ToolTip.visible: hovered
                }
                TextArea {
                    //placeholderText: "Timing"
                    id: textfieldTiming
                    Layout.fillWidth: true
                    background: null
                    enabled: checkCompleted.checked
                    text: currentAssignment ? currentAssignment.timing : ""
                    onEditingFinished: {
                        if (currentAssignment.timing !== text) {
                            currentAssignment.timing = text
                            currentAssignment.save()
                        }
                    }
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/notes.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Note")
                    ToolTip.visible: hovered
                }
                TextArea {
                    id: texteditNote
                    Layout.fillWidth: true
                    wrapMode: Text.WordWrap
                    selectByMouse: true
                    text: currentAssignment ? currentAssignment.note : ""
                    onEditingFinished: {
                        if (currentAssignment.note !== text)
                            currentAssignment.note = text
                            currentAssignment.save()
                    }
                }
            }
        }
    }

    footer: ColumnLayout {
        RowLayout {
            Layout.alignment: Qt.AlignRight
            Layout.margins: 10
            ToolButton {
                id: copyButton
                icon.source: "qrc:/icons/copy.svg"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    shareUtils.copyToClipboard(currentAssignment.getReminderText())
                }
            }

            ToolButton {
                id: shareButton
                icon.source: "qrc:/icons/share.svg"
                visible: canSendMidweekMeetingReminders
                onClicked: {
                    var pos = mapToGlobal(0, height)
                    shareUtils.share(currentAssignment.getReminderText(), "", pos)
                }
            }
        }
    }
}
