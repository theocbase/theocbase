import QtQuick 2.15
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.15
import "controls"

Item {
    width: 400
    height: 400
    property alias editAddressButton: editAddressButton
    property alias addressTableView: addressTableView
    property alias removeAddressButton: removeAddressButton
    property alias addAddressButton: addAddressButton

    SystemPalette {
        id: myPalette
        colorGroup: SystemPalette.Active
    }

    ColumnLayout {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: 12
        anchors.leftMargin: 12
        anchors.bottomMargin: 12
        anchors.topMargin: 12
        visible: true

        TableView {
            id: addressTableView
            sortIndicatorVisible: true

            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            TableViewColumn {
                role: "id"
                title: qsTr("ID")
                width: 30
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "territoryId"
                title: qsTr("Territory-ID")
                width: 30
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "country"
                title: qsTr("Country", "Short name of country")
                width: 60
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        color: myPalette.text
                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "state"
                title: qsTr("State",
                            "Short name of administrative area level 1")
                width: 60
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "county"
                title: qsTr("County", "Name of administrative area level 2")
                width: 100
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "city"
                title: qsTr("City", "Locality")
                width: 130
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "district"
                title: qsTr("District", "Sublocality")
                width: 100
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                id: streetColumn
                role: "street"
                title: qsTr("Street", "Street name")
                width: 230
                visible: true

                delegate: Item {
                    Label {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        color: styleData.textColor
                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                        elide: Text.ElideRight
                    }
                }
            }

            TableViewColumn {
                id: houseNumberColumn
                role: "houseNumber"
                title: qsTr("No.", "House or street number")
                width: 50
                visible: true

                delegate: houseNumberItemDelegate
            }

            TableViewColumn {
                role: "postalCode"
                title: qsTr("Postal code", "Mail code, ZIP")
                width: 50
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        color: myPalette.text
                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "wktGeometry"
                title: qsTr("Geometry", "Coordinate geometry of the address")
                width: 30
                visible: false

                delegate: Item {
                    Text {
                        anchors.fill: parent
                        anchors.leftMargin: 5
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft

                        text: typeof styleData.value === "undefined" ? "" : styleData.value
                    }
                }
            }

            TableViewColumn {
                role: "name"
                title: qsTr("Name", "Name of person or building")
                width: parent.width - streetColumn.width - houseNumberColumn.width - addressTypeNumberColumn.width - requestDateColumn.width - 50
                visible: true

                delegate: nameItemDelegate
            }

            TableViewColumn {
                id: addressTypeNumberColumn
                role: "addressTypeNumber"
                title: qsTr("Type", "Type of address")
                width: 150
                visible: true

                delegate: addressTypeItemDelegate
            }

            TableViewColumn {
                id: requestDateColumn
                role: "requestDate"
                title: qsTr("Request date", "Request date of an address, e.g. do not call request date")
                width: 100
                visible: true

                delegate: requestDateItemDelegate
            }

            rowDelegate: Rectangle {
                id: tableViewRowDelegate
                height: 25
                color: styleData.selected ? myPalette.highlight : (styleData.alternate ? myPalette.alternateBase : myPalette.base)
            }
        }

        RowLayout {
            width: 50
            Layout.fillWidth: false
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            spacing: 10

            ToolButton {
                id: addAddressButton

                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                tooltip: qsTr("Add new address")
                icon.source: "qrc:///icons/territory_add_address.svg"
                opacity: addAddressButton.enabled ? 1.0 : 0.5
                visible: canEditTerritories
            }

            ToolButton {
                id: editAddressButton

                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                tooltip: qsTr("Edit selected address")
                icon.source: "qrc:///icons/map-marker-edit.svg"
                opacity: editAddressButton.enabled ? 1.0 : 0.5
                visible: canEditTerritories
            }

            ToolButton {
                id: removeAddressButton

                Layout.minimumWidth: 24
                Layout.minimumHeight: 24
                tooltip: qsTr("Remove selected address")
                icon.source: "qrc:///icons/territory_remove_address.svg"
                opacity: removeAddressButton.enabled ? 1.0 : 0.5
                visible: canEditTerritories
            }
        }
    }
}
