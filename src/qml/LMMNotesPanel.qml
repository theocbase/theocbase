/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import net.theocbase 1.0
import "controls"

Page {
    title: openComments ? qsTr("Opening Comments") : qsTr("Concluding Comments")
    header: Label {
        text: title
        font: TBStyle.titleSmallFont
        padding: 10
    }

    property bool openComments: true
    property LMM_Meeting meeting
    property alias notes : texteditNote.text

    AssignmentController { id: myController }

    ScrollView {
        anchors.fill: parent
        contentWidth: availableWidth
        clip: true
        padding: 10

        RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                icon.source: "qrc:/icons/notes.svg"
                background: null
                ToolTip.text: qsTr("Notes")
                ToolTip.visible: hovered
            }
            TextArea{
                id: texteditNote
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: openComments ? meeting.openingComments : meeting.closingComments

                onEditingFinished: {
                    if (openComments) {
                        if (meeting.openingComments === text)
                            return
                        meeting.openingComments = text
                    } else {
                        if (meeting.closingComments === text)
                            return
                        meeting.closingComments = text
                    }
                    meeting.save()
                }
            }
        }
    }
}
