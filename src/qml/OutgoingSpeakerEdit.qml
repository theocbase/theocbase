/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.1
import net.theocbase 1.0
import "controls"
import "sidePanelScripts.js" as SPScripts

Page {
    id: outgoingSpeakerPanel

    title: qsTr("Outgoing Speaker")
    header: Label {
       text: title
       font: TBStyle.titleSmallFont
       padding: 10
    }

    property OutgoingSpeakersModel outModel
    property bool isLoading: true
    property int modelIndex: -1
    property int speakerId: -1
    property int themeId: -1
    property int congId: -1
    property date currentDate

    signal movedToSchedule(var date, var incoming)

    // javascript functions
    function reloadAssigneeDetailList() {
        assigneeDetailModel.loadLocalPublicTalkPersonDetails(speakerId, // include current assignment
                                                             themeId,
                                                             currentDate);
        assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder);
    }
    function reloadPublicTalkDetailList() {
        publicTalkDetailModel.loadPublicTalkDetails(themeId,
                                                    speakerId,
                                                    currentDate);
        publicTalkDetailProxyModel.sort(0, publicTalkLookupControl.sortOrder);
    }
    function reloadCongregationDetailList() {
        congregationDetailModel.loadCongregationDetails(false, currentDate);
        congregationDetailProxyModel.sort(0, congregationLookupControl.sortOrder);
    }

    function resetDefaultAssigneeLookupControlSettings() {
        settings.nonstudentAssignment_groupByIndex = 1;
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        settings.nonstudentAssignment_sortOrder = Qt.AscendingOrder;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        settings.nonstudentAssignment_detailRowCount = 3;
        settings.nonstudentAssignment_hideUnavailables = true;
        assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        settings.sync();
        reloadAssigneeDetailList();
    }
    function resetDefaultPublicTalkLookupControlSettings() {
        settings.publicTalk_groupByIndex = 1;
        publicTalkLookupControl.groupByIndex = settings.publicTalk_groupByIndex;
        settings.publicTalk_sortOrder = Qt.AscendingOrder;
        publicTalkLookupControl.sortOrder = settings.publicTalk_sortOrder;
        settings.publicTalk_detailRowCount = 3;
        publicTalkLookupControlMoreMenu.detailRowCount = settings.publicTalk_detailRowCount;
        settings.sync();
        reloadPublicTalkDetailList();
    }
    function resetDefaultCongregationLookupControlSettings() {
        settings.congregation_groupByIndex = 1;
        congregationLookupControl.groupByIndex = settings.congregation_groupByIndex;
        settings.congregation_sortOrder = Qt.AscendingOrder;
        congregationLookupControl.sortOrder = settings.congregation_sortOrder;
        settings.congregation_detailRowCount = 3;
        congregationLookupControlMoreMenu.detailRowCount = settings.congregation_detailRowCount;
        settings.sync();
        reloadCongregationDetailList();
    }

    // workaround to use the right color on Windows with dark theme
    SystemPalette { id: myPalette; colorGroup: SystemPalette.Active }

    CalendarPopup {
        id: calPopup
        onActivated: {
            outModel.moveToWeek(modelIndex, selectedDate)
        }
    }

    Settings {
        id: settings
        category: "LookupControl"
        property int publicTalk_groupByIndex: 1
        property bool publicTalk_sortOrder: Qt.AscendingOrder
        property int publicTalk_detailRowCount: 3
        property int congregation_groupByIndex: 1
        property bool congregation_sortOrder: Qt.AscendingOrder
        property int congregation_detailRowCount: 3
        property int nonstudentAssignment_groupByIndex: 1
        property bool nonstudentAssignment_sortOrder: Qt.AscendingOrder
        property int nonstudentAssignment_detailRowCount: 3
        property bool nonstudentAssignment_hideUnavailables: true
    }

    PublicTalkDetailModel { id: publicTalkDetailModel }
    PublicTalkDetailSFProxyModel {
        id: publicTalkDetailProxyModel
        source: publicTalkDetailModel
        onGroupByChanged: publicTalkDetailProxyModel.sort(0, publicTalkLookupControl.sortOrder)
    }
    CongregationDetailModel { id: congregationDetailModel }
    CongregationDetailSFProxyModel {
        id: congregationDetailProxyModel
        source: congregationDetailModel
        onGroupByChanged: congregationDetailProxyModel.sort(0, congregationLookupControl.sortOrder)
    }
    PersonDetailModel { id: assigneeDetailModel }
    PersonDetailSFProxyModel {
        id: assigneeDetailProxyModel
        source: assigneeDetailModel
        onGroupByChanged: assigneeDetailProxyModel.sort(0, assigneeLookupControl.sortOrder)
        areUnavailablesHidden: assigneeLookupControlMoreMenu.hideUnavailables
    }

    PublicTalkDetailGroupByModel { id: publicTalkDetailGroupByModel }
    PublicTalkDetailGroupByModel { id: congregationDetailGroupByModel }
    PublicTalkDetailGroupByModel { id: assigneeDetailGroupByModel }
    PersonDetailSectionDelegate { id: personDetailSectionDelegate }

    OutgoingSpeakerValidator {
        id: publicTalkValidator
        model: outModel
        row: modelIndex
        role: OutgoingSpeakersModel.ThemeRole
        onErrorChanged: publicTalkErrorLabel.text = error
    }

    OutgoingSpeakerValidator {
        id: congregationValidator
        model: outModel
        row: modelIndex
        role: OutgoingSpeakersModel.CongregationRole
        onErrorChanged: congregationErrorLabel.text = error
    }

    OutgoingSpeakerValidator {
        id: assigneeValidator
        model: outModel
        row: modelIndex
        role: OutgoingSpeakersModel.SpeakerRole
        onErrorChanged: assigneeErrorLabel.text = error
    }

    LookupControlMoreMenu {
        id: publicTalkLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        isHideUnavailablesAllowed: false
        onDetailRowCountChanged: settings.publicTalk_detailRowCount = detailRowCount
        onResetDefaultSettings: resetDefaultPublicTalkLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: congregationLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        isHideUnavailablesAllowed: false
        onDetailRowCountChanged: settings.congregation_detailRowCount = detailRowCount
        onResetDefaultSettings: resetDefaultCongregationLookupControlSettings()
    }

    LookupControlMoreMenu {
        id: assigneeLookupControlMoreMenu
        meetingType: MeetingType.WeekendMeeting
        isIncludingPartsOfOtherMeetingAllowed: false
        isLabelChangingAllowed: false
        onDetailRowCountChanged: settings.nonstudentAssignment_detailRowCount = detailRowCount
        onHideUnavailablesChanged: settings.nonstudentAssignment_hideUnavailables = hideUnavailables
        onResetDefaultSettings: resetDefaultAssigneeLookupControlSettings()
    }

    Component.onCompleted: {
        publicTalkDetailGroupByModel.remove(5); // remove congregation
        publicTalkDetailGroupByModel.setProperty(3, "value", "alphabet");
        publicTalkDetailGroupByModel.remove(0); // remove name
        publicTalkLookupControl.groupByIndex = settings.publicTalk_groupByIndex;
        publicTalkLookupControl.sortOrder = settings.publicTalk_sortOrder;
        publicTalkLookupControlMoreMenu.detailRowCount = settings.publicTalk_detailRowCount;
        congregationDetailGroupByModel.remove(5); // remove congregation
        congregationDetailGroupByModel.remove(4); // remove public talk number
        congregationDetailGroupByModel.remove(3); // remove public talk theme
        congregationLookupControl.groupByIndex = settings.congregation_groupByIndex;
        congregationLookupControl.sortOrder = settings.congregation_sortOrder;
        congregationLookupControlMoreMenu.detailRowCount = settings.congregation_detailRowCount;
        assigneeLookupControl.groupByIndex = settings.nonstudentAssignment_groupByIndex;
        assigneeLookupControl.sortOrder = settings.nonstudentAssignment_sortOrder;
        assigneeLookupControlMoreMenu.detailRowCount = settings.nonstudentAssignment_detailRowCount;
        assigneeLookupControlMoreMenu.hideUnavailables = settings.nonstudentAssignment_hideUnavailables;
        assigneeDetailGroupByModel.remove(4); // remove public talk number
        assigneeDetailGroupByModel.remove(3); // remove public talk theme

        reloadPublicTalkDetailList();
        reloadCongregationDetailList();
        reloadAssigneeDetailList();
    }

    Component.onDestruction: {
        settings.publicTalk_groupByIndex = publicTalkLookupControl.groupByIndex;
        settings.publicTalk_sortOrder = publicTalkLookupControl.sortOrder;
        settings.publicTalk_detailRowCount = publicTalkLookupControlMoreMenu.detailRowCount;
        settings.congregation_groupByIndex = congregationLookupControl.groupByIndex;
        settings.congregation_sortOrder = congregationLookupControl.sortOrder;
        settings.congregation_detailRowCount = congregationLookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_groupByIndex = assigneeLookupControl.groupByIndex;
        settings.nonstudentAssignment_sortOrder = assigneeLookupControl.sortOrder;
        settings.nonstudentAssignment_detailRowCount = assigneeLookupControlMoreMenu.detailRowCount;
        settings.nonstudentAssignment_hideUnavailables = assigneeLookupControlMoreMenu.hideUnavailables;
        settings.sync();
    }

    onOutModelChanged: {
        if (modelIndex >= 0) {
            speakerId = outModel.get(modelIndex).speakerId
            congId = outModel.get(modelIndex).congregationId
            themeId = outModel.get(modelIndex).themeId
        }
    }

    onModelIndexChanged: {
        isLoading = true;
        if (outModel == null)
            return

        speakerId = outModel.get(modelIndex).speakerId
        congId = outModel.get(modelIndex).congregationId
        themeId = outModel.get(modelIndex).themeId

        isLoading = false;
    }
    onCurrentDateChanged: {
        controller.date = currentDate
    }

    PublicMeetingController { id: controller }

    function saveChanges() {
        if (modelIndex < 0) {
            console.error("model index is negative!!!")
            return
        }
        outModel.editRow(modelIndex, speakerId, themeId, congId)
    }

    Pane {
        anchors.fill: parent
        clip: true
        padding: 10
        ColumnLayout {
            anchors.fill: parent

            //Speaker
            GridLayout {
                columns: 4
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/servant.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Speaker")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: assigneeLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: assigneeDetailProxyModel
                    groupByModel: assigneeDetailGroupByModel
                    moreMenu: assigneeLookupControlMoreMenu

                    textRole: "personFullName"
                    valueRole: "personId"
                    displayText: currentIndex === -1
                                 ? (outModel.get(modelIndex)
                                    ? outModel.get(modelIndex).speaker
                                    : "")
                                 : currentText

                    validator: isLoading ? null : assigneeValidator

                    delegate: PersonDetailDelegate {
                        id: assigneeDetailItem

                        detailRowCount: assigneeLookupControlMoreMenu.detailRowCount
                        labelingMode: 2
                        includePartsOfOtherMeeting: assigneeLookupControlMoreMenu.includePartsOfOtherMeeting
                        displayAssigneeLabel: false
                        displayTimeLine: false
                        isOutgoingSpeaker: true
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: assigneeDetailItem.width - 2
                            height: assigneeDetailItem.height - 2
                            color: assigneeLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: assigneeDetailItem.down || assigneeDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: assigneeDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        speakerId = currentValue;
                        saveChanges();
                        // trigger revalidation by switching isLoaded value
                        isLoading = true;
                        isLoading = false;
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = indexOfValue(speakerId);
                    }
                    popup.onAboutToHide: {
                        var id = speakerId;
                        assigneeDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: assigneeDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.nonstudentAssignment_groupByIndex = groupByIndex;
                            assigneeDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.nonstudentAssignment_sortOrder = sortOrder;
                        assigneeDetailProxyModel.sort(0, sortOrder);
                    }
                    onCleared: {
                        isLoading = true;
                        speakerId = -1;
                        saveChanges();
                        isLoading = false;
                    }
                }
                Label {
                    id: assigneeErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
                ToolButton {
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 2
                    Layout.row: 0
                    icon.source: "qrc:/icons/move_to_week.svg"
                    ToolTip.text: qsTr("Move to different week")
                    ToolTip.visible: hovered
                    onClicked: {
                        if (calPopup.opened) {
                            calPopup.close()
                        } else {
                            var pos = mapToItem(outgoingSpeakerPanel, 0, 0)
                            calPopup.selectedDate = controller.date
                            calPopup.x = Math.min(pos.x, outgoingSpeakerPanel.width - calPopup.width - 10)
                            calPopup.y = pos.y + height
                            calPopup.open()
                        }
                    }
                }
                ToolButton {
                    icon.source: "qrc:/icons/move_to_todo.svg"
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 3
                    Layout.row: 0
                    ToolTip.text: qsTr("Send to To Do List")
                    ToolTip.visible: hovered
                    onClicked: {
                        outModel.moveToTodo(modelIndex);
                    }
                }
            }

            // Theme
            GridLayout {
                columns: 3
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/title.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Theme")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: publicTalkLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    groupByIndex: -1
                    showEditButton: false
                    isEditing: false

                    model: publicTalkDetailProxyModel
                    groupByModel: publicTalkDetailGroupByModel
                    moreMenu: publicTalkLookupControlMoreMenu

                    textRole: "numberAndTheme"
                    valueRole: "themeId"

                    validator: isLoading ? null : publicTalkValidator

                    delegate: PublicTalkDetailDelegate {
                        id: publicTalkDetailItem

                        detailRowCount: publicTalkLookupControlMoreMenu.detailRowCount
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: publicTalkDetailItem.width - 2
                            height: publicTalkDetailItem.height - 2
                            color: publicTalkLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: publicTalkDetailItem.down || publicTalkDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: publicTalkDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        themeId = currentValue;
                        saveChanges();
                        // trigger revalidation by switching isLoaded value
                        isLoading = true;
                        isLoading = false;
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = indexOfValue(themeId);
                    }
                    popup.onAboutToHide: {
                        var id = currentValue;
                        publicTalkDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: publicTalkDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.publicTalk_groupByIndex = groupByIndex;
                            publicTalkDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.publicTalk_sortOrder = sortOrder;
                        publicTalkDetailProxyModel.sort(0, sortOrder);
                    }
                    onCleared: {
                        isLoading = true;
                        themeId = -1;
                        saveChanges();
                        isLoading = false;
                    }
                }
                Label {
                    id: publicTalkErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            // Congregation
            GridLayout {
                columns: 3
                rows: 2
                ToolButton {
                    icon.source: "qrc:/icons/home.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    Layout.column: 0
                    Layout.row: 0
                    ToolTip.text: qsTr("Congregation")
                    ToolTip.visible: hovered
                }
                LookupComboBox {
                    id: congregationLookupControl

                    height: 40
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 0

                    showFilterControls: false
                    showGroupControls: true
                    showEditButton: false
                    isEditing: false

                    model: congregationDetailProxyModel
                    groupByModel: congregationDetailGroupByModel
                    moreMenu: congregationLookupControlMoreMenu

                    groupByIndex: -1

                    textRole: "congregationName"
                    valueRole: "congregationId"

                    validator: isLoading ? null : congregationValidator

                    delegate: CongregationDetailDelegate {
                        id: congregationDetailItem

                        detailRowCount: congregationLookupControlMoreMenu.detailRowCount
                        background: Rectangle {
                            implicitWidth: 200
                            implicitHeight: 40
                            x: 1
                            y: 1
                            width: congregationDetailItem.width - 2
                            height: congregationDetailItem.height - 2
                            color: congregationLookupControl.currentIndex === index ? myPalette.highlight : (index % 2 == 0 ? myPalette.base : myPalette.alternateBase)

                            ColorOverlay {
                                anchors.fill: parent
                                source: parent
                                color: congregationDetailItem.down || congregationDetailItem.highlighted ? myPalette.highlight : "transparent"
                                opacity: congregationDetailItem.down ? 1.0 : 0.25
                            }
                        }
                    }

                    sectionProperty: groupByModel.get(groupByIndex) ? groupByModel.get(groupByIndex).value : ""
                    sectionCriteria: ViewSection.FullString
                    sectionDelegate: personDetailSectionDelegate

                    // When an item is selected, update the backend.
                    onActivated: {
                        congId = currentValue;
                        saveChanges();
                    }
                    // Set the initial currentIndex to the value stored in the backend.
                    Component.onCompleted: {
                        currentIndex = indexOfValue(congId);
                    }
                    popup.onAboutToHide: {
                        var id = currentValue;
                        congregationDetailProxyModel.filterText = ""; // reset filter when closing the popup
                        currentIndex = indexOfValue(id); // restore currentIndex to selected value
                    }
                    onSearchTextChanged: congregationDetailProxyModel.filterText = text
                    onGroupByIndexChanged: {
                        if (groupByIndex >= 0) {
                            settings.congregation_groupByIndex = groupByIndex;
                            congregationDetailProxyModel.groupByRole = groupByModel.get(groupByIndex).value;
                        }
                    }
                    onSortOrderChanged: {
                        settings.congregation_sortOrder = sortOrder;
                        congregationDetailProxyModel.sort(0, sortOrder);
                    }
                    onCleared: {
                        isLoading = true;
                        congId = -1;
                        saveChanges();
                        isLoading = false;
                    }
                }
                Label {
                    id: congregationErrorLabel
                    Layout.fillWidth: true
                    Layout.column: 1
                    Layout.row: 1
                    verticalAlignment: Text.AlignTop
                    font: TBStyle.bodySmallFont
                    color: TBStyle.alertColor
                    visible: text
                    wrapMode: Text.Wrap
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/contact_address.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Address")
                    ToolTip.visible: hovered
                }
                TextArea{
                    id: textAddress
                    topPadding: 0
                    bottomPadding: 0
                    background: null
                    Layout.fillWidth: true
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    readOnly: true
                    text: congId > 0 ? outModel.get(modelIndex).congregationAddress : ""
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/meeting_day.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Meeting day")
                    ToolTip.visible: hovered
                }
                TextField {
                    topPadding: 0
                    bottomPadding: 0
                    background: null
                    text: congId > 0 ? Qt.locale().dayName(outModel.get(modelIndex).date.getDay(), Locale.LongFormat) : ""
                    Layout.fillWidth: true
                    readOnly: true
                }
                ToolButton {
                    icon.source: "qrc:/icons/meeting_time.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Meeting time")
                    ToolTip.visible: hovered
                }
                TextField {
                    topPadding: 0
                    bottomPadding: 0
                    background: null
                    text: congId > 0 ? outModel.get(modelIndex).time : ""
                    Layout.fillWidth: true
                    readOnly: true
                }
            }

            RowLayout {
                Layout.fillWidth: true
                ToolButton {
                    icon.source: "qrc:/icons/contact_info.svg"
                    background: null
                    Layout.alignment: Qt.AlignLeft | Qt.AlignTop
                    ToolTip.text: qsTr("Info")
                    ToolTip.visible: hovered
                }
                TextArea {
                    Layout.fillWidth: true
                    topPadding: 0
                    bottomPadding: 0
                    background: null
                    selectByMouse: true
                    wrapMode: Text.WordWrap
                    readOnly: true
                    text: congId > 0 ? outModel.get(modelIndex).congregationInfo : ""
                }
            }

            Item {
                height: 20
            }

            // To do list
            TodoPanel {
                id: todo
                Layout.fillWidth: true

                isIncomingSpeaker: false

                onMovedToSchedule: {
                    outModel.loadList(currentDate);
                }
            }
        }
    }
}

