#include "school_detail.h"

school_detail::school_detail()
{
    mCurrentStudy = 0;
    mNextStudy = 0;
    studyread = false;
}

school_detail::school_detail(school_item &item)
{
    setAssignmentNumber( item.getAssignmentNumber() );
    setStudent( item.getStudent() );
    setAssistant( item.getAssistant() );
    setClassNumber( item.getClassNumber() );
    setDate( item.getDate() );
    setDone( item.getDone() );
    setId( item.getId() );
    setNote( item.getNote() );
    setScheduleId( item.getScheduleId() );
    setSetting( item.getSetting()->getNumber() );
    setSource( item.getSource() );
    setTheme( item.getTheme() );
    setTiming( item.getTiming() );
    setVolunteer( item.getVolunteer() );
    setOnlyBrothers( item.getOnlyBrothers() );
    mCurrentStudy = 0;
    mNextStudy = 0;
    studyread = false;
}

QVariant school_detail::studies()
{
    if (studiesList.count() == 0){
        studiesList.append(tr("Do not assign the next study"));
        school s;
        QList<schoolStudy*> slist = s.getStudies();
        foreach (schoolStudy *study, slist) {
            studiesList.append(QVariant(study->getNumber()).toString() + " " + study->getName());
        }
    }
    return QVariant::fromValue(studiesList);
}

QVariant school_detail::school_settings()
{
    if (settingList.count() == 0){
        settingList.append(tr("Not set"));
        school s;
        if (getStudent()){
            allSettings = s.getAllSettings(getStudent()->gender());
            foreach(school_setting *setting, allSettings) {
                settingList.append(QVariant(setting->getNumber()).toString() + " " + setting->getName());                
            }
        }
    }
    return QVariant::fromValue(settingList);
}

QVariant school_detail::volunteers()
{
    if (volunteersList.size() == 0){
        volunteersList.append(tr("Not set"));
        cpersons cp;
        QList<person*> allusers = cp.getAllPersons(0);

        foreach(person *p, allusers){

            switch(getAssignmentNumber()){
            case 0:
                // Bible highlights - Show only limited brothers
                if (!(p->usefor() & person::Highlights)) continue;
                break;
            case 1:
                // No 1 - Do not show sisters
                if (p->gender() != person::Male) continue;
                break;
            case 10:
                // Review reader - Show only wt readers
                if (!(p->usefor() & person::WtReader)) continue;
                break;
            default:
                break;
            }
            volunteersList.append(p->fullname() + ";" + QVariant(p->id()).toString());
        }
    }

    return QVariant::fromValue(volunteersList);
}

int school_detail::getActiveStudy()
{
    if (getAssignmentNumber() > 0 && getAssignmentNumber() < 10 && !studyread && getStudent()){
        school s;
        if (getDone()){
            QDate currentdate = getDate();
            mCurrentStudy = s.getStudyUsed(getStudent()->id(),currentdate,getType());
        }else{
            mCurrentStudy = s.getActiveStudyPoint(getStudent()->id(),getType(),false);
        }
        qDebug() << "active study" << (mCurrentStudy ? mCurrentStudy->getNumber() : 0);

        if(mCurrentStudy && !mCurrentStudy->getEndDate().isNull()){
            QList<schoolStudentStudy *> studies = s.getStudentStudies(getStudent()->id());
            foreach (schoolStudentStudy *study, studies){
                if (study->getStartdate() == mCurrentStudy->getEndDate().addDays(1)){
                    mNextStudy = study;
                    break;
                }
            }
        }
        studyread = true;
    }
    return mCurrentStudy ? mCurrentStudy->getNumber() : 0;
}

int school_detail::getNextStudy()
{
    if (!studyread) getActiveStudy();

    if (mNextStudy){
        return mNextStudy->getNumber();
    }else{
        if (mCurrentStudy && !mCurrentStudy->getEndDate().isValid()){
            return mCurrentStudy->getNumber();
        }else{
            return 0;
        }
    }
}

bool school_detail::getStudyExercise()
{
    if (!studyread) getActiveStudy();
    if (mCurrentStudy){
        return mCurrentStudy->getExercise();
    }else{
        return false;
    }
}

int school_detail::getSettingNumber()
{
    int index = -1;        
    if (getSetting() && getStudent()){
        if (allSettings.count() == 0){
            school s;
            allSettings = s.getAllSettings(getStudent()->gender());
        }
        for(int i=0;i<allSettings.count();++i){
            if (allSettings[i]->getNumber() == getSetting()->getNumber()){
                index = i;
                break;
            }
        }        
        return index+1;
    }else{        
        return -1;
    }
}

void school_detail::saveStudy(bool exerciseCompleted, bool assignmentDone, int nextStudy)
{
    if (!mCurrentStudy){
        qDebug() << "No active study";
        return;
    }
    if (mCurrentStudy->getExercise() == exerciseCompleted &&
            (getDone() == assignmentDone) &&
            (nextStudy == getNextStudy() ) ){
        qDebug() << "saveStudy: No changes!!";
        return;
    }

//    qDebug() << "saveStudy: exercise" << mCurrentStudy->getExercise() << exerciseCompleted;
//    qDebug() << "saveStudy: assignment done" << getDone() << assignmentDone;
//    qDebug() << "saveStudy: next study" << getNextStudy() << nextStudy;
    qDebug() << "saveStudy: changes found";

    // save active study

    if (assignmentDone){
        if (mCurrentStudy->getExercise() != exerciseCompleted) mCurrentStudy->setExercise(exerciseCompleted);
        ccongregation c;
        QDate fDate = getDate();
        QDate nDate = fDate.addDays(c.getMeetingDay(fDate,ccongregation::tms)-1);
        mCurrentStudy->setEndDate(nextStudy != mCurrentStudy->getNumber() ? nDate : QDate());
        mCurrentStudy->update();

        // next study?
        if ((mNextStudy) && (mNextStudy->getNumber() != nextStudy)){
            // next study changed or not assigned -> remove previous next study
            mNextStudy->remove();
            mNextStudy = 0;
        }
        if (!mNextStudy && nextStudy > 0 && nextStudy != mCurrentStudy->getNumber()){
            // add a new next study
            schoolStudentStudy *newNextStudy = new schoolStudentStudy();
            newNextStudy->setStudentId(getStudent()->id());
            newNextStudy->setExercise(false);
            newNextStudy->setNumber(nextStudy);
            newNextStudy->setStartdate(nDate.addDays(1));
            newNextStudy->update();
        }
    }else{
        // remove end date from current study
        if (mCurrentStudy->getEndDate().isValid()){
            mCurrentStudy->setEndDate(QDate());
            mCurrentStudy->update();
        }
        // remove next study if exists
        if (mNextStudy){
            mNextStudy->remove();
            mNextStudy = 0;
        }
    }
}

void school_detail::saveChanges(QString notes, QString timing, bool done, int setting, int volunteerid)
{
    int settingNumber = setting > 0 ? allSettings[setting-1]->getNumber() : -1;
    if ((getNote() != notes) ||
            (getTiming() != timing) ||
            ((getSettingNumber() != settingNumber) && (getSettingNumber() > 0 || settingNumber > 0)) ||
            (getDone() != done) ||
            ((getVolunteer() ? getVolunteer()->id() : -1) != volunteerid)){
        qDebug() << "save changed id:" << getId()
                 << "done:" << getDone() << done
                 <<  "notes:" << getNote() << notes
                 << "setting:" << getSetting()->getNumber() << setting
                 << "volunteerid:" << (getVolunteer() ? getVolunteer()->id() : -1) << volunteerid;
        setNote(notes);
        setTiming(timing);
        setSetting(settingNumber);
        setDone(done);
        setVolunteer(volunteerid > 0 ? cpersons::getPerson(volunteerid) : 0);
        save();
    }
}
