#ifndef SCHOOLVIEW_H
#define SCHOOLVIEW_H

#include <QObject>
#include <QString>
#include <QQmlContext>
#include <QQmlEngine>
#include <QDebug>
#include <QDate>
#include <QQmlListReference>
#include <QAbstractListModel>
#include <QQuickItem>
#include <QDate>
#include "../../school.h"
#include "school_detail.h"

class SchoolViewItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString numbername READ numberName NOTIFY numberNameChanged)
    Q_PROPERTY(QString student READ student WRITE setStudent NOTIFY studentChanged)
    Q_PROPERTY(QString assistant READ assistant WRITE setAssistant NOTIFY assistantChanged)
    Q_PROPERTY(QString volunteer READ volunteer CONSTANT)
    Q_PROPERTY(QString theme READ theme WRITE setTheme NOTIFY themeChanged)
    Q_PROPERTY(int idNumber READ idNumber WRITE setIdNumber NOTIFY idNumberChanged)
    Q_PROPERTY(int classnumber READ classNumber WRITE setClassNumber NOTIFY classnumberChanged)
    Q_PROPERTY(bool done READ done CONSTANT)
public:

        SchoolViewItem(QString number = "", QString theme = "",
                       QString student = "",
                       QString volunteer = "",
                       QString assistant = "",
                       int id = -1, int classnumber = 1,
                       bool done = false)
        {
            mNumberName = number;            
            mTheme = theme;
            mId = id;
            mStudent = student;
            mVolunteer = volunteer;
            mAssitant = assistant;
            mClassNumber = classnumber;
            mDone = done;
        }

        void setNumberName(QString no) { mNumberName = no; }
        QString numberName() { return mNumberName; }

        void setStudent(QString name) { mStudent = name; }
        QString student() { return mStudent; }

        void setVolunteer(QString name) { mVolunteer = name; }
        QString volunteer() { return mVolunteer; }

        void setAssistant(QString name) { mAssitant = name; }
        QString assistant() { return mAssitant; }

        void setTheme(QString value) { mTheme = value; }
        QString theme() { return mTheme; }

        void setIdNumber(int id) { mId = id; }
        int idNumber() { return mId; }

        void setClassNumber(int no){ mClassNumber = no; }
        int classNumber() { return mClassNumber; }

        void setDone(bool done){ mDone = done; }
        bool done() { return mDone; }
private:
        int mId;
        int mClassNumber = 1;
        QString mNumberName;
        QString mStudent;
        QString mVolunteer;
        QString mAssitant;
        QString mTheme;
        bool mDone;
signals:
    void studentChanged();
    void numberNameChanged();
    void themeChanged();
    void idNumberChanged();
    void classnumberChanged();
    void assistantChanged();
};

class schoolview : public QObject
{
    Q_OBJECT

public:
    schoolview(){
        mFirstDayOfWeek = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek()-1)*-1);
    }

    ~schoolview(){
    }

    Q_INVOKABLE QVariant loadSchedule(QDate date){
        qDebug() << "school load schedule" << date;
        mFirstDayOfWeek = date;
        updateList();
        return QVariant::fromValue(mSchoolList);
    }


    Q_INVOKABLE QDate currentDate()
    {
        return mFirstDayOfWeek;
    }

    Q_INVOKABLE school_detail *getAssignment(int index)
    {        
        school_detail *det = new school_detail();
        if (mSchoolList.size() > index){
            std::vector<std::unique_ptr<school_item> > list;
            if (s_class.getProgram(list, mFirstDayOfWeek)){
                det = new school_detail(*list[index]);
            }
        }
        return det;
    }

    Q_INVOKABLE int classesQty(){
        return s_class.getClassesQty();
    }

signals:
    Q_INVOKABLE void listUpdated(QVariant list);
private:
    QDate mFirstDayOfWeek;
    school s_class;
    QList<QObject *> mSchoolList;

    void updateList()
    {
        qDeleteAll(mSchoolList);
        mSchoolList.clear();

        std::vector<std::unique_ptr<school_item> >list;
        if (!s_class.getProgram(list,mFirstDayOfWeek)) return;
        for(unsigned int i = 0;i<list.size();i++){
            QString numbername = "";
            school_item &si = *list[i];
            switch (si.getAssignmentNumber()){
            case 0: numbername = tr("H","abbreviation of the 'highlights'"); break;
            case 1: numbername = "1"; break;
            case 2: numbername = "2"; break;
            case 3: numbername = "3"; break;
            case 10: numbername = tr("R","abbreviation of the 'reader'"); break;
            default: numbername = si.getNumberName();
            }

            mSchoolList.append(new SchoolViewItem(numbername,list.at(i)->getTheme(),
                                                  si.getStudent() ? si.getStudent()->fullname() : "",
                                                  si.getVolunteer() ? si.getVolunteer()->fullname() : "",
                                                  si.getAssistant() ? si.getAssistant()->fullname() : "",
                                                  si.getId(),si.getClassNumber(),
                                                  si.getDone()));
        }
    }
};

#endif // SCHOOLVIEW_H
