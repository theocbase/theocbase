/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "shareutils.h"
#include <UIKit/UIKit.h>
#include <MessageUI/MFMailComposeViewController.h>
#include <QDesktopServices>
#include <QDebug>
#include "../csync.h"
#include <SafariServices/SafariServices.h>
#include "iosutil.h"

@interface MailerController : UIViewController <MFMailComposeViewControllerDelegate>
{
    
}
- (void) dealloc;
- (void) sendMailWithAttachment:(NSString*) filename;
- (void) mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error;
@end

@implementation MailerController
- (void) dealloc
{
    [super dealloc];
}

- (void) sendMailWithAttachment: (NSString*) filename
{
    NSLog(@"Mailer:sendMailWithAttachment: create MFMailComposeViewController");
    NSLog(@"Filepath: %@",filename);
    
    if ( [MFMailComposeViewController canSendMail]){
        // [view addSubview: self];
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        [picker setSubject:@"TheocBase Mobile backup"];
        
        // Fill out the email body text
        NSString *emailBody = @"Backup file attached!";
        [picker setMessageBody:emailBody isHTML:NO];
        NSData *myData = [NSData dataWithContentsOfFile:filename];
        [picker addAttachmentData:myData mimeType:@"application/x-sqlite3" fileName:@"theocbase.sqlite"];
        
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:picker animated:NO completion:nil];
    }else{
        UIAlertController * message = [UIAlertController alertControllerWithTitle:@"Error"
                                                                          message:@"Unable to send mail in this device."
                                                                   preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK"
                                                       style:UIAlertActionStyleDefault
                                                       handler:nil];
        [message addAction:okButton];
        [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:message animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
#pragma unused(error)
    [controller dismissViewControllerAnimated:YES completion:nil];
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Result: canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Result: saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Result: sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Result: failed");
            break;
        default:
            NSLog(@"Result: not sent");
            break;
    }
}
@end

@interface DocPicker : UIDocumentPickerViewController <UIDocumentPickerDelegate>
{
}
@end

@implementation DocPicker
- (void) viewDidLoad
{
    self.delegate = self;
    [super viewDidLoad];
}

- (void) documentPicker:(UIDocumentPickerViewController *) controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls
{
    Q_UNUSED(controller);    
    if (urls.count > 0) {
        NSURL *fileURL = urls[0];        
        qDebug() << "Original file: " << fileURL.path;
                
        [fileURL startAccessingSecurityScopedResource];
        QString tempFile = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/" + QString::fromNSString(fileURL.lastPathComponent);
        if (QFile::exists(tempFile))
            QFile::remove(tempFile);
        bool ok = QFile::copy(QString::fromNSString(fileURL.path), tempFile);
        if (ok) {
            qDebug() << "Copied file: " << tempFile;
            emit ShareUtils::getInstance()->setReceivedUrl(tempFile);
        }
        [fileURL stopAccessingSecurityScopedResource];
    }
}

- (void) documentPickerWasCancelled:(UIDocumentPickerViewController *) controller
{
    Q_UNUSED(controller);
    qDebug() << "doc picker cancelled";
}
@end

ShareUtils* ShareUtils::mInstance = nullptr;

ShareUtils::ShareUtils(QObject *parent) : QObject(parent)
{
	mInstance = this;
    QDesktopServices::setUrlHandler("file", this, "handleUrlReceived");
    QDesktopServices::setUrlHandler("theocbase", this, "handleUrlReceived");
}

ShareUtils* ShareUtils::getInstance()
{
	if (!mInstance)
		mInstance = new ShareUtils();
	return mInstance;
}

void ShareUtils::setReceivedUrl(QString path)
{
    if (mViewFilePath == path)
        return;
    mViewFilePath = path;
    emit receivedUrlChanged(path);
}

void ShareUtils::saveBackup()
{
    QString dbfilename = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/theocbase.sqlite";
    if (!QFile::exists(dbfilename)) {
        qWarning() << "Database file not found" << dbfilename;
        emit error("Database file not found: " + dbfilename);
        return;
    }

    QString appDataRoot = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).value(0);
    if (!QDir(appDataRoot).exists()) {
        if (!QDir("").mkpath(appDataRoot)) {
            qWarning() << "Failed to create app data directory";
            return;
        }
    }
    appDataRoot.append("/share");
    if (!QDir(appDataRoot).exists()) {
        if (!QDir("").mkpath(appDataRoot)) {
            qWarning() << "Failed to create directory" << appDataRoot;
            emit error("Failed to create directory: " + appDataRoot);
            return;
        }
    }

    QString tempFilePath = appDataRoot.append("/theocbase_backup.sqlite");
    if (QFile::exists(tempFilePath))
        QFile::remove(tempFilePath);
    if (!QFile::copy(dbfilename,tempFilePath)) {
        qWarning() << "Failed to copy temporary file. From: " << dbfilename << "To: " << tempFilePath;
        emit error("Failed to copy temporary file. From: " + dbfilename + " to: " + tempFilePath);
        return;
    }
    QFile(tempFilePath).setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser);

    QUrl url = QUrl::fromLocalFile(tempFilePath);
    NSMutableArray *sharingItems = [NSMutableArray new];
    [sharingItems addObject:url.toNSURL()];

    // Get the UIViewController
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];

    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [qtController presentViewController:activityController animated:YES completion:nil];
}

QString ShareUtils::receivedUrl()
{
	return mViewFilePath;
}

void ShareUtils::handleUrlReceived(const QUrl &url)
{
    if (url.toString().startsWith("theocbase")) {
        if (url.hasQuery()) {
            QUrlQuery query(url.query());
            if (query.hasQueryItem("code")) {
                emit closeLoginPage();
                emit dbcallback(query.queryItemValue("code"));
            }
        }
    } else if (url.scheme() == "file") {
        qDebug() << "File recived" << url.toString();
        setReceivedUrl(url.toString());
    }
}

void ShareUtils::sendMail()
{
    QString dbfilename = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/theocbase.sqlite";

    if (!QFile::exists(dbfilename)) {
        qWarning() << "Database file not found" << dbfilename;
        return;
    }

    MailerController *mailer = [[MailerController alloc] init];
    [mailer sendMailWithAttachment:dbfilename.toNSString()];
}

void ShareUtils::shareText(const QString text)
{
    NSString *stringText  = text.toNSString();
    NSArray<NSString*> *dataToShare = @[stringText];
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];

    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    if (iosutil::getDeviceName() == "ipad") {
        //activityController.modalPresentationStyle = UIModalPresentationFormSheet;
        activityController.preferredContentSize = CGSizeMake(500, 600);
        auto popover = activityController.popoverPresentationController;

        popover.sourceView = qtController.view;
        popover.sourceRect = CGRectMake(mPoint.x(), mPoint.y(), 0, 0);
    }
    activityController.excludedActivityTypes = @[UIActivityTypeAirDrop];
    [qtController presentViewController:activityController animated:YES completion:nil];
}

void ShareUtils::loginDropbox(const QUrl &url)
{
    QUrlQuery query(url.query());
    query.removeQueryItem("redirect_uri");
    query.addQueryItem("redirect_uri","theocbase://dropbox_callback");
    QUrl modifiedUrl(url);
    modifiedUrl.setQuery(query);
    qDebug() << "authorization request" << modifiedUrl;
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL: modifiedUrl.toNSURL()];
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];
    [qtController presentViewController:svc animated:YES completion:nil];
    connect(this,&ShareUtils::closeLoginPage,[=]() {
        qDebug() << "close controller";
        [svc dismissViewControllerAnimated:false completion:nil];
    });
}

QString ShareUtils::openFile(QString format)
{
    NSArray<NSString *> *UTIs;
    if (format == "epub")
        UTIs = @[@"org.idpf.epub-container"];
    else
        UTIs = @[@"public.data"];

    DocPicker *picker = [[DocPicker alloc] initWithDocumentTypes:UTIs inMode: UIDocumentPickerModeOpen];
    picker.delegate = picker;
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];
    if (iosutil::getDeviceName() == "ipad") {
        picker.modalPresentationStyle = UIModalPresentationFormSheet;
    } else {
        picker.modalPresentationStyle = UIModalPresentationPopover;
    }
    [qtController presentViewController:picker animated:YES completion:nil];
    return format;
}

bool ShareUtils::copyFile(QString sourcePath, QString destPath)
{
    const QUrl sourceUrl(sourcePath);
    if (sourceUrl.isLocalFile())
        sourcePath = sourceUrl.toLocalFile();
    const QUrl destUrl(destPath);
    if (destUrl.isLocalFile())
        destPath = destUrl.toLocalFile();

    QFile sourceFile(sourcePath);
    QFile destFile(destPath);
    if (destFile.exists())
        destFile.remove();
    bool copied = sourceFile.copy(destPath);
    return copied;
}

bool ShareUtils::deleteFile(QString file)
{
    const QUrl fileUrl(file);
    if (fileUrl.isLocalFile())
        file = fileUrl.toLocalFile();
    return QFile::remove(file);
}

