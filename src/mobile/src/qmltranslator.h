/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QMLTRANSLATOR_H
#define QMLTRANSLATOR_H

#include <QObject>
#include <QTranslator>
#include <QApplication>
#include "../sql_class.h"

class QmlTranslator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int currentIndex READ getCurrentIndex WRITE setCurrentIndex NOTIFY languageChanged)
    Q_PROPERTY(QString changed READ emptyString NOTIFY languageChanged)
public:
    explicit QmlTranslator(QObject *parent = 0);
    ~QmlTranslator();

    void initTranslator();
    Q_INVOKABLE void setTranslation(QString langCode);
    int getCurrentIndex();
    void setCurrentIndex(int index);

    Q_INVOKABLE QStringList getLanguages();
    Q_INVOKABLE QString getLanguageName();

    QString emptyString() const;

signals:
    void languageChanged();
public slots:
private:
    QList<QPair<int, QString>> mLanguages;
    int mLanguageId;
    QTranslator mTranslatorMobile;
    QTranslator mTranslatorDesktop;
};

#endif // QMLTRANSLATOR_H
