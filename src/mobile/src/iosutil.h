/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IOSUTIL_H
#define IOSUTIL_H

#include <QObject>
#include <QVariantMap>

class iosutil : public QObject
{
    Q_OBJECT
    
    Q_PROPERTY(int unsafeTopMargin READ unsafeTopMargin NOTIFY unsafeMarginsChanged FINAL)
    Q_PROPERTY(int unsafeBottomMargin READ unsafeBottomMargin NOTIFY unsafeMarginsChanged FINAL)
    Q_PROPERTY(int unsafeLeftMargin READ unsafeLeftMargin NOTIFY unsafeMarginsChanged FINAL)
    Q_PROPERTY(int unsafeRightMargin READ unsafeRightMargin NOTIFY unsafeMarginsChanged FINAL)

public:
    iosutil(QObject *parent = nullptr);
    static void initiOS();
    static QString getDeviceName();
    
    int unsafeTopMargin() const;
    int unsafeBottomMargin() const;
    int unsafeLeftMargin() const;
    int unsafeRightMargin() const;
    
    Q_INVOKABLE void orientationChanged(int orientation);

signals:
    void unsafeMarginsChanged();
    
private:
    static void setStatusBarColorLight();

    int mUnsafeTopMargin;
    int mUnsafeBottomMargin;
    int mUnsafeLeftMargin;
    int mUnsafeRightMargin;
};

#endif // IOSUTIL_H
