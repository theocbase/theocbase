SOURCES += \
    $$PWD/main.cpp \
    $$PWD/messagebox.cpp \
    $$PWD/printcontroller.cpp \
    $$PWD/qmltranslator.cpp \
    ../sync_cloud.cpp \
    ../sql_class.cpp \
    ../specialevent.cpp \
    ../cpersons.cpp \
    ../person.cpp \
    ../ccongregation.cpp \
    ../family.cpp \
    ../lmm_assignment.cpp \
    ../lmm_assignment_ex.cpp \
    ../lmm_meeting.cpp \
    ../lmm_schedule.cpp \
    ../assignmentInfo.cpp \
    ../lmm_assignmentcontoller.cpp \
    ../sqlcombo.cpp \
    ../cpublictalks.cpp \
    ../publicmeeting_controller.cpp \
    ../todo.cpp \
    ../historytable.cpp \
    ../csync.cpp \
    ../generatexml.cpp \
    ../cloud/cloud_controller.cpp \
    ../cloud/theocbaseoauth.cpp \
    ../cloud/dropbox.cpp \
    ../availability/tb_availability.cpp \
    ../availability/availabilitychecker.cpp \
    ../availability/midweekmeetingavailabilitychecker.cpp \
    ../availability/weekendmeetingavailabilitychecker.cpp \
    ../general.cpp \
    ../xml_reader.cpp \
    ../zipper.cpp \
    ../epub.cpp \
    ../jwpub.cpp \
    ../importlmmworkbook.cpp \
    ../wtimport.cpp \
    ../outgoingspeakersmodel.cpp \
    ../accesscontrol.cpp \
    ../zip/qzip.cpp \
    ../weekinfo.cpp  \
    ../printingconditionals.cpp \
    ../print/printdocument.cpp \
    ../print/printcombination.cpp \
    ../print/printhospitality.cpp \
    ../print/printmidweekschedule.cpp \
    ../print/printmidweekslip.cpp \
    ../print/printmidweekworksheet.cpp \
    ../print/printoutgoingschedule.cpp \
    ../print/printoutgoingassignment.cpp \
    ../print/printtalksofspeakerslist.cpp \
    ../print/printterritoryassignmentrecord.cpp \
    ../print/printterritorycard.cpp \
    ../print/printterritorymapcard.cpp \
    ../print/printweekendschedule.cpp \
    ../print/printweekendworksheet.cpp \
    ../slipscanner.cpp \
    ../tbstyle.cpp

HEADERS += \
    $$PWD/messagebox.h \
    $$PWD/printcontroller.h \
    $$PWD/publishers_model.h \
    $$PWD/qmltranslator.h \
    $$PWD/printutil.h \
    ../constants.h \
    ../sharedlib_global.h \
    ../sync_cloud.h \
    ../specialevent.h \
    ../sql_class.h \
    ../singleton.h \
    ../cpersons.h \
    ../person.h \
    ../ccongregation.h \
    ../family.h \
    ../lmm_assignment.h \
    ../lmm_assignment_ex.h \
    ../lmm_meeting.h \
    ../lmm_schedule.h \
    ../assignmentInfo.h \
    ../sortfilterproxymodel.h \
    ../lmm_assignmentcontoller.h \
    ../sqlcombo.h \
    ../cpublictalks.h \
    ../publicmeeting_controller.h \
    ../todo.h \
    ../historytable.h \
    ../csync.h \
    ../generatexml.h \
    ../cloud/cloud_controller.h \
    ../cloud/theocbaseoauth.h \
    ../cloud/dropbox.h \
    ../availability/tb_availability.h \
    ../availability/availabilitychecker.h \
    ../availability/midweekmeetingavailabilitychecker.h \
    ../availability/weekendmeetingavailabilitychecker.h \
    ../general.h \
    ../xml_reader.h \
    ../zipper.h \
    ../jwpub.h \
    ../epub.h \
    ../importlmmworkbook.h \
    ../wtimport.h \
    ../outgoingspeakersmodel.h \
    ../accesscontrol.h \
    ../zip/qzipreader_p.h \
    ../zip/qzipwriter_p.h \
    ../weekinfo.h \
    ../printingconditionals.h \
    ../print/printdocument.h \
    ../print/printcombination.h \
    ../print/printhospitality.h \
    ../print/printmidweekschedule.h \
    ../print/printmidweekslip.h \
    ../print/printmidweekworksheet.h \
    ../print/printoutgoingschedule.h \
    ../print/printoutgoingassignment.h \
    ../print/printtalksofspeakerslist.h \
    ../print/printterritoryassignmentrecord.h \
    ../print/printterritorycard.h \
    ../print/printterritorymapcard.h \
    ../print/printweekendschedule.h \
    ../print/printweekendworksheet.h \
    ../slipscanner.h \
    ../tbstyle.h

FORMS += \
    ../historytable.ui
ios {
HEADERS += \
    $$PWD/iosutil.h \
    $$PWD/shareutils.h
OBJECTIVE_SOURCES += \
    $$PWD/messagebox.mm \
    $$PWD/iosutil.mm \
    $$PWD/printutil_ios.mm \
    $$PWD/shareutils_ios.mm
SOURCES -= \
    $$PWD/messagebox.cpp
LIBS += -framework MessageUI \
    -framework SafariServices \
    -framework WebKit
}

android {
QT += androidextras
HEADERS += \
    $$PWD/shareutils.h
SOURCES += \
    $$PWD/shareutils_android.cpp \
    $$PWD/printutil_android.cpp
}

