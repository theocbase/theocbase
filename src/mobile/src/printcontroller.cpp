/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printcontroller.h"

PrintController::PrintController(QObject *parent)
    : QObject(parent)
{
    QDir templateDir;
#if defined(Q_OS_IOS)
    templateDir = QDir(QApplication::applicationDirPath() + "/Templates");
#elif defined(Q_OS_ANDROID)
    templateDir = QDir("assets:/Templates");
#else
    templateDir = QDir(QApplication::applicationDirPath() + "/Templates");)
#endif

    QStringList nameFilter;
    nameFilter << "*.htm*"               
               << "*.jp*"
               << "*.png";
    mTemplateList = templateDir.entryList(nameFilter);

    // Custom templates
    QString appDataLocation = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString customTemplatesLocation = appDataLocation + "/custom_templates";    

     QDir dir(customTemplatesLocation);
    if(!dir.exists()){
        dir.mkpath(customTemplatesLocation);
    }
    QStringList customTemplates = dir.entryList(nameFilter);
    for (QString filename : customTemplates) {
        mTemplateList.append(filename);
    }
    std::sort(mTemplateList.begin(), mTemplateList.end());

    currentTemplate = QSharedPointer<PrintDocument>(new PrintMidweekSchedule());

    QSettings settings;
    mFontSize = settings.value("print/fontsize", 0).toInt();   
}

QStringList PrintController::templateList()
{
    QStringList newList;
    auto templateData = currentTemplate->getTemplateData();
    if (templateData) {
        for (auto t : mTemplateList) {
            if (t.contains(templateData->fileFilter, Qt::CaseInsensitive) && (templateData->notFileFilter.isEmpty() || !t.contains(templateData->notFileFilter, Qt::CaseInsensitive))) {
                newList.append(t);
            }
        }
    }

    return newList;
}

TemplateData::TemplateTypes PrintController::templateType()
{
    return currentTemplate->getTemplateData()->getTemplateType();
}

void PrintController::setTemplateType(TemplateData::TemplateTypes type)
{
    QDate _fromDate = currentTemplate->getFromDate();
    QDate _toDate = currentTemplate->getToDate();
    auto currentType = currentTemplate->getTemplateData()->getTemplateType();
    switch (type) {
    default:
    case TemplateData::MidweekScheduleTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintMidweekSchedule());
        break;
    case TemplateData::MidweekWorksheetTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintMidweekWorksheet());
        break;
    case TemplateData::MidweekSlipTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintMidweekSlip());
        break;
    case TemplateData::PublicMeetingTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintWeekendSchedule());
        break;
    case TemplateData::PublicMeetingWorksheet:
        if (currentType != type)
            currentTemplate.reset(new PrintWeekendWorksheet());
        break;
    case TemplateData::OutgoingScheduleTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintOutgoingSchedule());
        break;
    case TemplateData::OutgoingSlipTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintOutgoingAssignment());
        break;
    case TemplateData::HospitalityTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintHospitality());
        break;
    case TemplateData::TalksOfSpeakersTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintTalksOfSpeakersList());
        break;
    case TemplateData::CombinationTemplate:
        if (currentType != type)
            currentTemplate.reset(new PrintCombination());
        break;
    }
    currentTemplate->setFromDate(_fromDate);
    currentTemplate->setToDate(_toDate);
    qDebug() << currentTemplate->getFromDate() << currentTemplate->getToDate();
    emit typeChanged();
}

QString PrintController::templateName()
{
    if (currentTemplate->getTemplateData())
        return currentTemplate->getTemplateData()->getTemplateName();
    else
        return "";
}

void PrintController::setTemplateName(QString name)
{
    currentTemplate->setTemplateName(name);
    emit templateNameChanged();
}

QDate PrintController::fromDate()
{
    return currentTemplate->getFromDate();
}

void PrintController::setFromDate(QDate d)
{
    if (currentTemplate->getFromDate() == d)
        return;
    currentTemplate->setFromDate(d);
    emit fromDateChanged();
}

QDate PrintController::toDate()
{
    return currentTemplate->getToDate();
}

void PrintController::setToDate(QDate d)
{
    if (currentTemplate->getToDate() == d)
        return;
    currentTemplate->setToDate(d);
    emit toDateChanged();
}

int PrintController::fontSize()
{
    return mFontSize;
}

void PrintController::setFontSize(int size)
{
    mFontSize = size;
    QSettings settings;
    settings.setValue("print/fontsize", mFontSize);
    emit fontSizeChanged();
}



bool PrintController::assistantSlip()
{
    return mAssistantSlip;
}

void PrintController::setAssistantSlip(bool slip)
{
    mAssistantSlip = slip;
}

bool PrintController::assignedOnly()
{
    return mAssignedOnly;
}

void PrintController::setAssignedOnly(bool only)
{
    mAssignedOnly = only;
}

void PrintController::initDate(QDate d)
{
    QDate _fromDate = d.addDays(1 - d.day());
    _fromDate = _fromDate.addDays(1 - _fromDate.dayOfWeek());
    if (_fromDate.month() != d.month())
        _fromDate = _fromDate.addDays(7);
    QDate _thruDate = d.addMonths(1);
    _thruDate = _thruDate.addDays(-_thruDate.day());
    _thruDate = _thruDate.addDays(1 - _thruDate.dayOfWeek());
    setFromDate(_fromDate);
    setToDate(_thruDate);    
}

void PrintController::print()
{
    if (!mPrintUtil) {
        mPrintUtil = QSharedPointer<PrintUtil>(new PrintUtil());
        connect(mPrintUtil.data(), &PrintUtil::pageReady, this, &PrintController::previewReady);
    }

    if (currentTemplate->getTemplateData()->getTemplateType() == TemplateData::MidweekSlipTemplate) {
        dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setOnlyAssigned(assignedOnly());
        dynamic_cast<PrintMidweekSlip *>(currentTemplate.data())->setPrintAssistant(assistantSlip());
    }


    QVariant filledTemplate = currentTemplate->fillTemplate();
    qDebug() << fontSize();
    if (filledTemplate.type() == QVariant::String)
        mPrintUtil->printHTML(filledTemplate.toString(), currentTemplate->getLayout(), fontSize());
    else if (filledTemplate.type() == QVariant::ByteArray) {
        emit previewReady();
        mPrintUtil->printPDF(filledTemplate.toByteArray(), currentTemplate->getLayout());
    }
}
