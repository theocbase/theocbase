/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0

Page {
    id: weekendMeetingEdit
    title: qsTr("Public Talk", "Page title")
    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }
    PublicMeetingController { id: controller }

    header: BaseToolbar {
        title: weekendMeetingEdit.title
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: {
                stackView.pop()
            }
        }
    }

    ScrollView {
        width: parent.width
        height:parent.height
        contentWidth: width
        clip: true

        ColumnLayout {
            id: layout
            width: weekendMeetingEdit.availableWidth

            // Theme
            RowTitle {
                id: themeTitle
                text: qsTr("Theme")
            }

            ComboBoxTable {
                id: comboTheme
                Layout.fillWidth: true
                currentText: (meeting.themeNumber > 0 ? meeting.themeNumber + " " : "") + meeting.theme
                col1Name: themeTitle.text
                onBeforeMenuShown: {
                    model = controller.themeList(meeting.speaker ? meeting.speaker.id : 0)                    
                    console.log("model loaded")
                    //column2.resizeToContents()
                    //console.log("col2 resized")
                }
                onRowSelected: {
                    console.log(id)
                    meeting.setTheme(id)
                    meeting.save()
                }
            }

            // Congregations
            RowTitle {
                id: congTitle
                text: qsTr("Congregation")
            }
            ComboBoxTable {
                id: comboCongregation
                Layout.fillWidth: true
                currentText: meeting.speaker ? meeting.speaker.congregationName : ""
                col1Name: congTitle.text
                onBeforeMenuShown: {
                    model = controller.congregationList()
                    //column2.resizeToContents()
                }
                onRowSelected: {
                    console.log(id)
                    //myMeeting.setTheme(id)
                }
            }

            RowTitle {
                id: speakerTitle
                text: qsTr("Speaker")
            }
            ComboBoxTable {
                id: comboSpeaker
                Layout.fillWidth: true
                currentText: meeting.speaker ? meeting.speaker.fullname : ""
                currentId: meeting.speaker ? meeting.speaker.id : -1
                col1Name: speakerTitle.text
                onBeforeMenuShown: {
                    var congregationId = 0
                    if (comboCongregation.currentText != "") {
                        if (typeof(comboCongregation.model) === "undefined")
                            comboCongregation.model = controller.congregationList()
                        congregationId = comboCongregation.model.get(comboCongregation.model.find(comboCongregation.currentText,2)).id
                    }
                    var talkId = 0
                    if (comboTheme.currentText != "") {
                        if (typeof (comboTheme.model) === "undefined")
                            comboTheme.model = controller.themeList(0)
                        talkId = comboTheme.model.get(comboTheme.model.find(meeting.themeNumber + " " + meeting.theme,2)).id
                    }
                    model = controller.speakerList(talkId, congregationId)
                    //column2.resizeToContents()
                }
                onRowSelected: {
                    console.log(id)
                    var speaker = CPersons.getPerson(id)
                    meeting.speaker = speaker
                    meeting.save()
                }
            }

            RowTitle {
                text: qsTr("Mobile")
            }
            TextField {
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: meeting.speaker ? meeting.speaker.mobile : ""
                enabled: meeting.speaker
                onEditingFinished: {
                    if (meeting.speaker) {
                        meeting.speaker.mobile = text
                        meeting.speaker.update()
                    }
                }
                leftPadding: 10*dpi
                rightPadding: 10*dpi
            }

            RowTitle {
                text: qsTr("Phone")
            }
            TextField {
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: meeting.speaker ? meeting.speaker.phone : ""
                enabled: meeting.speaker
                onEditingFinished: {
                    if (meeting.speaker) {
                        meeting.speaker.phone = text
                        meeting.speaker.update()
                    }
                }
                leftPadding: 10*dpi
                rightPadding: 10*dpi
            }

            RowTitle {
                text: qsTr("Email")
            }
            TextField {
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: meeting.speaker ? meeting.speaker.email : ""
                enabled: meeting.speaker
                onEditingFinished: {
                    if (meeting.speaker) {
                        meeting.speaker.email = text
                        meeting.speaker.update()
                    }
                }
                leftPadding: 10*dpi
                rightPadding: 10*dpi
            }

            RowTitle {
                text: qsTr("Info")
            }
            TextArea {
                Layout.fillWidth: true
                selectByMouse: true
                wrapMode: Text.WordWrap
                text: meeting.speaker ? meeting.speaker.info : ""
                enabled: meeting.speaker
                onEditingFinished: {
                    if (meeting.speaker) {
                        meeting.speaker.info = text
                        meeting.speaker.update()
                    }
                }
                leftPadding: 10*dpi
                rightPadding: 10*dpi
            }

            // Hospitality
            RowTitle {
                id: hostTitle
                text: qsTr("Host")
            }

            ComboBoxTable {
                id: comboHost
                col1Name: hostTitle.text
                Layout.fillWidth: true
                currentText: meeting.hospitalityHost ? meeting.hospitalityHost.fullname : ""
                currentId: meeting.hospitalityHost ? meeting.hospitalityHost.id : -1
                onBeforeMenuShown: {
                    model = controller.hospitalityList();
                    var congregationId = 0
                }
                onRowSelected: {
                    meeting.hospitalityHost = CPersons.getPerson(id)
                    meeting.save()
                }
            }

            Item {
                Layout.fillHeight: true
            }
        }
    }
}

