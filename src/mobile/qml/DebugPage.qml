/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.3
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtQuick.Dialogs 1.2
import net.theocbase.mobile 1.0

Page {
    id: pageRect

    header: BaseToolbar {
        title: "Debug"
        componentLeft: ToolButton {
            icon.source: "qrc:///back.svg"
            icon.color: "white"
            onClicked: { stackView.pop() }
        }
    }

    Flickable {
        id: flickable1
        anchors.fill: parent
        flickableDirection: Flickable.VerticalFlick
        contentHeight: layout.height
        contentWidth: parent.width

        ColumnLayout {
            id: layout
            spacing: 0
            width: parent.width
            Label {
                text: "DEBUG INFORMATION"
                Layout.topMargin: 20
                Layout.leftMargin: 10*dpi
                color: "grey"
            }
            RowText {
                firstrow: true
                title: "Operating System"
                text: Qt.platform.os
            }
            RowText {
                title: "Screen size inch"
                text: app_info.screensize.toFixed(1)
            }

            RowText {
                title: "Screen size"
                text: Screen.width.toString() + "x" + Screen.height.toString()
            }
            RowText {
                title: "Screen available size"
                text: Screen.desktopAvailableWidth.toString() + "x" + Screen.desktopAvailableHeight.toString()
            }
            //            RowSwitch {
            //                title: "TEST"
            //                checkbox: false
            //                onClicked: {
            //                    textareatest.visible = !textareatest.visible
            //                }
            //            }

            RowText {
                title: "Pixel density"
                //visible: textareatest.visible
                text: Screen.pixelDensity.toString()
            }
            RowText {
                title: "Device Pixel Ratio"
                text: app_info.devicepixelratio.toString()
            }
            RowText {
                title: "Default font pixel size"
                text: app_info.fontpixels.toString()
            }
            RowText {
                title: "Default font point size"
                text: app_info.fontsizepoint.toString()
            }
            RowText {
                title: "Font size normal"
                text: app_info.fontsize.toString()
            }

            RowText {
                title: "Font size small"
                text: app_info.fontsizeSmall.toString()
            }

            RowText {
                title: "UI Languages"
                text: app_info.uilanguages
            }

            //            RowText {
            //                id: textPhone
            //                title: "test"
            //                arrow: true
            //            }
            //            RowText {
            //                id: backgroundtest
            //                title: "Background test"
            //                text: ccloud.backgroundDebug
            //            }


            ToolButton {
                text: "Share backup file"
                Layout.fillWidth: true
                implicitHeight: 40*dpi
                onClicked: {
                    shareUtils.saveBackup()
                }
            }

            ToolButton {
                text: "Import epub file"
                Layout.fillWidth: true
                onClicked: {
                    shareUtils.openFile("epub")
                }
            }

            //            TextArea {
            //                id: textareatest
            //                height: 150*dpi
            //                anchors.left: parent.left
            //                anchors.right: parent.right
            //                z: 2
            //            }

            //            MouseArea {
            //                anchors.fill: parent
            //                propagateComposedEvents: true
            //                onClicked: { console.log("mouse area click"); pageRect.forceActiveFocus(); mouse.accepted = false; }
            //            }
        }
    }
}

