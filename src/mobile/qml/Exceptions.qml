/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.calendar 1.0
import net.theocbase.mobile 1.0
import net.theocbase 1.0
import "js/moment.js" as Moment

Popup {
    id: exceptionPopup
    parent: Overlay.overlay
    x: (parent.width - width) / 2
    y: (parent.height - height) / 2
    modal: true
    implicitWidth:  350 * _dpi
    implicitHeight: 500 * _dpi
    closePolicy: Popup.NoAutoClose

    padding: 0
    property int midweek: 4    
    property int weekend: 6
    property int defaultMidweek: 4
    property int defaultWeekend: 7
    property int exStart: 0
    property date exStartDate: moment(weekDate).add(exStart, 'days').toDate()
    property int exEnd: 0
    property date exEndDate: moment(weekDate).add(exEnd, 'days').toDate()
    property date weekDate
    property bool canEditExceptions: accessControl.user && (accessControl.user.hasPermission(PermissionRule.CanEditSpecialEvents))
    property bool canChangeMidweekMeeting: true
    property bool canChangeWeekendMeeting: true

    property real _dpi: typeof dpi == "undefined" ? 1 : dpi
    property var specialEventRules: SpecialEvents.specialEventRules
    property SpecialEventRule specialEventRule: SpecialEvents.findSpecialEventRule(SpecialEvent.None)
    property string exceptionText: ""
    property WeekInfo weekInfo

    onOpened: {
        defaultMidweek = congCtrl.getMeetingDay(MeetingType.MidweekMeeting)
        defaultWeekend = congCtrl.getMeetingDay(MeetingType.WeekendMeeting)

        specialEventRule = SpecialEvents.findSpecialEventRule(weekInfo.specialEvent)
        midweek = weekInfo.midweekDay
        weekend = weekInfo.weekendDay
        exStart = isNaN(weekInfo.exceptionStart) ? 0 : moment(weekInfo.exceptionStart).diff(moment(weekInfo.date), 'day')
        exEnd = isNaN(weekInfo.exceptionEnd) ? 0 : moment(weekInfo.exceptionEnd).diff(moment(weekInfo.date), 'day')
        exceptionText = weekInfo.exceptionText
    }
    onSpecialEventRuleChanged: {
        exStart = specialEventRule.startDay - 1;
        if (exStart < 0)
            exStart = 0;
        exEnd = specialEventRule.endDay - 1;
        midweek = defaultMidweek;
        weekend = defaultWeekend;

        if (specialEventRule.isCircuitOverseersVisit)
            midweek = 2;
        if (specialEventRule.isConvention) {
            exStart = specialEventRule.isSingleDay ? 6 : 4
            exEnd = 6;
        }
        if (specialEventRule.exclusivity === SpecialEventExclusivity.NoOtherMeetingInSameWeek) {
            midweek = 0;
            weekend = 0;
        }
        if (specialEventRule.exclusivity === SpecialEventExclusivity.NoOtherMeetingInSamePartOfTheWeek) {
            if (exStart < 5)
                midweek = 0;
            else
                weekend = 0;
        }
        exceptionText = "";
        canChangeMidweekMeeting = midweek !== 0 && canEditExceptions;
        canChangeWeekendMeeting = weekend !== 0 && canEditExceptions;
    }
    onExStartChanged: {
        if (specialEventRule.exclusivity === SpecialEventExclusivity.NoOtherMeetingInSamePartOfTheWeek) {
            exEnd = exStart
            if (exStart < 5) {
                midweek = 0
                weekend = defaultWeekend
            } else {
                midweek = defaultMidweek
                weekend = 0
            }
        }
    }

    WeekInfo {
        date: weekDate
    }

    function saveChanges() {
        weekInfo.specialEvent = specialEventRule.id
        if (specialEventRule.id !== SpecialEvent.None) {
            weekInfo.exceptionText = (specialEventRule && specialEventRule.canChangeDescription) ? exceptionText : ""
            weekInfo.exceptionStart = exStartDate
            weekInfo.exceptionEnd = exEndDate
            weekInfo.midweekDay = midweek
            weekInfo.weekendDay = weekend
        }
        weekInfo.saveChanges()
    }

    CongregationCtrl { id: congCtrl }

    ToolBar {
        id: toolbar
        width: parent.width
        RowLayout {
            anchors.fill: parent
            ToolButton {
                icon.source: stack.depth > 1 ? "qrc:/back.svg" : "qrc:/cancel.svg"
                icon.color: "white"
                onClicked: {
                    // close but do not save
                    if (stack.depth > 1)
                        stack.pop()
                    else
                        exceptionPopup.close()
                }
            }
            Label {
                text: qsTr("Week starting %1").arg(weekDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat))
                Layout.fillWidth: true
                color: "white"
            }

            ToolButton {
                icon.source: "qrc:/done.svg"
                icon.color: "white"
                visible: stack.depth === 1
                enabled: canEditExceptions
                onClicked: {
                    // close and save the changes
                    saveChanges()
                    exceptionPopup.close()
                    reloadMainpage()
                }
            }
        }
    }

    StackView {
        id: stack
        anchors.fill: parent
        anchors.topMargin: toolbar.height
        initialItem: Page {
            id: exceptionPage
            height: 300
            ColumnLayout {
                spacing: 0
                anchors.fill: parent
                RowText {
                    arrow: true
                    title: specialEventRule ? specialEventRule.description : ""
                    text: (specialEventRule && specialEventRule.canChangeDescription) ? exceptionText : ""
                    enabled: canEditExceptions
                    onClicked: stack.push(exceptionTypesPage)
                }
                RowText {
                    id: textExceptionDates
                    arrow: true
                    title: exStartDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat) +
                           (exEnd > exStart ?
                                " - " + exEndDate.toLocaleDateString(Qt.locale(), Locale.ShortFormat)
                              : "")

                    visible: specialEventRule && (specialEventRule.id !== SpecialEvent.None)
                    enabled: canEditExceptions
                    onClicked: stack.push(exceptionDatesPage)
                }

                RowTitle {
                    text: qsTr("Meeting Days")
                    uppercase: true
                }

                RowText {
                    id: textMidweekMeeting
                    arrow: true
                    title: qsTr("Midweek Meeting")
                    text: midweek > 0 ? Qt.locale().dayName(midweek) : "-"
                    enabled: canChangeMidweekMeeting
                    onClicked: stack.push(midweekPage)
                }
                RowText {
                    id: textWeekendMeeting
                    arrow: true
                    title: qsTr("Weekend Meeting")
                    text: weekend  > 0 ? Qt.locale().dayName(weekend) : "-"
                    enabled: canChangeWeekendMeeting
                    onClicked: stack.push(weekendPage)                    
                }
                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }

    Component {
        id: exceptionTypesPage
        Page {
            implicitWidth: 300
            implicitHeight: 400
            ScrollView {
                width: parent.width
                height: parent.height
                contentWidth: availableWidth
                ColumnLayout {
                    width: stack.availableWidth - 20 * _dpi
                    x: 10 * _dpi
                    Repeater {
                        id: rpt
                        model: specialEventRules
                        RadioButton {
                            text: specialEventRules[index].description
                            Layout.fillWidth: true
                            checked: specialEventRule && (specialEventRule.id === specialEventRules[index].id)
                            onCheckedChanged: specialEventRule = specialEventRules[index]
                        }
                    }
                    TextField {
                        text: exceptionText
                        Layout.fillWidth: true
                        visible: specialEventRule && specialEventRule.canChangeDescription
                        onTextEdited: exceptionText = text
                    }
                }
            }
        }
    }

    Component {
        id: midweekPage
        Page {
            implicitWidth: 300
            implicitHeight: 400
            ExceptionDaySelector {
                selectedDay: midweek
                showWeekDays: true
                showWeekend: false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                onSelectedDayChanged: midweek = selectedDay
            }
        }
    }
    Component {
        id: weekendPage
        Page {
            implicitWidth: 300
            implicitHeight: 400
            ExceptionDaySelector {
                selectedDay: weekend
                showWeekDays: false
                showWeekend: true
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                onSelectedDayChanged: weekend = selectedDay
            }
        }
    }

    Component {
        id: exceptionDatesPage
        Page {
            implicitWidth: 300
            implicitHeight: 400
            padding: 10 * _dpi
            ExceptionDateSelector {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                weekDate: exceptionPopup.weekDate
                start: exStart
                end: exEnd
                onStartChanged: exStart = start
                onEndChanged: exEnd = end
            }
        }
    }
}
