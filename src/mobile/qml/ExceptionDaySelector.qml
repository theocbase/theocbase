/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    property bool showWeekDays: true
    property bool showWeekend: true
    property int selectedDay: 0
    property real _dpi: typeof dpi == "undefined" ? 1 : dpi

    RowLayout {
        height: 50 * _dpi
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10 * _dpi
        Repeater {
            model: 8
            Rectangle {
                id: rectangle
                width: 40 * _dpi
                height: width
                color: index == selectedDay ? "black" : "white"
                radius: width / 2
                border.width: 1
                visible: index == 0 || index <= 5 && showWeekDays || index >= 6 && showWeekend
                Label {
                    color: index == selectedDay ? "white" : "black"
                    text: index == 0 ? "-" : Qt.locale().dayName(index, Locale.ShortFormat)
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter

                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: selectedDay = index
                }
            }
        }
    }

}
