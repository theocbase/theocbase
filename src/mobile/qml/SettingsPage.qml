/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Page {
    id: settingsPage

    property DBAccount account: ccloud.authentication.getAccountInfo()
    property bool isDarkMode: systemPalette.windowText.hsvValue > systemPalette.window.hsvValue

    header: BaseToolbar {
        title: qsTr("Settings", "application settings") + qmlTranslator.changed
        componentRight: ToolButton {
            icon.source: ccloud.syncState === Cloud.Synchronized ?
                             "qrc:/cloud_done.svg" : ccloud.syncState === Cloud.Download ?
                                 "qrc:/cloud_download.svg" : "qrc:/cloud_upload.svg"
            icon.color: "white"
            onClicked: { syncpage.runSync() }
        }
    }

    SystemPalette { id: systemPalette }

    Connections {
        target: ccloud
        function onLoggedChanged(ok) {
            if (ok) {
                textEmail.text = account.email
                textName.text = account.name
                textSyncFile.text = account.syncFile
            }
        }
    }

    ScrollView {
        id: scroll
        width: parent.width
        height: parent.height
        contentWidth: availableWidth
        anchors.bottomMargin: 10        

        ColumnLayout {
            id: layout
            spacing: 0
            width: settingsPage.availableWidth

            Item { height: 10*dpi }
            RowTitle {
                text: qsTr("Last synchronized: %1").arg(ccloud.lastSyncTime.toLocaleString(Qt.locale(), Locale.ShortFormat)) +
                      " " + (ccloud.syncState === Cloud.Upload ? "↑" : ccloud.syncState === Cloud.Download ? "↓" : (ccloud.syncState === Cloud.Both ? "⇅" : "✓")) +
                      qmlTranslator.changed
                font.pointSize: app_info.fontsizeSmall
                horizontalAlignment: Text.AlignHCenter
                uppercase: false
            }

            Item { height: 10*dpi }
            Image {
                Layout.fillWidth: true
                Layout.margins: 5*dpi
                fillMode: Image.PreserveAspectFit
                source: "qrc:/Dropbox.svg"
                ColorOverlay {
                    anchors.fill: parent
                    visible: isDarkMode
                    source: parent
                    color: "white"
                }
            }
            RowText {
                title: ccloud.logged ? qsTr("Logout") : qsTr("Login") + qmlTranslator.changed
                arrow: true
                firstrow: true
                onClicked: {
                    ccloud.logout();
                    ccloud.login();
                    textEmail.text = ""
                    textName.text = ""
                    textSyncFile.text = ""
                }
            }

            RowText {
                id: textEmail
                title: qsTr("Email")
                text: account.email
            }
            RowText {
                id: textName
                title: qsTr("Name")
                text: account.name
            }
            RowText {
                id: textSyncFile
                title: "Syncfile"
                text: account.syncFile
                arrow: true
                onClicked: {
                    var page = stackView.clearAndPush(Qt.resolvedUrl("DropboxList.qml"));
                    page.onSyncFileChanged.connect(function(path){
                        text = path
                        ccloud.clearDatabase();
                        syncpage.runSync();
                    })
                }
            }

            RowTitle { text: qsTr("Info") + qmlTranslator.changed }

            RowText {
                id: textVersion
                title: qsTr("Version") + qmlTranslator.changed
                text: app_info.version
                firstrow: true
            }
            RowText {
                id: textHelp
                title: qsTr("TheocBase Homepage") + qmlTranslator.changed
                arrow: true
                onClicked: {
                    Qt.openUrlExternally("http://theocbase.net")
                }
            }
            RowText {
                title: qsTr("Feedback") + qmlTranslator.changed
                arrow: true
                onClicked: {
                    Qt.openUrlExternally("http://www.theocbase.net/support-forum.html")
                }
            }

            RowText {
                title: "Debug"
                arrow: true
                visible: true
                onClicked: {
                    stackView.clearAndPush(Qt.resolvedUrl("DebugPage.qml"));
                }
            }

            RowTitle { text: qsTr("Schedule") + qmlTranslator.changed }

            ButtonGroup { id: groupTimeDuration }
            RowSwitch {
                id: switchShowTime
                title: qsTr("Show Time") + qmlTranslator.changed
                checkbox: true
                firstrow: true
                checked: settings_ui.showTime
                exclusiveGroup: groupTimeDuration
                onCheckedChanged: {
                    settings_ui.showTime = checked
                    reloadMainpage()
                }
            }
            RowSwitch {
                title: qsTr("Show Duration") + qmlTranslator.changed
                checkbox: true
                checked: !switchShowTime.checked
                exclusiveGroup: groupTimeDuration
            }
            RowSwitch {
                title: qsTr("Show Song Titles") + qmlTranslator.changed
                checkbox: false
                checked: settings_ui.showSongTitles
                onCheckedChanged: {
                    settings_ui.showSongTitles = checked
                    reloadMainpage()
                }
            }

            RowTitle { text: qsTr("User Interface") }
            RowText {
                title: qsTr("Language")
                arrow: true
                text: qmlTranslator.getLanguageName()
                firstrow: true
                onClicked: {
                    if (Qt.inputMethod.visible) Qt.inputMethod.hide()
                    var languagePage = stackView.clearAndPush(Qt.resolvedUrl("SelectionListPage.qml"),
                                                              {model: qmlTranslator.getLanguages(), pageTitle: title,
                                                                  selectedRow: qmlTranslator.currentIndex})
                    languagePage.onActiveRowChanged.connect(function(index){
                        qmlTranslator.currentIndex = index
                        text = qmlTranslator.getLanguageName()
                        message.show("TheocBase", "You need to restart application for the changes to take effect")
                    })
                }
                MsgBox { id: message }
            }

            RowTitle { text: qsTr("Printing") }
            RowText {
                title: qsTr("Custom Templates")
                arrow: true
                firstrow: true
                onClicked: {
                    var page = stackView.clearAndPush(Qt.resolvedUrl("PrintTemplates.qml"));
                }
            }
        }
    }

}
