<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Pavadinimas</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Data</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Kitos pamokos neskirti</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Pakartoti pamoką</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Negaliojantys duomenys</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Pridėti laiką</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Medžiaga</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Mokinys</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Padėjėjas/a</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultatas</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Užbaigtas</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Savanoris</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Pasirinkti savanorį</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Dabartinė pamoka</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Atlikti pratimai</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Kita pamoka</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pasirinkti kitą pamoką</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Laikas</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Pastabos</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Mokyklos duomenys</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pradėti laiką</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Sustabdyti laiką</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Situacija</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Pasirinkti situaciją</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>Week starting %1</source>
        <translation>Savaitė nuo %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Sueigų dienos</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Šokiadienio sueiga</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Savaitgalio sueiga</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Medžiagos šaltinis</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Kalbėtojas</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Skaitovas</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Pastabos</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalės</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Užrašai</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>Chairman</source>
        <translation>Programos vedėjas</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Patarėjas</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Giesmė %1 ir malda</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Giesmė</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Skaitovas</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Įžanginiai žodžiai</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Malda</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Įkelti tvarkaraštį...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>KS</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>2S</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>3S</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Baigiamieji žodžiai</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Šiokiadienio sueiga</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Pakartoti pamoką</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pradėti skaičiuoti laiką</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Sustabdyti laiką</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Pridėti laiką?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Medžiagos šaltinis</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Mokinys</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultatas</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Atlikta</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Laikas</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Dabartinė pamoka</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Pratimas atliktas</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Kita pamoka</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pasirinkti kitą pamoką</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Pastabos</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Padėjėjas/a</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Savanoris</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Pradėti skaičiuoti laiką</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Padejejas/a neturėtų būti kažkas iš priešingos lyties.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Pamoka</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalės</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Paslink žemyn, kad atsinaujintų...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Paleisk, kad atsinaujintų...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Vartotojo vardas</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Slaptažodis</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prisijungti</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Vartotojo vardas arba el. pašto adresas</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El. paštas</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Sukurti paskyrą</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Atkurti slaptažodį</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>El. pašto adresas nerastas</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Pamiršau slaptažodį</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Prisijungimo puslapis</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Savaitė nuo %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Užrašai</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Išvykę kalbėtojai</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 kalbėtojas išvykęs šį savaitgalį</numerusform>
            <numerusform>%1 kalbėtojai išvykę šį savaitgalį</numerusform>
            <numerusform>%1 kalbėtojų išvykę šį savaitgalį</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Šį savaitgalį išvykusių kalbėtojų nėra </translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Spausdinimo pasirinkimai</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Tvarkaraštis</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Darbo lapai</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Išvažavęs kalbėtojo tvarkaraštis</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Išvykstančių kalbėtojų užduotys</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Spausdinti</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Pasirinktinis šablonų aplankas</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Giesmė ir malda</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Giesmė %1 ir malda</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>Viešoji kalba</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>SARGYBOS BOKŠTO STUDIJA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Giesmė %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Skaitovas</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Įkelti SB...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Savaitgalio sueiga</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Šiokiadienio sueiga</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Bendruomenė</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Kalbėtojas</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobilusis telefonas</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefonas</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El.paštas</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Kas kviečia pietų</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Viešoji kalba</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Vardas</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Pavardė</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brolis</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sesė</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Patarnautojas</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Šeima</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Šeimos narys su</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontakto domenys</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefonas</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El. paštas</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Visose salėse</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Tik pagrindinėje salėje</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Tik antrojoje salėje</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Šeimos galva</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Padėjėjas/a</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktyvus</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Programos vedėjas</translation>
    </message>
    <message>
        <source>Treasures from God&#39;s Word</source>
        <translation>Iš Dievo žodžio lobyno</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Biblijos skaitymas</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Pirmas pokalbis</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Pakartotinis aplankymas</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblijos studijos</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Esame krikščionys. Kalbos</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Bendruomenės Biblijos studijos</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Bendr. Biblijos studijos skaitovas</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Naujas skelbėjas</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Malda</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Kas kviečia kalbėtoją pietų</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobilusis telefonas</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Šokiadienio sueiga</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Kalba</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Savaitgalio sueiga</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Sargybos bokšto vadovas</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Sargybos bokšto skaitovas</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Skelbėjai</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting for Field Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God&#39;s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Apply Yourself To The Field Ministry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living As Christians</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God’s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Conversation Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living as Christians Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial Invitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Video Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant in student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Demonstration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hospitality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opening prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Life and Ministry Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service Talk</source>
        <translation>Tarnybos kalba</translation>
    </message>
    <message>
        <source>Support tasks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Pasirinkimų sąrašas</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Nustatymai</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Atsijungti</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informacija</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versija</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBasės interneto svetainė</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Atsilėpimai</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Paskutinį kartą sinchronizuota: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Tvarkaraštis</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Rodyti laiką</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Rodyti trukmę</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Vartotojo sąsaja</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Kalba</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prisijungti</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El.paštas</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Vardas</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Rodyti giesmės pavadinimą</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Spausdinimas</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Pasirinktinis šablonų aplankas</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sinchronizuoja...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Programos vedėjas</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Giesmė</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Savaitgalio sueigos programos vedėjas</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Giesmė</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Sargybos bokšto giesmė</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Malda</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Sargybos bokšto numeris</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Straipsnio nr</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Skaitovas</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Laiko tekmė</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Savaičių skaičius prieš pasirinktą datą</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Savaičių skaičius po pasirinktos datos</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>savaitės</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Savaitės, kurios papilkėja po užduoties</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Pakeitimai rasti programoje ir TheocBase talpykloje (%1rows). Ar norite išsaugoti įrenginyje esančius pakeitimus?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Debesijos duomenys ištrinti. Jūsų įrenginyje esantys duomenys bus pakeisti. Tęsti?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nepaskirti kitos pamokos</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nenustatyta</translation>
    </message>
</context></TS>