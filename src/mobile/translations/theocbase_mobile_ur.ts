<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ur">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>نام</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>تاریخ</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>مو‌ضو‌ع</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student</source>
        <translation>طالبِ‌علم</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>مددگار</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>رضاکار</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>موجودہ نکتہ</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>اگلا نکتہ</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>School Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>پس منظر</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>مو‌ضو‌ع</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>کنڈکٹر</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>مقرر</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>Chairman</source>
        <translation>میزبان</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>مشیر</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>گیت 1% او‌ر دُعا</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>گیت</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>کنڈکٹر</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>اِبتدائی کلمات</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>دُعا</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>اِختتامی کلمات</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>مو‌ضو‌ع</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student</source>
        <translation>طالبِ‌علم</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>موجودہ نکتہ</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>اگلا نکتہ</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>مددگار</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>رضاکار</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>نکتہ</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation>لاگ اِن</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>گیت او‌ر دُعا</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>گیت 1% او‌ر دُعا</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>عوامی تقریر</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>مینارِنگہبانی کا مطالعہ</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>گیت 1%</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>کنڈکٹر</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>مو‌ضو‌ع</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>کلیسیا</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>مقرر</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>عوامی تقریر</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brother</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sister</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Servant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>صرف اِضافی جماعتیں</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>مددگار</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>میزبان</translation>
    </message>
    <message>
        <source>Treasures from God&#39;s Word</source>
        <translation>پاک کلام سے سنہری باتیں</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>تلاو‌ت</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>پہلی ملاقات</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>و‌اپسی ملاقات</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>بائبل کو‌رس</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>بائبل کا کلیسیائی مطالعہ</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>نئے مبشر</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>دُعا</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>تقریر</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>مینارِنگہبانی کے مطالعہ کا کنڈکٹر</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>سنہری باتو‌ں</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>بات‌چیت اور و‌یڈیو</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>مبشر</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation>مسیحی زندگی او‌ر خدمت و‌الے اِجلاس</translation>
    </message>
    <message>
        <source>Meeting for Field Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God&#39;s Word</source>
        <translation>پاک کلام سے سنہری باتیں</translation>
    </message>
    <message>
        <source>Apply Yourself To The Field Ministry</source>
        <translation>شاگرد بنانے کی تربیت</translation>
    </message>
    <message>
        <source>Living As Christians</source>
        <translation>مسیحیو‌ں کے طو‌ر پر زندگی</translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation>گیت او‌ر دُعا</translation>
    </message>
    <message>
        <source>Treasures From God’s Word</source>
        <translation>پاک کلام سے سنہری باتیں</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>سنہری باتو‌ں</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>تلاو‌ت</translation>
    </message>
    <message>
        <source>Sample Conversation Video</source>
        <translation>بات‌چیت کے لیے تجاو‌یز و‌یڈیو</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>پہلی ملاقات</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>و‌اپسی ملاقات</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>بائبل کو‌رس</translation>
    </message>
    <message>
        <source>Living as Christians Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>بائبل کا کلیسیائی مطالعہ</translation>
    </message>
    <message>
        <source>Memorial Invitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Video Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>عوامی تقریر</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>مینارِنگہبانی کا مطالعہ</translation>
    </message>
    <message>
        <source>Meeting parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant in student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>میزبان</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>مشیر</translation>
    </message>
    <message>
        <source>Study conductor</source>
        <translation>مطالعہ کا کنڈکٹر</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>تقریر</translation>
    </message>
    <message>
        <source>Discussion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Demonstration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>مددگار</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>دُعا</translation>
    </message>
    <message>
        <source>Hospitality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opening prayer</source>
        <translation>اِبتدائی دُعا</translation>
    </message>
    <message>
        <source>Concluding prayer</source>
        <translation>اِختتامی دُعا</translation>
    </message>
    <message>
        <source>Life and Ministry Meeting Chairman</source>
        <translation>زندگی او‌ر خدمت اِجلاس کا میزبان</translation>
    </message>
    <message>
        <source>Public Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service Talk</source>
        <translation>خدمتی تقریر</translation>
    </message>
    <message>
        <source>Support tasks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>سیٹنگز</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>لاگ آوَٹ</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation>زبان</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>لاگ اِن</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>میزبان</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>گیت</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>گیت</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>دُعا</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>مو‌ضو‌ع</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>کنڈکٹر</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not set</source>
        <translation type="unfinished"></translation>
    </message>
</context></TS>