<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>שם</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>תאריך</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>אל תקצה את השיעור הבא</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>השאר בשיער הנוכחי</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>נתונים לא חוקיים</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>להוסיף את העיתוי?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>תלמיד</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>תוצאה</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>הושלם</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>מתנדב</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>בחר מתנדב</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>השיער הנוכחי</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>תרגילים הושלמו</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>השיעור הבא</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>בחר בשיעורר הבא</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>פרטי בית הספר</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>התחל שעון עצר</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>עצור שעון עצר</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>הגדרה</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>בחר הגדרה</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>Week starting %1</source>
        <translation>שבוע החל מ-%1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>ימי אסיפה</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>קריו</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>פרטים</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>יועצ</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>שיר%1 ותפילה</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>שִׁיר</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>לוח הזמנים לייבא ...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>MH</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>השאר שעויר הנוכחי</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>התחל שעון עצר</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>עצור שעון עצר</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>להוסיף את העיתוי?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>תלמיד</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>תוצאה</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>הושלם</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>השיעורר הנוכחי</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>תרגילים הושלמו</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>השיעור הבא</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>בחר בשיעור הבא</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>עוזר</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>מתנדב</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>שעוני עצירה</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>העוזר לא אמור להיות מישהו מהמין השני.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>נקודת לימוד</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>פרטים</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>משוך לרענן ...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>שחרר כדי לרענן...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>שם משתמש</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>התחברות</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>שם משתמש או דוא&quot;ל</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>דוא&quot;ל</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>צור חשבון</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>לאפס את הסיסמה</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>כתובת הדוא&quot;ל לא נמצאה!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>שכחת ססמא</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>דף כניסה</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>שבוע החל מ-%1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>נואמים יוצאים</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 נואם משם בסוף השבוע</numerusform>
            <numerusform>%1 נואמים משם בסוף השבוע</numerusform>
            <numerusform>%1 נואמים משם בסוף השבוע</numerusform>
            <numerusform>%1 נואמים משם בסוף השבוע</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>בסופ&quot;ש זה אין נואמים יוצאים</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>אפשרויות הדפסה</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>לוח זמנים</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>גיליון פעילות</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>לוח נואמים יוצאים</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>משימות נואמים יוצאים</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>לוח שיחות ותכנית מארחים</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>נאומים של נואמים</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>הדפס</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>שלוב</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>פתקי משימות</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>תבנית</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>פתקי משימות לעוזר</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>הדפסה הוקצתה בלבד</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>גודל טקסט</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>תבניות מותאמות אישית</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>שיר ותפילה</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>שיר%1 ותפילה</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>שיר%1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>ייבא WT ...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>אסיפה סוף השבוע</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>דוא&quot;ל</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>מארח</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>הרצאה פומבי</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>אח</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>אחות</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>מבשר</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>משפחה</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>בן משפחה מקושר ל</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>פרטי התקשרות</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>מספר טלפון</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>אימייל</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>כל כיתות</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>כיתה ראשית בלבד</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>רק כיתות משניות</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>ראש משפחה</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>פעיל</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <source>Treasures from God&#39;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>שיחה ראשונה</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>ביקור חוזר</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>שיעור מקרא</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>לחיות כמשיחיים נאומים</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>קרין שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>מבשר חדש</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>מארח לנואמים פומבים</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>נאום</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>קרין שיעור המצפה</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>פנינים רוחניות</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>דיון עם וידאו</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>מבשרים</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <source>Meeting for Field Service</source>
        <translation>מפגש לשירות</translation>
    </message>
    <message>
        <source>Treasures From God&#39;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <source>Apply Yourself To The Field Ministry</source>
        <translation>התייעל בשירות השדה</translation>
    </message>
    <message>
        <source>Living As Christians</source>
        <translation>לחיות כמשיחיים</translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation>שיר ותפילה</translation>
    </message>
    <message>
        <source>Treasures From God’s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>פנינים רוחניות</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <source>Sample Conversation Video</source>
        <translation>סרטוני השיחות לדוגמה</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>שיחה ראשונה</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>ביקור חוזר</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>שיעור מקרא</translation>
    </message>
    <message>
        <source>Living as Christians Talk</source>
        <translation>נאום של לחיות כמשיחיים</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <source>Memorial Invitation</source>
        <translation>הזמנה לערב הזיכרון</translation>
    </message>
    <message>
        <source>Other Video Part</source>
        <translation>חלק וידאו אחר</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>הרצאוה פומבי</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <source>Meeting parts</source>
        <translation>חלקי אסיפה</translation>
    </message>
    <message>
        <source>Other assignments</source>
        <translation>מסימות אחרות</translation>
    </message>
    <message>
        <source>Student parts</source>
        <translation>חלקי תלמידים</translation>
    </message>
    <message>
        <source>Assistant in student parts</source>
        <translation>עוזר בחלקי תלמידים</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>יועצ</translation>
    </message>
    <message>
        <source>Study conductor</source>
        <translation>מדריך שיעור</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>נאום</translation>
    </message>
    <message>
        <source>Discussion</source>
        <translation>דִיון</translation>
    </message>
    <message>
        <source>Demonstration</source>
        <translation>הדגמה</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <source>Hospitality</source>
        <translation>הכנסת אורחים</translation>
    </message>
    <message>
        <source>Opening prayer</source>
        <translation>תפילת פתיחה</translation>
    </message>
    <message>
        <source>Concluding prayer</source>
        <translation>תפילת סיום</translation>
    </message>
    <message>
        <source>Life and Ministry Meeting Chairman</source>
        <translation>יושב־ראש אסיפת אורח חיינו ושירותנו</translation>
    </message>
    <message>
        <source>Public Meeting Chairman</source>
        <translation>יושב ראוש אסיפה הפומבית</translation>
    </message>
    <message>
        <source>Service Talk</source>
        <translation>נאום שירות</translation>
    </message>
    <message>
        <source>Support tasks</source>
        <translation>משימות תמיכה</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>רשימת בחירות</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>הגדרות</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>להתנתק</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>גרסה</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>דף הבית של TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>משוב</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>סונכרן לאחרונה: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>לוח זמנים</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>הצג זמן</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>משך ההצגה</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>ממשק משתמש</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>שפה</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>התחברות</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>דוא&quot;ל</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>הצג כותרות שירים</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>הדפסה</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>תבניות מותאמות אישית</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>לסנכרן ...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>יו&quot;ר אסיפת סוף השבוע</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>שיר המצפה</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>בהוצאת המצפה</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>מאמר</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>מדריך</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>ציר זמן</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>מספר שבועות לפני התאריך שנבחר</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>מספר שבועות לאחר התאריך שנבחר</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>שבועות</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>מספר שבועות לאפור לאחר משימה</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>את אותם שינויים ניתן למצוא הן באופן מקומי והן בענן (%1 שורות). האם אתה רוצה לשמור על השינויים המקומיים?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>נתוני הענן אופסו. הנתונים המקומיים שלך יוחלפו. להמשיך?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>אל תקצה את השעורר הבא</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>לא מוכן</translation>
    </message>
</context></TS>