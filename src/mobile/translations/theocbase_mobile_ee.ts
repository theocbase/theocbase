<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ee">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Ŋkɔ</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Ŋkeke</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tanya</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Nyawo Tsoƒe</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Sukuvi</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Kpeɖeŋutɔ</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ewɔe Nyuie</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntɛr</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Tsɔ Voluntɛr</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nusɔsrɔ̃</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ewu Dɔdeasi nu</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Nusɔsrɔ̃ Evelia</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Tsɔ Nusɔsrɔ̃ Evelia</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Game</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nuŋlɔɖiwo</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Suku Details</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 ƒe Kwasiɖa</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Kpekpe Ŋkekewo</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kwasiɖa Domedome Kpekpe</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kwasiɖa Nuwuwu Kpekpe</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tanya</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Nyawo Tsoƒe</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dzikpɔla</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Nuƒola</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Nuxlẽla</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nuŋlɔɖiwo</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Nuŋlɔɖiwo</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>Chairman</source>
        <translation>Zimenɔla</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Aɖaŋuɖola</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Hadzidzi %1 Kple Gbedodoɖa</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Hadzidzi</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dzikpɔla</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Nuxlẽla</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Ŋgɔdonyawo</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Gbedodoɖa</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>He Ðoɖowɔɖia vɛ...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>FA</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>X1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>X2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Nyataƒonyawo</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kwasiɖa Domedome Kpekpe</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Àwɔ nusɔsrɔ̃ sia ake</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tanya</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Nyawo Tsoƒe</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Sukuvi</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Teƒeɖoɖo</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Ewu enu</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Game</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Nusɔsrɔ̃</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Ewɔ Dɔdeasiawo</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Nusɔsrɔ̃ si akplɔe ɖo</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Tsɔ nusɔsrɔ̃ si akplɔe ɖo</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nuŋlɔɖiwo</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Kpeɖeŋutɔ</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Voluntɛr</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Kpeɖeŋutɔa manye amesi ƒe vidzinu to vovo o.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Nusɔsrɔ̃</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>He ne yeyeavo nava...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Ðe asi ɖa ne yeyeavo nava...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Ezazãŋkɔ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Ge Ðe Eme</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Ezazãŋkɔ alo Email</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Ʋu Account Yeye</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Trɔ Password</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Míekpɔ Wò Email Dedzesi o!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Meŋlɔ Nye Password be</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Ge Ðe Eme</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>%1 ƒe Kwasiɖa</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Nuŋlɔɖiwo</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ðoɖowɔɖi</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Tae</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kwasiɖa Domedome Kpekpe</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kwasiɖa Nuwuwu Kpekpe</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Ŋuŋɔŋlɔawo Lolome</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Hadzidzi Kple Gbedodoɖa</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Hadzidzi %1 Kple Gbedodoɖa</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DUTOƑO NUƑO</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>GBETAKPƆXƆ NUSƆSRƆ̃</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Hadzidzi %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Dzikpɔla</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Nuxlẽla</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Tsɔ Gbetakpɔxɔa...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kwasiɖa Nuwuwu Kpekpe</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kwasiɖa Domedome Kpekpe</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tanya</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Hame</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Nuƒola</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Asi telefon</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Phone</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Amedzrowɔla</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Dutoƒo Nuƒo</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Dzigbeŋkɔ</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Ŋkɔ</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Nɔviŋutsu</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Nɔvinyɔnu</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Ƒome</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Zimenɔla</translation>
    </message>
    <message>
        <source>Treasures from God&#39;s Word</source>
        <translation>Nu Xɔasi Siwo Le Mawu Ƒe Nyaa Me</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Biblia Xexlẽ</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Yiyi Zi Gbãtɔ</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Trɔtrɔyi</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblia Nusɔsrɔ̃</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Kristotɔwo Ƒe Agbenɔnɔ Nuƒowo</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Hamea Ƒe Biblia Nusɔsrɔ̃</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Hame Biblia Nusɔsrɔ̃ ƒe Nuxlẽla</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Gbeƒaɖela yeye</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Gbedodoɖa</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Amedzrowɔla na Dutoƒonuƒonalawo</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Asi telefon</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Kwasiɖa Domedome Kpekpe</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Nuƒo</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Kwasiɖa Nuwuwu Kpekpe</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Gbetakpɔxɔ Dzikpɔla</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Gbetakpɔxɔ Xlẽla</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Mawu Ƒe Nyaa Me Kesinɔnuwo</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Numedzodzro kple video</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Gbeƒãɖelawo</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting for Field Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God&#39;s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Apply Yourself To The Field Ministry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living As Christians</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God’s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Conversation Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living as Christians Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial Invitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Video Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant in student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Demonstration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hospitality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opening prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Life and Ministry Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Support tasks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Nuwotrɔƒe</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Ðoɖowɔɖi</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Gbegbɔgblɔ</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Ŋkɔ</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Le etam</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Zimenɔla</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Hadzidzi</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Zimenɔnɔ na Kwasiɖa Nuwuwu Kpekpea</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Hadzidzi</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not set</source>
        <translation type="unfinished"></translation>
    </message>
</context></TS>