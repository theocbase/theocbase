<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ht">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Pa asiyen pwochen leson an pou travay</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Travay sou menm leson an</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Done sa a pa valab</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ajoute kantite tan an?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Referans</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elèv</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun k ap fè l avè w</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Leson an fin travay</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Chwazi yon volontè</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Leson w ap travay</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Egzèsis yo fèt</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen Leson</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Chwazi pwochen leson an</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantite tan</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Enfòmasyon sou lekòl la</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Demare kwonomèt la</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Kanpe kwonomèt la</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Kad</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Chwazi yon kad</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>Week starting %1</source>
        <translation>Semèn ki kòmanse %1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Jou Reyinyon yo</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reyinyon Lasemèn nan</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reyinyon Fen Semèn nan</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Referans</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektè</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detay</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>Chairman</source>
        <translation>Prezidan</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Konseye</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 ak Priyè</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektè</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Pawòl Entwodiksyon</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Enpòte Pwogram nan...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>SS1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>SS2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Pawòl konklizyon</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Travay sou menm leson an</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Demare kwonomèt la</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Kanpe kwonomèt la</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ajoute kantite tan an?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Referans</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elèv</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Fin travay leson an</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantite tan</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Leson w ap travay</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Egzèsis yo fèt</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen Leson</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Chwazi pwochen leson an</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Moun k ap fè l avè w</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Kwonomèt</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Moun k ap fè patisipasyon an ak elèv la pa dwe yon moun ki pa menm sèks avè l</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Pwen leson</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detay</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Rale l desann pou w mete l ajou</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Lage l pou w mete l ajou...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Non itilizatè a</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Modpas</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konekte</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Non itlizatè a oswa Imel li</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Kreye yon Kont</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reyaktive Modpas ou</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Adrès Imel sa a pa disponib!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Bliye Modpas</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Paj Koneksyon</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Semèn ki kòmanse %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ORATÈ K AP SÒTI AL BAY DISKOU NAN LÒT KONGREGASYON</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>Gen %1 oratè k ap sòti al bay diskou nan lòt kongregasyon wikenn sa a</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Pa gen oratè k ap sòti al bay diskou nan lòt kongregasyon wikenn sa a</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Opsyon pou Enprime</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Pwogram</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Fèy travay</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Kalandriye oratè k ap sòti yo</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Asiyasyon pou Oratè k ap Sòti yo</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Lis ak kalandriye moun k ap bay ospitalite</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Diskou Oratè yo ap bay</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Enprime</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reyinyon lasemèn nan</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reyinyon fen semèn nan</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Konbine ansanm</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Fich Asiyasyon</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Modèl</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Fich asiyasyon pou patnè yo</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Enprime moun ki asiyen yo sèlman</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Gwosè tèks la</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Modèl ki ka modifye</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Kantik ak Priyè</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 ak Priyè</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISKOU PIBLIK</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ETID TOUDEGAD</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektè</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Enpòte TG...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Kongregasyon</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Selilè</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefòn</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Moun k ap bay ospitalite</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Diskou Piblik</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Siyati</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Frè</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sè</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Frè nome</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Fanmi</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Fanmi li fè pati</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Enfòmasyon sou moun nan</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefòn</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Tout Sal yo</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Sal Prensipal Sèlman</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Sal Segondè Sèlman</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Chèf Fanmi</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun k ap fè l avè w</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prezidan</translation>
    </message>
    <message>
        <source>Treasures from God&#39;s Word</source>
        <translation>Trezò ki nan pawòl Bondye a</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Lekti Labib</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Premye Rankont</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Nouvèl Vizit</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Etid Biblik</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Diskou nan pati Lavi nou antanke kretyen</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Etid Biblik Kongregasyon an</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lektè Etid Biblik Kongregasyon an</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nouvo pwoklamatè</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Moun k ap bay oratè vizitè yo ospitalite</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Selilè</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reyinyon Lasemèn nan</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Diskou</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reyinyon Fen Semèn nan</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Kondiktè Etid Toudegad la</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lektè Etid Toudegad la</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Pwoklamatè yo</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting for Field Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God&#39;s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Apply Yourself To The Field Ministry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living As Christians</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Treasures From God’s Word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sample Conversation Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Living as Christians Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Memorial Invitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Video Part</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Meeting parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other assignments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant in student parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Study conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discussion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Demonstration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hospitality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Opening prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Concluding prayer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Life and Ministry Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Meeting Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Support tasks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Chwazi Lis</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Kad yo</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Dekonekte</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Vèsyon</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Paj Akèy TheocBase la</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Ide itilizatè yo</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Dènye senkwonizasyon an te fèt nan dat: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Pwogram</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Afiche Lè a</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Afiche kantite tan an</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Anviwònman Itilizatè a</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lang</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konekte</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Afiche Tit Kantik yo</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Enprime</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Modèl ki ka modifye</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Ap senkwonize...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Prezidan</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Prezidan Reyinyon Fen Semèn nan</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Kantik Toudegad</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Nimewo Toudegad la</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Kantite Atik</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lektè</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Kantite tan ki pase</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Kantite semèn anvan dat ki chwazi a</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Kantite semèn ki vini aprè dat ki chwazi a</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semèn yo</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Kantite semèn pou mete an gri aprè yon asiyasyon</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Chanjman ki fèt la yo parèt tou nan &quot;cloud&quot; (%1 ranje). Èske w vle anrejistre chanjman ki fèt la yo?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Enfòmasyon ki nan cloud yo efase. Y ap ranplase enfòmasyon ki nan òdinatè w la. Kontinye?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Pa asiyen pwochen leson an pou travay</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Sa a pa defini</translation>
    </message>
</context></TS>