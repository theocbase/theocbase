<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="uk">
<context>
    <name>ComboBoxTable</name>
    <message>
        <source>Name</source>
        <comment>Column name</comment>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <source>Date</source>
        <comment>Column name</comment>
        <translation>Дата</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Не призначати наступний урок</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Залишити поточний урок</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Невірні дані</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Додати хронометраж</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Джерело</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Учень</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помічник</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Доброволець</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Обрати добровольця</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Поточний урок</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Вправи виконано</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Наступний урок</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Обрати наступний урок</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Хронометраж</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Примітки</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Школа: докладніше</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Запустити секундомір</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Зупинити секундомір</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Ситуація</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Обери ситуацію</translation>
    </message>
</context>
<context>
    <name>Exceptions</name>
    <message>
        <source>Week starting %1</source>
        <translation>Початок тижня% 1</translation>
    </message>
    <message>
        <source>Meeting Days</source>
        <translation>Дні зібрання</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Зібрання серед тижня</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Зібрання у вихідні</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Джерело</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Промовець</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Читає</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Нотатки</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Деталі</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Нотатки</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>Chairman</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Дає поради</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Пісня %1 i молитва</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Пісня</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Читає</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Вступні слова</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Розклад імпорту...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>Г.З.</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>Д.К.1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>Д.К.2</translation>
    </message>
    <message>
        <source>Concluding Comments</source>
        <translation>Кінцеві слова</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Зустрічі серед тижня</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Залишити поточний урок</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Запустити секундомір</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Зупинити секундомір</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Додати хронометраж</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Джерело</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Учень</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Результат</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Час</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Поточний урок</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Вправи виконано</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Наступний урок</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Обрати наступний урок</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Нотатки</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Помічник</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Доброволець</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Cекундомір</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Помічник не повинен бути протилежної статі.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Пункт завдання</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Деталі</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Потягніть, щоб оновити...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Відпустіть, щоб оновити...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Username</source>
        <translation>Ім&apos;я користувача</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Логін</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Логін або E-mail</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Створити обліковий запис</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Скидання паролю</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Email адресу не знайдено!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Забув пароль</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Сторінка входу</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Week starting %1</source>
        <translation>Тиждень від %1</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Примітки</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>РЕКОМЕНДОВАНІ ПРОМОВЦІ</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>% 1 промовець на вихідних</numerusform>
            <numerusform>% 1 промовець на вихідних</numerusform>
            <numerusform>% 1 промовець на вихідних</numerusform>
            <numerusform>% 1 промовець на вихідних</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>На цих вихідних немає промовців</translation>
    </message>
</context>
<context>
    <name>PrintOptions</name>
    <message>
        <source>Print Options</source>
        <translation>Параметри друку</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Розклад</translation>
    </message>
    <message>
        <source>Worksheets</source>
        <translation>Робочі аркуші</translation>
    </message>
    <message>
        <source>Outgoing Speakers Schedule</source>
        <translation>Розклад рекомендованих промовців</translation>
    </message>
    <message>
        <source>Outgoing Speakers Assignments</source>
        <translation>Призначенні Рекомендовані Промовці</translation>
    </message>
    <message>
        <source>Call List and Hospitality Schedule</source>
        <translation>Список дзвінків та розклад гостинності</translation>
    </message>
    <message>
        <source>Talks of Speakers</source>
        <translation>Промови промовців</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Роздрукувати</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Зібрання серед тижня</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Зібрання у вихідні</translation>
    </message>
    <message>
        <source>Combination</source>
        <comment>Print template</comment>
        <translation>Комбінація</translation>
    </message>
    <message>
        <source>Assignment Slips</source>
        <translation>Комбінація призначень</translation>
    </message>
    <message>
        <source>Template</source>
        <comment>Print template</comment>
        <translation>Шаблон</translation>
    </message>
    <message>
        <source>Assignment Slips for Assistant</source>
        <translation>Комбінація призначень для помічника</translation>
    </message>
    <message>
        <source>Print Assigned Only</source>
        <comment>Assignment slip printing</comment>
        <translation>Друк призначений лише</translation>
    </message>
    <message>
        <source>Text size</source>
        <translation>Розмір тексту</translation>
    </message>
</context>
<context>
    <name>PrintTemplates</name>
    <message>
        <source>Custom Templates</source>
        <translation>Спеціальні шаблони</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Пісня і молитва</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Пісня %1 і молитва</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>ПУБЛІЧНА ПРОМОВА</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ВИВЧЕННЯ ВАРТОВОЇ БАШТИ</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Пісня %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Читає</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Імпортувати В.Б...</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Зібрання у вихідні</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Зібрання серед тижня</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Збір</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Промовець</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Мобільний</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Електронна пошта</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Гостинність</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Публічна Промова</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Прізвище</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Брат</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Сестра</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Служитель</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Сім&apos;я</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Член сім&apos;ї пов&apos;язаний з</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Контактна інформація</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Усі класи</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Тільки головний клас</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Тільки допоміжні класи</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Голова сім&apos;ї</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помічник</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Інфо</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Активний</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Treasures from God&#39;s Word</source>
        <translation>Скарби з Божого Слова</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Читання Біблії</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Перша розмова</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Повторні відвідини</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Біблійне вивчення</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Християнське життя Пункти</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Вивчення Біблії в зборі</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Читає на вивченні Біблії у зборі</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Новий вісник</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Виявляє гостинність до запрошених промовців.</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Мобільний</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Зібрання серед тижня</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Промова</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Зібрання у вихідні</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Вивчення Вартової Башти</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Читає на Вартової Башті</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Духовні перлини</translation>
    </message>
    <message>
        <source>Discussion with Video</source>
        <translation>Обговорення з відео</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Вісники</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Weekend Meeting</source>
        <translation>Зустріч у вихідні</translation>
    </message>
    <message>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation>Зібрання Наше Християнське Життя та Служіння</translation>
    </message>
    <message>
        <source>Meeting for Field Service</source>
        <translation>Зустріч для проповідницького служіння</translation>
    </message>
    <message>
        <source>Treasures From God&#39;s Word</source>
        <translation>СКАРБИ З БОЖОГО СЛОВА</translation>
    </message>
    <message>
        <source>Apply Yourself To The Field Ministry</source>
        <translation>ВДОСКОНАЛЮЙМО СВОЄ СЛУЖІННЯ</translation>
    </message>
    <message>
        <source>Living As Christians</source>
        <translation>ХРИСТИЯНСЬКЕ ЖИТТЯ</translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation>Пісня і молитва</translation>
    </message>
    <message>
        <source>Treasures From God’s Word</source>
        <translation>СКАРБИ З БОЖОГО СЛОВА</translation>
    </message>
    <message>
        <source>Spiritual Gems</source>
        <translation>Духовні перлини</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Читання Біблії</translation>
    </message>
    <message>
        <source>Sample Conversation Video</source>
        <translation>Зразок відео розмови</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Перша розмова.</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Повторні відвідини.</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Біблійне вивчення</translation>
    </message>
    <message>
        <source>Living as Christians Talk</source>
        <translation>ХРИСТИЯНСЬКЕ ЖИТТЯ</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Вивчення Біблії у зборі</translation>
    </message>
    <message>
        <source>Memorial Invitation</source>
        <translation>Запрошення на Спомин</translation>
    </message>
    <message>
        <source>Other Video Part</source>
        <translation>Інша частина відео</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>ПУБЛІЧНА ПРОМОВА</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Вивчення ВАРТОВЇ БАШТИ</translation>
    </message>
    <message>
        <source>Meeting parts</source>
        <translation>Частини зустрічі</translation>
    </message>
    <message>
        <source>Other assignments</source>
        <translation>Інші завдання</translation>
    </message>
    <message>
        <source>Student parts</source>
        <translation>Учнівські завдання</translation>
    </message>
    <message>
        <source>Assistant in student parts</source>
        <translation>Помичник по учнівськім завданням</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Головуючий</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Даючий поради</translation>
    </message>
    <message>
        <source>Study conductor</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Промова</translation>
    </message>
    <message>
        <source>Discussion</source>
        <translation>Обговорення</translation>
    </message>
    <message>
        <source>Demonstration</source>
        <translation>Демонстрація</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Помічник</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Читає</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
    <message>
        <source>Hospitality</source>
        <translation>Гостинність</translation>
    </message>
    <message>
        <source>Opening prayer</source>
        <translation>Вступна молитва</translation>
    </message>
    <message>
        <source>Concluding prayer</source>
        <translation>Заключна молитва</translation>
    </message>
    <message>
        <source>Life and Ministry Meeting Chairman</source>
        <translation>Ведучий Християнського життя та служіння</translation>
    </message>
    <message>
        <source>Public Meeting Chairman</source>
        <translation>Ведучий Публічної зустрічі</translation>
    </message>
    <message>
        <source>Service Talk</source>
        <translation>Публічна Пррмова</translation>
    </message>
    <message>
        <source>Support tasks</source>
        <translation>Допоміжні завдання</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Список вибору</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Налаштування</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Вийти</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Інформація</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Версія</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation> Домашня сторінка TheocBase</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Зворотній зв&apos;язок</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Остання синхронізація: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Розклад</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Показати час</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Показати тривалість</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Інтерфейс користувача</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Мова</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Вхід</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Ім&apos;я</translation>
    </message>
    <message>
        <source>Show Song Titles</source>
        <translation>Показати заголовки пісень</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation>Друк</translation>
    </message>
    <message>
        <source>Custom Templates</source>
        <translation>Користувацькі шаблони</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Синхронізація...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Пісня</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Ведучий зібрання у вихідні</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Пісня</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Пісня Вартової Башти</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Молитва</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Випуск Вартової Башти</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Стаття</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Ведучий</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Читає</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Графік</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Кількість тижнів перед обраною датою</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Кількість тижнів після обраної дати</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>тижнів</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Кількість тижнів до сірого після виконання завдання</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Ті ж зміни можуть бути знайдені і локально і в хмарі (%1 рядок). Хочете зберегти локальні зміни?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Хмарні дані скинуто. Ваші локальні дані буде замінено. Продовжити?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Не призначати наступний урок</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Не встановлено</translation>
    </message>
</context></TS>