TEMPLATE = app
QT += qml quick widgets sql network svg networkauth
TARGET = TheocBase
CONFIG += c++11
CONFIG -= bitcode

VERSION = 2022.04.1
DEFINES += APP_VERSION=\\\"$$VERSION\\\"
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050F00

# C++ files
include(src/src.pri)

# Default rules for deployment.
include(deployment.pri)
# SMTP Client
include(../smtp/smtpclient.pri)

SOURCES +=
HEADERS +=

RESOURCES += \
    qml/qml.qrc \
    ../database.qrc \
    icons/icons.qrc \
    translations/translations_mobile.qrc \
    ../translations/translations.qrc \
    ../fonts/fonts.qrc

# var, prepend, append
defineReplace(prependAll) {
    for(a,$$1):result += $$2$${a}$$3
    return($$result)
}

lupdate_only {
    TR_EXCLUDE += $$PWD/../accesscontrol.*
    SOURCES = qml/*.qml \
        src/school_detail.cpp
}

# Supported languages
LANGUAGES = af bg cpe_011 cs da de ee en es et el fi fr cpf_gcf gn he hi hr ht hu hy it ka lt my_MM ne nl no pl pt pt_BR pa pa_PK ro ru sk sl sr sv sw ta th_TH tl uk zh xh

# Available translations
TRANSLATIONS = $$prependAll(LANGUAGES, $$PWD/translations/theocbase_mobile_, .ts)

# Additional import path used to resolve QML modules in Qt Creator's code model
# QML_IMPORT_PATH =
