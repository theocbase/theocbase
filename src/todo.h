/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TODO_H
#define TODO_H

#include <QList>
#include <QStandardItemModel>
#include "sql_class.h"
#include "ccongregation.h"
#include "cpersons.h"
#include "cpublictalks.h"
#include "general.h"

class todo
{
public:
    static QList<todo *> getList();
    static bool addScheduledTalks(int speakerId, bool removed);
    static bool addDiscontinuedTalks(const QDate startDate, const int themeId);

    todo(bool isIncoming);
    todo(int id, bool isIncoming);

    bool isIncoming() const;

    int id() const;
    void setId(int newId);

    QDate date() const;
    void setDate(const QDate &newDate);
    int timeRange(int minTimeRange = 1, int maxTimeRange = 5, double timeRangeUnit = 28) const;

    QString speakerFullName() const;
    void setSpeakerFullName(const QString &newSpeakerFullName);
    person *speaker();

    QString congregationName() const;
    void setCongregationName(const QString &newCongregationName);
    int congregationId() const;

    const QString &themeAndNumber() const;
    void setThemeAndNumber(const QString &newThemeAndNumber);
    const cpttheme &theme() const;

    QString notes() const;
    void setNotes(const QString &newNotes);

    void deleteme();
    void save();
    QString moveToSchedule();

private:
    sql_class *sql;
    int m_id;
    bool m_isIncoming;
    QDate m_date;
    QString m_speakerFullName;
    QSharedPointer<person> m_speaker;
    QString m_congregationName;
    int m_congregationId;
    QString m_themeAndNumber;
    cpttheme m_theme;
    QString m_notes;
};

#endif // TODO_H
