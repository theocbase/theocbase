/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PRINTDOCUMENT_H
#define PRINTDOCUMENT_H

#include <QApplication>
#include <QObject>
#include <QTextCodec>
#include <QDate>
#include <QPageLayout>
#include <QPdfWriter>
#include <QPainter>
#include <QBuffer>
#include <QSettings>
#include <QStandardPaths>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>
#include <QTextDocumentFragment>

#include "../sql_class.h"
#include "../printingconditionals.h"
#include "../ccongregation.h"
#include "../lmm_meeting.h"
#include "../cpublictalks.h"
#include "../slipscanner.h"

class TemplateData
{
    Q_GADGET
public:
    enum TemplateTypes {
        NoTemplate,
        MidweekScheduleTemplate,
        MidweekWorksheetTemplate,
        MidweekSlipTemplate,
        CombinationTemplate,
        PublicMeetingTemplate,
        PublicMeetingWorksheet,
        OutgoingScheduleTemplate,
        OutgoingSlipTemplate,
        TerritoryAssignmentRecordTemplate,
        TerritoryCardTemplate,
        TerritoryMapCardTemplate,
        HospitalityTemplate,
        TalksOfSpeakersTemplate
    };
    Q_ENUM(TemplateTypes)

    TemplateData(TemplateTypes templateType, QString settingName, QString currentTemplate, QString fileFilter, QString notFileFilter = "");
    TemplateTypes getTemplateType();
    QString getTemplateName();
    void setTemplateName(QString name);

    // returns error message, if any
    QString setPaperSize(QString size = "");

    void loadMeta(QString html);
    QString metaValue(QString name, QString defaultValue = "");
    QString optionValue(QString name, QString defaultValue = "");
    void setOptionValue(QString name, QString value, bool okAdd = false, bool persist = false);

    sql_class *sql;
    QString fileFilter;
    QString notFileFilter;
    QString paperSize;
    QPageSize::PageSizeId standardPaperSize;
    QSizeF customPaperSize;
    QPageSize::Unit customPaperUnit;

private:
    TemplateTypes templateType;
    QString settingName;
    QString templateName;
    bool metaLoaded;
    QHash<QString, QString> meta;
    QHash<QString, QString> options;
};

class PrintDocument : virtual public QObject
{
    Q_DECLARE_TR_FUNCTIONS(PrintDocument)
public:
    PrintDocument();
    ~PrintDocument();
    void setTemplateName(QString templateName);
    virtual QVariant fillTemplate();

    QPageLayout getLayout();
    TemplateData *getTemplateData();

    QDate getFromDate();
    void setFromDate(QDate d);

    QDate getToDate();
    void setToDate(QDate d);

    bool qrCodeSupported();

protected:
    class weekInfo
    {
    public:
        weekInfo(QDate week, QDate mtgDate)
            : week(week), mtgDate(mtgDate) { }
        QDate week;
        QDate mtgDate;
        int weeks = 0;
        int fullweeknum = 0;
        int weeknum = 0;
        bool newmonth = false;
        bool endmonth = false;
        bool isFirst = false;
        bool isLast = false;
    };

    class extraOptions
    {
    public:
        extraOptions(QString name, QString tagName);

        QString name;
        QString tagName;
    };

    QString getTemplate(bool returnFileName = false);
    QString initLayout(QString html);
    void fillWeekList(int offsetFromMonday);
    void fillWeekList_SetWeeks(weekInfo *previous);
    QPair<QString, QString> templateGetSection(QString context, const QString start, const QString end, const QString classname);

    QString fillCommonItems(QString context, bool nomeeting, SpecialEventRule *specialEventRule);
    QString fillDate(QString context, ccongregation::meetings meetingtype, QString VarName);
    QString fillExceptionDate(QString context, QString tag);
    QString replaceDateTag(QString context, QString tag, QDate date, QString wrapperText = "%1");
    QString replaceDateRangeTag(QString context, QString tag, QDate startDate, QDate endDate);
    QString replaceTimeTag(QString context, QString tag, QTime time);
    QString replaceIntTag(QString context, QString tag, int number);
    QString addCssPageBreak();

    QSharedPointer<TemplateData> currentTemplateData;
    ccongregation::congregation myCongregation;
    ccongregation c;
    printingconditionals ifthen;
    sql_class *sql;
    weekInfo *currentWeek;
    QList<weekInfo *> weekList;
    int midweekmeetingday;
    int publicmeetingday;
    QPageLayout::Orientation po;
    QMarginsF pmlist;
    qreal textSizeFactorySlips;
    bool qrCodeSupport = false;
    QPageLayout layout;

private:
    void handleCurrentTemplateMeta(QString html);

    QMarginsF getMargins();
    QPageLayout::Orientation getOrientation();

    QDate fromDate;
    QDate thruDate;
};

#endif // PRINTDOCUMENT_H
