/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printterritorymapcard.h"

PrintTerritoryMapCard::PrintTerritoryMapCard()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::TerritoryMapCardTemplate, "territoryMapCardTemplate", "", "S-12"));
}

QVariant PrintTerritoryMapCard::fillTemplate()
{
    //qDebug() << "printTerritoryMapCard";
    QPagedPaintDevice *pdevice = nullptr;
    QByteArray byteArray;
    QPainter *painter(nullptr);
    QBuffer buffer;
    QString pages = "";

    //qDebug() << "font";
    QFont f;
    QFont ckFont;
    // font size adjusting
    f.setPixelSize(int(40 * textSizeFactorySlips));
    ckFont.setPixelSize(int(40 * textSizeFactorySlips));
    f.setFamily("Arial");
    ckFont.setFamily("Arial");
    ckFont.setBold(true);

    QSettings settings;
    mapSizeFactory = settings.value("print/mapsize", 1).toInt();

    QString templateSlipPath = getTemplate(true);
    if (!QFile(templateSlipPath).exists()) {
        initLayout("");
        if (templateSlipPath == "")
            templateSlipPath = tr("Template");

        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
        pdevice->setPageLayout(layout);
        painter = new QPainter(pdevice);
        painter->setWindow(0, 0, 2000, 2000);
        painter->setFont(f);
        painter->drawText(50, 50, templateSlipPath + " not found");
        painter->end();
        delete painter;
        return byteArray;
    }

    SlipScanner ss(nullptr, templateSlipPath, _forceRescan);
    if (ss.needFinalized)
        ss.FinalizeBoxes(0);

    QSize customsize = QSize(ss.pageWidth, ss.pageHeight);
    if (!ss.is4up) {
        currentTemplateData->setPaperSize("A6");
    } else {
        if (currentTemplateData->standardPaperSize == QPageSize::A6)
            currentTemplateData->setPaperSize("A4");
    }
    initLayout("");
    layout.setOrientation(QPageLayout::Landscape);
    if (_printer) {       
        pdevice = _printer;
        pdevice->setPageLayout(layout);
    } else {
        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);        
    }
    pdevice->setPageLayout(layout);
    painter = new QPainter(pdevice);
    painter->setWindow(0, 0, customsize.width(), customsize.height());


#ifdef Q_OS_WIN
    // windows fix
    double xTransform = ((layout.pageSize().rect(QPageSize::Point).x() * QVariant(customsize.width()).toDouble()) /
                         layout.pageSize().rect(QPageSize::Point).width());
    double yTransform = ((layout.pageSize().rect(QPageSize::Point).y() * QVariant(customsize.height()).toDouble()) /
                         layout.pageSize().rect(QPageSize::Point).height());
    //qDebug() << "page rect:" << pr->pageRect();
    //qDebug() << "paper rect:" << pr->paperRect();
    //qDebug() << "transform:" << xTransform << yTransform;
    painter->translate(xTransform * -1, yTransform * -1);
#endif

    painter->setFont(f);
    QFontMetrics fm = painter->fontMetrics();
    int textboxOffset(-fm.height());
    int textboxHeight(int(fm.height() * 2.1));
    int slipnum(0);
    int maxslipsperpage(!ss.is4up ? 1 : 4);

    int needNewPage(1); // 0=none, 1=need image, 2=need page

    QList<QRectF *> localityRegions;
    QList<QRectF *> territoryNumberRegions;
    QList<QRectF *> mapRegions;

    int txtnum(0);
    SlipScanner::region *box1;
    SlipScanner::region *box2;

    int pagecx = (ss.pageWidth / 2);
    int slipWidth = ss.pageWidth;
    if (ss.is4up && ss.textBoxes.count() == 8)
        slipWidth = ss.textBoxes[2]->x1 - ss.textBoxes[0]->x1;

    for (int snum = 0; snum < maxslipsperpage; snum++) {
        // depending on the language, the position of the locality and terr.no. is different
        // the longer textbox should be locality
        box1 = ss.textBoxes[txtnum++];
        box2 = ss.textBoxes[txtnum++];
        if (box1->width > box2->width) {
            localityRegions.append(new QRectF(box1->x1, box1->y2 + textboxOffset, box1->width, textboxHeight));
            territoryNumberRegions.append(new QRectF(box2->x1, box2->y2 + textboxOffset, box2->width, textboxHeight));
        } else {
            localityRegions.append(new QRectF(box2->x1, box2->y2 + textboxOffset, box2->width, textboxHeight));
            territoryNumberRegions.append(new QRectF(box1->x1, box1->y2 + textboxOffset, box1->width, textboxHeight));
        }

        int cx = pagecx;
        if (ss.is4up)
            cx = pagecx - slipWidth / 2 + (snum % 2) * slipWidth;

        mapRegions.append(new QRectF(cx - (725 + mapSizeFactory),
                                     ss.blankAreas[snum]->y1 + textboxHeight / 4,
                                     (1450 + mapSizeFactory * 2),
                                     ss.blankAreas[snum]->height));
    }

    sql_items territories = sql->selectSql("SELECT t.*,tc.city FROM territories t"
                                           " LEFT JOIN territory_city tc ON t.city_id = tc.id"
                                           " WHERE territory_number in ("
                                           + territoryNumberList + ")");

    for (unsigned int i = 0; i < territories.size(); i++) {
        if (needNewPage) {
            if (needNewPage > 1)
                pdevice->newPage();
            painter->drawImage(0, 0, ss.img);
            needNewPage = 0;
            slipnum = 0;
        }

        painter->drawText(*localityRegions[slipnum], Qt::AlignHCenter, territories[i].value("locality").toString());
        painter->drawText(*territoryNumberRegions[slipnum], Qt::AlignHCenter, territories[i].value("territory_number").toString());

        QSettings settings;
        int geoServiceProvider = settings.value("geo_service_provider/default", 0).toInt();
        QString googleAPIKey = settings.value("geo_service_provider/google_api_key", "").toString();
        googleAPIKey = googleAPIKey.isEmpty() ? "" : "&key=" + googleAPIKey;
        QString hereAppId = settings.value("geo_service_provider/here_app_id", "").toString();
        QString hereAppCode = settings.value("geo_service_provider/here_app_code", "").toString();
        hereAppId = hereAppId.isEmpty() ? "" : "&app_id=" + hereAppId;
        hereAppCode = hereAppCode.isEmpty() ? "" : "&app_code=" + hereAppCode;

        switch (geoServiceProvider) {
        case 1: {
            // Google
            break;
        }
        case 2: {
            // HERE geocoding service
            break;
        }
        default:
            // OSM
            geoServiceProvider = 2;
            if (!googleAPIKey.isEmpty())
                geoServiceProvider = 1;
            if (!hereAppId.isEmpty() && !hereAppCode.isEmpty())
                geoServiceProvider = 2;
            break;
        }

        // generate path parameter of territory boundary
        QString wktGeometry = territories[i].value("wkt_geometry").toString();

        QString geoPath;
        QRegularExpression regExp("((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))\\s*((?:[-+]?\\.\\d+|[-+]?\\d+(?:\\.\\d*)?))");
        QRegularExpression polygonRegex("\\(.*?\\)"); //g, polygonMatch;
        QRegularExpression multiPolygonRegex("\\(\\(.*?\\)\\)"); //g, multiPolygonMatch;
        int outerPolygonIndex = 0;
        QRegularExpressionMatchIterator iMultiPolygon = multiPolygonRegex.globalMatch(wktGeometry);
        while (iMultiPolygon.hasNext()) {
            QRegularExpressionMatch multiPolygonMatch = iMultiPolygon.next();
            QString multiPolygon = multiPolygonMatch.captured(0);
            QRegularExpressionMatchIterator iPolygon = polygonRegex.globalMatch(multiPolygon);

            int innerPolygonIndex = 0;
            while (iPolygon.hasNext()) {
                QRegularExpressionMatch polygonMatch = iPolygon.next();
                QString polygon = polygonMatch.captured(0);

                int i = 0;
                QRegularExpressionMatchIterator iMatch = regExp.globalMatch(polygon);
                QString separator("");
                QString lastLatLon("");
                double lastLatitude(0);
                double lastLongitude(0);
                while (iMatch.hasNext()) {
                    i += 1;

                    QRegularExpressionMatch match = iMatch.next();
                    // round to 4 decimal places
                    double longitude = qRound(match.captured(1).toDouble() * 1e4) / 1e4;
                    double latitude = qRound(match.captured(2).toDouble() * 1e4) / 1e4;

                    QString encLatLon("");
                    double deltaLatitude = i == 1 ? latitude : latitude - lastLatitude;
                    double deltaLongitude = i == 1 ? longitude : longitude - lastLongitude;
                    if (i != 1 && deltaLatitude == 0.0 && deltaLongitude == 0.0)
                        continue;
                    switch (geoServiceProvider) {
                    case 1: {
                        // Google
                        if (i == 1)
                            geoPath += "&path=color:0xff00ff|enc:";
                        encLatLon = encodePointValue(deltaLatitude);
                        encLatLon += encodePointValue(deltaLongitude);
                        geoPath += encLatLon;
                        break;
                    }
                    case 2: {
                        // HERE geocoding service
                        if (i == 1)
                            geoPath += QString("&lc%1=ff00ff&a%1=").arg(QVariant(outerPolygonIndex).toString());
                        geoPath += separator;
                        encLatLon = QVariant(latitude).toString();
                        encLatLon += ",";
                        encLatLon += QVariant(longitude).toString();
                        geoPath += encLatLon;
                        separator = ",";
                        break;
                    }
                    }

                    lastLatitude = latitude;
                    lastLongitude = longitude;
                }
                innerPolygonIndex += 1;
            }
            outerPolygonIndex += 1;
        }

        // request google staticmap image
        QNetworkAccessManager manager;
        QEventLoop q;
        QTimer tT;

        tT.setSingleShot(true);
        connect(&tT, SIGNAL(timeout()), &q, SLOT(quit()));
        connect(&manager, SIGNAL(finished(QNetworkReply *)),
                &q, SLOT(quit()));

        QNetworkRequest request;
        int mapWidth = int(mapRegions[slipnum]->width());
        int mapHeight = int(mapRegions[slipnum]->height());
        // Free maximum allowable value: 640x640
        if (mapWidth > 640) {
            mapHeight = int(640.0 / mapWidth * mapHeight);
            mapWidth = 640;
        }

        switch (geoServiceProvider) {
        case 1: {
            // Google
            request.setUrl(QUrl("https://maps.googleapis.com/maps/api/staticmap?scale=2&size=" + QString::number(mapWidth) + "x" + QString::number(mapHeight) + geoPath + googleAPIKey));
            break;
        }
        case 2: {
            // HERE geocoding service
            request.setUrl(QUrl("https://image.maps.api.here.com/mia/1.6/region?ppi=320&h=" + QString::number(mapHeight) + "&w=" + QString::number(mapWidth) + hereAppId + hereAppCode + geoPath));
            break;
        }
        }
        QNetworkReply *reply = manager.get(request);

        tT.start(15000); // 15s timeout
        q.exec();

        QByteArray responseData;
        if (tT.isActive()) {
            // download complete
            tT.stop();
            responseData = reply->readAll();

            if (reply->error() != QNetworkReply::NoError) {
                reply->close();
                QString errorMessage = QString::fromUtf8(responseData);
                if (errorMessage.isEmpty())
                    errorMessage = reply->errorString();
                errorMessage = QTextDocumentFragment::fromHtml(errorMessage).toPlainText();
                painter->drawText(*mapRegions[slipnum], errorMessage);
            } else {
                QImage image;
                image.loadFromData(responseData);
                painter->drawImage(*mapRegions[slipnum], image);
            }
        } else {
            // timeout
            painter->drawText(*mapRegions[slipnum], reply->errorString());
        }

        slipnum++;
        if (slipnum >= maxslipsperpage)
            needNewPage = 2;
    }

    //qDebug() << "finish up";
    painter->end();
    if (!_printer) {
        delete pdevice;
    }
    if (buffer.isOpen())
        buffer.close();
    delete painter;
    return byteArray;
}

void PrintTerritoryMapCard::setTerritoryNumberList(QString numbers)
{
    territoryNumberList = numbers;
}

void PrintTerritoryMapCard::setForceScan(bool rescan)
{
    _forceRescan = rescan;
}

void PrintTerritoryMapCard::setPrinter(QPagedPaintDevice *d)
{
    _printer = d;
}

QString PrintTerritoryMapCard::encodePointValue(double value)
{
    bool isNegative = value < 0;

    // Take the decimal value and multiply it by 1e5, rounding the result
    int intValue = qRound(value * 1e5);

    // Left-shift the binary value one bit
    intValue <<= 1;

    // If the original decimal value is negative, invert this encoding
    intValue = isNegative ? ~intValue : intValue;

    // Break the binary value out into 5-bit chunks (starting from the right hand side)
    // Place the 5-bit chunks into reverse order
    int chunks[6];
    chunks[0] = intValue & 0x1F;
    chunks[1] = (intValue & 0x3E0) >> 5;
    chunks[2] = (intValue & 0x7C00) >> 10;
    chunks[3] = (intValue & 0xF8000) >> 15;
    chunks[4] = (intValue & 0x1F00000) >> 20;
    chunks[5] = (intValue & 0x3E000000) >> 25;

    QString result("");
    unsigned int lastChunk = 0;
    for (unsigned int i = 5; i > 0; i--) {
        if (chunks[i] != 0) {
            lastChunk = i;
            break;
        }
    }

    for (unsigned int i = 0; i <= lastChunk; i++) {
        // OR each value with 0x20 if another bit chunk follows
        if (i < lastChunk)
            chunks[i] = chunks[i] | 0x20;

        // Add 63 to each value
        chunks[i] += 63;

        // Convert each value to its ASCII equivalent
        result += QChar(chunks[i]).toLatin1();
    }
    return result;
}
