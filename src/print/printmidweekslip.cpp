/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printmidweekslip.h"

PrintMidweekSlip::PrintMidweekSlip()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::MidweekSlipTemplate, "slipTemplate", "", "S-89"));
}

QVariant PrintMidweekSlip::fillTemplate()
{
    QPagedPaintDevice *pdevice = nullptr;
    QByteArray byteArray;
    QPainter *painter(nullptr);
    QBuffer buffer;

    QFont f;
    QFont ckFont;
    // font size adjusting
    QSettings settings;
    textSizeFactorySlips = settings.value("print/fontsize", 1).toInt();
    f.setPixelSize(40 + textSizeFactorySlips * 2);
    ckFont.setPixelSize(40 + textSizeFactorySlips * 2);
    qDebug() << f.pixelSize() << ckFont.pixelSize();
    f.setFamily("Arial");
    ckFont.setFamily("Arial");
    ckFont.setBold(true);

    QString templateSlipPath = getTemplate(true);
    if (!QFile(templateSlipPath).exists()) {
        initLayout("");
        if (templateSlipPath == "")
            templateSlipPath = tr("Slip Template");

        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
        pdevice->setPageLayout(layout);
        painter = new QPainter(pdevice);
        painter->setWindow(0, 0, 2000, 2000);
        painter->setFont(f);
        painter->drawText(50, 50, templateSlipPath + " not found");
        painter->end();
        delete painter;
        return byteArray;
    }

    SlipScanner ss(nullptr, templateSlipPath, _forceRescan);
    QSize customsize = QSize(ss.pageWidth, ss.pageHeight);
    if (!ss.is4up) {
        currentTemplateData->setPaperSize("A6");
    } else {
        if (currentTemplateData->standardPaperSize == QPageSize::A6)
            currentTemplateData->setPaperSize("A4");
    }
    initLayout("");
    layout.setOrientation(QPageLayout::Portrait);
    layout.setMargins(QMarginsF(0, 0, 0, 0));
    if (_printer) {
        pdevice = _printer;
    } else {
        buffer.setBuffer(&byteArray);
        buffer.open(QIODevice::WriteOnly);
        pdevice = new QPdfWriter(&buffer);
    }
    pdevice->setPageLayout(layout);
    painter = new QPainter(pdevice);
    painter->setWindow(0, 0, customsize.width(), customsize.height());

    painter->setFont(f);
    QFontMetrics fm = painter->fontMetrics();
    int textboxOffset(-fm.height());
    int textboxHeight(static_cast<int>(std::round(fm.height() * 2.1)));

    LMM_Meeting *mtg = new LMM_Meeting();
    int slipnum(0);
    int maxslipsperpage(ss.is4up ? 4 : 1);
    slipTypes slipType(slipTypes::veryOld);
    bool wasErr(false);

    if (ss.needFinalized) {
        // try for the pre-2019 box count since it's larger
        ss.FinalizeBoxes(11);
        bool tryFor10(false);

        if (ss.checkBoxes.length() == 11 * maxslipsperpage && ss.textBoxes.length() == 5 * maxslipsperpage) {
            // pre-2019 slip
        } else if (ss.checkBoxes.length() == 10 * maxslipsperpage && ss.textBoxes.length() == 4 * maxslipsperpage) {
        } else {
            tryFor10 = true;
        }

        if (!tryFor10) {
            // if the layout isn't known, try for 10 checkboxes
            tryFor10 = ss.layout != 703 && ss.layout != 704 && ss.layout != 803;
        }

        if (tryFor10)
            ss.FinalizeBoxes(10);
    }

    if (ss.checkBoxes.length() == 11 * maxslipsperpage && ss.textBoxes.length() == 5 * maxslipsperpage) {
        // pre-2019 slip
    } else if (ss.checkBoxes.length() == 10 * maxslipsperpage && ss.textBoxes.length() == 4 * maxslipsperpage) {
        slipType = slipTypes::is2019;
    } else if (ss.checkBoxes.length() == 10 * maxslipsperpage && ss.textBoxes.length() == 7 * maxslipsperpage && (ss.layout == 604 || ss.layout == 703 || ss.layout == 208 || ss.layout == 406 || ss.layout == 505)) {
        slipType = slipTypes::is2020;
    } else if (ss.checkBoxes.length() == 9 * maxslipsperpage && ss.textBoxes.length() == 6 * maxslipsperpage && (ss.layout == 603 || ss.layout == 306)) {
        slipType = slipTypes::is2021;
    } else {
        painter->drawText(100, 100, "Box count for this template is incorrect");
        painter->drawText(100, 200, "Expected " + QString::number(4 * maxslipsperpage) + " text boxes but have " + QString::number(ss.textBoxes.length()));
        painter->drawText(100, 300, "Expected " + QString::number(10 * maxslipsperpage) + " check boxes but have " + QString::number(ss.checkBoxes.length()));
        wasErr = true;
    }

    if (_debugBoxes) {
        int id(1);
        for (SlipScanner::region *box : ss.textBoxes) {
            painter->drawText(box->x1, box->y2, "Box #" + QString::number(id++));
            painter->drawRect(box->x1, box->y1, box->width, box->height);
        }
        id = 1;
        for (SlipScanner::region *box : ss.checkBoxes) {
            //qDebug() << "checkbox" << id << box->x1 << box->y1 << box->half << box->quadrant;
            painter->setFont(ckFont);
            drawCheckbox(painter, box->x1, box->y2);
            painter->setFont(f);
            painter->drawText(box->x1 - box->width * 3, box->y2, QString::number(id++));
            painter->drawText(box->x1 + box->width * 1, box->y2, QString::number(box->width) + "x" + QString::number(box->height));
            painter->drawRect(box->x1, box->y1, box->width, box->height);
        }
        wasErr = true;
    }

    if (wasErr) {
        painter->end();
        delete painter;
        return byteArray;
    }

    int needNewPage(1); // 0=none, 1=need image, 2=need page

    QList<SlipScanner::region *> br;
    QList<SlipScanner::region *> init;
    QList<SlipScanner::region *> rv1;
    QList<SlipScanner::region *> rv2;
    QList<SlipScanner::region *> rv3;
    QList<SlipScanner::region *> bs;
    QList<SlipScanner::region *> talk;
    QList<SlipScanner::region *> other;
    QList<SlipScanner::region *> main;
    QList<SlipScanner::region *> aux1;
    QList<SlipScanner::region *> aux2;
    QList<QRectF *> nm;
    QList<QRectF *> astn;
    QList<QRectF *> dt;
    QList<QRectF *> cp;
    QList<QRectF *> icStudy;
    QList<QRectF *> rv1Study;
    QList<QRectF *> rv2Study;
    QList<QRectF *> oasgn;

    //qDebug() << "fill lists";
    //qDebug() << ss.checkBoxes.length();
    //qDebug() << ss.textBoxes.length();
    int textFlags = Qt::AlignLeft;
    int cknum(0);
    int txtnum(0);
    SlipScanner::region *box;
    for (int snum = 0; snum < maxslipsperpage; snum++) {
        bool right2left(false);
        switch (ss.layout) { // layout is "boxes in first column" * 100 + "boxes in second column"
        case 208:
        case 406:
            if (slipType == slipTypes::is2020) {
                // rigth to left text (Hebrew)
                rv2.append(ss.checkBoxes[cknum++]);
                bs.append(ss.checkBoxes[cknum++]);
                talk.append(ss.checkBoxes[cknum++]);
                other.append(ss.checkBoxes[cknum++]);

                br.append(ss.checkBoxes[cknum++]);
                init.append(ss.checkBoxes[cknum++]);
                rv1.append(ss.checkBoxes[cknum++]);

                main.append(ss.checkBoxes[cknum++]);
                aux1.append(ss.checkBoxes[cknum++]);
                aux2.append(ss.checkBoxes[cknum++]);
                textFlags = Qt::AlignRight;
                right2left = true;
            } else {
                wasErr = true;
            }
            break;
        case 306:
            if (slipType == slipTypes::is2021) {
                // rigth to left text (Arabic)
                bs.append((ss.checkBoxes[cknum++]));
                talk.append((ss.checkBoxes[cknum++]));
                other.append((ss.checkBoxes[cknum++]));

                br.append((ss.checkBoxes[cknum++]));
                init.append((ss.checkBoxes[cknum++]));
                rv1.append((ss.checkBoxes[cknum++]));
                main.append((ss.checkBoxes[cknum++]));
                aux1.append((ss.checkBoxes[cknum++]));
                aux2.append((ss.checkBoxes[cknum++]));
                textFlags = Qt::AlignRight;
                right2left = true;
            } else {
                wasErr = true;
            }
            break;
        case 505:
            if (slipType == slipTypes::is2020) {
                // rigth to left text (Arabic)
                rv2.append(ss.checkBoxes[cknum++]);
                bs.append(ss.checkBoxes[cknum++]);
                talk.append(ss.checkBoxes[cknum++]);
                other.append(ss.checkBoxes[cknum++]);
                aux1.append(ss.checkBoxes[cknum++]);

                br.append(ss.checkBoxes[cknum++]);
                init.append(ss.checkBoxes[cknum++]);
                rv1.append(ss.checkBoxes[cknum++]);
                main.append(ss.checkBoxes[cknum++]);
                aux2.append(ss.checkBoxes[cknum++]);

                textFlags = Qt::AlignRight;
                right2left = true;
            } else {
                wasErr = true;
            }
            break;
        case 603:
            if (slipType == slipTypes::is2021) {
                br.append((ss.checkBoxes[cknum++]));
                init.append((ss.checkBoxes[cknum++]));
                rv1.append((ss.checkBoxes[cknum++]));
                main.append((ss.checkBoxes[cknum++]));
                aux1.append((ss.checkBoxes[cknum++]));
                aux2.append((ss.checkBoxes[cknum++]));
                bs.append((ss.checkBoxes[cknum++]));
                talk.append((ss.checkBoxes[cknum++]));
                other.append((ss.checkBoxes[cknum++]));
            }
            break;
        case 604:
            if (slipType == slipTypes::is2020) {
                br.append(ss.checkBoxes[cknum++]);
                init.append(ss.checkBoxes[cknum++]);
                rv1.append(ss.checkBoxes[cknum++]);
                main.append(ss.checkBoxes[cknum++]);
                aux1.append(ss.checkBoxes[cknum++]);
                aux2.append(ss.checkBoxes[cknum++]);
                rv2.append(ss.checkBoxes[cknum++]);
                bs.append(ss.checkBoxes[cknum++]);
                talk.append(ss.checkBoxes[cknum++]);
                other.append(ss.checkBoxes[cknum++]);
            } else {
                br.append(ss.checkBoxes[cknum++]);
                init.append(ss.checkBoxes[cknum++]);
                rv1.append(ss.checkBoxes[cknum++]);
                rv2.append(ss.checkBoxes[cknum++]);
                main.append(ss.checkBoxes[cknum++]);
                aux1.append(ss.checkBoxes[cknum++]);
                aux2.append(ss.checkBoxes[cknum++]);
                bs.append(ss.checkBoxes[cknum++]);
                talk.append(ss.checkBoxes[cknum++]);
                other.append(ss.checkBoxes[cknum++]);
            }
            break;
        case 703:
            if (slipType == slipTypes::is2020) {
                br.append(ss.checkBoxes[cknum++]);
                init.append(ss.checkBoxes[cknum++]);
                rv1.append(ss.checkBoxes[cknum++]);
                rv2.append(ss.checkBoxes[cknum++]);
                bs.append(ss.checkBoxes[cknum++]);
                talk.append(ss.checkBoxes[cknum++]);
                other.append(ss.checkBoxes[cknum++]);
                main.append(ss.checkBoxes[cknum++]);
                aux1.append(ss.checkBoxes[cknum++]);
                aux2.append(ss.checkBoxes[cknum++]);
            } else {
                br.append(ss.checkBoxes[cknum++]);
                init.append(ss.checkBoxes[cknum++]);
                rv1.append(ss.checkBoxes[cknum++]);
                rv2.append(ss.checkBoxes[cknum++]);
                main.append(ss.checkBoxes[cknum++]);
                aux1.append(ss.checkBoxes[cknum++]);
                aux2.append(ss.checkBoxes[cknum++]);
                bs.append(ss.checkBoxes[cknum++]);
                talk.append(ss.checkBoxes[cknum++]);
                other.append(ss.checkBoxes[cknum++]);
            }
            break;
        case 704:
            br.append(ss.checkBoxes[cknum++]);
            init.append(ss.checkBoxes[cknum++]);
            rv1.append(ss.checkBoxes[cknum++]);
            rv2.append(ss.checkBoxes[cknum++]);
            main.append(ss.checkBoxes[cknum++]);
            aux1.append(ss.checkBoxes[cknum++]);
            aux2.append(ss.checkBoxes[cknum++]);
            rv3.append(ss.checkBoxes[cknum++]);
            bs.append(ss.checkBoxes[cknum++]);
            talk.append(ss.checkBoxes[cknum++]);
            other.append(ss.checkBoxes[cknum++]);
            break;
        case 803:
            br.append(ss.checkBoxes[cknum++]);
            init.append(ss.checkBoxes[cknum++]);
            rv1.append(ss.checkBoxes[cknum++]);
            rv2.append(ss.checkBoxes[cknum++]);
            rv3.append(ss.checkBoxes[cknum++]);
            bs.append(ss.checkBoxes[cknum++]);
            talk.append(ss.checkBoxes[cknum++]);
            other.append(ss.checkBoxes[cknum++]);
            main.append(ss.checkBoxes[cknum++]);
            aux1.append(ss.checkBoxes[cknum++]);
            aux2.append(ss.checkBoxes[cknum++]);
            break;
        default:
            // TODO: show error message ??
            //QMessageBox::warning(this, "Unknown Slip", tr("You have a new kind of slip. Please enter a request on the forum so the app can be updated.  Mention code %1").replace("%1", QVariant(ss.layout).toString()));
            wasErr = true;
        }
        box = ss.textBoxes[txtnum++];
        nm.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));

        box = ss.textBoxes[txtnum++];
        astn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));

        box = ss.textBoxes[txtnum++];
        dt.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));

        if (slipType == slipTypes::veryOld) {
            box = ss.textBoxes[txtnum++];
            cp.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
            box = ss.textBoxes[txtnum++];
            oasgn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
        } else if (slipType == slipTypes::is2020) {
            if (right2left) {
                box = ss.textBoxes[txtnum++];
                rv2Study.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
                box = ss.textBoxes[txtnum++];
                oasgn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
                box = ss.textBoxes[txtnum++];
                icStudy.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
                box = ss.textBoxes[txtnum++];
                rv1Study.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
            } else {
                box = ss.textBoxes[txtnum++];
                icStudy.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
                box = ss.textBoxes[txtnum++];
                rv1Study.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
                box = ss.textBoxes[txtnum++];
                rv2Study.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
                box = ss.textBoxes[txtnum++];
                oasgn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
            }
        } else if (slipType == slipTypes::is2021) {
            box = ss.textBoxes[txtnum++];
            icStudy.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
            box = ss.textBoxes[txtnum++];
            rv1Study.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
            box = ss.textBoxes[txtnum++];
            oasgn.append(new QRectF(box->x1, box->y2 + textboxOffset, box->width, textboxHeight));
        }
    }

    //qDebug() << "br" << br.length();
    //qDebug() << "init" << init.length();
    //qDebug() << "rv" << rv.length();
    //qDebug() << "bs" << bs.length();
    //qDebug() << "other" << other.length();
    //qDebug() << "main" << main.length();
    //qDebug() << "aux1" << aux1.length();
    //qDebug() << "aux2" << aux2.length();
    //qDebug() << "nm" << nm.length();
    //qDebug() << "astn" << astn.length();
    //qDebug() << "dt" << dt.length();
    //qDebug() << "cp" << cp.length();
    //qDebug() << "oasgn" << oasgn.length();

    //qDebug() << "fillweeklist";
    fillWeekList(0);

    QHash<int, QList<SlipScanner::region *>> checkboxLookup;
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_BibleReading), br);
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_InitialCall), init);
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_ReturnVisit), rv1);
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_ReturnVisit) + 1, rv2); // TODO: this is probably obsolete
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_ReturnVisit) + 2, rv3);
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_BibleStudy), bs);
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_StudentTalk), talk);
    checkboxLookup.insert(MeetingPartClass::toTalkId(MeetingPart::LMM_MemorialInvitation), other);

    //qDebug() << "main loop";
    if (!wasErr) {
        for (weekInfo *w : weekList) {
            currentWeek = w;
            QDate tempdate(w->week);
            if (mtg->loadMeeting(tempdate)) {
                QList<LMM_Assignment *> prog = mtg->getAssignments();
                QList<int> ckUsed;
                for (LMM_Assignment *asgn : prog) {
                    for (int spk_asst = 0; spk_asst < 2; spk_asst++) {

                        if (!asgn->canCounsel())
                            continue;
                        LMM_Assignment_ex *studenttalk = qobject_cast<LMM_Assignment_ex *>(asgn);
                        if ((_onlyAssigned && !studenttalk->speaker()) || (spk_asst && !(_printAssistant && studenttalk->assistant())))
                            continue;

                        if (needNewPage) {
                            if (needNewPage > 1)
                                pdevice->newPage();
                            painter->drawImage(0, 0, ss.img);
                            needNewPage = 0;
                            slipnum = 0;
                        }

                        int sdate = c.getMeetingDay(tempdate, ccongregation::tms);
                        QDate date = tempdate.addDays(sdate - 1);
                        // we'll print the date after we (possibly) add study number
                        QString fullDateText(QLocale().toString(date, QLocale::ShortFormat));
                        qDebug() << "  date" << date;
                        //qDebug() << "  draw date";
                        //qDebug() << slipnum;
                        //qDebug() << dt[slipnum]->x1;

                        //qDebug() << "  draw speakers";
                        //qDebug() << "  slip " << slipnum << studenttalk->speaker()->fullname();

                        if (studenttalk->speaker())
                            painter->drawText(*nm[slipnum], textFlags, studenttalk->speaker()->fullname());
                        if (studenttalk->assistant())
                            painter->drawText(*astn[slipnum], textFlags, studenttalk->assistant()->fullname());

                        //qDebug() << "  draw other";
                        if (spk_asst && _printAssistant && studenttalk->assistant()) {
                            painter->setFont(ckFont);
                            drawCheckbox(painter, other[slipnum]->x1, other[slipnum]->y2);
                            painter->setFont(f);
                            painter->drawText(*oasgn[slipnum], textFlags, tr("Assistant"));
                        } else if (studenttalk->speaker()) {
                            QString txt;
                            if (slipType == slipTypes::is2019) {
                                fullDateText += "  (" + QString(tr("Study %1", "Text for study point on slip")).arg(QVariant(studenttalk->study_number()).toString()) + ")";
                            } else if (slipType == slipTypes::is2020 || slipType == slipTypes::is2021) {
                                txt = QString(tr("Study %1", "Text for study point on slip")).arg(QVariant(studenttalk->study_number()).toString());
                                switch (asgn->meetingPart()) {
                                case MeetingPart::LMM_MemorialInvitation:
                                    painter->drawText(*oasgn[slipnum], textFlags, txt);
                                    break;
                                case MeetingPart::LMM_InitialCall:
                                    painter->drawText(*icStudy[slipnum], textFlags, txt);
                                    break;
                                case MeetingPart::LMM_ReturnVisit:
                                    painter->drawText(*rv1Study[slipnum], textFlags, txt);
                                    break;
                                default:
                                    break;
                                }
                                txt.clear();
                            } else {
                                txt = QVariant(studenttalk->study_number()).toString() + " " + studenttalk->study_name();
                            }
                            if (!txt.isEmpty()) {
                                painter->drawText(*cp[slipnum], textFlags, txt);
                            }
                        }

                        painter->drawText(*dt[slipnum], textFlags, fullDateText);

                        //qDebug() << "  draw ck";
                        SlipScanner::region *ck;
                        ck = checkboxLookup[MeetingPartClass::toTalkId(studenttalk->meetingPart())][slipnum];
                        painter->setFont(ckFont);
                        drawCheckbox(painter, ck->x1, ck->y2);
                        painter->setFont(f);
                        int ckKey(MeetingPartClass::toTalkId(studenttalk->meetingPart()) * 100 + studenttalk->classnumber() * 10 + studenttalk->sequence());
                        if (ckUsed.contains(ckKey)) {
                            painter->setFont(ckFont);
                            for (int i = 0; i < 10; i++)
                                drawCheckbox(painter, other[slipnum]->x1, other[slipnum]->y2);
                            painter->setFont(f);
                            if (!spk_asst) {
                                painter->drawText(*oasgn[slipnum], textFlags, tr("2nd talk", "When printing slips: if the first talk is not 'Return Visit'"));
                                /*
                                switch (studenttalk->talkId()) {
                                case MeetingPart::LMM_InitialCall:
                                    oasgn[slipnum]->setValue(tr("1st talk", "When printing slips: if the first talk is not 'Inital Call'"));
                                    break;
                                case MeetingPart::LMM_ReturnVisit:
                                    oasgn[slipnum]->setValue(tr("2nd talk", "When printing slips: if the first talk is not 'Return Visit'"));
                                    break;
                                case MeetingPart::LMM_BibleStudy:
                                    oasgn[slipnum]->setValue(tr("3rd talk", "When printing slips: if the first talk is not 'Bible Study'"));
                                    break;
                                }
                                */
                            }
                        } else if (!spk_asst)
                            ckUsed.append(ckKey);

                        //qDebug() << " class";
                        switch (studenttalk->classnumber()) {
                        case 1:
                            drawCheckbox(painter, main[slipnum]->x1, main[slipnum]->y2);
                            break;
                        case 2:
                            drawCheckbox(painter, aux1[slipnum]->x1, aux1[slipnum]->y2);
                            break;
                        case 3:
                            drawCheckbox(painter, aux2[slipnum]->x1, aux2[slipnum]->y2);
                            break;
                        }

                        slipnum++;
                        if (slipnum >= maxslipsperpage)
                            needNewPage = 2;
                    }
                }
            }
        }
    }

    //qDebug() << "finish up";
    painter->end();
    if (!_printer) {
        delete pdevice;
    }
    if (buffer.isOpen())
        buffer.close();
    delete painter;
    return byteArray;
}

void PrintMidweekSlip::setOnlyAssigned(bool onlyAssigned)
{
    _onlyAssigned = onlyAssigned;
}

void PrintMidweekSlip::setPrintAssistant(bool printAssistant)
{
    _printAssistant = printAssistant;
}

void PrintMidweekSlip::setDebugBoxes(bool debugBoxes)
{
    _debugBoxes = debugBoxes;
}

void PrintMidweekSlip::setForceRescan(bool rescan)
{
    _forceRescan = rescan;
}

void PrintMidweekSlip::setPrinter(QPagedPaintDevice *d)
{
    _printer = d;
}

void PrintMidweekSlip::drawCheckbox(QPainter *painter, int x, int y)
{
#ifdef Q_OS_WIN
    QString checkmark = "√";
#else
    QString checkmark = "\u2713";
#endif
    for (int i = -2; i < 3; i++)
        painter->drawText(x + i, y, checkmark);
}
