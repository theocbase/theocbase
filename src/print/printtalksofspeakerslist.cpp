/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2020, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "printtalksofspeakerslist.h"

PrintTalksOfSpeakersList::PrintTalksOfSpeakersList()
{
    currentTemplateData = QSharedPointer<TemplateData>(
            new TemplateData(TemplateData::TalksOfSpeakersTemplate, "publicTalksTemplate", "PT-TalksOfSpeakers_Simple.htm", "pt-"));
}

QVariant PrintTalksOfSpeakersList::fillTemplate()
{
    QString str = getTemplate();
    str = initLayout(str);
    str = ifthen.tokenize(str);

    bool groupedByCircuit = true;
    bool ownCongregationOnly = currentTemplateData->optionValue("own-congregation-only") == "yes";
    bool showRevisionDate = currentTemplateData->optionValue("revision-date") == "yes";

    QString tst = sql->getSetting("print_pt_title", tr("Public Talks"));
    str.replace("!TITLE!", sql->getSetting("print_pt_title", tr("Public Talks")));
    str.replace("!CONTACT_INFO!", tr("Contact info"));
    str.replace("!SPEAKER_TALK_NUMBERS!", tr("Talk numbers", "Public talk numbers"));
    str = fillTemplateBlockPTOUTTitle(str);

    QString basepage = "";
    QString pagesec = "";
    QString sp_sec = "";
    QString pt_sec = "";

    QPair<QString, QString> sec = templateGetSection(str, "!TALK_START!", "!TALK_END!", "talk_class");
    pt_sec = sec.second;
    sec = templateGetSection(sec.first, "!SPEAKER_START!", "!SPEAKER_END!", "speaker_class");
    sp_sec = sec.second;
    sec = templateGetSection(sec.first, "!CONGREGATION_START!", "!CONGREGATION_END!", "congregation_class");
    pagesec = sec.second;
    basepage = sec.first;
    QString pages = "";

    ccongregation cc;
    cpublictalks cpt;
    QStandardItemModel *speakersList = nullptr;
    person *speaker(nullptr);

    QList<ccongregation::congregation> congregations;
    if (ownCongregationOnly)
        congregations.append(c.getMyCongregation());
    else
        congregations = cc.getAllCongregations(groupedByCircuit);

    for (ccongregation::congregation const &cong : congregations) {
        // congregation
        QString filledpage(pagesec);

        QString speakerSection("");
        speakersList = cpt.getSpeakers(0, cong.id, "", false, false);
        for (int i = 0; i < speakersList->rowCount(); ++i) {
            // speaker
            QString currectSpeakerSection(sp_sec);
            speaker = cpersons::getPerson(speakersList->index(i, 0).data(0).toInt());
            if (!speaker)
                continue;

            QString talkSection("");
            QString talkNumbers("");
            QString sep("");
            std::vector<ThemeListItem> themeList = cpt.getThemeList("", speaker);
            for (auto row : themeList) {
                // talk
                QString currentTalkSection(pt_sec);
                talkNumbers += sep + QVariant(row.number).toString();
                currentTalkSection.replace("!PT_NO!", QVariant(row.number).toString());
                currentTalkSection.replace("!PT_THEME!", row.theme);
                if (showRevisionDate) {
                    currentTalkSection.replace("!PT_REVISION!", row.revision);
                } else {
                    currentTalkSection.replace("!PT_REVISION!", "");
                }
                talkSection.append(currentTalkSection);
                sep = ", ";
            }
            currectSpeakerSection.replace("!PT_SPEAKER_TALK_NUMBERS!", talkNumbers);

            if (talkSection != "")
                currectSpeakerSection.replace("<div class=\"talk_class\"></div>", "<div class=\"talk_class\">" + talkSection + "</div>");

            currectSpeakerSection.replace("!PT_SPEAKER!", speaker->fullname());
            currectSpeakerSection.replace("!PT_SPEAKER_PHONE!", speaker->phone());
            currectSpeakerSection.replace("!PT_SPEAKER_MOBILE!", speaker->mobile());
            currectSpeakerSection.replace("!PT_SPEAKER_EMAIL!", speaker->email());
            speakerSection.append(currectSpeakerSection);
        }
        if (speakerSection != "")
            filledpage.replace("<div class=\"speaker_class\"></div>", "<div class=\"speaker_class\">" + speakerSection + "</div>");

        filledpage.replace("!CONGREGATION_NAME!", cong.name);
        filledpage.replace("!CONGREGATION_ADDRESS!", cong.address);
        filledpage.replace("!CONGREGATION_CIRCUIT!", cong.circuit);
        filledpage.replace("!CONGREGATION_INFO!", cong.info);
        pages.append(filledpage);
    }
    basepage.replace("<div class=\"congregation_class\"></div>", "<div class=\"congregation_class\">" + pages + "</div>");
    return ifthen.compute(basepage);
}
