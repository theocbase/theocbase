/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "importwintm.h"

importwintm::importwintm(QString filename)
{
    QFileInfo info(filename);
    m_directory = info.dir();
    thisweek = QDate::currentDate();
    sql = &Singleton<sql_class>::Instance();
}

void importwintm::Go()
{
    QFileInfoList files = m_directory.entryInfoList(QStringList("current.sd6"));
    // looping over one file is overkill.  But, we may need to import more of this type later.
    for (int i = 0; i < files.size(); i++)
        importpeople(files.at(i).absoluteFilePath());

    QMessageBox::information(0, QObject::tr("Import") + " WINTM", QObject::tr("Import Complete"));
}

void importwintm::importpeople(QString filename)
{
    oldIdToNewID.clear();
    sql_item firstandlast;
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    file.seek(0x80);
    data = file.readAll();
    for (pos = 0; pos < data.length();) {
        int startpos = pos;
        char temp;
        person p;
        int oldID = 0;
        int usefor = 0;

        p.setFirstname(readstring(17));
        p.setLastname(readstring(17));

        oldID = readint4(true);
        //p.setInfo("WINTMID: " + QString::number(oldID)); << not really needed
        pos += 4;

        pos++; // don't need anything from the first bit field
        pos++; // unused

        temp = readchar();
        p.setServant((temp & 2) > 0);
        if (p.servant())
            usefor |= person::LMM_Treasures | person::LMM_Digging; // formerly Highlights
        if ((temp & 4) > 0)
            usefor |= person::LMM_BibleReading; // formerly No. 1
        if ((temp & 8) > 0)
            usefor |= person::LMM_InitialCall | person::LMM_ReturnVisit | person::LMM_BibleStudy; // formerly No. 2
        bool isUsedForNr3;
        if ((temp & 16) > 0) {
            isUsedForNr3 = true;
            usefor |= person::LMM_InitialCall | person::LMM_ReturnVisit | person::LMM_BibleStudy; // formerly No. 3
        }
        if ((temp & 32) > 0)
            usefor |= person::Assistant;

        temp = readchar();
        if ((temp & 1) > 0)
            usefor |= person::WtReader | person::CBSReader;
        if ((temp & 4) > 0)
            p.setGender(person::Female);
        if ((temp & 8) > 0) {
            p.setGender(person::Male);
            if (isUsedForNr3)
                usefor |= person::LMM_ApplyTalks; // formerly No. 3
        }

        pos += 26; // unused

        bool frontonly = false, backonly = false;
        temp = readchar();
        if (temp == 0x0A)
            frontonly = true;
        else if (temp == 0x14)
            backonly = true;
        pos++;
        temp = readchar();
        if (temp == 0x0A)
            frontonly = true;
        else if (temp == 0x14)
            backonly = true;
        pos++;
        temp = readchar();
        if (temp == 0x0A)
            frontonly = true;
        else if (temp == 0x14)
            backonly = true;
        pos++;
        temp = readchar();
        if (temp == 0x0A)
            frontonly = true;
        else if (temp == 0x14)
            backonly = true;
        pos++;

        if (backonly)
            usefor |= person::SchoolAux;
        else if (frontonly)
            usefor |= person::SchoolMain;

        p.setUsefor(usefor);
        p.setCongregationid(sql->getSetting("congregation_id", "1").toInt());

        firstandlast.insert(":firstname", p.firstname());
        firstandlast.insert(":lastname", p.lastname());

        QVariant newID = sql->selectScalar("select id from persons where firstname = :firstname and lastname = :lastname", &firstandlast);
        if (newID.isNull()) {
            p.update();
            oldIdToNewID.insert(oldID, p.id());
        } else {
            oldIdToNewID.insert(oldID, newID.toInt());
        }
        pos = startpos + 0x80;
    }
}

QString importwintm::readstring(int maxlen)
{
    char *str = new char[maxlen + 1];
    char c;
    for (int pos2 = 0; pos2 <= maxlen; pos2++) {
        if (pos2 == maxlen)
            c = 0;
        else
            c = data.at(pos + pos2);
        str[pos2] = c;
        if (c == 0)
            break;
    }
    QString qstring = QString::fromLatin1(str);
    delete[] str;
    pos += maxlen;
    return qstring;
}

uint importwintm::readint4(bool LittleEndian)
{
    uint value = 0;
    char c;
    uint shift = LittleEndian ? 0 : 24;
    uint eachshift = LittleEndian ? 8 : -8;
    for (int pos2 = 0; pos2 < 4; pos2++) {
        c = data.at(pos + pos2);
        value += c << shift;
        shift += eachshift;
    }
    pos += 4;
    return value;
}

char importwintm::readchar()
{
    return data.at(pos++);
}
