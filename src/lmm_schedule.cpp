#include "lmm_schedule.h"

LMM_Schedule::LMM_Schedule(QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    m_schedule_db_id = -1;
}

LMM_Schedule::LMM_Schedule(MeetingPart meetingPart, int sequence, QDate date, QString theme, QString source, int study_number, int time, int dbId, QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    m_schedule_db_id = dbId;
    m_meetingPart = meetingPart;
    m_sequence = sequence;
    m_date = date;
    m_theme = theme;
    m_source = source;
    m_study_number = study_number;
    if (time > 0) {
        m_time = time;
    } else {
        switch (meetingPart) {
        case MeetingPart::LMM_Treasures:
            m_time = 10;
            break;
        case MeetingPart::LMM_Digging:
            m_time = 10;
            break;
        case MeetingPart::LMM_BibleReading:
            m_time = 4;
            break;
        case MeetingPart::LMM_SampleConversationVideo:
            m_time = 5;
            break;
        case MeetingPart::LMM_InitialCall:
            m_time = 2;
            break;
        case MeetingPart::LMM_ReturnVisit:
            m_time = 3;
            break;
        case MeetingPart::LMM_BibleStudy:
        case MeetingPart::LMM_StudentTalk:
            m_time = 6;
            break;
        case MeetingPart::LMM_CBS:
            m_time = 30;
            break;
        case MeetingPart::Service_Talk:
            m_time = 30;
            break;
        default:
            m_time = 10;
            break;
        }
    }
}

const MeetingPart &LMM_Schedule::meetingPart() const
{
    return m_meetingPart;
}

int LMM_Schedule::dbTalkId() const
{
    return MeetingPartClass::toDbTalkId(meetingPart()) + m_sequence;
}

int LMM_Schedule::sequence() const
{
    return m_sequence;
}

int LMM_Schedule::scheduleId() const
{
    return m_schedule_db_id;
}

int LMM_Schedule::dbTalkId(int talkId, int sequence)
{
    return talkId * 10 + sequence;
}

void LMM_Schedule::splitDbTalkId(int dbTalkId, MeetingPart &meetingPart, int &sequence)
{
    int talkId = dbTalkId / 10;
    meetingPart = MeetingPartClass::fromInt(talkId);
    sequence = dbTalkId % 10;
}

QString LMM_Schedule::getStringTalkType(int src)
{
    MeetingPart meetingPart(MeetingPartClass::fromInt(src));
    switch (meetingPart) {
    case MeetingPart::LMM_Chairman:
        return "CMN";
    case MeetingPart::LMM_Treasures:
        return "TRS";
    case MeetingPart::LMM_Digging:
        return "DIG";
    case MeetingPart::LMM_BibleReading:
        return "BiR";
    case MeetingPart::LMM_SampleConversationVideo:
        return "SCV";
    case MeetingPart::LMM_InitialCall:
        return "ICL";
    case MeetingPart::LMM_ReturnVisit:
        return "RV3";
    case MeetingPart::LMM_BibleStudy:
        return "BiS";
    case MeetingPart::LMM_StudentTalk:
        return "ST";
    case MeetingPart::LMM_LivingTalk1:
        return "LT1";
    case MeetingPart::LMM_LivingTalk2:
        return "LT2";
    case MeetingPart::LMM_LivingTalk3:
        return "LT3";
    case MeetingPart::LMM_CBS:
        return "CBS";
    case MeetingPart::Service_Talk:
        return "COT";
    default:
        return "";
    }
    return "";
}

int LMM_Schedule::getTalkTypeFromFullString(QString name)
{
    QMetaEnum e = QMetaEnum::fromType<MeetingPart>();
    static QHash<QString, MeetingPart> meetingPartList;
    if (meetingPartList.count() == 0) {
        for (int k = 0; k < e.keyCount(); k++) {
            MeetingPart meetingPart = (MeetingPart)e.value(k);
            meetingPartList.insert(MeetingPartClass::toString(meetingPart), meetingPart);
        }
    }
    if (meetingPartList.contains(name))
        return MeetingPartClass::toTalkId(meetingPartList[name]);
    else
        return -1;
}

QList<int> LMM_Schedule::getExpectedTalks()
{
    QList<int> expected;
    // TREASURES FROM GOD’S WORD
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_Treasures));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_Digging));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_BibleReading));
    // APPLY YOURSELF TO THE FIELD MINISTRY
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_InitialCall));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_ReturnVisit));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_BibleStudy));
    // LIVING AS CHRISTIANS
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_LivingTalk1));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_LivingTalk2));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_LivingTalk3));
    expected.append(MeetingPartClass::toDbTalkId(MeetingPart::LMM_CBS));
    return expected;
}

QDate LMM_Schedule::date() const
{
    return m_date;
}

void LMM_Schedule::setDate(QDate date)
{
    m_date = date;
    emit dateChanged(date);
}

int LMM_Schedule::time() const
{
    return m_time;
}

void LMM_Schedule::setTime(int time)
{
    m_time = time;
    emit timeChanged(time);
}

QString LMM_Schedule::theme() const
{
    return m_theme;
}

void LMM_Schedule::setTheme(QString theme)
{
    m_theme = theme;
    emit themeChanged(theme);
}

QString LMM_Schedule::source() const
{
    return m_source;
}

void LMM_Schedule::setSource(QString source)
{
    m_source = source;
    emit sourceChanged(source);
}

int LMM_Schedule::study_number() const
{
    return m_study_number;
}

void LMM_Schedule::setStudy_number(int study_number)
{
    m_study_number = study_number;
}

QString LMM_Schedule::study_name() const
{
    if (study_number() < 1)
        return "";
    return sql->selectScalar("select study_name from lmm_studies where study_number = ? and active", study_number(), "").toString();
}

bool LMM_Schedule::canMultiSchool() const
{
    return LMM_Schedule::canMultiSchool(meetingPart());
}

bool LMM_Schedule::canMultiSchool(MeetingPart meetingPart)
{
    AssignmentInfos *assignmentInfos = &AssignmentInfos::Instance();
    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart, false, 2);
    return assignmentInfo;
}

bool LMM_Schedule::canCounsel() const
{
    AssignmentInfos *assignmentInfos = &AssignmentInfos::Instance();

    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart());
    if (!assignmentInfo)
        throw UnknownAssignmentInfoIException(MeetingType::MidweekMeeting, 1, meetingPart(), false);
    return assignmentInfo->canCounsel();
}

bool LMM_Schedule::canHaveAssistant() const
{
    AssignmentInfos *assignmentInfos = &AssignmentInfos::Instance();
    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart());
    return assignmentInfo && assignmentInfo->canHaveAssistant();
}

QString LMM_Schedule::talkName() const
{
    return MeetingPartClass::toString(meetingPart());
}

int LMM_Schedule::roworder() const
{
    return m_roworder;
}

void LMM_Schedule::setRoworder(int roworder)
{
    m_roworder = roworder;
}

bool LMM_Schedule::save()
{
    if (!date().isValid())
        return false;
    if (meetingPart() == MeetingPart::None)
        return false;

    sql_item queryitems;
    queryitems.insert(":date", date());
    queryitems.insert(":talk_id", dbTalkId());
    m_schedule_db_id = sql->selectScalar("SELECT id FROM lmm_schedule WHERE date = :date AND talk_id = :talk_id and active", &queryitems, -1).toInt();

    sql_item insertitems;
    insertitems.insert("date", date());
    insertitems.insert("talk_id", dbTalkId());
    insertitems.insert("source", source());
    insertitems.insert("theme", theme());
    insertitems.insert("study_number", study_number());
    insertitems.insert("time", time());
    insertitems.insert("roworder", roworder());
    insertitems.insert("active", 1);

    bool returnVal(false);
    if (m_schedule_db_id > 0) {
        // update
        returnVal = sql->updateSql("lmm_schedule", "id", QString::number(m_schedule_db_id), &insertitems);
    } else {
        // insert new row
        m_schedule_db_id = sql->insertSql("lmm_schedule", &insertitems, "id");
        returnVal = m_schedule_db_id > 0;
    }
    return returnVal;
}

void LMM_Schedule::RemoveDuplicates()
{
    sql_class *s(&Singleton<sql_class>::Instance());
    s->execSql("UPDATE lmm_schedule SET active=1,time_stamp=strftime('%s','now') WHERE id IN (SELECT id FROM lmm_schedule s inner join (SELECT date, theme, source FROM lmm_schedule GROUP BY date, theme, source HAVING count(theme) > 1) dups on s.date = dups.date and s.theme = dups.theme)");
    s->execSql("UPDATE lmm_schedule SET active=0,time_stamp=strftime('%s','now') WHERE id IN (SELECT id FROM lmm_schedule s inner join (SELECT date, theme, source, max(talk_id) keeptalkid FROM lmm_schedule GROUP BY date, theme, source HAVING count(theme) > 1) dups on s.date = dups.date and s.theme = dups.theme and s.talk_id != keeptalkid)");
    s->execSql("update lmm_assignment set lmm_schedule_id = ifnull( (select newid.id from lmm_schedule inactive inner join lmm_schedule newid on inactive.date = newid.date and inactive.theme = newid.theme and newid.active where lmm_assignment.lmm_schedule_id = inactive.id and inactive.active = 0 and lmm_assignment.id not in (select a.id from lmm_assignment a inner join lmm_schedule activesch on a.lmm_schedule_id = activesch.id and activesch.active) ), lmm_schedule_id), time_stamp=strftime('%s','now') ");
    s->execSql("delete from lmm_assignment where lmm_schedule_id in (select s.id from lmm_meeting m inner join lmm_schedule s on m.date = s.date inner join lmm_assignment a on s.id = a.lmm_schedule_id where s.date >= '2018-01-01' and s.talk_id = 5 and a.assignee_id <> m.chairman and a.assignee_id <> m.counselor2 and a.assignee_id <> m.counselor3)");
    s->execSql("delete from lmm_assignment where id in (select a.id from (select date, lmm_schedule_id, classnumber, max(id) keepid from lmm_assignment group by date, lmm_schedule_id, classnumber having count(*) >1) dups inner join lmm_assignment a on dups.date = a.date and dups.lmm_schedule_id = a.lmm_schedule_id and dups.classnumber = a.classnumber and dups.keepid <> a.id)");
}
