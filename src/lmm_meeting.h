#ifndef LMM_MEETING_H
#define LMM_MEETING_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QVariant>
#include <QStandardItemModel>
#include <QDebug>
#include "lmm_assignment.h"
#include "lmm_assignment_ex.h"
#include "lmm_schedule.h"
#include "cpersons.h"
#include "person.h"
#include "ccongregation.h"
#include "sharedlib_global.h"
#include "assignmentInfo.h"
#include "tbstyle.h"

QString LMM_historyTableAbbreviation(MeetingPart meetingPart, bool assistant_or_reader = false);
QColor LMM_foregroundColor(MeetingPart meetingPart, QColor const &defCol = Qt::black);
QColor LMM_backgroundColor(MeetingPart meetingPart, QColor const &defCol = Qt::white);

typedef LMM_Assignment::MySortFilterProxyModel MySortFilterProxyModel;

//-----------------------------------------------------------------------------
class TB_EXPORT LMM_Meeting : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDate date READ date NOTIFY dateChanged)
    Q_PROPERTY(person *chairman READ chairman WRITE setChairman NOTIFY chairmanChanged)
    Q_PROPERTY(person *counselor2 READ counselor2 WRITE setCounselor2 NOTIFY counselor2Changed)
    Q_PROPERTY(person *counselor3 READ counselor3 WRITE setCounselor3 NOTIFY counselor3Changed)
    Q_PROPERTY(person *prayerBeginning READ prayerBeginning WRITE setPrayerBeginning NOTIFY prayerBeginningChanged)
    Q_PROPERTY(person *prayerEnd READ prayerEnd WRITE setPrayerEnd NOTIFY prayerEndChanged)
    Q_PROPERTY(int songBeginning READ songBeginning WRITE setSongBeginning NOTIFY songBeginningChanged)
    Q_PROPERTY(int songMiddle READ songMiddle WRITE setSongMiddle NOTIFY songMiddleChanged)
    Q_PROPERTY(int songEnd READ songEnd WRITE setSongEnd NOTIFY songEndChanged)
    Q_PROPERTY(QString songBeginningTitle READ songBeginningTitle NOTIFY songBeginningChanged)
    Q_PROPERTY(QString songMiddleTitle READ songMiddleTitle NOTIFY songMiddleChanged)
    Q_PROPERTY(QString songEndTitle READ songEndTitle NOTIFY songEndChanged)
    Q_PROPERTY(int classes READ classes WRITE setClasses NOTIFY classesChanged)
    Q_PROPERTY(QString bibleReading READ bibleReading WRITE setBibleReading NOTIFY bibleReadingChanged)
    Q_PROPERTY(QString openingComments READ openingComments WRITE setOpeningComments NOTIFY openingCommentsChanged)
    Q_PROPERTY(QString closingComments READ closingComments WRITE setClosingComments NOTIFY closingCommentsChanged)
    Q_PROPERTY(QDateTime startTime READ startTime NOTIFY startTimeChanged)
    Q_PROPERTY(QString notes READ notes WRITE setNotes NOTIFY notesChanged)

public:
    explicit LMM_Meeting(QObject *parent = nullptr, bool delayLoadingDetails = false);
    ~LMM_Meeting();

    QDate date() const;
    Q_INVOKABLE bool loadMeeting(QDate date, bool includeAllOptions = false);

    person *chairman() const;
    void setChairman(person *chairman);

    person *counselor2() const;
    void setCounselor2(person *counselor);

    person *counselor3() const;
    void setCounselor3(person *counselor);

    person *prayerBeginning() const;
    void setPrayerBeginning(person *prayer);

    person *prayerEnd() const;
    void setPrayerEnd(person *prayer);

    int songBeginning() const;
    QString songBeginningTitle() const;
    void setSongBeginning(int songBeginning);

    int songMiddle() const;
    QString songMiddleTitle() const;
    void setSongMiddle(int songMiddle);

    int songEnd() const;
    QString songEndTitle() const;
    void setSongEnd(int songEnd);

    QString bibleReading() const;
    void setBibleReading(QString reading);

    QString openingComments() const;
    void setOpeningComments(QString comments);

    QString closingComments() const;
    void setClosingComments(QString comments);

    int classes();
    void setClasses(int classes);

    QDateTime startTime();

    QString notes() const;
    void setNotes(QString notes);

    Q_INVOKABLE bool save();
    Q_INVOKABLE bool saveNotes();

    Q_INVOKABLE LMM_Assignment *getAssignment(MeetingPart meetingPart, int sequence = 0, int classnumber = 1);
    Q_INVOKABLE QList<LMM_Assignment *> getAssignments();
    Q_INVOKABLE QVariant getAssignmentsVariant();

    Q_INVOKABLE SortFilterProxyModel *getChairmanList();
    Q_INVOKABLE SortFilterProxyModel *getPrayerList();

    void loadAssignments(bool includeAllOptions = false);
    void createAssignments();
    QList<LMM_Schedule *> findSchedules(QDate firstDayOfWeek = QDate());

    Q_INVOKABLE QColor monthlyColor();
    Q_INVOKABLE QString monthlyRGBColor();
    Q_INVOKABLE QColor monthlyLightColor();

signals:
    void dateChanged(QDate date);
    void chairmanChanged(person *chairman);
    void counselor2Changed(person *counselor);
    void counselor3Changed(person *counselor);
    void prayerBeginningChanged(person *prayer);
    void prayerEndChanged(person *prayer);
    void songBeginningChanged(int song);
    void songMiddleChanged(int song);
    void songEndChanged(int song);
    void bibleReadingChanged(QString source);
    void openingCommentsChanged(QString source);
    void closingCommentsChanged(QString source);
    void classesChanged(int classes);
    void startTimeChanged();
    void notesChanged(QString notes);

public slots:

private:
    QDate m_date;
    int m_meeting_id;
    int m_songBeginning;
    int m_songMiddle;
    int m_songEnd;
    int m_classes;
    bool m_delayLoadingDetails;
    QList<LMM_Assignment *> m_talks;
    person *m_chairman;
    person *m_counselor2;
    person *m_counselor3;
    person *m_prayer_beginning;
    person *m_prayer_end;
    QString m_biblereading;
    QString m_openingcomments;
    QString m_closingcomments;
    QString m_notes;

    SortFilterProxyModel *getBrotherList(int usefor);
    QString getSongTitle(int song) const;
};

class LMMeetingValidator : public QValidator
{
    Q_OBJECT
    Q_PROPERTY(LMM_Meeting *meeting READ meeting WRITE setMeeting NOTIFY meetingChanged)
    Q_PROPERTY(LMMeetingValidator::Field field READ field WRITE setField NOTIFY fieldChanged)
public:
    enum Field {
        None = 0,
        Chairman = 1,
        Counselor2,
        Counselor3,
        OpeningPrayer,
        FinalPrayer
    };
    Q_ENUM(Field)

    LMMeetingValidator(QObject *parent = nullptr);
    ~LMMeetingValidator();

    QValidator::State validate(QString &input, int &pos) const override;

    LMM_Meeting *meeting() const;
    void setMeeting(LMM_Meeting *newMeeting);
    LMMeetingValidator::Field field() const;
    void setField(Field newField);

Q_SIGNALS:
    void meetingChanged();
    void fieldChanged();
    void errorChanged(const QString &error) const;

private:
    LMM_Meeting *m_meeting;
    Field m_field;
};
#endif // LMM_MEETING_H
