#ifndef CONGREGATIONDETAIL_H
#define CONGREGATIONDETAIL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include <QtMath>
#include "assignmentInfo.h"
#include "qabstractproxymodel.h"
#include "ccongregation.h"

class CongregationDetail : public QObject
{
    Q_OBJECT
public:
    CongregationDetail();
    CongregationDetail(const int congregationId, const QString congregationName,
                       const QDate date, const QString theme,
                       const int assigneeId, const QString assigneeFirstName, const QString assigneeLastName,
                       const QDate previous1Date, const QString previous1Theme,
                       const int previous1AssigneeId, const QString previous1assigneeFirstName, const QString previous1assigneeLastName,
                       const QDate previous2Date, const QString previous2Theme,
                       const int previous2AssigneeId, const QString previous2assigneeFirstName, const QString previous2assigneeLastName,
                       const int assignmentCount, const int assignmentFrequencyRange,
                       const int timeRange,
                       QObject *parent = nullptr);

    int congregationId() const;
    const QString &congregationName() const;

    QDate date() const;
    AssignmentInfo *assignmentInfo() const;
    QString theme() const;

    int assigneeId() const;
    QString assigneeFirstName() const;
    QString assigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString assigneeFullName(QString format = "FirstName LastName");

    QDate previous1Date() const;
    AssignmentInfo *previous1AssignmentInfo() const;
    QString previous1Theme() const;

    QString previous1AssigneeFirstName() const;
    QString previous1AssigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous1AssigneeFullName(QString format = "FirstName LastName");

    QDate previous2Date() const;
    AssignmentInfo *previous2AssignmentInfo() const;
    QString previous2Theme() const;

    QString previous2AssigneeFirstName() const;
    QString previous2AssigneeLastName() const;
    /**
     * @brief fullname - Get fullname
     * @param format - Name format (optional: if parameter is omitted, default value 'FirstName LastName' will be used.)
     *                 Options are:
     *                 + FirstName LastName
     *                 + LastName FirstName
     *                 + LastName, FirstName
     * @return - Fullname
     */
    QString previous2AssigneeFullName(QString format = "FirstName LastName");

    int assignmentCount() const;
    int assignmentFrequencyRange() const;

    int timeRange() const;

signals:
    void notification();

private:
    int m_congregationId;
    QString m_congregationName;
    QDate m_date;
    AssignmentInfo *m_assignmentInfo;
    QString m_theme;
    int m_assigneeId;
    QString m_assigneeFirstName;
    QString m_assigneeLastName;
    QDate m_previous1Date;
    AssignmentInfo *m_previous1AssignmentInfo;
    QString m_previous1Theme;
    int m_previous1AssigneeId;
    QString m_previous1AssigneeFirstName;
    QString m_previous1AssigneeLastName;
    QDate m_previous2Date;
    AssignmentInfo *m_previous2AssignmentInfo;
    QString m_previous2Theme;
    int m_previous2AssigneeId;
    QString m_previous2AssigneeFirstName;
    QString m_previous2AssigneeLastName;
    int m_assignmentCount;
    int m_assignmentFrequencyRange;
    int m_timeRange;

    QString getFullName(QString firstName, QString lastName, QString format = "FirstName LastName");
};

class CongregationDetailModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
    Q_PROPERTY(int congregationCount READ congregationCount)
    Q_PROPERTY(int minAssignmentCount READ minAssignmentCount)
    Q_PROPERTY(int maxAssignmentCount READ maxAssignmentCount)
public:
    explicit CongregationDetailModel(QObject *parent = nullptr);
    ~CongregationDetailModel();

    enum Roles {
        CongregationIdRole = Qt::UserRole,
        CongregationNameRole,
        AlphabetRole,
        DateRole,
        YearRole,
        ThemeRole,
        AssigneeFullNameRole,
        Previous1DateRole,
        Previous1ThemeRole,
        Previous1AssigneeFullNameRole,
        Previous2DateRole,
        Previous2ThemeRole,
        Previous2AssigneeFullNameRole,
        AssignmentCountRole,
        AssignmentFrequencyRangeRole,
        TimeRangeRole
    };

    enum class GroupByRoles : quint16 {
        AlphabetRole = quint16(Roles::AlphabetRole),
        YearRole = quint16(Roles::YearRole),
        ThemeRole = quint16(Roles::ThemeRole),
        AssignmentFrequencyRangeRole = quint16(Roles::AssignmentFrequencyRangeRole),
        TimeRangeRole = quint16(Roles::TimeRangeRole)
    };

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role) const;

    Q_INVOKABLE QVariantMap get(int row) const;

    void addCongregationDetail(CongregationDetail *congregationDetail);
    Q_INVOKABLE void clear();
    Q_INVOKABLE void loadCongregationDetails(bool includeOwnCongregation = true, QDate date = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek() - 1) * -1));
    int congregationCount() { return m_congregationCount; }
    int minAssignmentCount() { return m_minAssignmentCount; }
    int maxAssignmentCount() { return m_maxAssignmentCount; }

signals:
    void modelChanged();

private:
    AssignmentInfos *assignmentInfos;
    QList<CongregationDetail *> congregationDetails;
    int m_congregationCount;
    int m_minAssignmentCount;
    int m_maxAssignmentCount;
    QString displayNameFormat;

    void updateModel(QDate date, const sql_items &assignmentRows);
};

class CongregationDetailSFProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *source READ source WRITE setSource)
    Q_PROPERTY(QString filterText READ filterText WRITE setFilterText NOTIFY filterChanged)
    Q_PROPERTY(QByteArray sortRole READ sortRole WRITE setSortRole NOTIFY sortChanged)
    Q_PROPERTY(QByteArray groupByRole READ groupByRole WRITE setGroupByRole NOTIFY groupByChanged)

public:
    CongregationDetailSFProxyModel(QObject *parent = nullptr);

    QObject *source() const;
    void setSource(QObject *source);

    QString filterText() const;
    void setFilterText(QString value);

    QByteArray sortRole() const;
    void setSortRole(const QByteArray &role);

    QByteArray groupByRole() const;
    void setGroupByRole(const QByteArray &role);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    int roleKey(const QByteArray &role) const;

private:
    QString m_filterText;
    QByteArray m_groupByRole;

signals:
    void filterChanged();
    void sortChanged();
    void groupByChanged();
};

#endif // CONGREGATIONDETAIL_H
