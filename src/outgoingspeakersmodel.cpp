/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "outgoingspeakersmodel.h"

OutgoingSpeakersModel::OutgoingSpeakersModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

OutgoingSpeakersModel::~OutgoingSpeakersModel()
{
    qDeleteAll(_list);
    _list.clear();
}

int OutgoingSpeakersModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _list.empty() ? 0 : _list.count();
}

int OutgoingSpeakersModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QHash<int, QByteArray> OutgoingSpeakersModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[IdRole] = "id";
    items[SpeakerRole] = "speaker";
    items[SpeakerIdRole] = "speakerId";
    items[CongregationRole] = "congregation";
    items[CongregationIdRole] = "congregationId";
    items[CongregationInfo] = "congregationInfo";
    items[CongregationAddress] = "congregationAddress";
    items[TimeRole] = "time";
    items[ThemeRole] = "theme";
    items[ThemeNoRole] = "themeNo";
    items[ThemeIdRole] = "themeId";
    items[DateRole] = "date";
    return items;
}

QVariant OutgoingSpeakersModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case IdRole:
        return _list[index.row()]->id();
    case SpeakerRole:
        return _list[index.row()]->speaker() ? _list[index.row()]->speaker()->fullname() : "";
    case SpeakerIdRole:
        return _list[index.row()]->speaker() ? _list[index.row()]->speaker()->id() : -1;
    case CongregationRole:
        return _list[index.row()]->congregation().id > 0 ? _list[index.row()]->congregation().name : "";
    case CongregationIdRole:
        return _list[index.row()]->congregation().id;
    case CongregationAddress:
        return _list[index.row()]->congregation().address;
    case CongregationInfo:
        return _list[index.row()]->congregation().info;
    case CongregationCircuit:
        return _list[index.row()]->congregation().circuit;
    case TimeRole:
        return _list[index.row()]->congregation().getPublicmeeting(_date).getMeetingtime();
    case ThemeRole:
        return _list[index.row()]->theme().theme;
    case ThemeNoRole:
        return _list[index.row()]->theme().number;
    case ThemeIdRole:
        return _list[index.row()]->theme().id;
    case DateRole:
        return _list[index.row()]->date().startOfDay();
    default:
        return QVariant();
    }
}

QVariantMap OutgoingSpeakersModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

void OutgoingSpeakersModel::loadList(const QDate d)
{
    beginResetModel();
    qDeleteAll(_list);
    _list.clear();
    _date = QDate(d);
    cpublictalks c;
    _list = c.getOutgoingSpeakers(d);
    endResetModel();
    emit modelChanged();
}

void OutgoingSpeakersModel::addRow(const int speakerId, const int congId, const int themeId)
{
    if (!_date.isValid())
        return;
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    cpublictalks c;
    _list.append(c.addOutgoingSpeaker(_date, speakerId, themeId, congId));
    endInsertRows();
    emit modelChanged();
}

void OutgoingSpeakersModel::removeRow(const int rowIndex)
{
    beginRemoveRows(QModelIndex(), rowIndex, rowIndex);
    cpublictalks c;
    c.removeOutgoingSpeaker(get(rowIndex)["id"].toInt());
    _list.removeAt(rowIndex);
    endRemoveRows();
    emit modelChanged();
}

void OutgoingSpeakersModel::editRow(const int row, const int speakerId, const int themeId, const int congId)
{
    cpublictalks c;
    beginResetModel();
    _list[row]->setSpeaker(cpersons::getPerson(speakerId));
    _list[row]->setTheme(c.getThemeById(themeId));
    _list[row]->setCongregation(congId);
    _list[row]->save();
    endResetModel();
    emit modelChanged();
}

bool OutgoingSpeakersModel::moveToWeek(const int row, const QDateTime selectedDate)
{
    if (!_list[row]->speaker())
        return false;

    if (!selectedDate.isValid())
        return false;
    // use first day of week
    QDate _selectedDate = selectedDate.offsetFromUtc() < 0 ? selectedDate.toUTC().date() : selectedDate.date();
    QDate newDate = _selectedDate.addDays(1 - _selectedDate.dayOfWeek());
    if (newDate == _list[row]->date()) {
        return false;
    }

    beginResetModel();
    // check for existing scheduled talk in same congregation
    cpublictalks cp;
    QList<cpoutgoing *> outgoing = cp.getOutgoingSpeakers(newDate);
    foreach (cpoutgoing *ot, outgoing) {
        if (ot->congregation().id == _list[row]->congregation().id) {
            // conflict
            QDate d2 = ot->congregation().getPublicmeeting(newDate).getMeetingDate(newDate);
            QMessageBox msgbox;
            msgbox.setText(tr("The destination date already has a talk scheduled.") + QString("\n%1\n%2\n%3").arg(QLocale().toString(d2, QLocale::ShortFormat), ot->speaker() ? ot->speaker()->fullname() : "", ot->theme().theme));
            QPushButton *swapButton = msgbox.addButton(tr("Swap Talks", "Button text"), QMessageBox::YesRole);
            msgbox.addButton(tr("Cancel", "Button text"), QMessageBox::RejectRole);
            msgbox.exec();
            if (msgbox.clickedButton() == swapButton) {
                ot->setWeekOf(_list[row]->weekOf());
                ot->save();
            } else
                return false;
            break;
        }
    }
    _list[row]->setWeekOf(newDate);
    _list[row]->save();
    endResetModel();
    emit movedToWeek();
    return true;
}

bool OutgoingSpeakersModel::moveToTodo(const int row)
{
    if (!_list[row]->speaker())
        return false;
    todo t(false);
    t.setCongregationName(_list[row]->congregation().name);
    t.setSpeakerFullName(_list[row]->speaker()->fullname());
    t.setThemeAndNumber(QString("%1 (%2)").arg(_list[row]->theme().theme,
                                               QVariant(_list[row]->theme().number).toString()));
    t.setNotes(tr("From %1").arg(_list[row]->date().toString(Qt::ISODate)));
    t.save();
    removeRow(row);
    emit movedTodoList();
    return true;
}

OutgoingSpeakerValidator::OutgoingSpeakerValidator(QObject *parent)
    : QValidator(parent), m_model(nullptr), m_row(-1), m_role(OutgoingSpeakersModel::Roles::None)
{
}

OutgoingSpeakerValidator::~OutgoingSpeakerValidator()
{
}

QValidator::State OutgoingSpeakerValidator::validate(QString &input, int &pos) const
{
    Q_UNUSED(pos)

    if (input.isEmpty()) {
        emit errorChanged("");
        return Acceptable;
    }
    if (!model()) {
        emit errorChanged("");
        return Acceptable;
    }
    if (role() == OutgoingSpeakersModel::Roles::None) {
        emit errorChanged("");
        return Acceptable;
    }
    if (row() < 0 || model()->rowCount() <= row()) {
        emit errorChanged("");
        return Acceptable;
    }

    QDate meetingDate = model()->data(model()->index(row(), 0), OutgoingSpeakersModel::Roles::DateRole).toDate();
    if (!meetingDate.isValid()) {
        emit errorChanged("");
        return Intermediate;
    }
    if (meetingDate < QDate::currentDate()) {
        // do not validate historical assignments
        emit errorChanged("");
        return Acceptable;
    }

    cpublictalks cpt;
    int themeId = model()->data(model()->index(row(), 0), OutgoingSpeakersModel::Roles::ThemeIdRole).toInt();
    if (role() == OutgoingSpeakersModel::Roles::ThemeRole) {
        cpttheme pt = cpt.getThemeById(themeId);
        if (pt.discontinueDate.isValid() && pt.discontinueDate < meetingDate) {
            emit errorChanged(tr("This talk has been discontinued. Please choose another talk."));
            return Invalid;
        }
    }

    int speakerId = model()->data(model()->index(row(), 0), OutgoingSpeakersModel::Roles::SpeakerIdRole).toInt();
    cpersons cp;
    person *assignee = cp.getPerson(speakerId);
    if (assignee) {
        if (role() == OutgoingSpeakersModel::Roles::CongregationRole) {
            emit errorChanged("");
            return Acceptable;
        }

        if (role() == OutgoingSpeakersModel::Roles::ThemeRole) {
            sql_items speakersThemes = cpt.getThemeList(assignee);
            auto iterator = std::find_if(speakersThemes.begin(), speakersThemes.end(),
                                         [&themeId](const sql_item &themeItem) { return themeItem.value("theme_id").toInt() == themeId; });
            if (iterator == speakersThemes.end()) {
                emit errorChanged(tr("This talk cannot be delivered by the speaker. Please choose another talk or speaker."));
                return Invalid;
            }
            emit errorChanged("");
            return Acceptable;
        }

        if (!input.startsWith(assignee->fullname())) {
            emit errorChanged("");
            return Intermediate;
        }

        // check admission
        if (!(assignee->usefor())) {
            emit errorChanged(tr("Unassignable. Please check the speaker's settings."));
            return Invalid;
        }

        // check availability
        bool isUnavailable = (person::UseFor::IsBreak & assignee->usefor()) > 0;
        if (!isUnavailable) {
            QList<QPair<QDate, QDate>> unavailabilityList = assignee->getUnavailabilities();
            if (!unavailabilityList.empty()) {
                auto iterator = std::find_if(unavailabilityList.begin(), unavailabilityList.end(),
                                             [&meetingDate](const QPair<QDate, QDate> &unavailabilityItem) {
                                                 return unavailabilityItem.first <= meetingDate
                                                         && unavailabilityItem.second >= meetingDate;
                                             });
                isUnavailable = iterator != unavailabilityList.end();
            }
        }
        if (isUnavailable) {
            emit errorChanged(tr("The assigned person is not available on this day."));
            return Invalid;
        }

        // check additional assignment in the same month
        QDate weekCommencingDate = meetingDate.addDays(-meetingDate.dayOfWeek() + 1);
        tbAvailability::WeekendMeetingAvailabilityChecker checker(meetingDate, weekCommencingDate);
        tbAvailability::Availability a = checker.GetLocalPublicSpeaker();

        tbAvailability::AvailabilityItem *item = a.GetItemByPersonId(assignee->id());
        if (item && item->IsSpeakerAwayInSameMonth) {
            emit errorChanged(tr("It is preferred to assign speakers to be away only once a month."));
            return Invalid;
        }
        emit errorChanged("");
        return Acceptable;
    }
    emit errorChanged("");
    return Acceptable;
}

OutgoingSpeakersModel *OutgoingSpeakerValidator::model() const
{
    return m_model;
}

void OutgoingSpeakerValidator::setModel(OutgoingSpeakersModel *newModel)
{
    if (m_model == newModel)
        return;
    m_model = newModel;
    emit modelChanged();
}

int OutgoingSpeakerValidator::row() const
{
    return m_row;
}

void OutgoingSpeakerValidator::setRow(int newRow)
{
    if (m_row == newRow)
        return;
    m_row = newRow;
    emit rowChanged();
}

OutgoingSpeakersModel::Roles OutgoingSpeakerValidator::role() const
{
    return m_role;
}

void OutgoingSpeakerValidator::setRole(OutgoingSpeakersModel::Roles newRole)
{
    if (m_role == newRole)
        return;
    m_role = newRole;
    emit roleChanged();
}
