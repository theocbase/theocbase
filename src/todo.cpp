/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "todo.h"
#include "qmath.h"

QList<todo *> todo::getList()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QList<todo *> results;
    sql_items items = sql->selectSql("select * from publictalks_todolist where active order by inout, id");
    for (unsigned int i = 0; i < items.size(); i++) {
        sql_item item = items[i];
        todo *t = new todo(item.value("id").toInt(), item.value("inout").toString() == "I");
        t->setId(item.value("id").toInt());
        t->setDate(item.value("date").toDate());
        t->setSpeakerFullName(item.value("speaker").toString());
        t->setCongregationName(item.value("congregation").toString());
        t->setThemeAndNumber(item.value("theme").toString());
        t->setNotes(item.value("notes").toString());
        results.append(t);
    }
    return results;
}

bool todo::addScheduledTalks(int speakerId, bool removed)
{
    // move scheduled talks to To Do List
    // (when speaker moves to another congregation or is no longer speaker)
    sql_class *sql = &Singleton<sql_class>::Instance();
    ccongregation c;
    cpublictalks cpt;
    cptmeeting *meeting;
    person *speaker = cpersons::getPerson(QVariant(speakerId).toInt());
    QDate startDate = QDateTime::currentDateTime().date().addDays(-7);

    bool toDoListUpdated = false;

    // incoming talks: move to To Do List only if speaker is removed
    if (removed) {
        sql_items incomingTalks;

        incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

        if (!incomingTalks.empty()) {
            // move incoming talks
            for (unsigned int i = 0; i < incomingTalks.size(); i++) {
                int themeId = incomingTalks[i].value("theme_id").toInt();
                QDate scheduledDate = incomingTalks[i].value("date").toDate();
                cpttheme theme = cpt.getThemeById(themeId);

                todo t(true);
                t.setCongregationName(speaker->congregationName());
                t.setSpeakerFullName(speaker->fullname());
                t.setThemeAndNumber(theme.theme);
                t.setNotes(QObject::tr("From %1; speaker removed", "From [scheduled date]; speaker removed").arg(scheduledDate.toString(Qt::ISODate)));
                t.save();

                meeting = cpt.getMeeting(scheduledDate);
                meeting->theme = cpttheme();
                meeting->setSpeaker(nullptr);
                meeting->save();

                toDoListUpdated = true;
            }
        }
    }

    sql_items outgoingTalks;
    outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND speaker_id = " + QVariant(speakerId).toString() + " AND active");

    if (!outgoingTalks.empty()) {
        for (unsigned int i = 0; i < outgoingTalks.size(); i++) {
            // move outgoing talks
            int outgoingId = outgoingTalks[i].value("id").toInt();
            QDate scheduledDate = outgoingTalks[i].value("date").toDate();
            meeting = cpt.getMeeting(scheduledDate);

            if (cpt.removeOutgoingSpeaker(outgoingId)) {
                int outCongId = outgoingTalks[i].value("congregation_id").toInt();
                ccongregation::congregation cong = c.getCongregationById(outCongId);
                int themeId = outgoingTalks[i].value("theme_id").toInt();
                cpttheme theme = cpt.getThemeById(themeId);

                todo t(false);
                t.setCongregationName(cong.name);
                t.setSpeakerFullName(speaker->fullname());
                t.setThemeAndNumber(theme.theme);
                if (removed) {
                    // speaker removed
                    t.setNotes(QObject::tr("From %1; speaker removed", "From [scheduled date]; speaker removed").arg(scheduledDate.toString(Qt::ISODate)));
                } else {
                    // speaker moved to other congregation
                    t.setNotes(QObject::tr("From %1; speaker moved to %2", "From [scheduled date]; speaker moved to [new congregation]").arg(scheduledDate.toString(Qt::ISODate), speaker->congregationName()));
                }
                t.save();

                toDoListUpdated = true;
            }
        }
    }

    return toDoListUpdated;
}

bool todo::addDiscontinuedTalks(const QDate startDate, const int themeId)
{
    // move scheduled talks to To Do List
    // (when talk is discontinued)
    sql_class *sql = &Singleton<sql_class>::Instance();
    ccongregation c;
    cpublictalks cpt;
    cptmeeting *meeting;

    bool toDoListUpdated = false;

    // check for scheduled talks

    sql_items incomingTalks;
    incomingTalks = sql->selectSql("SELECT * from publicmeeting WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND theme_id = " + QVariant(themeId).toString() + " AND active");

    if (!incomingTalks.empty()) {
        // move incoming talks
        for (unsigned int i = 0; i < incomingTalks.size(); i++) {
            QDate scheduledDate = incomingTalks[i].value("date").toDate();
            cpttheme theme = cpt.getThemeById(themeId);

            int speakerId = incomingTalks[i].value("speaker_id").toInt();
            person *speaker = cpersons::getPerson(QVariant(speakerId).toInt());

            todo t(true);
            t.setDate(scheduledDate);
            if (speaker != nullptr) {
                t.setCongregationName(speaker->congregationName());
                t.setSpeakerFullName(speaker->fullname());
            }
            t.setThemeAndNumber(theme.theme);
            t.setNotes(QObject::tr("From %1; talk discontinued", "From [scheduled date]; talk discontinued").arg(scheduledDate.toString(Qt::ISODate)));
            t.save();

            meeting = cpt.getMeeting(scheduledDate);
            meeting->theme = cpttheme();
            meeting->setSpeaker(nullptr);
            meeting->save();

            toDoListUpdated = true;
        }
    }

    sql_items outgoingTalks;
    outgoingTalks = sql->selectSql("SELECT * from outgoing WHERE date > '" + startDate.toString(Qt::ISODate) + "' AND theme_id = " + QVariant(themeId).toString() + " AND active");

    if (!outgoingTalks.empty()) {
        for (unsigned int i = 0; i < outgoingTalks.size(); i++) {
            // move outgoing talks
            int outgoingId = outgoingTalks[i].value("id").toInt();
            QDate scheduledDate = outgoingTalks[i].value("date").toDate();
            meeting = cpt.getMeeting(scheduledDate);

            if (cpt.removeOutgoingSpeaker(outgoingId)) {
                int outCongId = outgoingTalks[i].value("congregation_id").toInt();
                ccongregation::congregation cong = c.getCongregationById(outCongId);

                int speakerId = outgoingTalks[i].value("speaker_id").toInt();
                person *speaker = cpersons::getPerson(QVariant(speakerId).toInt());

                cpttheme theme = cpt.getThemeById(themeId);

                todo t(false);
                t.setDate(scheduledDate);
                t.setCongregationName(cong.name);
                t.setSpeakerFullName(speaker->fullname());
                t.setThemeAndNumber(theme.theme);
                t.setNotes(QObject::tr("From %1; talk discontinued", "From [scheduled date]; talk discontinued").arg(scheduledDate.toString(Qt::ISODate)));
                t.save();

                toDoListUpdated = true;
            }
        }
    }

    return toDoListUpdated;
}

todo::todo(bool isIncoming)
    : m_id(-1), m_isIncoming(isIncoming), m_speaker(nullptr), m_congregationId(0)
{
    sql = &Singleton<sql_class>::Instance();
    sql_item s;
    s.insert("inout", isIncoming ? "I" : "O");
    setId(sql->insertSql("publictalks_todolist", &s, "id"));
}

todo::todo(int id, bool isIncoming)
    : m_id(id), m_isIncoming(isIncoming), m_speaker(nullptr), m_congregationId(0)
{
    sql = &Singleton<sql_class>::Instance();
}

bool todo::isIncoming() const
{
    return m_isIncoming;
}

int todo::id() const
{
    return m_id;
}

void todo::setId(int newId)
{
    m_id = newId;
}

QDate todo::date() const
{
    return m_date;
}

void todo::setDate(const QDate &newDate)
{
    if (newDate.isValid()) {
        // set to Monday
        m_date = newDate.addDays(1 - newDate.dayOfWeek());
    } else
        m_date = newDate;
}

int todo::timeRange(int minTimeRange, int maxTimeRange, double timeRangeUnit) const
{
    int timeRange = date().isValid()
            ? qFloor((double)QDate().currentDate().daysTo(date()) / timeRangeUnit)
            : 5;
    timeRange = qMax(timeRange, minTimeRange);
    timeRange = qMin(timeRange, maxTimeRange);
    return timeRange;
}

QString todo::speakerFullName() const
{
    return m_speakerFullName;
}

void todo::setSpeakerFullName(const QString &newSpeakerFullName)
{
    m_speakerFullName = newSpeakerFullName;
    if (isIncoming())
        m_speaker.reset(cpersons::getPerson(speakerFullName(), "partial", congregationId()));
    else
        m_speaker.reset(cpersons::getPerson(speakerFullName(), "partial", 0));
}

QString todo::congregationName() const
{
    return m_congregationName;
}

void todo::setCongregationName(const QString &newCongregationName)
{
    m_congregationName = newCongregationName;
    ccongregation c;
    if (!congregationName().isEmpty())
        m_congregationId = c.getCongregationIDByPartialName(congregationName());
    else
        m_congregationId = 0;
}

const QString &todo::themeAndNumber() const
{
    return m_themeAndNumber;
}

void todo::setThemeAndNumber(const QString &newThemeAndNumber)
{
    m_themeAndNumber = newThemeAndNumber;
    QRegularExpression re(R"(.* \((\d+)\))");
    QRegularExpressionMatch match = re.match(m_themeAndNumber);
    if (match.hasMatch()) {
        QString themeNumber = match.captured(1);
        cpublictalks cpt;
        m_theme = cpt.getThemeByNumber(QVariant(themeNumber).toInt(), date().isValid() ? date() : QDate::currentDate());
    }
}

const cpttheme &todo::theme() const
{
    return m_theme;
}

QString todo::notes() const
{
    return m_notes;
}

void todo::setNotes(const QString &newNotes)
{
    m_notes = newNotes;
}

int todo::congregationId() const
{
    return m_congregationId;
}

person *todo::speaker()
{
    return m_speaker.data();
}

void todo::deleteme()
{
    sql_item s;
    s.insert("active", 0);
    sql->updateSql("publictalks_todolist", "id", QString::number(id()), &s);
    setId(-1);
}

void todo::save()
{
    sql_item s;
    s.insert("inout", m_isIncoming ? "I" : "O");
    s.insert("date", date());
    s.insert("speaker", speakerFullName());
    s.insert("congregation", congregationName());
    s.insert("theme", themeAndNumber());
    s.insert("notes", notes());
    s.insert("active", 1);
    sql->updateSql("publictalks_todolist", "id", QVariant(id()).toString(), &s);
}

QString todo::moveToSchedule()
{
    QString errs;
    if (!date().isValid()) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Date"));
    } else {
        // set to Monday
        setDate(date().addDays(1 - date().dayOfWeek()));
    }

    if (isIncoming())
        m_speaker.reset(cpersons::getPerson(speakerFullName(), "partial", congregationId()));
    else
        m_speaker.reset(cpersons::getPerson(speakerFullName(), "partial", 0));
    if (!m_speaker) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Speaker", "Public talk"));
    } else if (isIncoming()) {
        m_congregationId = m_speaker->congregationid();
    }
    if (m_congregationId == 0) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Congregation"));
    }

    cpublictalks cp;
    if (theme().id <= 0) {
        if (!errs.isEmpty())
            errs.append(", ");
        errs.append(QObject::tr("Theme", "Public talk"));
    }

    if (!errs.isEmpty())
        return errs;

    cptmeeting *talk = cp.getMeeting(date());
    if (isIncoming()) {
        // check for existing scheduled talk
        if (talk->speaker()) {
            errs = QObject::tr("Date");
            return errs;
        }

        talk->setSpeaker(speaker());
        talk->theme = theme();
    } else {
        // check for existing scheduled talk in same congregation
        QList<cpoutgoing *> outgoing = cp.getOutgoingSpeakers(date());
        foreach (cpoutgoing *ot, outgoing) {
            if (ot->congregation().id == congregationId()) {
                errs = QObject::tr("Date");
                return errs;
            }
        }

        cp.addOutgoingSpeaker(date(), speaker()->id(), theme().id, congregationId());
    }
    talk->save();
    deleteme();
    return "";
}
