#include "family.h"

family::family(QObject *parent)
    : QObject(parent)
{
    sql = &Singleton<sql_class>::Instance();
    mId = -1;
    mHeadId = -1;
}

QString const& family::getName() const
{
    return mName;
}

void family::setName(QString const& newName)
{
    if (newName != mName) {
        mName = newName;
        emit nameChanged(newName);
    }
}

void family::setName(QString const& firstName, QString const& lastName)
{
    // family name was previously = firstName + " " + lastName
    QString newName = lastName + ", " + firstName;
    if (newName != mName) {
        mName = newName;
        emit nameChanged(newName);
    }
}

int family::getHeadId()
{
    return mHeadId;
}

void family::setHeadId(int headId)
{
    mHeadId = headId;
    emit headIdChanged(headId);
}

bool family::addMember(int person_id)
{
    sql_item queryvalue;
    queryvalue.insert(":person_id",person_id);
    int id = sql->selectScalar("SELECT id from families where person_id = :person_id", &queryvalue, -1).toInt();
    sql_item insertvalue;
    insertvalue.insert("person_id",person_id);
    insertvalue.insert("family_head",getHeadId());
    if (id < 1){
        // new
        return (sql->insertSql("families",&insertvalue,"id") > 0);
    }else{
        // update
        return sql->updateSql("families","person_id",QString::number(person_id),&insertvalue);
    }
}

bool family::removeMember(int person_id)
{
    sql_item queryvalue;
    queryvalue.insert(":person_id",person_id);
    int id = sql->selectScalar("SELECT id from families where person_id = :person_id", &queryvalue, -1).toInt();
    if (id > 0){
        sql_item updatevalue;
        updatevalue.insert("family_head",-1);
        return sql->updateSql("families","person_id",QString::number(person_id),&updatevalue);
    }else{
        return true;
    }
}

bool family::removeFamily()
{
    sql_item value;
    value.insert("family_head",-1);
    return sql->updateSql("families","family_head",QString::number(getHeadId()),&value);
}


QList<person *> family::getMembers()
{
    QList<person *> list;
    sql_item queryitem;
    queryitem.insert(":family_head",mHeadId);

    sql_items plist = sql->selectSql("SELECT person_id FROM families WHERE family_head = :family_head",&queryitem);
    for (unsigned int i = 0; i < plist.size(); i++){
        person *p = cpersons::getPerson(plist[i].value("person_id").toInt());
        if (p){
            p->setParent(this);
            list.append(p);
        }
    }
    return list;
}

QList<family *> family::getFamilies()
{
    QList<family *> list;
    sql_class *s_sql = &Singleton<sql_class>::Instance();
    sql_items f_items = s_sql->selectSql("SELECT families.family_head, persons.firstname, persons.lastname "
                                         "FROM families LEFT JOIN persons ON families.family_head = persons.id "
                                         "WHERE family_head > 0 GROUP BY family_head");
    for (unsigned int i = 0; i<f_items.size(); i++) {
        family *f = new family();
        f->setHeadId(f_items[i].value("family_head").toInt());
        f->setName(f_items[i].value("firstname").toString(), f_items[i].value("lastname").toString());
        list.append(f);
    }
    std::sort(list.begin(), list.end(), [](family const* a, family const* b){
        return QString::localeAwareCompare(a->getName(), b->getName()) < 0;
    });
    return list;
}

QVariant family::getFamiliesVariantList()
{
    QList<QObject*> tmpList;
    family *empty = new family(this);    
    empty->setName(tr("Not set"));
    tmpList.append(empty);
    for(family *f : getFamilies()){
        tmpList.append(f);
    }
    return QVariant::fromValue(tmpList);
}

family *family::getPersonFamily(int person_id)
{
    sql_item queryitem;
    queryitem.insert(":person_id",person_id);
    sql_class *s_sql = &Singleton<sql_class>::Instance();
    sql_items f = s_sql->selectSql("SELECT families.family_head, persons.firstname, persons.lastname "
                                 "FROM families LEFT JOIN persons ON families.family_head = persons.id WHERE person_id = :person_id",&queryitem);
    if (f.size() > 0){
        sql_item item = f[0];

        if (item.value("family_head").toInt() < 1)
            return nullptr;

        family *fm = new family();
        fm->setHeadId(item.value("family_head").toInt());
        fm->setName(item.value("firstname").toString(), item.value("lastname").toString());
        return fm;
    }else{
        return nullptr;
    }
}

family *family::getOrAddFamily(int headId)
{
    family *fm;
    fm = new family();
    fm->setHeadId(headId);
    fm->addMember(headId);
    fm = family::getPersonFamily(headId);
    return fm;
}

