/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpersons.h"

cpersons::cpersons(QObject *parent)
    : QObject(parent)
{
}

person *cpersons::test()
{
    person *p = new person();
    p->setId(2);
    p->setLastname("JIJWKHFHWJ");
    return p;
}

// TODO: examine usage of getAllPersons for resource leaks

// get all persons
// param type: 0 = all persons from current congregation
//             1 = only public talk speakers
//             2 = only public talk speakers in own congregation
//             3 = all persons who may share in public meeting
// return QList of person objects
QList<person *> cpersons::getAllPersons(int type)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QList<person *> list;
    QString wheretext = "";

    switch (type) {
    case 0:
        wheretext = " WHERE (congregation_id IS NULL OR congregation_id = " + sql->getSetting("congregation_id") + ")";
        break;

    case 1:
        wheretext = " WHERE usefor & 512";
        break;

    case 2:
        wheretext = " WHERE (congregation_id IS NULL OR congregation_id = " + sql->getSetting("congregation_id") + ") AND usefor & 512";
        break;

    case 3:
        wheretext = " WHERE usefor & 704";
        break;
    }

    Q_ASSERT(!wheretext.isEmpty());

    sql_items allp = sql->selectSql("SELECT * FROM persons" + wheretext + " AND active ORDER BY lastname, firstname");

    for (std::vector<sql_item>::iterator i = allp.begin(); i != allp.end(); ++i) {
        list.push_back(createPerson(*i, sql));
    }

    return list;
}

person *cpersons::createPerson(const sql_item &item, sql_class *sql)
{
    person *p = new person(item.value("id").toInt(), item.value("uuid").toString());

    p->setLastname(item.value("lastname").toString());
    p->setFirstname(item.value("firstname").toString());
    p->setPhone(item.value("phone").toString());
    p->setMobile(item.value("mobile").toString());
    p->setEmail(item.value("email").toString());
    p->setServant(item.value("servant").toBool());
    p->setGender(item.value("gender").toString() == "S" ? person::Female : person::Male);
    p->setUsefor(item.value("usefor").toInt());
    p->setCongregationid(item.value("congregation_id").toInt() == 0 ? QVariant(sql->getSetting("congregation_id")).toInt() : item.value("congregation_id").toInt());
    p->setInfo(item.value("info").toString());
    p->setDirtyFlag(false);

    Q_ASSERT(p->parent() == NULL);
    return p;
}

int cpersons::addPerson(person *p)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item s;
    // now that sql_class::insertSql uses parameters, the apostrophes are not an issue
    s.insert("lastname", p->lastname());
    s.insert("firstname", p->firstname());
    s.insert("phone", p->phone());
    s.insert("mobile", p->mobile());
    s.insert("email", p->email());
    s.insert("servant", p->servant());
    s.insert("usefor", p->usefor());
    s.insert("gender", p->gender() == person::Male ? "B" : "S");
    s.insert("congregation_id", p->congregationid());
    s.insert("info", p->info());
    if (p->uuid() != "")
        s.insert("uuid", p->uuid());
    p->setId(sql->insertSql("persons", &s, "id"));
    return p->id();
}

// caller deletes
QList<person *> cpersons::getPersons(QString name,
                                     QString format /* = "FirstName LastName" */,
                                     int congregationId /* = 0 */,
                                     bool publicSpeakersOnly /* = false*/)
{
    QList<person *> result;

    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlcommand;
    sql_item values;
    QString formatted = "firstname || ' ' || lastname";

    if (format == "LastName FirstName") {
        formatted = "lastname || ' ' || firstname";
    } else if (format == "LastName, FirstName") {
        formatted = "lastname || ', ' || firstname";
    }

    if (format == "partial") {
        if (!name.isEmpty()) {
            // duplicate code (sigh) from cpublictalks::getspeakers. Refactor later.
            QStringList parts = name.split(" ", Qt::SkipEmptyParts);

            if (parts.length() == 1) {
                formatted = "(firstname like :name OR lastname like :name)";
                values.insert(":name", "%" + name + "%");
            } else {
                formatted = "firstname like :name1 AND lastname like :name2";
                values.insert(":name1", "%" + parts[0] + "%");
                values.insert(":name2", "%" + parts[1] + "%");
            }

            sqlcommand = QString("SELECT * FROM persons WHERE %1 AND active").arg(formatted);
        }
    } else {
        sqlcommand = QString("SELECT * FROM persons WHERE %1 = :name AND active").arg(formatted);
        values.insert(":name", name);
    }

    if (!sqlcommand.isEmpty() && congregationId > 0) {
        sqlcommand.append(" AND congregation_id = " + QVariant(congregationId).toString());
    }

    if (!sqlcommand.isEmpty() && publicSpeakersOnly) {
        sqlcommand.append(" AND usefor & 512");
    }

    if (!sqlcommand.isEmpty()) {
        sql_items ps = sql->selectSql(sqlcommand, &values);

        for (std::vector<sql_item>::iterator i = ps.begin(); i != ps.end(); ++i) {
            result.push_back(createPerson(*i, sql));
        }
    }

    return result;
}

// TODO: examine usage of getPerson for resource leaks
// caller deletes
person *cpersons::getPerson(QString name,
                            QString format /* = "FirstName LastName" */,
                            int congregationId /* = 0 */,
                            bool publicSpeakersOnly /* = false */)
{
    QList<person *> persons = getPersons(name, format, congregationId, publicSpeakersOnly);
    person *result = persons.empty() ? nullptr : persons[0];

    foreach (person *p, persons) {
        if (p != result) {
            delete p;
        }
    }
    return result;
}

person *cpersons::getPersonByEmail(QString email, int congregationId)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlcommand = "SELECT id FROM persons WHERE email LIKE '%" + email + "%'";
    if (congregationId > 0) {
        sqlcommand.append(" AND congregation_id = " + QVariant(congregationId).toString());
    }
    sqlcommand.append(" AND active");
    int personId = sql->selectScalar(sqlcommand, nullptr, -1).toInt();
    return getPerson(personId);
}

// caller deletes
person *cpersons::getPublicSpeaker(QString name,
                                   QString format /* = "FirstName LastName" */,
                                   int congregationId /* = 0 */)
{
    return getPerson(name, format, congregationId, true);
}

// TODO: examine usage of getPerson for resource leaks
// caller deletes
person *cpersons::getPerson(int id)
{
    if (id < 1)
        return nullptr;

    sql_class *sql = &Singleton<sql_class>::Instance();

    sql_items ps = sql->selectSql("persons", "id", QString::number(id), "");
    if (!ps.empty()) {
        Q_ASSERT(ps.size() == 1);
        return createPerson(ps[0], sql);
    }

    return nullptr;
}

bool cpersons::removePerson(int id)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    // remove person from families-table
    sql_item value;
    value.insert(":person_id", id);
    sql->execSql("UPDATE families SET family_head = -1, time_stamp = strftime('%s','now') WHERE person_id = :person_id OR family_head = :person_id", &value);
    // deactive person in persons-table
    sql_item s;
    s.insert("active", 0);
    return sql->updateSql("persons", "id", QString::number(id), &s);
    //return sql->removeSql("persons","id = " + QVariant(id).toString());
}

PersonsModel::PersonsModel(QObject *parent)
    : QAbstractTableModel(parent)
{
    // Get own congregation
    myCongregationId = c.getMyCongregation().id;
}

PersonsModel::~PersonsModel()
{
    qDeleteAll(_list);
    _list.clear();
}

int PersonsModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return _list.empty() ? 0 : _list.count();
}

int PersonsModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QHash<int, QByteArray> PersonsModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[IdRole] = "id";
    items[FullNameRole] = "fullName";
    items[IsBreakRole] = "isBreak";
    return items;
}

QVariant PersonsModel::data(const QModelIndex &index, int role) const
{
    if (_list.isEmpty())
        return QVariant();
    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return _list[index.row()]->id();
        case 1:
            return _list[index.row()]->fullname("LastName, FirstName");
        case 2:
            return _list[index.row()]->usefor() & person::IsBreak;
        }
    }

    switch (role) {
    case IdRole:
        return _list[index.row()]->id();
    case FullNameRole:
        return _list[index.row()]->fullname("LastName, FirstName");
    case IsBreakRole:
        return _list[index.row()]->usefor() & person::IsBreak;
    default:
        return QVariant();
    }
}

bool PersonsModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    person *p = _list[index.row()];

    if (role == Qt::EditRole) {
        int column = index.column();

        switch (column) {
        case 0:
            //id
            break;
        case 1:
            //fullName
            break;
        case 2:
            //break
            p->setUsefor(p->usefor() & ~person::IsBreak);
            break;
        default:
            break;
        }

        //emit editCompleted();
    } else {
        switch (role) {
        case IsBreakRole:
            if (value.toBool())
                p->setUsefor(p->usefor() & ~person::IsBreak);
            else
                p->setUsefor(p->usefor() | person::IsBreak);
            emit dataChanged(index, index, { role });
            break;
        default:
            break;
        }
    }
    return true;
}

QVariantMap PersonsModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QModelIndex PersonsModel::getPersonIndex(int personId) const
{
    for (int row = 0; row < this->rowCount(); ++row) {
        QModelIndex rowIndex = this->index(row, 0);
        if (rowIndex.data(IdRole) == personId)
            return rowIndex;
    }
    return QModelIndex();
}

void PersonsModel::loadList()
{
    beginResetModel();
    qDeleteAll(_list);
    _list.clear();
    cpersons cp;
    _list = cp.getAllPersons(0);
    endResetModel();
    emit modelChanged();
}

int PersonsModel::addPerson()
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());

    person *newPerson = nullptr;

    // create new
    newPerson = new person();
    newPerson->setGender(person::Male);
    newPerson->setServant(false);
    newPerson->setLastname(tr("Last name"));
    newPerson->setFirstname(tr("First name"));
    newPerson->setCongregationid(myCongregationId);

    cpersons *cp = new cpersons();
    int newId = cp->addPerson(newPerson);
    newPerson->setId(newId);

    _list.append(newPerson);
    endInsertRows();
    emit modelChanged();
    return newId;
}

bool PersonsModel::setBreak(const int row, const bool isBreak)
{
    QModelIndex rowIndex = this->index(row, 0);
    if (rowIndex.isValid()) {
        setData(rowIndex, isBreak, IsBreakRole);
        return true;
    }
    return false;
}

void PersonsModel::removePerson(const int row)
{
    beginRemoveRows(QModelIndex(), row, row);
    cpersons *cp = new cpersons();
    int id = get(row)["id"].toInt();
    id = _list[row]->id();
    QString firstname = _list[row]->fullname();
    cp->removePerson(id);
    _list.removeAt(row);
    endRemoveRows();
    emit modelChanged();
}

void PersonsModel::updateRow(const int row, person *p)
{
    QModelIndex rowIndex = this->index(row, 0);
    if (rowIndex.isValid()) {
        _list[rowIndex.row()]->setFirstname(p->firstname());
        _list[rowIndex.row()]->setLastname(p->lastname());
        emit dataChanged(this->index(row, 0), this->index(row, 2));
    }
}

void PersonsModel::editRow(const int row, const int speakerId, const int themeId, const int congId)
{
    Q_UNUSED(row)
    Q_UNUSED(speakerId)
    Q_UNUSED(congId)
    Q_UNUSED(themeId)

    //    cpublictalks c;
    //    beginResetModel();
    //    _list[row]->setSpeaker(cpersons::getPerson(speakerId));
    //    _list[row]->setTheme(c.getThemeById(themeId));
    //    _list[row]->setCongregation(congId);
    //    _list[row]->save();
    //    endResetModel();
    //    emit modelChanged();
}

PersonsSortFilterProxyModel::PersonsSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *PersonsSortFilterProxyModel::source() const
{
    return sourceModel();
}

void PersonsSortFilterProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
}

void PersonsSortFilterProxyModel::setFilterCongregationId(int value)
{
    m_congregationId = value;
    invalidateFilter();
}

void PersonsSortFilterProxyModel::setFilterFullName(QString value)
{
    m_fullName = value;
    invalidateFilter();
}

bool PersonsSortFilterProxyModel::filterAcceptsRow(int sourceRow,
                                                   const QModelIndex &sourceParent) const
{
    QModelIndex indexFullName = sourceModel()->index(sourceRow, 1, sourceParent);
    return (sourceModel()->data(indexFullName).toString().contains(m_fullName, Qt::CaseInsensitive)); // (congregationId == m_congregationId);
}

bool PersonsSortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    return leftData.toString() < rightData.toString();
}
