#ifndef LMM_ASSIGNMENT_EX_H
#define LMM_ASSIGNMENT_EX_H

#include "lmm_assignment.h"
#include "person.h"
#include "family.h"
#include "assignmentInfo.h"
#include "availability/midweekmeetingavailabilitychecker.h"
#include <QValidator>

class LMM_Assignment_ex : public LMM_Assignment
{
    Q_OBJECT
    Q_PROPERTY(AssignmentInfo *subAssignmentInfo READ subAssignmentInfo CONSTANT)
    Q_PROPERTY(person *assistant READ assistant WRITE setAssistant NOTIFY assistantChanged)
    Q_PROPERTY(person *volunteer READ volunteer WRITE setVolunteer NOTIFY volunteerChanged)
    Q_PROPERTY(QString timing READ timing WRITE setTiming NOTIFY timingChanged)
    Q_PROPERTY(bool completed READ completed WRITE setCompleted NOTIFY completedChanged)
    Q_PROPERTY(QString setting READ setting WRITE setSetting NOTIFY settingChanged)

public:
    LMM_Assignment_ex(QObject *parent = nullptr);
    LMM_Assignment_ex(AssignmentInfo *assignmentInfo, AssignmentInfo *subAssignmentInfo, int sequence, int scheduleDbId, int assignmentDbId, QObject *parent = nullptr);

    AssignmentInfo *subAssignmentInfo() const;

    person *assistant() const;
    void setAssistant(person *assistant);

    person *volunteer() const;
    void setVolunteer(person *volunteer);

    QString timing() const;
    void setTiming(QString time);

    bool completed() const;
    void setCompleted(bool completed);

    QString setting() const;
    void setSetting(QString setting);

    Q_INVOKABLE SortFilterProxyModel *getAssistantList(); // for mobile only
    Q_INVOKABLE QString getReminderText() const;
    Q_INVOKABLE bool save();

signals:
    void assistantChanged();
    void volunteerChanged();
    void timingChanged(QString timing);
    void completedChanged(bool completed);
    void settingChanged(QString setting);

private:
    AssignmentInfo *m_subAssignmentInfo;
    person *m_assistant = nullptr;
    person *m_volunteer = nullptr;
};

class LMMAssignmentValidator : public QValidator
{
    Q_OBJECT
    Q_PROPERTY(LMM_Assignment *assignment READ assignment WRITE setAssignment NOTIFY assignmentChanged)
    Q_PROPERTY(bool isAssistant READ isAssistant WRITE setIsAssistant NOTIFY isAssistantChanged)
    Q_PROPERTY(bool isVolunteer READ isVolunteer WRITE setIsVolunteer NOTIFY isVolunteerChanged)
public:
    LMMAssignmentValidator(QObject *parent = nullptr);
    ~LMMAssignmentValidator();

    QValidator::State validate(QString &input, int &pos) const override;

    LMM_Assignment *assignment() const;
    void setAssignment(LMM_Assignment *newAssignment);
    bool isAssistant() const;
    void setIsAssistant(bool newIsAssistant);
    bool isVolunteer() const;
    void setIsVolunteer(bool newIsVolunteer);

Q_SIGNALS:
    void assignmentChanged();
    void isAssistantChanged();
    void isVolunteerChanged();
    void errorChanged(const QString &error) const;

private:
    LMM_Assignment *m_assignment;
    LMM_Assignment_ex *m_assignment_ex;
    bool m_isAssistant;
    bool m_isVolunteer;
};
#endif // LMM_ASSIGNMENT_EX_H
