#include "congregationdetail.h"

CongregationDetail::CongregationDetail()
{
}

CongregationDetail::CongregationDetail(const int congregationId, const QString congregationName,
                                       const QDate date, const QString theme,
                                       const int assigneeId, const QString assigneeFirstName, const QString assigneeLastName,
                                       const QDate previous1Date, const QString previous1Theme,
                                       const int previous1AssigneeId, const QString previous1AssigneeFirstName, const QString previous1AssigneeLastName,
                                       const QDate previous2Date, const QString previous2Theme,
                                       const int previous2AssigneeId, const QString previous2AssigneeFirstName, const QString previous2AssigneeLastName,
                                       const int assignmentCount, const int assignmentFrequencyRange, const int timeRange,
                                       QObject *parent)
    : QObject(parent), m_congregationId(congregationId), m_congregationName(congregationName), m_date(date), m_theme(theme), m_assigneeId(assigneeId), m_assigneeFirstName(assigneeFirstName), m_assigneeLastName(assigneeLastName), m_previous1Date(previous1Date), m_previous1Theme(previous1Theme), m_previous1AssigneeId(previous1AssigneeId), m_previous1AssigneeFirstName(previous1AssigneeFirstName), m_previous1AssigneeLastName(previous1AssigneeLastName), m_previous2Date(previous2Date), m_previous2Theme(previous2Theme), m_previous2AssigneeId(previous2AssigneeId), m_previous2AssigneeFirstName(previous2AssigneeFirstName), m_previous2AssigneeLastName(previous2AssigneeLastName), m_assignmentCount(assignmentCount), m_assignmentFrequencyRange(assignmentFrequencyRange), m_timeRange(timeRange)
{
}

int CongregationDetail::congregationId() const
{
    return m_congregationId;
}

const QString &CongregationDetail::congregationName() const
{
    return m_congregationName;
}

QDate CongregationDetail::date() const
{
    return m_date;
}

QDate CongregationDetail::previous1Date() const
{
    return m_previous1Date;
}

QString CongregationDetail::theme() const
{
    return m_theme;
}

int CongregationDetail::assigneeId() const
{
    return m_assigneeId;
}

QString CongregationDetail::assigneeFirstName() const
{
    return m_assigneeFirstName;
}

QString CongregationDetail::assigneeLastName() const
{
    return m_assigneeLastName;
}

QString CongregationDetail::assigneeFullName(QString format)
{
    return getFullName(this->assigneeFirstName(), this->assigneeLastName(), format);
}

QString CongregationDetail::previous1Theme() const
{
    return m_previous1Theme;
}

QString CongregationDetail::previous1AssigneeFirstName() const
{
    return m_previous1AssigneeFirstName;
}

QString CongregationDetail::previous1AssigneeLastName() const
{
    return m_previous1AssigneeLastName;
}

QString CongregationDetail::previous1AssigneeFullName(QString format)
{
    return getFullName(this->previous1AssigneeFirstName(), this->previous1AssigneeLastName(), format);
}

QDate CongregationDetail::previous2Date() const
{
    return m_previous2Date;
}

QString CongregationDetail::previous2Theme() const
{
    return m_previous2Theme;
}

QString CongregationDetail::previous2AssigneeFirstName() const
{
    return m_previous2AssigneeFirstName;
}

QString CongregationDetail::previous2AssigneeLastName() const
{
    return m_previous2AssigneeLastName;
}

QString CongregationDetail::previous2AssigneeFullName(QString format)
{
    return getFullName(this->previous2AssigneeFirstName(), this->previous2AssigneeLastName(), format);
}

int CongregationDetail::assignmentCount() const
{
    return m_assignmentCount;
}

int CongregationDetail::assignmentFrequencyRange() const
{
    return m_assignmentFrequencyRange;
}

int CongregationDetail::timeRange() const
{
    return m_timeRange;
}

QString CongregationDetail::getFullName(QString firstName, QString lastName, QString format)
{
    // FORMAT:
    // "FirstName LastName" = DEFAULT
    // "LastName FirstName"
    // "LastName, FirstName"
    if (firstName.isEmpty() && lastName.isEmpty())
        return "";
    format = format.replace("FirstName", "%1");
    format = format.replace("LastName", "%2");
    QString fullname = QString(format).arg(firstName).arg(lastName);
    return (fullname);
}

CongregationDetailModel::CongregationDetailModel(QObject *parent)
    : QAbstractTableModel(parent), assignmentInfos(&AssignmentInfos::Instance())
{
}

CongregationDetailModel::~CongregationDetailModel()
{
    qDeleteAll(congregationDetails);
    congregationDetails.clear();
}

int CongregationDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return congregationDetails.empty() ? 0 : congregationDetails.count();
}

int CongregationDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QHash<int, QByteArray> CongregationDetailModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[CongregationIdRole] = "congregationId";
    items[CongregationNameRole] = "congregationName";
    items[AlphabetRole] = "alphabet";
    items[DateRole] = "date";
    items[YearRole] = "year";
    items[ThemeRole] = "theme";
    items[AssigneeFullNameRole] = "assigneeFullName";
    items[Previous1DateRole] = "previous1Date";
    items[Previous1ThemeRole] = "previous1Theme";
    items[Previous1AssigneeFullNameRole] = "previous1AssigneeFullName";
    items[Previous2DateRole] = "previous2Date";
    items[Previous2ThemeRole] = "previous2Theme";
    items[Previous2AssigneeFullNameRole] = "previous2AssigneeFullName";
    items[AssignmentCountRole] = "assignmentCount";
    items[AssignmentFrequencyRangeRole] = "assignmentFrequencyRange";
    items[TimeRangeRole] = "timeRange";
    return items;
}

QVariant CongregationDetailModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row > congregationDetails.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return congregationDetails[index.row()]->congregationId();
        case 1:
            return congregationDetails[index.row()]->congregationName();
        case 2:
            return congregationDetails[index.row()]->congregationName().left(1);
        case 3:
            return congregationDetails[index.row()]->date();
        case 4:
            return congregationDetails[index.row()]->date().year();
        case 5:
            return congregationDetails[index.row()]->theme();
        case 6:
            return congregationDetails[index.row()]->assigneeFullName();
        case 7:
            return congregationDetails[index.row()]->previous1Date();
        case 8:
            return congregationDetails[index.row()]->previous1Theme();
        case 9:
            return congregationDetails[index.row()]->previous1AssigneeFullName();
        case 10:
            return congregationDetails[index.row()]->previous2Date();
        case 11:
            return congregationDetails[index.row()]->previous2Theme();
        case 12:
            return congregationDetails[index.row()]->previous2AssigneeFullName();
        case 13:
            return congregationDetails[index.row()]->assignmentCount();
        case 14:
            return congregationDetails[index.row()]->assignmentFrequencyRange();
        case 15:
            return congregationDetails[index.row()]->timeRange();
        }
    }

    switch (role) {
    case CongregationIdRole:
        return congregationDetails[index.row()]->congregationId();
    case CongregationNameRole:
        return congregationDetails[index.row()]->congregationName();
    case AlphabetRole:
        return congregationDetails[index.row()]->congregationName().left(1);
    case DateRole:
        return congregationDetails[index.row()]->date();
    case YearRole:
        return congregationDetails[index.row()]->date().year();
    case ThemeRole:
        return congregationDetails[index.row()]->theme();
    case AssigneeFullNameRole:
        return congregationDetails[index.row()]->assigneeFullName();
    case Previous1DateRole:
        return congregationDetails[index.row()]->previous1Date();
    case Previous1ThemeRole:
        return congregationDetails[index.row()]->previous1Theme();
    case Previous1AssigneeFullNameRole:
        return congregationDetails[index.row()]->previous1AssigneeFullName();
    case Previous2DateRole:
        return congregationDetails[index.row()]->previous2Date();
    case Previous2ThemeRole:
        return congregationDetails[index.row()]->previous2Theme();
    case Previous2AssigneeFullNameRole:
        return congregationDetails[index.row()]->previous2AssigneeFullName();
    case AssignmentCountRole:
        return congregationDetails[index.row()]->assignmentCount();
    case AssignmentFrequencyRangeRole:
        return congregationDetails[index.row()]->assignmentFrequencyRange();
    case TimeRangeRole:
        return congregationDetails[index.row()]->timeRange();
    default:
        return QVariant();
    }
}

QVariantMap CongregationDetailModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

void CongregationDetailModel::addCongregationDetail(CongregationDetail *congregationDetail)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    congregationDetails << congregationDetail;
    endInsertRows();
}

void CongregationDetailModel::clear()
{
    beginResetModel();
    qDeleteAll(congregationDetails);
    congregationDetails.clear();
    m_congregationCount = 0;
    m_minAssignmentCount = 0;
    m_maxAssignmentCount = 0;
    endResetModel();
}

// load congregations' visiting speakers history
// param date: for list type 0, the meeting date to check for alerts, statistics etc.
void CongregationDetailModel::loadCongregationDetails(bool includeOwnCongregation, QDate date)
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString sqlQuery = QString("SELECT"
                               " c.id congregationId, c.name congregationName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN date END) date,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN theme END) theme,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assignee_id END) assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assigneeFirstName END) assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assigneeLastName END) assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN date END) previous1_date,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN theme END) previous1_theme,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assignee_id END) previous1_assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assigneeFirstName END) previous1_assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assigneeLastName END) previous1_assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN date END) previous2_date,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN theme END) previous2_theme,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assignee_id END) previous2_assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assigneeFirstName END) previous2_assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assigneeLastName END) previous2_assigneeLastName,"
                               " MAX(assignmentRank) assignmentCount "
                               "FROM congregations c "
                               "LEFT JOIN ("
                               " SELECT c.id congregationId,"
                               "  RANK () OVER (PARTITION BY c.id ORDER BY h.date DESC) assignmentRank, h.*"
                               " FROM congregations c"
                               " JOIN ("
                               " SELECT c1.id congregationId, m1.date, pt.theme_name theme,"
                               " m1.speaker_id assignee_id, p1.firstname assigneeFirstName, p1.lastname assigneeLastName"
                               " FROM publicmeeting m1"
                               " LEFT JOIN persons p1 ON m1.speaker_id = p1.id"
                               " LEFT JOIN congregations c1 ON p1.congregation_id = c1.id"
                               " LEFT JOIN publictalks pt ON m1.theme_id = pt.id"
                               " WHERE m1.date >= '%1'" // public talk
                               ") h ON c.id = h.congregationId ")
                               .arg(date.addYears(-10).toString(Qt::ISODate));

    sqlQuery.append(" ORDER BY c.id, assignmentRank) ON congregationId = c.id "
                    "GROUP BY c.id ");
    if (!includeOwnCongregation) {
        ccongregation cc;
        int ownCongregationId = cc.getMyCongregation().id;
        sqlQuery.append(QString("HAVING c.id <> %1").arg(ownCongregationId));
    }
    sql_items assignmentRows = sql->selectSql(sqlQuery);
    updateModel(date, assignmentRows);
}

void CongregationDetailModel::updateModel(QDate date, const sql_items &assignmentRows)
{
    beginResetModel();
    qDeleteAll(congregationDetails);
    congregationDetails.clear();

    // update name display order
    sql_class *sql = &Singleton<sql_class>::Instance();
    displayNameFormat = sql->getSetting("nameFormat", "%2, %1");

    if (!assignmentRows.empty()) {
        QVector<int> congregationIds;

        // collect statistical data
        int minAssignmentCount = 999;
        int maxAssignmentCount = 0;
        for (unsigned int i = 0; i < assignmentRows.size(); i++) {
            sql_item s = assignmentRows[i];
            if (!congregationIds.contains(s.value("congregationId").toInt()))
                congregationIds.append(s.value("congregationId").toInt());
            minAssignmentCount = qMin(minAssignmentCount, s.value("assignmentCount").toInt());
            maxAssignmentCount = qMax(maxAssignmentCount, s.value("assignmentCount").toInt());
        }

        m_congregationCount = congregationIds.count();
        m_minAssignmentCount = minAssignmentCount;
        m_maxAssignmentCount = maxAssignmentCount;
        int minTimeRange = 1;
        int maxTimeRange = 5;
        double timeRangeUnit = 28; // days

        ccongregation cc;
        for (unsigned int i = 0; i < assignmentRows.size(); i++) {
            sql_item s = assignmentRows[i];
            int assignmentFrequencyRange = s.value("assignmentCount").isNull()
                    ? 0
                    : qCeil((s.value("assignmentCount").toDouble() - qMax(1, minAssignmentCount) + 1) / (maxAssignmentCount - qMax(1, minAssignmentCount) + 1) * 5);
            QDate lastDate = s.value("date").toDate();
            int timeRange = lastDate.isValid()
                    ? qFloor((double)lastDate.daysTo(date) / timeRangeUnit)
                    : 5;
            timeRange = qMax(timeRange, minTimeRange);
            timeRange = qMin(timeRange, maxTimeRange);

            QDate meetingDate = s.value("date").toDate();
            if (meetingDate.isValid())
                meetingDate = meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, meetingDate) - 1);
            QDate previous1_meetingDate = s.value("previous1_date").toDate();
            if (previous1_meetingDate.isValid())
                previous1_meetingDate = previous1_meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, previous1_meetingDate) - 1);
            QDate previous2_meetingDate = s.value("previous2_date").toDate();
            if (previous2_meetingDate.isValid())
                previous2_meetingDate = previous2_meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, previous2_meetingDate) - 1);

            QString theme = s.value("theme").toString();
            QString previous1Theme = s.value("previous1_theme").toString();
            QString previous2Theme = s.value("previous2_theme").toString();
            QString congregationName = s.value("congregationName").toString();
            addCongregationDetail(new CongregationDetail(s.value("congregationId").toInt(),
                                                         s.value("congregationName").toString(),
                                                         meetingDate,
                                                         theme,
                                                         s.value("assignee_id").toInt(),
                                                         s.value("assigneeFirstName").toString(),
                                                         s.value("assigneeLastName").toString(),
                                                         previous1_meetingDate,
                                                         previous1Theme,
                                                         s.value("previous1_assignee_id").toInt(),
                                                         s.value("previous1_assigneeFirstName").toString(),
                                                         s.value("previous1_assigneeLastName").toString(),
                                                         previous2_meetingDate,
                                                         previous2Theme,
                                                         s.value("previous2_assignee_id").toInt(),
                                                         s.value("previous2_assigneeFirstName").toString(),
                                                         s.value("previous2_assigneeLastName").toString(),
                                                         s.value("assignmentCount").toInt(),
                                                         assignmentFrequencyRange,
                                                         timeRange));
        }
    } else {
        m_congregationCount = 0;
        m_minAssignmentCount = 0;
        m_maxAssignmentCount = 0;
    }
    endResetModel();
}

CongregationDetailSFProxyModel::CongregationDetailSFProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *CongregationDetailSFProxyModel::source() const
{
    return sourceModel();
}

void CongregationDetailSFProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
}

QString CongregationDetailSFProxyModel::filterText() const
{
    return m_filterText;
}

void CongregationDetailSFProxyModel::setFilterText(QString value)
{
    m_filterText = value;
    invalidateFilter();
    emit filterChanged();
}

QByteArray CongregationDetailSFProxyModel::sortRole() const
{
    return roleNames().value(QSortFilterProxyModel::sortRole());
}

void CongregationDetailSFProxyModel::setSortRole(const QByteArray &role)
{
    QSortFilterProxyModel::setSortRole(roleKey(role));
    emit sortChanged();
}

QByteArray CongregationDetailSFProxyModel::groupByRole() const
{
    return m_groupByRole;
}

void CongregationDetailSFProxyModel::setGroupByRole(const QByteArray &role)
{
    beginResetModel();
    m_groupByRole = role;
    endResetModel();
    emit groupByChanged();
}

bool CongregationDetailSFProxyModel::filterAcceptsRow(int sourceRow,
                                                      const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return (sourceModel()->data(index, CongregationDetailModel::Roles::CongregationNameRole).toString().contains(filterText(), Qt::CaseInsensitive));
}

bool CongregationDetailSFProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    int inSectionSortMode = 0; // sort by date
    // compare sections first
    switch (roleKey(groupByRole())) {
    case CongregationDetailModel::Roles::AlphabetRole: {
        QVariant val1 = sourceModel()->data(left, CongregationDetailModel::Roles::CongregationNameRole);
        QVariant val2 = sourceModel()->data(right, CongregationDetailModel::Roles::CongregationNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        break;
    }
    case CongregationDetailModel::Roles::ThemeRole: {
        QVariant val1 = sourceModel()->data(left, CongregationDetailModel::Roles::ThemeRole);
        QVariant val2 = sourceModel()->data(right, CongregationDetailModel::Roles::ThemeRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        inSectionSortMode = 1; // sort by name within sections
        break;
    }
    case CongregationDetailModel::Roles::AssignmentFrequencyRangeRole: {
        QVariant val1 = sourceModel()->data(left, CongregationDetailModel::Roles::AssignmentCountRole);
        QVariant val2 = sourceModel()->data(right, CongregationDetailModel::Roles::AssignmentCountRole);
        if (!val1.isNull() && !val2.isNull()) {
            int assignmentCount1 = val1.toInt();
            int assignmentCount2 = val2.toInt();
            return assignmentCount1 < assignmentCount2;
        }
        break;
    }
    default:
        break;
    }

    if (inSectionSortMode == 1) {
        // sort by name within sections
        QVariant val1 = sourceModel()->data(left, CongregationDetailModel::Roles::CongregationNameRole);
        QVariant val2 = sourceModel()->data(right, CongregationDetailModel::Roles::CongregationNameRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
    }
    // sort by date within sections
    QVariant leftData = sourceModel()->data(left, CongregationDetailModel::Roles::DateRole);
    QVariant rightData = sourceModel()->data(right, CongregationDetailModel::Roles::DateRole);
    if (leftData.userType() == QMetaType::QDate) {
        return leftData.toDate() < rightData.toDate();
    } else
        return leftData.toString() < rightData.toString();
}

int CongregationDetailSFProxyModel::roleKey(const QByteArray &role) const
{
    QHash<int, QByteArray> roles = roleNames();
    QHashIterator<int, QByteArray> it(roles);
    while (it.hasNext()) {
        it.next();
        if (it.value() == role)
            return it.key();
    }
    return -1;
}
