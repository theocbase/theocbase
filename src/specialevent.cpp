/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2022, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "specialevent.h"

SpecialEventRule::SpecialEventRule(QObject *parent)
    : QObject(parent), m_id(SpecialEvent::None), m_description(""), m_canChangeDescription(false), m_exclusivity(SpecialEventExclusivity::RegularMeeting), m_startDay(Qt::Monday), m_endDay(Qt::Sunday), m_isCircuitOverseersVisit(false), m_isConvention(false), m_isSingleDay(false), m_isWithoutPublicTalk(false), m_isWithFinalTalk(false), m_isWatchtowerStudyAbbreviated(false), m_isProgramTransmitted(false)
{
}

SpecialEvent SpecialEventRule::id() const
{
    return m_id;
}

void SpecialEventRule::setId(SpecialEvent newId)
{
    m_id = newId;
}

QString SpecialEventRule::description() const
{
    return m_description;
}

void SpecialEventRule::setDescription(const QString &newDescription)
{
    m_description = newDescription;
}

bool SpecialEventRule::canChangeDescription() const
{
    return m_canChangeDescription;
}

void SpecialEventRule::setCanChangeDescription(bool newCanChangeDescription)
{
    m_canChangeDescription = newCanChangeDescription;
}

SpecialEventExclusivity SpecialEventRule::exclusivity() const
{
    return m_exclusivity;
}

void SpecialEventRule::setExclusivity(SpecialEventExclusivity newExclusivity)
{
    m_exclusivity = newExclusivity;
}

Qt::DayOfWeek SpecialEventRule::startDay() const
{
    return m_startDay;
}

void SpecialEventRule::setStartDay(Qt::DayOfWeek newStartDay)
{
    m_startDay = newStartDay;
}

Qt::DayOfWeek SpecialEventRule::endDay() const
{
    return m_endDay;
}

void SpecialEventRule::setEndDay(Qt::DayOfWeek newEndDay)
{
    m_endDay = newEndDay;
}

bool SpecialEventRule::isCircuitOverseersVisit() const
{
    return m_isCircuitOverseersVisit;
}

void SpecialEventRule::setIsCircuitOverseersVisit(bool newIsCircuitOverseersVisit)
{
    m_isCircuitOverseersVisit = newIsCircuitOverseersVisit;
}

bool SpecialEventRule::isConvention() const
{
    return m_isConvention;
}

void SpecialEventRule::setIsConvention(bool newIsConvention)
{
    m_isConvention = newIsConvention;
}

bool SpecialEventRule::isSingleDay() const
{
    return m_isSingleDay;
}

void SpecialEventRule::setIsSingleDay(bool newIsSingleDay)
{
    m_isSingleDay = newIsSingleDay;
}

bool SpecialEventRule::isWithoutPublicTalk() const
{
    return m_isWithoutPublicTalk;
}

void SpecialEventRule::setIsWithoutPublicTalk(bool newIsWithoutPublicTalk)
{
    m_isWithoutPublicTalk = newIsWithoutPublicTalk;
}

bool SpecialEventRule::isWithFinalTalk() const
{
    return m_isWithFinalTalk;
}

void SpecialEventRule::setIsWithFinalTalk(bool newIsWithFinalTalk)
{
    m_isWithFinalTalk = newIsWithFinalTalk;
}

bool SpecialEventRule::isWatchtowerStudyAbbreviated() const
{
    return m_isWatchtowerStudyAbbreviated;
}

void SpecialEventRule::setIsWatchtowerStudyAbbreviated(bool newIsWatchtowerStudyAbbreviated)
{
    m_isWatchtowerStudyAbbreviated = newIsWatchtowerStudyAbbreviated;
}

bool SpecialEventRule::isProgramTransmitted() const
{
    return m_isProgramTransmitted;
}

void SpecialEventRule::setIsProgramTransmitted(bool newProgramTransmitted)
{
    m_isProgramTransmitted = newProgramTransmitted;
}

SpecialEvents::SpecialEvents(QObject *parent)
    : QObject(parent)
{
    resetSpecialEventRules();
}

void SpecialEvents::resetSpecialEventRules()
{
    m_specialEventRules.clear();
    // None = regular meeting
    SpecialEventRule *noSpecialEventRule = new SpecialEventRule(this);
    noSpecialEventRule->setId(SpecialEvent::None);
    noSpecialEventRule->setDescription(tr("No exception"));
    m_specialEventRules.append(noSpecialEventRule);
    // Circuit Overseer's visit
    SpecialEventRule *coVisitEventRule = new SpecialEventRule(this);
    coVisitEventRule->setId(SpecialEvent::CircuitOverseersVisit);
    coVisitEventRule->setDescription(tr("Circuit overseer's visit"));
    coVisitEventRule->setStartDay(Qt::Tuesday);
    coVisitEventRule->setEndDay(Qt::Sunday);
    coVisitEventRule->setIsCircuitOverseersVisit(true);
    coVisitEventRule->setIsWithFinalTalk(true);
    coVisitEventRule->setIsWatchtowerStudyAbbreviated(true);
    m_specialEventRules.append(coVisitEventRule);
    // "Circuit assembly"
    SpecialEventRule *circuitAssemblyEventRule = new SpecialEventRule(this);
    circuitAssemblyEventRule->setId(SpecialEvent::CircuitAssembly);
    circuitAssemblyEventRule->setDescription(tr("Circuit assembly"));
    circuitAssemblyEventRule->setExclusivity(SpecialEventExclusivity::NoOtherMeetingInSameWeek);
    circuitAssemblyEventRule->setStartDay(Qt::Sunday);
    circuitAssemblyEventRule->setEndDay(Qt::Sunday);
    circuitAssemblyEventRule->setIsConvention(true);
    circuitAssemblyEventRule->setIsSingleDay(true);
    m_specialEventRules.append(circuitAssemblyEventRule);
    // "Convention"
    SpecialEventRule *conventionEventRule = new SpecialEventRule(this);
    conventionEventRule->setId(SpecialEvent::Convention);
    conventionEventRule->setDescription(tr("Convention"));
    conventionEventRule->setExclusivity(SpecialEventExclusivity::NoOtherMeetingInSameWeek);
    conventionEventRule->setStartDay(Qt::Friday);
    conventionEventRule->setEndDay(Qt::Sunday);
    conventionEventRule->setIsConvention(true);
    m_specialEventRules.append(conventionEventRule);
    // Virtual convention
    SpecialEventRule *virtualConventionEventRule = new SpecialEventRule(this);
    virtualConventionEventRule->setId(SpecialEvent::VirtualConvention);
    virtualConventionEventRule->setDescription(tr("Virtual convention", "Schedule for viewing convention sessions at home"));
    virtualConventionEventRule->setCanChangeDescription(true);
    virtualConventionEventRule->setIsWithoutPublicTalk(true);
    virtualConventionEventRule->setIsWatchtowerStudyAbbreviated(true);
    m_specialEventRules.append(virtualConventionEventRule);
    // Memorial
    SpecialEventRule *memorialEventRule = new SpecialEventRule(this);
    memorialEventRule->setId(SpecialEvent::Memorial);
    memorialEventRule->setDescription(tr("Memorial"));
    memorialEventRule->setExclusivity(SpecialEventExclusivity::NoOtherMeetingInSamePartOfTheWeek);
    memorialEventRule->setCanChangeDescription(true);
    m_specialEventRules.append(memorialEventRule);
    // Headquarters representative
    SpecialEventRule *hqRepresentativeEventRule = new SpecialEventRule(this);
    hqRepresentativeEventRule->setId(SpecialEvent::VisitOfHeadquartersRepresentative);
    hqRepresentativeEventRule->setDescription(tr("Visit of headquarters representative"));
    hqRepresentativeEventRule->setCanChangeDescription(true);
    hqRepresentativeEventRule->setIsProgramTransmitted(true);
    m_specialEventRules.append(hqRepresentativeEventRule);
    // Bethel speaker
    SpecialEventRule *bethelSpeakerEventRule = new SpecialEventRule(this);
    bethelSpeakerEventRule->setId(SpecialEvent::VisitOfBethelSpeaker);
    bethelSpeakerEventRule->setDescription(tr("Visit of Bethel speaker"));
    bethelSpeakerEventRule->setCanChangeDescription(true);
    bethelSpeakerEventRule->setIsWithFinalTalk(true);
    bethelSpeakerEventRule->setIsWatchtowerStudyAbbreviated(true);
    m_specialEventRules.append(bethelSpeakerEventRule);
    // Other exception
    SpecialEventRule *otherEventRule = new SpecialEventRule(this);
    otherEventRule->setId(SpecialEvent::Other);
    otherEventRule->setDescription(tr("Other exception"));
    otherEventRule->setCanChangeDescription(true);
    m_specialEventRules.append(otherEventRule);
}

QQmlListProperty<SpecialEventRule> SpecialEvents::specialEventRules()
{
    return QQmlListProperty<SpecialEventRule>(this, &this->m_specialEventRules);
}

int SpecialEvents::count() const
{
    return m_specialEventRules.count();
}

SpecialEventRule *SpecialEvents::specialEventRule(int index) const
{
    return m_specialEventRules.at(index);
}

SpecialEventRule *SpecialEvents::findSpecialEventRule(int id) const
{
    SpecialEventRule *noSpecialEvent = m_specialEventRules[0];
    for (int i = 0; i < m_specialEventRules.size(); ++i) {
        if (static_cast<int>(m_specialEventRules[i]->id()) == id)
            return m_specialEventRules[i];
        if (m_specialEventRules[i]->id() == SpecialEvent::None)
            noSpecialEvent = m_specialEventRules[i];
    }
    return noSpecialEvent;
}
