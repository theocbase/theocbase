#ifndef PROFILES_H
#define PROFILES_H

#include <QString>
#include <QDebug>
#include <QSettings>
#include <QApplication>
#include <QProcess>
#include <QDir>
#include <QStandardPaths>

class profiles : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList model MEMBER _model NOTIFY modelChanged)
    Q_PROPERTY(QString currentProfile READ currentProfile WRITE setCurrentProfile NOTIFY currentProfileChanged)
public:
    profiles(QObject *parent = nullptr);
    QString currentProfile() const;
    void setCurrentProfile(QString name);
    QString databaseFile() const;
    QString settingsName() const;

    Q_INVOKABLE void createProfile(QString name);
    Q_INVOKABLE void deleteProfile(QString name);

    Q_INVOKABLE void restart();

signals:
    void modelChanged();
    void currentProfileChanged();

private:
    QSettings _settings;
    QString _currentProfile;
    QStringList _model;
};

#endif // PROFILES_H
