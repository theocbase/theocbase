#include "publictalkdetail.h"

PublicTalkDetail::PublicTalkDetail()
{
}

PublicTalkDetail::PublicTalkDetail(const int themeId, const int themeNumber, const QString theme,
                                   const QDate date, const QString congregationName,
                                   const int assigneeId, const QString assigneeFirstName, const QString assigneeLastName,
                                   const QDate previous1Date, const QString previous1CongregationName,
                                   const int previous1AssigneeId, const QString previous1AssigneeFirstName, const QString previous1AssigneeLastName,
                                   const QDate previous2Date, const QString previous2CongregationName,
                                   const int previous2AssigneeId, const QString previous2AssigneeFirstName, const QString previous2AssigneeLastName,
                                   const int assignmentCount, const int assignmentFrequencyRange, const int timeRange,
                                   QObject *parent)
    : QObject(parent), m_themeId(themeId), m_themeNumber(themeNumber), m_theme(theme), m_date(date), m_congregationName(congregationName), m_assigneeId(assigneeId), m_assigneeFirstName(assigneeFirstName), m_assigneeLastName(assigneeLastName), m_previous1Date(previous1Date), m_previous1CongregationName(previous1CongregationName), m_previous1AssigneeId(previous1AssigneeId), m_previous1AssigneeFirstName(previous1AssigneeFirstName), m_previous1AssigneeLastName(previous1AssigneeLastName), m_previous2Date(previous2Date), m_previous2CongregationName(previous2CongregationName), m_previous2AssigneeId(previous2AssigneeId), m_previous2AssigneeFirstName(previous2AssigneeFirstName), m_previous2AssigneeLastName(previous2AssigneeLastName), m_assignmentCount(assignmentCount), m_assignmentFrequencyRange(assignmentFrequencyRange), m_timeRange(timeRange)
{
}

int PublicTalkDetail::themeId() const
{
    return m_themeId;
}

int PublicTalkDetail::themeNumber() const
{
    return m_themeNumber;
}

const QString PublicTalkDetail::themeNumberRange(int step) const
{
    return tr("%1 to %2", "Number range, e.g. 1 to 10")
            .arg(qFloor((m_themeNumber - 1) / step) * step + 1)
            .arg(qFloor((m_themeNumber - 1) / step) * step + step);
}

const QString &PublicTalkDetail::theme() const
{
    return m_theme;
}

const QString PublicTalkDetail::numberAndTheme(QString format)
{
    return QString(format).arg(m_themeNumber).arg(m_theme);
}

const QDate &PublicTalkDetail::date() const
{
    return m_date;
}

const QString &PublicTalkDetail::congregationName() const
{
    return m_congregationName;
}

int PublicTalkDetail::assigneeId() const
{
    return m_assigneeId;
}

const QString &PublicTalkDetail::assigneeFirstName() const
{
    return m_assigneeFirstName;
}

const QString &PublicTalkDetail::assigneeLastName() const
{
    return m_assigneeLastName;
}

const QString PublicTalkDetail::assigneeFullName(QString format) const
{
    return getFullName(this->assigneeFirstName(), this->assigneeLastName(), format);
}

const QDate &PublicTalkDetail::previous1Date() const
{
    return m_previous1Date;
}

const QString &PublicTalkDetail::previous1CongregationName() const
{
    return m_previous1CongregationName;
}

const QString &PublicTalkDetail::previous1AssigneeFirstName() const
{
    return m_previous1AssigneeFirstName;
}

const QString &PublicTalkDetail::previous1AssigneeLastName() const
{
    return m_previous1AssigneeLastName;
}

const QString PublicTalkDetail::previous1AssigneeFullName(QString format) const
{
    return getFullName(this->previous1AssigneeFirstName(), this->previous1AssigneeLastName(), format);
}

const QDate &PublicTalkDetail::previous2Date() const
{
    return m_previous2Date;
}

const QString &PublicTalkDetail::previous2CongregationName() const
{
    return m_previous2CongregationName;
}

const QString &PublicTalkDetail::previous2AssigneeFirstName() const
{
    return m_previous2AssigneeFirstName;
}

const QString &PublicTalkDetail::previous2AssigneeLastName() const
{
    return m_previous2AssigneeLastName;
}

const QString PublicTalkDetail::previous2AssigneeFullName(QString format) const
{
    return getFullName(this->previous2AssigneeFirstName(), this->previous2AssigneeLastName(), format);
}

int PublicTalkDetail::assignmentCount() const
{
    return m_assignmentCount;
}

int PublicTalkDetail::assignmentFrequencyRange() const
{
    return m_assignmentFrequencyRange;
}

int PublicTalkDetail::timeRange() const
{
    return m_timeRange;
}

const QString PublicTalkDetail::getFullName(QString firstName, QString lastName, QString format) const
{
    // FORMAT:
    // "FirstName LastName" = DEFAULT
    // "LastName FirstName"
    // "LastName, FirstName"
    if (firstName.isEmpty() && lastName.isEmpty())
        return "";
    format = format.replace("FirstName", "%1");
    format = format.replace("LastName", "%2");
    QString fullname = QString(format).arg(firstName).arg(lastName);
    return (fullname);
}

PublicTalkDetailModel::PublicTalkDetailModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

PublicTalkDetailModel::~PublicTalkDetailModel()
{
    qDeleteAll(publicTalkDetails);
    publicTalkDetails.clear();
}

int PublicTalkDetailModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return publicTalkDetails.empty() ? 0 : publicTalkDetails.count();
}

int PublicTalkDetailModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 6;
}

QHash<int, QByteArray> PublicTalkDetailModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[ThemeIdRole] = "themeId";
    items[ThemeNumberRole] = "themeNumber";
    items[ThemeNumberRangeRole] = "themeNumberRange";
    items[ThemeRole] = "theme";
    items[NumberAndThemeRole] = "numberAndTheme";
    items[ThemeAndNumberRole] = "themeAndNumber";
    items[AlphabetRole] = "alphabet";
    items[DateRole] = "date";
    items[YearRole] = "year";
    items[CongregationNameRole] = "congregationName";
    items[AssigneeFullNameRole] = "assigneeFullName";
    items[Previous1DateRole] = "previous1Date";
    items[Previous1CongregationNameRole] = "previous1CongregationName";
    items[Previous1AssigneeFullNameRole] = "previous1AssigneeFullName";
    items[Previous2DateRole] = "previous2Date";
    items[Previous2CongregationNameRole] = "previous2CongregationName";
    items[Previous2AssigneeFullNameRole] = "previous2AssigneeFullName";
    items[AssignmentCountRole] = "assignmentCount";
    items[AssignmentFrequencyRangeRole] = "assignmentFrequencyRange";
    items[TimeRangeRole] = "timeRange";
    return items;
}

QVariant PublicTalkDetailModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if (row < 0 || row > publicTalkDetails.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return publicTalkDetails[index.row()]->themeId();
        case 1:
            return publicTalkDetails[index.row()]->themeNumber();
        case 2:
            return publicTalkDetails[index.row()]->themeNumberRange(10);
        case 3:
            return publicTalkDetails[index.row()]->theme();
        case 4:
            return publicTalkDetails[index.row()]->theme().left(1);
        case 5:
            return publicTalkDetails[index.row()]->numberAndTheme();
        case 6:
            return publicTalkDetails[index.row()]->numberAndTheme("%2 (%1)");
        case 7:
            return publicTalkDetails[index.row()]->date();
        case 8:
            return publicTalkDetails[index.row()]->date().year();
        case 9:
            return publicTalkDetails[index.row()]->congregationName();
        case 10:
            return publicTalkDetails[index.row()]->assigneeFullName();
        case 11:
            return publicTalkDetails[index.row()]->previous1Date();
        case 12:
            return publicTalkDetails[index.row()]->previous1CongregationName();
        case 13:
            return publicTalkDetails[index.row()]->previous1AssigneeFullName();
        case 14:
            return publicTalkDetails[index.row()]->previous2Date();
        case 15:
            return publicTalkDetails[index.row()]->previous2CongregationName();
        case 16:
            return publicTalkDetails[index.row()]->previous2AssigneeFullName();
        case 17:
            return publicTalkDetails[index.row()]->assignmentCount();
        case 18:
            return publicTalkDetails[index.row()]->assignmentFrequencyRange();
        case 19:
            return publicTalkDetails[index.row()]->timeRange();
        }
    }

    switch (role) {
    case ThemeIdRole:
        return publicTalkDetails[index.row()]->themeId();
    case ThemeNumberRole:
        return publicTalkDetails[index.row()]->themeNumber();
    case ThemeNumberRangeRole:
        return publicTalkDetails[index.row()]->themeNumberRange(10);
    case ThemeRole:
        return publicTalkDetails[index.row()]->theme();
    case AlphabetRole:
        return publicTalkDetails[index.row()]->theme().left(1);
    case NumberAndThemeRole:
        return publicTalkDetails[index.row()]->numberAndTheme();
    case ThemeAndNumberRole:
        return publicTalkDetails[index.row()]->numberAndTheme("%2 (%1)");
    case DateRole:
        return publicTalkDetails[index.row()]->date();
    case YearRole:
        return publicTalkDetails[index.row()]->date().year();
    case CongregationNameRole:
        return publicTalkDetails[index.row()]->congregationName();
    case AssigneeFullNameRole:
        return publicTalkDetails[index.row()]->assigneeFullName();
    case Previous1DateRole:
        return publicTalkDetails[index.row()]->previous1Date();
    case Previous1CongregationNameRole:
        return publicTalkDetails[index.row()]->previous1CongregationName();
    case Previous1AssigneeFullNameRole:
        return publicTalkDetails[index.row()]->previous1AssigneeFullName();
    case Previous2DateRole:
        return publicTalkDetails[index.row()]->previous2Date();
    case Previous2CongregationNameRole:
        return publicTalkDetails[index.row()]->previous2CongregationName();
    case Previous2AssigneeFullNameRole:
        return publicTalkDetails[index.row()]->previous2AssigneeFullName();
    case AssignmentCountRole:
        return publicTalkDetails[index.row()]->assignmentCount();
    case AssignmentFrequencyRangeRole:
        return publicTalkDetails[index.row()]->assignmentFrequencyRange();
    case TimeRangeRole:
        return publicTalkDetails[index.row()]->timeRange();
    default:
        return QVariant();
    }
}

QVariantMap PublicTalkDetailModel::get(int row) const
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

void PublicTalkDetailModel::addPublicTalkDetail(PublicTalkDetail *publicTalkDetail)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    publicTalkDetails << publicTalkDetail;
    endInsertRows();
}

void PublicTalkDetailModel::clear()
{
    beginResetModel();
    qDeleteAll(publicTalkDetails);
    publicTalkDetails.clear();
    m_publicTalkCount = 0;
    m_minAssignmentCount = 0;
    m_maxAssignmentCount = 0;
    endResetModel();
}

// load congregations' visiting speakers history
// param date: for list type 0, the meeting date to check for alerts, statistics etc.
void PublicTalkDetailModel::loadPublicTalkDetails(int includeThemeId, int personId, QDate date)
{
    ccongregation c;
    QDate meetingDate = date.addDays(c.getMeetingDay(MeetingType::WeekendMeeting, date) - 1);
    sql_class *sql = &Singleton<sql_class>::Instance();
    QString langId = QVariant(sql->getLanguageDefaultId()).toString();
    QString sqlQuery = QString("SELECT"
                               " pt.id themeId, pt.theme_number themeNumber, pt.theme_name theme,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN date END) date,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN congregationName END) congregationName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assignee_id END) assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assigneeFirstName END) assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 1 THEN assigneeLastName END) assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN date END) previous1_date,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN congregationName END) previous1_congregationName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assignee_id END) previous1_assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assigneeFirstName END) previous1_assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN assigneeLastName END) previous1_assigneeLastName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN date END) previous2_date,"
                               " MIN(CASE WHEN assignmentRank = 2 THEN congregationName END) previous1_congregationName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assignee_id END) previous2_assignee_id,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assigneeFirstName END) previous2_assigneeFirstName,"
                               " MIN(CASE WHEN assignmentRank = 3 THEN assigneeLastName END) previous2_assigneeLastName,"
                               " MAX(assignmentRank) assignmentCount "
                               "FROM publictalks pt ");

    sqlQuery += QString(
                        "LEFT JOIN ("
                        " SELECT pt.id themeId,"
                        "  RANK () OVER (PARTITION BY pt.id ORDER BY h.date DESC) assignmentRank, h.*"
                        " FROM publictalks pt"
                        " JOIN ("
                        " SELECT m1.theme_id themeId, m1.date, c1.name congregationName,"
                        " m1.speaker_id assignee_id, p1.firstname assigneeFirstName, p1.lastname assigneeLastName"
                        " FROM publicmeeting m1"
                        " LEFT JOIN persons p1 ON m1.speaker_id = p1.id"
                        " LEFT JOIN congregations c1 ON p1.congregation_id = c1.id"
                        " WHERE m1.date >= '%1'" // public talk
                        ") h ON pt.id = h.themeId "
                        "ORDER BY pt.id, assignmentRank) ON themeId = pt.id "
                        "WHERE pt.lang_id = %2 AND pt.active AND"
                        " (((pt.discontinue_date IS NULL OR pt.discontinue_date = '' OR pt.discontinue_date > '%3')"
                        " AND (pt.release_date IS NULL OR pt.release_date = '' OR pt.release_date <= '%3'))"
                        " OR pt.id = %4) "
                        "GROUP BY pt.id ")
                        .arg(date.addYears(-10).toString(Qt::ISODate))
                        .arg(langId)
                        .arg(meetingDate.toString(Qt::ISODate))
                        .arg(includeThemeId);

    if (personId > 0) {
        sqlQuery += QString("HAVING pt.id IN ("
                            " SELECT theme_id "
                            " FROM speaker_publictalks"
                            " WHERE speaker_id = %1 AND active)"
                            " OR pt.id = %2")
                            .arg(personId)
                            .arg(includeThemeId);
    }

    sql_items assignmentRows = sql->selectSql(sqlQuery);
    updateModel(date, assignmentRows);
}

void PublicTalkDetailModel::updateModel(QDate date, const sql_items &assignmentRows)
{
    beginResetModel();
    qDeleteAll(publicTalkDetails);
    publicTalkDetails.clear();

    // update name display order
    sql_class *sql = &Singleton<sql_class>::Instance();
    displayNameFormat = sql->getSetting("nameFormat", "%2, %1");

    if (!assignmentRows.empty()) {
        QVector<int> publicTalkIds;

        // collect statistical data
        int minAssignmentCount = 999;
        int maxAssignmentCount = 0;
        for (unsigned int i = 0; i < assignmentRows.size(); i++) {
            sql_item s = assignmentRows[i];
            if (!publicTalkIds.contains(s.value("themeId").toInt()))
                publicTalkIds.append(s.value("themeId").toInt());
            minAssignmentCount = qMin(minAssignmentCount, s.value("assignmentCount").toInt());
            maxAssignmentCount = qMax(maxAssignmentCount, s.value("assignmentCount").toInt());
        }

        m_publicTalkCount = publicTalkIds.count();
        m_minAssignmentCount = minAssignmentCount;
        m_maxAssignmentCount = maxAssignmentCount;
        int minTimeRange = 1;
        int maxTimeRange = 5;
        double timeRangeUnit = 365; // days
        ccongregation cc;
        for (unsigned int i = 0; i < assignmentRows.size(); i++) {
            sql_item s = assignmentRows[i];
            int assignmentFrequencyRange = s.value("assignmentCount").isNull()
                    ? 0
                    : qCeil((s.value("assignmentCount").toDouble() - qMax(1, minAssignmentCount) + 1) / (maxAssignmentCount - qMax(1, minAssignmentCount) + 1) * 5);
            QDate lastDate = s.value("date").toDate();
            int timeRange = lastDate.isValid()
                    ? qFloor((double)lastDate.daysTo(date) / timeRangeUnit)
                    : 5;
            timeRange = qMax(timeRange, minTimeRange);
            timeRange = qMin(timeRange, maxTimeRange);

            QDate meetingDate = s.value("date").toDate();
            if (meetingDate.isValid())
                meetingDate = meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, meetingDate) - 1);
            QDate previous1_meetingDate = s.value("previous1_date").toDate();
            if (previous1_meetingDate.isValid())
                previous1_meetingDate = previous1_meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, previous1_meetingDate) - 1);
            QDate previous2_meetingDate = s.value("previous2_date").toDate();
            if (previous2_meetingDate.isValid())
                previous2_meetingDate = previous2_meetingDate.addDays(cc.getMeetingDay(MeetingType::WeekendMeeting, previous2_meetingDate) - 1);

            addPublicTalkDetail(new PublicTalkDetail(s.value("themeId").toInt(),
                                                     s.value("themeNumber").toInt(),
                                                     s.value("theme").toString(),
                                                     meetingDate,
                                                     s.value("congregationName").toString(),
                                                     s.value("assignee_id").toInt(),
                                                     s.value("assigneeFirstName").toString(),
                                                     s.value("assigneeLastName").toString(),
                                                     previous1_meetingDate,
                                                     s.value("previous1_congregationName").toString(),
                                                     s.value("previous1_assignee_id").toInt(),
                                                     s.value("previous1_assigneeFirstName").toString(),
                                                     s.value("previous1_assigneeLastName").toString(),
                                                     previous2_meetingDate,
                                                     s.value("previous2_congregationName").toString(),
                                                     s.value("previous2_assignee_id").toInt(),
                                                     s.value("previous2_assigneeFirstName").toString(),
                                                     s.value("previous2_assigneeLastName").toString(),
                                                     s.value("assignmentCount").toInt(),
                                                     assignmentFrequencyRange,
                                                     timeRange));
        }
    } else {
        m_publicTalkCount = 0;
        m_minAssignmentCount = 0;
        m_maxAssignmentCount = 0;
    }
    endResetModel();
}

PublicTalkDetailSFProxyModel::PublicTalkDetailSFProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

QObject *PublicTalkDetailSFProxyModel::source() const
{
    return sourceModel();
}

void PublicTalkDetailSFProxyModel::setSource(QObject *source)
{
    setSourceModel(qobject_cast<QAbstractItemModel *>(source));
}

QString PublicTalkDetailSFProxyModel::filterText() const
{
    return m_filterText;
}

void PublicTalkDetailSFProxyModel::setFilterText(QString value)
{
    m_filterText = value;
    invalidateFilter();
    emit filterChanged();
}

QByteArray PublicTalkDetailSFProxyModel::sortRole() const
{
    return roleNames().value(QSortFilterProxyModel::sortRole());
}

void PublicTalkDetailSFProxyModel::setSortRole(const QByteArray &role)
{
    QSortFilterProxyModel::setSortRole(roleKey(role));
    emit sortChanged();
}

QByteArray PublicTalkDetailSFProxyModel::groupByRole() const
{
    return m_groupByRole;
}

void PublicTalkDetailSFProxyModel::setGroupByRole(const QByteArray &role)
{
    beginResetModel();
    m_groupByRole = role;
    endResetModel();
    emit groupByChanged();
}

bool PublicTalkDetailSFProxyModel::filterAcceptsRow(int sourceRow,
                                                    const QModelIndex &sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return (sourceModel()->data(index, PublicTalkDetailModel::Roles::ThemeRole).toString().contains(filterText(), Qt::CaseInsensitive)
            || sourceModel()->data(index, PublicTalkDetailModel::Roles::ThemeNumberRole).toString() == filterText());
}

bool PublicTalkDetailSFProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    int inSectionSortMode = 0; // sort by date
    // compare sections first
    switch (roleKey(groupByRole())) {
    case PublicTalkDetailModel::Roles::AlphabetRole: {
        QVariant val1 = sourceModel()->data(left, PublicTalkDetailModel::Roles::ThemeRole);
        QVariant val2 = sourceModel()->data(right, PublicTalkDetailModel::Roles::ThemeRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
        inSectionSortMode = 1; // sort by name within sections
        break;
    }
    case PublicTalkDetailModel::Roles::ThemeNumberRangeRole: {
        QVariant val1 = sourceModel()->data(left, PublicTalkDetailModel::Roles::ThemeNumberRole);
        QVariant val2 = sourceModel()->data(right, PublicTalkDetailModel::Roles::ThemeNumberRole);
        if (!val1.isNull() && !val2.isNull()) {
            int assignmentCount1 = val1.toInt();
            int assignmentCount2 = val2.toInt();
            return assignmentCount1 < assignmentCount2;
        }
        break;
    }
    case PublicTalkDetailModel::Roles::AssignmentFrequencyRangeRole: {
        QVariant val1 = sourceModel()->data(left, PublicTalkDetailModel::Roles::AssignmentCountRole);
        QVariant val2 = sourceModel()->data(right, PublicTalkDetailModel::Roles::AssignmentCountRole);
        if (!val1.isNull() && !val2.isNull()) {
            int assignmentCount1 = val1.toInt();
            int assignmentCount2 = val2.toInt();
            return assignmentCount1 < assignmentCount2;
        }
        break;
    }
    default:
        break;
    }

    if (inSectionSortMode == 1) {
        // sort by name within sections
        QVariant val1 = sourceModel()->data(left, PublicTalkDetailModel::Roles::ThemeRole);
        QVariant val2 = sourceModel()->data(right, PublicTalkDetailModel::Roles::ThemeRole);
        int compResult = QString::compare(val1.toString(), val2.toString(), Qt::CaseInsensitive);
        if (compResult != 0)
            return compResult < 0;
    }
    // sort by date within sections
    QVariant leftData = sourceModel()->data(left, PublicTalkDetailModel::Roles::DateRole);
    QVariant rightData = sourceModel()->data(right, PublicTalkDetailModel::Roles::DateRole);
    if (leftData.userType() == QMetaType::QDate) {
        return leftData.toDate() < rightData.toDate();
    } else
        return leftData.toString() < rightData.toString();
}

int PublicTalkDetailSFProxyModel::roleKey(const QByteArray &role) const
{
    QHash<int, QByteArray> roles = roleNames();
    QHashIterator<int, QByteArray> it(roles);
    while (it.hasNext()) {
        it.next();
        if (it.value() == role)
            return it.key();
    }
    return -1;
}
