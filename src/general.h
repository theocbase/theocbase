#ifndef GENERAL_H
#define GENERAL_H

#include <QProcess>
#include <QFileInfo>
#include <QDir>
#include <QDate>
#include <QLocale>
#include <QDesktopServices>
#include <QUrl>
#include <QPixmap>
#include <QPainter>
#include <QIcon>
#include <QToolButton>
#include <QPixmap>
#include <QBitmap>
#include <QListWidgetItem>
#include <QLabel>

class general
{
public:
    static QDate TextToDate(QString text);
    static QString RemoveAccents(QString const& s);
    static void ShowInGraphicalShell(QWidget *parent, const QString &location);
    static QIcon changeIconColor(QIcon icon, QColor color);

    static void changeButtonIconColor(QList<QAbstractButton *> items);
    static void changeListWidgetIconColor(QListWidget *list);
    static void changeLabelIconColor(QList<QLabel*> items);
};

#endif // GENERAL_H
