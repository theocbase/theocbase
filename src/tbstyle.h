/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2022, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TBSTYLE_H
#define TBSTYLE_H

#include <QObject>
#include <QColor>
#include <QFont>
#include <QQmlEngine>
#include <QApplication>
#include <QPalette>
#include <QtMath>

class TBStyle : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(TBStyle)
    //QML_ELEMENT
public:
    enum ColorRole {
        Primary,
        PrimaryText,
        OnPrimary,
        Medium,
        Alert,
        LMMSection1,
        LMMSection1Text,
        LMMSection2,
        LMMSection2Text,
        LMMSection3,
        LMMSection3Text,
        PublicTalk,
        PublicTalkText,
        WatchtowerStudy,
        WatchtowerStudyText,
        OutgoingSpeakers,
        OutgoingSpeakersText,
        NColorRoles = OutgoingSpeakersText + 1
    };
    Q_ENUM(ColorRole)

    struct OKLCh {
        qreal L;
        qreal C;
        qreal h;
    };

    struct OKLab {
        qreal L;
        qreal a;
        qreal b;
    };

    enum TextStyleRole {
        BodySmall,
        BodyMedium,
        BodyLarge,
        TitleSmall,
        TitleMedium,
        TitleLarge,
        HeadlineSmall,
        NTextStyleRoles = HeadlineSmall + 1
    };
    Q_ENUM(TextStyleRole)

    // colors
    Q_PROPERTY(QColor primaryColor READ primaryColor CONSTANT)
    Q_PROPERTY(QColor primaryTextColor READ primaryTextColor CONSTANT)
    Q_PROPERTY(QColor onPrimaryColor READ onPrimaryColor CONSTANT)
    Q_PROPERTY(QColor mediumColor READ mediumColor CONSTANT)
    Q_PROPERTY(QColor alertColor READ alertColor CONSTANT)
    Q_PROPERTY(QColor lmmSection1Color READ lmmSection1Color CONSTANT)
    Q_PROPERTY(QColor lmmSection1TextColor READ lmmSection1TextColor CONSTANT)
    Q_PROPERTY(QColor lmmSection2Color READ lmmSection2Color CONSTANT)
    Q_PROPERTY(QColor lmmSection2TextColor READ lmmSection2TextColor CONSTANT)
    Q_PROPERTY(QColor lmmSection3Color READ lmmSection3Color CONSTANT)
    Q_PROPERTY(QColor lmmSection3TextColor READ lmmSection3TextColor CONSTANT)
    Q_PROPERTY(QColor publicTalkColor READ publicTalkColor CONSTANT)
    Q_PROPERTY(QColor publicTalkTextColor READ publicTalkTextColor CONSTANT)
    Q_PROPERTY(QColor watchtowerStudyColor READ watchtowerStudyColor CONSTANT)
    Q_PROPERTY(QColor watchtowerStudyTextColor READ watchtowerStudyTextColor CONSTANT)
    Q_PROPERTY(QColor outgoingSpeakersColor READ outgoingSpeakersColor CONSTANT)
    Q_PROPERTY(QColor outgoingSpeakersTextColor READ outgoingSpeakersTextColor CONSTANT)
    // text styles
    Q_PROPERTY(QFont bodySmallFont READ bodySmallFont CONSTANT)
    Q_PROPERTY(QFont bodyMediumFont READ bodyMediumFont CONSTANT)
    Q_PROPERTY(QFont bodyLargeFont READ bodyLargeFont CONSTANT)
    Q_PROPERTY(QFont titleSmallFont READ titleSmallFont CONSTANT)
    Q_PROPERTY(QFont titleMediumFont READ titleMediumFont CONSTANT)
    Q_PROPERTY(QFont titleLargeFont READ titleLargeFont CONSTANT)
    Q_PROPERTY(QFont headlineSmallFont READ headlineSmallFont CONSTANT)

    static TBStyle &Instance()
    {
        static TBStyle s_instance;
        QQmlEngine::setObjectOwnership(&s_instance, QQmlEngine::CppOwnership);
        return s_instance;
    }
    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine)
    {
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)

        TBStyle *s_instance = new TBStyle();
        return s_instance;
    }

    // colors
    inline const QColor &color(ColorRole cr) const
    {
        Q_ASSERT(cr < NColorRoles);
        return colorMap[cr];
    };

    inline const QColor &primaryColor() const { return color(Primary); }
    inline const QColor &primaryTextColor() const { return color(PrimaryText); }
    inline const QColor &onPrimaryColor() const { return color(OnPrimary); }
    inline const QColor &mediumColor() const { return color(Medium); }
    inline const QColor &alertColor() const { return color(Alert); }
    inline const QColor &lmmSection1Color() const { return color(LMMSection1); }
    inline const QColor &lmmSection1TextColor() const { return color(LMMSection1Text); }
    inline const QColor &lmmSection2Color() const { return color(LMMSection2); }
    inline const QColor &lmmSection2TextColor() const { return color(LMMSection2Text); }
    inline const QColor &lmmSection3Color() const { return color(LMMSection3); }
    inline const QColor &lmmSection3TextColor() const { return color(LMMSection3Text); }
    inline const QColor &publicTalkColor() const { return color(PublicTalk); }
    inline const QColor &publicTalkTextColor() const { return color(PublicTalkText); }
    inline const QColor &watchtowerStudyColor() const { return color(WatchtowerStudy); }
    inline const QColor &watchtowerStudyTextColor() const { return color(WatchtowerStudyText); }
    inline const QColor &outgoingSpeakersColor() const { return color(OutgoingSpeakers); }
    inline const QColor &outgoingSpeakersTextColor() const { return color(OutgoingSpeakersText); }
    static QColor getCompositeColor(QColor backgroundColor, QColor foregroundColor);

    // text styles
    inline const QFont &font(TextStyleRole tr) const
    {
        Q_ASSERT(tr < NTextStyleRoles);
        return fontMap[tr];
    };
    inline const QFont &bodySmallFont() const { return font(BodySmall); }
    inline const QFont &bodyMediumFont() const { return font(BodyMedium); }
    inline const QFont &bodyLargeFont() const { return font(BodyLarge); }
    inline const QFont &titleSmallFont() const { return font(TitleSmall); }
    inline const QFont &titleMediumFont() const { return font(TitleMedium); }
    inline const QFont &titleLargeFont() const { return font(TitleLarge); }
    inline const QFont &headlineSmallFont() const { return font(HeadlineSmall); }

private:
    // colors
    /**
     * @brief getContrastRatio - Calculate the contrast ratio between two colors using the WCAG formula
     * @param lightColor - lighter color
     * @param darkColor - darker color
     * @return - contrast ratio [1-21] (21 = constrast ratio betweeen black and white)
     */
    qreal getContrastRatio(QColor lightColor, QColor darkColor);
    OKLCh convertOKLab2OKLCh(OKLab color);
    OKLab convertOKLCh2OKLab(OKLCh color);
    OKLCh convertColor2OKLCh(QColor color);
    QColor convertOKLCh2Color(OKLCh color);
    qreal getOKLChColorDifference(OKLCh color1, OKLCh color2);
    /**
     * @brief findForegroundColor - Find a new foreground color for the given background color with good contrast
     * @param backgroundColor - background color
     * @param sourceColor - color to start from
     * @param minimumContrastRatio - minimum level for acceptable contrast
     * @return - closest color to the source, which meets the required contrast ratio
     */
    QColor findForegroundColor(QColor backgroundColor, QColor sourceColor, qreal minimumContrastRatio = 4.5);
    TBStyle(QObject *parent = nullptr);
    QColor colorMap[NColorRoles];

    // text styles
    QFont fontMap[NTextStyleRoles];
};

#endif // TBSTYLE_H
