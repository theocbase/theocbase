#include "profiles.h"

profiles::profiles(QObject *parent)
    : QObject(parent), _settings("theocbase.net", "common")
{
    qDebug() << _settings.fileName();
    _currentProfile = _settings.value("active_profile", "").toString();
    currentProfileChanged();
    _settings.beginGroup("profiles");
    auto keys = _settings.allKeys();
    if (!keys.contains("default")) {
        keys.insert(0, "default");
        _settings.setValue("default", "");
    }
    _model = keys;
    _settings.endGroup();
    modelChanged();
}

QString profiles::currentProfile() const
{
    return _currentProfile;
}

void profiles::setCurrentProfile(QString name)
{
    _currentProfile = name;
    _settings.setValue("active_profile", name);
    currentProfileChanged();
}

QString profiles::databaseFile() const
{
    return "theocbase" + (currentProfile().isEmpty() ? "" : "_" + currentProfile()) + ".sqlite";
}

QString profiles::settingsName() const
{
    return "TheocBase" + (currentProfile().isEmpty() ? "" : "_" + currentProfile());
}

void profiles::createProfile(QString name)
{
    if (name != "") {
        _settings.beginGroup("profiles");
        auto keys = _settings.allKeys();
        if (!keys.contains(name, Qt::CaseInsensitive)) {
            _settings.setValue(name, "");

            _model.append(name);
            modelChanged();
        }
        _settings.endGroup();
    }
}

void profiles::deleteProfile(QString name)
{
    if (name != "") {
        if (_model.contains(name))
            _model.removeOne(name);
        _settings.remove("profiles/" + name);
        modelChanged();
    }
    // delete database file
    QString databaseDir;
#if defined(Q_OS_MAC)
    databaseDir = QDir::homePath() + "/Library/TheocBase";
#elif defined(Q_OS_WIN)
    QSettings reg("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", QSettings::NativeFormat);
    databaseDir = reg.value("AppData").toString() + "\\TheocBase";
#else
    QDir::homePath() + "/.TheocBase";
#endif
    QString dbFile = databaseDir + QDir::separator() + "theocbase_" + name + ".sqlite";
    qDebug() << dbFile;
    QFile::remove(dbFile);

    // detele settings file
    QSettings s("theocbase.net", "TheocBase_" + name);
    qDebug() << s.fileName();
    QFile::remove(s.fileName());

    // delete application data location
    QString dirPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QDir dir(dirPath);
    if (dir.cdUp()) {
        qDebug() << dir.filePath("TheocBase_" + name);
        QDir d(dir.filePath("TheocBase_" + name));
        d.removeRecursively();
    }
}

void profiles::restart()
{
    if (QProcess::startDetached(qApp->applicationFilePath(), QStringList())) {
        qApp->quit();
    }
}
