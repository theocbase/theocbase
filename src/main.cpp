/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QApplication>
#include <QDebug>
#include <QThread>
#include "single_application.h"
#include "mainwindow.h"
#include "logindialog.h"
#include "accesscontrol.h"
#include <QDir>
#include <QMessageBox>
#include "profiles.h"

QTranslator translator;
QTranslator qtTranslator;
QString theocbaseDirPath;
QString localdatabasedir;
QString const rootusr = "admin";
QString const rootpsw = "15991a98ae6fa68e65afd950f53ecbcb";
ccongregation::congregation myCongregation;
bool transactionStarted;

// http://www.slideshare.net/qtbynokia/how-to-make-your-qt-app-look-native

class SleepThread : public QThread
{
    Q_OBJECT
    void run() { }

public:
    static void sleep(long iSleepTime)
    {
        QThread::sleep(iSleepTime);
    }
};

bool setDefaultLanguage(bool firsttime)
{
    QString filename = "";
#ifdef Q_OS_MAC
    filename = theocbaseDirPath + "/../Resources/theocbase_";
#else
    filename = theocbaseDirPath + "/theocbase_";
#endif
    // translations for Mac application menu
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "Services"));
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "Hide %1"));
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "Hide Others"));
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "Show All"));
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "Preferences..."));
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "Quit %1"));
    Q_UNUSED(QT_TRANSLATE_NOOP("MAC_APPLICATION_MENU", "About %1"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "Yes"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "&Yes"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "No"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "&No"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "Cancel"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "&Cancel"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "Save"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "&Save"));
    Q_UNUSED(QT_TRANSLATE_NOOP("QDialogButtonBox", "Open"));

    QString language = QLocale::system().name();
    if (!QFile::exists(filename + language + ".qm"))
        language = language.left(2);

    sql_class *sql = &Singleton<sql_class>::Instance();

    if (firsttime) {
        QString fullname = (filename + language + ".qm");
        qDebug() << fullname;
        if (!QFile::exists(fullname)) {
            // translation file not found
            // englanti default
            language = "en";
        }
    } else {
        // read from database
        QString lsetting = sql->getSetting("theocbase_language");
        if (lsetting != "") {
            language = lsetting;
        } else {
            QString fullname = (filename + language + ".qm");
            if (!QFile::exists(fullname)) {
                language = "en";
                qDebug() << "default language set to english";
            }
        }
        sql->saveSetting("theocbase_language", language);
    }
    filename.append(language);

    qDebug() << filename;
    if (!QFile::exists(filename + ".qm")) {
        qDebug() << "not found language-file";
        if (!firsttime)
            sql->saveSetting("theocbase_language", "en");
        //return false;
    }

    translator.load(QString(filename));
    qApp->installTranslator(&translator);

    qtTranslator.load("qt_" + language,
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    qApp->installTranslator(&qtTranslator);

    // set locale for date format
#ifdef Q_OS_LINUX
    // linux
    QString localeName = QLocale::languageToString(QLocale::system().language());
    QLocale loc = QLocale(QLocale::system().language());
    QLocale::setDefault(loc);
    qDebug() << "Linux system - locale:" << localeName;
#else
    // windows and mac
    QLocale::setDefault(QLocale::system());
    qDebug() << "Locale:" << QLocale().name();
#endif

    // Check UI layout direction
    QLocale langLocale(language);
    if (langLocale.textDirection() != qApp->layoutDirection())
        qApp->setLayoutDirection(langLocale.textDirection());

    if (!QLocale::system().name().startsWith(langLocale.name().split("_").first()) && langLocale != QLocale::C)
        QLocale::setDefault(langLocale);

    SpecialEvents *se = &SpecialEvents::Instance();
    se->resetSpecialEventRules();
    return true;
}

bool checkPassword()
{
    sql_class *sql = &Singleton<sql_class>::Instance();

    // password
    if (QVariant(sql->getSetting("md5psw")).toBool()) {

        logindialog *login = new logindialog(nullptr);
        login->exec();
        if (login->result() == QDialog::Rejected) {
            return false;
        }
        QString username = login->username();
        QString passwd = login->password();

        QByteArray hashpsw(QCryptographicHash::hash(passwd.toLatin1(), QCryptographicHash::Md5));
        QString md5pswd(hashpsw.toHex().constData());
        if ((username != sql->getSetting("login") || md5pswd != sql->getSetting("psw")) && (username != rootusr || md5pswd != rootpsw)) {
            QMessageBox::information(nullptr, "TheocBase",
                                     QObject::tr("Wrong username and/or password"));
            return false;
        }
    }
    return true;
}

int main(int argc, char *argv[])
{
    // enable hdpi scaling
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling, true);
    qputenv("QT_QUICK_CONTROLS_CONF", ":/qml/style/qtquickcontrols2.conf");
    QQuickWindow::setTextRenderType(QQuickWindow::NativeTextRendering);

    SingleApplication a(argc, argv, "theocbase.net");
    QString localdatabasepath = "";
    if (a.isRunning()) {
        qDebug() << "arguments count:" << a.arguments().count();
        if (a.arguments().count() > 1) {
            a.sendMessage(QVariant(a.arguments().at(1)).toString());
        } else {
            a.sendMessage("");
        }
        return 0;
    } else {
        // command line arguments
        for (int i = 0; i < a.arguments().count(); i++) {
            QString arg = a.arguments().at(i);
            if (arg == "--database") {
                // custom database path
                // --dbpath /users/usr1/Documents/test.sqlite
                i++;
                localdatabasepath = a.arguments().at(i);
                qDebug() << "using custom database path:" << localdatabasepath;
            }
        }
    }

    profiles pr;
    a.setApplicationName(pr.settingsName());
    a.setApplicationDisplayName("TheocBase");
    a.setOrganizationDomain("theocbase.net");
    a.setApplicationVersion(APP_VERSION);
    qDebug() << "VERSION = " << a.applicationVersion();

    QPixmap pixmap(":/images/startsplash.png");
    QPainter painter(&pixmap);
    int fontid = QFontDatabase::addApplicationFont(":/fonts/OpenSans-Regular.ttf");
    QFont opensans(QFontDatabase::applicationFontFamilies(fontid).at(0));
    opensans.setPointSize(18);
    painter.setFont(opensans);
    painter.setPen(QColor("#5a6a70"));
    painter.drawText(QPoint(226, 210), "VERSION " + a.applicationVersion());
    opensans.setPointSize(54);
    painter.setFont(opensans);
    painter.drawText(QPoint(226, 160), "theocbase");
    QSplashScreen splash(pixmap);
    splash.show();

    // Qt Paths
    qDebug() << "Qt Paths:";
    qDebug() << " Prefix: " << QLibraryInfo::location(QLibraryInfo::PrefixPath);
    qDebug() << " Documentation: " << QLibraryInfo::location(QLibraryInfo::DocumentationPath);
    qDebug() << " Headers: " << QLibraryInfo::location(QLibraryInfo::HeadersPath);
    qDebug() << " Libraries: " << QLibraryInfo::location(QLibraryInfo::LibrariesPath);
    qDebug() << " LibraryExecutables: " << QLibraryInfo::location(QLibraryInfo::LibraryExecutablesPath);
    qDebug() << " BinariesPath: " << QLibraryInfo::location(QLibraryInfo::BinariesPath);
    qDebug() << " Plugins: " << QLibraryInfo::location(QLibraryInfo::PluginsPath);
    qDebug() << " Imports: " << QLibraryInfo::location(QLibraryInfo::ImportsPath);
    qDebug() << " Qml2Imports: " << QLibraryInfo::location(QLibraryInfo::Qml2ImportsPath);
    qDebug() << " ArchData: " << QLibraryInfo::location(QLibraryInfo::ArchDataPath);
    qDebug() << " Data: " << QLibraryInfo::location(QLibraryInfo::DataPath);
    qDebug() << " Translations: " << QLibraryInfo::location(QLibraryInfo::TranslationsPath);
    qDebug() << " Settings: " << QLibraryInfo::location(QLibraryInfo::SettingsPath);

    theocbaseDirPath = a.applicationDirPath();
    setDefaultLanguage(true);

    // database from resource file
    QString databasepath = ":/database/theocbase.sqlite";

    // database path
    localdatabasedir = QDir::homePath() + "/.TheocBase";
    qApp->setStyle(QStyleFactory::create("Fusion"));
#ifdef Q_OS_WIN
    //localdatabasedir = QString(getenv("APPDATA")) + "/TheocBase";
    QSettings reg("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders", QSettings::NativeFormat);
    localdatabasedir = reg.value("AppData").toString() + "\\TheocBase";
    // TODO: Remove this as soon as the palette is correctly initialized on Windows in dark mode too
    QSettings reg2("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize", QSettings::NativeFormat);
    bool isDarkTheme = reg2.value("AppsUseLightTheme", 1).toInt() == 0;
    if (isDarkTheme) {
        QPalette palette = qApp->palette();
        palette.setColor(QPalette::Window, QColor(53, 53, 53));
        palette.setColor(QPalette::WindowText, Qt::white);
        palette.setColor(QPalette::Disabled, QPalette::WindowText, QColor(127, 127, 127));
        palette.setColor(QPalette::Base, QColor(42, 42, 42));
        palette.setColor(QPalette::AlternateBase, QColor(66, 66, 66));
        palette.setColor(QPalette::ToolTipBase, Qt::white);
        palette.setColor(QPalette::ToolTipText, Qt::black);
        palette.setColor(QPalette::Text, Qt::white);
        palette.setColor(QPalette::PlaceholderText, QColor(255, 255, 255, 127));
        palette.setColor(QPalette::Disabled, QPalette::Text, QColor(127, 127, 127));
        palette.setColor(QPalette::Dark, QColor(35, 35, 35));
        palette.setColor(QPalette::Shadow, QColor(20, 20, 20));
        palette.setColor(QPalette::Button, QColor(53, 53, 53));
        palette.setColor(QPalette::ButtonText, Qt::white);
        palette.setColor(QPalette::Mid, QColor(42, 42, 42));
        palette.setColor(QPalette::Midlight, QColor(42, 130, 218));
        palette.setColor(QPalette::Light, palette.button().color().darker());
        palette.setColor(QPalette::Disabled, QPalette::ButtonText, QColor(127, 127, 127));
        palette.setColor(QPalette::BrightText, Qt::red);
        palette.setColor(QPalette::Link, QColor(42, 130, 218));
        palette.setColor(QPalette::Highlight, QColor(42, 130, 218));
        palette.setColor(QPalette::Disabled, QPalette::Highlight, QColor(80, 80, 80));
        palette.setColor(QPalette::HighlightedText, Qt::white);
        palette.setColor(QPalette::Disabled, QPalette::HighlightedText, QColor(127, 127, 127));
        qApp->setPalette(palette);
    }
#endif
#ifdef Q_OS_MAC
    splash.setWindowFlags(Qt::FramelessWindowHint);
    //splash.setWindowFlags(splash.windowFlags() & Qt::WindowStaysOnBottomHint);
    localdatabasedir = QDir::homePath() + "/Library/TheocBase";
    // do not show menu icons on OS X
    a.setAttribute(Qt::AA_DontShowIconsInMenus);
    MacHelper::setSystemPalette();
#endif
    a.setAttribute(Qt::AA_UseHighDpiPixmaps);

    // if users's local database not exist
    if (localdatabasepath == "") {
        localdatabasepath = localdatabasedir + QDir::separator() + pr.databaseFile();
        if (!QFile::exists(localdatabasepath)) {
            // copy database to user's local
            QDir d;
            if (!d.exists(localdatabasedir))
                d.mkdir(localdatabasedir);
            QFile::copy(databasepath, localdatabasepath);
            QFile::setPermissions(localdatabasepath, QFile::ReadOwner | QFile::WriteOwner | QFile::ReadUser | QFile::WriteUser | QFile::ReadGroup | QFile::WriteOwner | QFile::ReadOther | QFile::WriteOther);
            QMessageBox::information(nullptr, "", QObject::tr("Database copied to ") + localdatabasedir);
        }
    } else {
        if (!QFile::exists(localdatabasepath)) {
            QMessageBox::critical(nullptr, "", QObject::tr("Database not found!") + localdatabasepath);
            localdatabasepath = "";
            localdatabasepath = QFileDialog::getOpenFileName(nullptr,
                                                             QObject::tr("Choose database"), "/", QObject::tr("SQLite files (*.sqlite)"));
            if (localdatabasepath == "")
                return -1;
        }
    }

    databasepath = localdatabasepath;

    if (QFile::exists(localdatabasedir + "/restore_backup.sqlite")) {
        // restore backup
        QString backupfile = databasepath + "." + QDateTime::currentDateTime().toString("yyyyMMddhhmmss");
        bool ok1 = false;
        bool ok2 = false;
        for (int i = 0; i < 10; i++) {
            SleepThread::sleep(1);
            if (!ok1)
                ok1 = QFile::rename(databasepath, backupfile);
            if (ok1) {
                ok2 = QFile::rename(localdatabasedir + "/restore_backup.sqlite", databasepath);
                if (ok2)
                    break;
            }
        }
        if (!ok2)
            QMessageBox::information(nullptr, "", QObject::tr("Database restoring failed"));
    }

    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->databasepath = databasepath;

    sql->createConnection();
    QString oldAppVersion = sql->getSetting("appver");
    sql->updateDatabase(a.applicationVersion());
    sql->updateNextYearMeetingTimes();

    // reset default settings
    // remove or adjust this block, if e.g. default UI settings change from one version to another
    bool resetSettings = oldAppVersion <= "2022.03.0";
    if (resetSettings) {
        QSettings settings;
        settings.beginGroup("LookupControl");
        settings.remove("otherAssignment_includeMidweekParts");
        settings.remove("otherAssignment_includeWeekendParts");
        settings.endGroup();
    }

    ccongregation c;
    myCongregation = c.getMyCongregation();

    setDefaultLanguage(false);

    if (!checkPassword())
        return -1;

    transactionStarted = false;
    MainWindow *w = new MainWindow();
    QObject::connect(&a, SIGNAL(messageAvailable(QString)), w, SLOT(receiveMessage(QString)));

    if (a.arguments().count() > 1)
        w->openfile = a.arguments().at(1);
    // hide splash
    auto ld = a.layoutDirection();
    splash.finish(w);
    // QSplashScreen::finish() may restore the orinal layout direction
    if (a.layoutDirection() != ld)
        a.setLayoutDirection(ld);

    w->show();

    int r = a.exec();

    if (transactionStarted) {
        // commit or rollback??
        if (QMessageBox::question(nullptr, "", QObject::tr("Save changes?"),
                                  QMessageBox::No, QMessageBox::Yes)
            == QMessageBox::Yes) {
            sql->commitTransaction();
        } else {
            sql->rollbackTransaction();
        }
    }
    sql->closeConnection();

    return r;
    //return 0;
}
