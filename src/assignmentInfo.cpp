/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2022, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "assignmentInfo.h"

AssignmentInfo::AssignmentInfo(QObject *parent)
    : QObject(parent)
{
}

AssignmentInfo::AssignmentInfo(MeetingType meetingType, MeetingSection meetingSection, MeetingPart meetingPart, AssignmentCategory assignmentCategory, AssignmentType assignmentType, AssignmentSubtype assignmentSubtype, person::UseFor role, int roomNumber, QObject *parent)
    : QObject(parent), m_meetingType(meetingType), m_meetingSection(meetingSection), m_meetingPart(meetingPart), m_assignmentCategory(assignmentCategory), m_assignmentType(assignmentType), m_assignmentSubtype(assignmentSubtype), m_role(role), m_roomNumber(roomNumber)
{
}

MeetingType AssignmentInfo::meetingType() const
{
    return m_meetingType;
}

MeetingSection AssignmentInfo::meetingSection() const
{
    return m_meetingSection;
}

QString AssignmentInfo::meetingSectionName() const
{
    return MeetingSectionClass::toString(meetingSection());
}

QColor AssignmentInfo::meetingSectionColor() const
{
    TBStyle *tbStyle = &TBStyle::Instance();
    switch (meetingSection()) {
    case MeetingSection::TreasuresFromGodsWord:
        return tbStyle->lmmSection1Color();
    case MeetingSection::ApplyYourselfToTheFieldMinistry:
        return tbStyle->lmmSection2Color();
    case MeetingSection::LivingAsChristians:
        return tbStyle->lmmSection3Color();
    case MeetingSection::PublicTalk:
        return tbStyle->publicTalkColor();
    case MeetingSection::WatchtowerStudy:
        return tbStyle->watchtowerStudyColor();
    }
}

QColor AssignmentInfo::meetingSectionTextColor() const
{
    TBStyle *tbStyle = &TBStyle::Instance();
    switch (meetingSection()) {
    case MeetingSection::TreasuresFromGodsWord:
        return tbStyle->lmmSection1TextColor();
    case MeetingSection::ApplyYourselfToTheFieldMinistry:
        return tbStyle->lmmSection2TextColor();
    case MeetingSection::LivingAsChristians:
        return tbStyle->lmmSection3TextColor();
    case MeetingSection::PublicTalk:
        return tbStyle->publicTalkTextColor();
    case MeetingSection::WatchtowerStudy:
        return tbStyle->watchtowerStudyTextColor();
    }
}

MeetingPart AssignmentInfo::meetingPart() const
{
    return m_meetingPart;
}

QString AssignmentInfo::meetingPartName() const
{
    return MeetingPartClass::toString(meetingPart());
}

AssignmentCategory AssignmentInfo::assignmentCategory() const
{
    return m_assignmentCategory;
}

AssignmentType AssignmentInfo::assignmentType() const
{
    return m_assignmentType;
}

QString AssignmentInfo::assignmentTypeName() const
{
    return AssignmentTypeClass::toString(assignmentType());
}

AssignmentSubtype AssignmentInfo::assignmentSubtype() const
{
    return m_assignmentSubtype;
}

person::UseFor AssignmentInfo::role() const
{
    return m_role;
}

bool AssignmentInfo::canCounsel() const
{
    return assignmentCategory() == AssignmentCategory::StudentPart;
}

bool AssignmentInfo::canHaveAssistant() const
{
    return assignmentType() == AssignmentType::Demonstration;
}

bool AssignmentInfo::isSupplementary() const
{
    return assignmentCategory() != AssignmentCategory::NonstudentMeetingPart
            && assignmentCategory() != AssignmentCategory::StudentPart;
}

int AssignmentInfo::roomNumber() const
{
    return m_roomNumber;
}

AssignmentInfos::AssignmentInfos(QObject *parent)
    : QObject(parent)
{
    initAssignmentInfos();
}

AssignmentInfo *AssignmentInfos::findAssignmentInfo(MeetingType meetingType, MeetingPart meetingPart, bool isSupplementary, int roomNumber, AssignmentSubtype assignmentSubtype) const
{
    auto it = std::find_if(m_assignmentInfos.begin(), m_assignmentInfos.end(),
                           [&meetingType, &meetingPart, &isSupplementary, &roomNumber, &assignmentSubtype](const AssignmentInfo *item) {
                               return item->meetingType() == meetingType && item->meetingPart() == meetingPart
                                       && (!isSupplementary
                                           || (item->assignmentCategory() == AssignmentCategory::StudentPartAssistant
                                               || item->assignmentCategory() == AssignmentCategory::OtherAssignment
                                               || item->assignmentCategory() == AssignmentCategory::SupportTask))
                                       && item->roomNumber() == roomNumber
                                       && item->assignmentSubtype() == assignmentSubtype;
                           });
    if (it != m_assignmentInfos.end()) {
        auto index = std::distance(m_assignmentInfos.begin(), it);
        return m_assignmentInfos[static_cast<size_t>(index)];
    }
    return nullptr;
}

AssignmentInfo *AssignmentInfos::findAssignmentInfo(int talkId, bool isSupplementary, int roomNumber, AssignmentSubtype assignmentSubtype) const
{
    return findAssignmentInfo(
            talkId > 20 && talkId < 27
                    ? MeetingType::WeekendMeeting
                    : MeetingType::MidweekMeeting,
            MeetingPartClass::fromInt(talkId), isSupplementary, roomNumber, assignmentSubtype);
}

QList<MeetingPart> AssignmentInfos::getMeetingParts(QList<MeetingType> meetingTypes, QList<AssignmentCategory> assignmntCategories, AssignmentType assignmentType) const
{
    QList<MeetingPart> result;
    foreach (AssignmentInfo *item, m_assignmentInfos) {
        foreach (MeetingType meetingType, meetingTypes) {
            foreach (AssignmentCategory assignmntCategory, assignmntCategories) {
                if (item->meetingType() == meetingType && item->assignmentCategory() == assignmntCategory && !result.contains(item->meetingPart())
                    && (assignmentType == AssignmentType::None || item->assignmentType() == assignmentType))
                    result.append(item->meetingPart());
            }
        }
    }
    return result;
}

void AssignmentInfos::initAssignmentInfos()
{
    if (m_assignmentInfos.count() > 0)
        m_assignmentInfos.clear();

    // NOTE: parent must be set in order to protect it from destroying by qml
    // placeholder for invalid assignment info
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::None, MeetingSection::None, MeetingPart::None,
                                                AssignmentCategory::None, AssignmentType::None, AssignmentSubtype::None, person::None, 0, this));
    // valid definitions
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::LMM_Chairman,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Chairman, AssignmentSubtype::None, person::LMM_Chairman, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::LMM_Counselor,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Counselor, AssignmentSubtype::None, person::LMM_Chairman, 2, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::LMM_Counselor,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Counselor, AssignmentSubtype::None, person::LMM_Chairman, 3, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::SongAndPrayer,
                                                AssignmentCategory::OtherAssignment, AssignmentType::Prayer, AssignmentSubtype::None, person::Prayer, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::LMM_Treasures,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Talk, AssignmentSubtype::None, person::LMM_Treasures, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::LMM_Digging,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Discussion, AssignmentSubtype::None, person::LMM_Digging, 1, this));
    for (int i = 1; i <= 3; ++i) {
        // add assignment infos for main hall and each auxiliary room
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::TreasuresFromGodsWord, MeetingPart::LMM_BibleReading,
                                                    AssignmentCategory::StudentPart, AssignmentType::Reader, AssignmentSubtype::None, person::LMM_BibleReading, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_SampleConversationVideo,
                                                    AssignmentCategory::NonstudentMeetingPart, AssignmentType::Discussion, AssignmentSubtype::None, person::LMM_OtherVideoPart, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_InitialCall,
                                                    AssignmentCategory::StudentPart, AssignmentType::Demonstration, AssignmentSubtype::None, person::LMM_InitialCall, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_InitialCall,
                                                    AssignmentCategory::StudentPartAssistant, AssignmentType::Assistant, AssignmentSubtype::None, person::Assistant, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_ReturnVisit,
                                                    AssignmentCategory::StudentPart, AssignmentType::Demonstration, AssignmentSubtype::None, person::LMM_ReturnVisit, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_ReturnVisit,
                                                    AssignmentCategory::StudentPartAssistant, AssignmentType::Assistant, AssignmentSubtype::None, person::Assistant, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_BibleStudy,
                                                    AssignmentCategory::StudentPart, AssignmentType::Demonstration, AssignmentSubtype::None, person::LMM_BibleStudy, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_BibleStudy,
                                                    AssignmentCategory::StudentPartAssistant, AssignmentType::Assistant, AssignmentSubtype::None, person::Assistant, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_StudentTalk,
                                                    AssignmentCategory::StudentPart, AssignmentType::Talk, AssignmentSubtype::None, person::LMM_ApplyTalks, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_MemorialInvitation,
                                                    AssignmentCategory::StudentPart, AssignmentType::Demonstration, AssignmentSubtype::None, person::LMM_InitialCall, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_MemorialInvitation,
                                                    AssignmentCategory::StudentPartAssistant, AssignmentType::Assistant, AssignmentSubtype::None, person::Assistant, i, this));
        m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::ApplyYourselfToTheFieldMinistry, MeetingPart::LMM_OtherFMVideoPart,
                                                    AssignmentCategory::NonstudentMeetingPart, AssignmentType::Discussion, AssignmentSubtype::None, person::LMM_OtherVideoPart, i, this));
    }
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::LivingAsChristians, MeetingPart::LMM_LivingTalk1,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Discussion, AssignmentSubtype::None, person::LMM_LivingTalks, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::LivingAsChristians, MeetingPart::LMM_LivingTalk2,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Discussion, AssignmentSubtype::None, person::LMM_LivingTalks, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::LivingAsChristians, MeetingPart::LMM_LivingTalk3,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Discussion, AssignmentSubtype::None, person::LMM_LivingTalks, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::LivingAsChristians, MeetingPart::LMM_CBS,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::StudyConductor, AssignmentSubtype::None, person::CBSConductor, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::LivingAsChristians, MeetingPart::LMM_CBS,
                                                AssignmentCategory::OtherAssignment, AssignmentType::Reader, AssignmentSubtype::None, person::CBSReader, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::MidweekMeeting, MeetingSection::LivingAsChristians, MeetingPart::Service_Talk,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Talk, AssignmentSubtype::None, person::PublicTalk, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::PublicTalk, MeetingPart::PM_Chairman,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Chairman, AssignmentSubtype::None, person::Chairman, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::PublicTalk, MeetingPart::SongAndPrayer,
                                                AssignmentCategory::OtherAssignment, AssignmentType::Prayer, AssignmentSubtype::OpeningPrayer, person::Prayer, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::PublicTalk, MeetingPart::PublicTalk,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::Talk, AssignmentSubtype::None, person::PublicTalk, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::WatchtowerStudy, MeetingPart::WatchtowerStudy,
                                                AssignmentCategory::NonstudentMeetingPart, AssignmentType::StudyConductor, AssignmentSubtype::None, person::WtCondoctor, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::WatchtowerStudy, MeetingPart::WatchtowerStudy,
                                                AssignmentCategory::OtherAssignment, AssignmentType::Reader, AssignmentSubtype::None, person::WtReader, 1, this));
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::WatchtowerStudy, MeetingPart::SongAndPrayer,
                                                AssignmentCategory::OtherAssignment, AssignmentType::Prayer, AssignmentSubtype::ConcludingPrayer, person::Prayer, 1, this));
    // support tasks
    m_assignmentInfos.append(new AssignmentInfo(MeetingType::WeekendMeeting, MeetingSection::PublicTalk, MeetingPart::PublicTalk,
                                                AssignmentCategory::SupportTask, AssignmentType::Hospitality, AssignmentSubtype::None, person::Hospitality, 1, this));
}

// UnknownAssignmentInfoIException
//======================
UnknownAssignmentInfoIException::UnknownAssignmentInfoIException(MeetingType meetingType, int roomNumber, MeetingPart meetingPart, bool isSupplementary)
    : std::invalid_argument(QObject::tr("Unknown assignment info = %1; class %2; %3; assistant (%4)")
                                    .arg(MeetingTypeClass::toString(meetingType))
                                    .arg(roomNumber)
                                    .arg(MeetingPartClass::toString(meetingPart))
                                    .arg(isSupplementary)
                                    .toLocal8Bit()
                                    .constData())
{
}

// UnknownTalkIException
//======================
UnknownTalkIException::UnknownTalkIException(int talkId)
    : std::invalid_argument(QObject::tr("Unknown meeting part (%1).\nPlease check the schedule for invalid or obsolete meeting parts.").arg(QString::number(talkId)).toLocal8Bit().constData())
{
}
UnknownTalkIException::UnknownTalkIException(int talkId, QDate date)
    : std::invalid_argument(QObject::tr("Unknown meeting part (%1).\nPlease check the schedule for invalid or obsolete meeting parts in the week starting %2.").arg(QString::number(talkId)).arg(QLocale().toString(date, QLocale::LongFormat)).toLocal8Bit().constData())
{
}
