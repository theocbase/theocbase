#ifndef WTIMPORT_H
#define WTIMPORT_H

#include "epub.h"
#include "internet.h"
#include "jwpub.h"
#include "xml_reader.h"
#include "sharedlib_global.h"

class TB_EXPORT wtimport : public QObject
{
    Q_OBJECT

public:
    explicit wtimport(QObject *parent = nullptr);
    explicit wtimport(QString fileName);
    QString Import();
    Q_INVOKABLE QString importFile(const QString fileName);
    Q_INVOKABLE void exportAssistFiles(); // doesn't belong in a class with "import" in the name, but we'll move it later :)

    enum xmlPartsContexts {
        studyArticleInfo,
        articlePara,
        h1,
        link,
        article,
        title,
        song
    };

    enum rxSongStates {
        noLoadAttempt,
        attempted,
        loaded
    };

    epub epb;
    bool prepared;
    QString lastError;

signals:

public slots:
    void ProcessTOCEntry(QString href, QString chapter);
    void xmlPartFound(QXmlStreamReader *xml, QXmlStreamReader::TokenType tokenType, int context, int relativeDepth);
    void ProcessCoverImage(QString fileName);

private:
    void init(QString fileName);
    bool downloadArticleDates();
    void addArticleDate(int article, QDate dt);
    sql_class *sql;
    int year;
    int month;
    int validDates;
    QString firstDate;
    QString lastDate;
    int rxSongState;
    QRegularExpression rxSong;
    QMap<int, QDate> articleDates;
    int queueOpenSong;
    int queueCloseSong;
    int queueArticle_TOC;
    int queueArticle_Detail;
    QString queueTheme;
};

#endif // WTIMPORT_H
