<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="he">
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="30"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="35"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="212"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="244"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="244"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="375"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="30"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="288"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="313"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="436"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="555"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
</context>
<context>
    <name>CalendarPopup</name>
    <message>
        <location filename="../qml/controls/+fusion/CalendarPopup.qml" line="74"/>
        <location filename="../qml/controls/+material/CalendarPopup.qml" line="76"/>
        <location filename="../qml/controls/+universal/CalendarPopup.qml" line="74"/>
        <location filename="../qml/controls/CalendarPopup.qml" line="74"/>
        <source>Today</source>
        <translation>היום</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>הצג כתובת קהילה</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="66"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>האם אתה בטוח שברצונך למחוק לצמיתות את נתוני הענן שלך?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="199"/>
        <source>Last synchronized: %1</source>
        <translation>סונכרן לאחרונה: %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="204"/>
        <source>Synchronize</source>
        <translation>לסנכרן</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="217"/>
        <source>Delete Cloud Data</source>
        <translation>מחק נתוני ענן</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>העזרה של TheocBase</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>לא ניתן להפעיל את מציג העזרה (%1)</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentValidator</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="287"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>האדם המוקצה אינו חבר בקהילה.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="301"/>
        <source>Unassignable. Please check the publisher&#39;s settings.</source>
        <translation>לא ניתן להקצאה. בדוק את ההגדרות של המבשר.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="333"/>
        <source>The assigned person is not available on this day.</source>
        <translation>האדם המוקצה אינו זמין ביום זה.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="340"/>
        <source>Assignment incomplete. Please assign an assistant.</source>
        <translation>המשימה לא הושלמה. נא להקצות עוזר.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="344"/>
        <source>Assignment incomplete. Please assign a student.</source>
        <translation>המשימה לא הושלמה. נא להקצות תלמיד.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="353"/>
        <source>Assignment incomplete. Assign a different person as student or assistant.</source>
        <translation>המשימה לא הושלמה. הקצה אדם אחר כתלמיד או עוזר.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="368"/>
        <source>The assistant should be of the same sex or should be a family member.</source>
        <translation>העוזר צריך להיות מאותו מין או להיות בן משפחה.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="374"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>העוזר לא אמור להיות מישהו מהמין השני.</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="26"/>
        <source>Opening Comments</source>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="26"/>
        <source>Concluding Comments</source>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="51"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="178"/>
        <source>Please find below details of your assignment:</source>
        <translation>אנא ראה למטה את פרטי המשימה שלך:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="179"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Source material</source>
        <translation>חומר מקור</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Please find below details of your assignment:</source>
        <translation>אנא ראה למטה את פרטי המשימה שלך:</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="185"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="189"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="196"/>
        <source>Study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="198"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="199"/>
        <source>Source material</source>
        <translation>חומר מקור</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="204"/>
        <source>Main hall</source>
        <translation>אולם ראשי</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="207"/>
        <source>Auxiliary classroom 1</source>
        <translation>כיתה משנה  א</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="210"/>
        <source>Auxiliary classroom 2</source>
        <translation>כיתה משנה  ב</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="213"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>מקום הגשת המשימה</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="646"/>
        <source>Enter source material here</source>
        <translation>הזן כאן חומר מקור</translation>
    </message>
</context>
<context>
    <name>LMMeetingValidator</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="902"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>האדם המוקצה אינו חבר בקהילה.</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="908"/>
        <source>Unassignable. Please check the publisher&#39;s settings.</source>
        <translation>לא ניתן להקצאה. בדוק את ההגדרות של המבשר.</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="926"/>
        <source>The assigned person is not available on this day.</source>
        <translation>האדם המוקצה אינו זמין ביום זה.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>שם משתמש או דוא&quot;ל</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>התחברות</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>שכחת ססמא</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>צור חשבון</translation>
    </message>
</context>
<context>
    <name>LookupControl</name>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="41"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="41"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="41"/>
        <location filename="../qml/controls/LookupControl.qml" line="41"/>
        <source>Search</source>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="118"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="118"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="118"/>
        <location filename="../qml/controls/LookupControl.qml" line="118"/>
        <source>By %1</source>
        <comment>Arrange list by the selected criteria</comment>
        <comment>the &apos;%1&apos;-part will be replaced by terms, such as date, name, ...</comment>
        <translation>לפי %1</translation>
    </message>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="168"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="168"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="168"/>
        <location filename="../qml/controls/LookupControl.qml" line="168"/>
        <source>Count=%1</source>
        <comment>Number of rows in a list</comment>
        <comment>the &apos;%1&apos;-part will be replaced by the actual number</comment>
        <translation>ספירה=%1</translation>
    </message>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="188"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="188"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="188"/>
        <location filename="../qml/controls/LookupControl.qml" line="188"/>
        <source>Settings</source>
        <translation>הגדרות</translation>
    </message>
</context>
<context>
    <name>LookupControlMoreMenu</name>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="41"/>
        <source>Display details</source>
        <translation>הצג פרטים</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="47"/>
        <source>Off</source>
        <translation>כבוי</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="58"/>
        <source>1 Line</source>
        <translation>1 שורה</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="69"/>
        <source>2 Lines</source>
        <translation>2 שורות</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="80"/>
        <source>3 Lines</source>
        <translation>3 שורות</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="91"/>
        <source>Automatically</source>
        <translation>באופן אוטומטי</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="107"/>
        <source>Include midweek parts</source>
        <translation>כלול חלקים באמצע השבוע</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="109"/>
        <source>Include weekend parts</source>
        <translation>כלול חלקי סוף שבוע</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="123"/>
        <source>Labeling</source>
        <comment>To select which text to display, e.g. the theme or name of a meeting part</comment>
        <translation>תיוג</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="20"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="22"/>
        <source>Meeting part</source>
        <translation>חלק אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="24"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="167"/>
        <source>Hide unavailables</source>
        <translation>הסתר דברים לא זמינים</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="176"/>
        <source>Reset default settings</source>
        <translation>אפס את הגדרות ברירת המחדל</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Services</source>
        <translation>שירותים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide %1</source>
        <translation>הסתר את%1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Hide Others</source>
        <translation>הסתר אחרים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Show All</source>
        <translation>הצג הכול</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Preferences...</source>
        <translation>העדפות ...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>Quit %1</source>
        <translation>צא מ-%1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>About %1</source>
        <translation>בערך:%1</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="32"/>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="301"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="423"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>יועץ כיתה משנית ב&apos;</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="544"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>יועץ כיתת לימוד שלישי</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="199"/>
        <source>TREASURES FROM GOD&#39;S WORD</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="206"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>התייעל בשירות השדה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="211"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="226"/>
        <location filename="../qml/MWMeetingModule.qml" line="246"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="242"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="256"/>
        <source>Import Schedule...</source>
        <translation>לוח הזמנים לייבא ...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="309"/>
        <location filename="../qml/MWMeetingModule.qml" line="584"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>MH</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="310"/>
        <location filename="../qml/MWMeetingModule.qml" line="585"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>A1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="311"/>
        <location filename="../qml/MWMeetingModule.qml" line="586"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>A2</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="319"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="319"/>
        <source>Counselor</source>
        <translation>יועצת</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="369"/>
        <location filename="../qml/MWMeetingModule.qml" line="421"/>
        <source>Song %1 &amp; Prayer</source>
        <translation>שיר% 1ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="389"/>
        <source>Opening Comments</source>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="407"/>
        <source>Concluding Comments</source>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="535"/>
        <source>Song %1</source>
        <translation>שיר %1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="562"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="571"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="32"/>
        <source>Song %1 and Prayer</source>
        <translation>שיר%1 ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="200"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="468"/>
        <source>Copyright</source>
        <translation>זכויות יוצרים</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>ספריות Qt המורשות תחת ה- GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="468"/>
        <source>TheocBase Team</source>
        <translation>צוות TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="333"/>
        <source>Last synchronized</source>
        <translation>סונכרן לאחרונה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Licensed under GPLv3.</source>
        <translation>מורשה תחת GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>Versions of Qt libraries </source>
        <translation>גרסאות ספריות Qt </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1272"/>
        <source>TheocBase data exchange</source>
        <translation>חילופי נתונים של TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="538"/>
        <source>New update available. Do you want to install?</source>
        <translation>עדכון חדש זמין. האם אתה רוצה להתקין?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="542"/>
        <source>No new update available</source>
        <translation>אין עדכון חדש זמין</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="564"/>
        <source>Save file</source>
        <translation>שמור את הקובץ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1627"/>
        <source>Select ePub file</source>
        <translation>בחר קובץ ePub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1545"/>
        <source>Send e-mail reminders?</source>
        <translation>לשלוח תזכורות בדוא&apos;ל?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1580"/>
        <source>Updates available...</source>
        <translation>עדכונים זמינים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1158"/>
        <source>Error sending e-mail</source>
        <translation>שגיאה בשליחת דוא&quot;ל</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="422"/>
        <source>WEEK STARTING %1</source>
        <translation>שבוע החל %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="557"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>ייצוא נואמים יוצאים עדיין לא מוכן, מצטער.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="568"/>
        <source>Save folder</source>
        <translation>שמור תיקיה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1160"/>
        <source>E-mail sent successfully</source>
        <translation>דוא&quot;ל נשלח בהצלחה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="581"/>
        <source>Saved successfully</source>
        <translation>נשמר בהצלחה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="649"/>
        <location filename="../mainwindow.cpp" line="654"/>
        <source>Counselor-Class II</source>
        <translation>יועץ-כתה ב&apos;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="650"/>
        <location filename="../mainwindow.cpp" line="655"/>
        <source>Counselor-Class III</source>
        <translation>יועץ-כתה ד&apos;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="670"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>עוזר/ת ל-%1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="737"/>
        <location filename="../mainwindow.cpp" line="952"/>
        <location filename="../mainwindow.cpp" line="977"/>
        <source>Kingdom Hall</source>
        <translation>אולם הממלכה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="648"/>
        <location filename="../mainwindow.cpp" line="653"/>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="677"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>קורין לשיעור המקרא של הקהילה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="662"/>
        <location filename="../mainwindow.cpp" line="678"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="651"/>
        <location filename="../mainwindow.cpp" line="656"/>
        <location filename="../mainwindow.cpp" line="701"/>
        <location filename="../mainwindow.cpp" line="703"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="662"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="760"/>
        <source>Speaker</source>
        <translation>נאום</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="765"/>
        <source>Watchtower Study Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="767"/>
        <source>Watchtower reader</source>
        <translation>קרין המצפה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1013"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation>את אותם שינויים ניתן למצוא הן באופן מקומי והן בענן (%1 שורות). האם אתה רוצה לשמור על השינויים המקומיים?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1054"/>
        <source>The cloud data has now been deleted.</source>
        <translation>נתוני הענן נמחקו כעת.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>Synchronize</source>
        <translation>לסנכרן</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1060"/>
        <source>Sign Out</source>
        <translation>התנתק</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1070"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>נתוני הענן נמחקו. הנתונים המקומיים שלך יוחלפו. לְהמשיך?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1169"/>
        <source>Open file</source>
        <translation>לפתוח קובץ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1196"/>
        <location filename="../mainwindow.cpp" line="1213"/>
        <source>Open directory</source>
        <translation>פתח את הספרייה</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1244"/>
        <source>Import Error</source>
        <translation>שגיאת ייבוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1244"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>לא ניתן היה לייבא מנאומים. חסרים קבצים:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1259"/>
        <source>Save unsaved data?</source>
        <translation>לשמור נתונים שלא נשמרו?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1268"/>
        <source>Import file?</source>
        <translation>לייבא קובץ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <location filename="../mainwindow.ui" line="377"/>
        <source>Export</source>
        <translation>יוצא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="275"/>
        <source>Public talks</source>
        <translation>הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="441"/>
        <source>info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="976"/>
        <source>Data exhange</source>
        <translation>חילופי נתונים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1029"/>
        <source>TheocBase Cloud</source>
        <translation>ענן TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="580"/>
        <location filename="../mainwindow.ui" line="609"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="853"/>
        <source>Print...</source>
        <translation>הדפס...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="856"/>
        <source>Print</source>
        <translation>הדפס</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="865"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>הגדרות ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="877"/>
        <source>Publishers...</source>
        <translation>מבשרים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="228"/>
        <location filename="../mainwindow.ui" line="880"/>
        <source>Publishers</source>
        <translation>מבשר</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="946"/>
        <source>Speakers...</source>
        <translation>נואמים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.ui" line="949"/>
        <source>Speakers</source>
        <translation>נואמים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="973"/>
        <source>Data exhange...</source>
        <translation>חילופי נתונים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="985"/>
        <source>TheocBase help...</source>
        <translation>עזרת TheocBase ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1000"/>
        <source>History</source>
        <translation>היסטוריה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1012"/>
        <location filename="../mainwindow.ui" line="1015"/>
        <source>Full Screen</source>
        <translation>מסך מלא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1020"/>
        <source>Startup Screen</source>
        <translation>עמוד פתיחה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1062"/>
        <source>Reminders...</source>
        <translation>תזכורות ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="513"/>
        <source>Data exchange</source>
        <translation>חילופי נתונים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="119"/>
        <source>Export Format</source>
        <translation>פורמט ייצוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="125"/>
        <source>For sending data to another user</source>
        <translation>למשלוח נתונים למשתמש אחר</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="144"/>
        <source>For easy import to Calendar programs</source>
        <translation>לייבוא קל לתוכניות לוח שנה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>Export Method</source>
        <translation>שיטת ייצוא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="189"/>
        <source>Events grouped by date</source>
        <translation>אירועים מקובצים לפי תאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>All day events</source>
        <translation>אירועי כל היום</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="218"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="282"/>
        <source>Outgoing Talks</source>
        <translation>נאומים יוצאים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Date Range</source>
        <translation>טווח תאריכים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Previous Weeks</source>
        <translation>שבועות קודמים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="321"/>
        <source>From Date</source>
        <translation>מתאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="345"/>
        <source>Thru Date</source>
        <translation>עד התאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="733"/>
        <source>File</source>
        <translation>קובץ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="739"/>
        <source>Tools</source>
        <translation>כלים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="752"/>
        <source>Help</source>
        <translation>עזרה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="841"/>
        <source>Today</source>
        <translation>היום</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="889"/>
        <source>Exit</source>
        <translation>יציאה</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="901"/>
        <source>Report bug...</source>
        <translation>דווח על באג ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="913"/>
        <source>Send feedback...</source>
        <translation>שלח משוב...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="931"/>
        <source>About TheocBase...</source>
        <translation>אודות TheocBase ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="958"/>
        <source>Check updates...</source>
        <translation>בדוק עדכונים...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="988"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1050"/>
        <source>Territories...</source>
        <translation>שטחים ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1053"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="820"/>
        <source>Back</source>
        <translation>חזור</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="832"/>
        <source>Next</source>
        <translation>הבא</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1038"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="922"/>
        <source>TheocBase website</source>
        <translation>אתר TheocBase</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <location filename="../qml/MeetingNotes.qml" line="26"/>
        <location filename="../qml/MeetingNotes.qml" line="47"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="33"/>
        <source>Outgoing Speaker</source>
        <translation>נואם יוצא</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="294"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="407"/>
        <source>Move to different week</source>
        <translation>עוברים לשבוע אחר</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="426"/>
        <source>Send to To Do List</source>
        <translation>שלח לרשימת המטלות</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="444"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="554"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="680"/>
        <source>Meeting day</source>
        <translation>יום אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="695"/>
        <source>Meeting time</source>
        <translation>שעה אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="714"/>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="658"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerValidator</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="259"/>
        <source>This talk has been discontinued. Please choose another talk.</source>
        <translation>הנאום הזו הופסקה. אנא בחר נאום אחרת.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="278"/>
        <source>This talk cannot be delivered by the speaker. Please choose another talk or speaker.</source>
        <translation>הנאום זו אינה יכולה להינשא על ידי הנועם. אנא בחר נאום או נעום אחר.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="292"/>
        <source>Unassignable. Please check the speaker&#39;s settings.</source>
        <translation>לא ניתן להקצאה. אנא בדוק את הגדרות הנעואם.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="310"/>
        <source>The assigned person is not available on this day.</source>
        <translation>האדם המוקצה אינו זמין ביום זה.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="321"/>
        <source>It is preferred to assign speakers to be away only once a month.</source>
        <translation>עדיף להקצות לנואמים להיעדר רק פעם בחודש.</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="178"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>בתאריך היעד כבר נקבעה נאום.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="179"/>
        <source>Swap Talks</source>
        <translation>החלף נאומים</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="180"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="206"/>
        <source>From %1</source>
        <translation>מ - %1</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="111"/>
        <source>Outgoing speakers</source>
        <translation>נואמים יוצאים</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="164"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>הנואם %1 נמצא בסוף השבוע</numerusform>
            <numerusform>הנואמים %1 נמצא בסוף השבוע</numerusform>
            <numerusform>הנואמים %1 נמצא בסוף השבוע</numerusform>
            <numerusform>הנואמים %1 נמצא בסוף השבוע</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="165"/>
        <source>No speakers away this weekend</source>
        <translation>בסופ&quot;ש זה אין נואמים יוצאים</translation>
    </message>
</context>
<context>
    <name>PersonDetailFilterModel</name>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="11"/>
        <source>Nonstudent meeting parts</source>
        <translation>חלקי אסיפה שאינם תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="17"/>
        <source>Other assignments</source>
        <translation>מסימות אחרות</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="23"/>
        <source>Student parts</source>
        <translation>חלקי תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="29"/>
        <source>Assistant in student parts</source>
        <translation>עוזר בחלקי תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="35"/>
        <source>Similar assignments only</source>
        <translation>משימות דומות בלבד</translation>
    </message>
</context>
<context>
    <name>PersonDetailGroupByModel</name>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="11"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="15"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="19"/>
        <source>Frequency</source>
        <translation>תדירות</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="23"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="27"/>
        <source>Meeting part</source>
        <translation>חלק אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="31"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="35"/>
        <source>Weeks idle</source>
        <translation>שבועות סרק</translation>
    </message>
</context>
<context>
    <name>PersonsModel</name>
    <message>
        <location filename="../cpersons.cpp" line="393"/>
        <source>Last name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <location filename="../cpersons.cpp" line="394"/>
        <source>First name</source>
        <translation>שם פרטי</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&lt;--</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="31"/>
        <source>Select at least one option</source>
        <translation>בחר אפשרות אחת לפחות</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="41"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="39"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="47"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="48"/>
        <source>Counselor</source>
        <translation>יועץ</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="50"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="49"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="51"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="44"/>
        <source>Public Meeting</source>
        <translation>אסיפת פומבי</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="45"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="46"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="43"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="42"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="53"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>דקה/ות</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="68"/>
        <location filename="../print/printcombination.cpp" line="69"/>
        <source>Combined Schedule</source>
        <translation>לוח זמנים משולב</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="70"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="71"/>
        <source>Outgoing Speakers</source>
        <translation>נאומים יוצים</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="88"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>שבוע החל %1</translation>
    </message>
</context>
<context>
    <name>PrintDocument</name>
    <message>
        <location filename="../print/printdocument.cpp" line="144"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>התבנית לא נמצאה</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="149"/>
        <source>Can&#39;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>לא ניתן לקרוא את הקובץ</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="235"/>
        <source>Service Talk</source>
        <translation>נאום שירות</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="247"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>מתחיל ב</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSchedule</name>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="37"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="38"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="40"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="41"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="42"/>
        <location filename="../print/printmidweekschedule.cpp" line="82"/>
        <source>Treasures from God&#39;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="43"/>
        <location filename="../print/printmidweekschedule.cpp" line="83"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>התייעל בשירות השדה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="44"/>
        <location filename="../print/printmidweekschedule.cpp" line="84"/>
        <source>Living as Christians</source>
        <translation>לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="46"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>דקה/ות</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="71"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="72"/>
        <source>Worksheet</source>
        <translation>גיליון פעילות</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="73"/>
        <location filename="../print/printmidweekschedule.cpp" line="106"/>
        <source>Opening Comments</source>
        <comment>See Workbook</comment>
        <translation>דברי פתיחה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="74"/>
        <location filename="../print/printmidweekschedule.cpp" line="107"/>
        <source>Concluding Comments</source>
        <comment>See Workbook</comment>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="75"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="76"/>
        <source>Prayer</source>
        <translation>תפילה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="77"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="78"/>
        <source>Counselor</source>
        <translation>יועץ</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="79"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="80"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="81"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="85"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="86"/>
        <source>Circuit Overseer</source>
        <translation>משגיח נפה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="87"/>
        <source>No regular meeting</source>
        <translation>אין אסיפה קבועה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="88"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="89"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="90"/>
        <source>Theme</source>
        <comment>Talk Theme description from workbook</comment>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="91"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="92"/>
        <source>Exercises</source>
        <translation>תרגילים</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="93"/>
        <source>Timing</source>
        <comment>Assignment completed in time?</comment>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="94"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="95"/>
        <source>Next study</source>
        <translation>השיעור הבא</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="96"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>רקע</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="97"/>
        <location filename="../print/printmidweekschedule.cpp" line="98"/>
        <location filename="../print/printmidweekschedule.cpp" line="99"/>
        <source>Class</source>
        <translation>כתה</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="100"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>יועצת כיתת משנית</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="101"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>כיתה משנית</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="102"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>אולם ראשי</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="103"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation>MH</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="104"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="105"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>תלמיד</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="108"/>
        <source>Today</source>
        <translation>היום</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="109"/>
        <source>Next week</source>
        <translation>שבוע הבא</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSlip</name>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="50"/>
        <source>Slip Template</source>
        <translation>תבנית לפתק</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="465"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="469"/>
        <location filename="../print/printmidweekslip.cpp" line="471"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation>שיעור %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="509"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>שיחה שנייה</translation>
    </message>
</context>
<context>
    <name>PrintMidweekWorksheet</name>
    <message>
        <location filename="../print/printmidweekworksheet.cpp" line="61"/>
        <source>Class </source>
        <translation>כתה </translation>
    </message>
</context>
<context>
    <name>PrintOutgoingSchedule</name>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="59"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="60"/>
        <location filename="../print/printoutgoingschedule.cpp" line="170"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="62"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="63"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="163"/>
        <source>Talk</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="164"/>
        <source>Outgoing Speakers</source>
        <translation>נאומים יוצים</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="165"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="166"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="167"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="168"/>
        <source>Theme Number</source>
        <translation>מספר נושא</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="169"/>
        <source>Circuit</source>
        <translation>נפה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="171"/>
        <source>Start Time</source>
        <translation>שעת התחלה</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="173"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>הערה: אח יקר, למרות תחזוקה מדוקדקת של מסדי נתונים, לפעמים זמנים או כתובות עשויים להיות מיושנים. אז אנא אמת זאת על ידי חיפוש אלה דרך JW.ORG. תודה!</translation>
    </message>
</context>
<context>
    <name>PrintTalksOfSpeakersList</name>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="37"/>
        <location filename="../print/printtalksofspeakerslist.cpp" line="38"/>
        <source>Public Talks</source>
        <translation>הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="39"/>
        <source>Contact info</source>
        <translation>פרטים ליצירת קשר</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="40"/>
        <source>Talk numbers</source>
        <comment>Public talk numbers</comment>
        <translation>מספרים נאומים</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryAssignmentRecord</name>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="35"/>
        <source>Territory Assignment Record</source>
        <translation>שיא הקצאת שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="36"/>
        <source>Territory Coverage</source>
        <translation>כיסוי שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="37"/>
        <source>Total number of territories</source>
        <translation>סה&quot;כ שטחים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="38"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt;6 חודשים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="39"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 עד 12 חודשים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="40"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>לפני 12 חודשים</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="41"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation>ממוצע לשנה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="177"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="178"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="179"/>
        <source>Address type</source>
        <translation>סוג כתובת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="180"/>
        <source>Assigned to</source>
        <translation>שהוקצה ל</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="181"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="183"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="185"/>
        <source>Date checked out</source>
        <translation>תאריך לקיחת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="186"/>
        <source>Date checked back in</source>
        <translation>תאריך החזרת</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="187"/>
        <source>Date assigned</source>
        <comment>Territory assignment</comment>
        <translation>תאריך הוקצה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="188"/>
        <source>Date completed</source>
        <comment>Territory assignment</comment>
        <translation>התאריך הושלם</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="189"/>
        <source>Date last worked</source>
        <translation>התאריך האחרון עבד</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="190"/>
        <source>Last date completed</source>
        <comment>Column to record the date on which each territory was last completed, when beginning a new S-13 sheet.</comment>
        <translation>התאריך האחרון הושלם</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="191"/>
        <source>Date requested</source>
        <translation>תאריך מבוקש</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="192"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="193"/>
        <source>Service year</source>
        <translation>שנת שירות</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="194"/>
        <source>City</source>
        <translation>עיר</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="195"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="196"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="197"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="198"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation>מ</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="208"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="209"/>
        <source>Locality</source>
        <translation>מקום</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="210"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation>מפה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="211"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="212"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>מיקוד</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="213"/>
        <source>Name of publisher</source>
        <translation>שם המבשר</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="214"/>
        <source>Remark</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="215"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="216"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="217"/>
        <source>Territory</source>
        <translation>שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="218"/>
        <source>Terr. No.</source>
        <translation>שטח מס&apos;</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="219"/>
        <source>Territory type</source>
        <translation>סוג שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="220"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation>ל</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="221"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation>זוג</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="222"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation>סכום</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryCard</name>
    <message>
        <location filename="../print/printterritorycard.cpp" line="35"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>כרטיס שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="36"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation>מפת שטח</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="37"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation>רשימת כתובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="38"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation>מפת שטח עם רשימת כתובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="39"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation>רשימת רחובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="40"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation>מפת שטח עם רשימת רחובות</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="41"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation>רשימת אסור להתקשר</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryMapCard</name>
    <message>
        <location filename="../print/printterritorymapcard.cpp" line="53"/>
        <source>Template</source>
        <translation>תבנית</translation>
    </message>
</context>
<context>
    <name>PrintWeekendSchedule</name>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="36"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="37"/>
        <source>Public Meeting</source>
        <translation>אסיפת פומבי</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="40"/>
        <location filename="../print/printweekendschedule.cpp" line="167"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>קהילה %1</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="41"/>
        <location filename="../print/printweekendschedule.cpp" line="168"/>
        <source>Congregation</source>
        <translation>קהלה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="43"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="44"/>
        <location filename="../print/printweekendschedule.cpp" line="63"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="46"/>
        <location filename="../print/printweekendschedule.cpp" line="65"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="47"/>
        <location filename="../print/printweekendschedule.cpp" line="102"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="48"/>
        <location filename="../print/printweekendschedule.cpp" line="104"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="49"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="50"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="51"/>
        <location filename="../print/printweekendschedule.cpp" line="68"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="53"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>דקה/ות</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="54"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="55"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation>מארח</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="62"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="66"/>
        <source>WT Reader</source>
        <translation>קורא WT</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="69"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="105"/>
        <source>Circuit Overseer</source>
        <translation>משגיח נפה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="108"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="120"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="123"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="205"/>
        <source>Watchtower Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="208"/>
        <source>No regular meeting</source>
        <translation>אין אסיפה קבועה</translation>
    </message>
</context>
<context>
    <name>PrintWeekendWorksheet</name>
    <message>
        <location filename="../print/printweekendworksheet.cpp" line="47"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
</context>
<context>
    <name>ProfileManager</name>
    <message>
        <location filename="../qml/ProfileManager.qml" line="19"/>
        <source>Add profile</source>
        <translation>הוסף פרופיל</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="59"/>
        <source>Switch</source>
        <translation>החלף</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="81"/>
        <source>Enter new profile name</source>
        <translation>הזן שם פרופיל חדש</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="96"/>
        <source>TheocBase needs to be restarted. Click OK to continue.</source>
        <translation>יש להפעיל מחדש את TheocBase. לחץ על אישור כדי להמשיך.</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="103"/>
        <source>Deleting a profile will remove the current database and settings files. This option cannot be undone. Would you like to delete the profile data files?</source>
        <translation>מחיקת פרופיל תסיר את מסד הנתונים וקבצי ההגדרות הנוכחיים. לא ניתן לבטל אפשרות זו. האם תרצה למחוק את קבצי נתוני הפרופיל?</translation>
    </message>
</context>
<context>
    <name>PublicTalkDetail</name>
    <message>
        <location filename="../publictalkdetail.cpp" line="32"/>
        <source>%1 to %2</source>
        <translation>%1 עד %2</translation>
    </message>
</context>
<context>
    <name>PublicTalkDetailGroupByModel</name>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="14"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="18"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="22"/>
        <source>Frequency</source>
        <translation>תדירות</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="26"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="30"/>
        <source>Number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="34"/>
        <source>Congregation</source>
        <translation>קהלה</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="32"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="632"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="368"/>
        <source>Congregation</source>
        <translation>קהלה</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="475"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="590"/>
        <source>Move to different week</source>
        <translation>עוברים לשבוע אחר</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="609"/>
        <source>Send to To Do List</source>
        <translation>שלח לרשימת המשימות</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="738"/>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="757"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="776"/>
        <source>Email</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="795"/>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="818"/>
        <source>Host</source>
        <translation>מארח</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>Yes</source>
        <translation>כן</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>&amp;Yes</source>
        <translation>&amp;כן</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>No</source>
        <translation>לא</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>&amp;No</source>
        <translation>&amp;לא</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>&amp;Cancel</source>
        <translation>&amp;לבטל</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>Save</source>
        <translation>לשמור</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>&amp;Save</source>
        <translation>&amp;לשמור</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="78"/>
        <source>Open</source>
        <translation>לפתוח</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="170"/>
        <source>Wrong username and/or password</source>
        <translation>שם משתמש ו/או סיסמה שגויים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="314"/>
        <source>Database not found!</source>
        <translation>מסד נתונים לא נמצא!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="317"/>
        <source>Choose database</source>
        <translation>בחר מסד נתונים</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="317"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>קבצי SQLite (* .sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="341"/>
        <source>Database restoring failed</source>
        <translation>שחזור מסד הנתונים נכשל</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="390"/>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Save changes?</source>
        <translation>שמור שינויים?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="310"/>
        <source>Database copied to </source>
        <translation>מסד הנתונים הועתק אל</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="40"/>
        <source>Database file not found! Searching path =</source>
        <translation>קובץ מסד הנתונים לא נמצא! נתיב חיפוש =</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="59"/>
        <source>Database Error</source>
        <translation>שגיאה במסד נתונים</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="705"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>גרסה זו של היישום (%1) ישנה יותר ממסד הנתונים (%2). יש סבירות חזקה שהודעות שגיאה יופיעו וייתכן שהשינויים לא יישמרו כהלכה. אנא הורד והתקן את הגרסה האחרונה לקבלת התוצאות הטובות ביותר.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="368"/>
        <location filename="../mainwindow.cpp" line="909"/>
        <location filename="../sql_class.cpp" line="1226"/>
        <location filename="../sql_class.cpp" line="1227"/>
        <location filename="../todo.cpp" line="373"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="369"/>
        <source>Circuit</source>
        <translation>נפה</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="749"/>
        <source>Database updated</source>
        <translation>מסד הנתונים עודכן</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="302"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (אין אסיפה)</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="365"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="284"/>
        <location filename="../cpublictalks.cpp" line="366"/>
        <source>Last</source>
        <translation>אחרון</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="368"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="369"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="82"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="370"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="83"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="371"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="84"/>
        <source>Time</source>
        <translation>זמן</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="625"/>
        <location filename="../generatexml.cpp" line="109"/>
        <source>Default language not selected!</source>
        <translation>שפת ברירת המחדל לא נבחרה!</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="744"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>שורת הכותרת של קובץ ה- CSV אינה חוקית.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="297"/>
        <source>Confirm password!</source>
        <translation>אשר סיסמה!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2370"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>משתמש עם אותה כתובת דוא&apos;ל כבר התווסף.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2584"/>
        <source>All talks have been added to this week</source>
        <translation>כל השיחות התווספו השבוע</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="57"/>
        <location filename="../importkhs.cpp" line="53"/>
        <location filename="../importwintm.cpp" line="37"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="57"/>
        <location filename="../importkhs.cpp" line="53"/>
        <location filename="../importwintm.cpp" line="37"/>
        <source>Import Complete</source>
        <translation>הייבוא הושלם</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="95"/>
        <source>Unable to read new Workbook format</source>
        <translation>לא ניתן לקרוא פורמט חדש של גיליון פעילות</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="97"/>
        <source>Database not set to handle language &#39;%1&#39;</source>
        <translation>מסד הנתונים לא מוגדר לטפל בשפה &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="99"/>
        <source>Unable to find year/month</source>
        <translation>לא ניתן למצוא שנה / חודש</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="100"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>שום דבר לא מיובא (לא מזוהים תאריכים)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="104"/>
        <location filename="../wtimport.cpp" line="33"/>
        <location filename="../wtimport.cpp" line="34"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>מיובא %1 שבועות מ-%2 עד %3</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="429"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>בחר את שמות השיחה כדי להתאים לשמות שמצאנו בגיליון פעילות</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>י&apos;ר</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>נ&apos;ע</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>פ&apos;ר</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#ק</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>Mem</source>
        <comment>history table: abbreviation for assistant/householder of &apos;Memorial Inviation&apos;</comment>
        <translation>ערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="19"/>
        <source>Vid</source>
        <comment>history table: abbreviation for &apos;Extra Video Part&apos;</comment>
        <translation>וידאו</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="22"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="23"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="26"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="27"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="32"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>#3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="34"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>CL1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>CL2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>CL3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="41"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>ק-ש&apos;מ</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="32"/>
        <source>One-time Scanning of New Slip</source>
        <translation>סריקה חד פעמית של הפתק חדש</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="726"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>כל האסיפות סוף השבוע עבור</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="48"/>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="50"/>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="52"/>
        <source>Meeting for Field Service</source>
        <translation>מפגש לשירות</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="92"/>
        <source>Treasures From God&#39;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="94"/>
        <source>Apply Yourself To The Field Ministry</source>
        <translation>התייעל בשירות השדה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="96"/>
        <source>Living As Christians</source>
        <translation>לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="160"/>
        <source>Song and Prayer</source>
        <translation>שיר ותפילה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="162"/>
        <source>Treasures From God’s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="164"/>
        <source>Spiritual Gems</source>
        <translation>פנינים רוחניות</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="166"/>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="168"/>
        <source>Sample Conversation Video</source>
        <translation>סרטוני השיחות לדוגמה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="170"/>
        <source>Initial Call</source>
        <translation>שיחה ראשונה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="172"/>
        <source>Return Visit</source>
        <translation>ביקור חוזר</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="174"/>
        <source>Bible Study</source>
        <translation>שיעור מקרא</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="178"/>
        <location filename="../assignmentInfo.h" line="180"/>
        <location filename="../assignmentInfo.h" line="182"/>
        <source>Living as Christians Talk</source>
        <translation>נאום של לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="184"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="186"/>
        <source>Service Talk</source>
        <translation>נאום שירות</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="188"/>
        <source>Memorial Invitation</source>
        <translation>הזמנה לערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="190"/>
        <source>Other Video Part</source>
        <translation>חלק וידאו אחר</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="194"/>
        <source>Public Talk</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="196"/>
        <source>Watchtower Study</source>
        <translation>שיעור מצפה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="242"/>
        <source>Meeting parts</source>
        <translation>חלקי אסיפה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="244"/>
        <source>Other assignments</source>
        <translation>מסימות אחרות</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="246"/>
        <source>Student parts</source>
        <translation>חלקי תלמיד</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="248"/>
        <source>Assistant in student parts</source>
        <translation>עוזר בחלקי תלמיד</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="250"/>
        <source>Support tasks</source>
        <translation>משימות תמיכה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="286"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="288"/>
        <source>Counselor</source>
        <translation>יועצ</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="290"/>
        <source>Study conductor</source>
        <translation>מדריך שיעור</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="292"/>
        <source>Talk</source>
        <translation>נאום</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="294"/>
        <source>Discussion</source>
        <translation>דִיון</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="296"/>
        <source>Demonstration</source>
        <translation>הדגמה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="298"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="300"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="302"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="304"/>
        <source>Hospitality</source>
        <translation>הכנסת אורחים</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="334"/>
        <source>Opening prayer</source>
        <translation>תפילת פתיחה</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="336"/>
        <source>Concluding prayer</source>
        <translation>תפילת סיום</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="338"/>
        <source>Life and Ministry Meeting Chairman</source>
        <translation>יושב־ראש אסיפת אורח חיינו ושירותנו</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="340"/>
        <source>Public Meeting Chairman</source>
        <translation>יושב ראוש אסיפה הפומבית</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="846"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>כל הנאומים היוצאות ל</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="862"/>
        <location filename="../mainwindow.cpp" line="873"/>
        <source>Outgoing Talks</source>
        <translation>נאומים יוצאים</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="978"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="367"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="80"/>
        <location filename="../schoolreminder.cpp" line="183"/>
        <location filename="../todo.cpp" line="353"/>
        <location filename="../todo.cpp" line="390"/>
        <location filename="../todo.cpp" line="401"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="373"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="86"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>יחד</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Sister</source>
        <translation>אחות</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="168"/>
        <source>Brother</source>
        <translation>אח</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Dear %1 %2</source>
        <translation>%1 %2 היקר/ה</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="173"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>אנא ראה למטה פרטים על המשימות הקרובות שלך:</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="241"/>
        <source>Regards</source>
        <translation>בברכה</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="228"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>מבשר/ת</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="117"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>צפה בלוח הזמנים של אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="119"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ערוך את לוח הזמנים לאסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="121"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>הצג את הגדרות האסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="123"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>ערוך את הגדרות האסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="125"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>שלח תזכורות לאסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="127"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>הדפס לוח זמנים לאסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="129"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>הדפס פתקי משימות לאסיפות אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="131"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>הדפס דפי עבודה של אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="133"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>צפה בלוח הזמנים של אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="135"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ערוך את לוח הזמנים לאסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="137"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>הצג את הגדרות האסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="139"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>ערוך את הגדרות האסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="141"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>צפה ברשימת ההרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="143"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>ערוך את רשימת ההרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="145"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>תזמן אירוח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="147"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>הדפס לוח זמנים לאסיפות סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="149"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>הדפיסו גיליון פעילות של אסיפות סוף השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="151"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>הדפס לוח זמנים של נואמים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="153"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>הדפס משימות של נואמים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="155"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>הדפם הכנסת אורחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="157"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>הדפיסו רשימת הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="167"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>צפו בנואמים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="169"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>ערוך נואמים פומבים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="159"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>צפו במבשרים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="161"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>ערוך מבשרים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="163"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation>צפו בנתוני התלמידים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="165"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation>ערוך את נתוני התלמידים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="171"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation>הצג זכויות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="173"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation>ערוך זכויות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="175"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation>צפו בהיסטוריית השיחות של אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="177"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation>צפה בזמינות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="179"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation>ערוך זמינות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="181"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>הצג הרשאות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="183"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>ערוך הרשאות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="185"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>צפו בשטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="187"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>ערוך שטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="189"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>הדפסת שיא שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="191"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>הדפסת כרטיס מפת שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="193"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>הדפסת מפת שטח וגיליונות כתובת</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="195"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>הצג הגדרות שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="197"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>ערוך הגדרות שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="199"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>צפו בהקצאות שטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="201"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>הצג כתובות שטח</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="203"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>צפו בהגדרות הקהילה</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="205"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>ערוך את הגדרות הקהילה</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="207"/>
        <location filename="../accesscontrol.h" line="211"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>צפו באירועים מיוחדים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <location filename="../accesscontrol.h" line="213"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>ערוך אירועים מיוחדים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>צפה ברשימת השירים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>ערוך את רשימת השירים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation>מחק נתוני ענן</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="230"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>זקן</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="232"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>יו&quot;ר LMM</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="234"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>משגיח LMM</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="236"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>מתאמ הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="238"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>משגיח שטחים</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="240"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>מזכיר</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="242"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>משגיח שירות</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="244"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>מתאם BOE</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="246"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>מנהל</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="499"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>יחידת הרוחב אינה תואמת את יחידת הגובה</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="527"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>ערך לא חוקי, מצטער.</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.cpp" line="267"/>
        <source>Unknown assignment info = %1; class %2; %3; assistant (%4)</source>
        <translation>פרטי מסימה לא ידועים = %1; מחלקה %2; %3; עוזר (%4)</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.cpp" line="280"/>
        <source>Unknown meeting part (%1).
Please check the schedule for invalid or obsolete meeting parts.</source>
        <translation>חלק tאסיפה לא ידוע (%1).
אנא בדוק את לוח הזמנים עבור חלקי אסיפה לא חוקיים או מיושנים.</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.cpp" line="284"/>
        <source>Unknown meeting part (%1).
Please check the schedule for invalid or obsolete meeting parts in the week starting %2.</source>
        <translation>חלק אסיפה לא ידוע (%1).
אנא בדוק את לוח הזמנים עבור חלקי אסיפה לא חוקיים או מיושנים בשבוע שמתחיל %2.</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="72"/>
        <location filename="../todo.cpp" line="107"/>
        <source>From %1; speaker removed</source>
        <translation>מ - %1; הנואם הוסר</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="110"/>
        <source>From %1; speaker moved to %2</source>
        <translation>מ - %1; הנואם עבר ל-%2</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="154"/>
        <location filename="../todo.cpp" line="190"/>
        <source>From %1; talk discontinued</source>
        <translation>מ - %1; הנאום הופסקה</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="366"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="380"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="151"/>
        <location filename="../settings.ui" line="154"/>
        <location filename="../settings.ui" line="1421"/>
        <source>Exceptions</source>
        <translation>חריגים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="175"/>
        <location filename="../settings.ui" line="178"/>
        <source>Public Talks</source>
        <translation>הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="671"/>
        <source>Custom templates folder</source>
        <translation>תיקיית תבניות מותאמות אישית</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="805"/>
        <source>Open database location</source>
        <translation>פתח מיקום מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="859"/>
        <source>Backup database</source>
        <translation>גיבוי מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="888"/>
        <source>Restore database</source>
        <translation>שחזר מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1272"/>
        <source>Names display order</source>
        <translation>שמות מציגים סדר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1212"/>
        <source>Color palette</source>
        <translation>פלטת צבעים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1279"/>
        <source>By last name</source>
        <translation>לפי שם המשפחה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1286"/>
        <source>By first name</source>
        <translation>לפי שם פרטי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1205"/>
        <source>Light</source>
        <translation>בהיר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1176"/>
        <source>Dark</source>
        <translation>כהה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1361"/>
        <source>Show song titles</source>
        <translation>הראה כותרות שירים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1635"/>
        <source>Weekend meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1931"/>
        <location filename="../settings.ui" line="2114"/>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;שגיאות&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2261"/>
        <source>Public talks maintenance</source>
        <translation>תחזוקת הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2353"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>קבע אירוח לנואמים פומבים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2869"/>
        <source>Streets</source>
        <translation>רחובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2883"/>
        <source>Default street type:</source>
        <translation>ברירת מחדל של סוג רחוב:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2911"/>
        <source>Street types</source>
        <translation>סוגי רחובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3161"/>
        <source>Map marker scale:</source>
        <translation>קנה מידה של סמן מפה:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3187"/>
        <source>Geo Services</source>
        <translation>שירותי גיאוגרפיה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3195"/>
        <source>Google:</source>
        <translation>Google:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3205"/>
        <location filename="../settings.ui" line="3208"/>
        <source>API Key</source>
        <translation>מפתח API</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3215"/>
        <source>Here:</source>
        <translation>פה:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3222"/>
        <source>Default:</source>
        <translation>ברירת מחדל:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3232"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3237"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3242"/>
        <source>Here</source>
        <translation>פה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3257"/>
        <location filename="../settings.ui" line="3260"/>
        <source>App Id</source>
        <translation>מזהה אפליקציה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3267"/>
        <location filename="../settings.ui" line="3270"/>
        <source>App Code</source>
        <translation>קוד אפליקציה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3343"/>
        <source>Send E-Mail Reminders</source>
        <translation>לשלוח תזכורות בדוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3718"/>
        <location filename="../settings.cpp" line="2145"/>
        <source>Profile Manager</source>
        <translation>מנהל פרופילים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3316"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>שלח תזכורות בעת סגירת TheocBase</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3326"/>
        <source>E-Mail Options</source>
        <translation>אפשרויות דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3377"/>
        <source>Sender&#39;s e-mail</source>
        <translation>הדוא&apos;ל של השולח</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3387"/>
        <source>Sender&#39;s name</source>
        <translation>שם השולח</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3451"/>
        <source>Account</source>
        <translation>חשבון</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3514"/>
        <source>Test Connection</source>
        <translation>בדיקת חיבור</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="614"/>
        <source>Printing</source>
        <translation>הדפסה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3547"/>
        <source>Users</source>
        <translation>משתמשים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3563"/>
        <source>E-mail:</source>
        <translation>דוא&apos;ל:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3616"/>
        <source>Rules</source>
        <translation>כללים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="139"/>
        <location filename="../settings.ui" line="142"/>
        <source>General</source>
        <translation>כללי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="732"/>
        <source>Backup</source>
        <translation>גיבוי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="406"/>
        <source>Current congregation</source>
        <translation>הקהילה הנוכחית</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <source>User interface</source>
        <translation>ממשק משתמש</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface language</source>
        <translation>שפת ממשק משתמש</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="936"/>
        <source>Security</source>
        <translation>הגנה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1079"/>
        <source>Enable password</source>
        <translation>אפשר סיסמה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1099"/>
        <source>Enable database encryption</source>
        <translation>אפשר הצפנת מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Confirm</source>
        <translation>לאשר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="163"/>
        <location filename="../settings.ui" line="166"/>
        <location filename="../settings.ui" line="1584"/>
        <source>Life and Ministry Meeting</source>
        <translation>אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1757"/>
        <source>Remove Duplicates</source>
        <translation>הסר כפילויות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1764"/>
        <source>Meeting Items</source>
        <translation>פריטי אסיפות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1863"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>לוח זמנים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2363"/>
        <source>Hide discontinued</source>
        <translation>הסתר הופסק</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2545"/>
        <source>Add songs</source>
        <translation>הוסף שירים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="187"/>
        <location filename="../settings.ui" line="2551"/>
        <source>Songs</source>
        <translation>שירים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2623"/>
        <location filename="../settings.ui" line="2629"/>
        <source>Add song one at a time</source>
        <translation>הוסף שיר אחד בכל פעם</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2644"/>
        <source>Song number</source>
        <translation>מספר השיר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2651"/>
        <source>Song title</source>
        <translation>כותרת השיר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2725"/>
        <source>Cities</source>
        <translation>ערים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2797"/>
        <source>Territory types</source>
        <translation>סוגי שטחים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3010"/>
        <source>Addresses</source>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3016"/>
        <source>Address types</source>
        <translation>סוגי כתובות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2875"/>
        <location filename="../settings.ui" line="3124"/>
        <source>Configuration</source>
        <translation>תצורה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="214"/>
        <location filename="../settings.cpp" line="2384"/>
        <source>Access Control</source>
        <translation>בקרת גישה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3022"/>
        <source>Type number:</source>
        <translation>מספר סוג:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2917"/>
        <location filename="../settings.ui" line="3032"/>
        <location filename="../settings.ui" line="3553"/>
        <source>Name:</source>
        <translation>שם:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2986"/>
        <location filename="../settings.ui" line="3042"/>
        <source>Color:</source>
        <translation>צבע:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2927"/>
        <location filename="../settings.ui" line="3052"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3132"/>
        <source>Default address type:</source>
        <translation>סוג כתובת ברירת מחדל:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="196"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="339"/>
        <location filename="../settings.cpp" line="1216"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="542"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="383"/>
        <source>Current Circuit Overseer</source>
        <translation>משגיח נפה נוכחי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="586"/>
        <source>Click to edit</source>
        <translation>לחץ כדי לערוך</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="979"/>
        <location filename="../settings.ui" line="3507"/>
        <source>Username</source>
        <translation>שם משתמש</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="989"/>
        <location filename="../settings.ui" line="3424"/>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="457"/>
        <location filename="../settings.ui" line="1597"/>
        <location filename="../settings.ui" line="1651"/>
        <source>Mo</source>
        <translation>ב&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="462"/>
        <location filename="../settings.ui" line="1602"/>
        <location filename="../settings.ui" line="1656"/>
        <source>Tu</source>
        <translation>ג&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="467"/>
        <location filename="../settings.ui" line="1607"/>
        <location filename="../settings.ui" line="1661"/>
        <source>We</source>
        <translation>ד&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="472"/>
        <location filename="../settings.ui" line="1612"/>
        <location filename="../settings.ui" line="1666"/>
        <source>Th</source>
        <translation>ה&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="477"/>
        <location filename="../settings.ui" line="1617"/>
        <location filename="../settings.ui" line="1671"/>
        <source>Fr</source>
        <translation>ו&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="482"/>
        <location filename="../settings.ui" line="1622"/>
        <location filename="../settings.ui" line="1676"/>
        <source>Sa</source>
        <translation>שבת</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="487"/>
        <location filename="../settings.ui" line="1627"/>
        <location filename="../settings.ui" line="1681"/>
        <source>Su</source>
        <translation>א&apos;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1534"/>
        <location filename="../settings.ui" line="1563"/>
        <location filename="../settings.ui" line="1953"/>
        <location filename="../settings.ui" line="2014"/>
        <location filename="../settings.ui" line="2064"/>
        <location filename="../settings.ui" line="2087"/>
        <location filename="../settings.ui" line="2136"/>
        <location filename="../settings.ui" line="2147"/>
        <location filename="../settings.ui" line="2184"/>
        <location filename="../settings.ui" line="2207"/>
        <location filename="../settings.ui" line="2294"/>
        <location filename="../settings.ui" line="2494"/>
        <location filename="../settings.ui" line="2572"/>
        <location filename="../settings.ui" line="2595"/>
        <location filename="../settings.ui" line="2670"/>
        <location filename="../settings.ui" line="2746"/>
        <location filename="../settings.ui" line="2772"/>
        <location filename="../settings.ui" line="2818"/>
        <location filename="../settings.ui" line="2844"/>
        <location filename="../settings.ui" line="2949"/>
        <location filename="../settings.ui" line="2975"/>
        <location filename="../settings.ui" line="3071"/>
        <location filename="../settings.ui" line="3097"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1458"/>
        <source>Circuit Overseer&#39;s visit</source>
        <translation>ביקור משגיח הנפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1463"/>
        <source>Convention</source>
        <translation>כינוס</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1468"/>
        <source>Memorial</source>
        <translation>ערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1473"/>
        <source>Zone overseer&#39;s talk</source>
        <translation>Zone overseer&apos;s talk</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>Other exception</source>
        <translation>חריג אחר</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1486"/>
        <location filename="../settings.cpp" line="625"/>
        <source>Start date</source>
        <translation>תאריך התחלה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1504"/>
        <location filename="../settings.cpp" line="626"/>
        <source>End date</source>
        <translation>תאריך סיום</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1592"/>
        <location filename="../settings.ui" line="1646"/>
        <source>No meeting</source>
        <translation>אין אסיפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1574"/>
        <source>Description</source>
        <translation>תיאור</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1797"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>ייבא את חגיליון הפעילות לאסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1725"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>ראשי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1876"/>
        <source>Year</source>
        <translation>שנה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="419"/>
        <location filename="../settings.ui" line="1913"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2039"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>לוח אסיפות אמצע השבוע לאסיפה שנבחרה לעיל</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="205"/>
        <source>Reminders</source>
        <translation>תזכורות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3670"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>הפעל קובץ המסופק על ידי דלפק העזרה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>הגדרות</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1731"/>
        <source>Number of classes</source>
        <translation>מספר קיטות</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2924"/>
        <source>Studies</source>
        <translation>שיעורים</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2377"/>
        <location filename="../settings.ui" line="2383"/>
        <source>Add subject one at a time</source>
        <translation>הוסף נושא אחד בכל פעם</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2417"/>
        <source>Public talk number</source>
        <translation>מספר הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2424"/>
        <source>Public talk subject</source>
        <translation>נושא הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2434"/>
        <location filename="../settings.ui" line="2681"/>
        <source>Language</source>
        <translation>שפה</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2466"/>
        <source>Add congregations and speakers</source>
        <translation>הוסף קהילות ונואמים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="947"/>
        <location filename="../settings.cpp" line="1218"/>
        <location filename="../settings.cpp" line="1288"/>
        <source>Number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2724"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="567"/>
        <source>Select a backup file</source>
        <translation>בחר קובץ גיבוי</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1053"/>
        <location filename="../settings.cpp" line="1373"/>
        <location filename="../settings.cpp" line="1683"/>
        <location filename="../settings.cpp" line="1751"/>
        <location filename="../settings.cpp" line="1835"/>
        <location filename="../settings.cpp" line="1931"/>
        <source>Remove selected row?</source>
        <translation>להסיר את השורה שנבחרה?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1147"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>הרצאה פומבי עם אותו מספר כבר נשמרת!
האם אתה רוצה להפסיק את הנאום הקודמת?
 
נאום מתוזמנות יועברו לרשימת המטלות.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1289"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1389"/>
        <source>Song number missing</source>
        <translation>מספר השיר חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1392"/>
        <source>Song title missing</source>
        <translation>כותרת השיר חסרה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1402"/>
        <source>Song is already saved!</source>
        <translation>השיר כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1411"/>
        <source>Song added to database</source>
        <translation>השיר נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1451"/>
        <source>City</source>
        <translation>עִיר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1487"/>
        <location filename="../settings.cpp" line="1581"/>
        <source>Type</source>
        <translation>סוג</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1647"/>
        <source>City name missing</source>
        <translation>חסר שם עיר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1656"/>
        <source>City is already saved!</source>
        <translation>עיר כבר נשמרה!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1664"/>
        <source>City added to database</source>
        <translation>עיר נוספה למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1715"/>
        <source>Territory type name missing</source>
        <translation>חסר שם סוג השטח</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1724"/>
        <source>Territory type is already saved!</source>
        <translation>סוג השטח כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1732"/>
        <source>Territory type added to database</source>
        <translation>סוג השטח נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1801"/>
        <source>Name of the street type is missing</source>
        <translation>חסר שם סוג הרחוב</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1808"/>
        <source>Street type is already saved!</source>
        <translation>סוג הרחוב כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1816"/>
        <source>Street type added to database</source>
        <translation>סוג רחוב נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2384"/>
        <source>Remove permissions for the selected user?</source>
        <translation>להסיר הרשאות עבור המשתמש שנבחר?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2658"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="948"/>
        <location filename="../settings.cpp" line="2723"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="950"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>שוחרר ב</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="951"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>הופסק ב</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.cpp" line="1102"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>הפסקת נאום זו תעביר את הנאומים שנקבעו עם ראשי פרקים זה לרשימת המטלות.

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1219"/>
        <source>Revision</source>
        <translation>תיקון</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1531"/>
        <location filename="../settings.cpp" line="1582"/>
        <location filename="../settings.cpp" line="2261"/>
        <location filename="../settings.cpp" line="2317"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1532"/>
        <location filename="../settings.cpp" line="1583"/>
        <source>Color</source>
        <translation>צבע</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1888"/>
        <source>Number of address type is missing</source>
        <translation>מספר סוג הכתובת חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1892"/>
        <source>Name of address type is missing</source>
        <translation>חסר שם סוג הכתובת</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1901"/>
        <source>Address type is already saved!</source>
        <translation>סוג הכתובת כבר נשמר!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1911"/>
        <source>Address type added to database</source>
        <translation>סוג כתובת נוסף למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2024"/>
        <source>Error sending e-mail</source>
        <translation>שגיאה בשליחת דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2114"/>
        <location filename="../settings.cpp" line="2867"/>
        <source>Select ePub file</source>
        <translation>בחר קובץ ePub</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2152"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>אזהרה: ודא שקובץ זה מקור ממקור מהימן. להמשיך?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2156"/>
        <source>Command File</source>
        <translation>קובץ פקודה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2549"/>
        <location filename="../settings.cpp" line="2598"/>
        <source>Meeting</source>
        <translation>אסיפה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2549"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>להסיר את כל האסיפה? (השתמש רק בכדי להסיר נתונים לא חוקיים ממסד הנתונים)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2590"/>
        <source>Enter source material here</source>
        <translation>הזן כאן חומר מקור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2598"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>להסיר את השיחה הזו? (השתמש רק בכדי להסיר נתונים לא חוקיים ממסד הנתונים)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2659"/>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2660"/>
        <source>Song 1</source>
        <translation>שיר 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2661"/>
        <source>Song 2</source>
        <translation>שיר 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2662"/>
        <source>Song 3</source>
        <translation>שיר 3</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2722"/>
        <source>Meeting Item</source>
        <translation>פריט אסיפה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2725"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2726"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2886"/>
        <source>Study Number</source>
        <translation>מספר שיעור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2887"/>
        <source>Study Name</source>
        <translation>שם השיעור</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="952"/>
        <location filename="../settings.cpp" line="1220"/>
        <location filename="../settings.cpp" line="1290"/>
        <location filename="../settings.cpp" line="1452"/>
        <location filename="../settings.cpp" line="1488"/>
        <location filename="../settings.cpp" line="1584"/>
        <source>Language id</source>
        <translation>מזהה שפה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1119"/>
        <source>Public talk number missing</source>
        <translation>מספר הרצאה פומבי חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1122"/>
        <source>Public talk subject missing</source>
        <translation>נושא הרצאה פומבי חסר</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1143"/>
        <source>Public talk is already saved!</source>
        <translation>הרצאה פומבי כבר נשמרה!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1177"/>
        <source>Public talk added to database</source>
        <translation>הרצאה פומבי נוספה למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1184"/>
        <location filename="../settings.cpp" line="1418"/>
        <location filename="../settings.cpp" line="1670"/>
        <location filename="../settings.cpp" line="1738"/>
        <location filename="../settings.cpp" line="1822"/>
        <location filename="../settings.cpp" line="1918"/>
        <source>Adding failed</source>
        <translation>ההוספה נכשלה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="624"/>
        <source>Exception</source>
        <translation>חריגה</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="627"/>
        <source>Meeting 1</source>
        <translation>אסיפה 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="628"/>
        <source>Meeting 2</source>
        <translation>אסיפה 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="585"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>מסד הנתונים שוחזר. התוכנית תופעל מחדש.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2925"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>להסיר את כל השיעורים? (השתמש רק בכדי להסיר נתונים לא חוקיים ממסד הנתונים)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1215"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1214"/>
        <source>Id</source>
        <translation>Id</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1217"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="549"/>
        <source>Save database</source>
        <translation>שמור מסד נתונים</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="557"/>
        <source>Database backuped</source>
        <translation>מסד הנתונים מגובה</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/controls/+fusion/Sidebar.qml" line="26"/>
        <location filename="../qml/controls/+material/Sidebar.qml" line="29"/>
        <location filename="../qml/controls/+universal/Sidebar.qml" line="29"/>
        <location filename="../qml/controls/Sidebar.qml" line="26"/>
        <source>Select an assignment on the left to edit</source>
        <translation>בחר משימה בצד שמאל כדי לערוך</translation>
    </message>
</context>
<context>
    <name>SpecialEvents</name>
    <message>
        <location filename="../specialevent.cpp" line="169"/>
        <source>No exception</source>
        <translation>אין יוצא מן הכלל</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="174"/>
        <source>Circuit overseer&#39;s visit</source>
        <translation>ביקור משגיח נפה</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="184"/>
        <source>Circuit assembly</source>
        <translation>כינוס נפתי</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="194"/>
        <source>Convention</source>
        <translation>כינוס</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="203"/>
        <source>Virtual convention</source>
        <comment>Schedule for viewing convention sessions at home</comment>
        <translation>כינוס וירטואלי</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="211"/>
        <source>Memorial</source>
        <translation>ערב הזיכרון</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="218"/>
        <source>Visit of headquarters representative</source>
        <translation>ביקור נציג מהסניף הראשי</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="225"/>
        <source>Visit of Bethel speaker</source>
        <translation>ביקור של נואם בתעם בית-אל</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="233"/>
        <source>Other exception</source>
        <translation>חריג אחר</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="14"/>
        <source>Welcome to theocbase</source>
        <translation>ברוך הבא ל- theocbase</translation>
    </message>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="38"/>
        <source>Do not show again</source>
        <translation>אל תציג שוב</translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="34"/>
        <source>Student Assignment</source>
        <translation>משימת תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="302"/>
        <source>With the same student</source>
        <translation>עם אותו תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="352"/>
        <source>Source</source>
        <translation>מקור</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="377"/>
        <source>Student</source>
        <translation>תלמיד</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="507"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="632"/>
        <source>Study point</source>
        <translation>נקודת שיעור</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="656"/>
        <source>Result</source>
        <translation>תוצאה</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="661"/>
        <source>Completed</source>
        <translation>הושלם</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="692"/>
        <source>Volunteer</source>
        <translation>מתנדב</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="841"/>
        <source>Timing</source>
        <translation>תזמון</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="866"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="55"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="118"/>
        <source>Street:</source>
        <translation>רחוב:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="128"/>
        <source>Postalcode:</source>
        <translation>מיקוד:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="150"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="165"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="183"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="201"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="77"/>
        <source>Country:</source>
        <translation>מדינה:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="87"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>מדינה:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="97"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>עיר:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="107"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>מחוז:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="138"/>
        <source>No.:</source>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="219"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>עיר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="236"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="254"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="271"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="288"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>מיקוד</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="305"/>
        <source>Latitude</source>
        <translation>קו רוחב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="322"/>
        <source>Longitude</source>
        <translation>קו אורך</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="354"/>
        <source>OK</source>
        <translation>בסדר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="359"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation>בחר את הרחובות שיתווספו לשטח:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="41"/>
        <source>search</source>
        <comment>Search in the list of new street names</comment>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="70"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="88"/>
        <source>Hide already added streets</source>
        <comment>Hide those streets that are already in the territory&apos;s street list</comment>
        <translation>הסתר רחובות שכבר נוספו</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="104"/>
        <source>OK</source>
        <translation>אשור</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="109"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="158"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="200"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="246"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="306"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>לא מוגדר [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="389"/>
        <source>Edit address</source>
        <translation>ערוך כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="441"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>הוסף כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="442"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>אנא בחר כתובת ברשימת תוצאות החיפוש.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="502"/>
        <location filename="../qml/TerritoryAddressList.qml" line="512"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>כתובת חיפוש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="503"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>לא נמצאה כתובת.</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="40"/>
        <source>ID</source>
        <translation>זיהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>זיהוי-שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="82"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="104"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>מדינה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="126"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="168"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>מחוז</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="147"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>עיר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="190"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="214"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="223"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>מיקוד</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="245"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>גאומטריה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="266"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="276"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>סוג</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="286"/>
        <source>Request date</source>
        <translation>תאריך מבוקש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="311"/>
        <source>Add new address</source>
        <translation>הוסף כתובת חדשה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="322"/>
        <source>Edit selected address</source>
        <translation>ערוך את הכתובת שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="333"/>
        <source>Remove selected address</source>
        <translation>הסר את הכתובת שנבחרה</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="81"/>
        <source>Filename:</source>
        <translation>שם קובץ:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="130"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>התאם שדות KML</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="59"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>גבולות שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="66"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="157"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>תיאור:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="175"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>חפש לפי &quot;תיאור&quot; אם השטח לא נמצא לפי &quot;שם&quot;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="189"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>שטח מס&apos;:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="193"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>מקום</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="197"/>
        <source>Remark</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="214"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>התאמת שדות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="227"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>כתובת:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>שם:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="259"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>סוג כתובת:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="281"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>פלט שם קובץ לכתובות שנכשלו:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="351"/>
        <source>Import</source>
        <translation>יבוא</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="360"/>
        <source>Close</source>
        <translation>סגור</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="128"/>
        <source>Group by:</source>
        <translation>קבץ לפי:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="137"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>עִיר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="139"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>סוג</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="140"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>עבד</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="241"/>
        <source>Add new territory</source>
        <translation>הוסף שטח חדש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="257"/>
        <source>Remove selected territory</source>
        <translation>הסר שטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="348"/>
        <source>Remark</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="359"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>סוג:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="386"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>עיר:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="452"/>
        <source>Assignments</source>
        <translation>משימות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="644"/>
        <source>Publisher</source>
        <translation>מבשר/ת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="781"/>
        <source>Streets</source>
        <translation>רחובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="820"/>
        <source>Addresses</source>
        <translation>כתובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="290"/>
        <source>No.:</source>
        <translation>מס&apos;:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="301"/>
        <source>Number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="313"/>
        <source>Locality:</source>
        <translation>מקום:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="323"/>
        <source>Locality</source>
        <translation>מקום</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="422"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>מפה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="618"/>
        <source>ID</source>
        <translation>זיהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="631"/>
        <source>Publisher-ID</source>
        <translation>מבשר-זיהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="700"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>לקיחת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="707"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>החזרת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="730"/>
        <source>Add new assignment</source>
        <translation>הוסף משימה חדשה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="757"/>
        <source>Remove selected assignment</source>
        <translation>הסר את המשימה שנבחרה</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="398"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>כתובת חיפוש</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="851"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation>
            <numerusform>הגבול החדש חופף את %n שטח:</numerusform>
            <numerusform>הגבול החדש חופף את %n השטחים:</numerusform>
            <numerusform>הגבול החדש חופף את %n השטחים:</numerusform>
            <numerusform>הגבול החדש חופף את %n השטחים:</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="853"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#39;No&#39; if overlapping areas should remain in their territories and to add only the part, that doesn&#39;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation>האם ברצונך להקצות שטחים חופפים לשטח הנוכחי?
בחר &apos;לא&apos; אם אזורים חופפים צריכים להישאר בשטחים שלהם ולהוסיף רק את החלק, שאינו חופף לשטחים אחרים.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="887"/>
        <source>Join to the selected territory</source>
        <comment>Join two territories into one</comment>
        <translation>הצטרף לשטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="917"/>
        <source>Add address to selected territory</source>
        <translation>הוסף כתובת לשטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="926"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>הקצה לשטח שנבחרה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="935"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>הסר כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1026"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>%1 מכתובת/ות %2 מיובאות.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1041"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>יבוא גבולות שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1051"/>
        <location filename="../qml/TerritoryMap.qml" line="1062"/>
        <location filename="../qml/TerritoryMap.qml" line="1077"/>
        <location filename="../qml/TerritoryMap.qml" line="1086"/>
        <location filename="../qml/TerritoryMap.qml" line="1119"/>
        <location filename="../qml/TerritoryMap.qml" line="1128"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>ייבא נתוני שטחים</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1052"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>%n שטח מיובא או מעודכן.</numerusform>
            <numerusform>%n שטחים מיובאים או מעודכנים.</numerusform>
            <numerusform>%n שטחים מיובאים או מעודכנים.</numerusform>
            <numerusform>%n שטחים מיובאים או מעודכנים.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1078"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>אנא בחר את שדות הכתובת והשם.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1087"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>השדות שנבחרו צריכים להיות שונים.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1095"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>ייבא כתובות שטחים</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1096"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>הכתובות יתווספו לשטח הנוכחי. אנא בחר שטח תחילה.</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1120"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>%n כתובת מיובא.</numerusform>
            <numerusform>%n כתובות מיובאות.</numerusform>
            <numerusform>%n כתובות מיובאות.</numerusform>
            <numerusform>%n כתובות מיובאות.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1130"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>לא נבחרה שטח חוקית.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1132"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>לא ניתן היה לקרוא את קובץ הייבוא.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1164"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1166"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1195"/>
        <location filename="../qml/TerritoryMap.qml" line="1203"/>
        <source>Open file</source>
        <translation>קובץ פתוח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1196"/>
        <location filename="../qml/TerritoryMap.qml" line="1197"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>קבצי KML (* .kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1196"/>
        <location filename="../qml/TerritoryMap.qml" line="1204"/>
        <location filename="../qml/TerritoryMap.qml" line="1212"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>כל הקבצים (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1204"/>
        <location filename="../qml/TerritoryMap.qml" line="1205"/>
        <location filename="../qml/TerritoryMap.qml" line="1212"/>
        <location filename="../qml/TerritoryMap.qml" line="1213"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>קבצי CSV (* .csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1204"/>
        <location filename="../qml/TerritoryMap.qml" line="1212"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>קבצי טקסט (* .txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1211"/>
        <source>Save file</source>
        <translation>שמור את הקובץ</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="66"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>לחפש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="153"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>ייבא נתונים</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="197"/>
        <source>Show/hide streets</source>
        <comment>Show/hide streets of territories</comment>
        <translation>הראה / הסתיר רחובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="209"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>החלף מצב עריכה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="223"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation>צור גבול</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="235"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>הסר את הגבול</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="247"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation>פיצול שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="164"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>זום מלא</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="174"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>הצג / הסתר שטחים</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="185"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>הצג / הסתר סמנים</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation>שם רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="210"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation>לא מוגדר [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="278"/>
        <location filename="../qml/TerritoryStreetList.qml" line="289"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation>הוסף רחובות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="279"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation>לא נמצאו רחובות.</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="40"/>
        <source>ID</source>
        <translation>זהוי</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>זהוי-שטח</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation>שם רחוב</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation>ממספר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation>למספר</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation>כמות</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation>זוג</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation>גאומטריה</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="165"/>
        <source>Add new street</source>
        <translation>הוסף רחוב חדש</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="176"/>
        <source>Remove selected street</source>
        <translation>הסר את הרחוב שנבחר</translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt;6 חודשים</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 עד 12 חודשים</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>לפני 12 חודשים</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>לא עבד</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>לא הוקצה</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>שטח</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>שטחים</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="128"/>
        <source>To Do List</source>
        <translation>רשימת משימות</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="166"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>לא ניתן לתזמן פריט זה עד לתיקון שדות אלה:%1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="192"/>
        <source>Count=%1</source>
        <translation>ספירה=%1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="214"/>
        <source>Settings</source>
        <translation>הגדרות</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="458"/>
        <source>Edit To Do Item</source>
        <translation>ערוך פריט משימה</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="479"/>
        <source>Meeting day</source>
        <translation>יום אסיפה</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="523"/>
        <source>Congregation</source>
        <translation>קהלה</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="630"/>
        <location filename="../qml/TodoPanel.qml" line="857"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="747"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
</context>
<context>
    <name>TodoValidator</name>
    <message>
        <location filename="../todomodel.cpp" line="385"/>
        <source>This item is overdue. Please remove it or choose another date.</source>
        <translation>פריט זה הגיע באיחור. אנא הסר אותו או בחר תאריך אחר.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="392"/>
        <location filename="../todomodel.cpp" line="400"/>
        <source>This date is already scheduled. Please choose another date.</source>
        <translation>התאריך הזה כבר נקבע. אנא בחר תאריך אחר.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="414"/>
        <source>This talk has been discontinued. Please choose another talk.</source>
        <translation>הנאום הזו הופסקה. אנא בחר נאום אחרת.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="428"/>
        <source>The speaker is no member of this congregation. Please choose another speaker or congregation.</source>
        <translation>הנאום אינו חבר בקהילה זו. אנא בחר נאום או קהילה אחרת.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="441"/>
        <source>This talk cannot be delivered by the speaker. Please choose another talk or speaker.</source>
        <translation>הנאום זו אינה יכולה להינשא על ידי הנועם. אנא בחר נאום או נעום אחר.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="462"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>האדם המוקצה אינו חבר בקהילה.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="469"/>
        <source>Unassignable. Please check the speaker&#39;s settings.</source>
        <translation>לא ניתן להקצאה. אנא בדוק את הגדרות הנעואם.</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="13"/>
        <source>Text</source>
        <translation>טקסט</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="30"/>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="190"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="27"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>נאום השירות</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="57"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="82"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="111"/>
        <location filename="../qml/WEMeetingModule.qml" line="131"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="127"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="138"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="161"/>
        <location filename="../qml/WEMeetingModule.qml" line="374"/>
        <source>Song &amp; Prayer</source>
        <translation>שירה ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="162"/>
        <location filename="../qml/WEMeetingModule.qml" line="375"/>
        <source>Song %1 &amp; Prayer</source>
        <translation>שיר% 1ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="209"/>
        <source>PUBLIC TALK</source>
        <translation>הרצאה פומבי</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="271"/>
        <source>WATCHTOWER STUDY</source>
        <translation>שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="277"/>
        <source>Song %1</source>
        <translation>שיר %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="302"/>
        <source>Import WT...</source>
        <translation>ייבא WT ...</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="330"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="337"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>WEMeetingSongAndPrayerPanel</name>
    <message>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="13"/>
        <source>Song and Prayer</source>
        <translation>שיר ותפילה</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="14"/>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="333"/>
        <source>Song</source>
        <translation>שיר</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="200"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
</context>
<context>
    <name>WEMeetingValidator</name>
    <message>
        <location filename="../cpublictalks.cpp" line="1061"/>
        <source>This talk has been discontinued. Please choose another talk.</source>
        <translation>הנאום הזו הופסקה. אנא בחר נאום אחרת.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1071"/>
        <source>The speaker is no member of this congregation. Please choose another speaker or congregation.</source>
        <translation>הנאום אינו חבר בקהילה זו. אנא בחר נאום או קהילה אחרת.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1085"/>
        <source>This talk cannot be delivered by the speaker. Please choose another talk or speaker.</source>
        <translation>הנאום זו אינה יכולה להינשא על ידי הנועם. אנא בחר נאום או נעום אחר.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1102"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>האדם המוקצה אינו חבר בקהילה.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1108"/>
        <source>Unassignable. Please check the publisher&#39;s settings.</source>
        <translation>לא ניתן להקצאה. בדוק את ההגדרות של המבשר.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1127"/>
        <source>The assigned person is not available on this day.</source>
        <translation>האדם המוקצה אינו זמין ביום זה.</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="30"/>
        <source>Watchtower Study</source>
        <translation>שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="279"/>
        <source>Watchtower Issue</source>
        <translation>הוצאה המצפה</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="305"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>מאמר</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="332"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="362"/>
        <source>Conductor</source>
        <translation>מדריך</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="488"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="121"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(חסר תיעוד)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>גרסה חדשה זמינה</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>הורד</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>גרסה חדשה...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>אין עדכון חדש זמין</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="107"/>
        <source>File reading failed</source>
        <translation>קריאת הקבצים נכשלה</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="338"/>
        <source>XML file generated in the wrong version.</source>
        <translation>קובץ XML שנוצר בגרסה הלא נכונה.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="406"/>
        <source>Persons - added </source>
        <translation>אנשים - נוסף</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="463"/>
        <source>Persons - updated </source>
        <translation>אנשים - עודכן</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="641"/>
        <source>Public talks - theme added </source>
        <translation>הרצאות, פומביים - הנושא נוסף </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="699"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>הרצאה פומבי ושיור המצפה - לוח הזמנים עודכן </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="705"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>הרצאה פומבי ושיור המצפה - לוח זמנים נוסף </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="501"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>אל תתקשר</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1885"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>שמור כתובות שנכשלו</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1886"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>הקובץ נמצא במצב קריאה בלבד</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>לא מוכן</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>אשור</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>בסדר אבל JSON לא זמין</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>מאשר</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>ההרשאה נכשלה</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>חסר מזהה לקוח</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>חסר סוד לקוח</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>צריך קוד אישור</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>צריך רענון אסימון</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="193"/>
        <source>Number of weeks after selected date</source>
        <translation>מספר שבועות לאחר התאריך שנבחר</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="240"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>מספר שבועות לאפור לאחר משימה</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="214"/>
        <source>weeks</source>
        <translation>שבועות</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="106"/>
        <source>Timeline</source>
        <translation>ציר זמן</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="177"/>
        <source>Number of weeks before selected date</source>
        <translation>מספר שבועות לפני התאריך שנבחר</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="318"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>מבשר</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="516"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="578"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>נ&apos;פ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="584"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>י&apos;ר</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="593"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>ק-מ&apos;פ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="803"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="804"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="805"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="806"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="807"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="808"/>
        <source>Reader</source>
        <translation>קרין</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>Next &gt;</source>
        <translation>הבא&gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>&lt; Back</source>
        <translation>&lt;חזרה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Cancel</source>
        <translation>לבטל</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="65"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ייבוא לוח הזמנים של בתי הספר התיאוקרטיים. העתק את לוח הזמנים המלא מ- WTLibrary והדבק למטה (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="66"/>
        <source>Check schedule</source>
        <translation>בדוק את לוח הזמנים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="69"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>יבוא שיעורים. העתק מחקרים מ- WTLibrary והדבק למטה (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="70"/>
        <source>Check studies</source>
        <translation>לבדוק שיעורים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="73"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ייבוא הגדרות. העתק הגדרות מ- WTLibrary והדבק למטה (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="74"/>
        <source>Check settings</source>
        <translation>בדוק הגדרות</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="78"/>
        <source>Check subjects</source>
        <translation>בדוק נושאים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="81"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>הוסף נואמים וקהילות. העתק את כל הנתונים ללוח והדבק למטה (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="82"/>
        <source>Check data</source>
        <translation>בדוק נתונים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="86"/>
        <source>Check songs</source>
        <translation>בדוק שירים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="98"/>
        <source>No schedule to import.</source>
        <translation>אין לוח זמנים לייבוא.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="144"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="145"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>אנא הוסף תאריך התחלה YYYY-MM-DD (למשל, 2011-01-03)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="152"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>התאריך אינו היום הראשון בשבוע (שני)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="206"/>
        <source>Import songs</source>
        <translation>ייבא שירים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="248"/>
        <source>Only brothers</source>
        <translation>רק אחים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="477"/>
        <source>Subject</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="521"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="524"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="527"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="530"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="533"/>
        <source>Public talks</source>
        <translation>הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="536"/>
        <source>Language</source>
        <translation>שפה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="552"/>
        <location filename="../importwizard.cpp" line="553"/>
        <source>First name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="552"/>
        <location filename="../importwizard.cpp" line="553"/>
        <source>Last name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="181"/>
        <source>Import subjects</source>
        <translation>יבוא נושאים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="182"/>
        <location filename="../importwizard.cpp" line="207"/>
        <source>Choose language</source>
        <translation>בחר שפה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="37"/>
        <source>Save to database</source>
        <translation>שמור למסד הנתונים</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="77"/>
        <source>Add public talk&#39;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>הוסף את נושאי ההרצאה פומבי העתק נושאים והדבק למטה (Ctrl + V / cmd + V).
המספר צריך להיות בעמודה הראשונה והנושא בשנייה.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="85"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>הוסף שירים. העתק את כל הנתונים ללוח והדבק למטה (Ctrl + V / cmd + V).
המספר צריך להיות בעמודה הראשונה והנושא בשנייה.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="237"/>
        <source>date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="240"/>
        <source>number</source>
        <translation>מספר</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="243"/>
        <source>subject</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="246"/>
        <source>material</source>
        <translation>חומר</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="357"/>
        <source>setting</source>
        <translation>הגדרה</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="418"/>
        <source>study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="604"/>
        <source>Title</source>
        <translation>כותרת</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="678"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>הרצאה פומבית עם אותו מספר כבר נשמרת!
האם אתה רוצה להפסיק את הנאום הקודמת?
 
נאום מתוזמנות יועברו לרשימת המטלות.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="681"/>
        <source>Previous talk: </source>
        <translation>נאום קודם: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="681"/>
        <source>New talk: </source>
        <translation>נאום חדש: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="796"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>נושא של הרצאה פומבי לא נמצאו. הוסף נושא ונסה שוב!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="819"/>
        <source> rows added</source>
        <translation> הוסיפו שורות</translation>
    </message>
</context>
<context>
    <name>lmmWorksheetRegEx</name>
    <message>
        <location filename="../lmmworksheetregex.ui" line="203"/>
        <source>Language Options</source>
        <translation>אפשרויות שפה</translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="13"/>
        <source>Talk Name in the Workbook</source>
        <translation>שם הנאום בגיליון פעילות</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="15"/>
        <source>Meeting Item</source>
        <translation>פריט אסיפה</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="41"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>לא ידוע</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>עורך סוג השיחה</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>שם משתמש</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>סיסמה</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="1195"/>
        <source>Sister</source>
        <translation>אחות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1188"/>
        <source>Brother</source>
        <translation>אח</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1204"/>
        <source>Servant</source>
        <translation>משרת</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>מבשרים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="237"/>
        <source>General</source>
        <translation>כללי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="527"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="627"/>
        <source>Public talks</source>
        <translation>הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="361"/>
        <location filename="../personsui.ui" line="620"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="604"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="680"/>
        <source>Watchtower reader</source>
        <translation>קרין המצפה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1211"/>
        <source>Prayer</source>
        <translation>תפלה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="567"/>
        <source>Cong. Bible Study reader</source>
        <translation>קרין שיעור המקרה הקהילתי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="709"/>
        <source>Meeting for field ministry</source>
        <translation>מפגשי השירות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="986"/>
        <location filename="../personsui.cpp" line="583"/>
        <source>First name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1011"/>
        <location filename="../personsui.cpp" line="582"/>
        <source>Last name</source>
        <translation>שם משפחה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1120"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1173"/>
        <source>E-mail</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="765"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="890"/>
        <location filename="../personsui.ui" line="913"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="646"/>
        <source>Watchtower Study Conductor</source>
        <translation>מדריך שיעור המצפה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1218"/>
        <source>Family Head</source>
        <translation>ראש משפחה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1225"/>
        <source>Family member linked to</source>
        <translation>בן משפחה מקושר ל</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="330"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="486"/>
        <source>Only Auxiliary Classes</source>
        <translation>רק כיתות משניות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="459"/>
        <source>Only Main Class</source>
        <translation>כיתה ראשונה בלבד</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1241"/>
        <source>Active</source>
        <translation>פעיל</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="380"/>
        <source>Treasures From God&#39;s Word</source>
        <translation>אוצרות מדבר־אלוהים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="431"/>
        <source>Initial Call</source>
        <translation>שיחה ראשונה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="394"/>
        <source>Bible Reading</source>
        <translation>קריאת המקרא</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="493"/>
        <source>All Classes</source>
        <translation>כל הכיתות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="513"/>
        <source>Bible Study</source>
        <translation>שיעור מקרא</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="560"/>
        <source>Congregation Bible Study</source>
        <translation>שיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="553"/>
        <source>Living as Christians Talks</source>
        <translation>נאומים של לחיות כמשיחיים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="506"/>
        <source>Return Visit</source>
        <translation>ביקור חוזר</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="934"/>
        <source>Personal Info</source>
        <translation>מידע אישי</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1248"/>
        <source>Host for Public Speakers</source>
        <translation>מארח לנואמים פומבים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1067"/>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="217"/>
        <source>Details</source>
        <translation>פרטים</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="387"/>
        <source>Spiritual Gems</source>
        <translation>פנינים רוחניות</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="520"/>
        <source>Talk</source>
        <translation>נאום</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="537"/>
        <source>Discussion with Video</source>
        <translation>דיון עם וידאו</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="721"/>
        <source>History</source>
        <translation>היסטוריה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="755"/>
        <source>date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="760"/>
        <source>no</source>
        <translation>מס&apos;</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="770"/>
        <source>Note</source>
        <translation>הערה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="775"/>
        <source>Time</source>
        <translation>זמן</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="780"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>יחד</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="789"/>
        <source>Unavailable</source>
        <translation>אינו זמין</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="823"/>
        <source>Start</source>
        <translation>התחלה</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="828"/>
        <source>End</source>
        <translation>סוֹף</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="92"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="137"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="412"/>
        <source>A person with the same name already exists: &#39;%1&#39;. Do you want to change the name?</source>
        <translation>אדם עם אותו שם כבר קיים: &apos;%1&apos;. האם אתה רוצה לשנות את השם?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="633"/>
        <source>Remove student?</source>
        <translation>להסיר את התלמיד?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="610"/>
        <source>Remove student</source>
        <translation>הסר את התלמיד</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="435"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 מתוכנן להרצאות פומביים! הנאומים האלה כן
תועבר לרשימת המטלות אם תסיר אותו כנואם.
להסיר אותו כנואם?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="624"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 מתוכנן להרצאה פומבי! השיחות האלה כן
תועבר לרשימת המטלות אם תסיר את התלמיד.</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1009"/>
        <source>Copy to the clipboard</source>
        <translation>העתק ללוח</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1012"/>
        <source>Copy</source>
        <translation>עותק</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="228"/>
        <location filename="../printui.ui" line="312"/>
        <source>Schedule</source>
        <translation>לוח זמנים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>הדפס</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="376"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>לוח זמנים רשימת שיחות והכבסת אורחים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="683"/>
        <source>Midweek Meeting Title</source>
        <translation>כותרת אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="603"/>
        <source>Weekend Meeting Title</source>
        <translation>כותרת אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="693"/>
        <source>Show Section Titles</source>
        <translation>הצג כותרות קטע</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="670"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>דברי פתיחה‎</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="251"/>
        <source>Assignment Slips for Assistants</source>
        <translation>פתקים משימות לעוזרים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="347"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>לוחות זמנים של נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="363"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>משימות נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="389"/>
        <source>Talks of Speakers</source>
        <translation>נומים של נואמים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="507"/>
        <source>Territory Map Card</source>
        <translation>כרטיס מפת השטח</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="517"/>
        <source>Map and Addresses Sheets</source>
        <translation>מפה וגיליונות כתובות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="610"/>
        <source>Concluding Comments Title</source>
        <comment>See S-140</comment>
        <translation>כותרת לדברי סיכום</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="724"/>
        <source>Show public talk revision date</source>
        <translation>הצג תאריך עדכון להרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="637"/>
        <source>Show duration</source>
        <translation>משך ההצגה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="617"/>
        <source>Show time</source>
        <translation>הצג זמן</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="710"/>
        <source>Show Workbook Issue no.</source>
        <translation>הצג מספר הגיליון פעילות.</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="703"/>
        <source>Show Watchtower Issue no.</source>
        <translation>הצג גיליון מצפה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="717"/>
        <source>Own congregation only</source>
        <translation>קהילה שלך בלבד</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="868"/>
        <source>Territory number(s)</source>
        <translation>מספר/ים שטח/ים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="875"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>תחום פסיקים; לחץ על Enter כדי לרענן</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="881"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territory Record</source>
        <translation>שיא שטח</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="770"/>
        <source>Template</source>
        <translation>תבנית</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="796"/>
        <source>Paper Size</source>
        <translation>גודל נייר</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="858"/>
        <source>Print From Date</source>
        <translation>הדפסה מתאריך</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="816"/>
        <source>Print Thru Date</source>
        <translation>הדפס עד תאריך</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="939"/>
        <source>Printing</source>
        <translation>הדפסה</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="942"/>
        <location filename="../printui.ui" line="977"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="241"/>
        <source>Assignment Slips</source>
        <translation>טפסים של משימות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="630"/>
        <source>Share in Dropbox</source>
        <translation>שתף בדרופבוקס</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="271"/>
        <location filename="../printui.ui" line="331"/>
        <source>Worksheets</source>
        <translation>גיליוני פעילות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="530"/>
        <source>Meetings for field ministry</source>
        <translation>מפגשי השירות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="402"/>
        <source>Combination</source>
        <translation>שלוב</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="559"/>
        <source>Additional Options</source>
        <translation>אפשרויות נוספות</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="584"/>
        <source>Show Song Titles</source>
        <translation>הצג כותרות שירים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="647"/>
        <source>Show Counsel Text</source>
        <translation>הצג טקסט של יועץ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="453"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>לוח נואמים יוצאים</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="261"/>
        <source>Print assigned only</source>
        <translation>הדפסה הוקצתה בלבד</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="406"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>הועתק ללוח. הדבק לתכנית לעיבוד תמלילים (Ctrl + V / Cmd + V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="453"/>
        <location filename="../printui.cpp" line="545"/>
        <location filename="../printui.cpp" line="1181"/>
        <source>file created</source>
        <translation>הקובץ נוצר</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="779"/>
        <source>Concluding Comments</source>
        <translation>דברי סיכום</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1016"/>
        <source>Invalid entry, sorry.</source>
        <translation>ערך לא חוקי, מצטער.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="417"/>
        <location filename="../printui.cpp" line="465"/>
        <location filename="../printui.cpp" line="525"/>
        <source>Save file</source>
        <translation>שמור את הקובץ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="466"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="389"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>המותאם אישית...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="621"/>
        <source>Converting %1 to JPG file</source>
        <translation>המרת %1 לקובץ JPG</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="200"/>
        <location filename="../printui.ui" line="440"/>
        <location filename="../printui.cpp" line="770"/>
        <source>Midweek Meeting</source>
        <translation>אסיפת אמצע השבוע</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1009"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>גודל נייר מותאם אישית חדש</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1009"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>פורמט: רוחב x גובה. רוחב וגובה יכולים להיות באינץ או מ&quot;מ. דוגמה 210 מ&quot;מ x 297 מ&quot;מ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="284"/>
        <location filename="../printui.ui" line="427"/>
        <location filename="../printui.cpp" line="771"/>
        <source>Weekend Meeting</source>
        <translation>אסיפת סוף השבוע</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="778"/>
        <source>Opening Comments</source>
        <translation>דברי פתיחה</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="190"/>
        <source>From %1</source>
        <translation>מ-%1</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="219"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>בתאריך היעד כבר נקבעה נאום.</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="220"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>החלף נאומים</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="221"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>לבטל</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>תזכורות</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="59"/>
        <source>Date range</source>
        <translation>טווח תאריכים</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="132"/>
        <source>Send selected reminders</source>
        <translation>שלח תזכורות נבחרות</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="145"/>
        <source>From</source>
        <translation>מ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="158"/>
        <source>To</source>
        <translation>ל</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="205"/>
        <source>Details</source>
        <translation>פרטים</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="249"/>
        <source>Date</source>
        <translation>תאריך</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="254"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="264"/>
        <source>Subject</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="269"/>
        <source>Message</source>
        <translation>הודעה</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="259"/>
        <source>E-Mail</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="93"/>
        <source>Email sending...</source>
        <translation>שליחת דוא&quot;ל ...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="94"/>
        <source>Cancel</source>
        <translation>ךבטל</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="120"/>
        <source>Error sending e-mail</source>
        <translation>שגיאה בשליחת דוא&apos;ל</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>יושב ראש</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>יועץ-כתה ב&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>יועץ-כתה ד&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>תפילה א&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>תפילה ב&apos;</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>ביטול - משימת אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>משימת אסיפת אורח חיינו ושירותנו המשיחיים</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Name</source>
        <translation>שם</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="184"/>
        <source>Assignment</source>
        <translation>משימה</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="187"/>
        <source>Theme</source>
        <translation>נושא</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="189"/>
        <source>Source Material</source>
        <translation>חומר מקור</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="206"/>
        <source>Assistant</source>
        <translation>עוזר</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="209"/>
        <source>Study</source>
        <translation>שיעור</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="215"/>
        <source>Main hall</source>
        <translation>אולם ראשי</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="218"/>
        <source>Auxiliary classroom 1</source>
        <translation>כיתה משנה  א</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="221"/>
        <source>Auxiliary classroom 2</source>
        <translation>כיתה משנה  ב</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="224"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>מקום הגשת המשימה</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="229"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>קרין לשיעור המקרא הקהילתי</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>עוזר/ת ל-%1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="236"/>
        <source>Cancellation</source>
        <translation>ביטול</translation>
    </message>
</context>
<context>
    <name>sidePanelScripts</name>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="36"/>
        <source>No recent assignment</source>
        <translation>אין מסימוה אחרונה</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="43"/>
        <source>No weeks idle</source>
        <translation>אין שבועות סרק</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/sidePanelScripts.js" line="45"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>שבוע</numerusform>
            <numerusform>שבועות</numerusform>
            <numerusform>שבועות</numerusform>
            <numerusform>שבועות</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="46"/>
        <source>%1 or more weeks</source>
        <translation>%1 שבועות או יותר</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="61"/>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="62"/>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="63"/>
        <source>MH</source>
        <translation>MH</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>קהילות ונאומים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="365"/>
        <source>Congregation...</source>
        <translation>קהילה...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="336"/>
        <source>Speaker...</source>
        <translation>נואם...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1663"/>
        <source>Toggle Talks Editable</source>
        <translation>החלף את הנאומים הניתנים לעריכה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1611"/>
        <source>Add Multiple Talks</source>
        <translation>הוסף נאומים מרובים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="71"/>
        <location filename="../speakersui.ui" line="112"/>
        <location filename="../speakersui.ui" line="156"/>
        <location filename="../speakersui.ui" line="222"/>
        <location filename="../speakersui.ui" line="1614"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="459"/>
        <source>Select a Congregation or Speaker</source>
        <translation>בחר קהילה או נואם</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="625"/>
        <source>Info</source>
        <translation>מידע</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="681"/>
        <source>Address</source>
        <translation>כתובת</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="740"/>
        <location filename="../speakersui.cpp" line="181"/>
        <source>Circuit</source>
        <translation>נפה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="575"/>
        <location filename="../speakersui.ui" line="1428"/>
        <location filename="../speakersui.cpp" line="189"/>
        <source>Congregation</source>
        <translation>קהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="68"/>
        <source>Speakers</source>
        <translation>נואמים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="109"/>
        <source>Group by congregation</source>
        <translation>קבוצה לפי קהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="153"/>
        <source>Group by circuit</source>
        <translation>קבץ לפי נפה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="197"/>
        <source>Filter</source>
        <translation>לסנן</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="219"/>
        <source>Configure Filter</source>
        <translation>הגדר את המסנן</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="500"/>
        <source>Congregation Details</source>
        <translation>פרטי הקהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="758"/>
        <source>Meeting Times</source>
        <translation>זמני אסיפות</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="850"/>
        <location filename="../speakersui.ui" line="1020"/>
        <source>Mo</source>
        <translation>ב&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="855"/>
        <location filename="../speakersui.ui" line="1025"/>
        <source>Tu</source>
        <translation>ג&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="860"/>
        <location filename="../speakersui.ui" line="1030"/>
        <source>We</source>
        <translation>ד&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.ui" line="1035"/>
        <source>Th</source>
        <translation>ה&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="870"/>
        <location filename="../speakersui.ui" line="1040"/>
        <source>Fr</source>
        <translation>ו&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="875"/>
        <location filename="../speakersui.ui" line="1045"/>
        <source>Sa</source>
        <translation>שבת</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="880"/>
        <location filename="../speakersui.ui" line="1050"/>
        <source>Su</source>
        <translation>א&apos;</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1210"/>
        <source>Personal Info</source>
        <translation>מידע אישי</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1518"/>
        <location filename="../speakersui.cpp" line="843"/>
        <source>First Name</source>
        <translation>שם פרטי</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1559"/>
        <source>Mobile</source>
        <translation>נייד</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1341"/>
        <location filename="../speakersui.cpp" line="844"/>
        <source>Last Name</source>
        <translation>שם משפחה</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup_2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1316"/>
        <source>Phone</source>
        <translation>טלפון</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1631"/>
        <source>Public Talks</source>
        <translation>הרצאות פומביים</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1400"/>
        <source>E-mail</source>
        <translation>דוא&apos;ל</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1254"/>
        <source>Notes</source>
        <translation>הערות</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="197"/>
        <source>Speaker</source>
        <translation>נואם</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="371"/>
        <location filename="../speakersui.cpp" line="551"/>
        <source>Undefined</source>
        <translation>לא מוגדר</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="454"/>
        <location filename="../speakersui.cpp" line="459"/>
        <source>%1 Meeting Day/Time</source>
        <translation>יום / זמן האסיפה %1</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="584"/>
        <source>A speaker with the same name already exists: &#39;%1&#39;. Do you want to change the name?</source>
        <translation>נואם בשם זהה כבר קיים: &apos;%1&apos;. האם אתה רוצה לשנות את השם?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="716"/>
        <source>The congregation has speakers!</source>
        <translation>בקהילה יש נואמים!</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="721"/>
        <source>Remove the congregation?</source>
        <translation>להסיר את הקהילה?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="767"/>
        <source>Remove the speaker?</source>
        <translation>להסיר את הנואם?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="772"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>הנואם מתוכנן לנאומים! הנאומים האלה כן
תועבר לרשימת המטלות אם תסיר את הנואם.</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="836"/>
        <source>Missing Information</source>
        <translation>מידע חסר</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="836"/>
        <source>Select congregation first</source>
        <translation>בחר תחילה קהילה</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="869"/>
        <source>New Congregation</source>
        <translation>קהילה חדשה</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="934"/>
        <source>Add Talks</source>
        <translation>הוסף נאומים</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="934"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>הזן מספרי נאום המופרדים בפסיקים או בנקודות</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="970"/>
        <source>Change congregation to &#39;%1&#39;?</source>
        <translation>לשנות את הקהילה ל- &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="972"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>הנואם מתוכנן לנאומים יוצאים. הנאומים האלה כן
תועבר לרשימת המטלות אם אתה משנה את הקהילה.</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>עמוד פתיחה</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="245"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation>התנגשות גרסה: השינויים בענן נעשו עם גרסה חדשה יותר!</translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="251"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation>התנגשות גרסא: על משתמש מורשה לעדכן את נתוני הענן באותה גרסה.</translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>שטחים</translation>
    </message>
</context></TS>
