<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr">
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="30"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="35"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="212"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="244"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="244"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="375"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="30"/>
        <source>Congregation Bible Study</source>
        <translation>Étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="288"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="313"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="436"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="555"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
</context>
<context>
    <name>CalendarPopup</name>
    <message>
        <location filename="../qml/controls/+fusion/CalendarPopup.qml" line="74"/>
        <location filename="../qml/controls/+material/CalendarPopup.qml" line="76"/>
        <location filename="../qml/controls/+universal/CalendarPopup.qml" line="74"/>
        <location filename="../qml/controls/CalendarPopup.qml" line="74"/>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>Afficher l&apos;adresse de l&apos;assemblée</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="66"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>Es-tu sûr de vouloir supprimer définitivement tes données du nuage ?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="199"/>
        <source>Last synchronized: %1</source>
        <translation>Synchronisé le : %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="204"/>
        <source>Synchronize</source>
        <translation>Synchroniser</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="217"/>
        <source>Delete Cloud Data</source>
        <translation>Effacer les données du nuage</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>Aide TheocBase</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>Impossible de démarrer l&apos;affichage de l&apos;aide (%1)</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentValidator</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="287"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>La personne assignée n&apos;est pas membre de l&apos;assemblée.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="301"/>
        <source>Unassignable. Please check the publisher&#39;s settings.</source>
        <translation>Pas attribuable. Merci de vérifier les paramètres du proclamateur.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="333"/>
        <source>The assigned person is not available on this day.</source>
        <translation>La personne assignée n&apos;est pas disponible ce jour.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="340"/>
        <source>Assignment incomplete. Please assign an assistant.</source>
        <translation>Attribution incomplète. Merci d&apos;assigner un interlocuteur.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="344"/>
        <source>Assignment incomplete. Please assign a student.</source>
        <translation>Attribution incomplète. Merci d&apos;assigner un interlocuteur.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="353"/>
        <source>Assignment incomplete. Assign a different person as student or assistant.</source>
        <translation>Attribution incomplète. Attribue une personne différente en tant qu&apos;élève ou interlocuteur.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="368"/>
        <source>The assistant should be of the same sex or should be a family member.</source>
        <translation>L&apos;interlocuteur doit être du même sexe ou un membre de la famille.</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="374"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>L&apos;interlocuteur ne doit pas être de sexe opposé.</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="26"/>
        <source>Opening Comments</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="26"/>
        <source>Concluding Comments</source>
        <translation>Paroles de conclusion</translation>
    </message>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="51"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="178"/>
        <source>Please find below details of your assignment:</source>
        <translation>Trouve ci-dessous les détails de ton attribution :</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="179"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Source material</source>
        <translation>Matières</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Please find below details of your assignment:</source>
        <translation>Trouve ci-dessous les détails de ton attribution :</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="185"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="189"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="196"/>
        <source>Study</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="198"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="199"/>
        <source>Source material</source>
        <translation>Matières</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="204"/>
        <source>Main hall</source>
        <translation>Salle principale</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="207"/>
        <source>Auxiliary classroom 1</source>
        <translation>Deuxième salle</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="210"/>
        <source>Auxiliary classroom 2</source>
        <translation>Troisième salle</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="213"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>À présenter dans</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="646"/>
        <source>Enter source material here</source>
        <translation>Saisir les références des matières ici</translation>
    </message>
</context>
<context>
    <name>LMMeetingValidator</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="902"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>La personne assignée n&apos;est pas membre de l&apos;assemblée.</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="908"/>
        <source>Unassignable. Please check the publisher&#39;s settings.</source>
        <translation>Pas attribuable. Merci de vérifier les paramètres du proclamateur.</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="926"/>
        <source>The assigned person is not available on this day.</source>
        <translation>La personne assignée n&apos;est pas disponible ce jour.</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>Nom d’utilisateur ou courriel</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>Mot de passe oublié</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>Créer un compte</translation>
    </message>
</context>
<context>
    <name>LookupControl</name>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="41"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="41"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="41"/>
        <location filename="../qml/controls/LookupControl.qml" line="41"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="118"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="118"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="118"/>
        <location filename="../qml/controls/LookupControl.qml" line="118"/>
        <source>By %1</source>
        <comment>Arrange list by the selected criteria</comment>
        <comment>the &apos;%1&apos;-part will be replaced by terms, such as date, name, ...</comment>
        <translation>Par %1</translation>
    </message>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="168"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="168"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="168"/>
        <location filename="../qml/controls/LookupControl.qml" line="168"/>
        <source>Count=%1</source>
        <comment>Number of rows in a list</comment>
        <comment>the &apos;%1&apos;-part will be replaced by the actual number</comment>
        <translation>Nombre=%1</translation>
    </message>
    <message>
        <location filename="../qml/controls/+fusion/LookupControl.qml" line="188"/>
        <location filename="../qml/controls/+material/LookupControl.qml" line="188"/>
        <location filename="../qml/controls/+universal/LookupControl.qml" line="188"/>
        <location filename="../qml/controls/LookupControl.qml" line="188"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
</context>
<context>
    <name>LookupControlMoreMenu</name>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="41"/>
        <source>Display details</source>
        <translation>Afficher les détails</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="47"/>
        <source>Off</source>
        <translation>Désactivé</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="58"/>
        <source>1 Line</source>
        <translation>1 Ligne</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="69"/>
        <source>2 Lines</source>
        <translation>2 Lignes</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="80"/>
        <source>3 Lines</source>
        <translation>3 Lignes</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="91"/>
        <source>Automatically</source>
        <translation>Automatiquement</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="107"/>
        <source>Include midweek parts</source>
        <translation>Inclure des parties de réunions de semaine</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="109"/>
        <source>Include weekend parts</source>
        <translation>Inclure des parties de réunions de week-end</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="123"/>
        <source>Labeling</source>
        <comment>To select which text to display, e.g. the theme or name of a meeting part</comment>
        <translation>Étiquetage</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="20"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="22"/>
        <source>Meeting part</source>
        <translation>Partie de Réunion</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="24"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="167"/>
        <source>Hide unavailables</source>
        <translation>Masquer les indisponibles</translation>
    </message>
    <message>
        <location filename="../qml/LookupControlMoreMenu.qml" line="176"/>
        <source>Reset default settings</source>
        <translation>Ré-initialiser les paramètres par défaut</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Services</source>
        <translation>Services</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide %1</source>
        <translation>Masquer %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Hide Others</source>
        <translation>Masquer les autres</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Show All</source>
        <translation>Tout voir</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Preferences...</source>
        <translation>Préférences…</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>Quit %1</source>
        <translation>Quitter %1</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>About %1</source>
        <translation>À propos de : %1</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="32"/>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="301"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="423"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>Conseiller adjoint 2</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="544"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>Conseiller adjoint 3</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="199"/>
        <source>TREASURES FROM GOD&#39;S WORD</source>
        <translation>JOYAUX DE LA PAROLE DE DIEU</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="206"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>APPLIQUES-TOI AU MINISTÈRE</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="211"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>VIE CHRÉTIENNE</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="226"/>
        <location filename="../qml/MWMeetingModule.qml" line="246"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="242"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="256"/>
        <source>Import Schedule...</source>
        <translation>Importer le Cahier (ePub)</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="309"/>
        <location filename="../qml/MWMeetingModule.qml" line="584"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="310"/>
        <location filename="../qml/MWMeetingModule.qml" line="585"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>S2</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="311"/>
        <location filename="../qml/MWMeetingModule.qml" line="586"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>S3</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="319"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="319"/>
        <source>Counselor</source>
        <translation>Conseiller</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="369"/>
        <location filename="../qml/MWMeetingModule.qml" line="421"/>
        <source>Song %1 &amp; Prayer</source>
        <translation>Cantique %1 et Prière</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="389"/>
        <source>Opening Comments</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="407"/>
        <source>Concluding Comments</source>
        <translation>Paroles de conclusion</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="535"/>
        <source>Song %1</source>
        <translation>Cantique %1</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="562"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="571"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="32"/>
        <source>Song %1 and Prayer</source>
        <translation>Cantique %1 et Prière</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="200"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="468"/>
        <source>Copyright</source>
        <translation>Droits d&apos;auteur</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Bibliothèques Qt sous licence GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="468"/>
        <source>TheocBase Team</source>
        <translation>L&apos;équipe TheocBase</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="333"/>
        <source>Last synchronized</source>
        <translation>Dernière synchronisation</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="471"/>
        <source>Licensed under GPLv3.</source>
        <translation>Sous licence GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="477"/>
        <source>Versions of Qt libraries </source>
        <translation>Versions des bibliothèques Qt </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1272"/>
        <source>TheocBase data exchange</source>
        <translation>TheocBase échange de données</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="538"/>
        <source>New update available. Do you want to install?</source>
        <translation>Une mise à jour est disponible. Veux-tu l&apos;installer ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="542"/>
        <source>No new update available</source>
        <translation>Pas de mise à jour disponible</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="564"/>
        <source>Save file</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1627"/>
        <source>Select ePub file</source>
        <translation>Sélectionner le fichier ePub</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1545"/>
        <source>Send e-mail reminders?</source>
        <translation>Envoyer des rappels par courriel ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1580"/>
        <source>Updates available...</source>
        <translation>Mises à jour disponibles…</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1158"/>
        <source>Error sending e-mail</source>
        <translation>Erreur lors de l&apos;envoi du courriel</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="422"/>
        <source>WEEK STARTING %1</source>
        <translation>SEMAINE DU %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="557"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>L&apos;exportation des orateurs sortants n&apos;est pas encore prête, désolé.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="568"/>
        <source>Save folder</source>
        <translation>Enregistrer le dossier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1160"/>
        <source>E-mail sent successfully</source>
        <translation>Courriel envoyé avec succès</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="581"/>
        <source>Saved successfully</source>
        <translation>Enregistrement réussi</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="649"/>
        <location filename="../mainwindow.cpp" line="654"/>
        <source>Counselor-Class II</source>
        <translation>Conseiller-Classe II</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="650"/>
        <location filename="../mainwindow.cpp" line="655"/>
        <source>Counselor-Class III</source>
        <translation>Conseiller-Classe III</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="670"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Interlocuteur de %1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="737"/>
        <location filename="../mainwindow.cpp" line="952"/>
        <location filename="../mainwindow.cpp" line="977"/>
        <source>Kingdom Hall</source>
        <translation>Salle du Royaume</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="648"/>
        <location filename="../mainwindow.cpp" line="653"/>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="677"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Lecteur de l&apos;étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="662"/>
        <location filename="../mainwindow.cpp" line="678"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="651"/>
        <location filename="../mainwindow.cpp" line="656"/>
        <location filename="../mainwindow.cpp" line="701"/>
        <location filename="../mainwindow.cpp" line="703"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="662"/>
        <source>Timing</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="908"/>
        <source>Public Talk</source>
        <translation>Discours public</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="760"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="765"/>
        <source>Watchtower Study Conductor</source>
        <translation>Conducteur de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="767"/>
        <source>Watchtower reader</source>
        <translation>Lecteur de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1013"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation>Les mêmes modifications sont présentes localement et dans le nuage (%1 lignes). Veux-tu conserver les modifications locales ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1054"/>
        <source>The cloud data has now been deleted.</source>
        <translation>Les données du nuage sont maintenant supprimées.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>Synchronize</source>
        <translation>Synchroniser</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1060"/>
        <source>Sign Out</source>
        <translation>Se déconnecter</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1070"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>Les données du nuage ont été supprimées. Tes données locales vont être remplacées. Veux-tu continuer ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1169"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1196"/>
        <location filename="../mainwindow.cpp" line="1213"/>
        <source>Open directory</source>
        <translation>Ouvrir un dossier</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1244"/>
        <source>Import Error</source>
        <translation>Erreur d’importation</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1244"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>Impossible d&apos;importer depuis le logiciel Ta1ks. Ils manquent des fichiers:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1259"/>
        <source>Save unsaved data?</source>
        <translation>Enregistrer les modifications ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1268"/>
        <source>Import file?</source>
        <translation>Importer un fichier ?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="108"/>
        <location filename="../mainwindow.ui" line="377"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="275"/>
        <source>Public talks</source>
        <translation>Discours publics</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="407"/>
        <source>Import</source>
        <translation>Import</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="441"/>
        <source>info</source>
        <translation>info</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="976"/>
        <source>Data exhange</source>
        <translation>Échange de données</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1029"/>
        <source>TheocBase Cloud</source>
        <translation>TheocBase Cloud</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="580"/>
        <location filename="../mainwindow.ui" line="609"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="853"/>
        <source>Print...</source>
        <translation>Imprimer…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="856"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="865"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Paramètres…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="877"/>
        <source>Publishers...</source>
        <translation>Proclamateurs…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="228"/>
        <location filename="../mainwindow.ui" line="880"/>
        <source>Publishers</source>
        <translation>Proclamateurs</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="946"/>
        <source>Speakers...</source>
        <translation>Orateurs…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="255"/>
        <location filename="../mainwindow.ui" line="949"/>
        <source>Speakers</source>
        <translation>Orateurs</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="973"/>
        <source>Data exhange...</source>
        <translation>Échange de données…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="985"/>
        <source>TheocBase help...</source>
        <translation>Aide TheocBase…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1000"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1012"/>
        <location filename="../mainwindow.ui" line="1015"/>
        <source>Full Screen</source>
        <translation>Plein écran</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1020"/>
        <source>Startup Screen</source>
        <translation>Page d&apos;accueil</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1062"/>
        <source>Reminders...</source>
        <translation>Rappels…</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="513"/>
        <source>Data exchange</source>
        <translation>Échange de données</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="119"/>
        <source>Export Format</source>
        <translation>Format d&apos;export</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="125"/>
        <source>For sending data to another user</source>
        <translation>Pour envoyer les données à un autre utilisateur</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="144"/>
        <source>For easy import to Calendar programs</source>
        <translation>Pour importer vers des agendas électroniques</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>Export Method</source>
        <translation>Méthode d&apos;export</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="189"/>
        <source>Events grouped by date</source>
        <translation>Évènements groupés par date</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>All day events</source>
        <translation>Évènements de la journée</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="218"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="282"/>
        <source>Outgoing Talks</source>
        <translation>Discours à l&apos;extérieur</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Date Range</source>
        <translation>Période</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Previous Weeks</source>
        <translation>Semaines précédentes</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="321"/>
        <source>From Date</source>
        <translation>Depuis le</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="345"/>
        <source>Thru Date</source>
        <translation>Jusqu&apos;au</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="733"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="739"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="752"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="841"/>
        <source>Today</source>
        <translation>Aujourd&apos;hui</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="889"/>
        <source>Exit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="901"/>
        <source>Report bug...</source>
        <translation>Rapporter un bogue…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="913"/>
        <source>Send feedback...</source>
        <translation>Envoyer des remarques…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="931"/>
        <source>About TheocBase...</source>
        <translation>À propos de TheocBase…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="958"/>
        <source>Check updates...</source>
        <translation>Vérifier les mises à jour…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="988"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1050"/>
        <source>Territories...</source>
        <translation>Territoires…</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1053"/>
        <source>Territories</source>
        <translation>Territoires</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="820"/>
        <source>Back</source>
        <translation>Retour</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="832"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1038"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="922"/>
        <source>TheocBase website</source>
        <translation>Site internet TheocBase</translation>
    </message>
</context>
<context>
    <name>MeetingNotes</name>
    <message>
        <location filename="../qml/MeetingNotes.qml" line="26"/>
        <location filename="../qml/MeetingNotes.qml" line="47"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Notes</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="33"/>
        <source>Outgoing Speaker</source>
        <translation>Orateur sortant</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="294"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="407"/>
        <source>Move to different week</source>
        <translation>Déplacer vers une autre semaine</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="426"/>
        <source>Send to To Do List</source>
        <translation>Mettre dans la liste &quot;À Faire&quot;</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="444"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="554"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="680"/>
        <source>Meeting day</source>
        <translation>Jour de réunion</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="695"/>
        <source>Meeting time</source>
        <translation>Horaires des réunions</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="714"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="658"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerValidator</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="259"/>
        <source>This talk has been discontinued. Please choose another talk.</source>
        <translation>Ce discours n&apos;est plus d&apos;actualité. Merci de choisir un autre discours.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="278"/>
        <source>This talk cannot be delivered by the speaker. Please choose another talk or speaker.</source>
        <translation>Ce discours ne peut pas être prononcé par l&apos;orateur. Merci de choisir un autre discours ou orateur.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="292"/>
        <source>Unassignable. Please check the speaker&#39;s settings.</source>
        <translation>Non attribuable. Merci de vérifier les paramètres de l&apos;orateur.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="310"/>
        <source>The assigned person is not available on this day.</source>
        <translation>La personne assignée n&apos;est pas disponible ce jour.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="321"/>
        <source>It is preferred to assign speakers to be away only once a month.</source>
        <translation>Il est préférable d&apos;assigner les absences des orateurs une seule fois par mois.</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="178"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>Il y a déjà un discours programmé à la date choisie.</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="179"/>
        <source>Swap Talks</source>
        <translation>Échanger les discours</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="180"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="206"/>
        <source>From %1</source>
        <translation>Depuis le %1</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="111"/>
        <source>Outgoing speakers</source>
        <translation>Orateurs Sortants</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="164"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 orateur sortant ce week-end</numerusform>
            <numerusform>%1 orateurs sortants ce week-end</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="165"/>
        <source>No speakers away this weekend</source>
        <translation>Pas d&apos;orateurs sortants ce week-end</translation>
    </message>
</context>
<context>
    <name>PersonDetailFilterModel</name>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="11"/>
        <source>Nonstudent meeting parts</source>
        <translation>Parties de réunion non attribuables</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="17"/>
        <source>Other assignments</source>
        <translation>Autres attributions</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="23"/>
        <source>Student parts</source>
        <translation>Parties d&apos;élève</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="29"/>
        <source>Assistant in student parts</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailFilterModel.qml" line="35"/>
        <source>Similar assignments only</source>
        <translation>Seulement les attributions identiques</translation>
    </message>
</context>
<context>
    <name>PersonDetailGroupByModel</name>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="11"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="15"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="19"/>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="23"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="27"/>
        <source>Meeting part</source>
        <translation>Partie de Réunion</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="31"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/PersonDetailGroupByModel.qml" line="35"/>
        <source>Weeks idle</source>
        <translation>Semaines d&apos;inactivité</translation>
    </message>
</context>
<context>
    <name>PersonsModel</name>
    <message>
        <location filename="../cpersons.cpp" line="393"/>
        <source>Last name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../cpersons.cpp" line="394"/>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&lt;--</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrintCombination</name>
    <message>
        <location filename="../print/printcombination.cpp" line="31"/>
        <source>Select at least one option</source>
        <translation>Sélectionner au moins une option</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="41"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Assemblée %1</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="39"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="47"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="48"/>
        <source>Counselor</source>
        <translation>Conseiller</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="50"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="49"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="51"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="44"/>
        <source>Public Meeting</source>
        <translation>Réunion publique</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="45"/>
        <source>Public Talk</source>
        <translation>Discours public</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="46"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="43"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="42"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="53"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="68"/>
        <location filename="../print/printcombination.cpp" line="69"/>
        <source>Combined Schedule</source>
        <translation>Programme combiné</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="70"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="71"/>
        <source>Outgoing Speakers</source>
        <translation>Orateurs sortants</translation>
    </message>
    <message>
        <location filename="../print/printcombination.cpp" line="88"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>Semaine du %1</translation>
    </message>
</context>
<context>
    <name>PrintDocument</name>
    <message>
        <location filename="../print/printdocument.cpp" line="144"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>Modèle introuvable</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="149"/>
        <source>Can&#39;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>Impossible de lire le fichier</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="235"/>
        <source>Service Talk</source>
        <translation>Discours de service</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="247"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>Commence à</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSchedule</name>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="37"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Assemblée %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="38"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="40"/>
        <source>Public Talk</source>
        <translation>Discours public</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="41"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="42"/>
        <location filename="../print/printmidweekschedule.cpp" line="82"/>
        <source>Treasures from God&#39;s Word</source>
        <translation>Joyaux de la Parole de Dieu</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="43"/>
        <location filename="../print/printmidweekschedule.cpp" line="83"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>Applique-toi au ministère</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="44"/>
        <location filename="../print/printmidweekschedule.cpp" line="84"/>
        <source>Living as Christians</source>
        <translation>Vie chrétienne</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="46"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="71"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="72"/>
        <source>Worksheet</source>
        <translation>Feuille de travail</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="73"/>
        <location filename="../print/printmidweekschedule.cpp" line="106"/>
        <source>Opening Comments</source>
        <comment>See Workbook</comment>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="74"/>
        <location filename="../print/printmidweekschedule.cpp" line="107"/>
        <source>Concluding Comments</source>
        <comment>See Workbook</comment>
        <translation>Paroles de conclusion</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="75"/>
        <source>Song</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="76"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="77"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="78"/>
        <source>Counselor</source>
        <translation>Conseiller</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="79"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="80"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="81"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="85"/>
        <source>Congregation Bible Study</source>
        <translation>Étude biblique de l’assemblée</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="86"/>
        <source>Circuit Overseer</source>
        <translation>Responsable de circonscription</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="87"/>
        <source>No regular meeting</source>
        <translation>Pas de réunion habituelle</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="88"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="89"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="90"/>
        <source>Theme</source>
        <comment>Talk Theme description from workbook</comment>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="91"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>Sources</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="92"/>
        <source>Exercises</source>
        <translation>Exercices</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="93"/>
        <source>Timing</source>
        <comment>Assignment completed in time?</comment>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="94"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="95"/>
        <source>Next study</source>
        <translation>Prochaine leçon</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="96"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>Cadre</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="97"/>
        <location filename="../print/printmidweekschedule.cpp" line="98"/>
        <location filename="../print/printmidweekschedule.cpp" line="99"/>
        <source>Class</source>
        <translation>Salle</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="100"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>Conseiller en salle secondaire</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="101"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>Salle secondaire</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="102"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>Salle principale</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="103"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation>SP</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="104"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="105"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>Élève</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="108"/>
        <source>Today</source>
        <translation>Aujourd’hui</translation>
    </message>
    <message>
        <location filename="../print/printmidweekschedule.cpp" line="109"/>
        <source>Next week</source>
        <translation>Semaine prochaine</translation>
    </message>
</context>
<context>
    <name>PrintMidweekSlip</name>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="50"/>
        <source>Slip Template</source>
        <translation>Modèle de fiche</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="465"/>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="469"/>
        <location filename="../print/printmidweekslip.cpp" line="471"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation>Leçon %1</translation>
    </message>
    <message>
        <location filename="../print/printmidweekslip.cpp" line="509"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>2d sujet</translation>
    </message>
</context>
<context>
    <name>PrintMidweekWorksheet</name>
    <message>
        <location filename="../print/printmidweekworksheet.cpp" line="61"/>
        <source>Class </source>
        <translation>Salle </translation>
    </message>
</context>
<context>
    <name>PrintOutgoingSchedule</name>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="59"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Assemblée %1</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="60"/>
        <location filename="../print/printoutgoingschedule.cpp" line="170"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="62"/>
        <source>Public Talk</source>
        <translation>Discours public</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="63"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="163"/>
        <source>Talk</source>
        <translation>Discours</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="164"/>
        <source>Outgoing Speakers</source>
        <translation>Orateurs sortants</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="165"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="166"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="167"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="168"/>
        <source>Theme Number</source>
        <translation>Numéro du thème</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="169"/>
        <source>Circuit</source>
        <translation>Circonscription</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="171"/>
        <source>Start Time</source>
        <translation>Heure de début</translation>
    </message>
    <message>
        <location filename="../print/printoutgoingschedule.cpp" line="173"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>NOTE : Cher frère, malgré une maintenance minutieuse de la base de données, parfois des dates ou des adresses pourraient être erronées. Merci de vérifier en les recherchant sur JW.ORG.</translation>
    </message>
</context>
<context>
    <name>PrintTalksOfSpeakersList</name>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="37"/>
        <location filename="../print/printtalksofspeakerslist.cpp" line="38"/>
        <source>Public Talks</source>
        <translation>Discours public</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="39"/>
        <source>Contact info</source>
        <translation>Coordonnées</translation>
    </message>
    <message>
        <location filename="../print/printtalksofspeakerslist.cpp" line="40"/>
        <source>Talk numbers</source>
        <comment>Public talk numbers</comment>
        <translation>Numéros de discours</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryAssignmentRecord</name>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="35"/>
        <source>Territory Assignment Record</source>
        <translation>Enregistrement des attributions de territoire</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="36"/>
        <source>Territory Coverage</source>
        <translation>Couverture du territoire</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="37"/>
        <source>Total number of territories</source>
        <translation>Nombre total de territoires</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="38"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 mois</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="39"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 à 12 mois</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="40"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 mois</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="41"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation>Moyenne annuelle</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="177"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="178"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation>Adresses</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="179"/>
        <source>Address type</source>
        <translation>Type d’adresse</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="180"/>
        <source>Assigned to</source>
        <translation>Attribué à</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="181"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="183"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Assemblée %1</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="185"/>
        <source>Date checked out</source>
        <translation>Date de sortie</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="186"/>
        <source>Date checked back in</source>
        <translation>Date de rentrée</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="187"/>
        <source>Date assigned</source>
        <comment>Territory assignment</comment>
        <translation>Date d&apos;attribution</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="188"/>
        <source>Date completed</source>
        <comment>Territory assignment</comment>
        <translation>Terminé le</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="189"/>
        <source>Date last worked</source>
        <translation>Date du dernier exercice</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="190"/>
        <source>Last date completed</source>
        <comment>Column to record the date on which each territory was last completed, when beginning a new S-13 sheet.</comment>
        <translation>Travaillé pour la dernière fois le</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="191"/>
        <source>Date requested</source>
        <translation>Date demandée</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="192"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="193"/>
        <source>Service year</source>
        <translation>Année de service</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="194"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="195"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="196"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Département</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="197"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Région</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="198"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="208"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>N°</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="209"/>
        <source>Locality</source>
        <translation>Localité</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="210"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation>Carte</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="211"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="212"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>Code postal</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="213"/>
        <source>Name of publisher</source>
        <translation>Nom du proclamateur</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="214"/>
        <source>Remark</source>
        <translation>Remarque</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="215"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Département</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="216"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Rue</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="217"/>
        <source>Territory</source>
        <translation>Territoire</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="218"/>
        <source>Terr. No.</source>
        <translation>Terr. N°.</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="219"/>
        <source>Territory type</source>
        <translation>Type de territoire</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="220"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation>À</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="221"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../print/printterritoryassignmentrecord.cpp" line="222"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation>Somme</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryCard</name>
    <message>
        <location filename="../print/printterritorycard.cpp" line="35"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>Carte de territoire</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="36"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation>Carte de territoire</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="37"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation>Liste d&apos;adresses</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="38"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation>Carte de territoire avec liste d&apos;adresses</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="39"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation>Liste des rues</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="40"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation>Carte du territoire avec la liste des rues</translation>
    </message>
    <message>
        <location filename="../print/printterritorycard.cpp" line="41"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation>Liste &quot;Ne pas contacter&quot;</translation>
    </message>
</context>
<context>
    <name>PrintTerritoryMapCard</name>
    <message>
        <location filename="../print/printterritorymapcard.cpp" line="53"/>
        <source>Template</source>
        <translation>Modèle</translation>
    </message>
</context>
<context>
    <name>PrintWeekendSchedule</name>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="36"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="37"/>
        <source>Public Meeting</source>
        <translation>Réunion publique</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="40"/>
        <location filename="../print/printweekendschedule.cpp" line="167"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>Assemblée %1</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="41"/>
        <location filename="../print/printweekendschedule.cpp" line="168"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="43"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="44"/>
        <location filename="../print/printweekendschedule.cpp" line="63"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="46"/>
        <location filename="../print/printweekendschedule.cpp" line="65"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="47"/>
        <location filename="../print/printweekendschedule.cpp" line="102"/>
        <source>Public Talk</source>
        <translation>Discours public</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="48"/>
        <location filename="../print/printweekendschedule.cpp" line="104"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="49"/>
        <source>Congregation Bible Study</source>
        <translation>Étude biblique de l’assemblée</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="50"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>Réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="51"/>
        <location filename="../print/printweekendschedule.cpp" line="68"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="53"/>
        <source>min.</source>
        <comment>Abbreviation of minutes</comment>
        <translation>min</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="54"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="55"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation>Hôte</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="62"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="66"/>
        <source>WT Reader</source>
        <translation>Lecteur TG</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="69"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="105"/>
        <source>Circuit Overseer</source>
        <translation>Responsable de circonscription</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="108"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="120"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="123"/>
        <source>Song</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="205"/>
        <source>Watchtower Conductor</source>
        <translation>Conducteur de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../print/printweekendschedule.cpp" line="208"/>
        <source>No regular meeting</source>
        <translation>Pas de réunion habituelle</translation>
    </message>
</context>
<context>
    <name>PrintWeekendWorksheet</name>
    <message>
        <location filename="../print/printweekendworksheet.cpp" line="47"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
</context>
<context>
    <name>ProfileManager</name>
    <message>
        <location filename="../qml/ProfileManager.qml" line="19"/>
        <source>Add profile</source>
        <translation>Ajouter un profil</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="59"/>
        <source>Switch</source>
        <translation>Basculer</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="81"/>
        <source>Enter new profile name</source>
        <translation>Entrer un nouveau nom de profil</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="96"/>
        <source>TheocBase needs to be restarted. Click OK to continue.</source>
        <translation>TheocBase a besoin d&apos;être redémarré. Clique sur OK pour continuer.</translation>
    </message>
    <message>
        <location filename="../qml/ProfileManager.qml" line="103"/>
        <source>Deleting a profile will remove the current database and settings files. This option cannot be undone. Would you like to delete the profile data files?</source>
        <translation>La suppression d&apos;un profil supprimera la base de données et les fichiers de paramètres actuels. Cette option est irréversible. Veux-tu vraiment supprimer les fichiers de données du profil ?</translation>
    </message>
</context>
<context>
    <name>PublicTalkDetail</name>
    <message>
        <location filename="../publictalkdetail.cpp" line="32"/>
        <source>%1 to %2</source>
        <translation>%1 à %2</translation>
    </message>
</context>
<context>
    <name>PublicTalkDetailGroupByModel</name>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="14"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="18"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="22"/>
        <source>Frequency</source>
        <translation>Fréquence</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="26"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="30"/>
        <source>Number</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkDetailGroupByModel.qml" line="34"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="32"/>
        <source>Public Talk</source>
        <translation>Discours Public</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="632"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="368"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="475"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="590"/>
        <source>Move to different week</source>
        <translation>Déplacer vers une autre semaine</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="609"/>
        <source>Send to To Do List</source>
        <translation>Mettre dans la liste &quot;À Faire&quot;</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="738"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="757"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="776"/>
        <source>Email</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="795"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="818"/>
        <source>Host</source>
        <translation>Hôte</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>&amp;Yes</source>
        <translation>&amp;Oui</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>&amp;No</source>
        <translation>&amp;Non</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuler</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="78"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="170"/>
        <source>Wrong username and/or password</source>
        <translation>Nom d&apos;utilisateur et/ou mot de passe incorrect</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="314"/>
        <source>Database not found!</source>
        <translation>Base de données non trouvée !</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="317"/>
        <source>Choose database</source>
        <translation>Choisir la base de données</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="317"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>Fichiers SQLite (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="341"/>
        <source>Database restoring failed</source>
        <translation>La restauration de la base de donnée a échouée</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="390"/>
        <location filename="../mainwindow.cpp" line="287"/>
        <source>Save changes?</source>
        <translation>Enregistrer les modifications ?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="310"/>
        <source>Database copied to </source>
        <translation>Base de données copié vers </translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="40"/>
        <source>Database file not found! Searching path =</source>
        <translation>Base de données non trouvée ! Répertoire de recherche :</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="59"/>
        <source>Database Error</source>
        <translation>Erreur de base de données</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="705"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>Cette version de l&apos;application (%1) est plus vieille que la base de données (%2). Il y a une forte probabilité que des messages d&apos;erreur apparaissent et que les modifications ne soient pas sauvegardées correctement. Merci de télécharger et installer la dernière version pour de meilleurs résultats.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="368"/>
        <location filename="../mainwindow.cpp" line="909"/>
        <location filename="../sql_class.cpp" line="1226"/>
        <location filename="../sql_class.cpp" line="1227"/>
        <location filename="../todo.cpp" line="373"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="369"/>
        <source>Circuit</source>
        <translation>Circonscription</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="749"/>
        <source>Database updated</source>
        <translation>Base de données mise à jour</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="302"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (Pas de réunion)</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="365"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="284"/>
        <location filename="../cpublictalks.cpp" line="366"/>
        <source>Last</source>
        <translation>Dernière</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="368"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="369"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="82"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="370"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="83"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="371"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="84"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="625"/>
        <location filename="../generatexml.cpp" line="109"/>
        <source>Default language not selected!</source>
        <translation>Langue par défaut non sélectionnée !</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="744"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>La première ligne du fichier CSV est invalide.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="297"/>
        <source>Confirm password!</source>
        <translation>Confirmer le mot de passe !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2370"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>Un utilisateur avec la même adresse électronique a déjà été ajouté.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2584"/>
        <source>All talks have been added to this week</source>
        <translation>Toutes les attributions ont été ajoutées à cette semaine</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="57"/>
        <location filename="../importkhs.cpp" line="53"/>
        <location filename="../importwintm.cpp" line="37"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="57"/>
        <location filename="../importkhs.cpp" line="53"/>
        <location filename="../importwintm.cpp" line="37"/>
        <source>Import Complete</source>
        <translation>Importation terminée</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="95"/>
        <source>Unable to read new Workbook format</source>
        <translation>Impossible de lire le nouveau format du Cahier Vie et ministère</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="97"/>
        <source>Database not set to handle language &#39;%1&#39;</source>
        <translation>La base de données ne peut pas utiliser la langue &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="99"/>
        <source>Unable to find year/month</source>
        <translation>Incapable de trouver l&apos;année/mois</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="100"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>Rien n&apos;a été importé (aucune date reconnue)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="104"/>
        <location filename="../wtimport.cpp" line="33"/>
        <location filename="../wtimport.cpp" line="34"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>%1 semaines ajoutées. Du %2 au %3</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="429"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>Merci de sélectionner le thème des discours correspondant au Cahier Vie et ministère.</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>Pr</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#L</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>Mem</source>
        <comment>history table: abbreviation for assistant/householder of &apos;Memorial Inviation&apos;</comment>
        <translation>Mém</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="19"/>
        <source>Vid</source>
        <comment>history table: abbreviation for &apos;Extra Video Part&apos;</comment>
        <translation>Vid</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="22"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="23"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="26"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="27"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="32"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>#3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="34"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>VC1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>VC2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>VC3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="41"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>EB-L</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>EB</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="32"/>
        <source>One-time Scanning of New Slip</source>
        <translation>Numérisation unique d&apos;une nouvelle fiche d&apos;attribution</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="726"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>Toutes les réunions de week-end</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="48"/>
        <location filename="../mainwindow.cpp" line="738"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="50"/>
        <source>Our Christian Life and Ministry Meeting</source>
        <translation>Réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="52"/>
        <source>Meeting for Field Service</source>
        <translation>Réunion de service</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="92"/>
        <source>Treasures From God&#39;s Word</source>
        <translation>Joyaux de la Parole de Dieu</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="94"/>
        <source>Apply Yourself To The Field Ministry</source>
        <translation>Applique-toi au ministère</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="96"/>
        <source>Living As Christians</source>
        <translation>Vie chrétienne</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="160"/>
        <source>Song and Prayer</source>
        <translation>Cantique et Prière</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="162"/>
        <source>Treasures From God’s Word</source>
        <translation>Joyaux de la Parole de Dieu</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="164"/>
        <source>Spiritual Gems</source>
        <translation>Perles spirituelles</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="166"/>
        <source>Bible Reading</source>
        <translation>Lecture de la Bible</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="168"/>
        <source>Sample Conversation Video</source>
        <translation>Vidéo de modèle de conversation</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="170"/>
        <source>Initial Call</source>
        <translation>Premier contact</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="172"/>
        <source>Return Visit</source>
        <translation>Nouvelle visite</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="174"/>
        <source>Bible Study</source>
        <translation>Cours biblique</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="178"/>
        <location filename="../assignmentInfo.h" line="180"/>
        <location filename="../assignmentInfo.h" line="182"/>
        <source>Living as Christians Talk</source>
        <translation>Discours Vie chrétienne</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="184"/>
        <source>Congregation Bible Study</source>
        <translation>Étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="186"/>
        <source>Service Talk</source>
        <translation>Discours de service</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="188"/>
        <source>Memorial Invitation</source>
        <translation>Invitation au Mémorial</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="190"/>
        <source>Other Video Part</source>
        <translation>Autre partie vidéo</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="194"/>
        <source>Public Talk</source>
        <translation>Discours Public</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="196"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="242"/>
        <source>Meeting parts</source>
        <translation>Parties de réunions</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="244"/>
        <source>Other assignments</source>
        <translation>Autres attributions</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="246"/>
        <source>Student parts</source>
        <translation>Parties d&apos;élève</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="248"/>
        <source>Assistant in student parts</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="250"/>
        <source>Support tasks</source>
        <translation>Tâches de support</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="286"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="288"/>
        <source>Counselor</source>
        <translation>Conseiller</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="290"/>
        <source>Study conductor</source>
        <translation>Conducteur d&apos;Étude</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="292"/>
        <source>Talk</source>
        <translation>Discours</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="294"/>
        <source>Discussion</source>
        <translation>Discussion</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="296"/>
        <source>Demonstration</source>
        <translation>Démonstration</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="298"/>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="300"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="302"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="304"/>
        <source>Hospitality</source>
        <translation>Hospitalité</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="334"/>
        <source>Opening prayer</source>
        <translation>Prière d&apos;introduction</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="336"/>
        <source>Concluding prayer</source>
        <translation>Prière de conclusion</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="338"/>
        <source>Life and Ministry Meeting Chairman</source>
        <translation>Président de la réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.h" line="340"/>
        <source>Public Meeting Chairman</source>
        <translation>Président de la réunion publique</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="846"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>Tous les discours sortants pour</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="862"/>
        <location filename="../mainwindow.cpp" line="873"/>
        <source>Outgoing Talks</source>
        <translation>Discours à l&apos;extérieur</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="978"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="367"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="80"/>
        <location filename="../schoolreminder.cpp" line="183"/>
        <location filename="../todo.cpp" line="353"/>
        <location filename="../todo.cpp" line="390"/>
        <location filename="../todo.cpp" line="401"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="373"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="86"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Ensemble</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Sister</source>
        <translation>Sœur</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="168"/>
        <source>Brother</source>
        <translation>Frère</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Dear %1 %2</source>
        <translation>Bonjour %1 %2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="173"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>Trouve ci-dessous les détails de ta prochaine attribution :</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="241"/>
        <source>Regards</source>
        <translation>Fraternellement</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="228"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>Proclamateur</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="117"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Voir le programme de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="119"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Modifier le programme de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="121"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>Voir les paramètres pour la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="123"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>Modifier les paramètres pour la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="125"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>Envoyer des rappels pour la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="127"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Imprimer le programme de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="129"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>Imprimer les fiches d&apos;attribution de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="131"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>Imprimer les feuilles de travail de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="133"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Voir le programme de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="135"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Modifier le programme de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="137"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>Voir les paramètres de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="139"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>Modifier les paramètres de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="141"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>Voir la liste des discours</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="143"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>Modifier la liste des discours</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="145"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>Programme de l&apos;hospitalité</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="147"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>Imprimer le programme de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="149"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>Imprimer les feuilles de travail de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="151"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>Imprimer le programme des orateurs</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="153"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>Imprimer les attributions des orateurs</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="155"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>Imprimer le programme de l&apos;hospitalité</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="157"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>Imprimer la liste des discours</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="167"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>Voir les orateurs publics</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="169"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>Modifier les orateurs publics</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="159"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>Voir les proclamateurs</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="161"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>Modifier les proclamateurs</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="163"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation>Voir les données des élèves</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="165"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation>Modifier les données des élèves</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="171"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation>Voir les privilèges</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="173"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation>Modifier les privilèges</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="175"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation>Afficher l&apos;historique des discours de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="177"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation>Voir les disponibilités</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="179"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation>Modifier les disponibilités</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="181"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>Voir les permissions</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="183"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>Modifier les permissions</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="185"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>Voir les territoires</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="187"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>Modifier les territoires</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="189"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>Imprimer les attributions de territoire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="191"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>Imprimer la carte de territoire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="193"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>Imprimer la carte du territoire et la liste des adresses</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="195"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>Voir les paramètres du territoire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="197"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>Modifier les paramètres du territoire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="199"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>Voir les attributions du territoire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="201"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>Voir les adresses du territoire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="203"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>Voir les paramètres de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="205"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>Modifier les paramètres de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="207"/>
        <location filename="../accesscontrol.h" line="211"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>Voir les événements spéciaux</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <location filename="../accesscontrol.h" line="213"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>Modifier les événements spéciaux</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>Voir les cantiques</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>Modifier les cantiques</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation>Effacer les données du nuage</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="230"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>Ancien</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="232"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>Président de la réunion Vie Chrétienne et Ministère</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="234"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>Responsable de la réunion Vie Chrétienne et Ministère</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="236"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>Coordinateur des discours publics</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="238"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>Préposé aux territoires</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="240"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>Secrétaire</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="242"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>Responsable de la prédication</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="244"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>Coordinateur du CA</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="246"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>Administrateur</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="499"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>L&apos;unité de la largeur n&apos;est pas la même que celle de la hauteur</translation>
    </message>
    <message>
        <location filename="../print/printdocument.cpp" line="527"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>Entrée invalide, désolé.</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.cpp" line="267"/>
        <source>Unknown assignment info = %1; class %2; %3; assistant (%4)</source>
        <translation>Informations d&apos;attributions inconnues = %1 ; classe %2 ; %3 ; assistant (%4)</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.cpp" line="280"/>
        <source>Unknown meeting part (%1).
Please check the schedule for invalid or obsolete meeting parts.</source>
        <translation>Partie de réunion inconnue (%1).
Merci de vérifier le calendrier pour les parties de réunion non valides ou obsolètes.</translation>
    </message>
    <message>
        <location filename="../assignmentInfo.cpp" line="284"/>
        <source>Unknown meeting part (%1).
Please check the schedule for invalid or obsolete meeting parts in the week starting %2.</source>
        <translation>Partie de réunion inconnue (%1).
Merci de vérifier le calendrier pour les parties de réunion non valides ou obsolètes dans la semaine commençant le %2.</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="72"/>
        <location filename="../todo.cpp" line="107"/>
        <source>From %1; speaker removed</source>
        <translation>Depuis %1 ; orateur supprimé</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="110"/>
        <source>From %1; speaker moved to %2</source>
        <translation>Depuis le %1, la nouvelle assemblée de l&apos;orateur est %2</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="154"/>
        <location filename="../todo.cpp" line="190"/>
        <source>From %1; talk discontinued</source>
        <translation>Plan non attribuable, depuis le %1</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="366"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="380"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="151"/>
        <location filename="../settings.ui" line="154"/>
        <location filename="../settings.ui" line="1421"/>
        <source>Exceptions</source>
        <translation>Exceptions</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="175"/>
        <location filename="../settings.ui" line="178"/>
        <source>Public Talks</source>
        <translation>Discours publics</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="671"/>
        <source>Custom templates folder</source>
        <translation>Dossier des modèles personnalisés</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="805"/>
        <source>Open database location</source>
        <translation>Ouvrir l&apos;emplacement de la base de données</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="859"/>
        <source>Backup database</source>
        <translation>Sauvegarder la base de données</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="888"/>
        <source>Restore database</source>
        <translation>Restaurer la base de données</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1272"/>
        <source>Names display order</source>
        <translation>Ordre d&apos;affichage des noms</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1212"/>
        <source>Color palette</source>
        <translation>Palette de couleurs</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1279"/>
        <source>By last name</source>
        <translation>Par nom</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1286"/>
        <source>By first name</source>
        <translation>Par prénom</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1205"/>
        <source>Light</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1176"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1361"/>
        <source>Show song titles</source>
        <translation>Voir le titre des cantiques</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1635"/>
        <source>Weekend meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1931"/>
        <location filename="../settings.ui" line="2114"/>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Erreurs&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2261"/>
        <source>Public talks maintenance</source>
        <translation>Gestion des discours publics</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2353"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>Gestion de l&apos;hospitalité pour les orateurs</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2869"/>
        <source>Streets</source>
        <translation>Rues</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2883"/>
        <source>Default street type:</source>
        <translation>Type de rue par défaut :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2911"/>
        <source>Street types</source>
        <translation>Types de rue</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3161"/>
        <source>Map marker scale:</source>
        <translation>Échelle du marqueur :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3187"/>
        <source>Geo Services</source>
        <translation>Géolocalisation</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3195"/>
        <source>Google:</source>
        <translation>Google Maps :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3205"/>
        <location filename="../settings.ui" line="3208"/>
        <source>API Key</source>
        <translation>Clé API</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3215"/>
        <source>Here:</source>
        <translation>Here.com :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3222"/>
        <source>Default:</source>
        <translation>Par défaut :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3232"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3237"/>
        <source>Google</source>
        <translation>Google Maps</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3242"/>
        <source>Here</source>
        <translation>Here.com</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3257"/>
        <location filename="../settings.ui" line="3260"/>
        <source>App Id</source>
        <translation>App ID</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3267"/>
        <location filename="../settings.ui" line="3270"/>
        <source>App Code</source>
        <translation>App Code</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3343"/>
        <source>Send E-Mail Reminders</source>
        <translation>Envoyer des rappels par courriel</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3718"/>
        <location filename="../settings.cpp" line="2145"/>
        <source>Profile Manager</source>
        <translation>Gestionnaire de profils</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3316"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>Envoyer les rappels à la fermeture de TheocBase</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3326"/>
        <source>E-Mail Options</source>
        <translation>Paramètres courriel</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3377"/>
        <source>Sender&#39;s e-mail</source>
        <translation>Adresse électronique de l&apos;expéditeur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3387"/>
        <source>Sender&#39;s name</source>
        <translation>Nom de l&apos;expéditeur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3451"/>
        <source>Account</source>
        <translation>Compte</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3514"/>
        <source>Test Connection</source>
        <translation>Tester la connexion</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="614"/>
        <source>Printing</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3547"/>
        <source>Users</source>
        <translation>Utilisateurs</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3563"/>
        <source>E-mail:</source>
        <translation>Courriel :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3616"/>
        <source>Rules</source>
        <translation>Règles</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="139"/>
        <location filename="../settings.ui" line="142"/>
        <source>General</source>
        <translation>Principale</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="732"/>
        <source>Backup</source>
        <translation>Sauvegarde</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="406"/>
        <source>Current congregation</source>
        <translation>Assemblée actuelle</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <source>User interface</source>
        <translation>Interface utilisateur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface language</source>
        <translation>Langue de l&apos;interface utilisateur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="936"/>
        <source>Security</source>
        <translation>Sécurité</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1079"/>
        <source>Enable password</source>
        <translation>Activer le mot de passe</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1099"/>
        <source>Enable database encryption</source>
        <translation>Activer le chiffrement de la base de données</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Confirm</source>
        <translation>Confirmer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="163"/>
        <location filename="../settings.ui" line="166"/>
        <location filename="../settings.ui" line="1584"/>
        <source>Life and Ministry Meeting</source>
        <translation>Réunion Vie Chrétienne et Ministère</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1757"/>
        <source>Remove Duplicates</source>
        <translation>Supprimer les doublons</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1764"/>
        <source>Meeting Items</source>
        <translation>Parties de la réunion</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1863"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>Programme</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2363"/>
        <source>Hide discontinued</source>
        <translation>Masquer les plans arrêtés</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2545"/>
        <source>Add songs</source>
        <translation>Ajouter des cantiques</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="187"/>
        <location filename="../settings.ui" line="2551"/>
        <source>Songs</source>
        <translation>Cantiques</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2623"/>
        <location filename="../settings.ui" line="2629"/>
        <source>Add song one at a time</source>
        <translation>Ajouter un cantique à la fois</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2644"/>
        <source>Song number</source>
        <translation>Numéro du cantique</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2651"/>
        <source>Song title</source>
        <translation>Titre du cantique</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2725"/>
        <source>Cities</source>
        <translation>Villes</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2797"/>
        <source>Territory types</source>
        <translation>Types de territoire</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3010"/>
        <source>Addresses</source>
        <translation>Adresses</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3016"/>
        <source>Address types</source>
        <translation>Types d´adresses</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2875"/>
        <location filename="../settings.ui" line="3124"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="214"/>
        <location filename="../settings.cpp" line="2384"/>
        <source>Access Control</source>
        <translation>Contrôle des accès</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3022"/>
        <source>Type number:</source>
        <translation>Numéro du type :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2917"/>
        <location filename="../settings.ui" line="3032"/>
        <location filename="../settings.ui" line="3553"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2986"/>
        <location filename="../settings.ui" line="3042"/>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2927"/>
        <location filename="../settings.ui" line="3052"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3132"/>
        <source>Default address type:</source>
        <translation>Type d&apos;adresse par défaut :</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="196"/>
        <source>Territories</source>
        <translation>Territoires</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="339"/>
        <location filename="../settings.cpp" line="1216"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="542"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="383"/>
        <source>Current Circuit Overseer</source>
        <translation>Responsable de la circonscription</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="586"/>
        <source>Click to edit</source>
        <translation>Cliquer pour modifier</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="979"/>
        <location filename="../settings.ui" line="3507"/>
        <source>Username</source>
        <translation>Nom d’utilisateur</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="989"/>
        <location filename="../settings.ui" line="3424"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="457"/>
        <location filename="../settings.ui" line="1597"/>
        <location filename="../settings.ui" line="1651"/>
        <source>Mo</source>
        <translation>Lun</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="462"/>
        <location filename="../settings.ui" line="1602"/>
        <location filename="../settings.ui" line="1656"/>
        <source>Tu</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="467"/>
        <location filename="../settings.ui" line="1607"/>
        <location filename="../settings.ui" line="1661"/>
        <source>We</source>
        <translation>Mer</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="472"/>
        <location filename="../settings.ui" line="1612"/>
        <location filename="../settings.ui" line="1666"/>
        <source>Th</source>
        <translation>Jeu</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="477"/>
        <location filename="../settings.ui" line="1617"/>
        <location filename="../settings.ui" line="1671"/>
        <source>Fr</source>
        <translation>Ven</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="482"/>
        <location filename="../settings.ui" line="1622"/>
        <location filename="../settings.ui" line="1676"/>
        <source>Sa</source>
        <translation>Sam</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="487"/>
        <location filename="../settings.ui" line="1627"/>
        <location filename="../settings.ui" line="1681"/>
        <source>Su</source>
        <translation>Dim</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1534"/>
        <location filename="../settings.ui" line="1563"/>
        <location filename="../settings.ui" line="1953"/>
        <location filename="../settings.ui" line="2014"/>
        <location filename="../settings.ui" line="2064"/>
        <location filename="../settings.ui" line="2087"/>
        <location filename="../settings.ui" line="2136"/>
        <location filename="../settings.ui" line="2147"/>
        <location filename="../settings.ui" line="2184"/>
        <location filename="../settings.ui" line="2207"/>
        <location filename="../settings.ui" line="2294"/>
        <location filename="../settings.ui" line="2494"/>
        <location filename="../settings.ui" line="2572"/>
        <location filename="../settings.ui" line="2595"/>
        <location filename="../settings.ui" line="2670"/>
        <location filename="../settings.ui" line="2746"/>
        <location filename="../settings.ui" line="2772"/>
        <location filename="../settings.ui" line="2818"/>
        <location filename="../settings.ui" line="2844"/>
        <location filename="../settings.ui" line="2949"/>
        <location filename="../settings.ui" line="2975"/>
        <location filename="../settings.ui" line="3071"/>
        <location filename="../settings.ui" line="3097"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1458"/>
        <source>Circuit Overseer&#39;s visit</source>
        <translation>Visite du responsable de circonscription</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1463"/>
        <source>Convention</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1468"/>
        <source>Memorial</source>
        <translation>Mémorial</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1473"/>
        <source>Zone overseer&#39;s talk</source>
        <translation>Discours du représentant du siège mondial</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>Other exception</source>
        <translation>Autre exception</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1486"/>
        <location filename="../settings.cpp" line="625"/>
        <source>Start date</source>
        <translation>Date de début</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1504"/>
        <location filename="../settings.cpp" line="626"/>
        <source>End date</source>
        <translation>Date de fin</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1592"/>
        <location filename="../settings.ui" line="1646"/>
        <source>No meeting</source>
        <translation>Pas de réunion</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1574"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1797"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>Importer le Cahier pour la réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1725"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>Principal</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1876"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="419"/>
        <location filename="../settings.ui" line="1913"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2039"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>Programme des réunions de semaine pour la sélection ci-dessus</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="205"/>
        <source>Reminders</source>
        <translation>Rappels</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3670"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>Exécuter un fichier fourni par le Support Technique</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1731"/>
        <source>Number of classes</source>
        <translation>Nombre de classes</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2924"/>
        <source>Studies</source>
        <translation>Leçons</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2377"/>
        <location filename="../settings.ui" line="2383"/>
        <source>Add subject one at a time</source>
        <translation>Ajouter un thème à la fois</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2417"/>
        <source>Public talk number</source>
        <translation>Numéro de discours</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2424"/>
        <source>Public talk subject</source>
        <translation>Thème du discours public</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2434"/>
        <location filename="../settings.ui" line="2681"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2466"/>
        <source>Add congregations and speakers</source>
        <translation>Ajouter des assemblées et des orateurs</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="947"/>
        <location filename="../settings.cpp" line="1218"/>
        <location filename="../settings.cpp" line="1288"/>
        <source>Number</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2724"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="567"/>
        <source>Select a backup file</source>
        <translation>Sélectionner un fichier de sauvegarde</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1053"/>
        <location filename="../settings.cpp" line="1373"/>
        <location filename="../settings.cpp" line="1683"/>
        <location filename="../settings.cpp" line="1751"/>
        <location filename="../settings.cpp" line="1835"/>
        <location filename="../settings.cpp" line="1931"/>
        <source>Remove selected row?</source>
        <translation>Supprimer la ligne sélectionnée ?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1147"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Un discours public avec le même numéro est déjà enregistré !
Veux-tu interrompre le discours précédent ?

Les discours planifiés seront déplacés dans la liste &quot;À Faire&quot;.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1289"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1389"/>
        <source>Song number missing</source>
        <translation>Le numéro du cantique n&apos;est pas indiqué</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1392"/>
        <source>Song title missing</source>
        <translation>Il manque le titre du cantique</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1402"/>
        <source>Song is already saved!</source>
        <translation>Le cantique existe déjà!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1411"/>
        <source>Song added to database</source>
        <translation>Le cantique est ajouté dans la base de données</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1451"/>
        <source>City</source>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1487"/>
        <location filename="../settings.cpp" line="1581"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1647"/>
        <source>City name missing</source>
        <translation>Il manque le nom de la ville</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1656"/>
        <source>City is already saved!</source>
        <translation>Ville déjà enregistrée !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1664"/>
        <source>City added to database</source>
        <translation>Ville ajoutée à la base de données</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1715"/>
        <source>Territory type name missing</source>
        <translation>Type de territoire manquant</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1724"/>
        <source>Territory type is already saved!</source>
        <translation>Type de territoire déjà enregistré !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1732"/>
        <source>Territory type added to database</source>
        <translation>Type de territoire ajouté à la base</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1801"/>
        <source>Name of the street type is missing</source>
        <translation>Il manque le nom du type de rue</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1808"/>
        <source>Street type is already saved!</source>
        <translation>Type de rue déjà enregistré !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1816"/>
        <source>Street type added to database</source>
        <translation>Type de rue ajouté à la base de données</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2384"/>
        <source>Remove permissions for the selected user?</source>
        <translation>Supprimer des permissions à l&apos;utilisateur choisi ?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2658"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="948"/>
        <location filename="../settings.cpp" line="2723"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="950"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>Publié le</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="951"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>Arrêté le</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Final Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.cpp" line="1102"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>Interrompre ce discours déplacera les discours programmés avec ce plan dans la liste &quot;À Faire&quot;.

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1219"/>
        <source>Revision</source>
        <translation>Révision</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1531"/>
        <location filename="../settings.cpp" line="1582"/>
        <location filename="../settings.cpp" line="2261"/>
        <location filename="../settings.cpp" line="2317"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1532"/>
        <location filename="../settings.cpp" line="1583"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1888"/>
        <source>Number of address type is missing</source>
        <translation>Il manque le numéro du type d&apos;adresse</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1892"/>
        <source>Name of address type is missing</source>
        <translation>Il manque le nom du type d’adresse</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1901"/>
        <source>Address type is already saved!</source>
        <translation>Type d&apos;adresse déjà enregistré !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1911"/>
        <source>Address type added to database</source>
        <translation>Type d’adresse ajouté dans la base de données</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2024"/>
        <source>Error sending e-mail</source>
        <translation>Erreur lors de l&apos;envoi du courriel</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2114"/>
        <location filename="../settings.cpp" line="2867"/>
        <source>Select ePub file</source>
        <translation>Sélectionner le fichier ePub</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2152"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>Attention : Assure-toi que ce fichier provient d&apos;une source de confiance. Continuer ?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2156"/>
        <source>Command File</source>
        <translation>Fichier de commandes</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2549"/>
        <location filename="../settings.cpp" line="2598"/>
        <source>Meeting</source>
        <translation>Réunion</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2549"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>Supprimer la réunion entière ? (À utiliser uniquement pour supprimer les données non valides de la base de données)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2590"/>
        <source>Enter source material here</source>
        <translation>Entrer les références des matières ici</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2598"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>Supprimer ce discours ? (À utiliser uniquement pour supprimer les données invalides de la base de données)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2659"/>
        <source>Bible Reading</source>
        <translation>Lecture de la Bible</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2660"/>
        <source>Song 1</source>
        <translation>Cantique 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2661"/>
        <source>Song 2</source>
        <translation>Cantique 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2662"/>
        <source>Song 3</source>
        <translation>Cantique 3</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2722"/>
        <source>Meeting Item</source>
        <translation>Partie de la réunion</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2725"/>
        <source>Timing</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2726"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2886"/>
        <source>Study Number</source>
        <translation>Numéro de la leçon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2887"/>
        <source>Study Name</source>
        <translation>Nom de la leçon</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="952"/>
        <location filename="../settings.cpp" line="1220"/>
        <location filename="../settings.cpp" line="1290"/>
        <location filename="../settings.cpp" line="1452"/>
        <location filename="../settings.cpp" line="1488"/>
        <location filename="../settings.cpp" line="1584"/>
        <source>Language id</source>
        <translation>ID de langue</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1119"/>
        <source>Public talk number missing</source>
        <translation>Numéro de discours public manquant</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1122"/>
        <source>Public talk subject missing</source>
        <translation>Thème de discours public manquant</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1143"/>
        <source>Public talk is already saved!</source>
        <translation>Discours public déjà enregistré !</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1177"/>
        <source>Public talk added to database</source>
        <translation>Discours public ajouté à la base de données</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1184"/>
        <location filename="../settings.cpp" line="1418"/>
        <location filename="../settings.cpp" line="1670"/>
        <location filename="../settings.cpp" line="1738"/>
        <location filename="../settings.cpp" line="1822"/>
        <location filename="../settings.cpp" line="1918"/>
        <source>Adding failed</source>
        <translation>L&apos;ajout a échoué</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="624"/>
        <source>Exception</source>
        <translation>Exception</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="627"/>
        <source>Meeting 1</source>
        <translation>Réunion 1</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="628"/>
        <source>Meeting 2</source>
        <translation>Réunion 2</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="585"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Base de données restaurée. Le programme va redémarrer.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2925"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>Supprimer toutes les leçons ? (À utiliser uniquement pour supprimer des données invalides de la base de données)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1215"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1214"/>
        <source>Id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1217"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="549"/>
        <source>Save database</source>
        <translation>Enregistrer la base de données</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="557"/>
        <source>Database backuped</source>
        <translation>Base de données sauvegardées</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/controls/+fusion/Sidebar.qml" line="26"/>
        <location filename="../qml/controls/+material/Sidebar.qml" line="29"/>
        <location filename="../qml/controls/+universal/Sidebar.qml" line="29"/>
        <location filename="../qml/controls/Sidebar.qml" line="26"/>
        <source>Select an assignment on the left to edit</source>
        <translation>Sélectionner une attribution sur la gauche pour la modifier.</translation>
    </message>
</context>
<context>
    <name>SpecialEvents</name>
    <message>
        <location filename="../specialevent.cpp" line="169"/>
        <source>No exception</source>
        <translation>Pas d&apos;exception</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="174"/>
        <source>Circuit overseer&#39;s visit</source>
        <translation>Visite du responsable de circonscription</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="184"/>
        <source>Circuit assembly</source>
        <translation>Assemblée de circonscription</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="194"/>
        <source>Convention</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="203"/>
        <source>Virtual convention</source>
        <comment>Schedule for viewing convention sessions at home</comment>
        <translation>Assemblée virtuelle</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="211"/>
        <source>Memorial</source>
        <translation>Mémorial</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="218"/>
        <source>Visit of headquarters representative</source>
        <translation>Visite d&apos;un représentant du Collège Central</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="225"/>
        <source>Visit of Bethel speaker</source>
        <translation>Visite d&apos;un orateur du Béthel</translation>
    </message>
    <message>
        <location filename="../specialevent.cpp" line="233"/>
        <source>Other exception</source>
        <translation>Autre exception</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="14"/>
        <source>Welcome to theocbase</source>
        <translation>Bienvenue sur theocbase</translation>
    </message>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="38"/>
        <source>Do not show again</source>
        <translation>Ne plus afficher à nouveau</translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="34"/>
        <source>Student Assignment</source>
        <translation>Attribution de l&apos;élève</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="302"/>
        <source>With the same student</source>
        <translation>Avec le même élève</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="352"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="377"/>
        <source>Student</source>
        <translation>Élève</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="507"/>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="632"/>
        <source>Study point</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="656"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="661"/>
        <source>Completed</source>
        <translation>Terminé</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="692"/>
        <source>Volunteer</source>
        <translation>Volontaire</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="841"/>
        <source>Timing</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="866"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="55"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="118"/>
        <source>Street:</source>
        <translation>Rue :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="128"/>
        <source>Postalcode:</source>
        <translation>Code postal :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="150"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="165"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="183"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Département</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="201"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Département</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="77"/>
        <source>Country:</source>
        <translation>Pays :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="87"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>Département :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="97"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>Ville :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="107"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Région :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="138"/>
        <source>No.:</source>
        <translation>N° :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="219"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="236"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>Région</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="254"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>Rue</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="271"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Num.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="288"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>Code Postal</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="305"/>
        <source>Latitude</source>
        <translation>Latitude</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="322"/>
        <source>Longitude</source>
        <translation>Longitude</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="354"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="359"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation>Sélectionner les rues à ajouter au territoire :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="41"/>
        <source>search</source>
        <comment>Search in the list of new street names</comment>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="70"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>Rue</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="88"/>
        <source>Hide already added streets</source>
        <comment>Hide those streets that are already in the territory&apos;s street list</comment>
        <translation>Masquer les rues déjà ajoutées</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="104"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="109"/>
        <source>Cancel</source>
        <translation>Supprimer</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="158"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="200"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>N°</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="246"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="306"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>Indéfini [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="389"/>
        <source>Edit address</source>
        <translation>Modifier l’adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="441"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>Ajouter une adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="442"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>Merci de sélectionner une adresse dans la liste des résultats de la recherche.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="502"/>
        <location filename="../qml/TerritoryAddressList.qml" line="512"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Chercher une adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="503"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>Aucune adresse trouvée.</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="40"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>ID-Territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="82"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>Pays</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="104"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>Département</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="126"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>Département</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="168"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>Région</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="147"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="190"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation>Rue</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="214"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>Num.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="223"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>Code Postal</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="245"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>Coordonnées GPS</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="266"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="276"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="286"/>
        <source>Request date</source>
        <translation>Date de la demande</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="311"/>
        <source>Add new address</source>
        <translation>Ajouter une nouvelle adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="322"/>
        <source>Edit selected address</source>
        <translation>Modifier l&apos;adresse sélectionnée</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="333"/>
        <source>Remove selected address</source>
        <translation>Supprimer l&apos;adresse sélectionnée</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="81"/>
        <source>Filename:</source>
        <translation>Nom de fichier:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="130"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>Correspondance des champs KML</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="59"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>Limites du territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="66"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>Adresses</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="157"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="175"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>Rechercher par la &quot;Description&quot; si un territoire n&apos;est pas trouvé par son &quot;Nom&quot;</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="189"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>Territoire Num.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="193"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>Localité</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="197"/>
        <source>Remark</source>
        <translation>Remarque</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="214"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>Faire correspondre</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="227"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="259"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>Type d&apos;adresse :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="281"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>Liste des adresses invalides :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="351"/>
        <source>Import</source>
        <translation>Importer</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="360"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="128"/>
        <source>Group by:</source>
        <translation>Grouper par :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="137"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>Ville</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="139"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="140"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>Travaillé</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="241"/>
        <source>Add new territory</source>
        <translation>Ajouter un nouveau territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="257"/>
        <source>Remove selected territory</source>
        <translation>Supprimer le territoire sélectionné</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="348"/>
        <source>Remark</source>
        <translation>Remarque</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="359"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="386"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>Ville :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="452"/>
        <source>Assignments</source>
        <translation>Attributions</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="644"/>
        <source>Publisher</source>
        <translation>Proclamateur</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="781"/>
        <source>Streets</source>
        <translation>Rues</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="820"/>
        <source>Addresses</source>
        <translation>Adresses</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="290"/>
        <source>No.:</source>
        <translation>N° :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="301"/>
        <source>Number</source>
        <translation>Numéro</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="313"/>
        <source>Locality:</source>
        <translation>Localité :</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="323"/>
        <source>Locality</source>
        <translation>Localité</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="422"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>Plan</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="618"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="631"/>
        <source>Publisher-ID</source>
        <translation>ID-Proclamateur</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="700"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>Date de sortie</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="707"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>Date de rentrée</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="730"/>
        <source>Add new assignment</source>
        <translation>Ajouter une nouvelle attribution</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="757"/>
        <source>Remove selected assignment</source>
        <translation>Supprimer l&apos;attribution sélectionné</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="398"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>Chercher une adresse</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="851"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation>
            <numerusform>La nouvelle limite chevauche %n territoire :</numerusform>
            <numerusform>La nouvelle limite chevauche %n territoires :</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="853"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &#39;No&#39; if overlapping areas should remain in their territories and to add only the part, that doesn&#39;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation>Veux-tu attribuer des zones de chevauchement au territoire actuel ? 
Sélectionne « Non » si les zones de chevauchement doivent rester dans leur territoire, et pour ajouter seulement la partie qui ne chevauche pas d’autres territoires.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="887"/>
        <source>Join to the selected territory</source>
        <comment>Join two territories into one</comment>
        <translation>Fusionner avec le territoire sélectionné</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="917"/>
        <source>Add address to selected territory</source>
        <translation>Ajouter une adresse au territoire sélectionné</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="926"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>Attribuer au territoire sélectionné</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="935"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>Supprimer l&apos;adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1026"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>%1 sur %2 adresse(s) importée(s).</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1041"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>Importer les limites du territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1051"/>
        <location filename="../qml/TerritoryMap.qml" line="1062"/>
        <location filename="../qml/TerritoryMap.qml" line="1077"/>
        <location filename="../qml/TerritoryMap.qml" line="1086"/>
        <location filename="../qml/TerritoryMap.qml" line="1119"/>
        <location filename="../qml/TerritoryMap.qml" line="1128"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>Importer les données de territoires</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1052"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>%n territoire importé ou mis à jour.</numerusform>
            <numerusform>%n territoires importés ou mis à jour.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1078"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Merci de sectionner les champs adresse et nom.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1087"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>Les champs sélectionnés devraient être différents.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1095"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>Importer les adresses du territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1096"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>Les adresses seront ajoutées au territoire actuel. Merci de sélectionner d&apos;abord un territoire.</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="1120"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>%n adresse importée.</numerusform>
            <numerusform>%n adresses importées.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1130"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>Aucun territoire valide n&apos;est sélectionné.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1132"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>Le fichier d&apos;import ne peut pas être lu.</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1164"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1166"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1195"/>
        <location filename="../qml/TerritoryMap.qml" line="1203"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1196"/>
        <location filename="../qml/TerritoryMap.qml" line="1197"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>Fichiers KML (*.kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1196"/>
        <location filename="../qml/TerritoryMap.qml" line="1204"/>
        <location filename="../qml/TerritoryMap.qml" line="1212"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1204"/>
        <location filename="../qml/TerritoryMap.qml" line="1205"/>
        <location filename="../qml/TerritoryMap.qml" line="1212"/>
        <location filename="../qml/TerritoryMap.qml" line="1213"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>Fichiers CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1204"/>
        <location filename="../qml/TerritoryMap.qml" line="1212"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>Fichiers texte (*.txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1211"/>
        <source>Save file</source>
        <translation>Enregistrer le fichier</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="66"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="153"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>Importer des données</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="197"/>
        <source>Show/hide streets</source>
        <comment>Show/hide streets of territories</comment>
        <translation>Afficher/Masquer les rues</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="209"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>Modifier</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="223"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation>Créer des limites</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="235"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>Supprimer les limites</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="247"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation>Diviser un territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="164"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>Tout voir</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="174"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>Afficher/masquer les territoires</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="185"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>Afficher/masquer les marqueurs</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation>Nom de la rue</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="210"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation>Indéfini [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="278"/>
        <location filename="../qml/TerritoryStreetList.qml" line="289"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation>Ajouter des rues</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="279"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation>Aucune rue trouvée.</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="40"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>ID-Territoire</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation>Nom de la rue</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation>Du numéro</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation>Au numéro</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation>Quantité</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation>Tracé</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="165"/>
        <source>Add new street</source>
        <translation>Ajouter une nouvelle rue</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="176"/>
        <source>Remove selected street</source>
        <translation>Supprimer la rue sélectionnée</translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; 6 mois</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>6 à 12 mois</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>&gt; 12 mois</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>Non travaillé</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>Non attribué</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>territoire</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>territoires</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="128"/>
        <source>To Do List</source>
        <translation>À Faire</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="166"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>Impossible de programmer cette partie à moins de corriger les champs : %1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="192"/>
        <source>Count=%1</source>
        <translation>Nombre=%1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="214"/>
        <source>Settings</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="458"/>
        <source>Edit To Do Item</source>
        <translation>Modifier la tâche à accomplir</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="479"/>
        <source>Meeting day</source>
        <translation>Jour de réunion</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="523"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="630"/>
        <location filename="../qml/TodoPanel.qml" line="857"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="747"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
</context>
<context>
    <name>TodoValidator</name>
    <message>
        <location filename="../todomodel.cpp" line="385"/>
        <source>This item is overdue. Please remove it or choose another date.</source>
        <translation>Cet élément est en retard. Merci de le supprimer ou de choisir une autre date.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="392"/>
        <location filename="../todomodel.cpp" line="400"/>
        <source>This date is already scheduled. Please choose another date.</source>
        <translation>Cette date est déjà programmée. Merci de choisir une autre date.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="414"/>
        <source>This talk has been discontinued. Please choose another talk.</source>
        <translation>Ce discours n&apos;est plus d&apos;actualité. Merci de choisir un autre discours.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="428"/>
        <source>The speaker is no member of this congregation. Please choose another speaker or congregation.</source>
        <translation>L&apos;orateur n&apos;est pas membre de cette congrégation. Merci de choisir un autre orateur ou une autre assemblée.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="441"/>
        <source>This talk cannot be delivered by the speaker. Please choose another talk or speaker.</source>
        <translation>Ce discours ne peut pas être prononcé par l&apos;orateur. Merci de choisir un autre discours ou orateur.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="462"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>La personne assignée n&apos;est pas membre de l&apos;assemblée.</translation>
    </message>
    <message>
        <location filename="../todomodel.cpp" line="469"/>
        <source>Unassignable. Please check the speaker&#39;s settings.</source>
        <translation>Non attribuable. Merci de vérifier les paramètres de l&apos;orateur.</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="13"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="30"/>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="190"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="27"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>Discours de service</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="57"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="82"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="111"/>
        <location filename="../qml/WEMeetingModule.qml" line="131"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="127"/>
        <source>Notes</source>
        <comment>Meeting Notes</comment>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="138"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="161"/>
        <location filename="../qml/WEMeetingModule.qml" line="374"/>
        <source>Song &amp; Prayer</source>
        <translation>Cantique et Prière</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="162"/>
        <location filename="../qml/WEMeetingModule.qml" line="375"/>
        <source>Song %1 &amp; Prayer</source>
        <translation>Cantique %1 et Prière</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="209"/>
        <source>PUBLIC TALK</source>
        <translation>DISCOURS PUBLIC</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="271"/>
        <source>WATCHTOWER STUDY</source>
        <translation>ÉTUDE DE LA TOUR DE GARDE</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="277"/>
        <source>Song %1</source>
        <translation>Cantique %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="302"/>
        <source>Import WT...</source>
        <translation>Importer la TG…</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="330"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="337"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
</context>
<context>
    <name>WEMeetingSongAndPrayerPanel</name>
    <message>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="13"/>
        <source>Song and Prayer</source>
        <translation>Cantique et Prière</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="14"/>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="333"/>
        <source>Song</source>
        <translation>Cantique</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingSongAndPrayerPanel.qml" line="200"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
</context>
<context>
    <name>WEMeetingValidator</name>
    <message>
        <location filename="../cpublictalks.cpp" line="1061"/>
        <source>This talk has been discontinued. Please choose another talk.</source>
        <translation>Ce discours n&apos;est plus d&apos;actualité. Merci de choisir un autre discours.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1071"/>
        <source>The speaker is no member of this congregation. Please choose another speaker or congregation.</source>
        <translation>L&apos;orateur n&apos;est pas membre de cette congrégation. Merci de choisir un autre orateur ou une autre assemblée.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1085"/>
        <source>This talk cannot be delivered by the speaker. Please choose another talk or speaker.</source>
        <translation>Ce discours ne peut pas être prononcé par l&apos;orateur. Merci de choisir un autre discours ou orateur.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1102"/>
        <source>The assigned person is no member of the congregation.</source>
        <translation>La personne assignée n&apos;est pas membre de l&apos;assemblée.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1108"/>
        <source>Unassignable. Please check the publisher&#39;s settings.</source>
        <translation>Non attribuable. Merci de vérifier les paramètres du proclamateur.</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="1127"/>
        <source>The assigned person is not available on this day.</source>
        <translation>La personne assignée n&apos;est pas disponible ce jour.</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="30"/>
        <source>Watchtower Study</source>
        <translation>Étude de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="279"/>
        <source>Watchtower Issue</source>
        <translation>Édition de La Tour de Garde</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="305"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Article</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="332"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="362"/>
        <source>Conductor</source>
        <translation>Conducteur</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="488"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="121"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(Enregistrement manquant)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>Nouvelle version disponible</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>Télécharger</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>Nouvelle version…</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>Pas de mise à jour disponible</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="107"/>
        <source>File reading failed</source>
        <translation>La lecture du fichier a échoué</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="338"/>
        <source>XML file generated in the wrong version.</source>
        <translation>Fichier XML généré dans la mauvaise version.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="406"/>
        <source>Persons - added </source>
        <translation>Personnes - ajoutées </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="463"/>
        <source>Persons - updated </source>
        <translation>Personnes - mises à jour </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="641"/>
        <source>Public talks - theme added </source>
        <translation>Discours publics - thème ajouté </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="699"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>Discours public et TG - programme mis à jour </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="705"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>Discours public et TG - programme ajouté </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="501"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>Ne pas contacter</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1885"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>Enregistrer les adresses invalides</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1886"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>Le fichier est en lecture seule</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>Non défini</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>OK mais JSON indisponible</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>Autorisation en cours…</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>L&apos;autorisation a échouée</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>ID Client Manquant</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>Secret Client Manquant</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>Code d&apos;autorisation requis</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>Une actualisation du jeton est nécessaire. Ceci est lié à la connexion Gmail</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="193"/>
        <source>Number of weeks after selected date</source>
        <translation>Nombre de semaines après la date sélectionnée</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="240"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Nombre de semaines grisées après une attribution</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="214"/>
        <source>weeks</source>
        <translation>semaines</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="106"/>
        <source>Timeline</source>
        <translation>Chronologie</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="177"/>
        <source>Number of weeks before selected date</source>
        <translation>Nombre de semaines avant la date sélectionnée</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="318"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>Proclamateurs</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="516"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="578"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>DP</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="584"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>Pr</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="593"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>TG-L</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="803"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="804"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="805"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="806"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="807"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="808"/>
        <source>Reader</source>
        <translation>Lecteur</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>Next &gt;</source>
        <translation>Suivant &gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>&lt; Back</source>
        <translation>&lt; Retour</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="65"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importation du programme de l&apos;école théocratique. Copier le programme complet depuis la WT Library et le coller ci-dessous (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="66"/>
        <source>Check schedule</source>
        <translation>Vérifier le programme</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="69"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importation des leçons. Copier les leçons depuis WT Library et les coller ci-dessous (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="70"/>
        <source>Check studies</source>
        <translation>Vérifier les leçons</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="73"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>Importation des paramètres. Copier les paramètres depuis la WT Library et les coller ci-dessous (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="74"/>
        <source>Check settings</source>
        <translation>Vérifier les cadres</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="78"/>
        <source>Check subjects</source>
        <translation>Vérifier les thèmes</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="81"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>Ajouter le nom des orateurs et des assemblées. Copier toutes les données et les coller ci-dessous (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="82"/>
        <source>Check data</source>
        <translation>Vérifier les données</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="86"/>
        <source>Check songs</source>
        <translation>Vérifier les cantiques</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="98"/>
        <source>No schedule to import.</source>
        <translation>Pas de programme à importer.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="144"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="145"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>Merci d&apos;ajouter la date de début au format AAAA-MM-JJ (ex: 2019-01-01)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="152"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>La date ne correspond pas au premier jour de la semaine (Lundi)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="206"/>
        <source>Import songs</source>
        <translation>Importer des cantiques</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="248"/>
        <source>Only brothers</source>
        <translation>Seulement des frères</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="477"/>
        <source>Subject</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="521"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="524"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="527"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="530"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="533"/>
        <source>Public talks</source>
        <translation>Discours publics</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="536"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="552"/>
        <location filename="../importwizard.cpp" line="553"/>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="552"/>
        <location filename="../importwizard.cpp" line="553"/>
        <source>Last name</source>
        <translation>Nom de famille</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="181"/>
        <source>Import subjects</source>
        <translation>Importer des thèmes</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="182"/>
        <location filename="../importwizard.cpp" line="207"/>
        <source>Choose language</source>
        <translation>Choisis la langue</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="37"/>
        <source>Save to database</source>
        <translation>Enregistrer dans la base de données</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="77"/>
        <source>Add public talk&#39;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>Ajouter les thèmes de discours publics. Copier/coller les thèmes ci-dessous (Ctrl + V / cmd + V).
Le numéro du discours doit être dans la première colonne, et le thème dans la deuxième.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="85"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>Ajouter des cantiques. Copier toutes les données dans le presse-papiers et coller ci-dessous (Ctrl + V / cmd + V). 
Le numéro doit être dans la première colonne et le thème dans la seconde.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="237"/>
        <source>date</source>
        <translation>date</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="240"/>
        <source>number</source>
        <translation>numéro</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="243"/>
        <source>subject</source>
        <translation>thème</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="246"/>
        <source>material</source>
        <translation>matière</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="357"/>
        <source>setting</source>
        <translation>cadre</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="418"/>
        <source>study</source>
        <translation>leçon</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="604"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="678"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>Un discours public avec le même numéro est déjà enregistré !
Veux-tu interrompre le discours précédent ?

Les discours planifiés seront déplacés dans la liste &quot;À Faire&quot;.</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="681"/>
        <source>Previous talk: </source>
        <translation>Discours précédant : </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="681"/>
        <source>New talk: </source>
        <translation>Nouveau discours : </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="796"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>Thèmes de discours publics non trouvés. Ajouter des thèmes et essayer encore !</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="819"/>
        <source> rows added</source>
        <translation> lignes ajoutées</translation>
    </message>
</context>
<context>
    <name>lmmWorksheetRegEx</name>
    <message>
        <location filename="../lmmworksheetregex.ui" line="203"/>
        <source>Language Options</source>
        <translation>Options de langue</translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="13"/>
        <source>Talk Name in the Workbook</source>
        <translation>Nom du discours/exposé dans le Cahier Vie et ministère</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="15"/>
        <source>Meeting Item</source>
        <translation>Partie de la réunion</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="41"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>Inconnu</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation>Éditeur de type d&apos;attribution</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>Nom d’utilisateur</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="1195"/>
        <source>Sister</source>
        <translation>Sœur</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1188"/>
        <source>Brother</source>
        <translation>Frère</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1204"/>
        <source>Servant</source>
        <translation>Frère nommé</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>Proclamateurs</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="237"/>
        <source>General</source>
        <translation>Principale</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="527"/>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="627"/>
        <source>Public talks</source>
        <translation>Discours publics</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="361"/>
        <location filename="../personsui.ui" line="620"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="604"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="680"/>
        <source>Watchtower reader</source>
        <translation>Lecteur de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1211"/>
        <source>Prayer</source>
        <translation>Prière</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="567"/>
        <source>Cong. Bible Study reader</source>
        <translation>Lecteur de l&apos;EB</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="709"/>
        <source>Meeting for field ministry</source>
        <translation>Réunion pour la prédication</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="986"/>
        <location filename="../personsui.cpp" line="583"/>
        <source>First name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1011"/>
        <location filename="../personsui.cpp" line="582"/>
        <source>Last name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1120"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1173"/>
        <source>E-mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="765"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="890"/>
        <location filename="../personsui.ui" line="913"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="646"/>
        <source>Watchtower Study Conductor</source>
        <translation>Conducteur de la Tour de Garde</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1218"/>
        <source>Family Head</source>
        <translation>Chef de famille</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1225"/>
        <source>Family member linked to</source>
        <translation>Membre de la famille de</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="330"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="486"/>
        <source>Only Auxiliary Classes</source>
        <translation>Salles secondaires uniquement</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="459"/>
        <source>Only Main Class</source>
        <translation>Salle principale uniquement</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1241"/>
        <source>Active</source>
        <translation>Actif</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="380"/>
        <source>Treasures From God&#39;s Word</source>
        <translation>Joyaux de la Parole de Dieu</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="431"/>
        <source>Initial Call</source>
        <translation>Premier contact</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="394"/>
        <source>Bible Reading</source>
        <translation>Lecture de la Bible</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="493"/>
        <source>All Classes</source>
        <translation>Toutes les classes</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="513"/>
        <source>Bible Study</source>
        <translation>Cours biblique</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="560"/>
        <source>Congregation Bible Study</source>
        <translation>Étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="553"/>
        <source>Living as Christians Talks</source>
        <translation>Discours Vie chrétienne</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="506"/>
        <source>Return Visit</source>
        <translation>Nouvelle visite</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="934"/>
        <source>Personal Info</source>
        <translation>Info personnelles</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1248"/>
        <source>Host for Public Speakers</source>
        <translation>Hôte pour les orateurs publics</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1067"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="217"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="387"/>
        <source>Spiritual Gems</source>
        <translation>Perles spirituelles</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="520"/>
        <source>Talk</source>
        <translation>Discours</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="537"/>
        <source>Discussion with Video</source>
        <translation>Discussion avec vidéo</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="721"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="755"/>
        <source>date</source>
        <translation>date</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="760"/>
        <source>no</source>
        <translation>n°</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="770"/>
        <source>Note</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="775"/>
        <source>Time</source>
        <translation>Heure</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="780"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>Avec</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="789"/>
        <source>Unavailable</source>
        <translation>Indisponible</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="823"/>
        <source>Start</source>
        <translation>Début</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="828"/>
        <source>End</source>
        <translation>Fin</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="92"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="137"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="412"/>
        <source>A person with the same name already exists: &#39;%1&#39;. Do you want to change the name?</source>
        <translation>Une personne du même nom existe déjà : &apos;%1&apos;. Veux-tu changer le nom ?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="633"/>
        <source>Remove student?</source>
        <translation>Supprimer l&apos;élève ?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="610"/>
        <source>Remove student</source>
        <translation>Supprimer l&apos;élève</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="435"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 doit prononcer des discours publics ! Ces discours seront 
déplacés dans la liste &quot;À Faire&quot; si tu l’enlèves des orateurs. 
Veux-tu le supprimer de la liste des orateurs ?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="624"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 doit prononcer des discours publics ! Si tu l&apos;effaces de la liste des élèves, 
ces discours seront déplacés dans la liste &quot;À Faire&quot;.</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1009"/>
        <source>Copy to the clipboard</source>
        <translation>Copier dans le presse-papiers</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1012"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="228"/>
        <location filename="../printui.ui" line="312"/>
        <source>Schedule</source>
        <translation>Programme</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="376"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>Liste d&apos;appels et programme d’hospitalité</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="683"/>
        <source>Midweek Meeting Title</source>
        <translation>Titre de la réunion de semaine</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="603"/>
        <source>Weekend Meeting Title</source>
        <translation>Titre de la réunion de week-end</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="693"/>
        <source>Show Section Titles</source>
        <translation>Afficher les titres de section</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="670"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>Titre des Paroles d&apos;introduction</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="251"/>
        <source>Assignment Slips for Assistants</source>
        <translation>Fiches d&apos;attribution pour les interlocuteurs</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="347"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>Programme des orateurs sortants</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="363"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>Attributions des orateurs sortants</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="389"/>
        <source>Talks of Speakers</source>
        <translation>Discours des orateurs</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="507"/>
        <source>Territory Map Card</source>
        <translation>Carte de territoire</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="517"/>
        <source>Map and Addresses Sheets</source>
        <translation>Carte et liste des adresses</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="610"/>
        <source>Concluding Comments Title</source>
        <comment>See S-140</comment>
        <translation>Titre des paroles de conclusion</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="724"/>
        <source>Show public talk revision date</source>
        <translation>Montrer la date de révision du discours public</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="637"/>
        <source>Show duration</source>
        <translation>Afficher la durée</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="617"/>
        <source>Show time</source>
        <translation>Afficher l&apos;heure</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="710"/>
        <source>Show Workbook Issue no.</source>
        <translation>Afficher le cahier n°</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="278"/>
        <source>qrc:/qml/CongregationMap.qml</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="703"/>
        <source>Show Watchtower Issue no.</source>
        <translation>Afficher La Tour de Garde n°</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="717"/>
        <source>Own congregation only</source>
        <translation>Seulement sa propre assemblée</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="868"/>
        <source>Territory number(s)</source>
        <translation>Numéro(s) de territoire</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="875"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>Séparé par des virgules ; appuyer sur Entrée pour actualiser</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="881"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territory Record</source>
        <translation>Relevé des attributions de territoire</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="770"/>
        <source>Template</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="796"/>
        <source>Paper Size</source>
        <translation>Taille du papier</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="858"/>
        <source>Print From Date</source>
        <translation>Imprimer depuis le</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="816"/>
        <source>Print Thru Date</source>
        <translation>Imprimer jusqu&apos;au</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="939"/>
        <source>Printing</source>
        <translation>Imprimer</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="942"/>
        <location filename="../printui.ui" line="977"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.ui" line="241"/>
        <source>Assignment Slips</source>
        <translation>Fiches d&apos;attribution d&apos;exposé</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="630"/>
        <source>Share in Dropbox</source>
        <translation>Partager dans Dropbox</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="271"/>
        <location filename="../printui.ui" line="331"/>
        <source>Worksheets</source>
        <translation>Feuilles de travail</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="530"/>
        <source>Meetings for field ministry</source>
        <translation>Réunions pour la prédication</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="402"/>
        <source>Combination</source>
        <translation>Combinaison</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="559"/>
        <source>Additional Options</source>
        <translation>Options supplémentaires</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="584"/>
        <source>Show Song Titles</source>
        <translation>Voir le titre des cantiques</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="647"/>
        <source>Show Counsel Text</source>
        <translation>Afficher le texte de la technique oratoire</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="453"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>Programme des orateurs sortants</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="261"/>
        <source>Print assigned only</source>
        <translation>Imprimer uniquement les attributions assignées</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="406"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>Copié dans le presse-papier. À coller dans un programme de traitement de texte (Ctrl+V/Cmd+V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="453"/>
        <location filename="../printui.cpp" line="545"/>
        <location filename="../printui.cpp" line="1181"/>
        <source>file created</source>
        <translation>fichier créé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="779"/>
        <source>Concluding Comments</source>
        <translation>Paroles de conclusion</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1016"/>
        <source>Invalid entry, sorry.</source>
        <translation>Entrée invalide, désolé.</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="417"/>
        <location filename="../printui.cpp" line="465"/>
        <location filename="../printui.cpp" line="525"/>
        <source>Save file</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="466"/>
        <source>Territories</source>
        <translation>Territoires</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="389"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>Personnalisé…</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="621"/>
        <source>Converting %1 to JPG file</source>
        <translation>Conversion de %1 en fichier JPG</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="200"/>
        <location filename="../printui.ui" line="440"/>
        <location filename="../printui.cpp" line="770"/>
        <source>Midweek Meeting</source>
        <translation>Réunion de semaine</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1009"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>Nouveau format de papier personnalisé</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1009"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>Format : Largeur x Hauteur. La largeur et la hauteur peuvent être en &apos;mm&apos;. Par exemple : 210mm x 297mm</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="284"/>
        <location filename="../printui.ui" line="427"/>
        <location filename="../printui.cpp" line="771"/>
        <source>Weekend Meeting</source>
        <translation>Réunion de week-end</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="778"/>
        <source>Opening Comments</source>
        <translation>Introduction</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="190"/>
        <source>From %1</source>
        <translation>Depuis le %1</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="219"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>Il y a déjà un discours programmé à la date choisie.</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="220"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>Échanger les discours</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="221"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>Rappels</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="59"/>
        <source>Date range</source>
        <translation>Période</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="132"/>
        <source>Send selected reminders</source>
        <translation>Envoyer les rappels sélectionnés</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="145"/>
        <source>From</source>
        <translation>De</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="158"/>
        <source>To</source>
        <translation>Pour</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="205"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="249"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="254"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="264"/>
        <source>Subject</source>
        <translation>Objet</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="269"/>
        <source>Message</source>
        <translation>Message</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="259"/>
        <source>E-Mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="93"/>
        <source>Email sending...</source>
        <translation>Envoi du courriel…</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="94"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="120"/>
        <source>Error sending e-mail</source>
        <translation>Erreur lors de l&apos;envoi du courriel</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>Président</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>Conseiller-Classe II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>Conseiller-Classe III</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>Prière d&apos;introduction</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>Prière de conclusion</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Annulation - Attribution à la réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="162"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>Attribution à la réunion Vie chrétienne et ministère</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="184"/>
        <source>Assignment</source>
        <translation>Attribution</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="187"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="189"/>
        <source>Source Material</source>
        <translation>Matières</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="206"/>
        <source>Assistant</source>
        <translation>Interlocuteur</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="209"/>
        <source>Study</source>
        <translation>Leçon</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="215"/>
        <source>Main hall</source>
        <translation>Salle principale</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="218"/>
        <source>Auxiliary classroom 1</source>
        <translation>Deuxième salle</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="221"/>
        <source>Auxiliary classroom 2</source>
        <translation>Troisième salle</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="224"/>
        <source>To be given in</source>
        <comment>Refer to main hall or aux. classroom. See S-89</comment>
        <translation>À présenter dans</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="229"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>Lecteur de l&apos;Étude biblique de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>Interlocuteur de %1</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="236"/>
        <source>Cancellation</source>
        <translation>Annulation</translation>
    </message>
</context>
<context>
    <name>sidePanelScripts</name>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="36"/>
        <source>No recent assignment</source>
        <translation>Pas d&apos;attribution récente</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="43"/>
        <source>No weeks idle</source>
        <translation>Pas de semaines d&apos;inactivité</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/sidePanelScripts.js" line="45"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n semaine</numerusform>
            <numerusform>%n semaines</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="46"/>
        <source>%1 or more weeks</source>
        <translation>%1 semaines ou plus</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="61"/>
        <source>A1</source>
        <translation>S2</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="62"/>
        <source>A2</source>
        <translation>S3</translation>
    </message>
    <message>
        <location filename="../qml/sidePanelScripts.js" line="63"/>
        <source>MH</source>
        <translation>SP</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>Assemblées et Orateurs</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="365"/>
        <source>Congregation...</source>
        <translation>Assemblée…</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="336"/>
        <source>Speaker...</source>
        <translation>Orateur…</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1663"/>
        <source>Toggle Talks Editable</source>
        <translation>Passe les Discours en mode Édition</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1611"/>
        <source>Add Multiple Talks</source>
        <translation>Ajouter plusieurs discours</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="71"/>
        <location filename="../speakersui.ui" line="112"/>
        <location filename="../speakersui.ui" line="156"/>
        <location filename="../speakersui.ui" line="222"/>
        <location filename="../speakersui.ui" line="1614"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="459"/>
        <source>Select a Congregation or Speaker</source>
        <translation>Sélectionner une Assemblée ou un Orateur</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="625"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="681"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="740"/>
        <location filename="../speakersui.cpp" line="181"/>
        <source>Circuit</source>
        <translation>Circonscription</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="575"/>
        <location filename="../speakersui.ui" line="1428"/>
        <location filename="../speakersui.cpp" line="189"/>
        <source>Congregation</source>
        <translation>Assemblée</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="68"/>
        <source>Speakers</source>
        <translation>Orateurs</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="109"/>
        <source>Group by congregation</source>
        <translation>Grouper par assemblée</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="153"/>
        <source>Group by circuit</source>
        <translation>Grouper par circonscription</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="197"/>
        <source>Filter</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="219"/>
        <source>Configure Filter</source>
        <translation>Configurer le filtre</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="500"/>
        <source>Congregation Details</source>
        <translation>Détails de l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="758"/>
        <source>Meeting Times</source>
        <translation>Horaires des réunions</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="850"/>
        <location filename="../speakersui.ui" line="1020"/>
        <source>Mo</source>
        <translation>Lun</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="855"/>
        <location filename="../speakersui.ui" line="1025"/>
        <source>Tu</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="860"/>
        <location filename="../speakersui.ui" line="1030"/>
        <source>We</source>
        <translation>Mer</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.ui" line="1035"/>
        <source>Th</source>
        <translation>Jeu</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="870"/>
        <location filename="../speakersui.ui" line="1040"/>
        <source>Fr</source>
        <translation>Ven</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="875"/>
        <location filename="../speakersui.ui" line="1045"/>
        <source>Sa</source>
        <translation>Sam</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="880"/>
        <location filename="../speakersui.ui" line="1050"/>
        <source>Su</source>
        <translation>Dim</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1210"/>
        <source>Personal Info</source>
        <translation>Informations personnelles</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1518"/>
        <location filename="../speakersui.cpp" line="843"/>
        <source>First Name</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1559"/>
        <source>Mobile</source>
        <translation>Portable</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1341"/>
        <location filename="../speakersui.cpp" line="844"/>
        <source>Last Name</source>
        <translation>Nom de famille</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup_2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1316"/>
        <source>Phone</source>
        <translation>Téléphone</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <source>buttonGroup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="1631"/>
        <source>Public Talks</source>
        <translation>Discours Publics</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1400"/>
        <source>E-mail</source>
        <translation>Courriel</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1254"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="197"/>
        <source>Speaker</source>
        <translation>Orateur</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="371"/>
        <location filename="../speakersui.cpp" line="551"/>
        <source>Undefined</source>
        <translation>Indéfini</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="454"/>
        <location filename="../speakersui.cpp" line="459"/>
        <source>%1 Meeting Day/Time</source>
        <translation>Jour et heure de réunion %1</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="584"/>
        <source>A speaker with the same name already exists: &#39;%1&#39;. Do you want to change the name?</source>
        <translation>Un orateur avec le même nom existe déjà : &apos;%1&apos;. Veux-tu changer le nom ?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="716"/>
        <source>The congregation has speakers!</source>
        <translation>L&apos;assemblée a des orateurs !</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="721"/>
        <source>Remove the congregation?</source>
        <translation>Supprimer l&apos;assemblée ?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="767"/>
        <source>Remove the speaker?</source>
        <translation>Supprimer l&apos;orateur ?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="772"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>L&apos;orateur doit prononcer des discours ! Ces discours seront
déplacés dans la liste &quot;À Faire&quot; si tu supprimes l&apos;orateur.</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="836"/>
        <source>Missing Information</source>
        <translation>Information manquante</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="836"/>
        <source>Select congregation first</source>
        <translation>Sélectionner d&apos;abord l&apos;assemblée</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="869"/>
        <source>New Congregation</source>
        <translation>Nouvelle assemblée</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="934"/>
        <source>Add Talks</source>
        <translation>Ajouter des discours</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="934"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>Entrer les numéros des discours séparés par des virgules ou des points</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="970"/>
        <source>Change congregation to &#39;%1&#39;?</source>
        <translation>Changer l&apos;assemblée en &apos;%1&apos; ?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="972"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>L&apos;orateur doit prononcer des discours à l&apos;extérieur. Ces discours seront
déplacés dans la liste &quot;À Faire&quot;, si tu le changes d&apos;assemblée.</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>Page d&apos;accueil</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="245"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation>Conflit de version : Les changements dans le nuage ont été effectués avec une version plus récente !</translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="251"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation>Conflit de version : Les données du nuage doivent être mises à jour avec la même version par un utilisateur autorisé.</translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>Territoires</translation>
    </message>
</context></TS>
