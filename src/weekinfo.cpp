#include "weekinfo.h"

WeekInfo::WeekInfo(QObject *parent)
    : QObject(parent)
{
    c.clearExceptionCache();
    m_specialEvent = SpecialEvent::None;
    m_midweekDay = 0;
    m_weekendDay = 0;
}

QDate WeekInfo::date() const
{
    return m_date;
}

QString WeekInfo::exceptionText() const
{
    return m_exceptionText;
}

SpecialEvent WeekInfo::specialEvent() const
{
    return m_specialEvent;
}

QDate WeekInfo::exceptionStart() const
{
    return m_exceptionStart;
}

QDate WeekInfo::exceptionEnd() const
{
    return m_exceptionEnd;
}

int WeekInfo::midweekDay() const
{
    return m_midweekDay;
}

int WeekInfo::weekendDay() const
{
    return m_weekendDay;
}

void WeekInfo::saveChanges()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql_item s;
    SpecialEventRule *oldSpecialEventRule = c.getSpecialEventRule(m_date);
    SpecialEvents *se = &SpecialEvents::Instance();
    SpecialEventRule *newSpecialEventRule = se->findSpecialEventRule(static_cast<int>(specialEvent()));
    QDate d1;
    QDate d2;
    int id = -1;
    QString oldText = "";

    if (oldSpecialEventRule->id() != SpecialEvent::None) {
        c.getExceptionDates(m_date, d1, d2);
        sql_item q;
        q.insert("date", d1);
        q.insert("date2", d2);
        sql_items oldItems = sql->selectSql("SELECT id, desc FROM exceptions WHERE date = :date AND date2 = :date2 AND active", &q);
        if (oldItems.size() > 0) {
            id = oldItems[0].value("id").toInt();
            oldText = oldItems[0].value("desc").toString();
        }
    }

    if (oldSpecialEventRule->id() == specialEvent() && d1 == exceptionStart() && d2 == exceptionEnd() && oldText == exceptionText()) {
        qDebug() << "no changes!";
    } else {
        if (specialEvent() == SpecialEvent::None) {
            if (id > -1) {
                // remove existing exception
                s.insert("active", 0);
                s.insert("id", id);
                if (sql->execSql("UPDATE exceptions SET active = :active, time_stamp = strftime('%s','now') WHERE id = :id", &s)) {
                    if (oldSpecialEventRule->isCircuitOverseersVisit() && m_date.year() >= 2016) {
                        // Remove LMM_Schedule when the previous exception was None and m_date.year() >= 2016
                        s.clear();
                        s.insert("date", m_date);
                        s.insert("talk_id", MeetingPartClass::toDbTalkId(MeetingPart::Service_Talk));
                        s.insert("active", 0);
                        sql->execSql("update lmm_schedule set active = :active, time_stamp = strftime('%s','now') where "
                                     "date=:date and talk_id=:talk_id",
                                     &s);
                    }
                }
            }
        } else {
            if (!exceptionStart().isValid() || !exceptionEnd().isValid())
                return;

            // Start and end dates
            if (newSpecialEventRule->isConvention()) {
                if (newSpecialEventRule->isSingleDay())
                    setExceptionEnd(exceptionStart()); // Circuit Assembly
                else {
                    // Regional/International Convention
                    setExceptionStart(m_date.addDays(newSpecialEventRule->startDay() - 1));
                    setExceptionEnd(m_date.addDays(newSpecialEventRule->endDay() - 1));
                }
            }

            cpublictalks cp;
            // Circuit overseer's visit
            if (newSpecialEventRule->isCircuitOverseersVisit()) {
                if (m_date.year() >= 2016) {
                    // add co talk for lmm_schedule
                    LMM_Schedule cotalk(MeetingPart::Service_Talk, 0, m_date, "", "", 30, -1);
                    cotalk.setTheme(cotalk.talkName());
                    cotalk.save();

                    // remove counselor2 and 3
                    LMM_Meeting mtg;
                    if (mtg.loadMeeting(m_date)) {
                        mtg.setCounselor2(nullptr);
                        mtg.setCounselor3(nullptr);
                        mtg.save();
                    }
                    // remove assignments from aux. classes
                    sql_item a;
                    a.insert("date", m_date);
                    sql->execSql("UPDATE lmm_assignment SET assignee_id = -1, assistant_id = -1,"
                                 "time_stamp = strftime('%s','now') WHERE date = :date AND classnumber > 1",
                                 &a);

                    // remove CBS conductor and reader
                    a.insert("talk_id", MeetingPartClass::toDbTalkId(MeetingPart::LMM_CBS));
                    int scheduleid = sql->selectScalar("SELECT id FROM lmm_schedule WHERE talk_id = :talk_id AND date = :date", &a, -1).toInt();
                    if (scheduleid > -1) {
                        a.remove("talk_id");
                        a.insert("scheduleid", scheduleid);
                        sql->execSql("UPDATE lmm_assignment SET assignee_id = -1, assistant_id = -1,"
                                     "time_stamp = strftime('%s','now') WHERE date = :date AND lmm_schedule_id = :scheduleid",
                                     &a);
                    }

                    // remove Watchtower reader
                    QSharedPointer<cptmeeting> weekendmeeting(cp.getMeeting(m_date));
                    if (weekendmeeting->id > -1) {
                        weekendmeeting->setWtReader(nullptr);
                        weekendmeeting->save();
                    }
                }
            }

            // Midweek meeting
            if (midweekDay() == 0) {
                // no meeting -> clear midweek meeting data
                sql_items si;
                si = sql->selectSql("lmm_assignment", "date", m_date.toString(Qt::ISODate), "");
                if (!si.empty()) {
                    sql_item i;
                    i.insert("date", m_date);
                    sql->execSql("UPDATE lmm_assignment SET assignee_id = -1, assistant_id = -1, volunteer_id = -1,"
                                 "completed = 0, note = '', timing = '', time_stamp = strftime('%s','now') WHERE date = :date",
                                 &i);
                }
                LMM_Meeting mtg(nullptr);
                if (mtg.loadMeeting(m_date)) {
                    mtg.setChairman(nullptr);
                    mtg.setPrayerBeginning(nullptr);
                    mtg.setPrayerEnd(nullptr);
                    mtg.setCounselor2(nullptr);
                    mtg.setCounselor3(nullptr);
                    mtg.save();
                }
            }

            // Weekend meeting
            if (weekendDay() == 0) {
                // no meeting -> clear weekend meeting data
                QSharedPointer<cptmeeting> weekendmeeting(cp.getMeeting(m_date));
                if (weekendmeeting->id > -1) {
                    weekendmeeting->setChairman(nullptr);
                    weekendmeeting->setTheme(-1);
                    weekendmeeting->setSpeaker(nullptr);
                    weekendmeeting->setSongTalk(0);
                    weekendmeeting->setHospitalityhost(nullptr);
                    weekendmeeting->setWtConductor(nullptr);
                    weekendmeeting->setWtReader(nullptr);
                    weekendmeeting->save();
                }
            }

            s.clear();
            s.insert("type", static_cast<int>(specialEvent()));
            s.insert("active", 1);
            s.insert("date", exceptionStart());
            s.insert("date2", exceptionEnd());
            s.insert("desc", newSpecialEventRule->canChangeDescription() ? exceptionText() : "");
            s.insert("schoolday", midweekDay());
            s.insert("publicmeetingday", weekendDay());

            if (id < 0) {
                // Create new
                sql->insertSql("exceptions", &s, "id");
            } else {
                // Update existing
                s.insert("id", id);
                sql->updateSql("exceptions", "id", QString::number(id), &s);
            }
        }
        c.clearExceptionCache();
        loadCurrentWeek();
    }
}

void WeekInfo::load()
{
    c.clearExceptionCache();
    loadCurrentWeek();
}

QString WeekInfo::exceptionDisplayText() const
{
    return m_exceptionDisplayText;
}

void WeekInfo::setDate(QDate date)
{
    if (m_date == date)
        return;

    m_date = date;
    emit dateChanged(m_date);
}

void WeekInfo::setExceptionText(QString exceptionText)
{
    if (m_exceptionText == exceptionText)
        return;

    m_exceptionText = exceptionText;
    emit exceptionTextChanged(m_exceptionText);
}

void WeekInfo::setSpecialEvent(SpecialEvent specialEvent)
{
    if (m_specialEvent == specialEvent)
        return;

    m_specialEvent = specialEvent;
    emit specialEventChanged(m_specialEvent);
}

void WeekInfo::setExceptionStart(QDate exceptionStart)
{
    if (m_exceptionStart == exceptionStart)
        return;

    m_exceptionStart = exceptionStart;
    emit exceptionStartChanged(m_exceptionStart);
}

void WeekInfo::setExceptionEnd(QDate exceptionEnd)
{
    if (m_exceptionEnd == exceptionEnd)
        return;

    m_exceptionEnd = exceptionEnd;
    emit exceptionEndChanged(m_exceptionEnd);
}

void WeekInfo::setMidweekDay(int midweekDay)
{
    if (m_midweekDay == midweekDay)
        return;

    m_midweekDay = midweekDay;
    emit midweekDayChanged(m_midweekDay);
}

void WeekInfo::setWeekendDay(int weekendDay)
{
    if (m_weekendDay == weekendDay)
        return;

    m_weekendDay = weekendDay;
    emit weekendDayChanged(m_weekendDay);
}

void WeekInfo::loadCurrentWeek()
{
    QDate d1;
    QDate d2;
    SpecialEventRule *specialEventRule(c.getSpecialEventRule(m_date));
    QString text(c.getExceptionText(m_date, false));
    QString exceptionDisplayText(text);
    if (specialEventRule->id() != SpecialEvent::None)
        c.getExceptionDates(m_date, d1, d2);
    if (specialEventRule->canChangeDescription() && exceptionDisplayText.isEmpty())
        exceptionDisplayText = specialEventRule->description();
    if (exceptionDisplayText != m_exceptionDisplayText) {
        m_exceptionDisplayText = exceptionDisplayText;
        emit exceptionDisplayTextChanged(m_exceptionDisplayText);
    }
    setExceptionText(text);
    setSpecialEvent(specialEventRule->id());
    setExceptionStart(d1);
    setExceptionEnd(d2);
    setMidweekDay(c.getMeetingDay(m_date, ccongregation::meetings::tms));
    setWeekendDay(c.getMeetingDay(m_date, ccongregation::meetings::pm));
}
