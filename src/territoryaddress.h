﻿/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2017, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TERRITORYADDRESS_H
#define TERRITORYADDRESS_H

#include <QObject>
#include <QAbstractTableModel>
#include "sql_class.h"
#include <QSortFilterProxyModel>

class TerritoryAddress : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id NOTIFY notification)
    Q_PROPERTY(int territoryId READ territoryId NOTIFY notification)
    Q_PROPERTY(QString wktGeometry READ wktGeometry NOTIFY notification)

public:
    explicit TerritoryAddress(QObject *parent = nullptr);
    TerritoryAddress(const int territoryId, QObject *parent = nullptr);
    TerritoryAddress(const int territoryId, const QString country,
                     const QString state, const QString county, const QString city,
                     const QString district, const QString street, const QString houseNumber,
                     const QString postalCode, const QString wktGeometry, QObject *parent = nullptr);
    TerritoryAddress(const int id, const int territoryId, const QString country,
                     const QString state, const QString county, const QString city,
                     const QString district, const QString street, const QString houseNumber,
                     const QString postalCode, const QString wktGeometry, const QString name,
                     const int addressTypeNumber, const QDate requestDate, QObject *parent = nullptr);
    ~TerritoryAddress();

    int id() const;
    int territoryId() const;
    void setTerritoryId(const int value);
    QString country() const;
    void setCountry(const QString value);
    QString state() const;
    void setState(const QString value);
    QString county() const;
    void setCounty(const QString value);
    QString city() const;
    void setCity(const QString value);
    QString district() const;
    void setDistrict(const QString value);
    QString street() const;
    void setStreet(const QString value);
    QString houseNumber() const;
    void setHouseNumber(const QString value);
    QString postalCode() const;
    void setPostalCode(const QString value);
    QString wktGeometry() const;
    void setWktGeometry(const QString value);
    double latitude() const;
    double longitude() const;
    QString name() const;
    void setName(const QString value);
    int addressTypeNumber() const;
    void setAddressTypeNumber(const int value);
    const QDate &requestDate() const;
    void setRequestDate(const QDate &newRequestDate);
    bool isDirty() const { return m_isDirty; }
    void setIsDirty(const bool value);
    bool save();

private:
    int m_id;
    int m_territoryId;
    QString m_country;
    QString m_state;
    QString m_county;
    QString m_city;
    QString m_district;
    QString m_street;
    QString m_houseNumber;
    QString m_postalCode;
    QString m_wktGeometry;
    QString m_name;
    int m_addressTypeNumber;
    QDate m_requestDate;
    bool m_isDirty;
    double m_latitude;
    double m_longitude;

    void setLatLon();

signals:
    void notification();
    void requestDateChanged();
};

class TerritoryAddressModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(TerritoryAddress *selectedAddress READ selectedAddress NOTIFY notification)

public:
    enum Roles {
        AddressIdRole = Qt::UserRole,
        TerritoryIdRole,
        CountryRole,
        StateRole,
        CountyRole,
        CityRole,
        DistrictRole,
        StreetRole,
        HouseNumberRole,
        PostalCodeRole,
        WktGeometryRole,
        NameRole,
        AddressTypeNumberRole,
        RequestDateRole,
        LatitudeRole,
        LongitudeRole
    };

    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    TerritoryAddressModel();
    TerritoryAddressModel(QObject *parent);
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QVariantMap get(int row);
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
    Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QModelIndex getAddressIndex(int addressId) const;
    int addAddress(TerritoryAddress *territoryAddress);
    Q_INVOKABLE int addAddress(int territoryId, const QString country, const QString state, const QString county,
                               const QString city, const QString district, const QString street,
                               const QString houseNumber, const QString postalCode, QString wktGeometry);
    Q_INVOKABLE void removeAddress(int row);
    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) Q_DECL_OVERRIDE;
    Q_INVOKABLE void loadAddresses(int territoryId = 0);
    Q_INVOKABLE void saveAddresses();
    Q_INVOKABLE bool updateAddress(int addressId, QString wktGeometry);
    Q_INVOKABLE bool updateAddress(int addressId, int territoryId);
    Q_INVOKABLE bool updateAddress(int addressId, int territoryId, const QString country, const QString state, const QString county,
                                   const QString city, const QString district, const QString street,
                                   const QString houseNumber, const QString postalCode, QString wktGeometry);

    TerritoryAddress *selectedAddress() const { return m_selectedAddress; }
    Q_INVOKABLE void setSelectedAddress(const QModelIndex &selectedIndex = QModelIndex());

    Q_INVOKABLE double getMarkerScale();
    Q_INVOKABLE int getDefaultAddressTypeNumber();

private:
    TerritoryAddress *getItem(const QModelIndex &index) const;

    QList<TerritoryAddress *> territoryAddresses;
    TerritoryAddress *m_selectedAddress;

signals:
    void notification();
    void editCompleted();
    void selectedChanged(const QModelIndex &value = QModelIndex());
};

class TerritoryAddressSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

public:
    TerritoryAddressSortFilterProxyModel(QObject *parent = nullptr);

    int filterTerritoryId() { return m_territoryId; }
    Q_INVOKABLE void setFilterTerritoryId(int value);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

private:
    int m_territoryId;
};

#endif // TERRITORYADDRESS_H
