/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef OUTGOINGSPEAKERSMODEL_H
#define OUTGOINGSPEAKERSMODEL_H

#include <QAbstractTableModel>
#include <QPushButton>
#include "cpublictalks.h"
#include "todo.h"
#include "availability/weekendmeetingavailabilitychecker.h"

class OutgoingSpeakersModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
public:
    enum Roles {
        None = 0,
        IdRole = Qt::UserRole,
        CongregationRole,
        CongregationIdRole,
        CongregationAddress,
        CongregationInfo,
        CongregationCircuit,
        SpeakerRole,
        SpeakerIdRole,
        TimeRole,
        ThemeRole,
        ThemeNoRole,
        ThemeIdRole,
        DateRole,
    };
    Q_ENUM(Roles)
    explicit OutgoingSpeakersModel(QObject *parent = nullptr);
    ~OutgoingSpeakersModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role) const;

    Q_INVOKABLE QVariantMap get(int row) const;

    Q_INVOKABLE void loadList(const QDate d);

    Q_INVOKABLE void addRow(const int speakerId, const int congId, const int themeId);

    Q_INVOKABLE void removeRow(const int rowIndex);

    Q_INVOKABLE void editRow(const int row, const int speakerId, const int themeId, const int congId);

    Q_INVOKABLE bool moveToWeek(const int row, const QDateTime selectedDate);
    Q_INVOKABLE bool moveToTodo(const int row);
signals:
    void modelChanged();
    void movedTodoList();
    void movedToWeek();

private:
    QDate _date;
    QList<cpoutgoing *> _list;
};

class OutgoingSpeakerValidator : public QValidator
{
    Q_OBJECT
    Q_PROPERTY(OutgoingSpeakersModel *model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(int row READ row WRITE setRow NOTIFY rowChanged)
    Q_PROPERTY(OutgoingSpeakersModel::Roles role READ role WRITE setRole NOTIFY roleChanged)
public:
    OutgoingSpeakerValidator(QObject *parent = nullptr);
    ~OutgoingSpeakerValidator();

    QValidator::State validate(QString &input, int &pos) const override;

    OutgoingSpeakersModel *model() const;
    void setModel(OutgoingSpeakersModel *newModel);

    int row() const;
    void setRow(int newRow);

    OutgoingSpeakersModel::Roles role() const;
    void setRole(OutgoingSpeakersModel::Roles newRole);

Q_SIGNALS:
    void modelChanged();
    void rowChanged();
    void errorChanged(const QString &error) const;
    void roleChanged();

private:
    OutgoingSpeakersModel *m_model;
    int m_row;
    OutgoingSpeakersModel::Roles m_role;
};
#endif // OUTGOINGSPEAKERSMODEL_H
