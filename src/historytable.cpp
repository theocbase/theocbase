/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2015, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "historytable.h"
#include "ui_historytable.h"
#include "lmm_meeting.h"

#include <QTextDocument>
#include <QTextTable>
#include <QToolTip>
#include <QBitmap>
//#include <QDebug>

enum DataInfo {
    UserID = Qt::UserRole,
    Sorting = Qt::UserRole + 1,
    AssignMask = Qt::UserRole + 2
};

//------------------------------------------------------------------------------

// In order to sort in a better way
class HistoryTableWidgetItem : public QTableWidgetItem
{
public:
    HistoryTableWidgetItem()
        : QTableWidgetItem(QTableWidgetItem::UserType) { }
    HistoryTableWidgetItem(QString const &text)
        : QTableWidgetItem(text, QTableWidgetItem::UserType) { }

    bool operator<(const QTableWidgetItem &other) const
    {
        // Retreive last assignment of current row (starting from current column)
        bool ok = false;
        int col = this->column() + 1;
        size_t value = 0;
        while (--col >= 0 && !ok) {
            auto item = this->tableWidget()->item(row(), col);
            if (item)
                value = item->data(DataInfo::Sorting).toInt(&ok);
        }
        if (!ok)
            value = std::numeric_limits<size_t>::max();

        // Retreive last assignment of other row (starting from item column)
        bool other_ok = false;
        int other_col = other.column() + 1;
        size_t other_value = 0;
        while (--other_col >= 0 && !other_ok) {
            auto item = other.tableWidget()->item(other.row(), other_col);
            if (item)
                other_value = item->data(DataInfo::Sorting).toInt(&other_ok);
        }
        if (!other_ok)
            other_value = std::numeric_limits<size_t>::max();

        // now compare properly
        if (col > other_col)
            return true;
        if (col < other_col)
            return false;

        // in case we compare same school assignement
        // coming from different school classes:
        //   just compare the text (as default)
        if (value == other_value)
            return this->QTableWidgetItem::operator<(other);

        return value < other_value;
    }
};

//------------------------------------------------------------------------------

historytable::historytable(QWidget *parent)
    : QDialog(parent), ui(new Ui::historytable)
{
    sql = &Singleton<sql_class>::Instance();
    tbStyle = &TBStyle::Instance();
    assignmentInfos = &AssignmentInfos::Instance();
    ui->setupUi(this);

    setStyleSheet("QDialog{ background-color: palette(base); }");

    general::changeButtonIconColor(QList<QAbstractButton *>({ ui->toolButton, ui->btn_close }));

    changeFilterButtonColor(ui->btn_filter_sg, tbStyle->lmmSection1TextColor());
    changeFilterButtonColor(ui->btn_filter_am, tbStyle->lmmSection2TextColor());
    changeFilterButtonColor(ui->btn_filter_cl, tbStyle->lmmSection3TextColor());
    changeFilterButtonColor(ui->btn_filter_pt, tbStyle->publicTalkTextColor());
    changeFilterButtonColor(ui->btn_filter_wt, tbStyle->watchtowerStudyTextColor());

    QSettings settings;
    ui->spinBoxGreyPeriod->setValue(settings.value("historytable/grey", 4).toInt());

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    // QObject::connect(ui->btn_filter_sg, &QToolButton::toggled, [this](bool){this->refreshHistory(); });
    setWindowFlags(windowFlags() & ~Qt::Dialog);
    on_btn_close_clicked();
    setMouseTracking(true);
    ui->frameButtons->setMouseTracking(true);
}

historytable::~historytable()
{
    delete ui;
}

// column name reminder: (0) date (1) num (2) source (3) note (4) time (5) assistant (6) SettingNum (7) SettingDesc
Array2<FormatedString> getSchoolHistory(int assigneeID, QColor defaultFG, QColor defaultBG, bool includeAsAssistant)
{
    Array2<FormatedString> tableWidgetSchoolTask;

    QString abcd = "ABCD";
    auto cp = std::make_shared<cpersons>();
    tableWidgetSchoolTask.setRowCount(0);
    //tableWidgetSchoolTask.setColumnWidth(1,30);
    //tableWidgetSchoolTask.setColumnWidth(4,60);
    //m_ui->tableWidget_2->setRowCount(0);
    //m_ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);

    auto assigneeIDstr = QVariant(assigneeID).toString();
    int iLine = 0;

    //
    // LMM history (meeting form started 2016-01-01)
    //
    ccongregation c;
    auto sql = &Singleton<sql_class>::Instance();
    sql_items tasks = sql->selectSql("SELECT lmm_assignment.*, lmm_schedule.id as schedule_id, lmm_schedule.theme, lmm_schedule.study_number, lmm_schedule.talk_id"
                                     " FROM lmm_assignment LEFT JOIN lmm_schedule ON lmm_assignment.lmm_schedule_id = lmm_schedule.id"
                                     " WHERE (assignee_id = "
                                     + assigneeIDstr + " OR volunteer_id = " + assigneeIDstr + (includeAsAssistant ? " or assistant_id = " + assigneeIDstr + " )" : ")") + " AND schedule_id IS NOT NULL AND lmm_assignment.date >= '2016-01-01'" + " ORDER BY date DESC");
    if (!tasks.empty()) {
        tableWidgetSchoolTask.setRowCount(static_cast<int>(tasks.size()));
        for (unsigned int i = 0; i < tasks.size(); ++i, ++iLine) {
            sql_item task = tasks[i];

            bool assignee = task.value("assignee_id").toInt() == assigneeID;
            bool wasAssistant = task.value("assistant_id").toInt() == assigneeID;
            bool cancelled = task.value("volunteer_id").toInt() > 0;
            bool volunteer = cancelled && !assignee && !wasAssistant;
            bool completed = task.value("completed").toInt() != 0; // check box indicating that correction is filled (note: .toBool() does'nt work)

            // date
            QDate tempdate = task.value("date").toDate();
            int daynumber = c.getMeetingDay(MeetingType::MidweekMeeting, tempdate);
            tableWidgetSchoolTask.setItem(iLine, 0, tempdate.addDays(daynumber - 1).toString(Qt::ISODate));

            MeetingPart meetingPart;
            int sequence(0);
            int dbTalkID(task.value("talk_id").toInt());
            LMM_Schedule::splitDbTalkId(dbTalkID, meetingPart, sequence);
            AssignmentInfos *assignmentInfos = &AssignmentInfos::Instance();
            AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart);
            if (dbTalkID <= 0 || !assignmentInfo)
                continue;

            QString text = LMM_historyTableAbbreviation(meetingPart, wasAssistant);
            int cls(task.value("classnumber").toInt());
            if (cls > 1)
                text += abcd.mid(cls - 1, 1);
            tableWidgetSchoolTask.setItem(iLine, 1, text);
            tableWidgetSchoolTask.setItem(iLine, 2, task.value("theme").toString());

            // Retrieve study  for !!!!!!!!!!!!
            QString study;

            bool canCounsel(assignmentInfo->canCounsel());
            if (canCounsel && !wasAssistant) {
                study = QString("[th #%1]").arg(task.value("study_number").toString());
            }

            // coloring
            QColor fg = LMM_foregroundColor(meetingPart, defaultFG);
            tableWidgetSchoolTask.item(iLine, 0).setForeground(fg);

            QColor bg = LMM_backgroundColor(meetingPart, defaultBG);
            tableWidgetSchoolTask.item(iLine, 0).setBackground(bg);

            // status
            if (!cancelled) {
                tableWidgetSchoolTask.setItem(iLine, 3, study + task.value("note").toString());
                tableWidgetSchoolTask.setItem(iLine, 4, task.value("timing").toString());

                person *partner = cp->getPerson(task.value(wasAssistant ? "assignee_id" : "assistant_id").toInt());
                if (partner) {
                    tableWidgetSchoolTask.setItem(iLine, 5, partner->fullname());
                    tableWidgetSchoolTask.item(iLine, 5).setIconName(wasAssistant ? ":/icons/assigned_assistant.svg" : ":/icons/assigned_student.svg");
                }
            } else {
                tableWidgetSchoolTask.item(iLine, 0).setStrikeOut(true);
                tableWidgetSchoolTask.item(iLine, 1).setStrikeOut(true);
                tableWidgetSchoolTask.item(iLine, 2).setStrikeOut(true);
            }

            auto setting = task.value("setting");
            if (!setting.isNull()) {
                //tableWidgetSchoolTask.setItem(iLine, 6, QVariant(setting).toString()); // was number
                tableWidgetSchoolTask.setItem(iLine, 7, setting.toString()); // is now just free text
            }

            if (canCounsel) {
                if (volunteer) {
                    tableWidgetSchoolTask.item(iLine, 0).setItalic(true);
                    tableWidgetSchoolTask.item(iLine, 1).setItalic(true);
                    tableWidgetSchoolTask.item(iLine, 2).setItalic(true);
                } else if (!completed) //task.value("setting").toString().isEmpty())
                {
                    tableWidgetSchoolTask.item(iLine, 0).setBold(true);
                    tableWidgetSchoolTask.item(iLine, 1).setBold(true);
                    tableWidgetSchoolTask.item(iLine, 2).setBold(true);
                }
            }
        }
    }

    tableWidgetSchoolTask.sortItems(0, Qt::DescendingOrder);
    //tableWidgetSchoolTask.resizeColumnToContents(0); // date
    //tableWidgetSchoolTask.resizeColumnToContents(1); // what
    //tableWidgetSchoolTask.resizeColumnToContents(4); // time
    //tableWidgetSchoolTask.resizeColumnToContents(5); // assistant

    return tableWidgetSchoolTask;
}

void historytable::refreshHistory(bool publicmeeting)
{
    ui->tableWidget->clear();
    showpublictalk = publicmeeting;
    ui->frame->setVisible(!showpublictalk);
    ui->btn_filter_sg->setVisible(!showpublictalk);
    ui->btn_filter_am->setVisible(!showpublictalk);
    ui->btn_filter_cl->setVisible(!showpublictalk);
    ui->btn_filter_pt->setVisible(!showpublictalk);
    ui->btn_filter_wt->setVisible(!showpublictalk);
    ui->spinBoxGreyPeriod->setVisible(!showpublictalk);
    if (publicmeeting) {
        getPublicMeetingHistory();
        return;
    }
    ui->tableWidget->setSortingEnabled(false);

    cpersons pc;
    QList<person *> users = pc.getAllPersons(0);

    LMM_Meeting lmm_meeting;
    int school_qty = lmm_meeting.classes();
    int iweeks_past = ui->spinBox_past->value() + ui->spinBoxGreyPeriod->value();
    int iweeks_future = ui->spinBox_future->value();
    int iweeks = iweeks_past + iweeks_future;

    int ensure_full_sort = 0;
    if (iweeks < users.size()) {
        ensure_full_sort = users.size() - iweeks;
        iweeks_past += ensure_full_sort;
        iweeks += ensure_full_sort;
    }

    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->setRowCount(0);
    ui->tableWidget->setColumnCount(iweeks + 1); // +1 for publisher column

    // Hide weeks before the period required by the user
    // these are there so we can 'grey' correctly.
    for (int i = 1; i < ui->tableWidget->columnCount(); i++) {
        ui->tableWidget->setColumnHidden(i, (i <= (ensure_full_sort + ui->spinBoxGreyPeriod->value() - 1)));
    }

    // filtering publisher depending on the filters
    bool wantSpiritualGem = !ui->btn_filter_sg->isChecked();
    bool wantApplyToMinistry = !ui->btn_filter_am->isChecked();
    bool wantChristianLife = !ui->btn_filter_cl->isChecked();
    bool wantPublicTalk = !ui->btn_filter_pt->isChecked();
    bool wantWatchtower = !ui->btn_filter_wt->isChecked();

    bool wantMidweek = wantSpiritualGem || wantApplyToMinistry || wantChristianLife;
    bool wantWeekend = wantPublicTalk || wantWatchtower;
    int values = 0;
    if (!wantSpiritualGem || !wantApplyToMinistry || !wantChristianLife
        || !wantPublicTalk || !wantPublicTalk) {
        // something is disabled, so had to filter publisher
        if (wantSpiritualGem) // cbs 1
            values |= person::CBSConductor + person::CBSReader
                    + person::LMM_Chairman + person::LMM_Treasures + person::LMM_Digging + person::LMM_BibleReading;

        if (wantApplyToMinistry) // LLM Apply to Ministry
            values |= person::LMM_PreparePresentations + person::LMM_InitialCall + person::LMM_ReturnVisit + person::LMM_BibleStudy;

        if (wantChristianLife) // LMM Living as Christians
            values |= person::LMM_Chairman + person::LMM_LivingTalks;

        if (wantPublicTalk || wantWatchtower)
            values |= person::Chairman + person::PublicTalk + person::WtCondoctor + person::WtReader;

        // other person type person::IsBreak  person::FieldMinistry   person::Assistant   person::SchoolMain    person::SchoolAux
    }
    qDebug() << values;

    // loop all publishers
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Publishers", "History Table (column title)")));
    for (int i = 0; i < users.size(); i++) {
        person *p = users[i];
        if (values > 0) {
            if (!(values & p->usefor())) {
                // continue for to the next publishers
                continue;
            }
        }

        // add new row, and change standard height
        auto row = ui->tableWidget->rowCount();
        ui->tableWidget->setRowCount(row + 1);
        ui->tableWidget->setRowHeight(row, ui->tableWidget->rowHeight(row) * 0.8);

        auto item = new QTableWidgetItem(p->fullname("LastName, FirstName"));
        item->setData(DataInfo::UserID, p->id());
        if (p->usefor() & person::IsBreak) {
            //QFont font(item->font());
            //font.setStrikeOut(true);
            //item->setFont(font);
            QBrush brush = item->foreground();
            brush.setColor(Qt::gray);
            item->setForeground(brush);
        } else if (p->servant()) {
            QFont font(item->font());
            font.setBold(true);
            item->setFont(font);
            //QBrush brush = item->foreground();
            //brush.setColor(Qt::gray);
            //item->setForeground(brush);
        }
        //ui->tableWidget->setVerticalHeaderItem(ui->tableWidget->rowCount()-1, item);
        //item->setBackgroundColor(ui->tableWidget->horizontalHeaderItem(0)->backgroundColor());

        auto tableWidgetSchoolTask = getSchoolHistory(p->id(), palette().text().color(), palette().base().color());
        int rows = std::min(10, tableWidgetSchoolTask.rowCount()) + 1;
        QTextDocument doc;
        QTextCursor cursor(&doc);
        cursor.movePosition(QTextCursor::Start);
        cursor.insertText(item->text()); // publisher full name

        QTextTableFormat format;
        format.setBorder(1);
        format.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
        format.setCellSpacing(0);
        format.setCellPadding(2);
        format.setMargin(0);
        QTextTable *table = cursor.insertTable(rows, 7, format);
        table->cellAt(0, 0).firstCursorPosition().insertText(QObject::tr("Date"));
        table->cellAt(0, 1).firstCursorPosition().insertText(QObject::tr("#"));
        table->cellAt(0, 2).firstCursorPosition().insertText(QObject::tr("Assignment"));
        table->cellAt(0, 3).firstCursorPosition().insertText(QObject::tr("Note"));
        table->cellAt(0, 4).firstCursorPosition().insertText(QObject::tr("Time"));
        table->cellAt(0, 5).firstCursorPosition().insertText(" ");
        table->cellAt(0, 6).firstCursorPosition().insertText(QObject::tr("Together", "The column header text to show partner in student assignment"));

        if (tableWidgetSchoolTask.rowCount()) {
            for (int r = 1; r < rows; ++r) {
                for (int c = 0; c < 6; ++c) {
                    auto item = tableWidgetSchoolTask.item(r - 1, c);
                    QTextCharFormat format;

                    QFont font;
                    font.setItalic(item.italic());
                    font.setBold(item.bold());
                    font.setStrikeOut(item.strikeOut());
                    format.setFont(font);

                    if (item.customBackground())
                        format.setBackground(item.background());
                    if (item.customForeground())
                        format.setForeground(item.foreground());

                    if (!item.iconName().isEmpty()) {
                        table->cellAt(r, c).firstCursorPosition().insertImage(item.iconName());
                        c++;
                    }

                    table->cellAt(r, c).firstCursorPosition().insertText(item.text(), format);
                    table->cellAt(r, c).setFormat(format);
                }
            }
        }
        item->setToolTip(doc.toHtml());
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, item);
    }

    QDate first_date = firstdayofweek.addDays(-7 * (iweeks_past - 1));
    QDate last_date = firstdayofweek.addDays(7 * (iweeks_future));

    sql_items lmm;
    if (wantMidweek)
        lmm = sql->selectSql("SELECT lmm_assignment.*, lmm_schedule.talk_id from lmm_assignment "
                             "LEFT JOIN lmm_schedule ON lmm_assignment.lmm_schedule_id = lmm_schedule.id "
                             "WHERE talk_id IS NOT NULL AND lmm_assignment.date >= '"
                             + first_date.toString(Qt::ISODate) + "' AND lmm_assignment.date <= '" + last_date.toString(Qt::ISODate) + "'");

    sql_items lmm_chairman_and_prayers;
    if (wantMidweek) // ui->comboBox->currentIndex() == 2)
        lmm_chairman_and_prayers = sql->selectSql("SELECT * from lmm_meeting WHERE date >= '" + first_date.toString(Qt::ISODate) + "' AND date <= '" + last_date.toString(Qt::ISODate) + "'");

    sql_items weekend;
    if (wantWeekend)
        weekend = sql->selectSql("SELECT DISTINCT * from publicmeetinghistory WHERE weekof >= '" + first_date.toString(Qt::ISODate) + "' AND weekof <= '" + last_date.toString(Qt::ISODate) + "'");

    sql_items e1 = sql->selectSql("SELECT * from unavailables WHERE ((start_date >= '" + first_date.toString(Qt::ISODate) + "' AND start_date <= '" + last_date.addDays(6).toString(Qt::ISODate) + "') OR (end_date <= '" + last_date.addDays(6).toString(Qt::ISODate) + "' AND end_date >= '" + first_date.toString(Qt::ISODate) + "')) AND active");

    QColor colorBase(palette().base().color());
    QColor colorAlternate(palette().alternateBase().color());
    QColor headerColor(colorBase);
    QColor colorFG = QTableWidgetItem().foreground().color();
    QColor colorBG = QTableWidgetItem().background().color();

    QDate tempdate = first_date;
    int month = tempdate.month();

    for (int w = 1; w <= iweeks; w++) {
        int daynumber = c.getMeetingDay(MeetingType::MidweekMeeting, tempdate);

        QDate meetingday = tempdate.addDays(daynumber - 1);
        if (daynumber == 0) {
            // no meeting
            meetingday = tempdate;
            for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
                auto *it = new HistoryTableWidgetItem();
                it->setBackground(QBrush(colorAlternate, Qt::DiagCrossPattern));
                ui->tableWidget->setItem(i, w, it);
            }
        }

        QTableWidgetItem *headeritem = new QTableWidgetItem();
        headeritem->setText(QVariant(meetingday.year()).toString() + "\n" + QVariant(meetingday.month()).toString() + "\n" + QVariant(meetingday.day()).toString());
        if (month != meetingday.month()) {
            month = meetingday.month();
            if (headerColor == colorBase) {
                headerColor = colorAlternate;
            } else {
                headerColor = colorBase;
            }
        }
        headeritem->setBackground(tempdate == firstdayofweek ? palette().highlight().color() : headerColor);

        ui->tableWidget->setHorizontalHeaderItem(w, headeritem);

        // new LMM
        if (!lmm_chairman_and_prayers.empty() && tempdate.year() >= 2016) {
            for (unsigned int i = 0; i < lmm_chairman_and_prayers.size(); i++) {
                sql_item si = lmm_chairman_and_prayers[i];
                if (si.value("date").toDate() == tempdate) {
                    int userid = si.value("chairman").toInt();
                    int rowindex = retreiveRow(userid);
                    if (rowindex >= 0) {
                        int sortIndex = MeetingPartClass::toTalkId(MeetingPart::LMM_Chairman);
                        QString text = LMM_historyTableAbbreviation(MeetingPart::LMM_Chairman);
                        addToCell(rowindex, w, sortIndex, text, LMM_foregroundColor(MeetingPart::LMM_Chairman, colorFG), LMM_backgroundColor(MeetingPart::LMM_Chairman, colorBG));
                    }
                }
            }
        }

        //  new LMM (school and others)
        if (!lmm.empty() && tempdate.year() >= 2016) {
            for (unsigned int i = 0; i < lmm.size(); i++) {
                sql_item si = lmm[i];
                if (si.value("date").toDate() == tempdate) {
                    int userid = si.value("assignee_id").toInt();
                    int repuserid = si.value("volunteer_id").toInt();
                    int rowindex = retreiveRow(userid);
                    int reprowindex = retreiveRow(repuserid);
                    if (reprowindex >= 0)
                        rowindex = reprowindex;
                    if (rowindex >= 0) {
                        // talk number
                        if (si.value("talk_id").toInt() <= 0)
                            continue;

                        MeetingPart meetingPart;
                        int sequence(0);
                        LMM_Schedule::splitDbTalkId(si.value("talk_id").toInt(), meetingPart, sequence);
                        AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(MeetingType::MidweekMeeting, meetingPart);
                        if (!assignmentInfo)
                            continue;
                        if (!wantSpiritualGem && assignmentInfo->meetingSection() == MeetingSection::TreasuresFromGodsWord) // Higlight, Digging, #R
                            continue;
                        if (!wantApplyToMinistry && assignmentInfo->meetingSection() == MeetingSection::ApplyYourselfToTheFieldMinistry) // #R, Prepare, #1, #2, #3
                            continue;
                        if (!wantChristianLife && assignmentInfo->meetingSection() == MeetingSection::LivingAsChristians) // CL1, CL2, CL3, BS
                            continue;

                        QString talk_text = LMM_historyTableAbbreviation(meetingPart), text;

                        if (assignmentInfo->assignmentCategory() == AssignmentCategory::StudentPart || assignmentInfo->meetingSection() == MeetingSection::ApplyYourselfToTheFieldMinistry) {
                            text = talk_text;
                            // show school classes if multiple exists`
                            if (school_qty > 1)
                                text += QString(" (%1)").arg(si.value("classnumber").toString());
                        } else if (meetingPart == MeetingPart::LMM_CBS)
                            text = talk_text + "-" + tr("C", "abbreviation of the 'conductor'");
                        else
                            text = talk_text;

                        // show volunteer
                        if (reprowindex >= 0)
                            text = ">" + text;

                        // merge with other assigment if any
                        int sortIndex = MeetingPartClass::toTalkId(meetingPart);
                        addToCell(rowindex, w, sortIndex, text, LMM_foregroundColor(meetingPart, colorFG), LMM_backgroundColor(meetingPart, colorBG));
                        bool completed = si.value("completed").toInt() != 0;
                        if (!completed && assignmentInfo->canCounsel()) {
                            auto item = ui->tableWidget->item(rowindex, w);
                            QFont f(item->font());
                            f.setBold(true);
                            item->setFont(f);
                            if (tempdate <= QDateTime::currentDateTime().date()) {
                                // assignment cell: font in red
                                QBrush brush = item->foreground();
                                brush.setColor(tbStyle->alertColor());
                                item->setForeground(brush);

                                // proclaimer name cell: font in red
                                auto itemProclaimer = ui->tableWidget->item(rowindex, 0);
                                brush = itemProclaimer->foreground();
                                brush.setColor(tbStyle->alertColor());
                                itemProclaimer->setForeground(brush);
                            }
                        }

                        //
                        // and now the assistant (or reader)
                        //

                        userid = si.value("assistant_id").toInt();
                        rowindex = retreiveRow(userid);
                        if (rowindex >= 0) {
                            // get talk abbreviation for assistant/householer
                            talk_text = LMM_historyTableAbbreviation(meetingPart, true);
                            // merge with other assigment if any
                            if (meetingPart == MeetingPart::LMM_CBS)
                                addToCell(rowindex, w, sortIndex, talk_text, LMM_foregroundColor(meetingPart, colorFG), LMM_backgroundColor(meetingPart, colorBG));
                            else // no sort and no background color for householders
                                addToCell(rowindex, w, MeetingPartClass::toTalkId(MeetingPart::None), talk_text, LMM_foregroundColor(meetingPart, colorFG));
                        }
                    }
                }
            }
        }

        // weekend
        if (!weekend.empty()) {
            for (unsigned int i = 0; i < weekend.size(); i++) {
                sql_item sqlrow = weekend[i];
                if (sqlrow.value("weekof").toDate() == tempdate) {
                    // publik meeting
                    if (wantPublicTalk) {
                        // speaker
                        int rowindex = retreiveRow(sqlrow.value("speaker_id").toInt());
                        if (rowindex >= 0 && rowindex < ui->tableWidget->rowCount()) {
                            QString num = sqlrow.value("theme_number").toString();
                            QString text = tr("PT", "abbreviation of 'public talk' for history table") + "-" + num;
                            addToCell(rowindex, w, MeetingPartClass::toTalkId(MeetingPart::PublicTalk), text, tbStyle->publicTalkTextColor(), tbStyle->publicTalkColor());
                        }
                        // chairman (without backround, because it's not a huge thing to prepare ;)
                        rowindex = retreiveRow(sqlrow.value("chairman_id").toInt());
                        if (rowindex >= 0) {
                            QString text = tr("Ch", "abbreviation of public talk 'chairman' for history table");
                            addToCell(rowindex, w, MeetingPartClass::toTalkId(MeetingPart::PM_Chairman), text, tbStyle->publicTalkTextColor());
                        }
                    }
                    // watchtower
                    if (wantWatchtower) {
                        // just the reader, without background (as for all reading tasks, except Bible Reading)
                        int rowindex = retreiveRow(sqlrow.value("wtreader_id").toInt());
                        if (rowindex >= 0) {
                            QString text = tr("WT-R", "abbreviation of 'watchtower reader' for history table");
                            addToCell(rowindex, w, MeetingPartClass::toTalkId(MeetingPart::WatchtowerStudy), text, tbStyle->watchtowerStudyTextColor(), tbStyle->watchtowerStudyColor());
                        }
                    }
                }
            }
        }

        if (!e1.empty()) {
            for (unsigned int i = 0; i < e1.size(); i++) {
                sql_item ei = e1[i];
                QDate d1 = ei.value("start_date").toDate();
                QDate d2 = ei.value("end_date").toDate();
                if (d1 <= meetingday && d2 >= meetingday) {
                    int userid = ei.value("person_id").toInt();
                    int rowindex = retreiveRow(userid);
                    if (rowindex >= 0) {
                        //ui->tableWidget->setItem(rowindex,w,new QTableWidgetItem("P"));
                        auto status = new HistoryTableWidgetItem;
                        status->setIcon(QIcon(QPixmap(":/icons/warning_inactive.svg").scaled(25, 25)));
                        ui->tableWidget->setItem(rowindex, w, status);
                    }
                }
                // SELECT * FROM unavailables WHERE start_date <= '2010-06-13' AND end_date >= '2010-06-13'
            }
        }
        tempdate = tempdate.addDays(7);
    }
    ui->tableWidget->setSortingEnabled(true);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);

    // it is needed to have an item everywhere in order the 'sort' works correctly...
    for (int i = 0; i < ui->tableWidget->rowCount(); ++i)
        for (int j = 0; j < ui->tableWidget->columnCount(); ++j)
            if (ui->tableWidget->item(i, j) == nullptr)
                ui->tableWidget->setItem(i, j, new HistoryTableWidgetItem);
}

bool historytable::windowMinimized()
{
    return mWindowMinimized;
}

int historytable::minimizedWidth()
{
    return ui->btn_open->width();
}

void historytable::showHistory()
{
    on_btn_open_clicked();
}

int historytable::retreiveRow(int userid)
{
    //return idlist.indexOf(userid);
    if (ui->tableWidget == nullptr)
        return -1;
    for (int i = 0; i < ui->tableWidget->rowCount(); ++i) {
        auto item = ui->tableWidget->item(i, 0); //->verticalHeaderItem(i);
        if (item && item->data(DataInfo::UserID) == userid)
            return item->row();
    }
    return -1;
}

void historytable::updateCellPixmap(int row, int col)
{
    auto item = ui->tableWidget->item(row, col);
    if (item == nullptr)
        return;

    bool hasData = false;
    int data = ui->tableWidget->item(row, col)->data(DataInfo::Sorting).toInt(&hasData);

    if (hasData == false)
        return;

    auto assignedMask = ui->tableWidget->item(row, col)->data(DataInfo::AssignMask).toInt(&hasData);
    if (!hasData)
        assignedMask = 0;

    QPixmap px;
    QColor fillColor;
    auto talk_id = data;
    AssignmentInfo *assignmentInfo = assignmentInfos->findAssignmentInfo(talk_id);
    if (assignmentInfo == nullptr)
        return;
    if (assignmentInfo && assignmentInfo->meetingSection() == MeetingSection::TreasuresFromGodsWord) {
        px = QPixmap(":/icons/lmm-gw.svg");
        fillColor = tbStyle->lmmSection1TextColor();
    } else if (assignmentInfo && assignmentInfo->meetingSection() == MeetingSection::ApplyYourselfToTheFieldMinistry) {
        px = QPixmap(":/icons/lmm-fm.svg");
        fillColor = tbStyle->lmmSection2TextColor();
    } else if (assignmentInfo && assignmentInfo->meetingSection() == MeetingSection::LivingAsChristians) {
        px = QPixmap(":/icons/lmm-cl.svg");
        fillColor = tbStyle->lmmSection3TextColor();
    } else if (talk_id <= MeetingPartClass::toTalkId(MeetingPart::PublicTalk)) {
        px = QPixmap(":/icons/pt.svg");
        fillColor = tbStyle->publicTalkTextColor();
    } else {
        px = QPixmap(":/icons/wt.svg");
        fillColor = tbStyle->watchtowerStudyTextColor();
    }

    int mask = talk_id >= 0 ? int(1 << talk_id) : 0;
    if ((mask & assignedMask) && (assignedMask - mask))
        fillColor = tbStyle->alertColor();

    QPixmap pixmap(px.size());
    pixmap.fill(fillColor);
    pixmap.setMask(px.createMaskFromColor(Qt::transparent));
    item->setIcon(pixmap);
}

void historytable::addToCell(int row, int col, int sort_index, QString const &text, QColor fg, QColor bg)
{
    QString fullText = text;
    bool hasData = false;
    int data = 0, assignedMask = 0;
    if (ui->tableWidget->item(row, col) && ui->tableWidget->item(row, col)->text() != "") {
        QString currText = ui->tableWidget->item(row, col)->text();
        fullText = currText + "," + text;

        assignedMask = ui->tableWidget->item(row, col)->data(DataInfo::AssignMask).toInt(&hasData);
        if (!hasData)
            assignedMask = 0;
        data = ui->tableWidget->item(row, col)->data(DataInfo::Sorting).toInt(&hasData);
    }

    int mask = sort_index >= 0 ? int(1 << sort_index) : 0;
    if (mask & assignedMask)
        return; // detect that there is a doublon in the queried table (would be better to fix the SQL...)

    if (fullText.isEmpty())
        fullText = "???"; // debug

    // prefer reassign to reset colors (especially the background)
    auto item = new HistoryTableWidgetItem(fullText);
    ui->tableWidget->setItem(row, col, item);
    if (hasData) //&& sorting_value > 0)
    {
        if (sort_index != MeetingPartClass::toTalkId(MeetingPart::None))
            item->setData(DataInfo::Sorting, std::min(data, int(sort_index)));
        else
            item->setData(DataInfo::Sorting, data);
    } else if (sort_index != MeetingPartClass::toTalkId(MeetingPart::None))
        item->setData(DataInfo::Sorting, sort_index);
    item->setData(DataInfo::AssignMask, assignedMask | mask);

    if (!fullText.isEmpty())
        item->setToolTip(fullText.replace(",", "\n"));

    //QBrush brush = ui->tableWidget->item(row,col)->foreground();
    //brush.setColor(tbStyle->lmmSection3TextColor());
    //ui->tableWidget->item(row,col)->setForeground(brush);
    item->setForeground(fg);

    if (bg.isValid()) {
        item->setBackground(bg);
        addColors(row, col);
    }
    updateCellPixmap(row, col);
    // task conflict  to enhance cause Chairman or reading should not raise conflict
    // and red color is too flashy
    //if (fullText.contains("\n"))
    //    item->setBackground(Qt::red);
}

void historytable::addColors(int row, int col)
{
    // color
    int colorvalue = 160;
    int reduction = colorvalue / ui->spinBoxGreyPeriod->value();
    for (int ci = 1; ci < ui->spinBoxGreyPeriod->value() + 1; ci++) {
        if (ui->tableWidget->columnCount() <= col + ci)
            continue;
        if (ui->tableWidget->item(row, col + ci) == nullptr || ui->tableWidget->item(row, col + ci)->text().isEmpty()) {
            QTableWidgetItem *item = new HistoryTableWidgetItem;
            item->setBackground(QColor::fromRgb(128, 128, 128, colorvalue));
            ui->tableWidget->setItem(row, col + ci, item);
        }
        colorvalue -= reduction;
    }
}

void historytable::getPublicMeetingHistory()
{
    cpersons pc;

    ccongregation::congregation myCongregation = c.getMyCongregation();
    int publicmeetingday = myCongregation.getPublicmeeting(firstdayofweek).getMeetingday();
    QList<person *> users = pc.getAllPersons(3);
    QList<int> idlist;
    for (int i = 0; i < users.size(); i++) {
        idlist.append(users[i]->id());
    }

    QList<ccongregation::congregation> clist = c.getAllCongregations();

    int iweeks_past = ui->spinBox_past->value();
    int iweeks_future = ui->spinBox_future->value();
    int iweeks = iweeks_past + iweeks_future;
    ui->tableWidget->clear();
    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->setColumnCount(6);
    for (int i = 0; i < ui->tableWidget->columnCount(); i++)
        ui->tableWidget->setColumnHidden(i, false);
    ui->tableWidget->setRowCount(iweeks);

    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Date")));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Theme")));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Speaker")));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Congregation")));
    ui->tableWidget->setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Chairman")));
    ui->tableWidget->setHorizontalHeaderItem(5, new QTableWidgetItem(tr("Reader")));
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Interactive);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Interactive);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Interactive);

    QDate first_date = firstdayofweek.addDays(-7 * (iweeks_past - 1));
    QDate last_date = firstdayofweek.addDays(7 * (iweeks_future));

    sql_items q;
    q = sql->selectSql("SELECT * from publicmeetinghistory WHERE weekof >= '" + first_date.toString(Qt::ISODate) + "' AND weekof <= '" + last_date.toString(Qt::ISODate) + "'");

    QDate tempdate = first_date;
    for (int w = 0; w < iweeks; w++) {
        //ui->tableWidget->setItem(w,0, new QTableWidgetItem(tempdate.addDays(publicmeetingday-1).toString(Qt::DefaultLocaleShortDate)));
        auto date_item = new QTableWidgetItem(QLocale().toString(tempdate.addDays(publicmeetingday - 1), QLocale::ShortFormat));
        date_item->setData(Qt::EditRole, tempdate.addDays(publicmeetingday - 1));
        ui->tableWidget->setItem(w, 0, date_item);
        if (!q.empty()) {
            for (unsigned int i = 0; i < q.size(); i++) {
                sql_item sqlrow = q[i];
                if (sqlrow.value("weekof").toDate() == tempdate) {
                    ui->tableWidget->setItem(w, 1, new QTableWidgetItem(sqlrow.value("theme").toString()));
                    int rowindex = idlist.indexOf(sqlrow.value("speaker_id").toInt());
                    if (rowindex >= 0) {
                        ui->tableWidget->setItem(w, 2, new QTableWidgetItem(users[rowindex]->fullname()));

                        for (ccongregation::congregation const &c : clist) {
                            if (c.id == users[rowindex]->congregationid()) {
                                ui->tableWidget->setItem(w, 3, new QTableWidgetItem(c.name));
                                break;
                            }
                        }
                    }
                    rowindex = idlist.indexOf(sqlrow.value("chairman_id").toInt());
                    if (rowindex >= 0) {
                        ui->tableWidget->setItem(w, 4, new QTableWidgetItem(users[rowindex]->fullname()));
                    }
                    rowindex = idlist.indexOf(sqlrow.value("wtreader_id").toInt());
                    if (rowindex >= 0) {
                        ui->tableWidget->setItem(w, 5, new QTableWidgetItem(users[rowindex]->fullname()));
                    }
                }
            }
        }
        tempdate = tempdate.addDays(7);
    }
}

void historytable::changeFilterButtonColor(QToolButton *button, QColor color)
{
    QPalette p = palette();
    QIcon icon;
    QIcon normalIcon = general::changeIconColor(button->icon(), p.color(QPalette::Window));
    QIcon selectedIcon = general::changeIconColor(button->icon(), p.color(QPalette::ButtonText));
    icon.addPixmap(normalIcon.pixmap(button->iconSize()), QIcon::Normal, QIcon::State::Off);
    icon.addPixmap(selectedIcon.pixmap(button->iconSize()), QIcon::Normal, QIcon::State::On);
    button->setIcon(icon);
    QString style = QString("QToolButton { background-color: %1; } QToolButton:checked {  background-color: %2; }")
                            .arg(color.name())
                            .arg(palette().button().color().name());
    button->setStyleSheet(style);
}

void historytable::on_toolButton_clicked()
{
    refreshHistory(showpublictalk);
}

void historytable::on_spinBoxGreyPeriod_valueChanged(int arg1)
{
    QSettings settings;
    settings.setValue("historytable/grey", arg1);
}

void historytable::on_btn_filter_sg_clicked()
{
    refreshHistory();
}

void historytable::on_btn_filter_am_clicked()
{
    refreshHistory();
}

void historytable::on_btn_filter_cl_clicked()
{
    refreshHistory();
}

void historytable::on_btn_filter_pt_clicked()
{
    refreshHistory();
}

void historytable::on_btn_filter_wt_clicked()
{
    refreshHistory();
}

//-----------------------------------------------------------------------------
// FormatedString
//-----------------------------------------------------------------------------

FormatedString::FormatedString(QString t)
    : m_text(t)
{
}

FormatedString::FormatedString(char *t)
    : m_text(QString::fromUtf8(t))
{
}

void FormatedString::setText(QString const &t)
{
    m_text = t;
}

QString FormatedString::text() const
{
    return m_text;
}

void FormatedString::setIconName(QString const &i)
{
    m_iconName = i;
}

QString FormatedString::iconName() const
{
    return m_iconName;
}

void FormatedString::setForeground(QColor c)
{
    this->m_customFG = true;
    this->m_foregroundColor = c;
}

void FormatedString::setBackground(QColor c)
{
    this->m_customBG = true;
    this->m_backgroundColor = c;
}

QColor FormatedString::foreground() const
{
    return m_customFG ? m_foregroundColor : Qt::black;
}

QColor FormatedString::background() const
{
    return m_customBG ? m_backgroundColor : Qt::white;
}

bool FormatedString::customForeground() const
{
    return m_customFG;
}

bool FormatedString::customBackground() const
{
    return m_customBG;
}

void FormatedString::setStrikeOut(bool b)
{
    m_strikeOut = b;
}

bool FormatedString::strikeOut() const
{
    return m_strikeOut;
}

void FormatedString::setItalic(bool b)
{
    m_italic = b;
}

bool FormatedString::italic() const
{
    return m_italic;
}

void FormatedString::setBold(bool b)
{
    m_bold = b;
}

bool FormatedString::bold() const
{
    return m_bold;
}

bool FormatedString::operator<(FormatedString const &other) const
{
    return this->m_text < other.m_text;
}

bool FormatedString::operator>(FormatedString const &other) const
{
    return this->m_text > other.m_text;
}

void historytable::on_btn_close_clicked()
{
    ui->frameExpand->setVisible(false);
    ui->frameButtons->setVisible(false);
    ui->tableWidget->setVisible(false);
    this->resize(this->width(), 0);
    mWindowMinimized = true;
    emit visibleChanged(false);
}

void historytable::mousePressEvent(QMouseEvent *event)
{
    if (mWindowMinimized)
        return;
    if (event->pos().y() < 10) {
        oldPos = event->globalPos();
        this->setCursor(Qt::SizeVerCursor);
        mousePressed = true;
    }
    event->accept();
}

void historytable::mouseMoveEvent(QMouseEvent *event)
{
    if (mWindowMinimized)
        return;
    if (mousePressed) {
        const QPoint delta = event->globalPos() - oldPos;
        oldPos = event->globalPos();
        move(this->pos().x(), this->pos().y() + delta.y());
        resize(this->width(), this->height() + (delta.y() * -1));
        if (this->height() < 50) {
            mouseReleaseEvent(event);
            on_btn_close_clicked();
        }
    } else {
        this->setCursor(event->pos().y() < 6 ? Qt::SizeVerCursor : Qt::ArrowCursor);
    }
    event->accept();
}

void historytable::mouseReleaseEvent(QMouseEvent *event)
{
    this->setCursor(Qt::ArrowCursor);
    mousePressed = false;
    event->accept();
}

void historytable::on_btn_open_clicked()
{
    ui->frameExpand->setVisible(false);
    ui->frameButtons->setVisible(true);
    ui->tableWidget->setVisible(true);
    resize(this->width(), 200);
    mWindowMinimized = false;
    emit visibleChanged(true);
}
