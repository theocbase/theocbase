/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TODOMODEL_H
#define TODOMODEL_H

#include <QAbstractTableModel>
#include "todo.h"

class TodoModel : public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
public:
    enum Roles {
        None = 0,
        IdRole = Qt::UserRole,
        CongregationNameRole,
        CongregationIdRole,
        SpeakerFullNameRole,
        SpeakerIdRole,
        DateRole,
        TimeRangeRole,
        ThemeAndNumberRole,
        ThemeIdRole,
        ThemeNumberRangeRole,
        NotesRole,
        IsIncomingRole
    };
    Q_ENUM(Roles)

    enum class GroupByRoles : quint16 {
        CongregationNameRole = quint16(Roles::CongregationNameRole),
        SpeakerFullNameRole = quint16(Roles::SpeakerFullNameRole),
        ThemeNumberRangeRole = quint16(Roles::ThemeNumberRangeRole),
        TimeRangeRole = quint16(Roles::TimeRangeRole)
    };

    explicit TodoModel(QObject *parent = nullptr);
    ~TodoModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QHash<int, QByteArray> roleNames() const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    Q_INVOKABLE void loadList();
    Q_INVOKABLE int addRow(const bool inComing);
    Q_INVOKABLE void removeRow(const int rowIndex);
    Q_INVOKABLE void saveRow(const int rowIndex);
    Q_INVOKABLE void editRow(const int row, const QDate date, const QString speakerFullName, const QString congregationName, const QString themeAndNumber, const QString notes);
    Q_INVOKABLE QVariantMap get(int row) const;
    Q_INVOKABLE QModelIndex getTodoIndex(int todoId) const;
    Q_INVOKABLE QString moveToSchedule(const int row);

signals:
    void modelChanged();

private:
    QList<todo *> _list;
};

class TodoSFProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QObject *source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(bool isIncoming READ isIncoming WRITE setIsIncoming NOTIFY isIncomingChanged)
    Q_PROPERTY(QString filterText READ filterText WRITE setFilterText NOTIFY filterChanged)
    Q_PROPERTY(QByteArray sortRole READ sortRole WRITE setSortRole NOTIFY sortChanged)
    Q_PROPERTY(QByteArray groupByRole READ groupByRole WRITE setGroupByRole NOTIFY groupByChanged)

public:
    TodoSFProxyModel(QObject *parent = nullptr);

    QObject *source() const;
    void setSource(QObject *source);

    bool isIncoming() const;
    void setIsIncoming(bool newIsIncoming);

    QString filterText() const;
    void setFilterText(QString value);

    QByteArray sortRole() const;
    void setSortRole(const QByteArray &role);

    QByteArray groupByRole() const;
    void setGroupByRole(const QByteArray &role);

    Q_INVOKABLE virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
    {
        QSortFilterProxyModel::sort(column, order);
        emit sortChanged();
    }

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
    int roleKey(const QByteArray &role) const;

private:
    bool m_isIncoming;
    QString m_filterText;
    QByteArray m_groupByRole;

signals:
    void sourceChanged();
    void isIncomingChanged();
    void filterChanged();
    void sortChanged();
    void groupByChanged();
};

class TodoValidator : public QValidator
{
    Q_OBJECT
    Q_PROPERTY(TodoModel *model READ model WRITE setModel NOTIFY modelChanged)
    Q_PROPERTY(int todoId READ todoId WRITE setTodoId NOTIFY todoIdChanged)
    Q_PROPERTY(TodoModel::Roles role READ role WRITE setRole NOTIFY roleChanged)
public:
    TodoValidator(QObject *parent = nullptr);
    ~TodoValidator();

    QValidator::State validate(QString &input, int &pos) const override;

    TodoModel *model() const;
    void setModel(TodoModel *newModel);

    TodoModel::Roles role() const;
    void setRole(TodoModel::Roles newRole);

    int todoId() const;
    void setTodoId(int newTodoId);

Q_SIGNALS:
    void modelChanged();
    void todoIdChanged();
    void errorChanged(const QString &error) const;
    void roleChanged();

private:
    TodoModel *m_model;
    int m_todoId;
    TodoModel::Roles m_role;
};
#endif // TODOMODEL_H
