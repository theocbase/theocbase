#ifndef PDFFORM_H
#define PDFFORM_H

#include <QString>
#include <QStringList>

class pdfform
{
public:
    pdfform();


    class field {
    public:
        field(QString name, bool useParens);
        virtual QString value();
        QString fdfvalue();
        QString Name() const;
    protected:
        QString convertUnicode16BE(QString txt);
        QString name;
        bool useParens;
    };

    class checkbox : public field {
    public:
        checkbox(QString name);
        bool getValue();
        void setValue(bool v);
        QString value();
    private:
        bool ckvalue;
    };

    class textbox : public field {
    public:
        textbox(QString name);
        QString getValue();
        void setValue(QString v);
        QString value();
    private:
        QString txtvalue;
    };


    void append(field* f);

    QString fdfvalue();

private:
    QList<field*> fields;
};

#endif // PDFFORM_H
