#! /bin/bash

# Exit script on error
set -e

# Constants
INSTALL_BUILDER_VERSION=23.7.0
THEOCBASE_SOURCE_DIR=/theocbase.src
THEOCBASE_BUILD_DIR=/build-theocbase
THEOCBASE_APP_DIR=/TheocBase.AppDir
# Install builder project file (Theocbase.xml) expects installer files to be located in subdirectory of the Thecobase source directory
# (local copy of the git repo in this case)
THEOCBASE_INSTALLER_DIR=${THEOCBASE_SOURCE_DIR}/installer
QT_BIN_DIR=/usr/local/qt5/bin
QT_LIB_DIR=/usr/local/qt5/lib
QT_SHARE_DIR=/usr/local/qt5
INSTALL_BUILDER=/opt/installbuilder-${INSTALL_BUILDER_VERSION}

# Ensure distribution ouput directory is clean
rm -rf /dist/*
rm -rf ${THEOCBASE_SOURCE_DIR}
rm -rf ${THEOCBASE_BUILD_DIR}

# Clone the version specified in .env file
git clone -b ${THEOCBASE_BRANCH} --depth=1 https://bitbucket.org/theocbase/theocbase.git ${THEOCBASE_SOURCE_DIR}
cd ${THEOCBASE_SOURCE_DIR}
GIT_HASH=$(git log --pretty=format:'%h' -n 1)
cd ..

# Check for local otherwise add required constants.h file from dropbox
CONSTANTS=/constants.h
if [ -f "$CONSTANTS" ]; then
    cp ${CONSTANTS} ${THEOCBASE_SOURCE_DIR}/src/
else
    wget -q -O ${THEOCBASE_SOURCE_DIR}/src/constants.h  https://www.dropbox.com/s/r0kycavb651otey/constants.h?dl=0
fi

##### Create builddir and execute compilation #####
mkdir ${THEOCBASE_BUILD_DIR}
cd ${THEOCBASE_BUILD_DIR}
${QT_BIN_DIR}/qmake CONFIG+=Release ${THEOCBASE_SOURCE_DIR}/src/theocbase.pro
make -j $(nproc)
make install
cd /

##### Perpare AppDir #####
mkdir -p ${THEOCBASE_APP_DIR}/usr/bin
mkdir -p ${THEOCBASE_APP_DIR}/usr/lib
mkdir -p ${THEOCBASE_APP_DIR}/usr/share/applications
mkdir -p ${THEOCBASE_APP_DIR}/usr/share/icons/hicolor/256x256/apps
mkdir -p ${THEOCBASE_APP_DIR}/usr/plugins/geoservices
mkdir -p ${THEOCBASE_APP_DIR}/usr/translations
mkdir -p ${THEOCBASE_APP_DIR}/usr/qml
mkdir -p ${THEOCBASE_APP_DIR}/usr/bin/docs
mkdir -p ${THEOCBASE_APP_DIR}/usr/bin/autoupdate
cp -pv ${THEOCBASE_BUILD_DIR}/release/theocbase 		    ${THEOCBASE_APP_DIR}/usr/bin
cp -pv ${THEOCBASE_SOURCE_DIR}/src/images/theocbase.png     ${THEOCBASE_APP_DIR}/theocbase.png
cp -pv ${THEOCBASE_SOURCE_DIR}/src/images/theocbase.png     ${THEOCBASE_APP_DIR}/usr/share/icons/hicolor/256x256/apps/theocbase.png
cp -pv ${THEOCBASE_SOURCE_DIR}/src/translations/*.qm        ${THEOCBASE_APP_DIR}/usr/bin/
cp -pv ${THEOCBASE_SOURCE_DIR}/docs/*.qhc                   ${THEOCBASE_APP_DIR}/usr/bin/docs/
cp -pv ${THEOCBASE_SOURCE_DIR}/docs/*.qch                   ${THEOCBASE_APP_DIR}/usr/bin/docs/
cp -prv ${THEOCBASE_SOURCE_DIR}/templates                   ${THEOCBASE_APP_DIR}/usr/bin/
# QT file that require manual copy
cp -pv ${QT_BIN_DIR}/assistant                              ${THEOCBASE_APP_DIR}/usr/bin
cp -pv ${QT_SHARE_DIR}/translations/assistant*.qm           ${THEOCBASE_APP_DIR}/usr/translations
cp -prv ${QT_SHARE_DIR}/plugins/qmltooling                  ${THEOCBASE_APP_DIR}/usr/plugins/
cp -prv ${QT_SHARE_DIR}/plugins/sensors                     ${THEOCBASE_APP_DIR}/usr/plugins/
cp -pv ${QT_SHARE_DIR}/plugins/geoservices/libqtgeoservices_osm.so    ${THEOCBASE_APP_DIR}/usr/plugins/geoservices/
cp -pv ${QT_SHARE_DIR}/plugins/geoservices/libqtgeoservices_nokia.so  ${THEOCBASE_APP_DIR}/usr/plugins/geoservices/
cp -prv ${QT_SHARE_DIR}/plugins/webview                     ${THEOCBASE_APP_DIR}/usr/plugins/
cp -prv ${QT_SHARE_DIR}/qml/QtWebView                       ${THEOCBASE_APP_DIR}/usr/qml/
# Required additional system library
#cp -pv /usr/lib64/libsoftokn3.so                      ${THEOCBASE_APP_DIR}/usr/lib/
cp -pv /lib/x86_64-linux-gnu/libgpg-error.so.0              ${THEOCBASE_APP_DIR}/usr/lib/
# Autoupdate
${INSTALL_BUILDER}/autoupdate/bin/customize.run build ${THEOCBASE_SOURCE_DIR}/bitrock/autoUpdateProject.xml linux-x64
cp -pv ${INSTALL_BUILDER}/autoupdate/output/autoupdate-linux-x64.run ${THEOCBASE_APP_DIR}/usr/bin/autoupdate
cp -pv ${THEOCBASE_SOURCE_DIR}/update.ini                            ${THEOCBASE_APP_DIR}/usr/bin/autoupdate
# Generate temporary desktop file, which is used to build
cat > ${THEOCBASE_APP_DIR}/usr/share/applications/TheocBase.desktop << EOF
[Desktop Entry]
Version=1.0
Name=TheocBase
GenericName=TheocBase
Comment=TheocBase Team
Exec=theocbase
Terminal=false
Type=Application
Categories=Utility;TextEditor;
MimeType=text/thb;
Icon=theocbase
EOF

##### Generate AppDir #####
# Required
export QMAKE=${QT_BIN_DIR}/qmake
export QML_SOURCES_PATHS=${THEOCBASE_SOURCE_DIR}/src/qml
# Check https://github.com/linuxdeploy/linuxdeploy-plugin-qt/blob/master/src/qt-modules.h for list of supported plugins
export EXTRA_QT_PLUGINS=''
export LD_LIBRARY_PATH=${QT_LIB_DIR}
./linuxdeploy-x86_64.AppImage --appdir ${THEOCBASE_APP_DIR} --desktop-file ${THEOCBASE_APP_DIR}/usr/share/applications/TheocBase.desktop --plugin qt

# Remove libnss libraries, linuxdeploy qt plugin does not seem to support exlusion of libraries yet
#   Removed for OpenSuse Leap 15.3
rm -f ${THEOCBASE_APP_DIR}/usr/lib/libnss*.so
rm -f ${THEOCBASE_APP_DIR}/usr/lib/libX*.so.*
#   Required for Ubuntu 20.04
rm -f ${THEOCBASE_APP_DIR}/usr/lib/libsmime3.so

##### Prepare installer dir #####
mkdir -p ${THEOCBASE_INSTALLER_DIR}/autoupdate
mkdir -p ${THEOCBASE_INSTALLER_DIR}/languages
# TheocBase files
cp -prv ${THEOCBASE_APP_DIR}/usr/* 					  ${THEOCBASE_INSTALLER_DIR}
mv -v ${THEOCBASE_INSTALLER_DIR}/bin/docs             ${THEOCBASE_INSTALLER_DIR}
mv -v ${THEOCBASE_INSTALLER_DIR}/bin/autoupdate       ${THEOCBASE_INSTALLER_DIR}
mv -v  ${THEOCBASE_INSTALLER_DIR}/bin/templates       ${THEOCBASE_INSTALLER_DIR}
cp -pv ${THEOCBASE_SOURCE_DIR}/src/translations/*.qm  ${THEOCBASE_INSTALLER_DIR}/languages/
# Startup script
cat >  /${THEOCBASE_INSTALLER_DIR}/theocbase.sh << EOF &&
#!/bin/sh
appname=\`basename \$0 | sed s,\.sh$,,\`

dirname=\`dirname \$0\`
tmp="\${dirname#?}"

if [ "\${dirname%\$tmp}" != "/" ]; then
dirname=\$PWD/\$dirname
fi

\$dirname/AppRun --no-sandbox \$*
EOF

##### Generate installer #####
# If installer build was executed correctly, copy installer to dist volume
${INSTALL_BUILDER}/bin/builder build ${THEOCBASE_SOURCE_DIR}/bitrock/TheocBase.xml --license /license.xml

THEOCBASE_VERSION=$(cat ${THEOCBASE_SOURCE_DIR}/bitrock/TheocBase.xml | grep -i "<version>" | cut -d ">" -f2 | cut -d "<" -f1)

if [[ ${RELEASE} = "True" ]]
then
	NEW_FILE_NAME=TheocBase-${THEOCBASE_VERSION}-linux-x64-installer.run
else
	NEW_FILE_NAME=TheocBase-${THEOCBASE_VERSION}-TESTBUILD-${GIT_HASH}-linux-x64-installer.run
fi

cp -pv ${THEOCBASE_SOURCE_DIR}/bitrock/output/*.run /dist/${NEW_FILE_NAME}
cd /dist/
sha256sum ${NEW_FILE_NAME} > SHA256SUM
