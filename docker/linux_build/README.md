# TheocBase installation instruction

All commands should be executed from this location (The same directory in which this README.md file is located)
Ensure you first copy the license.xml file. Edit .env file. The value `THEOCBASE_VERSION` should reflect the version that needs to be compiled.
After a succesfull run of the container, the install package should be located in the `dist` directory.

## Linux execution
### Podman
1. Install Podman. E.g. On a RedHat Enterprise Linux 8 based system: `dnf -y install podman`
1. Install podman-compose: `pip3 install --user podman-compose`
1. Execute the build: `podman-compose up --build --force-recreate`

The open build container to debug:
`podman run -ti --env-file=.env -v ./dist:/dist --privileged theocbase_compile bash`

### Docker
1. Install Docker using the default package manager of your OS. Some systems (like RedHat Enterpise Linux 8 based systems) switched to Podman instead. But it more or less compatible.
1. Ensure you are allowed to execute the container as a normal user (or use sudo):
1. On a RedHat Enterprise Linux 7 compatible system this could be achieved by executing the following steps.
    1. Create a group called docker: `sudo groupadd docker` 
    1. Add your user account to the docker group: `sudo usermod -aG docker $USER`
    1. Restart the docker daemon: `sudo systemctl restart docker`
    1. Either restart your session or execute: `newgrp docker`

## Windows and MacOS execution
1. Install Docker Desktop
1. Execute the following command on the Windows/Powertools (or a MacOS terminal) command line: `docker-compose up --build --force-recreate`